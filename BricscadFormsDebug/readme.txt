Enabling breakpoint in Forms while running Bricscad requires changing in bricscad.exe.config file.
Same options makes problems for unit testing (StackOverflowException with program crash).

Enable Forms debugging only when needed, otherwise disable it.


* Ran from Tools menu (runs BricscadFormsDebugging.exe with parameters - Bricscad directory and "true"/"false" for enable/disable)