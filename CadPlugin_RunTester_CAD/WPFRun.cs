﻿using CadPlugin.KSopt.CADModels.Views.MainWindow.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin_RunTester
{
    class WPFRun
    {
        public void StartWPF()
        {
            new KSoptView().ShowDialog();
        }
    }
}
