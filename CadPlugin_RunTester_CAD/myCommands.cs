﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Reflection;
using System.Diagnostics;
using forms = System.Windows.Forms;
using CadPlugin.Common;
using NetworkKS.Isybau;
using NetworkKS.CADModels.Layouts;
using NetworkKS.CADModels.SewageColors;
using FakeItEasy;
using NetworkKS.CADModels;
using NetworkKS.Networks;
using NetworkKS.CADModels.NamedObjectsDictionary;
using NetworkKS.Controllers;
using NetworkKS.Calculations;
using NetworkKS.CADModels.MainPathCreator;
using CadPlugin.Common.CAD;
using System.Runtime.InteropServices;
using wf = System.Windows.Forms;
//using System.Drawing;
using NetworkKS.CADModels.MainPathCreator.PathWithArcs;
using ks = NetworkKS;
using NetworkKS.SewageElements;
using NetworkKS.Networks.NetworkItems;
using NetworkKS.Views;
using NetworkKS.Views.ConnectionPositionsViews.Model;
using NetworkKS.Views.ConnectionPositionsViews;
using NetworkKS.CADModels.LSection;
using CadPlugin.KHNeu.Views.KHNeuModels;
using CadPlugin.KHNeu.Models;
using CadPlugin.KHNeu._Diagnostics;
using CadPlugin.KHNeu.Models.TerrainHeights;
using CadPlugin.KHNeu.Models.DTMs;
using CadPlugin.KHNeu.Views.Models;
using CadPlugin.KHNeu.Views;
using SewageUtilities.ShaftHelpers;



#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif


// This line is not mandatory, but improves loading performances
[assembly: CommandClass(typeof(CadPlugin_RunTester_CAD.myCommands))]

namespace CadPlugin_RunTester_CAD
{

    public class myCommands
    {
        

        static Tooltip t;
        static Document doc => Application.DocumentManager.MdiActiveDocument;
        static Database db => doc.Database;
        static bool firstRun = true;
        public static void PointMonitorEventHandler(PointMonitorEventArgs e)
        {
            string s = "";
            s += $"\nX={e.Context.ComputedPoint.X.ToString("0.00")} Y={e.Context.ComputedPoint.Y.ToString("0.00")}";
            s += $"\nX={e.Context.RawPoint.X.ToString("0.00")} Y={e.Context.RawPoint.Y.ToString("0.00")}";
            t.DisplayedText = s;
        }
        private static ObjectId? Select3dPoly(string text)
        {
            var opt = new PromptEntityOptions(text);
            opt.SetRejectMessage("Not a 3d poly!");
            opt.AddAllowedClass(typeof(Polyline3d), true);
            var res = doc.Editor.GetEntity(opt);
            if (res.Status == PromptStatus.OK)
            {
                ObjectId oid = res.ObjectId;
                return oid;
            }
            return null;
        }

        private static void TestBlockExplode()
        {
            var ed = doc.Editor;
            var opt = new PromptEntityOptions("Select Block: ");
            opt.SetRejectMessage("Not a Block");
            opt.AddAllowedClass(typeof(BlockReference), true);
            var res = ed.GetEntity(opt);
            if (res.Status == PromptStatus.OK)
            {
                using (TransactionWrapper tw = doc.StartTransaction(true))
                {
                    var br = tw.GetObjectWrite<BlockReference>(res.ObjectId);
                    CADBlockHelper.ScaleBlockReference(tw.Tr, br, 2);
                }
            }
        }

        private static void RotateBlock()
        {
            var ed = doc.Editor;
            var opt = new PromptEntityOptions("Select Block: ");
            opt.SetRejectMessage("Not a Block");
            opt.AddAllowedClass(typeof(BlockReference), true);
            var res = ed.GetEntity(opt);
            if (res.Status == PromptStatus.OK)
            {
                using (TransactionWrapper tw = doc.StartTransaction(true))
                {
                    Vector2d v = new Vector2d(1, 2);
                    var br = tw.GetObjectWrite<BlockReference>(res.ObjectId);
                    //br.Rotate(tw, BlockRotationParameters.CreateRotationBy(45, true, false));
                    br.Rotate(tw, BlockRotationParameters.CreateAlignDirection(v, false, true));
                }
            }
        }
        [CommandMethod("RunMyCommand", CommandFlags.Session)]
        public void RunTests()
        {
            RotateBlock();
            //TestBlockExplode();
            //KHNeuController.StartWindow(doc);
            //new KHNeuSettings().ShowDialog();
            return;

            TerrainHeightFactory.Instance.UpdateDocument(doc);
            var heightType = HeightSourceType.SelectDTM;
            if (heightType == HeightSourceType.SelectDTM)
            {
                if (DTMManager.Instance.Current == null)
                    DTMManager.Instance.SelectDTM(doc);
                if (DTMManager.Instance.Current == null)
                    return;
            }
            using (var highlight = DTMTriangleHighlight.Instance.StartHighlight(doc))
            {
                ITerrainHeight heightCalc = TerrainHeightFactory.Instance.GetHeightCalculator(heightType);
                if (heightCalc == null)
                    return;
                (heightCalc as ITerrainHeightHighlight)?.Highlight();

                // Start 

                using (TransactionWrapper tw = doc.StartTransaction(true))
                {
                    var text = tw.CreateEntity<MText>();
                    var jigger = new NextShaftJigger(doc.Editor, null, null, null);
                    doc.Editor.Drag(jigger);

                }
            }
        }
        [CommandMethod("0DTM", CommandFlags.Session)]
        public void SelectDtm()
        {
            //DTMManager.Instance.SelectDTM(doc);
            DiagnosticsDTM.LoadDTM();
        }
        [CommandMethod("0SelectTriangles")]
        public void SelectTriangles()
        {
            DiagnosticsDTM.MarkTrianglesForSection();
        }
        [CommandMethod("0TrackTriangle")]
        public void TrackTriangles()
        {
            DiagnosticsDTM.TrackPoint();
        }

        private void Editor_PointMonitor(object sender, PointMonitorEventArgs e)
        {

        }
    }


}
