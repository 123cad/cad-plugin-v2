﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autodesk.AutoCAD.Geometry;

namespace testplugin1
{
    public static class ImportExport066
    {
        public static string SaveFile(List<TStation> stations, ref int groundlevel, ref int crownOfTheRoad)
        {
            bool gl = false;
            bool cotr = false;
            //gde se cuva jos nije odredjeno
            StringBuilder text = new StringBuilder();
            foreach (TStation station in stations)
            {
                if (station.NumberOfGLPoints <= 0)
                    break;
                //ako se barem jednom udje u petlju, znaci postoji gl i treba ga izabrati u profiler
                gl = true;
                int n = station.NumberOfGLPoints / 4 + (station.NumberOfGLPoints % 4 == 0 ? 0 : 1);
                int stationsNum = station.NumberOfGLPoints;
                for (int i = 0; i < n; i++)
                {
                    text.Append("66     " + String.Format("{0,2}", station.GL)
                       + String.Format("{0,9}", (int)(station.Distance * 1000)) +
                       string.Format("{0,2}", i + 1) + " " +
                       string.Format("{0,7}", (int)(station.GetGLPoint(i * 4).X * 1000)) + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4).Y * 1000)));
                    if (i * 4 + 1 < stationsNum)
                        text.Append(" " + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 1).X * 1000)) + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 1).Y * 1000)));
                    if (i * 4 + 2 < stationsNum)
                        text.Append(" " + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 2).X * 1000)) + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 2).Y * 1000)));
                    if (i * 4 + 3 < stationsNum)
                        text.Append(" " + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 3).X * 1000)) + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 3).Y * 1000)));
                    text.Append("\r\n");
                }
                if (station.NumberOfCOTRPoints > 0)
                {
                    cotr = true;
                    text.Append("66     " + String.Format("{0,2}", station.COTR)
                      + String.Format("{0,9}", (int)(station.Distance * 1000)) + " 1 " +
                      string.Format("{0,7}", (int)(station.GetCOTRPoint(0).X * 1000)) + string.Format("{0,7}", (int)(station.GetCOTRPoint(0).Y * 1000)));
                    text.Append(" " + string.Format("{0,7}", (int)(station.GetCOTRPoint(1).X * 1000)) + string.Format("{0,7}", (int)(station.GetCOTRPoint(1).Y * 1000)));
                    text.Append(" " + string.Format("{0,7}", (int)(station.GetCOTRPoint(2).X * 1000)) + string.Format("{0,7}", (int)(station.GetCOTRPoint(2).Y * 1000)));
                }

                text.Append("\r\n");
            }
            if (!cotr)
                crownOfTheRoad = -1;
            if (!gl)
                groundlevel = -1;
            return text.ToString();
        }
        public static List<TStation> LoadFile(string[] content)
        {
            //dohvatiti sve i smestiti u listu stanica
            List<TStation> stations = new List<TStation>(); 
            int levelline;
            double station;
            int lineIndex;//broj linije - ako polilinija ima vise od 4 tacke, onda je vrednost >1
            double[] x = new double[4];
            double[] y = new double[4];
            List<Point2d> gl = new List<Point2d>();
            List<Point2d> cotr = new List<Point2d>();
            List<Point2d> links = new List<Point2d>();
            List<Point2d> rechts = new List<Point2d>();
            int lastLevelline;
            double lastStation = -1;
            for (int i = 0; i < content.Length; i++)
            {
                string line = content[i];
                levelline = int.Parse(line.Substring(7, 2));
                station = int.Parse(line.Substring(9, 9)) / 1000.0;
                lineIndex = int.Parse(line.Substring(18, 2));
                x[0] = int.Parse(line.Substring(21, 7)) / 1000.0;
                y[0] = int.Parse(line.Substring(28, 7)) / 1000.0;
                int numberOfPoints = 1;
                if (line.Length >= 50)
                {
                    x[1] = int.Parse(line.Substring(36, 7)) / 1000.0;
                    y[1] = int.Parse(line.Substring(43, 7)) / 1000.0;
                    numberOfPoints++;
                    if (line.Length >= 65)
                    {
                        x[2] = int.Parse(line.Substring(51, 7)) / 1000.0;
                        y[2] = int.Parse(line.Substring(58, 7)) / 1000.0;
                        numberOfPoints++;
                        if (line.Length >= 80)
                        {
                            x[3] = int.Parse(line.Substring(66, 7)) / 1000.0;
                            y[3] = int.Parse(line.Substring(73, 7)) / 1000.0;
                            numberOfPoints++;
                        }
                    }
                }
                if (i > 0)
                {
                    if (lastStation != station)
                    {
                        TStation st = new TStation(lastStation, CustomSettings.GroundLevelIndex, CustomSettings.CrownOfTheRoadIndex, gl, cotr, CustomSettings.LinksIndex,
                            CustomSettings.RechtIndex, links, rechts);
                        stations.Add(st);
                        gl = new List<Point2d>();
                        cotr = new List<Point2d>();
                        links = new List<Point2d>();
                        rechts = new List<Point2d>();
                    }
                }
                if (levelline == CustomSettings.GroundLevelIndex)
                    for (int j = 0; j < numberOfPoints; j++)
                        gl.Add(new Point2d(x[j], y[j]));
                else if (levelline == CustomSettings.CrownOfTheRoadIndex)
                    for (int j = 0; j < numberOfPoints; j++)
                        cotr.Add(new Point2d(x[j], y[j]));
                else if (levelline == CustomSettings.LinksIndex)
                    for (int j = 0; j < numberOfPoints; j++)
                        links.Add(new Point2d(x[j], y[j]));
                else if (levelline == CustomSettings.RechtIndex)
                    for (int j = 0; j < numberOfPoints; j++)
                        rechts.Add(new Point2d(x[j], y[j]));
                lastStation = station;
                if (i == content.Length - 1)
                {//ako smo u poslednjoj liniji onda treba da dodamo stanicu
                    TStation st = new TStation(lastStation, CustomSettings.GroundLevelIndex, CustomSettings.CrownOfTheRoadIndex, gl, cotr, CustomSettings.LinksIndex,
                            CustomSettings.RechtIndex, links, rechts);
                    stations.Add(st);
                }
            }

            return stations;
        }
    }
}
