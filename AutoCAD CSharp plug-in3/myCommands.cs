﻿// (C) Copyright 2013 by Microsoft 
//
using System;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using testplugin1;

using Autodesk;
using Autodesk.AutoCAD;
using Application = Autodesk.AutoCAD.ApplicationServices.Application; 
//using Window = Autodesk.AutoCAD.Windows.Window;
using System.Collections.Generic;

// This line is not mandatory, but improves loading performances
[assembly: CommandClass(typeof(AutoCAD_CSharp_plug_in3.MyCommands))]

namespace AutoCAD_CSharp_plug_in3
{

    // This class is instantiated by AutoCAD for each document when
    // a command is called by the user the first time in the context
    // of a given document. In other words, non static data in this class
    // is implicitly per-document!
    public class MyCommands
    {
        // The CommandMethod attribute can be applied to any public  member 
        // function of any public class.
        // The function should take no arguments and return nothing.
        // If the method is an intance member then the enclosing class is 
        // intantiated for each document. If the member is a static member then
        // the enclosing class is NOT intantiated.
        //
        // NOTE: CommandMethod has overloads where you can provide helpid and
        // context menu.

        // Modal Command with localized name
        [CommandMethod("MyGroup", "MyCommand", "MyCommandLocal", CommandFlags.Modal)]
        public void MyCommand() // This method can have any name
        {
            // Put your command code here
        }

        // Modal Command with pickfirst selection
        [CommandMethod("MyGroup", "MyPickFirst", "MyPickFirstLocal", CommandFlags.Modal | CommandFlags.UsePickSet)]
        public void MyPickFirst() // This method can have any name
        {
            PromptSelectionResult result = Application.DocumentManager.MdiActiveDocument.Editor.GetSelection();
            if (result.Status == PromptStatus.OK)
            {
                // There are selected entities
                // Put your command using pickfirst set code here
            }
            else
            {
                // There are no selected entities
                // Put your command code here
            }
        }

        [CommandMethod("123PROF")]
        public void ProfilerCall()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            //TEST
            AxesGroup ag = new AxesGroup();
            using (Transaction tran = db.TransactionManager.StartTransaction())
            {
                DBDictionary nod = tran.GetObject(db.NamedObjectsDictionaryId, OpenMode.ForRead) as DBDictionary;
                DBDictionary qsects = tran.GetObject(nod.GetAt("C123_QSECT"), OpenMode.ForRead) as DBDictionary;
                //TEST
                BlockTable blockTable = tran.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tran.GetObject(blockTable[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

                foreach (DBDictionaryEntry entry in qsects)
                {
                    int res;
                    if (!int.TryParse(entry.Key, out res))
                        continue;
                    DBDictionary qsect = tran.GetObject(entry.Value, OpenMode.ForRead) as DBDictionary;
                    //u qsect se nalazi trenutni ulaz, treba da izvlacimo podatke
                    double distance;
                    string name;
                    int index = res;
                    double height;
                    List<Point3d> gl = null;
                    List<Point3d> cotr = null;
                    int axis;
                    Point3d cp;
                    //Line3d cl;
                    double dx;
                    TypedValue[] values;
                    Xrecord rec = tran.GetObject(qsect.GetAt("MORE"), OpenMode.ForRead) as Xrecord;
                    values = rec.Data.AsArray();
                    dx = (double)values[0].Value;
                    distance = (double)values[1].Value;
                    name = (string)values[2].Value;

                    rec = tran.GetObject(qsect.GetAt("SECT_DATA"), OpenMode.ForRead) as Xrecord;
                    values = rec.Data.AsArray();
                    axis = (short)values[0].Value;
                    height = (double)values[2].Value;

                    rec = tran.GetObject(qsect.GetAt("START_POS_MARKER"), OpenMode.ForRead) as Xrecord;
                    string handleName = (string)rec.Data.AsArray()[0].Value;
                    long handleValue = long.Parse(handleName, System.Globalization.NumberStyles.AllowHexSpecifier);
                    Circle c = tran.GetObject(db.GetObjectId(false, new Handle(handleValue), 0), OpenMode.ForRead) as Circle;
                    cp = new Point3d(c.Center.X, c.Center.Y, c.Center.Z);

                    if (qsect.Contains("GRAD_0_ROAD_POLY"))
                    {
                        rec = tran.GetObject(qsect.GetAt("GRAD_0_ROAD_POLY"), OpenMode.ForRead) as Xrecord;
                        //Autodesk.AutoCAD.GraphicsInterface.Polyline pl;
                        handleName = (string)rec.Data.AsArray()[0].Value;
                        handleValue = long.Parse(handleName, System.Globalization.NumberStyles.AllowHexSpecifier);
                        Polyline pl = tran.GetObject(db.GetObjectId(false, new Handle(handleValue), 0), OpenMode.ForRead) as Polyline;
                        cotr = new List<Point3d>();
                        for (int i = 0; i < pl.NumberOfVertices; i++)
                            cotr.Add(pl.GetPoint3dAt(i));

                    }
                    if (qsect.Contains("TERR_0_POLY"))
                    {
                        rec = tran.GetObject(qsect.GetAt("TERR_0_POLY"), OpenMode.ForRead) as Xrecord;
                        Polyline pl;
                        handleName = (string)rec.Data.AsArray()[0].Value;
                        handleValue = long.Parse(handleName, System.Globalization.NumberStyles.AllowHexSpecifier);
                        pl = tran.GetObject(db.GetObjectId(false, new Handle(handleValue), 0), OpenMode.ForRead) as Polyline;
                        gl = new List<Point3d>();
                        for (int i = 0; i < pl.NumberOfVertices; i++)
                            gl.Add(pl.GetPoint3dAt(i));
                    }
                    ProfileData pd = new ProfileData(distance, name, index, height, gl, cotr, axis, cp, dx);
                    ag.AddProfile(pd);

                    //dodavanje test linije od centra u desno
                    /*Point3d p1 = new Point3d(cp.X + Math.Abs(dx), cp.Y, 0);
                    Point3d p2 = new Point3d(cp.X + Math.Abs(dx) + 15, cp.Y + 15, 0);
                    Line l = new Line(p1, p2);
                    btr.AppendEntity(l);
                    tran.AddNewlyCreatedDBObject(l, true);*/
                }
                ag.FinishImport(true);
                tran.Commit();
            }
            int groundlevel = CustomSettings.GroundLevelIndex;
            int crownOfTheRoad = CustomSettings.CrownOfTheRoadIndex;
            string content = ImportExport066.SaveFile(ag.GetStationsForExport(), ref groundlevel, ref crownOfTheRoad);
            string nameFile = Application.DocumentManager.MdiActiveDocument.Name;
            nameFile = nameFile.Substring(nameFile.LastIndexOf("\\") + 1);

            string[] data = _123CAD.Program.StartProfiler(groundlevel, crownOfTheRoad, nameFile, content);
            List<TStation> stationsNew = ImportExport066.LoadFile(data);
            //sad ne moramo da kreiramo sve novo nego mozemo da dodajemo podatke (links i rechts) u postojeci axes group
            using (Transaction tran = db.TransactionManager.StartTransaction())
            {
                BlockTable bt = tran.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tran.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                foreach (TStation st in stationsNew)
                {
                    ProfileData pd = ag.GetProfile(st.Distance);
                    if (pd == null)//ne sme da bude null jer smo te stanice izbacili
                        throw new System.Exception("Problem with import 066 file from profiler!");
                    pd.AddLinksAndRechts(st.GetLinksPoints(), st.GetRechtPoints());
                    pd.GlobalizeLinksAndRechts();
                    Polyline plL = new Polyline();
                    if (pd.Links != null)
                    {
                        for (int i = 0; i < pd.Links.Count; i++)
                            plL.AddVertexAt(i, new Point2d(pd.Rechts[i].X, pd.Rechts[i].Y), 0, 0, 0);
                        //plL.SetPointAt(0, new Point2d(pd.Links[i].X, pd.Links[i].Y));
                        btr.AppendEntity(plL);
                        tran.AddNewlyCreatedDBObject(plL, true);
                    }
                    Polyline plR = new Polyline();
                    if (pd.Rechts != null)
                    {
                        for (int i = 0; i < pd.Rechts.Count; i++)
                            plR.AddVertexAt(i, new Point2d(pd.Rechts[i].X, pd.Rechts[i].Y), 0, 0, 0);
                        //plR.SetPointAt(i, new Point2d(pd.Rechts[i].X, pd.Rechts[i].Y));
                        btr.AppendEntity(plR);
                        tran.AddNewlyCreatedDBObject(plR, true);
                    }


                }
                tran.Commit();
            }
        }
        
    }

}
