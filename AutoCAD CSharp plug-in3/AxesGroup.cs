﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autodesk.AutoCAD.GraphicsInterface;
using Autodesk.AutoCAD.Geometry;

namespace testplugin1
{
    static class CustomSettings
    {
        /// <summary>
        /// koliko se dodaje na stanicu za novu axis (1000)
        /// </summary>
        public const int AxisBoundaryDefault = 1000;
        public const int GroundLevelIndex = 10;
        public const int CrownOfTheRoadIndex = 20;
        public const int LinksIndex = 30;
        public const int RechtIndex = 40;
    }
    /// <summary>
    /// Klasa u koju se ucitavaju podaci za slanje u profiler
    /// </summary>
    class AxesGroup
    {
        private Dictionary<int, SingleAxis> _axes;
        public int NumberOfAxes { get { return _axes == null ? -1 : _axes.Count; } }
        public AxesGroup()
        {
            _axes = new Dictionary<int, SingleAxis>();
        }
        public bool AddProfile(ProfileData data)
        {
            if (!_axes.ContainsKey(data.Axis))
                _axes.Add(data.Axis, new SingleAxis());
            _axes[data.Axis].AddEntry(data);
            return true;
        }
        public ProfileData GetProfile(double station)
        {
            ProfileData data;
            foreach (SingleAxis axis in _axes.Values)
            {
                if (axis.GetProfile(station, out data))
                    return data;
            }
            return null;
        }
        public ProfileData GetProfile(int index)
        {
            if (_axes.ContainsKey(index))
            {
                SingleAxis sa = _axes[index];
                return sa.GetProfile(index);
            }
            return null;
        }
        /// <summary>
        /// Metoda koja dodeljuje 1000 axisima 
        /// </summary>
        public void FinishImport(bool relativize)
        {
            int multiplier = 0;
            for (int i = 0; i < _axes.Count; i++)
                if (_axes.ContainsKey(i) && _axes[i].NumberOfProfiles > 0)
                {
                    _axes[i].AxisBoundary = 1000 * multiplier++;
                    if (relativize)
                        _axes[i].Relativize();
                }
        }
        public List<TStation> GetStationsForExport()
        {
            List<TStation> temp = new List<TStation>(), t;
            foreach (SingleAxis axis in _axes.Values)
            {
                t = axis.GetStations(axis.AxisBoundary);
                temp.AddRange(t);
            }
            temp.Sort(new Comparison<TStation>(SortStationsByDistanceGrow));
            return temp;
        }
        public int SortStationsByDistanceGrow(TStation s1, TStation s2)
        {
            double res = s1.Distance - s2.Distance;
            if (res < 0) return -1;
            if (res == 0) return 0;
            return 1;
        }
    }
    class SingleAxis
    {
        /// <summary>
        /// U hiljadama
        /// </summary>
        public double AxisBoundary;
        public double StartStation { get; private set; }
        public double EndStation{ get; private set; }
        private Dictionary<double, int> _distance_index;
        private Dictionary<int, double> _index_distance;
        //ne treba! ne mogu da sortiram kako treba, pa cu koristiti gorenavedene dictionaries, i sortirati po potrebi
        //private List<int> _indexes;//lista indeksa koji predstavljaju sortirane stanice po njihovoj udaljenosti
        private Dictionary<int, ProfileData> _index_profiles;//svaki od njih sadrzi informacije o profilu
        public int NumberOfProfiles { get { return _index_profiles.Count; } }
        public SingleAxis()
        {
            AxisBoundary = -1;
            StartStation = EndStation = -1;
            _distance_index = new Dictionary<double, int>();
            _index_distance = new Dictionary<int, double>();
            _index_profiles = new Dictionary<int, ProfileData>();
        }
        public bool AddEntry(ProfileData data)
        {
            if (_index_distance.ContainsKey(data.Index))
                return false;
            data.Distance = Math.Round(data.Distance, 3);
            _index_distance.Add(data.Index, data.Distance);
            _distance_index.Add(data.Distance, data.Index);
            _index_profiles.Add(data.Index, data);
            if (StartStation == -1)
                StartStation = EndStation = data.Distance;
            else if (StartStation > data.Distance)
                StartStation = data.Distance;
            else if (EndStation < data.Distance)
                EndStation = data.Distance;
            return true;
        }
        public ProfileData GetProfile(int index)
        {
            if (_index_profiles.ContainsKey(index))
                return _index_profiles[index];
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="station">stanica sa zaracunatim hiljadama za axis</param>
        /// <param name="data">Stanice ima u axis ili ne, gleda se posle povratnog parametra</param>
        /// <returns>true - stanica pripada ovoj axis, false stanica ne pripada</returns>
        public bool GetProfile(double station, out ProfileData data)//pocetak stanica za axis
        {
            if (AxisBoundary < 0)
                throw new Exception();
            double temp = station - AxisBoundary; 
            data = null;//stanica pripada ovoj axis ali ne postoji
            if (temp < 0 || temp >= CustomSettings.AxisBoundaryDefault)
                return false;//stanica ne pripada ovoj axis
            if (_distance_index.ContainsKey(temp))
                data = _index_profiles[_distance_index[temp]];
            return true;
        }
        //TODO getdata - dohvata listu podataka za eksport, u posebnom formatu
        public void Relativize()
        {
            foreach (ProfileData data in _index_profiles.Values)
                data.Relativize();
        }
        public List<TStation> GetStations(double axisDistance)
        {
            List<TStation> stations = new List<TStation>();
            foreach (ProfileData data in _index_profiles.Values)
            {
                TStation t = data.GetInformation();
                t.Distance += axisDistance;
                stations.Add(t);
            }
            return stations;
        }
    }
    class ProfileData
    {
        public double Distance;
        public string Name;
        public int Index;//jedinstveni indeks profila
        public double Height;
        public List<Point3d> GroundLevel;
        public List<Point3d> CrownOfTheRoad;
        public List<Point3d> Links;
        public List<Point3d> Rechts;
        public int Axis;
        public Point3d Center;
        private bool _isRelativized;
        //public Line3d CenterLine;//da li nam treba
        public double DX;//relativna leva koordinata, na krug se dodaje ova vrednost da bi se dobilo centralno x
        public ProfileData(double distance, string name, int index, double height, List<Point3d> gl, List<Point3d> cotr, int axis, Point3d center, double dx)
        {
            _isRelativized = false;
            Distance = distance;
            Index = index;
            Height = height;
            GroundLevel = gl;
            CrownOfTheRoad = cotr;
            Axis = axis;
            Center = center;
            Name = name;
            DX = dx;
        }
        /// <summary>
        /// Apsolutne koordinate prebacuje u relativne, zaokruzivanje
        /// </summary>
        public void Relativize()
        {//TODO mozda da pratim dal je uradjeno ovo ili nije 
            
            double x = Center.X + Math.Abs(DX);
            double y = Center.Y;
            double h = Height;
            List<Point3d> tempGL;
            List<Point3d> tempCOTR;
            if (GroundLevel != null)
            {
                tempGL = new List<Point3d>();
                foreach (Point3d p in GroundLevel)
                {
                    Point3d p1 = new Point3d(Math.Round(p.X - x, 3), Math.Round(p.Y - y + h,3), Math.Round(p.Z,3));
                    tempGL.Add(p1);
                }
                GroundLevel = tempGL;
            }
            if (CrownOfTheRoad != null)
            {
                tempCOTR = new List<Point3d>();
                int i = 0;
                foreach (Point3d p in CrownOfTheRoad)
                {
                    Point3d p1 = new Point3d(Math.Round(i == 1?0:p.X - x,3), Math.Round(p.Y - y + h,3), Math.Round(p.Z,3));
                    tempCOTR.Add(p1);
                    i++;
                }
                CrownOfTheRoad = tempCOTR;
            }
            Distance = Math.Round(Distance, 3);
            _isRelativized = true;
        }
        //TODO trebace nam metoda za eksport - ona prebacuje u odgovarajuci format
        //i postavlja odgovarajuci broj stanice (+ 1000 * i)
        public TStation GetInformation()
        {
            TStation station = new TStation();
            station.Distance = Distance;
            if (GroundLevel != null)
                foreach (Point3d p in GroundLevel)
                    station.GetGLPoints().Add(new Point2d(p.X, p.Y));
            if (CrownOfTheRoad != null)
                foreach (Point3d p in CrownOfTheRoad)
                    station.GetCOTRPoints().Add(new Point2d(p.X, p.Y));
            if (station.otherPoints != null)
                throw new Exception("Nije definisano stanje za ostale nivoe!");
            return station;
        }
        public void AddLinksAndRechts(List<Point2d> links, List<Point2d> rechts)
        {
            if (links != null && links.Count > 0)
            {
                Links = new List<Point3d>(links.Count);
                foreach (Point2d p in links)
                    Links.Add(new Point3d(p.X, p.Y, 0));
            }
            if (rechts != null && rechts.Count > 0)
            {
                Rechts = new List<Point3d>(rechts.Count);
                foreach (Point2d p in rechts)
                    Rechts.Add(new Point3d(p.X, p.Y, 0));
            }
        }
        public void GlobalizeLinksAndRechts()
        {
            double x = Center.X + Math.Abs(DX);
            double y = Center.Y;
            double h = Height;
            if (Links != null && Links.Count > 0)
                for (int i = 0; i < Links.Count; i++)// (Point3d p in Links)
                    Links[i] = Links[i].Add(new Vector3d(x, - Height + y, 0));
            if (Rechts != null && Rechts.Count > 0)
                for (int i = 0; i < Rechts.Count; i++)// (Point3d p in Rechts)
                    Rechts[i] = Rechts[i].Add(new Vector3d(x, -Height + y, 0));
        }
        
    }
    /// <summary>
    /// Klasa za cuvanje informacija o stanici
    /// </summary>
    public class TStation
    {
        public double Distance;
        public int GL;
        public int COTR;
        private List<Point2d> GLPoints;
        private List<Point2d> COTRPoints;
        public int NumberOfGLPoints
        {
            get
            {
                if (GLPoints != null)
                    return GLPoints.Count;
                return -1;
            }
        }
        public int NumberOfCOTRPoints
        {
            get
            {
                if (COTRPoints.Count != null)
                    return COTRPoints.Count;
                return -1;
            }
        }
        public int Links;
        public int Rechts;
        private List<Point2d> LinksPoints;
        private List<Point2d> RechtsPoints;
        public int NumberOfLinksPoints
        {
            get { if (LinksPoints == null) return -1; return LinksPoints.Count; }
        }
        public int NumberOfRechtsPoints
        {
            get { if (RechtsPoints == null) return -1; return RechtsPoints.Count; }
        }
        public List<Point2d> GetGLPoints()
        {
            return GLPoints;
        }
        public List<Point2d> GetCOTRPoints()
        {
            return COTRPoints;
        }
        public List<Point2d> GetLinksPoints()
        {
            return LinksPoints;
        }
        public List<Point2d> GetRechtPoints()
        {
            return RechtsPoints;
        }
        public Point2d GetLinksPoint(int index)
        {
            return LinksPoints[index];
        }
        public Point2d GetRechtPoint(int index)
        {
            return RechtsPoints[index];
        }
        public Point2d GetGLPoint(int index)
        {
            /*if (GLPoints == null)
                return null;
            if (GLPoints.Count > index)
                return null;*/
            return GLPoints[index];
        }
        public Point2d GetCOTRPoint(int index)
        {
            /*if (COTRPoints == null)
                return null;
            if (COTRPoints.Count > index)
                return null;*/
            return COTRPoints[index];
        }
        /// <summary>
        /// Ne eksportuju se ni nista ove druge linije!!! Da bi se ubacile u projekat
        /// mora da se radi relativizacija, vadjenje formata za export, export, itd
        /// </summary>
        public List<List<Point2d>> otherPoints;
        public TStation()
        {
            Distance = 0;
            GL = CustomSettings.GroundLevelIndex;
            COTR = CustomSettings.CrownOfTheRoadIndex;
            Links = CustomSettings.LinksIndex;
            Rechts = CustomSettings.RechtIndex;
            GLPoints = new List<Point2d>();
            COTRPoints = new List<Point2d>();
            LinksPoints = new List<Point2d>();
            RechtsPoints = new List<Point2d>();
            otherPoints = null;
        }
        public TStation(double distance, int gl, int cotr, List<Point2d> glPoints, List<Point2d> cotrPoints, int link, int recht, List<Point2d> links, List<Point2d> rechts)
        {
            Distance = distance;
            GL = gl;
            COTR = cotr;
            GLPoints = glPoints;
            COTRPoints = cotrPoints;
            Links = link;
            Rechts = recht;
            LinksPoints = links;
            RechtsPoints = rechts;
            otherPoints = null;
            
        }
    }
}
