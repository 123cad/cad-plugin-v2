Tests are executed in COMMON_CAD_directory (not in "123CAD Dev"). All libraries are copied there, so it can work.
All additional libraries (downloaded with Nuget, etc...) have to be manually copied (we don't want to put output dir to COMMON).
This is small drawback, but for now i will try to avoid copying multiple files on each test run.

Updates are needed in PostBuild event (output of files).
And manual load of CadPlugin in Loader.