﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using MyUtilities.Geometry;
using my = MyUtilities.Geometry;
using CadPlugin.PointReduction.CAD;
using CadPlugin.PointReduction.Views;
using System.Windows.Forms;
using System.IO;
using CadPlugin.PointReduction.PerformanceTesting;
using static CadPlugin.PointReduction.PerformanceTesting.TestRunner;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
//using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD 
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
//using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif 

namespace CadPlugin.PointReduction
{
    /// <summary>
    /// Loads file, reads xyz points and reduces them.
    /// Any character (other than digit) at beggining of the line skips that line.
    /// XYZ points are divided by space character (1 or more), but all points must ocupy same positions in every row.
    /// </summary>
    class PointReductionCommand
    {
        private static string LastDeltaZs = "30,50,70";
        private static int LastDeltaZ = 30;
        /// <summary>
        /// Calculates data for multiple settings (without drawing, just performance testing).
        /// </summary>
        /// <param name="settings"></param>
        internal static void RunPerformanceTests(Document doc)
        {
            PromptIntegerOptions opts = new PromptIntegerOptions("Test type: 1 - single file; 2 - multiple files");
            opts.DefaultValue = 1;
            opts.AllowArbitraryInput = false;
            opts.AllowNone = false;
            PromptIntegerResult res = doc.Editor.GetInteger(opts);
            if (res.Status != PromptStatus.OK)
                return;
            TestType test = res.Value == 1 ? TestType.SingleFile : TestType.MultipleFiles;
            List<double> heights = new List<double>();
            switch (test)
            {
                case TestType.SingleFile:
                    {
                        //while (true)
                        {
                            PromptStringOptions optss = new PromptStringOptions("Enter delta height in cm (comma delimited)");
                            optss.DefaultValue = LastDeltaZs;
                            var ress = doc.Editor.GetString(optss);
                            if (ress.Status != PromptStatus.OK)
                            {
                                doc.Editor.WriteMessage("Canceled");
                                return;
                            }
                            string[] vals = ress.StringResult.Split(',');
                            string last = "";
                            foreach(string s in vals)
                            {
                                if (string.IsNullOrEmpty(s))
                                    continue;
                                int t;
                                if (!int.TryParse(s, out t))
                                    continue;
                                if (last != "")
                                    last += ",";
                                last += t;
                                heights.Add(t / 100.0);
                            }
                            if (heights.Count == 0)
                                return;
                            LastDeltaZs = last;
                            //break;
                        }
                    }
                    break;
                case TestType.MultipleFiles:
                    {
                        opts = new PromptIntegerOptions("Type delta height (cm): ");
                        opts.DefaultValue = LastDeltaZ;
                        res = doc.Editor.GetInteger(opts);
                        if (res.Status != PromptStatus.OK)
                        {
                            doc.Editor.WriteMessage("Canceled");
                            return;
                        }
                        LastDeltaZ = res.Value;
                        heights.Add(LastDeltaZ / 100.0);
                    }
                    break;
            }
            ITestOutput output = new EditorOutput(doc);
            TestRunner.RunPerformanceTest(test, heights.ToArray());
            doc.Editor.WriteMessage("Test finished");
        }
        public static void Run(Document doc)
        {
            Editor ed = doc.Editor; 
            Database db = doc.Database;
            UserSettings user = UserSettingsHelper.GetUserSettings(ed);
            if (user == null)
                return;

            string fileName = "";
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.CheckFileExists = true;
            ofd.Filter = "XYZ files (*.xyz;*.dat;*.data)|*.xyz;*.dat;*.data|All Files (*.*)|*.*";
            if (ofd.ShowDialog() != DialogResult.OK)
                return;
            fileName = ofd.FileName;


            Statistics stats = new Statistics(user);
            stats.StartTimer();
            Points allPoints = null;
            using (StreamReader fileReader = new StreamReader(fileName))
                allPoints = PointsFileLoader.LoadPoints(fileReader);
            stats.PointsLoadedFromFileDuration = stats.StopTimer();
            if (allPoints.TotalPoints == 0)
                return;

            ReducedPoints reduced = new ReducedPoints();
            ReductionRunner.PerformCalculations(allPoints, user, stats, reduced);

            stats.TotalPointsCount = allPoints.TotalPoints; 
            int totalPoints = allPoints.TotalPoints;
            int remainingPoints = stats.PointsRemainingAfterReduction; 
            int removedPoints = totalPoints - remainingPoints;
            double percentOfRemainingPoints = 100.0 * remainingPoints / totalPoints;

            IDrawPointsView view = new DrawPointsView(stats);
            ((System.Windows.Forms.Form)view).ShowDialog();
            var res = view.Result;
            if (res == null)
                return;

            stats.StartTimer();
            stats.PointsRemainingAfterReduction = reduced.TotalPoints;
            if (res.DrawReducedPoints)
                PointCadDrawer.DrawReducedPoints(db, reduced);
            stats.DrawReducedPointsDuration = stats.StopTimer();

            if (res.DrawAllPoints)
            {
                stats.StartTimer();
                PointCadDrawer.DrawAllPoints(db, allPoints.AllPoints);
                stats.DrawAllPointsDuration = stats.StopTimer();
            }


            string msg = $"Total points(reduced/skipped): {totalPoints} ({remainingPoints} / {removedPoints})\n" +
                $"Remaining points: {DoubleHelper.ToStringInvariant(percentOfRemainingPoints, 2)} %\n";
            msg += $"Points loaded: {stats.PointsLoadedFromFileDuration}ms \n" +
                $"Points processed in: {stats.PointsProcessedDuration}ms \n" +
                $"Minum calculated distance: X={DoubleHelper.ToStringInvariant(allPoints.MinimumDistanceX, 2)} Y={DoubleHelper.ToStringInvariant(allPoints.MinimumDistanceY, 2)}\n" +
                $"Reduced points drawn in: {stats.DrawReducedPointsDuration}ms \n" +
                $"All points drawn in: {stats.DrawAllPointsDuration}ms";

            doc.SendStringToExecute("ZOOM ", true, false, false);
            doc.SendStringToExecute("_e ", true, false, false);

            System.Windows.Forms.MessageBox.Show(msg, "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            ed.WriteMessage(msg);
        }


        

    }
}
