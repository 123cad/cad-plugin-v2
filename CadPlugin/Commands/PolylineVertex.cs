﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Commands
{
	/// <summary>
	/// Insert vertex to existing polyline (2d, 3d...)
	/// </summary>
	class PolylineVertex
	{
		public static void Insert(Document doc)
		{

			Database db = doc.Database;
			Editor ed = doc.Editor;

			PromptEntityOptions opts = new PromptEntityOptions("2D- oder 3D-Polylinie wählen");// "Select polyline:");
			opts.SetRejectMessage("Keine 2D/3D Polylinie gewählt!");// "Not a 2D/3D polyline!");
			opts.AddAllowedClass(typeof(Polyline), true);
			opts.AddAllowedClass(typeof(Polyline2d), true);
			opts.AddAllowedClass(typeof(Polyline3d), true);

			PromptEntityResult res = ed.GetEntity(opts);
			if (res.Status == PromptStatus.OK)
			{
				PromptPointOptions optP = new PromptPointOptions("Punkt wählen:");// "Select point:");
				PromptPointResult optPR = ed.GetPoint(optP);
				if (optPR.Status == PromptStatus.OK)
				{
					Point3d newPoint = optPR.Value;
					using (Transaction tr = db.TransactionManager.StartTransaction())
					{
						BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
						BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

						/*Polyline2d p2d = new Polyline2d();
						btr.AppendEntity(p2d);
						tr.AddNewlyCreatedDBObject(p2d, true);

						Point3d v = optPR.Value;
						for (int i = 0; i < 4; i++)
						{
							p2d.AppendVertex(new Vertex2d(v, 0, 0, 0, 0));
							v = v.Add(new Vector3d(2, 2, 2));
						}
						tr.Commit();
						return;//*/

						DBObject obj = tr.GetObject(res.ObjectId, OpenMode.ForWrite);
						Polyline pl = obj as Polyline;
						Polyline2d pl2d = obj as Polyline2d;
						Polyline3d pl3d = obj as Polyline3d;
						Point3d? point = null;
						if (pl != null)
						{
							point = pl.GetClosestPointTo(optPR.Value, false);
							double index = pl.GetParameterAtPoint(point.Value);
							pl.AddVertexAt((int)index + 1, new Point2d(newPoint.X, newPoint.Y), 0, 0, 0);
						}
						else if (pl2d != null)
						{
							point = pl2d.GetClosestPointTo(optPR.Value, false);
							double index = pl2d.GetParameterAtPoint(point.Value);
							using (Vertex2d v2d = new Vertex2d())
							{
								v2d.Position = newPoint;
								int i = 0;
								foreach (ObjectId oid in pl2d)
								{
									if ((int)index == i)
									{
										Vertex2d t = tr.GetObject(oid, OpenMode.ForWrite) as Vertex2d;
										pl2d.InsertVertexAt(t, v2d);
										break;
									}
									else
										i++;
								}
							}
						}
						else if (pl3d != null)
						{
							point = pl3d.GetClosestPointTo(optPR.Value, false);
							double index = pl3d.GetParameterAtPoint(point.Value);



#if BRICSCAD
							using (PolylineVertex3d v3d = new PolylineVertex3d(newPoint))
							{

								int i = 0;
								foreach (ObjectId oid in pl3d)
								{
									if ((int)index == i)
									{
										PolylineVertex3d t = tr.GetObject(oid, OpenMode.ForRead) as PolylineVertex3d;
										pl3d.InsertVertexAt(t, v3d);
										break;
									}
									else
										i++;
								}
							}
#endif
#if AUTOCAD
							// Crashing at pl3d.InsertVertexAt(t, v3d); , some memory corruption.
							// null also can't be passed as argument (although documentation says it can).
							// This is workaround.

							var points = pl3d.GetPoints(tr).ToList();
							points.Insert((int)index+1, newPoint);

							pl3d.ReplacePoints(tr, points.ToArray());

							//int i = 0;
							//foreach (ObjectId oid in pl3d)
							//{
							//	if ((int)index == i)
							//	{
							//		using (PolylineVertex3d v3d = new PolylineVertex3d(newPoint))
							//		{
							//			PolylineVertex3d t = tr.GetObject(oid, OpenMode.ForWrite) as PolylineVertex3d;
							//
							//			pl3d.InsertVertexAt(null, v3d);
							//
							//			pl3d.AppendVertex(v3d);
							//			pl3d.InsertVertexAt(t, v3d);
							//			tr.AddNewlyCreatedDBObject(v3d, true);
							//			break;
							//		}
							//	}
							//	else
							//		i++;
							//}
#endif

						}
						tr.Commit();
					}
				}
			}
		}
		public static void Remove(Document doc)
		{
			Database db = doc.Database;
			Editor ed = doc.Editor;

			PromptEntityOptions opts = new PromptEntityOptions("2D- oder 3D-Polylinie wählen");// "Select polyline:");
			opts.SetRejectMessage("Keine 2D / 3D Polylinie gewählt!");// "Not a 2D / 3D polyline!");
			opts.AddAllowedClass(typeof(Polyline), true);
			opts.AddAllowedClass(typeof(Polyline2d), true);
			opts.AddAllowedClass(typeof(Polyline3d), true);

			PromptEntityResult res = ed.GetEntity(opts);
			if (res.Status == PromptStatus.OK)
			{
				PromptPointOptions optP = new PromptPointOptions("Punkt wählen:");// "Select point:");
				PromptPointResult optPR = ed.GetPoint(optP);
				if (optPR.Status == PromptStatus.OK)
				{
					Point3d newPoint = optPR.Value;
					using (Transaction tr = db.TransactionManager.StartTransaction())
					{
						BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
						BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

						DBObject obj = tr.GetObject(res.ObjectId, OpenMode.ForWrite);
						Polyline pl = obj as Polyline;
						Polyline2d pl2d = obj as Polyline2d;
						Polyline3d pl3d = obj as Polyline3d;
						Point3d? point = null;
						if (pl != null)
						{
							point = pl.GetClosestPointTo(optPR.Value, false);
							double index = pl.GetParameterAtPoint(point.Value);
							//pl.AddVertexAt((int)index + 1, new Point2d(newPoint.X, newPoint.Y), 0, 0, 0);
							pl.RemoveVertexAt((int)Math.Round(index));
						}
						else if (pl2d != null)
						{
							point = pl2d.GetClosestPointTo(optPR.Value, false);
							double index = pl2d.GetParameterAtPoint(point.Value);
							using (Vertex2d v2d = new Vertex2d())
							{
								v2d.Position = newPoint;
								int i = 0;
								foreach (ObjectId oid in pl2d)
								{
									if ((int)index == i)
									{
										Vertex2d t = tr.GetObject(oid, OpenMode.ForWrite) as Vertex2d;
										t.Erase();
										pl2d.RecordGraphicsModified(true);
										break;
									}
									else
										i++;
								}
							}
						}
						else if (pl3d != null)
						{
							point = pl3d.GetClosestPointTo(optPR.Value, false);
							double index = pl3d.GetParameterAtPoint(point.Value);
							int i = 0;
							foreach (ObjectId oid in pl3d)
							{
								if ((int)index == i)
								{
									PolylineVertex3d t = tr.GetObject(oid, OpenMode.ForWrite) as PolylineVertex3d;
									t.Erase();
									pl3d.RecordGraphicsModified(true);
									break;
								}
								else
									i++;
							}
						}
						tr.Commit();
					}
					ed.SetImpliedSelection(new ObjectId [0]{ });
				}
			}
		}
	}
}
