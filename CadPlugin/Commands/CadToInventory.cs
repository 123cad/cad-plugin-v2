﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Commands
{
    class CadToInventory
    {
        public static void Invoke(Document doc)
        {
            // Parent is set so toolbar appears on top of parent only (not on top of all desktop windows).
            CadPlugin.Inventories.CadToInventories.MainToolbars.MainToolbar toolbar = CadPlugin.Inventories.CadToInventories.MainToolbars.MainToolbar.GetForm();
            if (toolbar.FormAlreadyOpened)
                return;
#if BRICSCAD
            toolbar.Show(Application.MainWindow);
            //toolbar.ShowDialog( Application.MainWindow);
            //toolbar.ShowDialog(Application.MainWindow);
            //Application.ShowModelessDialog(Application.MainWindow, toolbar, false);
            //Application.ShowModalDialog(toolbar);


            //Application.ShowModelessDialog(Application.MainWindow, toolbar);
#endif
#if AUTOCAD
            
            Application.ShowModelessDialog(Application.MainWindow.Handle, toolbar);
#endif
            // Attached only once.
            toolbar.DataReady += (d) =>
            {
                toolbar.FormClosed += (_, __) =>
                {
                    Commands.InventoryCommands.StartCommand(doc, d);
                };
            };
        }
    }
}
