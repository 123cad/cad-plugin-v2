﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsForms = System.Windows.Forms;
using System.IO;
using CadPlugin.Profilers;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Commands
{
    class AxOrtho66
    {
        public static void LoadStation3DPolylines(Document document)
        {
            //Selektuj poliliniju

            // get the editor object so we can carry out some input 
            Editor ed = document.Editor;
            Database db = document.Database;

            Polyline polyline;
            //Polyline2d polyline2D;

            PromptEntityOptions entityOptions = new PromptEntityOptions(/*"Select Polyline: "*/"Achslinie wählen:");
            PromptEntityResult getWhichEntityResult = ed.GetEntity(entityOptions);
            if ((getWhichEntityResult.Status != PromptStatus.OK))
                return;
            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                DBObject oid = acTrans.GetObject(getWhichEntityResult.ObjectId, OpenMode.ForRead);
                polyline = oid as Polyline;
                if (polyline == null)
                {
                    ed.WriteMessage("Objekt ist keine Polylinie!");
                    return;
                    //if ((oid as Polyline2d) == null)
                      //  ed.WriteMessage("Objekt ist keine 2D-Polylinie!\n\r");
                }
            }

            //Biranje 066 fajla
            WindowsForms.FileDialog fileDialog = new WindowsForms.OpenFileDialog();
            fileDialog.CheckFileExists = true;
            fileDialog.Filter = @"066 Datei (.066,.66)|*.066;*.66";

            if (fileDialog.ShowDialog() != WindowsForms.DialogResult.OK)
            {
                ed.WriteMessage("Datei nicht gewählt!");
                return;
            }
            //Ucitavanje 066
            List<TStation> stations = ImportExport066.LoadFileSimple(File.ReadAllLines(fileDialog.FileName));
            if (stations.Count == 0)
            {
                ed.WriteMessage("066 Datei Fehler");
                return;
            }
            //Biranje levelline za ucitavanje
            Dictionary<int, bool> levellines = new System.Collections.Generic.Dictionary<int, bool>();
            foreach (TStation station in stations)
            {
                foreach (var line in station.GetOtherPoints())
                {
                    int t = (int)line[0].X;
                    if (!levellines.ContainsKey(t))
                        levellines.Add(t, true);
                }
            }
            int SelectedLevelline = 0;
            List<int> temp = new List<int>();
            foreach (var key in levellines.Keys)
                temp.Add(key);
            if (temp.Count == 0)
            {
                ed.WriteMessage("Keine Horizontal in Datei, Abbruch");// No levellines defined in file, aborting!");
                return;
            }
            List<int> SelectedLevellines = LevellineChoser.ChoseLevelline(temp);
            if (SelectedLevellines == null)
            {
                ed.WriteMessage("Abbruch");//User canceled");
                return;
            }
            //ucitavanje 3d polilinija
            stations.Sort(StationsComparer);
            foreach (int _levelline in SelectedLevellines)
            {
                SelectedLevelline = _levelline;
                using (Transaction tran = db.TransactionManager.StartTransaction())
                {
                    BlockTable bt = tran.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                    BlockTableRecord btr = tran.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                    DBObject oid = tran.GetObject(getWhichEntityResult.ObjectId, OpenMode.ForRead);
                    polyline = oid as Polyline;
                    for (int i = 0; i < stations.Count; i++)
                    {
                        double distance = stations[i].Distance;
                        Point3d p;
                        try
                        {
                            p = polyline.GetPointAtDist(distance);
                        }
                        catch (System.Exception)
                        {
                            //System.Windows.MessageBox.Show("Not all stations are loaded, polyline is shorter!");
                            continue;
                        }
                        //sad trazimo levelline
                        List<MyUtilities.Geometry.Point2d> levelline = null;
                        foreach (var v in stations[i].GetOtherPoints())
                        {
                            if ((int)v[0].X == SelectedLevelline)
                            {
                                levelline = v;
                                break;
                            }
                        }
                        if (levelline == null)
                            continue;
                        //
                        double segment = polyline.GetParameterAtDistance(distance);
                        Vector3d directionVector = new Vector3d();
                        if (polyline.GetSegmentType((int)segment) == SegmentType.Arc)
                        {
                            double parameter = segment;
                            double currentPar = parameter - ((int)parameter);
                            double newDistance = 0;
                            if (currentPar > 0.5)
                                newDistance = distance - 0.001;
                            else
                                newDistance = distance + 0.001;
                            Point3d newP = polyline.GetPointAtDist(newDistance);
                            if (currentPar > 0.5)
                            {
                                var tempP3d = newP;
                                newP = p;
                                p = tempP3d;
                            }
                            directionVector = new Vector3d(newP.X - p.X, newP.Y - p.Y, 0);
                        }
                        else
                        {
                            LineSegment2d line = polyline.GetLineSegment2dAt((int)segment);
                            directionVector = new Vector3d(line.EndPoint.X - line.StartPoint.X, line.EndPoint.Y - line.StartPoint.Y, 0);
                        }


                        double x = directionVector.X;
                        double y = directionVector.Y;
                        Vector3d leftVector = new Vector3d(-y, x, 0);
                        Vector3d rightVector = new Vector3d(y, -x, 0);
                        Polyline3d polyline3d = new Polyline3d();
                        polyline3d.SetDatabaseDefaults();
                        btr.AppendEntity(polyline3d);
                        tran.AddNewlyCreatedDBObject(polyline3d, true);
                        //ovde treba da idemo po iteracijama, prvo 2d pravimo pa onda dodajemo visinu
                        for (int j = 1; j < levelline.Count; j++)
                        {
                            double lenght = levelline[j].X;
                            Vector3d currentVector = rightVector;
                            if (lenght < 0)
                            {
                                currentVector = leftVector;
                                lenght *= -1;
                            }
                            double multiplier = 1 / currentVector.Length;
                            currentVector = currentVector.MultiplyBy(multiplier * lenght);
                            currentVector = currentVector.Add(p.GetAsVector());

                            PolylineVertex3d v3d = new PolylineVertex3d(new Point3d(currentVector.X, currentVector.Y, levelline[j].Y));
                            //btr.AppendEntity(v3d);
                            //tran.AddNewlyCreatedDBObject(v3d, true);
                            polyline3d.AppendVertex(v3d);
                        }
                    }
                    tran.Commit();
                }
            }
        }
        private static int StationsComparer(TStation t1, TStation t2)
        {
            return (int)(t1.Distance * 1000) - (int)(t2.Distance * 1000);
        }
    }
}
