﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CadPlugin.Inventories.InventoryManager;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Commands
{
	public class CheckEntityCreator
	{
		public static void Start()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			Database db = doc.Database;
			Editor ed = doc.Editor;
			PromptEntityOptions opt = new PromptEntityOptions("Select entity:");
			PromptEntityResult res = ed.GetEntity(opt);
			if (res.Status != PromptStatus.OK)
			{
				ed.WriteMessage(" Canceled\n");
				return;
			}
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				Entity e = tr.GetObjectRead<Entity>(res.ObjectId);
				long handle = e.ObjectId.Handle.Value;
				if (CreatedCADEntities.EntitySources.ContainsKey(handle))
				{
					ed.WriteMessage("Selected entity created by: " + CreatedCADEntities.EntitySources[handle]);
				}
			}
		}
	}
}
