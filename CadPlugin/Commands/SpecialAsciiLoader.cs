﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using CadPlugin.Common;
using System.Diagnostics;
using System.Windows.Forms;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Commands
{
    /// <summary>
    /// Different format (not like ascii .xyz format).
    /// </summary>
	class SpecialAsciiLoader
	{
		private class SurroundingIndexes
		{
			public int CurrentX { get; set; }
			public int CurrentY { get; set; }
			public int Top { get; set; }
			public int Bottom { get; set; }
			public int Left { get; set; }
			public int Right { get; set; }
		}

		/// <summary>
		/// Works with special ascii file.
		/// First 6 rows are general info (data is parsed starting from character 14).
		/// Row0: number of columns (not mandatory, just helps us with performance)
		/// Row1: number of rows (not mandatory, just helps us with performance)
		/// Row2: bottom X coordinate.
		/// Row3: bottom Y coordinate.
		/// Row4: step performed by y or x coordinate.
		/// Row5: - not used
		/// Row6 (and next): table of height values for points.
		/// </summary>
		/// <param name="doc"></param>
		public static void Start(Document doc)
		{
			try
			{
				Begin(doc);
			}
			catch (System.Exception e)
			{
				MessageBox.Show("Unable to load lidar file. Check log for details");
				MyLog.MyTraceSource.Error("Unable to process lidar file.\r\nError: " + e);
			}
		}
		private static void Begin(Document doc)
		{
			System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
			dlg.CheckFileExists = true;
			dlg.Filter = "Any file (*.*)|*.*";
			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
			sw.Start();
			if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				string fileName = dlg.FileName;
				string[] content = System.IO.File.ReadAllLines(fileName);

				System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

				int predictedColumns = int.Parse(content[0].Substring(5).Trim());
				int predictedRows = int.Parse(content[1].Substring(5).Trim());
				List<List<double>> grid = new List<List<double>>(predictedRows);
				// XY are considered bottom left - 0,0 .
				double startX = double.Parse(content[2].Substring(9).Trim());
				double startY = double.Parse(content[3].Substring(9).Trim());
				double step = double.Parse(content[4].Substring(8));
				double no_data = double.Parse(content[5].Substring(12));
				int totalColumns = 0;
				int rowIndex = 0;
				BlocksAttributes.CadProgressBar.StartLongOperation(predictedRows);
				List<double> rowHeights = new List<double>();
				// Parse heights.
				for (int k = content.Length - 1; k > 5; k--)
				{
					string[] values = content[k].Trim().Split(' ');
					double currentX = rowIndex * step + startX;
					double currentY = startY;
					if (totalColumns < values.Length)
						totalColumns = values.Length;
					foreach (string s in values)
					{
						double z = double.Parse(s);
						rowHeights.Add(z);
						//points.Add(new Point3d(currentX, currentY, z));
						currentY += step;
					}
					rowIndex++;
					grid.Add(rowHeights);
					rowHeights = new List<double>();
					BlocksAttributes.CadProgressBar.PerformStep();

				}
				System.Diagnostics.Debug.WriteLine($"Loading file and parsing: {sw.ElapsedMilliseconds} ms");
				sw.Restart();
				int totalRows = rowIndex;
				bool[,] mask = new bool[totalColumns, totalRows];
				double[,] heights = new double[totalColumns, totalRows];

				// Extract heights to a 2d array, with masks.
				// 2d array is used for performance reasons.
				// Mask tells if point is to be drawn or not (if it has been reducted).
				int i = 0;
				int j = 0;
				long totalPointsCount = 0;
				long totalCreatedPoints = 0;
				foreach (var row in grid)
				{
					foreach (var column in row)
					{
						mask[j, i] = !DoubleHelper.AreEqual(column, no_data, 0.00001);
						heights[j, i] = column;
						j++;
					}
					j = 0;
					i++;
				}
				System.Diagnostics.Debug.WriteLine($"Preparing mask: {sw.ElapsedMilliseconds} ms");
				sw.Restart();

				PromptDoubleOptions opt = new PromptDoubleOptions("Set reduction: ");
				opt.DefaultValue = 0.1;
				PromptDoubleResult res = doc.Editor.GetDouble(opt);
				if (res.Status != PromptStatus.OK)
					return;
				// Value by which heights are considered identical.
				double reduction = res.Value;

				/*opt = new PromptDoubleOptions("Set spike limit: ");
				opt.DefaultValue = 5;
				res = doc.Editor.GetDouble(opt);
				if (res.Status != PromptStatus.OK)
					return;*/
				// Spike definition values.
				double flatRate = res.Value;

				if (false && flatRate >= 0)
					RemoveSpikePoints(flatRate, mask, heights);
				if (reduction >= 0)
					ReducePoints(reduction, mask, heights);

				System.Diagnostics.Debug.WriteLine($"Reducing points: {sw.ElapsedMilliseconds} ms");
				sw.Restart();
				BlocksAttributes.CadProgressBar.EndLongOperation();
				BlocksAttributes.CadProgressBar.StartLongOperation(totalRows);

				string reductedPointsLayer = "reducted_" + reduction.ToString("0.000");
				string reductedPointsLayerSkipped = reductedPointsLayer + "_skipped_points";
				CADLayerHelper.AddLayerIfDoesntExist(doc.Database, reductedPointsLayer);
				CADLayerHelper.AddLayerIfDoesntExist(doc.Database, reductedPointsLayerSkipped);
				CADLayerHelper.SetLayersFrozenState(doc, true, reductedPointsLayerSkipped);
				using (Transaction tr = doc.Database.TransactionManager.StartTransaction())
				{
					TransactionData data = new TransactionData(doc, tr);
					double currentY = 0;
					for (i = 0; i < totalRows; i++)
					{
						double currentX = 0;
						for (j = 0; j < totalColumns; j++)
						{
							DBPoint p = tr.CreateEntity<DBPoint>(data.Btr);
							p.Position = new Point3d(startX + currentX, startY + currentY , heights[j, i]);
							// when drawn and compared to google maps, it seems x and y are inverted.
							//p.Position = new Point3d(currentY, currentX, heights[i, j]);
							if (mask[j, i])
							{
								p.Layer = reductedPointsLayer;
								totalCreatedPoints++;
							}
							else
							{
								p.Layer = reductedPointsLayerSkipped;
							}
							totalPointsCount++;
							currentX += step;
						}
						currentY += step;
						BlocksAttributes.CadProgressBar.PerformStep();

					}
					tr.Commit();
				}
				BlocksAttributes.CadProgressBar.EndLongOperation();
				System.Diagnostics.Debug.WriteLine($"Creating DBPoints: {sw.ElapsedMilliseconds} ms");
				sw.Stop();


				doc.Editor.WriteMessage($"Total points created: {totalCreatedPoints} \n");
				doc.Editor.WriteMessage($"Total points skipped: {totalPointsCount - totalCreatedPoints} \n");
				doc.Editor.WriteMessage($"Total points found: {totalPointsCount} \n");

				doc.SendStringToExecute(".ZOOM ", false, false, false);
				doc.SendStringToExecute("_e ", false, false, false);
			}
		}

		/// <summary>
		/// Points which have smaller height difference than all surrounding points, are
		/// </summary>
		private static void RemoveSpikePoints(double flatRate, bool[,] mask, double[,] heights)
		{
			int totalRows = mask.GetLength(0);
			int totalColumns = mask.GetLength(1);
			int spikePointsCount = 0;
			for (int i = 1; i < totalRows - 1; i++)
			{
				for (int j = 1; j < totalColumns - 1; j++)
				{
					var t = GetSurroundingIndexes(i, j, mask, heights);

					double z = heights[i, j];
					double x1 = heights[i, t.Left] - z;
					double x2 = heights[i, t.Right] - z;
					double y1 = heights[t.Top, j] - z;
					double y2 = heights[t.Bottom, j] - z;
					if (Math.Abs(x1) < flatRate
						&& Math.Abs(x2) < flatRate
						&& Math.Abs(y1) < flatRate
						&& Math.Abs(y2) < flatRate
						)
					{
						heights[i, j] = (heights[i, t.Left] + heights[i, t.Right]) / 2;
						spikePointsCount++;
					}
				}
			}
			Debug.WriteLine($"Total reduced points: {spikePointsCount}");
		}
		/// <summary>
		/// Gets first valid height indexes around current height. 
		/// In the mask some heights are removed, that is why not neighbour heights are not neccessary used.
		/// </summary>
		/// <param name="currentX"></param>
		/// <param name="currentY"></param>
		/// <param name="mask"></param>
		/// <param name="heights"></param>
		/// <returns></returns>
		private static SurroundingIndexes GetSurroundingIndexes(int currentX, int currentY, bool[,] mask, double[,] heights)
		{
			SurroundingIndexes d = new SurroundingIndexes()
			{
				CurrentX = currentX,
				CurrentY = currentY
			};
			int totalRows = mask.GetLength(0);
			int totalColumns = mask.GetLength(1);
			// Get first left height.
			int offsetIndex = currentY - 1;
			while (offsetIndex > 0 && !mask[currentX, offsetIndex])
				offsetIndex--;
			d.Left = offsetIndex;
			// Get first right height.
			offsetIndex = currentY + 1;
			while (offsetIndex < totalColumns - 1 && !mask[currentX, offsetIndex])
				offsetIndex++;
			d.Right = offsetIndex;
			// Get first top height.
			offsetIndex = currentX - 1;
			while (offsetIndex > 0 && !mask[offsetIndex, currentY])
				offsetIndex--;
			d.Top = offsetIndex;
			// Get first bottom height.
			offsetIndex = currentX + 1;
			while (offsetIndex < totalRows - 1 && !mask[offsetIndex, currentY])
				offsetIndex++;
			d.Bottom = offsetIndex;
			return d;
		}
		/// <summary>
		/// Removes points which have smaller height difference against surrounding points (top, bottom, left, righ)
		/// </summary>
		/// <param name="reduction"></param>
		/// <param name="mask"></param>
		/// <param name="heights"></param>
		private static void ReducePoints(double reduction, bool[,] mask, double[,] heights)
		{
			int totalRows = mask.GetLength(0);
			int totalColumns = mask.GetLength(1);

			for (int i = 1; i < totalRows - 1; i++)
			{
				for (int j = 1; j < totalColumns - 1; j++)
				{
					var t = GetSurroundingIndexes(i, j, mask, heights);

					double z = heights[i, j];
					double x1 = heights[i, t.Left] - z;
					double x2 = heights[i, t.Right] - z;
					double y1 = heights[t.Top, j] - z;
					double y2 = heights[t.Bottom, j] - z;
					if (Math.Abs(x1) < reduction
						&& Math.Abs(x2) < reduction
						&& Math.Abs(y1) < reduction
						&& Math.Abs(y2) < reduction)
						mask[i, j] = false;
				}
			}
		}
	}
}
