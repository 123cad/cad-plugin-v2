﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using forms = System.Windows.Forms;
using System.Runtime.InteropServices;
using CadPlugin.LinetypeToolbars;
using CadPlugin.HatchToolbars.Data;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

[assembly: CommandClass(typeof(CadPlugin.Commands.LinetypeToolbar))]

namespace CadPlugin.Commands
{
	public class LinetypeToolbar
	{		
		private static LinetypeToolbars.Toolbar linetypeToolbar;

		private static bool commandInProgress = false;

		/// <summary>
		/// Called when application is loded to CAD.
		/// </summary>
		public static void ApplicationInitialized()
		{
			// If toolbar was opened last time, it reopens here.
			if (Properties.LinetypeToolbar.Default.ToolbarOpened)
			{
				// Open toolbar with saved width and height settings
				//ToolbarShow();

				MyCommands.LinetypeToolbarShow();
			}
		}
		public static void ToolbarShow()
		{
			if (linetypeToolbar == null)
			{
				Action<string> invokeCommand = (s) => Application.DocumentManager.MdiActiveDocument.SendStringToExecute(s + '\n', true, false, false);
				linetypeToolbar = new LinetypeToolbars.Toolbar();
				linetypeToolbar.LinetypeSelected += LinetypeToolbar_CreateAttribute;
				linetypeToolbar.CreateTable += LinetypeToolbar_CreateTable;
				linetypeToolbar.DeleteTable += LinetypeToolbar_DeleteTable;
				linetypeToolbar.AppendAreaToTable += LinetypeToolbar_AppendAreaToTable;
				linetypeToolbar.RemoveAreaFromtTable += LinetypeToolbar_RemoveAreaFromtTable;
				linetypeToolbar.AreaToSelection += LinetypeToolbar_AreaToSelection;
				linetypeToolbar.AllAreasToSelection += LinetypeToolbar_AllAreasToSelection;
				linetypeToolbar.AreaTablesToSelection += LinetypeToolbar_AreaTablesToSelection;
				linetypeToolbar.SaveTableToCSV += LinetypeToolbar_SaveTableToCSV;
				linetypeToolbar.TableSettings += HatchToolbar_TableSettings;
				linetypeToolbar.TopMost = false;

				linetypeToolbar.FormClosed += (_, __) =>
				{
					linetypeToolbar = null;
				};
				linetypeToolbar.MouseEnter += (_, __) =>
				{
					// No need that toolbar gets focus.
					//hatchToolbar.Activate();
				};
				linetypeToolbar.MouseLeave += (_, __) =>
				{
					// Toolbar get mouse input focus without being active window of the desktop.
					// CAD application can always have focus, and toolbar will be visible and clickable.
					// Any time mouse leaves the form, set focus to CAD.
					if (Application.DocumentManager.MdiActiveDocument != null)
						Application.DocumentManager.MdiActiveDocument.SetFocus();
					
				};

				Application.BeginQuit += (_, __) =>
				{
					if (linetypeToolbar != null)
						linetypeToolbar.Close();
					linetypeToolbar = null;
				};
#if BRICSCAD
				// This one makes problem, but not sure which one exactly.
				//Application.ShowModelessDialog((forms.IWin32Window)Application.MainWindow, hatchToolbar);
				linetypeToolbar.Show((forms.IWin32Window)Application.MainWindow);
#endif
#if AUTOCAD
				Application.ShowModelessDialog(Application.MainWindow.Handle, linetypeToolbar);
#endif

				LinetypeCommands.CommandFinished += HatchCommands_CommandFinished;
			}
			else if (!linetypeToolbar.Visible)
				ShowToolbar(Application.DocumentManager.MdiActiveDocument);
		}


		private static void HatchCommands_CommandFinished(Document obj)
		{
			if (commandInProgress)
			{
				commandInProgress = false;
				ShowToolbar(obj);
			}
		}

		public static void ExportExtensionDic(Document doc)
		{
			string fileName = "extensionDic_linetype.xml";
			if (ExportExtensionDictionary.ExportToXML(doc, fileName))
				System.Diagnostics.Process.Start(fileName);
		}
		public static void LinetypeToolbar_AreaTablesToSelection()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			commandInProgress = true;
			try
			{
				LinetypeCommands.AreaTablesToSelection(doc);
				//doc.SendStringToExecute("123HatchSelectAreaTables\n", true, false, false);
			}
			finally
			{
				//ShowToolbar(doc);
			}
		}
		public static void LinetypeToolbar_AllAreasToSelection()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			commandInProgress = true;
			try
			{
				LinetypeCommands.AllAreasToSelection(doc);
			//doc.SendStringToExecute("123HatchSelectTableAllAreas\n", true, false, false);
			}
			finally
			{
				ShowToolbar(doc);
			}
		}

		public static void LinetypeToolbar_AreaToSelection()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			commandInProgress = true;
			try
			{
				LinetypeCommands.AreaToSelection(doc);
				//doc.SendStringToExecute("123HatchSelectTableArea\n", true, false, false);
			}
			finally
			{
				ShowToolbar(doc);
			}
		}

		private static void LinetypeToolbar_SaveTableToCSV()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			commandInProgress = true;
			try
			{
				LinetypeCommands.ExportTable(doc);
				//doc.SendStringToExecute("123HatchSelectTableArea\n", true, false, false);
			}
			finally
			{
				ShowToolbar(doc);
			}
		}
		private static void HatchToolbar_TableSettings()
		{
			try
			{
				LinetypeToolbars.ToolbarSettings.TableSettingsView.ShowTable();
			}
			finally
			{
			}
		}
		public static void LinetypeToolbar_RemoveAreaFromtTable()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			try
			{
				LinetypeCommands.RemoveAreaFromTable(doc);
			}
			finally
			{
				ShowToolbar(doc);
			}
		}

		public static void LinetypeToolbar_AppendAreaToTable()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			try
			{
				LinetypeCommands.AddAreaToTable(doc);
			}
			finally
			{
				ShowToolbar(doc);
			}
		}

		public static void LinetypeToolbar_DeleteTable()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			try
			{
				LinetypeCommands.DeleteTable(doc);
			}
			finally
			{
				ShowToolbar(doc);
			}
		}

		public static void LinetypeToolbar_CreateTable()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			try
			{
				LinetypeCommands.CreateLinetypeTable(doc);
			}
			finally
			{
				ShowToolbar(doc);
			}
		}

		private static void LinetypeToolbar_CreateAttribute(LinetypeToolbars.SingleLinetypeData data)
		{
			bool valid = true;
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			try
			{
				valid = CADLineTypeHelper.CheckLinetype(doc.Database, data.LinetypeType.PatternName);

				if (valid)
					LinetypeCommands.LinetypeDataSelected(doc, data);
			}
			finally
			{
				ShowToolbar(doc);
				if (!valid)
					System.Windows.MessageBox.Show("Der Linientyp" + //"Linetype" + 
													": <" + data.LinetypeType.PatternName + "> " +
													"als" + //"for" + 
													"\"" + data.Name + "\"" + 
													"ist nicht geladen!"//"is not loaded!"
													);
			}
		}
		private static void HideToolbar(Document doc)
		{
			linetypeToolbar.Enabled = false;
			linetypeToolbar.Visible = false;

			//doc.SetFocus();
		}
		private static void ShowToolbar(Document doc, IEnumerable<ObjectId> objs  = null)
		{
			linetypeToolbar.Enabled = true;
			linetypeToolbar.Visible = true;
			doc.SetFocus();
			if (objs != null)
				SelectEntities(objs);
		}



		/// <summary>
		/// Selection set requires to be executed inside command (with CommandFlags.Redraw).
		/// Toolbar works outside of command context.
		/// So internaly new command will be invoked, with data set as static variable.
		/// </summary>
		private static void SelectEntities(IEnumerable<ObjectId> objects)
		{
			if (objects == null)
				return;
			Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
			ed.SetImpliedSelection(new ObjectId[0]);
			ObjectId[] objs = objects.ToArray();
			ed.SetImpliedSelection(objs);
			PromptSelectionResult selRes = ed.SelectImplied();
		}


	}
}
