﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
using StyleNamespace = Teigha.GraphicsInterface;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
using StyleNamespace = Autodesk.AutoCAD.GraphicsInterface;
#endif

namespace CadPlugin.Commands
{
    class Labels
    {
        /// <summary>
        /// Initalizes labels.  Sets content and position of mtext (must be inside transaction)
        /// </summary>
        /// <param name="document"></param>
        public static void DisplayLabel(Document document)
        {
            //get data for display
            LabelSettingsHolder label = new LabelSettingsHolder();
            //get data
            Document doc = document;
            Database db = doc.Database;
            int ratioDecimalPlaces = -1;
            int slopeDecimalPlaces = -1;

            string crownData = "CROWN_TEXT_SETTINGS";
            string labelDecimals = "LABEL_DECIMAL_PLACES";
            int crownLevel = -1;
            using (Transaction tr = doc.TransactionManager.StartTransaction())
            {
                DBDictionary nod = tr.GetObject(db.NamedObjectsDictionaryId, OpenMode.ForRead) as DBDictionary;
                //find all axes
                if (!nod.Contains("C123_AXIS_DATA"))
                    return;
                DBDictionary axesData = tr.GetObject(nod.GetAt("C123_AXIS_DATA"), OpenMode.ForRead) as DBDictionary;
                if (axesData == null)
                    return;//no qsects
                //get values for levellines which require label in all axes
                foreach (DBDictionaryEntry entry in axesData)
                {
                    int axisIndex;//index of axis
                    if (!int.TryParse(entry.Key, out axisIndex))
                        continue;//key of entry is not a number - something is wrong, we need only numbers
                    //get axis name
                    string axisName = "";
                    {
                        DBDictionary axesDic = tr.GetObject(nod.GetAt("C123_AXIS"), OpenMode.ForRead) as DBDictionary;
                        if (!axesDic.Contains(axisIndex.ToString()))
                            continue;
                        ObjectId axisoId = axesDic.GetAt(axisIndex.ToString());
                        //if (axisoId.IsNull) 
                        //continue;
                        DBObject axisdbobj = tr.GetObject(axisoId, OpenMode.ForRead);
                        //if (axisdbobj == null)
                        //continue;
                        DBDictionary axisDic = axisdbobj as DBDictionary;
                        Xrecord axisData = tr.GetObject(axisDic.GetAt("DATA"), OpenMode.ForRead) as Xrecord;
                        axisName = (string)axisData.Data.AsArray()[2].Value;
                    }
                    label.AddAxis(axisIndex, axisName);
                    DBDictionary axis = tr.GetObject(entry.Value, OpenMode.ForWrite) as DBDictionary;

                    #region Get label decimals data
                    if (axis.Contains(labelDecimals))
                    {
                        if (ratioDecimalPlaces == -1) //if set, skip
                        {
                            Xrecord decimals = tr.GetObject(axis.GetAt(labelDecimals), OpenMode.ForRead) as Xrecord;
                            TypedValue[] vals = decimals.Data.AsArray();
                            //(90, ratioDecimals), (90, slopeDecimals)
                            ratioDecimalPlaces = (int)vals[0].Value;
                            slopeDecimalPlaces = (int)vals[1].Value;
                            label.NumberOfDecimalPlacesForRatio = ratioDecimalPlaces;
                            label.NumberOfDecimalPlacesForPercent = slopeDecimalPlaces;
                        }
                    }
                    else
                    {//it can happen that this doesn't exist in drawing (profiler was not called) so we ensure existence of this info
                        if (!axis.Contains(labelDecimals))
                        {
                            Xrecord decimals = new Xrecord();
                            //(90, ratioDecimals), (90, slopeDecimals)
                            TypedValue[] vals = new TypedValue[2];
                            vals[0] = new TypedValue(90, 2);
                            vals[1] = new TypedValue(90, 2);
                            decimals.Data = new ResultBuffer(vals);
                            axis.SetAt(labelDecimals, decimals);
                            tr.AddNewlyCreatedDBObject(decimals, true);
                            ratioDecimalPlaces = label.NumberOfDecimalPlacesForRatio = 2;
                            slopeDecimalPlaces = label.NumberOfDecimalPlacesForPercent = 2;
                        }
                    }
                    #endregion

                    #region get profiler data
                    string profilerData = "PROFILER_DATA";
                    if (axis.Contains(profilerData))
                    {
                        DBDictionary prof = tr.GetObject(axis.GetAt(profilerData), OpenMode.ForRead) as DBDictionary;
                        foreach (DBDictionaryEntry level in prof)
                        {
                            int levelline = 0;
                            //just in case we check this, because we can save other data here
                            if (!int.TryParse(level.Key, out levelline))
                                continue;
                            Xrecord record = tr.GetObject(level.Value, OpenMode.ForRead) as Xrecord;
                            //0 - is displayed, 1 - label type, 2 - text size, 3 - level name
                            TypedValue[] vals = record.Data.AsArray();
                            label.AddLevelline(axisIndex, levelline);
                            LevellineLabelSettings settings = label[axisIndex, levelline];
#if BRICSCAD
                            settings.Display = (bool)vals[0].Value;
#endif
#if AUTOCAD
                        settings.Display = (short)vals[0].Value > 0;
#endif
                            settings.Name = (string)vals[3].Value;
                            settings.Type = (string)vals[1].Value == Language.Ratio ? LabelType.RATIO : LabelType.PERCENT;
                            settings.TextSize = (double)vals[2].Value;
                        }
                    }
                    #endregion

                    //check if crown data is set - if not create it
                    #region crown data
                    if (!axis.Contains(crownData))
                    {
                        Xrecord rec = new Xrecord();
                        // 0 - display, 1 - label type, 2 - text size, 3 - empty
                        rec.Data = new ResultBuffer(new TypedValue(290, false), new TypedValue(1, ""), new TypedValue(40, 0.2), new TypedValue(1, ""));
                        axis.SetAt(crownData, rec);
                        tr.AddNewlyCreatedDBObject(rec, true);
                    }
                    Xrecord crown = tr.GetObject(axis.GetAt(crownData), OpenMode.ForRead) as Xrecord;
                    TypedValue[] values = crown.Data.AsArray();
                    //NOTE crown of the road maybe doesn't exist. we display to user data to set for cotr, but maybe it doesn't exist
                    label.AddLevelline(axisIndex, crownLevel);
                    LevellineLabelSettings st = label[axisIndex, crownLevel];
                    st.Name = "Crown of the road";
#if BRICSCAD
                    st.Display = (bool)values[0].Value;
#endif
#if AUTOCAD
                    st.Display = (short)values[0].Value > 0;
#endif
                    st.Type = (string)values[1].Value == Language.Ratio ? LabelType.RATIO : LabelType.PERCENT;
                    st.TextSize = (double)values[2].Value;
                    #endregion
                }
                #region check crown data in each qsect

                //check if we have entry for crown of the road in each qsect. it is used to save text settings for crown of the road
                DBDictionary qsects = tr.GetObject(nod.GetAt("C123_QSECT"), OpenMode.ForWrite) as DBDictionary;
                foreach (DBDictionaryEntry entry in qsects)
                {
                    DBDictionary qsect = tr.GetObject(entry.Value, OpenMode.ForWrite) as DBDictionary;
                    if (qsect != null && !qsect.Contains(crownData))
                    {
                        Xrecord rec = new Xrecord();
                        // 0 - left text handle , 1 - right text handle
                        rec.Data = new ResultBuffer(new TypedValue(1, ""), new TypedValue(1, ""));
                        qsect.SetAt(crownData, rec);
                        tr.AddNewlyCreatedDBObject(rec, true);
                    }
                }

                #endregion
                tr.Commit();
            }
            if (!LabelSettings.SetLabels(label))
                return;
            //iterate through all qsects and set new values
            using (Transaction tr = doc.TransactionManager.StartTransaction())
            {
                DBDictionary nod = tr.GetObject(db.NamedObjectsDictionaryId, OpenMode.ForRead) as DBDictionary;
                //find all axes
                DBDictionary axesData = tr.GetObject(nod.GetAt("C123_AXIS_DATA"), OpenMode.ForRead) as DBDictionary;
                if (axesData == null)
                    return;//no qsects
                //all labels go on same layer 123_QSchnitt_Neigungs_Beschriftung, so first check if it exists - if not create it
                string layername = "123_QSchnitt_Neigungs_Beschriftung";
                ObjectId layerid;
                //if layer doesn't exists create it
                using (Transaction tran1 = doc.TransactionManager.StartTransaction())
                {
                    LayerTable lt = tran1.GetObject(db.LayerTableId, OpenMode.ForWrite) as LayerTable;
                    LayerTableRecord ltr;
                    if (!lt.Has(layername))
                    {
                        ltr = new LayerTableRecord();
                        ltr.Name = layername;
                        //ltr.Color = Autodesk.AutoCAD.Colors.Color.FromColorIndex(Autodesk.AutoCAD.Colors.ColorMethod.ByLayer, 7);
                        //NOTE color of levellines set here
                        ltr.Color = Color.FromColorIndex(ColorMethod.ByLayer, 7);
                        lt.Add(ltr);
                        tran1.AddNewlyCreatedDBObject(ltr, true);
                    }
                    layerid = lt[layername];
                    tran1.Commit();
                }

                //get values for levellines which require label in all axes
                foreach (DBDictionaryEntry entry in axesData)
                {
                    int res;//index of axis
                    if (!int.TryParse(entry.Key, out res))
                        continue;//key of entry is not a number - something is wrong, we need only numbers
                    //label.AddAxis(res);
                    DBDictionary axis = tr.GetObject(entry.Value, OpenMode.ForRead) as DBDictionary;

                    #region Set label decimals data
                    if (axis.Contains(labelDecimals))
                    {
                        Xrecord decimals = tr.GetObject(axis.GetAt(labelDecimals), OpenMode.ForWrite) as Xrecord;
                        TypedValue[] vals = decimals.Data.AsArray();
                        //(90, ratioDecimals), (90, slopeDecimals)
                        vals[0] = new TypedValue(90, label.NumberOfDecimalPlacesForRatio);
                        vals[1] = new TypedValue(90, label.NumberOfDecimalPlacesForPercent);
                        decimals.Data = new ResultBuffer(vals);
                    }
                    #endregion

                    string profilerData = "PROFILER_DATA";
                    if (axis.Contains(profilerData))
                    {
                        DBDictionary prof = tr.GetObject(axis.GetAt(profilerData), OpenMode.ForRead) as DBDictionary;
                        foreach (DBDictionaryEntry level in prof)
                        {
                            int levelline = 0;
                            //just in case we check this, because we can save other data here
                            if (!int.TryParse(level.Key, out levelline))
                                continue;
                            Xrecord record = tr.GetObject(level.Value, OpenMode.ForWrite) as Xrecord;
                            LevellineLabelSettings settings = label[res, levelline];
                            //0 - is displayed, 1 - label type, 2 - text size, 3 - levelline name
                            ResultBuffer buffer = new ResultBuffer(new TypedValue(290, settings.Display),
                                new TypedValue(1, (settings.Type == LabelType.PERCENT ? Language.Percent : Language.Ratio)),
                                new TypedValue(40, settings.TextSize),
                                new TypedValue(1, label[res, levelline].Name));
                            record.Data = buffer;
                        }
                        //set crown of the road new data, and in qsects refresh - 
                        LevellineLabelSettings st = label[res, crownLevel];
                        Xrecord rc = tr.GetObject(axis.GetAt(crownData), OpenMode.ForWrite) as Xrecord;
                        TypedValue[] tv = rc.Data.AsArray();
                        tv[0] = new TypedValue(290, st.Display);
                        tv[1] = new TypedValue(1, (st.Type == LabelType.PERCENT ? Language.Percent : Language.Ratio));
                        tv[2] = new TypedValue(40, st.TextSize);
                        rc.Data = new ResultBuffer(tv);
                    }
                    {
                        LevellineLabelSettings st = label[res, crownLevel];
                        if (st == null)
                            continue;
                        Xrecord crown = tr.GetObject(axis.GetAt(crownData), OpenMode.ForWrite) as Xrecord;
                        TypedValue[] v = crown.Data.AsArray();
                        v[0] = new TypedValue(290, st.Display);
                        v[1] = new TypedValue(1, (st.Type == LabelType.PERCENT ? Language.Percent : Language.Ratio));
                        v[2] = new TypedValue(40, st.TextSize);
                        crown.Data = new ResultBuffer(v);
                    }
                }
                //iterate through all axes and create MText object, and other required data!
                if (nod.Contains("C123_QSECT"))
                {
                    DBDictionary qsects = tr.GetObject(nod.GetAt("C123_QSECT"), OpenMode.ForWrite) as DBDictionary;
                    BlockTableRecord btr = (BlockTableRecord)tr.GetObject(db.CurrentSpaceId, OpenMode.ForWrite);
                    foreach (DBDictionaryEntry entry in qsects)
                    {
                        int profileIndex = 0;
                        if (!int.TryParse(entry.Key, out profileIndex))
                            continue;

                        DBDictionary qsect = tr.GetObject(entry.Value, OpenMode.ForWrite) as DBDictionary;
                        Xrecord rec = tr.GetObject(qsect.GetAt("SECT_DATA"), OpenMode.ForRead) as Xrecord;
                        TypedValue[] values = rec.Data.AsArray();
                        int axisIndex = (short)values[0].Value;


                        DBDictionary profilerData = null;//we have to let it go to foreach loop, because maybe there is crown of the road levelline
                        if (qsect.Contains("PROFILER_POLYLINES"))
                            profilerData = tr.GetObject(qsect.GetAt("PROFILER_POLYLINES"), OpenMode.ForWrite) as DBDictionary;
                        foreach (int levelline in label.GetAllLevellines(axisIndex))
                        {
                            //here should check if levelline is -1. if it is, we have to set mtext objects for both sides of crown of the road, and after we same thing do in refresh labels

                            LevellineLabelSettings settings = label[axisIndex, levelline];
                            //0 - polyline handle, 1 - pl name, 2 - text handle
                            if (levelline == -1)//if levelline is crown of the road
                            {
                                rec = tr.GetObject(qsect.GetAt(crownData), OpenMode.ForWrite) as Xrecord;
                                values = rec.Data.AsArray();
                                //2 values, left and right text handle
                                MText t1, t2;
                                t1 = t2 = null;
                                string handle1 = (string)values[0].Value;
                                string handle2 = (string)values[1].Value;
                                if (handle1 != "")//both are same set - if one is not set, then other is also not set
                                {
                                    long handleV = long.Parse(handle1, System.Globalization.NumberStyles.AllowHexSpecifier);
                                    t1 = tr.GetObject(db.GetObjectId(false, new Handle(handleV), 0), OpenMode.ForWrite) as MText;
                                    handleV = long.Parse(handle2, System.Globalization.NumberStyles.AllowHexSpecifier);
                                    t2 = tr.GetObject(db.GetObjectId(false, new Handle(handleV), 0), OpenMode.ForWrite) as MText;
                                }
                                if (!settings.Display)
                                {
                                    if (t1 != null)
                                    {
                                        t1.Erase(true);
                                        t2.Erase(true);
                                        values[0] = new TypedValue(1, "");
                                        values[1] = new TypedValue(1, "");
                                        rec.Data = new ResultBuffer(values);
                                    }
                                    continue;
                                }
                                if (t1 == null)
                                {
                                    t1 = new MText();
                                    t2 = new MText();
                                    t1.SetLayerId(layerid, false);
                                    t2.SetLayerId(layerid, false);
                                    btr.AppendEntity(t1);
                                    btr.AppendEntity(t2);
                                    tr.AddNewlyCreatedDBObject(t1, true);
                                    tr.AddNewlyCreatedDBObject(t2, true);
                                }
                                values[0] = new TypedValue(1, t1.Handle.Value.ToString("x"));
                                values[1] = new TypedValue(1, t2.Handle.Value.ToString("x"));
                                rec.Data = new ResultBuffer(values);

                                continue;
                            }
                            if (profilerData == null || !profilerData.Contains(levelline.ToString()))
                                continue;
                            rec = tr.GetObject(profilerData.GetAt(levelline.ToString()), OpenMode.ForWrite) as Xrecord;
                            values = rec.Data.AsArray();
                            MText text = null;
                            string textHandle = (string)values[2].Value;
                            if (textHandle != "")
                            {
                                long handleValue = long.Parse(textHandle, System.Globalization.NumberStyles.AllowHexSpecifier);
                                text = tr.GetObject(db.GetObjectId(false, new Handle(handleValue), 0), OpenMode.ForWrite) as MText;
                            }
                            if (!settings.Display)
                            {
                                //check if text exists, and if it is delete it and erase handle of text 
                                if (text != null)
                                {
                                    text.Erase(true);
                                    values[2] = new TypedValue(1, "");
                                    rec.Data = new ResultBuffer(values);
                                }
                                continue;
                            }
                            if (text == null)
                            {
                                text = new MText();
                                text.SetLayerId(layerid, false);
                                btr.AppendEntity(text);
                                tr.AddNewlyCreatedDBObject(text, true);
                            }
                            values[2] = new TypedValue(1, text.Handle.Value.ToString("x"));
                            rec.Data = new ResultBuffer(values);
                        }
                    }
                }


                tr.Commit();
            }
            //RefreshLabels();
        }


        public static void Refresh(Document document)
        {
            //iterate through axis data, if levelline is displayed iterate through all qsects of axis and refresh values
            Document doc = document;
            Database db = doc.Database;
            //doc.LockDocument();
            //because we iterate through qsect, we need easy access to data <axisIndex, <levelline number, (label type, text size, levelline name)>
            Dictionary<int, Dictionary<int, Tuple<LabelType, double, string>>> data = new Dictionary<int, Dictionary<int, Tuple<LabelType, double, string>>>();


            string crownData = "CROWN_TEXT_SETTINGS";
            int crownLevel = -1;
            int ratioDecimals = -1;
            int slopeDecimals = -1;
            string labelDecimals = "LABEL_DECIMAL_PLACES";
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                DBDictionary nod = tr.GetObject(db.NamedObjectsDictionaryId, OpenMode.ForRead) as DBDictionary;

                //find all axes
                if (!nod.Contains("C123_AXIS_DATA"))
                {
                    tr.Dispose();
                    return;//no axes
                }
                DBDictionary axesData = tr.GetObject(nod.GetAt("C123_AXIS_DATA"), OpenMode.ForRead) as DBDictionary;

                //get values for levellines which require label in all axes
                foreach (DBDictionaryEntry entry in axesData)
                {
                    int res;//index of axis
                    if (!int.TryParse(entry.Key, out res))
                        continue;//key of entry is not a number - something is wrong, we need only numbers
                    DBDictionary axis = tr.GetObject(entry.Value, OpenMode.ForRead) as DBDictionary;
                    if (ratioDecimals == -1)
                    {
                        if (axis.Contains(labelDecimals))
                        {
                            Xrecord decimals = tr.GetObject(axis.GetAt(labelDecimals), OpenMode.ForRead) as Xrecord;
                            TypedValue[] vals = decimals.Data.AsArray();
                            ratioDecimals = (int)vals[0].Value;
                            slopeDecimals = (int)vals[1].Value;
                        }
                    }
                    string profilerData = "PROFILER_DATA";
                    if (axis.Contains(profilerData))
                    {
                        DBDictionary prof = tr.GetObject(axis.GetAt(profilerData), OpenMode.ForRead) as DBDictionary;
                        foreach (DBDictionaryEntry level in prof)
                        {
                            int levelline = 0;
                            //just in case we check this, because we can save other data here
                            if (!int.TryParse(level.Key, out levelline))
                                continue;
                            Xrecord record = tr.GetObject(level.Value, OpenMode.ForRead) as Xrecord;
                            //0 - is displayed, 1 - label type, 2 - text size, 3 - levelline name
                            TypedValue[] vals = record.Data.AsArray();
                            //int levelline = int.Parse(level.Key);
#if BRICSCAD
                            if (!(bool)vals[0].Value)
#endif
#if AUTOCAD
                            if ((short)vals[0].Value == 0)
#endif
                                continue;
                            if (!data.ContainsKey(res))
                                data.Add(res, new Dictionary<int, Tuple<LabelType, double, string>>());
                            if (!data[res].ContainsKey(levelline))
                            {
                                LabelType lType = (string)vals[1].Value == Language.Percent ? LabelType.PERCENT : LabelType.RATIO;
                                double textSize = (double)vals[2].Value;
                                string levelName = (string)vals[3].Value;
                                data[res].Add(levelline, new Tuple<LabelType, double, string>(lType, textSize, levelName));
                            }
                        }
                    }

                    if (axis.Contains(crownData))
                    {
                        Xrecord rec = tr.GetObject(axis.GetAt(crownData), OpenMode.ForWrite) as Xrecord;
                        TypedValue[] vals = rec.Data.AsArray();
#if BRICSCAD
                        if ((bool)vals[0].Value)
#endif
#if AUTOCAD
                        if ((short)vals[0].Value > 0)
#endif
                        {
                            if (!data.ContainsKey(res))
                                data.Add(res, new Dictionary<int, Tuple<LabelType, double, string>>());
                            if (!data[res].ContainsKey(crownLevel))
                            {
                                LabelType lType = (string)vals[1].Value == Language.Percent ? LabelType.PERCENT : LabelType.RATIO;
                                double textSize = (double)vals[2].Value;
                                data[res].Add(crownLevel, new Tuple<LabelType, double, string>(lType, textSize, ""));
                            }
                        }

                    }

                }

                //go through all stations (in all axes) and set labels positions and text
                if (nod.Contains("C123_QSECT"))
                {
                    DBDictionary qsects = tr.GetObject(nod.GetAt("C123_QSECT"), OpenMode.ForWrite) as DBDictionary;
                    if (qsects.Count == 0)
                        return;
                    try
                    {
                        //iterate through all stations
                        foreach (DBDictionaryEntry entry in qsects)
                        {
                            DBDictionary qsect = tr.GetObject(entry.Value, OpenMode.ForWrite) as DBDictionary;
                            if (qsect == null)
                                continue;
                            //in sect data is stored axis index for current qsect
                            Xrecord qData = tr.GetObject(qsect.GetAt("SECT_DATA"), OpenMode.ForRead) as Xrecord;
                            int axisIndex = (short)qData.Data.AsArray()[0].Value;
                            //get center point of current profile, so can calculate relative coordinates to profile
                            Xrecord rec = tr.GetObject(qsect.GetAt("MORE"), OpenMode.ForRead) as Xrecord;
                            TypedValue[] values = rec.Data.AsArray();
                            double dx = (double)values[0].Value;//delta value, from circel marker to center line

                            rec = tr.GetObject(qsect.GetAt("START_POS_MARKER"), OpenMode.ForRead) as Xrecord;
                            string handleName = (string)rec.Data.AsArray()[0].Value;
                            long handleValue = long.Parse(handleName, System.Globalization.NumberStyles.AllowHexSpecifier);
                            Circle c = tr.GetObject(db.GetObjectId(false, new Handle(handleValue), 0), OpenMode.ForRead) as Circle;
                            Point3d cp = new Point3d(c.Center.X - dx, c.Center.Y, c.Center.Z);


                            //if axis is stored here, then we have at least one levelline to display, otherwise this axis doesn't require labeling
                            if (data.ContainsKey(axisIndex))
                            {
                                //special segment in each qsect where is stored group of levelline-polyline handle-levelline name-text handle .
                                if (qsect.Contains("PROFILER_POLYLINES"))
                                {
                                    DBDictionary prof = tr.GetObject(qsect.GetAt("PROFILER_POLYLINES"), OpenMode.ForRead) as DBDictionary;
                                    if (prof == null)
                                        continue;

                                    foreach (DBDictionaryEntry levelline in prof)
                                    {
                                        //i think here should be no other than numbers in the key
                                        int level = int.Parse(levelline.Key);
                                        if (!data[axisIndex].ContainsKey(level))
                                            continue;
                                        Xrecord record = tr.GetObject(levelline.Value, OpenMode.ForRead) as Xrecord;
                                        //get polyline handle, and get polyline
                                        //get label handle and get label
                                        //calculate data
                                        // 1 - levelline name, 0 - polyline handle, 2 - text handle
                                        TypedValue[] levelData = record.Data.AsArray();
                                        string plHandleString = (string)levelData[0].Value;
                                        Handle plHandle = new Handle(long.Parse(plHandleString, System.Globalization.NumberStyles.AllowHexSpecifier));
                                        Polyline pl = tr.GetObject(db.GetObjectId(false, plHandle, 0), OpenMode.ForRead) as Polyline;
                                        //this should prevent program from crashing if user deletes data
                                        if (pl == null)
                                            continue;
                                        Point3d first = pl.GetPoint3dAt(0);
                                        //first = first.Subtract(new Vector3d(cp.X, cp.Y, cp.Z));
                                        Point3d second = pl.GetPoint3dAt(1);

                                        //this should prevent program from crashing if user deletes data
                                        if (first == null || second == null)
                                            continue;

                                        //second = second.Subtract(new Vector3d(cp.X, cp.Y, cp.Z));
                                        LabelType lType = data[axisIndex][level].Item1;

                                        string textHandleString = (string)levelData[2].Value;
                                        Handle textHandle = new Handle(long.Parse(textHandleString, System.Globalization.NumberStyles.AllowHexSpecifier));
                                        MText text = tr.GetObject(db.GetObjectId(false, textHandle, 0), OpenMode.ForWrite) as MText;
                                        //this should prevent program from crashing if user deletes data
                                        if (text == null)
                                            continue;

                                        double textSize = data[axisIndex][level].Item2;//(double)levelData[3].Value;
                                        text.TextHeight = textSize;
                                        SetMTextData(tr, doc, first, second, cp, text, lType, ratioDecimals, slopeDecimals);
                                    }
                                }
                                //set crown
                                if (!data[axisIndex].ContainsKey(crownLevel))
                                    continue;
                                {
                                    Xrecord record = tr.GetObject(qsect.GetAt(crownData), OpenMode.ForRead) as Xrecord;
                                    // 0 - left text handle, 1 - right text handle
                                    TypedValue[] levelData = record.Data.AsArray();
                                    string leftTxt = (string)levelData[0].Value;
                                    Handle leftTextH = new Handle(long.Parse(leftTxt, System.Globalization.NumberStyles.AllowHexSpecifier));
                                    MText leftText = tr.GetObject(db.GetObjectId(false, leftTextH, 0), OpenMode.ForWrite) as MText;
                                    string rightTxt = (string)levelData[1].Value;
                                    Handle rightTextH = new Handle(long.Parse(rightTxt, System.Globalization.NumberStyles.AllowHexSpecifier));
                                    MText rightText = tr.GetObject(db.GetObjectId(false, rightTextH, 0), OpenMode.ForWrite) as MText;



                                    //get crown of the road polyline
                                    if (qsect.Contains("GRAD_0_ROAD_POLY"))
                                    {
                                        rec = tr.GetObject(qsect.GetAt("GRAD_0_ROAD_POLY"), OpenMode.ForRead) as Xrecord;
                                        //Autodesk.AutoCAD.GraphicsInterface.Polyline pl;
                                        handleName = (string)rec.Data.AsArray()[0].Value;
                                        handleValue = long.Parse(handleName, System.Globalization.NumberStyles.AllowHexSpecifier);
                                        Polyline pl = tr.GetObject(db.GetObjectId(false, new Handle(handleValue), 0), OpenMode.ForRead) as Polyline;

                                        LabelType lType = data[axisIndex][crownLevel].Item1;

                                        double textSize = data[axisIndex][crownLevel].Item2;//(double)levelData[3].Value;

                                        if (leftText != null)
                                            leftText.TextHeight = textSize;
                                        if (rightText != null)
                                            rightText.TextHeight = textSize;


                                        Point3d first = pl.GetPoint3dAt(0);
                                        //first = first.Subtract(new Vector3d(cp.X, cp.Y, cp.Z));
                                        Point3d second = pl.GetPoint3dAt(1);
                                        //second = second.Subtract(new Vector3d(cp.X, cp.Y, cp.Z));

                                        if (leftText != null)
                                            SetMTextData(tr, doc, first, second, cp, leftText, lType, ratioDecimals, slopeDecimals);
                                        first = pl.GetPoint3dAt(1);
                                        //first = first.Subtract(new Vector3d(cp.X, cp.Y, cp.Z));
                                        second = pl.GetPoint3dAt(2);
                                        //second = second.Subtract(new Vector3d(cp.X, cp.Y, cp.Z));

                                        if (rightText != null)
                                            SetMTextData(tr, doc, first, second, cp, rightText, lType, ratioDecimals, slopeDecimals);

                                    }
                                }
                            }
                        }
                    }
                    catch (System.Exception)
                    { }
                }
                tr.Commit();

            }
        }

        private static bool deleteProfilerData = false;
        
        /// <summary>
        /// Deletes labels from program, and optionally deletes all profiler related data
        /// </summary>
        public static void Clear(Document document)
        {
            //clear settings from axis data

            //clear settings from qsect
            Document doc = document;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            using (Transaction tr = doc.TransactionManager.StartTransaction())
            {
                DBDictionary nod = tr.GetObject(db.NamedObjectsDictionaryId, OpenMode.ForRead) as DBDictionary;
                if (nod.Contains("C123_AXIS_DATA"))
                {
                    DBDictionary axesData = tr.GetObject(nod.GetAt("C123_AXIS_DATA"), OpenMode.ForWrite) as DBDictionary;
                    foreach (DBDictionaryEntry entry in axesData)
                    {
                        int axisIndex;//index of axis
                        if (!int.TryParse(entry.Key, out axisIndex))
                            continue;//key of entry is not a number - something is wrong, we need only numbers
                        DBDictionary axis = tr.GetObject(entry.Value, OpenMode.ForWrite) as DBDictionary;
                        //crown set
                        {
                            string crown = "CROWN_TEXT_SETTINGS";
                            if (axis.Contains(crown))
                            {
                                Xrecord rec = tr.GetObject(axis.GetAt(crown), OpenMode.ForWrite) as Xrecord;
                                TypedValue[] values = rec.Data.AsArray();
                                values[0] = new TypedValue(290, false);
                                rec.Data = new ResultBuffer(values);

                                if (deleteProfilerData)
                                    axis.Remove(crown);
                            }
                        }
                        string profilerDataEntry = "PROFILER_DATA";
                        if (axis.Contains(profilerDataEntry))
                        {
                            if (deleteProfilerData)
                                axis.Remove(profilerDataEntry);
                            else
                            {//if only removing labels, then set display to false
                                DBDictionary profilerData = tr.GetObject(axis.GetAt(profilerDataEntry), OpenMode.ForRead) as DBDictionary;
                                foreach (DBDictionaryEntry entry1 in profilerData)
                                {
                                    int levelline = int.Parse(entry1.Key);
                                    Xrecord rec = tr.GetObject(entry1.Value, OpenMode.ForWrite) as Xrecord;
                                    TypedValue[] values = rec.Data.AsArray();
                                    values[0] = new TypedValue(290, false);
                                    rec.Data = new ResultBuffer(values);
                                }
                            }
                        }
                    }
                }
                if (nod.Contains("C123_QSECT"))
                {
                    DBDictionary qsects = tr.GetObject(nod.GetAt("C123_QSECT"), OpenMode.ForWrite) as DBDictionary;
                    foreach (DBDictionaryEntry entry in qsects)
                    {
                        int qsectIndex;//index of axis
                        if (!int.TryParse(entry.Key, out qsectIndex))
                            continue;//key of entry is not a number - something is wrong, we need only numbers
                        DBDictionary qsect = tr.GetObject(entry.Value, OpenMode.ForWrite) as DBDictionary;
                        //crown set
                        {
                            string crown = "CROWN_TEXT_SETTINGS";
                            if (qsect.Contains(crown))
                            {
                                Xrecord rec = tr.GetObject(qsect.GetAt(crown), OpenMode.ForWrite) as Xrecord;
                                MText left;
                                MText right;
                                TypedValue[] values = rec.Data.AsArray();
                                string textHandle = (string)values[0].Value;
                                if (textHandle != "")
                                {
                                    long handleValue = long.Parse(textHandle, System.Globalization.NumberStyles.AllowHexSpecifier);
                                    left = tr.GetObject(db.GetObjectId(false, new Handle(handleValue), 0), OpenMode.ForWrite) as MText;
                                    left.Erase(true);
                                }
                                textHandle = (string)values[1].Value;
                                if (textHandle != "")
                                {
                                    long handleValue = long.Parse(textHandle, System.Globalization.NumberStyles.AllowHexSpecifier);
                                    right = tr.GetObject(db.GetObjectId(false, new Handle(handleValue), 0), OpenMode.ForWrite) as MText;
                                    right.Erase(true);
                                }
                                values[0] = new TypedValue(1, "");
                                values[1] = new TypedValue(1, "");
                                rec.Data = new ResultBuffer(values);
                                if (deleteProfilerData)
                                {
                                    qsect.Remove(crown);
                                }
                            }
                        }
                        string qsectProfiler = "PROFILER_POLYLINES";
                        if (!qsect.Contains(qsectProfiler))
                            continue;
                        DBDictionary profilerData = tr.GetObject(qsect.GetAt(qsectProfiler), OpenMode.ForWrite) as DBDictionary;
                        foreach (DBDictionaryEntry entry1 in profilerData)
                        {
                            int levelline = int.Parse(entry1.Key);
                            Xrecord rec = tr.GetObject(entry1.Value, OpenMode.ForWrite) as Xrecord;
                            TypedValue[] values = rec.Data.AsArray();
                            //delete text object
                            string textHandle = (string)values[2].Value;
                            if (textHandle != "")
                            {
                                long handleValue = long.Parse(textHandle, System.Globalization.NumberStyles.AllowHexSpecifier);
                                MText text = tr.GetObject(db.GetObjectId(false, new Handle(handleValue), 0), OpenMode.ForWrite) as MText;
                                text.Erase(true);
                            }
                            values[2] = new TypedValue(1, "");
                            rec.Data = new ResultBuffer(values);
                            //delete polyline object
                            if (deleteProfilerData)
                            {
                                string polylineHandle = (string)values[0].Value;
                                if (polylineHandle != "")
                                {
                                    long handleValue = long.Parse(polylineHandle, System.Globalization.NumberStyles.AllowHexSpecifier);
                                    Polyline pl = tr.GetObject(db.GetObjectId(false, new Handle(handleValue), 0), OpenMode.ForWrite) as Polyline;
                                    pl.Erase(true);
                                }
                                values[0] = new TypedValue(1, "");


                            }
                        }
                        if (deleteProfilerData)
                            qsect.Remove(qsectProfiler);
                    }
                }

                tr.Commit();
            }
            deleteProfilerData = false;
        }

        /// <summary>
        /// Clears labels and deletes profiler related data.
        /// </summary>
        /// <param name="doc"></param>
        public static void ClearProfilerData(Document doc)
        {
            //PromptIntegerOptions opt = new PromptIntegerOptions("Enter code 123 to confirm delete all data loaded from Profiler");
            PromptIntegerOptions opt = new PromptIntegerOptions("Bitte Code 123 eingeben und mit <Enter> bestätigen, um alle ProFiler Objekte zu löschen:");
            PromptIntegerResult res = doc.Editor.GetInteger(opt);
            if (res.Status != PromptStatus.OK)
                return;
            if (res.Value != 123)
                return;
            deleteProfilerData = true;
            Clear(doc);
            deleteProfilerData = false;
        }

        private static void SetMTextData(Transaction tr, Document doc, Point3d p1, Point3d p2, Point3d center, MText textControl, LabelType type, int ratioDecimals, int slopeDecimals)
        {
            #region Set label text
            double angleRad = MyUtilities.Helpers.Calculations.AngleBetweenPointsRAD(p1.X, p2.X, p1.Y, p2.Y);
            string labelText = "";
            if (type == LabelType.PERCENT)
            {
                //result is always considering as angle is going from 90deg to -90deg, fo 1st and 4th quadrant. if line is on the left side, 
                //just have to invert calculated sign. \|/ = + + ; /|/ = - +
                //NOTE if one point is on the left/right, and other one is on the opposite, we consider it as right side line
                bool isLeft = Math.Round(p1.X - center.X, 2) <= 0 && Math.Round(p2.X - center.X) <= 0;
                bool positive = angleRad >= 0 && !isLeft || angleRad <= 0 && isLeft;
                if (positive)
                    labelText = "↑  ";
                else
                    labelText = "↓  ";
                double percent = angleRad * 180 / Math.PI;
                //percent = Math.Abs(percent);
                // % = tg(angle) * 100
                percent = Math.Tan(angleRad) * 100;
                //percent /= 90;
                //percent *= 100;
                percent = Math.Abs(percent);
                labelText += percent.ToString("F" + slopeDecimals, System.Globalization.NumberFormatInfo.InvariantInfo) + " % ";
            }
            else if (type == LabelType.RATIO)
            {
                //we need one of values to be exactly 1!
                //so, we find both values, and multiply them until one of them has value 1. Goal is to multiply with smaller number.
                //if 0.5:0.4 - we use multiplier 2, because it is less than 2.5
                double x = p1.X - p2.X;
                double y = p1.Y - p2.Y;
                double lenght = Math.Sqrt(x * x + y * y);
                double t1 = Math.Sin(angleRad) * lenght;
                double t2 = Math.Cos(angleRad) * lenght;
                t1 = Math.Abs(t1);
                t2 = Math.Abs(t2);
                //we chose value which is closer to 1, and scale other value
                double scale = 0;
                if (t1 <= t2)
                {
                    scale = 1 / t1;
                    t2 *= scale;
                    labelText = "1:" + t2.ToString("F" + ratioDecimals, System.Globalization.NumberFormatInfo.InvariantInfo);
                }
                else
                {
                    scale = 1 / t2;
                    t1 *= scale;
                    labelText = t1.ToString("F" + ratioDecimals, System.Globalization.NumberFormatInfo.InvariantInfo) + ":1";
                }

            }
            #endregion

            //get style so i can calculate size of mtext
            TextStyleTable st = (TextStyleTable)tr.GetObject(doc.Database.TextStyleTableId, OpenMode.ForRead);

            StyleNamespace.TextStyle style;// = new TextStyle(;
            //NOTE if other textstyle is used in mtext box, here can be problems with positioning because we assume Standard style is used
            string name = "Standard";
            TextStyleTableRecord s = tr.GetObject(st[name], OpenMode.ForRead) as TextStyleTableRecord;
            style = new StyleNamespace.TextStyle();
            style.BigFontFileName = s.BigFontFileName;
            style.FileName = s.FileName;
            style.TextSize = textControl.TextHeight;
            style.ObliquingAngle = s.ObliquingAngle;
            style.XScale = s.XScale;
            byte n = style.LoadStyleRec;

            double height = 0;
            double width = 0;
            textControl.Contents = labelText;
#if BRICSCAD
            //TODO text size bricscad
            height = textControl.ActualHeight;
            width = textControl.ActualWidth;

#endif
#if AUTOCAD
            Extents2d ex = style.ExtentsBox(labelText, true, true, null);
            height = ex.MaxPoint.Y - ex.MinPoint.Y;
            width = ex.MaxPoint.X - ex.MinPoint.X;
#endif
            //rotate text control
            Matrix3d ucs = doc.Editor.CurrentUserCoordinateSystem;
            textControl.Rotation = 0;
            //textControl.Rotation = angleRad;//strange thing happen, possible reason is if angleRad is NaN
            Matrix3d trans =
                Matrix3d.Rotation(
                  angleRad,
                  ucs.CoordinateSystem3d.Zaxis,
                  new Point3d(0, 0, 0));
            textControl.TransformBy(trans);


            height *= 1.1;//so text doesn't touch line
            width /= 2;//we move center of text to center of line
            double textX = p2.X + p1.X;//move text position on center of line
            double textY = p2.Y + p1.Y;
            textX /= 2;
            textY /= 2;

            textControl.Location = new Point3d(textX - height * Math.Sin(angleRad) - width * Math.Cos(angleRad), textY + height * Math.Cos(angleRad) - width * Math.Sin(angleRad), 0);
        }

    }
}
