﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common
{
    public class NODExport
    {
        /// <summary>
        /// Exports complete NOD to XML file.
        /// </summary>
        /// <param name="fileName">If null, exporting to 123CAD documents folder.</param>
        public static void ExportNODToFile(Document doc = null, string fileName = null)
        {
            doc = doc ?? Application.DocumentManager.MdiActiveDocument;
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                Commands.Private.Nod.InvokeNew(tw, tw.NOD, true, fileName);

                // Do commit even for readonly operations.
                tw.Commit();
            }

        }
    }
}
