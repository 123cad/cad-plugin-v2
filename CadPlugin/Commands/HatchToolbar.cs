﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using forms = System.Windows.Forms;
using System.Runtime.InteropServices;
using CadPlugin.HatchToolbars;
using CadPlugin.HatchToolbars.Data;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

[assembly: CommandClass(typeof(CadPlugin.Commands.HatchToolbar))]

namespace CadPlugin.Commands
{
	public class HatchToolbar
	{		
		private static HatchToolbars.Toolbar hatchToolbar;

		private static bool commandInProgress = false;

		/// <summary>
		/// Called when application is loded to CAD.
		/// </summary>
		public static void ApplicationInitialized()
		{
			// If toolbar was opened last time, it reopens here.
			if (Properties.HatchToolbar.Default.ToolbarOpened)
			{
				// Open toolbar with saved width and height settings
				//ToolbarShow();

				MyCommands.HatchToolbarShow();
			}
		}

		public static void ToolbarShow()
		{
			if (hatchToolbar == null)
			{
				Action<string> invokeCommand = (s) => Application.DocumentManager.MdiActiveDocument.SendStringToExecute(s + '\n', true, false, false);
				hatchToolbar = new HatchToolbars.Toolbar();
				hatchToolbar.HatchSelected += HatchToolbar_CreateAttribute;
				hatchToolbar.CreateTable += HatchToolbar_CreateTable;
				hatchToolbar.DeleteTable += HatchToolbar_DeleteTable;
				hatchToolbar.AppendAreaToTable += HatchToolbar_AppendAreaToTable;
				hatchToolbar.RemoveAreaFromtTable += HatchToolbar_RemoveAreaFromtTable;
				hatchToolbar.AreaToSelection += HatchToolbar_AreaToSelection;
				hatchToolbar.AllAreasToSelection += HatchToolbar_AllAreasToSelection;
				hatchToolbar.AreaTablesToSelection += HatchToolbar_AreaTablesToSelection;
				hatchToolbar.SaveTableToCSV += HatchToolbar_SaveTableToCSV;
				hatchToolbar.TableSettings += HatchToolbar_TableSettings;
				hatchToolbar.TopMost = false;

				hatchToolbar.FormClosed += (_, __) =>
				{
					hatchToolbar = null;
				};
				hatchToolbar.MouseEnter += (_, __) =>
				{
					// No need that toolbar gets focus.
					//hatchToolbar.Activate();
				};
				hatchToolbar.MouseLeave += (_, __) =>
				{
					// Toolbar get mouse input focus without being active window of the desktop.
					// CAD application can always have focus, and toolbar will be visible and clickable.
					// Any time mouse leaves the form, set focus to CAD.
					if (Application.DocumentManager.MdiActiveDocument != null)
						Application.DocumentManager.MdiActiveDocument.SetFocus();
					
				};

				Application.BeginQuit += (_, __) =>
				{
					if (hatchToolbar != null)
						hatchToolbar.Close();
					hatchToolbar = null;
				};
#if BRICSCAD
				// This one makes problem, but not sure which one exactly.
				//Application.ShowModelessDialog((forms.IWin32Window)Application.MainWindow, hatchToolbar);
				hatchToolbar.Show((forms.IWin32Window)Application.MainWindow);
#endif
#if AUTOCAD
				Application.ShowModelessDialog(Application.MainWindow.Handle, hatchToolbar);
#endif

				HatchCommands.CommandFinished += HatchCommands_CommandFinished;
			}
			else if (!hatchToolbar.Visible)
				ShowToolbar(Application.DocumentManager.MdiActiveDocument);
		}


		private static void HatchCommands_CommandFinished(Document obj)
		{
			if (commandInProgress)
			{
				commandInProgress = false;
				ShowToolbar(obj);
			}
		}

		public static void ExportExtensionDic(Document doc)
		{
			string fileName = "extensionDic_Hatch.xml";
			if (ExportExtensionDictionary.ExportToXML(doc, fileName))
				System.Diagnostics.Process.Start(fileName);
		}
		public static void HatchToolbar_AreaTablesToSelection()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			commandInProgress = true;
			try
			{
				HatchCommands.AreaTablesToSelection(doc);
				//doc.SendStringToExecute("123HatchSelectAreaTables\n", true, false, false);
			}
			finally
			{
				//ShowToolbar(doc);
			}
		}
		public static void HatchToolbar_AllAreasToSelection()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			commandInProgress = true;
			try
			{
			HatchCommands.AllAreasToSelection(doc);
			//doc.SendStringToExecute("123HatchSelectTableAllAreas\n", true, false, false);
			}
			finally
			{
				ShowToolbar(doc);
			}
		}

		public static void HatchToolbar_AreaToSelection()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			commandInProgress = true;
			try
			{
				HatchCommands.AreaToSelection(doc);
				//doc.SendStringToExecute("123HatchSelectTableArea\n", true, false, false);
			}
			finally
			{
				ShowToolbar(doc);
			}
		}

		private static void HatchToolbar_SaveTableToCSV()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			commandInProgress = true;
			try
			{
				HatchCommands.ExportTable(doc);
				//doc.SendStringToExecute("123HatchSelectTableArea\n", true, false, false);
			}
			finally
			{
				ShowToolbar(doc);
			}
		}
		private static void HatchToolbar_TableSettings()
		{
			try
			{
				HatchToolbars.ToolbarSettings.TableSettingsView.ShowTable();
			}
			finally
			{
			}
		}
		public static void HatchToolbar_RemoveAreaFromtTable()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			try
			{
				HatchCommands.RemoveAreaFromTable(doc);
			}
			finally
			{
				ShowToolbar(doc);
			}
		}

		public static void HatchToolbar_AppendAreaToTable()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			try
			{
				HatchCommands.AddAreaToTable(doc);
			}
			finally
			{
				ShowToolbar(doc);
			}
		}

		public static void HatchToolbar_DeleteTable()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			try
			{
				HatchCommands.DeleteTable(doc);
			}
			finally
			{
				ShowToolbar(doc);
			}
		}

		public static void HatchToolbar_CreateTable()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			try
			{
				HatchCommands.CreateHatchTable(doc);
			}
			finally
			{
				ShowToolbar(doc);
			}
		}

		private static void HatchToolbar_CreateAttribute(CadPlugin.HatchToolbars.SingleHatchData data)
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			HideToolbar(doc);
			try
			{
				HatchCommands.HatchDataSelected(doc, data);
			}
			finally
			{
				ShowToolbar(doc);
			}
		}
		private static void HideToolbar(Document doc)
		{
			hatchToolbar.Enabled = false;
			hatchToolbar.Visible = false;

			//doc.SetFocus();
		}
		private static void ShowToolbar(Document doc, IEnumerable<ObjectId> objs  = null)
		{
			hatchToolbar.Enabled = true;
			hatchToolbar.Visible = true;
			doc.SetFocus();
			if (objs != null)
				SelectEntities(objs);
		}



		/// <summary>
		/// Selection set requires to be executed inside command (with CommandFlags.Redraw).
		/// Toolbar works outside of command context.
		/// So internaly new command will be invoked, with data set as static variable.
		/// </summary>
		private static void SelectEntities(IEnumerable<ObjectId> objects)
		{
			if (objects == null)
				return;
			Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
			ed.SetImpliedSelection(new ObjectId[0]);
			ObjectId[] objs = objects.ToArray();
			ed.SetImpliedSelection(objs);
			PromptSelectionResult selRes = ed.SelectImplied();
		}


	}
}
