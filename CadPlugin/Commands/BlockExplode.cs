﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Commands
{
    /// <summary>
    /// Replaces attributes from a block with texts.
    /// </summary>
    class BlockExplode
    {
        /// <summary>
        /// Starts command with user choosing BlockReference to apply the change.
        /// </summary>
        /// <param name="doc"></param>
        internal static void StartSelectedBlock(Document doc)
        {
            Editor ed = doc.Editor;
            Database db = doc.Database;

            PromptEntityOptions opts = new PromptEntityOptions("Select BlockReference: ");
            opts.SetRejectMessage("Not a BlockReference!");
            opts.AddAllowedClass(typeof(BlockReference), true);
            opts.AllowNone = false;
            PromptEntityResult res = ed.GetEntity(opts);
            if (res.Status == PromptStatus.OK)
            {
                string blockName = "";
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    BlockReference br = tr.GetObject(res.ObjectId, OpenMode.ForRead) as BlockReference;
                    blockName = br.Name;
                }
                bool applyToAllBlocks = false;
                PromptKeywordOptions kOpt = new PromptKeywordOptions($"Apply to all <{blockName}> blocks in the drawing?");
                kOpt.Keywords.Add("Ja");
                kOpt.Keywords.Add("Nein");
                kOpt.Keywords.Default = "Nein";
                kOpt.AllowArbitraryInput = false;
                PromptResult kRes = ed.GetKeywords(kOpt);
                if (kRes.Status == PromptStatus.OK)
                {
                    applyToAllBlocks = kRes.StringResult == "Ja";
                    List<ObjectId> blocks = new List<ObjectId>();
                    using (TransactionWrapper tr = doc.StartTransaction())
                    {
                        tr.MS.UpgradeOpen();
                        if (applyToAllBlocks)
                            blocks.AddRange(CADBlockHelper.GetAllBlocksForName(tr, blockName));
                        else
                            blocks.Add(res.ObjectId);
                        foreach(ObjectId oid in blocks)
                        {
                            BlockReference br = tr.GetObjectWrite<BlockReference>(oid);
                            ConvertBlockText(new TransactionData(tr), br);
                        }
                        tr.Commit();
                    }
                }
            }

        }
        /// <summary>
        /// Converts attribute entities to text.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="br"></param>
        private static void ConvertBlockText(TransactionData data, BlockReference br, bool RemoveAttributes = true)
        {
            IEnumerable<Entity> entities = CADBlockHelper.GetBlockDefinitionEntities(data.Tr, br);
            Vector3d v = br.Position.GetAsVector();
            foreach(ObjectId oid in br.AttributeCollection)
            {
                AttributeReference ar = data.Tr.GetObject(oid, OpenMode.ForWrite) as AttributeReference;
                if (ar != null)
                {
                    DBText text = data.Tr.CreateEntity<DBText>(data.Btr);
                    text.TextString = ar.TextString;
                    text.Position = ar.Position;//.Add(v);
                    text.Layer = ar.Layer;
                    text.Color = ar.Color;
                    text.Height = ar.Height;
                    if (RemoveAttributes)
                    {
                        ar.TextString = "";
                    }
                }
            }
        }
    }
}
