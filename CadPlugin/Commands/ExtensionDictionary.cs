﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Commands
{
    public class ExtensionDictionary
    {
        /// <summary>
        /// Shows XData for selected entity.
        /// </summary>
        /// <param name="doc"></param>
        public static void CheckXData(Document doc)
        {
            var opt = new PromptEntityOptions("Select entity:");
            var res = doc.Editor.GetEntity(opt);
            if (res.Status == PromptStatus.OK)
            {
                var list = new List<string>();
                using (TransactionWrapper tw = doc.StartTransaction())
                {
                    var e = tw.GetObjectRead<Entity>(res.ObjectId);
                    var dbDic = tw.GetObjectRead<DBDictionary>(e.ExtensionDictionary);
                    foreach(var entry in dbDic)
                    {
                        string msg = "";
                        var obj = tw.GetObjectRead<DBObject>(entry.Value);
                        var xRec = obj as Xrecord;
                        var dic = obj as DBDictionary;
                        if (xRec != null)
                        {
                            foreach (var item in xRec.Data)
                                msg += $"[ ({item.TypeCode} ({dxfName(item.TypeCode)}) : {item.Value} ]";
                        }
                        if (dic != null)
                            msg = "[DbDictionary exists...]";
                        list.Add(msg);
                    }
                    foreach (var item in e.XData)
                    {
                        string name = dxfName(item.TypeCode);
                        list.Add($"item ({name}) : {item.Value.ToString()}");
                    }

                    // Do commit even for readonly operations.
                    tw.Commit();
                }
                doc.Editor.WriteMessage("ExtensionDictionary values for entity:\n");
                doc.Editor.WriteMessage("Handle: " + res.ObjectId.Handle.Value + "\n");
                foreach (var l in list)
                    doc.Editor.WriteMessage(l + "\n");
                doc.Editor.WriteMessage("-----------------\n");
            }
        }

        private static string dxfName(short dxf)
        {
            string s = "";
            try
            {
                s = Enum.GetName(typeof(DxfCode), dxf);
            }
            catch { }
            return s;
        }
    }
}
