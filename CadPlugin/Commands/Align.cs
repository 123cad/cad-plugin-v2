﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Commands
{
    class Align
    {
        public static void Invoke(Document document)
        {
            Editor ed = document.Editor;
            Database db = document.Database;
            ObjectId textId;
            ObjectId lineId;
            List<ObjectId> entitiesRotate = new List<ObjectId>();


            while (true)
            {
                entitiesRotate.Clear();
                PromptEntityResult res = ed.GetEntity(/*"Select block:"*/"Wähle Text oder Block!");
                while (true)
                {
                    if (res.Status == PromptStatus.OK)
                    {
                        using (Transaction tr = db.TransactionManager.StartTransaction())
                        {
                            textId = res.ObjectId;
                            DBObject obj = tr.GetObject(textId, OpenMode.ForRead);
                            if (obj as MText != null || obj as DBText != null || obj as BlockReference != null)
                            {
                                entitiesRotate.Add(textId);
                                break;
                            }
                            res = ed.GetEntity(/*"Selected entity is not block! Try again:"*/"Gewähltes Objekt ist kein Text oder Block");
                        }
                    }
                    else
                        return;
                }
                Point3d selectedPoint;
                while (true)
                {
                    res = ed.GetEntity(/*"Select another block or select line:"*/"Nächsten Text oder Linie wählen:");
                    if (res.Status == PromptStatus.OK)
                    {
                        using (Transaction tr = db.TransactionManager.StartTransaction())
                        {
                            DBObject textObj = tr.GetObject(res.ObjectId, OpenMode.ForRead);
                            DBObject obj = tr.GetObject(res.ObjectId, OpenMode.ForRead);
                            Line line = obj as Line;
                            Polyline pl = obj as Polyline;
                            Polyline3d pl3d = obj as Polyline3d;
                            if (textObj as MText != null || textObj as DBText != null || obj as BlockReference != null)
                            {
                                entitiesRotate.Add(res.ObjectId);
                            }
                            else if (line != null || pl != null || pl3d != null)
                            {
                                lineId = res.ObjectId;
                                selectedPoint = res.PickedPoint;
                                break;
                            }
                            else
                                ed.WriteMessage(/*"Selected entity is not block or line! Try again:"*/"Gewähltes Objekt ist kein Text oder Linie!");
                        }
                    }
                    else
                        return;
                }


                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                    BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                    DBObject obj = tr.GetObject(lineId, OpenMode.ForRead);
                    Line line = obj as Line;
                    double rotation = 0;
                    if (line != null)
                        rotation = line.Angle;
                    else
                    {
                        //If polyline is selected then we get arc/line segment to get an angle
                        Curve curve = obj as Curve;
                        //Polyline polyline = obj as Polyline;
                        //Polyline3d polyline3d = obj as Polyline3d;
                        if (curve != null)
                        {
                            double parameter = curve.GetParameterAtPoint(curve.GetClosestPointTo(selectedPoint, false));
                            int indexOfStartPoint = (int)parameter;
                            Point3d start = curve.GetPointAtParameter(indexOfStartPoint);
                            Point3d end = curve.GetPointAtParameter(indexOfStartPoint + 1);
                            //We rotate entities only in x-y plane, so z coordinate is set to 0
                            //Maybe it will work also with z != 0 depending on how angle is calculated - is it in xy plane or it takes z into account 
                            //(i mean it is only in xy plane so any z can be used, but i am too lazy to check and it doesn't matter) 
                            start = new Point3d(start.X, start.Y, 0);
                            end = new Point3d(end.X, end.Y, 0);
                            using (Line l = new Line(start, end))
                                rotation = l.Angle;
                            //rotation = new Line(polyline.GetPoint3dAt(indexOfStartPoint), polyline.GetPoint3dAt(indexOfStartPoint + 1)).Angle;

                            /*//If needed to align to tangent of line then we check for segment type and go on
                             * SegmentType segmentType = polyline.GetSegmentType(indexOfStartPoint);
                            if (segmentType == SegmentType.Arc)
                            {
                                CircularArc3d arc = polyline.GetArcSegmentAt(indexOfStartPoint);
                                rotation = new Line(arc.StartPoint, arc.EndPoint).Angle;

                            }
                            else if (segmentType == SegmentType.Line)
                            {
                                LineSegment3d lineSegment = polyline.GetLineSegmentAt(indexOfStartPoint);
                                rotation = new Line(lineSegment.StartPoint, lineSegment.EndPoint).Angle;
                            }
                            else
                            {
                                ed.WriteMessage("Expected segment of polyline is arc or line. Current one is: " + segmentType);
                                return;
                            }*/
                        }
                    }

                    foreach (ObjectId id in entitiesRotate)
                    {
                        DBObject dbObj = tr.GetObject(id, OpenMode.ForWrite);
                        MText mT = dbObj as MText;
                        DBText t = dbObj as DBText;
                        BlockReference br = dbObj as BlockReference;
                        Entity ent = null;
                        //Because current rotation of block can be != 0, and we get Matrix3d which adds degrees to current degrees, 
                        //we first have to set angle to 0, then to rotate to angle of line. That is done buy subtracting current rotation angle.
                        double textRotation = 0;
                        Point3d textPosition;
                        if (mT != null)
                        {
                            textRotation = mT.Rotation;
                            textPosition = mT.Location;
                            ent = mT;
                        }
                        else if (t != null)
                        {
                            textRotation = t.Rotation;
                            textPosition = t.Position;
                            ent = t;
                        }
                        else if (br != null)
                        {
                            textRotation = br.Rotation;
                            textPosition = br.Position;
                            ent = br;
                        }
                        else
                            throw new NotImplementedException("Object can't be rotated. SHOULD NEVER GET HERE.");

                        Matrix3d r = Matrix3d.Rotation(rotation - textRotation, new Vector3d(0, 0, 1), textPosition);
                        ent.TransformBy(r);
                    }

                    tr.Commit();
                }
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                    BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                    Line line = tr.GetObject(lineId, OpenMode.ForRead) as Line;


                    PromptKeywordOptions opt = new PromptKeywordOptions(/*"Type \"+\" for 180° rotation of selected blocks. And press Enter to continue, or Esc for cancel."*/
                        "Eingabe von [45, 90, 180, 270] zur ° Drehung. ENTER für Weiter or ESCAPE for Cancel!");
                    opt.Keywords.Add("45");
                    opt.Keywords.Add("90");
                    opt.Keywords.Add("180");
                    opt.Keywords.Add("270");
                    opt.AllowNone = true;
                    opt.AllowArbitraryInput = false;
                    PromptResult result = ed.GetKeywords(opt);

                    if (result.Status == PromptStatus.OK)
                    {
                        char rc = result.StringResult.Length > 0 ? result.StringResult[0] : 'a';
                        if (rc != 'a' && (rc == '4' || rc == '9' || rc == '1' || rc == '2'))
                        {
                            foreach (ObjectId id in entitiesRotate)
                            {
                                DBObject dbObj = tr.GetObject(id, OpenMode.ForWrite);
                                MText mT = dbObj as MText;
                                DBText t = dbObj as DBText;
                                BlockReference br = dbObj as BlockReference;
                                Entity ent = null;
                                //Because current rotation of block can be != 0, and we get Matrix3d which adds degrees to current degrees, 
                                //we first have to set angle to 0, then to rotate to angle of line. That is done buy subtracting current rotation angle.
                                Point3d textPosition;
                                if (mT != null)
                                {
                                    textPosition = mT.Location;
                                    ent = mT;
                                }
                                else if (t != null)
                                {
                                    textPosition = t.Position;
                                    ent = t;
                                }
                                else if (br != null)
                                {
                                    textPosition = br.Position;
                                    ent = br;
                                }
                                else
                                    throw new NotImplementedException("Object can't be rotated. SHOULD NEVER GET HERE.");
                                double angle = Math.PI;
                                if (rc == '4')
                                    angle = Math.PI / 4;
                                else if (rc == '9')
                                    angle = Math.PI / 2;
                                else if (rc == '2')
                                    angle = Math.PI * 6 / 4;
                                Matrix3d r = Matrix3d.Rotation(angle, new Vector3d(0, 0, 1), textPosition);
                                ent.TransformBy(r);
                            }
                        }
                    }
                    else
                        if (result.Status != PromptStatus.None)//None means enter is pressed. In that case we just continue. 
                        return;

                    tr.Commit();
                }
            }
        }

    }
}
