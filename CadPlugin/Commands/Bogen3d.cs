﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Commands
{
    class Bogen3d
    {
        public static void ArcTo3dPolyline(Document document)
        {
            Editor ed = document.Editor;
            Database db = document.Database;

            try
            {
                PromptEntityOptions entityOptions = new PromptEntityOptions("Wähle Anfangsbereich des Bogens");//Select start point of arc:");
                PromptEntityResult getWhichEntityResult = ed.GetEntity(entityOptions);
                bool isCanceled = getWhichEntityResult.Status == PromptStatus.Cancel;
                if (getWhichEntityResult.Status != PromptStatus.OK)
                    ThrowException(getWhichEntityResult.Status, "");
                Point3d? startArcPoint = getWhichEntityResult.PickedPoint;
                Point3d? startPoint = null;
                Point3d? endPoint = null;
                double startDistance = 0;
                double endDistance = 0;
                double lenght = 0;
                double height = 0;
                double step;//na kojem rastojanju da dodajemo liniju
                double stepHeight;//koliko da podignemo svaku liniju
                bool backwards = false;

                Polyline polyline = null;
                Arc arc = null;
                CircularArc3d arc3d = null;
                My.ObjectType workingObjectType = My.ObjectType.ARC;

                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    DBObject oid = tr.GetObject(getWhichEntityResult.ObjectId, OpenMode.ForRead);
                    if (oid as Arc != null)//Arc object
                        workingObjectType = My.ObjectType.ARC;
                    else
                        if (oid as Polyline != null)// || oid as Polyline2d != null || oid as Polyline3d != null) //kasnije dodati ovo
                        workingObjectType = My.ObjectType.POLYLINE;
                    else
                        ThrowException(PromptStatus.Error, "Bogen oder Polylinie auswählen");// Arc or polyline must be selected!");
                }

                if (workingObjectType == My.ObjectType.POLYLINE)
                {
                    using (Transaction tr1 = db.TransactionManager.StartTransaction())
                    {
                        polyline = tr1.GetObject(getWhichEntityResult.ObjectId, OpenMode.ForRead) as Polyline;
                        startArcPoint = polyline.GetClosestPointTo(startArcPoint.Value, false);
                        try
                        {
                            double parameter = polyline.GetParameterAtPoint(polyline.GetClosestPointTo(getWhichEntityResult.PickedPoint, false));
                            arc3d = polyline.GetArcSegmentAt((int)parameter);
                            startDistance = polyline.GetDistanceAtParameter((int)parameter);
                            endDistance = polyline.GetDistanceAtParameter((int)parameter + 1);
                            lenght = endDistance - startDistance;
                        }
                        catch (System.Exception)
                        {
                            ThrowException(PromptStatus.Error, "Problem mit gewählter Polylinie");//  Problem exists with selected polyline, please try again.");
                        }
                    }
                }
                else if (workingObjectType == My.ObjectType.ARC)
                {
                    using (Transaction tran = db.TransactionManager.StartTransaction())
                    {
                        arc = tran.GetObject(getWhichEntityResult.ObjectId, OpenMode.ForRead) as Arc;
                        startArcPoint = arc.GetClosestPointTo(startArcPoint.Value, false);
                        lenght = endDistance = arc.Length;
                    }
                    startDistance = 0;

                }
                else
                    ThrowException(PromptStatus.Error, "Objekt ist nicht unterstützt!");// Object is not suported!");
                //Dohvati pocetnu tacku
                //PromptPointResult startingPointOfArc = ed.GetPoint(new PromptPointOptions("Select starting point of arc (arc segment):\n"));
                //if (startingPointOfArc.Status != PromptStatus.OK)
                //ThrowException(startingPointOfArc.Status, "");
                if (workingObjectType == My.ObjectType.ARC)
                {
                    using (Transaction tran = db.TransactionManager.StartTransaction())
                    {
                        arc = tran.GetObject(getWhichEntityResult.ObjectId, OpenMode.ForRead) as Arc;
                        double distance1 = arc.GetDistAtPoint(startArcPoint.Value);// startingPointOfArc.Value.DistanceTo(arc.StartPoint);
                        double distance2 = lenght / 2 - distance1;//startingPointOfArc.Value.DistanceTo(arc.EndPoint);
                        if (distance2 < 0)
                        {
                            startArcPoint = arc.EndPoint;
                            backwards = true;
                        }
                        else
                            startArcPoint = arc.StartPoint;
                    }
                }
                else if (workingObjectType == My.ObjectType.POLYLINE)
                {
                    using (Transaction tran = db.TransactionManager.StartTransaction())
                    {
                        polyline = tran.GetObject(getWhichEntityResult.ObjectId, OpenMode.ForRead) as Polyline;
                        double distance1 = polyline.GetDistAtPoint(startArcPoint.Value) - startDistance;//startingPointOfArc.Value.DistanceTo(arc3d.StartPoint);
                        double distance2 = lenght / 2 - distance1;//startingPointOfArc.Value.DistanceTo(arc3d.EndPoint);
                        if (distance2 < 0)
                        {
                            startArcPoint = arc3d.EndPoint;
                            backwards = true;
                        }
                        else
                            startArcPoint = arc3d.StartPoint;
                    }
                }
                else
                    ThrowException(PromptStatus.Error, "Objekt ist nicht unterstützt!");//Object is not suported!");
                //dohvati krajnju tacku ili blok
                PromptEntityResult getStartObject = ed.GetEntity(new PromptEntityOptions("Wähle Anfangspunkt oder Block"));//Select start point or block:"));
                if (getStartObject.Status != PromptStatus.OK)
                    ThrowException(getStartObject.Status, "");
                using (Transaction tran = db.TransactionManager.StartTransaction())
                {
                    DBObject oid = tran.GetObject(getStartObject.ObjectId, OpenMode.ForRead);
                    if (oid as DBPoint == null && oid as BlockReference == null)
                        ThrowException(PromptStatus.Error, "Objekt ist nicht unterstützt!");//Object is not suported!");
                    if (oid as DBPoint != null)
                        startPoint = (oid as DBPoint).Position;
                    else
                        startPoint = (oid as BlockReference).Position;
                }
                PromptEntityResult getEndObject = ed.GetEntity(new PromptEntityOptions("Wähle Endpunkt oder Block"));//Select end point or block:"));
                if (getEndObject.Status != PromptStatus.OK)
                    ThrowException(getEndObject.Status, "");
                using (Transaction tran = db.TransactionManager.StartTransaction())
                {
                    DBObject oid = tran.GetObject(getEndObject.ObjectId, OpenMode.ForRead);
                    if (oid as DBPoint == null && oid as BlockReference == null)
                        ThrowException(PromptStatus.Error, "Objekt ist nicht unterstützt!");//Object is not suported!");
                    if (oid as DBPoint != null)
                        endPoint = (oid as DBPoint).Position;
                    else
                        endPoint = (oid as BlockReference).Position;
                    oid = tran.GetObject(getStartObject.ObjectId, OpenMode.ForRead);
                    if (oid as DBPoint == null && oid as BlockReference == null)
                        ThrowException(PromptStatus.Error, "Objekt ist nicht unterstützt!");//Object is not suported!");
                    if (oid as DBPoint != null)
                        startPoint = (oid as DBPoint).Position;
                    else
                        startPoint = (oid as BlockReference).Position;
                }
                PromptDoubleResult doubleResult = ed.GetDouble(new PromptDoubleOptions(" Intervall"));//Enter step:"));
                if (doubleResult.Status != PromptStatus.OK)
                    ThrowException(doubleResult.Status, "");
                step = doubleResult.Value;
                if (step < 0 || step > lenght)
                    throw new My.InvalidDataException();
                height = endPoint.Value.Z - startPoint.Value.Z;

                double temp = lenght / step;//broj tacaka
                double rest = temp - (int)temp;//ostatak
                int numberOfSteps = (int)temp + (((int)rest * 10000) != 0 ? 1 : 0);//racunanje broja tacaka pomocu ostatka
                stepHeight = height / numberOfSteps;
                double currentHeight = startPoint.Value.Z;
                double currentDistance = startDistance;
                if (backwards)
                    currentDistance = endDistance;
                using (Transaction tran = db.TransactionManager.StartTransaction())
                {
                    BlockTable bt = tran.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                    BlockTableRecord btr = tran.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

                    Polyline3d pol3d = new Polyline3d();
                    btr.AppendEntity(pol3d);
                    tran.AddNewlyCreatedDBObject(pol3d, true);
                    arc = tran.GetObject(getWhichEntityResult.ObjectId, OpenMode.ForRead) as Arc;
                    polyline = tran.GetObject(getWhichEntityResult.ObjectId, OpenMode.ForRead) as Polyline;
                    //int count = 0;//for debugging purpose
                    for (int i = 0; i <= numberOfSteps; i++)
                    {
                        Point3d? point = null;
                        if (workingObjectType == My.ObjectType.ARC)
                            point = arc.GetPointAtDist(currentDistance);
                        else if (workingObjectType == My.ObjectType.POLYLINE)
                            point = polyline.GetPointAtDist(currentDistance);

                        point = point.Value.Add(new Vector3d(0, 0, currentHeight));
                        pol3d.AppendVertex(new PolylineVertex3d(point.Value));

                        currentDistance = currentDistance + (backwards ? -1 : 1) * step;
                        currentHeight += stepHeight;
                        if (i + 1 == numberOfSteps)
                        {
                            currentDistance = endDistance;
                            if (backwards)
                                currentDistance = startDistance;
                            currentHeight = endPoint.Value.Z;
                        }
                        //FEDER

                        //if (i == numberOfSteps)
                        //{
                        //    if (count == 5)
                        //        break;
                        //    currentDistance = startDistance;
                        //    i = 0;
                        //    count++;
                        //}

                    }
                    tran.Commit();
                }
            }
            catch (My.UnsuportedOperationException e)
            {
                ed.WriteMessage(e.message + "\n");
            }
            catch (System.Exception)
            {

            }
            return;

        }
        private static void ThrowException(PromptStatus status, string message)
        {
            switch (status)
            {
                case PromptStatus.Cancel:
                    throw new My.CanceledOperationException();
                default:
                    throw new My.UnsuportedOperationException(message);
            }
        }
    }
}
