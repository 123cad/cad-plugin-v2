﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Commands.Private
{
	class KeywordTests
	{
		public static void TestKeyword()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			Database db = doc.Database;
			Editor ed = doc.Editor;

			PromptKeywordOptions opts = new PromptKeywordOptions("Message: ");
			opts.Keywords.Add("First 123");
			opts.Keywords.Add("Second");
			opts.Keywords.Default = "First 123";
			opts.AppendKeywordsToMessage = true;
			//ed.StartUserInteraction();
			//ed.GetPoint();
			opts.AllowArbitraryInput = true;

			PromptResult res = ed.GetKeywords(opts);

			System.Diagnostics.Debug.WriteLine(res.Status);
			System.Diagnostics.Debug.WriteLine(res.StringResult);

		}
	}
}
