﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Commands.Private
{
	class SolidPipeTransformTest
	{
		public static void Invoke()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			Database db = doc.Database;
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
				BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

				Solid3d s = new Solid3d();
				btr.AppendEntity(s);
				tr.AddNewlyCreatedDBObject(s, true);

				Vector3d v = new Vector3d(1, 0.8, -0.6);
				v = v.MultiplyBy(5);
				Polyline3d l = new Polyline3d();
				Point3d start = new Point3d(2, 3, 1);
				{
					btr.AppendEntity(l);
					tr.AddNewlyCreatedDBObject(l, true);
					l.AppendPoint(start);
					l.AppendPoint(start.Add(Vector3d.ZAxis));
					l.AppendPoint(start.Add(v));
					//l.StartPoint = start;
					//l.EndPoint = start.Add(v);
				}

				Circle c = new Circle();
				btr.AppendEntity(c);
				tr.AddNewlyCreatedDBObject(c, true);
				c.Center = l.StartPoint;
				c.Radius = 0.2;

				SweepOptionsBuilder sob = new SweepOptionsBuilder();
				sob.Align = SweepOptionsAlignOption.AlignSweepEntityToPath;
				sob.BasePoint = l.StartPoint;

				s.CreateSweptSolid(c, l, sob.ToSweepOptions());


				/*double height = 5;
				s.CreateFrustum(height, 1, 1, 1);


				

				CadPlugin.Geometry.Vector3d perpendicular = v.FromCADVector().CrossProduct(new CadPlugin.Geometry.Vector3d(0, 0, 1));
				CadPlugin.Geometry.Vector2d v1, v2;
				v1 = new Geometry.Vector2d(0, 1);
				v2 = new Geometry.Vector2d(v.FromCADVector().GetAs2d().Length, v.Z);

				double angle = v1.GetAngleRad(v2);

				Matrix3d m = Matrix3d.Displacement(Vector3d.ZAxis.MultiplyBy(height / 2));
				//s.TransformBy(m);
				Matrix3d rotation = m.PreMultiplyBy(Matrix3d.Rotation(angle, perpendicular.ToCADVector(), new Point3d()));

				//s.TransformBy(rotation);

				m = rotation.PreMultiplyBy(Matrix3d.Displacement(start.GetAsVector()));
				s.TransformBy(m);*/



				tr.Commit();
			}
		}
	}
}
