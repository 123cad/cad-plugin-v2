﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Commands
{
    public class XDataView
    {
        /// <summary>
        /// Shows XData for selected entity.
        /// </summary>
        /// <param name="doc"></param>
        public static void CheckXData(Document doc)
        {
            var opt = new PromptEntityOptions("Select entity:");
            var res = doc.Editor.GetEntity(opt);
            if (res.Status == PromptStatus.OK)
            {
                var list = new List<string>();
                using (TransactionWrapper tw = doc.StartTransaction())
                {
                    var e = tw.GetObjectWrite<Entity>(res.ObjectId);
                    if (e.XData == null)
                    {
                        doc.Editor.WriteMessage("XData is null.\n");
                        return;
                    }
                    foreach (var item in e.XData)
                    {
                        string name = dxfName(item.TypeCode);
                        list.Add($"item ({name}) : {item.Value.ToString()}");
                    }

                    // Do commit even for readonly operations.
                    tw.Commit();
                }
                doc.Editor.WriteMessage("XData values for entity:\n");
                doc.Editor.WriteMessage("Handle: " + res.ObjectId.Handle.Value + "\n");
                foreach (var l in list)
                    doc.Editor.WriteMessage(l + "\n");
                doc.Editor.WriteMessage("-----------------\n");
            }
        }

        private static string dxfName(short dxf)
        {
            string s = "";
            try
            {
                s = Enum.GetName(typeof(DxfCode), dxf);
            }
            catch { }
            return s;
        }
    }
}
