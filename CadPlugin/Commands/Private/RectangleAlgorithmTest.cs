﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.HatchToolbars.Data;
using geo = MyUtilities.Geometry;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Commands.Private
{
	class RectangleAlgorithmTest
	{
		public static void TestRectangle()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			Database db = doc.Database;
			Editor ed = doc.Editor;
			PromptEntityOptions opt = new PromptEntityOptions("");
			opt.SetRejectMessage("not a polyline");
			opt.AddAllowedClass(typeof(Polyline), true);
			//opt.AddAllowedClass(typeof(Polyline3d), true);
			PromptEntityResult res = ed.GetEntity(opt);
			if (res.Status == PromptStatus.OK)
			{
				using (Transaction tr = db.TransactionManager.StartTransaction())
				{
					DBObject obj = tr.GetObject(res.ObjectId, OpenMode.ForRead);
					Polyline pl = obj as Polyline;
					Polyline3d pl3d = obj as Polyline3d;
					List<Point3d> points = new List<Point3d>();
					if (pl != null)
					{
						for (int i = 0; i < pl.NumberOfVertices; i++)
							points.Add(pl.GetPoint3dAt(i));
					}
					double br = 0;
					double ho = 0;
					bool isRectangle = HoheBreiteCalculator.Calculate(ref ho, ref br, points.Select(x=>new geo.Point3d(x.X, x.Y, x.Z)).ToArray());
					ed.WriteMessage("Rectangle: " + (isRectangle ? "YES" : "NO") + "\n");
					ed.WriteMessage("hoehe: " + ho.ToString("0.00") + " bre: " + br.ToString("0.00") + "\n");
				}
			}
		}
	}
}
