﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Diagnostics;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Commands.Private
{
	/// <summary>
	/// Checking the ExtensionDictionary.
	/// </summary>
	class ExtensionDic
	{
		public static void ShowForObject(Document doc)
		{
			string dir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			string fileName = dir +  "\\extensionDic.xml";
			PromptEntityOptions opt = new PromptEntityOptions("Select entity:");
			opt.SetRejectMessage("Not an entity!");
			PromptEntityResult res = doc.Editor.GetEntity(opt);
			if (res.Status == PromptStatus.OK)
			{
				using (Transaction tr = doc.Database.TransactionManager.StartTransaction())
				{
					Entity ent = tr.GetObject(res.ObjectId, OpenMode.ForRead) as Entity;
					using (XmlTextWriter writer = new XmlTextWriter(fileName, Encoding.Unicode))
					{
						writer.Indentation = 1;
						writer.IndentChar = '\t';
						writer.Formatting = Formatting.Indented;

						writer.WriteStartDocument();
						writer.WriteStartElement("ExtensionDic");

						if (ent.ExtensionDictionary.IsNull)
						{
							writer.WriteElementString("msg", "ExtensionDitionary not found!");
						}
						else
						{
							DBDictionary dic = tr.GetObject(ent.ExtensionDictionary, OpenMode.ForRead) as DBDictionary;
							foreach(DBDictionaryEntry entry in dic)
							{
								writer.WriteStartElement(entry.Key);
								{
									if (entry.Value == ObjectId.Null)
										writer.WriteString("ObjectId.Null");
									else
									{
										Xrecord rec = tr.GetObject(entry.Value, OpenMode.ForRead) as Xrecord;
										if (rec != null && rec.Data != null)
											foreach (TypedValue val in rec.Data.AsArray())
											{
												writer.WriteStartElement("entry");
												writer.WriteAttributeString("type", val.TypeCode + ":" + ((DxfCode)val.TypeCode));
												writer.WriteString(val.Value.ToString());
												writer.WriteEndElement();
											}
									}
								}
								writer.WriteEndElement();
							}
						}

						writer.WriteEndElement();
						writer.WriteEndDocument();
					}
				}
				Process.Start(fileName);

			}
		}
	}
}
