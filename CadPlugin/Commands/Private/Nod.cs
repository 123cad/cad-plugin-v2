﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Xml;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Commands.Private
{
    class Nod
    {
		private static bool exportAll = false;
        private static bool IsPropertyDesired(string val)
        {
			if (exportAll)
				return true;
            // All numbers only are welcome (axis index "_0"...)
            int i;
            string v = string.Concat(val.Select(c => c != '_').ToArray());
            if (int.TryParse(val, out i) || int.TryParse(v, out i))
                return true;
            
            //return true;
            switch (val)
            {
                case "CHILD_DATA_RA_STATION": break;
                case "DATA": break;
                case "MAIN": break;
                case "AXIS2D": break;
                case "ELEMENT": break;
                case "GRADIENT_VERTEX": break;
                case "WIDTH_L": break;
                case "BLOCK": break;
                case "CHILD_DATA_GRAD_TANG": break;
                case "CHILD_DATA_GRAD_EDGE": break;
                case "CHILD_DATA_PROP": break;
                case "CHILD_DATA_TERR": break;
                case "SECT_DATA": break;
                case "CHILD_DATA_TERR_0": break;
                case "CHILD_DATA_TERR_1": break;
                case "MORE": break;
                case "START_POS_MARKER": break;
                case "C123_AXIS": break;
                case "C123_QSECT": break;
                case "C123_AXIS_DATA": break;
                case "C123_LSECT": break;
                case "C123_KOTE": break;
				case "0": break;
                case "1": break;
                case "2": break;
                case "3": break;
                case "4": break;
                
                default: return false;
            }
            return true;

        }

        /// <summary>
        /// Export NOD with user providing export info.
        /// </summary>
        /// <param name="tr"></param>
        /// <param name="nod"></param>
        public static void InvokeNew(Transaction tr, DBDictionary nod)
        {
            PromptKeywordOptions opts = new PromptKeywordOptions("Complete NOD?\n");
            opts.Keywords.Add("Yes");
            opts.Keywords.Default = "Yes";
            opts.AllowArbitraryInput = false;
            PromptResult res = Application.DocumentManager.MdiActiveDocument.Editor.GetKeywords(opts);
            bool b = res.StringResult.ToLower() == "yes";

            InvokeNew(tr, nod, b);
        }

        /// <summary>
        /// Export NOD without user interaction.
        /// </summary>
        /// <param name="tr"></param>
        /// <param name="nod"></param>
        /// <param name="exportAll">Export all entries or just internally predefined.</param>
        /// <param name="fileName">If null, exporting to 123CAD documents folder.</param>
        public static void InvokeNew (Transaction tr, DBDictionary nod, bool exportAll, string fileName = null)
        {
            Nod.exportAll = exportAll;
            if (fileName == null)
            {
                
                string dir = GlobalConfig.DirectoryManager.Instance.DocumentsDir; //Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                fileName = dir + "testNOD.xml";
            }

            string message = "Export partial";
			if (exportAll)
				message = "Exporting complete NOD";
			Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage(message);
            using (XmlTextWriter writer = new XmlTextWriter(fileName, Encoding.Unicode))
            {
                writer.Indentation = 1;
                writer.IndentChar = '\t';
                writer.Formatting = Formatting.Indented;

                writer.WriteStartDocument();
                writer.WriteStartElement("NamedObjectsDictionary");

                WriteXmlRec(tr, nod, writer);

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            Process.Start(fileName);
        }
        private static void WriteXmlRec(Transaction tr, DBDictionary dic, XmlWriter writer)
        {
            foreach (DBDictionaryEntry entry in dic)
            {
                if (!IsPropertyDesired(entry.Key.Trim()))
                    continue;
                string name = entry.Key;
                if (name.Length == 0 || char.IsNumber(name[0]))
                    name = "_" + name;
                name = name.Replace(' ', '_');
                writer.WriteStartElement(name);

                DBObject obj = tr.GetObject(dic.GetAt(entry.Key), OpenMode.ForRead);
                DBDictionary newDic = obj as DBDictionary;
                Xrecord rec = obj as Xrecord;
                
                if (newDic != null)
                {
                    WriteXmlRec(tr, newDic, writer);
                }
                if (rec != null && rec.Data != null)
                {
                    StringBuilder content = new StringBuilder();
                    TypedValue[] values = rec.Data.AsArray();
                    foreach (TypedValue tv in values)
                        content.Append(" { " + tv.TypeCode + " , " + tv.Value + " } ");
                    writer.WriteStartElement("Xrecord");
                    string s = content.ToString();
                    // There was a bug with surrogate character.
                    // \ is interepretated as 2 bytes character, but values were invalid.
                    s = s.Replace('\\', '/');
                    writer.WriteString(s);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
        }
        public static void InvokeOld(Document document)
        {

            // Get the current document and database, and start a transaction 
            Document acDoc = document;

            Database acCurDb = acDoc.Database;
            System.Collections.Generic.Dictionary<string, string> axed = new System.Collections.Generic.Dictionary<string, string>();
            using (Transaction acTrans = acCurDb.TransactionManager.StartTransaction())
            { // This example returns the layer table for the current database 
                DBDictionary acDicTbl;
                acDicTbl = acTrans.GetObject(acCurDb.NamedObjectsDictionaryId,
                    OpenMode.ForRead) as DBDictionary;

                //Dictionary structure extractor
                StringBuilder content = new StringBuilder();


                // Step through the NOD table and print each NOD name 
                foreach (DBDictionaryEntry acObjId in acDicTbl)
                {
                    DBDictionary childDic = acTrans.GetObject(acDicTbl.GetAt(acObjId.Key), OpenMode.ForRead) as DBDictionary;
                    content.AppendLine(acObjId.Key);
                    if (childDic != null)
                        foreach (DBDictionaryEntry objId in childDic)
                        {
                            DBObject obj = acTrans.GetObject(childDic.GetAt(objId.Key), OpenMode.ForRead);
                            DBDictionary dic = obj as DBDictionary;
                            Xrecord rec = obj as Xrecord;
                            if (dic != null)
                            {
                                content.AppendLine("\t" + objId.Key + " (D)");
                            }
                            else if (rec == null)
                            {
                                content.AppendLine("\tEmpty Line");
                            }
                            else if (IsPropertyDesired(objId.Key.Trim()))
                            {

                                content.Append("\t" + objId.Key + " (V)");
                                TypedValue[] val = rec.Data.AsArray();
                                foreach (TypedValue v in val)
                                    content.Append(" < " + v.TypeCode + "," + v.Value + " > ");
                                content.AppendLine();
                            }
                            if (dic != null)
                            {
                                foreach (DBDictionaryEntry entry in dic)
                                {
                                    DBObject obj1 = acTrans.GetObject(dic.GetAt(entry.Key), OpenMode.ForRead);
                                    DBDictionary dic1 = obj1 as DBDictionary;
                                    Xrecord rec1 = obj1 as Xrecord;
                                    if (acObjId.Key.Contains("QSECT") && (entry.Key == "SECT_DATA" || entry.Key == "MORE"))
                                    {
                                        //axed.Add(entry.Key,rec2.GetField());
                                        TypedValue[] array = rec1.Data.AsArray();
                                        //axed.Add(i++.ToString(),array[0xc].Value + " = " + array[0].Value);
                                    }
                                    if (dic1 != null)
                                    {
                                        content.AppendLine("\t\t" + entry.Key + " (D)");
                                    }
                                    else if (rec1 == null)
                                    {
                                        content.AppendLine("\t\tEmpty Line");
                                    }
                                    else if (IsPropertyDesired(entry.Key.Trim()))
                                    {
                                        //if (!objId.Key.Contains("ROW") && !objId.Key.Contains("PROP") && !objId.Key.Contains("GRAD")

                                        content.Append("\t\t" + entry.Key + " (V)");
                                        TypedValue[] val = rec1.Data.AsArray();
                                        foreach (TypedValue v in val)
                                            content.Append(" < " + v.TypeCode + "," + v.Value + " > ");
                                        content.AppendLine();
                                    }
                                    if (dic1 != null)
                                    {
                                        foreach (DBDictionaryEntry entry1 in dic1)
                                        {
                                            DBObject obj2 = acTrans.GetObject(dic1.GetAt(entry1.Key), OpenMode.ForRead);
                                            DBDictionary dic2 = obj2 as DBDictionary;
                                            Xrecord rec2 = obj2 as Xrecord;
                                            if (dic2 != null)
                                            {
                                                content.AppendLine("\t\t\t" + entry1.Key + " (D)");
                                            }
                                            else if (rec2 == null)
                                            {
                                                content.AppendLine("\t\t\tEmpty Line");
                                            }
                                            else if (IsPropertyDesired(entry1.Key.Trim()))
                                            {
                                                content.Append("\t\t\t" + entry1.Key + " (V)");
                                                TypedValue[] val = rec2.Data.AsArray();
                                                foreach (TypedValue v in val)
                                                    content.Append(" < " + v.TypeCode + "," + v.Value + " > ");
                                                content.AppendLine();
                                            }
                                            if (dic2 != null)
                                            {
                                                foreach (DBDictionaryEntry entry2 in dic2)
                                                {
                                                    DBObject obj3 = acTrans.GetObject(dic2.GetAt(entry2.Key), OpenMode.ForRead);
                                                    DBDictionary dic3 = obj3 as DBDictionary;
                                                    Xrecord rec3 = obj3 as Xrecord;
                                                    if (dic3 != null)
                                                    {
                                                        content.AppendLine("\t\t\t\t" + entry2.Key + " (D)");
                                                    }
                                                    else if (rec3 == null)
                                                    {
                                                        content.AppendLine("\t\t\t\tEmpty Line");
                                                    }
                                                    else if (IsPropertyDesired(entry2.Key.Trim()))
                                                    {
                                                        content.Append("\t\t\t\t" + entry2.Key + " (V)");
                                                        TypedValue[] val = rec3.Data.AsArray();
                                                        foreach (TypedValue v in val)
                                                            content.Append(" < " + v.TypeCode + "," + v.Value + " > ");
                                                        content.AppendLine();
                                                    }
                                                    if (dic3 != null)
                                                    {
                                                        foreach (DBDictionaryEntry entry3 in dic3)
                                                        {
                                                            DBObject obj4 = acTrans.GetObject(dic2.GetAt(entry3.Key), OpenMode.ForRead);
                                                            DBDictionary dic4 = obj4 as DBDictionary;
                                                            Xrecord rec4 = obj4 as Xrecord;
                                                            if (dic4 != null)
                                                            {
                                                                content.AppendLine("\t\t" + entry3.Key + " (D)");
                                                                //content.AppendLine("\t\t\t\t\tMore dictionaries exist!");
                                                            }
                                                            else if (rec4 == null)
                                                            {
                                                                content.AppendLine("\t\t\t\t\tEmpty Line");
                                                            }
                                                            else if (IsPropertyDesired(entry3.Key.Trim()))
                                                            {
                                                                content.Append("\t\t\t\t\t" + entry3.Key + " (V)");
                                                                TypedValue[] val = rec4.Data.AsArray();
                                                                foreach (TypedValue v in val)
                                                                    content.Append(" < " + v.TypeCode + "," + v.Value + " > ");
                                                                content.AppendLine();
                                                            }
                                                            if (dic4 != null)
                                                            {
                                                                foreach (DBDictionaryEntry entry4 in dic4)
                                                                {
                                                                    DBObject obj5 = acTrans.GetObject(dic4.GetAt(entry4.Key), OpenMode.ForRead);
                                                                    DBDictionary dic5 = obj5 as DBDictionary;
                                                                    Xrecord rec5 = obj5 as Xrecord;
                                                                    if (dic5 != null)
                                                                    {
                                                                        //content.AppendLine("\t\t" + entry5.Key + " (D)");
                                                                        content.AppendLine("\t\t\t\t\t\tMore dictionaries exist!");
                                                                    }
                                                                    else if (rec5 == null)
                                                                    {
                                                                        content.AppendLine("\t\t\t\t\t\tEmpty Line");
                                                                    }
                                                                    else if (IsPropertyDesired(entry4.Key.Trim()))
                                                                    {
                                                                        content.Append("\t\t\t\t\t\t" + entry4.Key + " (V)");
                                                                        TypedValue[] val = rec5.Data.AsArray();
                                                                        foreach (TypedValue v in val)
                                                                            content.Append(" < " + v.TypeCode + "," + v.Value + " > ");
                                                                        content.AppendLine();
                                                                    }
                                                                }

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //Xrecord rec = acTrans.GetObject

                        }
                    //Xrecord rec = childDic.GetAt
                    //LayerTableRecord acLyrTblRec;
                    //acLyrTblRec = acTrans.GetObject(acObjId,
                    //OpenMode.ForRead) as LayerTableRecord;
                    //acDoc.Editor.WriteMessage("\n" + acLyrTblRec.Name);
                }  // Dispose of the transaction
                System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
                dialog.ShowDialog();
                File.WriteAllText(dialog.FileName, content.ToString());
                Process.Start(dialog.FileName);
            }

        }

    }
}
