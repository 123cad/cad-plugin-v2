﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Commands.Private
{
    class Unclassified
    {
		public static void LoadCUI()
		{
			MyPlugin.LoadCui();
		}
        //[CommandMethod("123test1")]
        public static void tr()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;

            using (Transaction tr = Application.DocumentManager.MdiActiveDocument.TransactionManager.StartTransaction())
            {
                BlockTable bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForWrite);
                BlockTableRecord ms = (BlockTableRecord)tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite);
                Line l = new Line(new Point3d(), new Point3d(10, 10, 10));
                ObjectId id = CADLineTypeHelper.GetLineTypeId(db, "DASHEDDOT");
                l.LinetypeId = id;
                ms.AppendEntity(l);
                tr.AddNewlyCreatedDBObject(l, true);
                tr.Commit();
            }
        }
        //[CommandMethod("123test")]
        public static void test()
        {
            //if (!IsActivated("123test"))
            //return;
            Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
            Database db = Application.DocumentManager.MdiActiveDocument.Database;
            ObjectId blockReferenceId;
            List<ObjectId> blockReferences = new List<ObjectId>();



            PromptEntityResult res = ed.GetEntity(/*"Select block:"*/"Wähle Block!");
            while (true)
            {
                if (res.Status == PromptStatus.OK)
                {
                    using (Transaction tr = db.TransactionManager.StartTransaction())
                    {
                        blockReferenceId = res.ObjectId;
                        BlockReference br = tr.GetObject(blockReferenceId, OpenMode.ForWrite) as BlockReference;
                        if (br != null)
                        {
                            blockReferences.Add(blockReferenceId);
                            Matrix3d m = Matrix3d.Rotation(-br.Rotation, new Vector3d(0, 0, 1), br.Position);
                            br.TransformBy(m);
                            tr.Commit();
                            break;
                        }
                        res = ed.GetEntity(/*"Selected entity is not block! Try again:"*/"Gewähltes Objekt ist kein Block");
                    }
                }
                else
                    return;
            }
        }
        //[CommandMethod("123BLOCKROTATE")]
        public static void BlockRotate()
        {
            //if (!IsCommandActivated("123BLOCKROTATE"))
                //return;
            Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
            Database db = Application.DocumentManager.MdiActiveDocument.Database;
            ObjectId blockReferenceId;
            ObjectId lineId;
            List<ObjectId> blockReferences = new List<ObjectId>();


            while (true)
            {
                blockReferences.Clear();
                PromptEntityResult res = ed.GetEntity(/*"Select block:"*/"Wähle Block!");
                while (true)
                {
                    if (res.Status == PromptStatus.OK)
                    {
                        using (Transaction tr = db.TransactionManager.StartTransaction())
                        {
                            blockReferenceId = res.ObjectId;
                            if (tr.GetObject(blockReferenceId, OpenMode.ForRead) as BlockReference != null)
                            {
                                blockReferences.Add(blockReferenceId);
                                break;
                            }
                            res = ed.GetEntity(/*"Selected entity is not block! Try again:"*/"Gewähltes Objekt ist kein Block");
                        }
                    }
                    else
                        return;
                }
                Point3d selectedPoint;
                while (true)
                {
                    res = ed.GetEntity(/*"Select another block or select line:"*/"Nächsten Block oder Linie wählen:");
                    if (res.Status == PromptStatus.OK)
                    {
                        using (Transaction tr = db.TransactionManager.StartTransaction())
                        {
                            BlockReference bRef = tr.GetObject(res.ObjectId, OpenMode.ForRead) as BlockReference;
                            DBObject obj = tr.GetObject(res.ObjectId, OpenMode.ForRead);
                            Line line = obj as Line;
                            Polyline pl = obj as Polyline;
                            if (bRef != null)
                            {
                                blockReferences.Add(res.ObjectId);
                            }
                            else if (line != null || pl != null)
                            {
                                lineId = res.ObjectId;
                                selectedPoint = res.PickedPoint;
                                break;
                            }
                            else
                                ed.WriteMessage(/*"Selected entity is not block or line! Try again:"*/"Gewähltes Objekt ist kein Block oder Linie!");
                        }
                    }
                    else
                        return;
                }


                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                    BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                    DBObject obj = tr.GetObject(lineId, OpenMode.ForRead);
                    Line line = obj as Line;
                    double rotation = 0;
                    if (line != null)
                        rotation = line.Angle;
                    else
                    {
                        //If polyline is selected then we get arc/line segment to get an angle
                        Polyline polyline = obj as Polyline;
                        double parameter = polyline.GetParameterAtPoint(polyline.GetClosestPointTo(selectedPoint, false));
                        int indexOfStartPoint = (int)parameter;
                        SegmentType segmentType = polyline.GetSegmentType(indexOfStartPoint);
                        if (segmentType == SegmentType.Arc)
                        {
                            CircularArc3d arc = polyline.GetArcSegmentAt(indexOfStartPoint);
                            rotation = new Line(arc.StartPoint, arc.EndPoint).Angle;

                        }
                        else if (segmentType == SegmentType.Line)
                        {
                            LineSegment3d lineSegment = polyline.GetLineSegmentAt(indexOfStartPoint);
                            rotation = new Line(lineSegment.StartPoint, lineSegment.EndPoint).Angle;
                        }
                        else
                        {
                            ed.WriteMessage("Expected segment of polyline is arc or line. Current one is: " + segmentType);
                            return;
                        }
                    }

                    foreach (ObjectId id in blockReferences)
                    {
                        BlockReference blockRef = tr.GetObject(id, OpenMode.ForWrite) as BlockReference;
                        //Because current rotation of block can be != 0, and we get Matrix3d which adds degrees to current degrees, 
                        //we first have to set angle to 0, then to rotate to angle of line. That is done buy subtracting current rotation angle.
                        Matrix3d r = Matrix3d.Rotation(rotation - blockRef.Rotation, new Vector3d(0, 0, 1), blockRef.Position);
                        blockRef.TransformBy(r);
                    }

                    tr.Commit();
                }
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                    BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;


                    PromptKeywordOptions opt = new PromptKeywordOptions(/*"Type \"+\" for 180° rotation of selected blocks. And press Enter to continue, or Esc for cancel."*/
                        "Eingabe von [+] zur 180° Drehung. ENTER für Weiter or ESCAPE for Cancel!");
                    opt.Keywords.Add("+");
                    opt.AllowNone = true;
                    opt.AllowArbitraryInput = false;
                    PromptResult result = ed.GetKeywords(opt);

                    if (result.Status == PromptStatus.OK)
                    {
                        if (result.StringResult.Length > 0 && result.StringResult[0] == '+')
                        {
                            foreach (ObjectId id in blockReferences)
                            {
                                BlockReference blockRef = tr.GetObject(id, OpenMode.ForWrite) as BlockReference;
                                Matrix3d r = Matrix3d.Rotation(Math.PI, new Vector3d(0, 0, 1), blockRef.Position);
                                blockRef.TransformBy(r);
                            }
                        }
                    }
                    else
                        if (result.Status != PromptStatus.None)//None means enter is pressed. In that case we just continue. 
                        return;

                    tr.Commit();
                }
            }
        }
        //[CommandMethod("123miladintest")]
        public static void testMiladin()
        {
            Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
            Database db = Application.DocumentManager.MdiActiveDocument.Database;
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                PromptEntityResult res = ed.GetEntity(/*"Select block:"*/"Wähle Text!");
                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                DBObject line = tr.GetObject(res.ObjectId, OpenMode.ForRead);
                MText mText = line as MText;
                bool b = mText.SupportsCollection("a");
                if (b)
                    return;
            }
        }
        //[CommandMethod("123TXTROTATE")]
        public static void TextRotate()
        {
            //if (!IsActivated("123TXTROTATE"))
            //return;
            Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
            Database db = Application.DocumentManager.MdiActiveDocument.Database;
            ObjectId textId;
            ObjectId lineId;
            List<ObjectId> textBlock = new List<ObjectId>();


            while (true)
            {
                textBlock.Clear();
                PromptEntityResult res = ed.GetEntity(/*"Select block:"*/"Wähle Text!");
                while (true)
                {
                    if (res.Status == PromptStatus.OK)
                    {
                        using (Transaction tr = db.TransactionManager.StartTransaction())
                        {
                            textId = res.ObjectId;
                            DBObject obj = tr.GetObject(textId, OpenMode.ForRead);
                            if (obj as MText != null || obj as DBText != null)
                            {
                                textBlock.Add(textId);
                                break;
                            }
                            res = ed.GetEntity(/*"Selected entity is not block! Try again:"*/"Gewähltes Objekt ist kein Text");
                        }
                    }
                    else
                        return;
                }
                Point3d selectedPoint;
                while (true)
                {
                    res = ed.GetEntity(/*"Select another block or select line:"*/"Nächsten Text oder Linie wählen:");
                    if (res.Status == PromptStatus.OK)
                    {
                        using (Transaction tr = db.TransactionManager.StartTransaction())
                        {
                            DBObject textObj = tr.GetObject(res.ObjectId, OpenMode.ForRead);
                            DBObject obj = tr.GetObject(res.ObjectId, OpenMode.ForRead);
                            Line line = obj as Line;
                            Polyline pl = obj as Polyline;
                            if (textObj as MText != null || textObj as DBText != null)
                            {
                                textBlock.Add(res.ObjectId);
                            }
                            else if (line != null || pl != null)
                            {
                                lineId = res.ObjectId;
                                selectedPoint = res.PickedPoint;
                                break;
                            }
                            else
                                ed.WriteMessage(/*"Selected entity is not block or line! Try again:"*/"Gewähltes Objekt ist kein Text oder Linie!");
                        }
                    }
                    else
                        return;
                }


                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                    BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                    DBObject obj = tr.GetObject(lineId, OpenMode.ForRead);
                    Line line = obj as Line;
                    double rotation = 0;
                    if (line != null)
                        rotation = line.Angle;
                    else
                    {
                        //If polyline is selected then we get arc/line segment to get an angle
                        Polyline polyline = obj as Polyline;
                        double parameter = polyline.GetParameterAtPoint(polyline.GetClosestPointTo(selectedPoint, false));
                        int indexOfStartPoint = (int)parameter;
                        SegmentType segmentType = polyline.GetSegmentType(indexOfStartPoint);
                        if (segmentType == SegmentType.Arc)
                        {
                            CircularArc3d arc = polyline.GetArcSegmentAt(indexOfStartPoint);
                            rotation = new Line(arc.StartPoint, arc.EndPoint).Angle;

                        }
                        else if (segmentType == SegmentType.Line)
                        {
                            LineSegment3d lineSegment = polyline.GetLineSegmentAt(indexOfStartPoint);
                            rotation = new Line(lineSegment.StartPoint, lineSegment.EndPoint).Angle;
                        }
                        else
                        {
                            ed.WriteMessage("Expected segment of polyline is arc or line. Current one is: " + segmentType);
                            return;
                        }
                    }

                    foreach (ObjectId id in textBlock)
                    {
                        MText mT = tr.GetObject(id, OpenMode.ForWrite) as MText;
                        DBText t = tr.GetObject(id, OpenMode.ForWrite) as DBText;
                        Entity ent = mT != null ? (Entity)mT : (Entity)t;
                        //Because current rotation of block can be != 0, and we get Matrix3d which adds degrees to current degrees, 
                        //we first have to set angle to 0, then to rotate to angle of line. That is done buy subtracting current rotation angle.
                        double textRotation = mT != null ? mT.Rotation : t.Rotation;
                        Point3d textPosition = mT != null ? mT.Location : t.Position;
                        Matrix3d r = Matrix3d.Rotation(rotation - textRotation, new Vector3d(0, 0, 1), textPosition);
                        ent.TransformBy(r);
                    }

                    tr.Commit();
                }
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                    BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                    Line line = tr.GetObject(lineId, OpenMode.ForRead) as Line;


                    PromptKeywordOptions opt = new PromptKeywordOptions(/*"Type \"+\" for 180° rotation of selected blocks. And press Enter to continue, or Esc for cancel."*/
                        "Eingabe von [+] zur 180° Drehung. ENTER für Weiter or ESCAPE for Cancel!");
                    opt.Keywords.Add("+");
                    opt.AllowNone = true;
                    opt.AllowArbitraryInput = false;
                    PromptResult result = ed.GetKeywords(opt);

                    if (result.Status == PromptStatus.OK)
                    {
                        if (result.StringResult.Length > 0 && result.StringResult[0] == '+')
                        {
                            foreach (ObjectId id in textBlock)
                            {
                                MText mT = tr.GetObject(id, OpenMode.ForWrite) as MText;
                                DBText t = tr.GetObject(id, OpenMode.ForWrite) as DBText;
                                Point3d textPosition = mT != null ? mT.Location : t.Position;
                                Matrix3d r = Matrix3d.Rotation(Math.PI, new Vector3d(0, 0, 1), textPosition);
                                Entity ent = mT != null ? (Entity)mT : (Entity)t;
                                ent.TransformBy(r);
                            }
                        }
                    }
                    else
                        if (result.Status != PromptStatus.None)//None means enter is pressed. In that case we just continue. 
                        return;

                    tr.Commit();
                }
            }
        }
    }
}
