﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Commands
{
    public static class BlockRotationCommand
    {
        public static void Run(Document doc)
        {
            // Select BlockReference.
            var opts = new PromptEntityOptions("Select a Block");
            opts.SetRejectMessage("Not a BlockReference!");
            opts.AddAllowedClass(typeof(BlockReference), true);
            opts.AllowNone = true;
            var res = doc.Editor.GetEntity(opts);
            if (res.Status != PromptStatus.OK)
            {
                doc.Editor.WriteMessage("Canceled");
                return;
            }

            BlockRotations.BlockRotateViewModel.ShowWindow(doc, res.ObjectId);

            // Start View
            // View can rotate

        }
    }
}
