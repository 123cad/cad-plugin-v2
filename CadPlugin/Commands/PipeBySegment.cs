﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using CadPlugin.Commands.PipeBySegments;
using CadPlugin.PipeEntitiesJigs;
using CadPlugin.Sewage.Create;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Commands
{
	class PipeBySegment
	{
		private class PointMonitor
		{
			internal Point3d CurrentPoint;
			internal void PointMonitorEvent(object sender, PointMonitorEventArgs e)
			{
				CurrentPoint = new Point3d(e.Context.ComputedPoint.X, e.Context.ComputedPoint.Y, e.Context.ComputedPoint.Z);
				//System.Diagnostics.Debug.WriteLine(e.Context.ComputedPoint);
			}
		}

		/// <summary>
		/// Last time selected water direction.
		/// </summary>
		private static WaterDirection LastUsed = WaterDirection.Downstream;

		internal static void CreateShaftPipe(Document doc)
		{
			PromptKeywordOptions opts = new PromptKeywordOptions("Water direction ");
			string[] keys = { "Downstream", "Upstream" };
			string def = LastUsed == WaterDirection.Downstream ? keys[0] : keys[1];
			opts.Keywords.Add(keys[0], keys[0]);
			opts.Keywords.Add(keys[1], keys[1]);
			opts.Keywords.Default = def;

			PromptResult res = doc.Editor.GetKeywords(opts);
			if (res.Status != PromptStatus.OK)
				return;

			WaterDirection dir = WaterDirection.Downstream;
			if (res.StringResult == keys[1])
				dir = WaterDirection.Upstream;
			LastUsed = dir;
			CreatePipe.Start(doc, dir);
		}



		private static bool GetSegmentLength(Editor ed, ref double length)
		{
			PromptDoubleOptions doubleOpt = new PromptDoubleOptions("Type segment length:");
			PromptDoubleResult res = ed.GetDouble(doubleOpt);
			length = res.Value;
			return res.Status == PromptStatus.OK;
		}
		private static bool GetDiameter(Editor ed, ref double diameter)
		{
			PromptDoubleOptions doubleOpt = new PromptDoubleOptions("Type shaft radius:");
			doubleOpt.DefaultValue = 1;
			PromptDoubleResult res = ed.GetDouble(doubleOpt);
			diameter = res.Value;
			// Diameter is returned.
			diameter *= 2;
			if (diameter == 0)
				return false;
			return res.Status == PromptStatus.OK;
		}
		private static bool GetSlope(Editor ed, ref double slope, bool slopeExists = false)
		{
			PromptDoubleOptions doubleOpt = new PromptDoubleOptions("Type slope in % " + (slopeExists ? " (current slope: " + DoubleHelper.ToStringInvariant(slope, 2) + ")" : "") + ":");
			if (slopeExists)
				doubleOpt.DefaultValue = slope;
			PromptDoubleResult res = ed.GetDouble(doubleOpt);
			if (res.Status != PromptStatus.OK)
				return false;
			slope = res.Value / 100.0;
			return true;
		}
		
	}
}
