﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Commands
{
    class BlockExport
    {
        public static void Invoke(Document document)
        {
            Editor ed = document.Editor;
            Database db = document.Database;

            BlockReference block;

            PromptEntityOptions entityOptions = new PromptEntityOptions("Block wählen");//Select block reference:\n");
            PromptEntityResult result;// = ed.GetEntity(entityOptions);
            Dictionary<string, int> columns = new Dictionary<string, int>();//posto neki atributi mogu da ne postoje u blockreference
            //ja treba da postavim samo one koji postoje. Zato ne idemo sekvencijalno nego dodajemo atirbute po indeksu
            string name = "";// block.BlockName;
            int i = 0;//indeks prvog slobodnog mesta za kolonu
            //dodajemo kolone dinamicki, onaj red koji nema kolonu ili je vrednost prazan string dobija tacku
            while (true)
            {
                result = ed.GetEntity(entityOptions);
                if (result.Status != PromptStatus.OK)
                    return;
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    DBObject obj = tr.GetObject(result.ObjectId, OpenMode.ForRead);
                    block = obj as BlockReference;
                    if (block == null)
                    {
                        ed.WriteMessage("Gewähltes Objekt ist keine Block!");//  Selected object is not block reference! Please try again.");
                    }
                    else
                    {
                        columns.Add("x", i++);
                        columns.Add("y", i++);
                        columns.Add("z", i++);
                        columns.Add("Name", i++);
                        name = block.Name;

                        break;
                    }
                }
            }
            //lista koja predstavlja listu blokova. 
            List<List<object>> table = new List<List<object>>();
            List<ObjectId> usedBlocks = new List<ObjectId>();
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                BlockTable blockTable = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord modelSpace = tr.GetObject(blockTable[BlockTableRecord.ModelSpace], OpenMode.ForRead) as BlockTableRecord;

                // dve iteracije
                // 1 - dohvata objectId svih blokova koje koristimo i sve atribute
                // 2 - popunjava tabelu sa podacima

                foreach (ObjectId oId in modelSpace)
                {
                    DBObject obj1 = tr.GetObject(oId, OpenMode.ForRead);
                    BlockReference reference = obj1 as BlockReference;
                    if (reference == null)
                        continue;
                    if (reference.Name == name)
                    {
                        usedBlocks.Add(oId);
                        foreach (ObjectId oid in reference.AttributeCollection)
                        {
                            DBObject temp = tr.GetObject(oid, OpenMode.ForRead);
                            AttributeReference atr = temp as AttributeReference;
                            if (!columns.ContainsKey(atr.Tag))
                            {
                                columns.Add(atr.Tag, i);
                                i++;
                            }
                        }
                    }
                }
                // 2 iteracija dohvatamo podatke
                //kreiramo tabelu(uz postavljanje podrazumevanih vrednosti) pa je posle popunjavamo
                table = new List<List<object>>();
                int columnCount = columns.Count;
                int rowCount = usedBlocks.Count;
                for (int j = 0; j < rowCount; j++)
                {
                    List<object> row = new List<object>(columnCount);
                    for (int k = 0; k < columnCount; k++)
                        row.Add("");//CHECK stavljamo tacku ako je element prazan
                    table.Add(row);
                }
                int currentRow = 0;
                //idemo block po block, uzimamo podatke i popunjavamo red po red u tabeli
                foreach (ObjectId oId in usedBlocks)
                {
                    DBObject obj1 = tr.GetObject(oId, OpenMode.ForRead);
                    BlockReference reference = obj1 as BlockReference;
                    //dohvata po redosledu zadatom u prethodnom bloku - 
                    List<object> row = table[currentRow];// new List<object>(columns.Count);
                    row[0] = reference.Position.X;
                    row[1] = reference.Position.Y;
                    row[2] = reference.Position.Z;
                    row[3] = reference.Name;
                    //pristupamo atributu po indeksu
                    foreach (ObjectId oid in reference.AttributeCollection)
                    {
                        DBObject temp = tr.GetObject(oid, OpenMode.ForRead);
                        AttributeReference atr = temp as AttributeReference;
                        //ako je prazan string onda ostaje tacka
                        if (atr.TextString != "")
                        {
                            //prebacujemo vrednost uint double ili int ako moze. bitno je da prvo ide int, jer svaka vrednost moze da ide u double
                            double valueD = 0;
                            int valueInt = 0;
                            try
                            {
                                valueInt = int.Parse(atr.TextString);
                                row[columns[atr.Tag]] = valueInt;
                            }
                            catch (System.Exception)
                            {
                                try
                                {
                                    valueD = DoubleHelper.ParseInvariantDouble(atr.TextString);// double.Parse(atr.TextString);
                                    row[columns[atr.Tag]] = valueD;
                                }
                                catch (System.Exception)
                                {
                                    row[columns[atr.Tag]] = atr.TextString;
                                }
                            }
                        }
                    }
                    currentRow++;
                }
            }
            List<string> columnNames = new List<string>();
            foreach (string s in columns.Keys)
                columnNames.Add(s);
            BlockExportView.Program.StartExporter(columnNames, table);
            ed.WriteMessage("Befehl erfolgreich beendet");//Block export finished.");
        }

    }
}
