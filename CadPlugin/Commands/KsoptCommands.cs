﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkKS.CADModels;
using NetworkKS.CADModels.Layouts;
using NetworkKS.Networks;
using NetworkKS.Controllers;
using NetworkKS.Calculations;
using NetworkKS.CADModels.MainPathCreator;
using Ninject.Parameters;
using MyExceptions;
using NetworkKS.CADModels.Views.MainWindow.KSoptModels;
using NetworkKS.Isybau;
using Microsoft.Win32;
using NetworkKS.CADModels.NamedObjectsDictionary.LSRows;
using NetworkKS.CADModels.LSection;
using wf = System.Windows.Forms;
using SharedUtilities.GlobalDependencyInjection;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Commands
{
	class KsoptCommands
	{
		private static Document doc => Application.DocumentManager.MdiActiveDocument;
		public static void CommandKSOPT_LoadIsybau()
		{
			var dlg = new OpenFileDialog();
			dlg.Filter = "XML files (*.xml)|*.xml";
			dlg.CheckFileExists = true;
			if (dlg.ShowDialog() == true)
			{
				var network = new IsybauImport().LoadNetworkFromFile(dlg.FileName);

				DwgDataManager mgr = new DwgDataManager(doc);

				mgr.AssignNetworkId(network);
				network.Name = "Network " + network.Id;

				PromptStringOptions opts = new PromptStringOptions("Type network name:");
				opts.DefaultValue = network.Name;
				opts.AllowSpaces = true;
				PromptResult res = doc.Editor.GetString(opts);
				if (res.Status == PromptStatus.OK)
				{
					network.Name = res.StringResult;
				}

				mgr.SaveNetwork(network);

				//DrawNetwork(doc, network, mgr);
				new KSoptController().EditSelected(doc, network.Id);
			}
		}
		public static void CommandKSOPT_CreatePath()
		{
			DwgDataManager mgr = new DwgDataManager(doc);
			PathCalculation calc = new PathCalculation();
			NetworkController controller = new NetworkController(mgr, calc);
			var network = controller.CreateNewNetwork(GlobalDI.Get<ICreateMainPath>(new ConstructorArgument("doc", doc)));
			if (network == null)
				return;
			DrawNetwork(doc, network, mgr);
		}
		private static void DrawNetwork(Document doc, SewageNetwork network, DwgDataManager dataMgr)
		{
			PromptKeywordOptions opts = new PromptKeywordOptions("Select layout:");
			opts.Keywords.Add("2d");
			opts.Keywords.Add("3d");
			opts.Keywords.Add("v3d");
			opts.Keywords.Default = opts.Keywords[1].LocalName;
			opts.AllowArbitraryInput = false;
			PromptResult res = doc.Editor.GetKeywords(opts);
			if (res.Status != PromptStatus.OK)
				return;
			LayoutModes modes = LayoutModes.Layout3d;
			if (res.StringResult.ToLower() == "2d")
				modes = LayoutModes.Layout2d;
			if (res.StringResult.ToLower() == "v3d")
				modes = LayoutModes.Layout3dVisual;

			dataMgr.SaveNetwork(network);
			dataMgr.SetNetworkVisible(network, new DrawingSettings() { LayoutDraw = modes });
			doc.Editor.ZoomExtents();
		}
		public static void CommandKSOPT_SaveIsybau()
		{
			DwgDataManager mgr = new DwgDataManager(doc);
			if (!mgr.NetworksInfo.Any())
			{
				doc.Editor.WriteMessage("No networks exists!");
				return;
			}
			mgr.LoadNetwork(mgr.NetworksInfo.First().Id);
			SewageNetwork network = mgr.GetNetwork(mgr.NetworksInfo.First().Id);

			var dlg = new System.Windows.Forms.SaveFileDialog();
			dlg.Filter = "XML files (*.xml)|*.xml";
			dlg.OverwritePrompt = true;
			if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				try
				{
					var ident = new NetworkKS.Isybau.IsybauExport().FromNetwork(network);
					Isybau2015.IsybauSave.SaveToXMLFile(dlg.FileName, ident);
				}
				catch (IsybauException e)
				{
					System.Windows.Forms.MessageBox.Show("Unable to export!" + e.ToStringData());
				}
			}
		}
		public static void StartKSoptNew(Document doc)
		{
			//new KSopt.CADModels.Views.MainWindow.Views.KSoptView().ShowDialog();
			//new ShaftPointWPF().ShowDialog();
			new KSoptController().CreateNew(doc);

		}
		public static void StartKSoptNewFromBlocks(Document doc)
        {
			new KSoptController().CreateNewFromBlocks(doc);
        }
		public static void StartKSoptEdit(Document doc)
		{
			//new KSopt.CADModels.Views.MainWindow.Views.KSoptView().ShowDialog();
			//new ShaftPointWPF().ShowDialog();
			new KSoptController().EditExisting(doc);
		}

		/// <summary>
		/// Refreshes all LS that are connected to a SN.
		/// </summary>
		/// <param name="network"></param>
		public static void RefreshAllLS()
		{
			//DwgDataManager mgr;
			// All networks referenced by all LS sections in the drawing (networkId, terrainName).
			//var networkIds = new[] { new { LsIndex = -1, NetworkId = (short)0, TerrainName = "" } }.ToList();
			//networkIds.Clear();

			/*using (var ls = new C123InteropWrapper.COM123CADWrapper())
			{
				ls.Sect.BeginUpdate();
				bool selectLS = ls.Sect.IsLSSelected() != 0;
				int currentLS = -1;
				if (selectLS)
					currentLS = ls.Sect.Index;

				IEnumerable<int> existingLsIds = ls.Dwg.DoesLSectExist() > 0 ? ls.Dwg.GetLSects() : Enumerable.Empty<int>();
				//IEnumerable<int> existingLsIds = ls.Dwg.GetLSects();
				foreach (var lsId in existingLsIds)
				{
					ls.Sect.SetActiveLSect((short)lsId);
					short networkId = -1;
					string terrainName = "";
					int lsIndex = ls.Sect.Index;
					string data = ls.Sect.GetNetworkDataForLS(lsIndex);
					if (!LsDataConverter.TryParse(data, ref networkId, ref terrainName))
					{
						if (string.IsNullOrEmpty(data))
							MyLog.MyTraceSource.Warning($"For current LS {lsIndex} sewage data not found.");
						else
							MyLog.MyTraceSource.Warning($"For current LS {lsIndex} error parsing data <{data}>");
						return;
					}
					networkIds.Add(new { LsIndex = lsId, NetworkId = networkId, TerrainName = terrainName });
					//networkIds.Add(new Tuple<int, string>(networkId, terrainName));
				}
				if (selectLS)
					ls.Sect.SetActiveLSect((short)currentLS);

				mgr = new DwgDataManager(doc);
				HashSet<int> processedNetworks = new HashSet<int>();
				foreach (var t in networkIds)
				{
					// RefreshLS updates all related LS - SN is enough to apply just once.
					if (processedNetworks.Contains(t.NetworkId))
						continue;
					processedNetworks.Add(t.NetworkId);
					int networkId = t.NetworkId;
					var info = mgr.NetworksInfo?.FirstOrDefault(x => x.Id == networkId);

					if (info == null)
					{
						MyLog.MyTraceSource.Error($"Network with id {networkId}, which is used in LS {t.LsIndex} can't be found!");
						wf.MessageBox.Show($"Network with id {networkId}, which is used in LS {t.LsIndex} can't be found!", "", wf.MessageBoxButtons.OK, wf.MessageBoxIcon.Error);
						continue;
					}
					mgr.LoadNetwork(networkId);
					LSManager.RefreshLS(doc, mgr.GetNetwork(networkId));
				}
				ls.Sect.EndUpdate();
			}*/
			// Refresh all existing networks.
			// This allows to delete remaining rows, when LS is deleted using LSOPT.
            //mgr = new DwgDataManager(doc);
            //foreach (var n in mgr.NetworksInfo)
            //{
            //    int networkId = n.Id;
            //    mgr.LoadNetwork(networkId);
            //    LSManager.RefreshLS(doc, mgr.GetNetwork(networkId));
            //}
            new KSoptController().RefreshAllLS(doc);
        }



	}
}
