﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Commands
{
    /// <summary>
    /// Test drawing from benchmark (not related to other structures, simple draw 3d pipes/shafts).
    /// </summary>
    public class SewageElementsStandaloneDrawer
    {
        /// <summary>
        /// Draws pipe (with connection pipes) in 3d, in disconnected scenario (standalone).
        /// </summary>
        /// <param name="doc"></param>
        public static void CreatePipe(Document doc)
        {
            double pipeWall = 0.05;
            double pipeWallConnection = 0.05;
            doc.Editor.WriteMessage($"INFO: Pipe wall thickness is {(int)(pipeWall * 100)}cm. \n"
                //+ $"Connection pipe wall thickness is {(int)(pipeWall * 100)}cm"
                );
            ObjectId mainPipe;
            List<ObjectId> connectionPipes = new List<ObjectId>();
            PromptEntityOptions opts = new PromptEntityOptions("Select main polyline");
            opts.SetRejectMessage("Not a polyline!");
            opts.AddAllowedClass(typeof(Polyline), true);
            PromptEntityResult res = doc.Editor.GetEntity(opts);
            if (res.Status != PromptStatus.OK)
                return;
            mainPipe = res.ObjectId;

            opts = new PromptEntityOptions("Select connection pipe polyline: ");
            while (true)
            {
                opts.AllowNone = true;
                res = doc.Editor.GetEntity(opts);
                if (res.Status == PromptStatus.None)
                    break;
                if (res.Status != PromptStatus.OK)
                    return;
                connectionPipes.Add(res.ObjectId);
            }

            PromptDoubleOptions optsDouble = new PromptDoubleOptions("Get main diameter (min 0.2m):");
            optsDouble.DefaultValue = 0.6;
            PromptDoubleResult resDouble = doc.Editor.GetDouble(optsDouble);
            if (resDouble.Status != PromptStatus.OK)
                return;
            double mainDm = resDouble.Value;
            double connDm = mainDm;
            if (connectionPipes.Any())
            {
                optsDouble = new PromptDoubleOptions("Get connection diameter (min 0.2m): ");
                optsDouble.DefaultValue = 0.4;
                resDouble = doc.Editor.GetDouble(optsDouble);
                if (resDouble.Status != PromptStatus.OK)
                    return;
                connDm = resDouble.Value;
            }


            using (TransactionWrapper tw = doc.StartTransaction())
            {
                Polyline main = tw.GetObjectRead<Polyline>(mainPipe);
                List<Polyline> conns = new List<Polyline>();
                foreach (var id in connectionPipes)
                    conns.Add(tw.GetObjectRead<Polyline>(id));

                Solid3d mainSolid = CADSolid3dHelper.CreateCylinderAlongPath(tw, main, mainDm);// CreateSolidPipe(tw, main, mainDm);
                AppendConnectionRing(tw, mainSolid, main, mainDm);
                AppendSealRing(tw, mainSolid, main, mainDm);

                List<Solid3d> connSolids = new List<Solid3d>();
                foreach (Polyline l in conns)
                {
                    var sl = CreateSolidPipe(tw, l, connDm);
                    connSolids.Add(sl);
                    AppendConnectionRing(tw, sl, l, connDm);
                    AppendSealRing(tw, sl, l, connDm);
                }

                var mainSolidInner = CreateSolidPipe(tw, main, mainDm - pipeWall * 2);
                List<Solid3d> connSolidsInner = new List<Solid3d>();

                foreach (Polyline l in conns)
                {
                    var sl = CreateSolidPipe(tw, l, connDm - pipeWallConnection * 2);
                    connSolidsInner.Add(sl);
                }

                // Make solid of main and connection pipes.
                foreach (var cs in connSolids)
                {
                    mainSolid.BooleanOperation(BooleanOperationType.BoolUnite, cs);
                }
                foreach (var vs in connSolidsInner)
                {
                    mainSolid.BooleanOperation(BooleanOperationType.BoolSubtract, vs);
                }
                mainSolid.BooleanOperation(BooleanOperationType.BoolSubtract, mainSolidInner);


                // Do commit even for readonly operations.
                tw.Commit();
            }

            Solid3d CreateSolidPipe(TransactionWrapper tw, Polyline l, double diameter)
            {
                Solid3d solid = CADSolid3dHelper.CreateCylinderAlongPath(tw, l, diameter);
                return solid;
            }
            void AppendConnectionRing(TransactionWrapper tw, Solid3d solid, Polyline l, double diameter)
            {
                using (Polyline l1 = new Polyline())
                {
                    double distance = l.GetDistanceAtParameter(l.NumberOfVertices - 1);
                    Point3d p1 = l.GetPointAtDist(distance - 0.05);
                    Point3d p2 = l.GetPointAtDist(distance);

                    Vector3d v = p1.GetVectorTo(p2);// l.StartPoint.GetVectorTo(l.EndPoint);
                    p2 = p2.Add(v);
                    l1.AddPoints(p1.To2d(), p2.To2d());
                    //l1.StartPoint = p1;// l.EndPoint.Add(v.GetNormal().Negate().MultiplyBy(0.05));
                    //l1.EndPoint = p2;// l.EndPoint.Add(v.GetNormal().MultiplyBy(0.05));
                    l1.Elevation = p1.Z;
                    Solid3d ring = CreateSolidPipe(tw, l1, diameter + 0.05);
                    Solid3d ringInner = CreateSolidPipe(tw, l1, diameter);
                    ring.BooleanOperation(BooleanOperationType.BoolSubtract, ringInner);
                    solid.BooleanOperation(BooleanOperationType.BoolUnite, ring);
                }
            }
            void AppendSealRing(TransactionWrapper tw, Solid3d solid, Polyline l, double diameter)
            {
                using (Polyline l1 = new Polyline())
                {
                    // For arc there is a problem. Because when subtract inner from main ring, some very small surface remains
                    // (because of radius), and this causes the problem.
                    Point3d p1 = l.GetPointAtDist(0);
                    Point3d p2 = l.GetPointAtDist(0.05);
                    Vector3d v = p1.GetVectorTo(p2);// l.StartPoint.GetVectorTo(l.EndPoint);
                    Point3d p0 = p1.Add(v.Negate().GetNormal().MultiplyBy(0.02));
                    Point3d p3 = p2.Add(v.GetNormal().Negate().MultiplyBy(0.01));
                    l1.AddPoints(p1.To2d(), p2.To2d());
                    l1.Elevation = p1.Z;
                    //l1.StartPoint = p1;// l.StartPoint.Add(v.GetNormal().MultiplyBy(0.00));
                    //l1.EndPoint = p2;// l.StartPoint.Add(v.GetNormal().MultiplyBy(0.03));
                    Solid3d ring = CreateSolidPipe(tw, l1, diameter + 0.04);
                    l1.ReplacePoints(p0.To2d(), p3.To2d());
                    Solid3d ringInner = CreateSolidPipe(tw, l1, diameter);
                    bool keep = false;
                    if (keep)
                    {
                        string layer = "b";
                        CADLayerHelper.AddLayerIfDoesntExist(tw.Db, layer);
                        ring.Layer = layer;
                        ringInner.Layer = layer;
                        tw.MS.AppendEntity(ring);
                        tw.MS.AppendEntity(ringInner);
                        tw.Tr.AddNewlyCreatedDBObject(ring, true);
                        tw.Tr.AddNewlyCreatedDBObject(ringInner, true);
                    }
                    else
                    {
                        //ring.BooleanOperation(BooleanOperationType.BoolSubtract, ringInner);
                        solid.BooleanOperation(BooleanOperationType.BoolUnite, ring);
                        solid.BooleanOperation(BooleanOperationType.BoolSubtract, ringInner);
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="doc"></param>
        public static void CreateShaft(Document doc)
        {
            PromptPointOptions opts = new PromptPointOptions("Select point:");
            PromptPointResult res = doc.Editor.GetPoint(opts);
            if (res.Status != PromptStatus.OK)
                return;
            Point3d center = res.Value;

            double r = 3;
            double rO = r * 1.6;
            double a = r * 1.1;
            double cutR = r * 5;
            double sphereR = 1.33 * r * 2;
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                Line l = tw.CreateEntity<Line>();
                l.StartPoint = new Point3d();
                l.EndPoint = new Point3d(0, 0, 5);



                Solid3d shaftObject = CADSolid3dHelper.CreateCylinderAlignedTo(tw, l.StartPoint.FromCADPoint(),
                                                                                l.EndPoint.FromCADPoint(),
                                                                                rO);
                Solid3d innerCircle = CADSolid3dHelper.CreateCylinderAlignedTo(tw, l.StartPoint.FromCADPoint(),
                                                                                l.EndPoint.FromCADPoint(),
                                                                                r);

                Solid3d cutCircle = CADSolid3dHelper.CreateCylinderAlignedTo(tw, l.StartPoint.FromCADPoint(),
                                                                                l.EndPoint.FromCADPoint(),
                                                                                cutR);
                shaftObject.BooleanOperation(BooleanOperationType.BoolSubtract, innerCircle);


                double offset = cutR / 2 + r / 2 * 1.2;
                //offset = offset / 2 + r * 0.2;

                Vector3d move = new Vector3d(offset, 0, 0);
                cutCircle.TransformBy(Matrix3d.Displacement(move));
                shaftObject.BooleanOperation(BooleanOperationType.BoolSubtract, cutCircle);

                move = new Vector3d(0, offset, 0);
                cutCircle = CADSolid3dHelper.CreateCylinderAlignedTo(tw, l.StartPoint.FromCADPoint(),
                                                                                l.EndPoint.FromCADPoint(),
                                                                                cutR);
                cutCircle.TransformBy(Matrix3d.Displacement(move));
                shaftObject.BooleanOperation(BooleanOperationType.BoolSubtract, cutCircle);

                move = new Vector3d(-offset, 0, 0);
                cutCircle = CADSolid3dHelper.CreateCylinderAlignedTo(tw, l.StartPoint.FromCADPoint(),
                                                                                 l.EndPoint.FromCADPoint(),
                                                                                 cutR);
                cutCircle.TransformBy(Matrix3d.Displacement(move));
                shaftObject.BooleanOperation(BooleanOperationType.BoolSubtract, cutCircle);

                move = new Vector3d(0, -offset, 0);
                cutCircle = CADSolid3dHelper.CreateCylinderAlignedTo(tw, l.StartPoint.FromCADPoint(),
                                                                                 l.EndPoint.FromCADPoint(),
                                                                                 cutR);
                cutCircle.TransformBy(Matrix3d.Displacement(move));
                shaftObject.BooleanOperation(BooleanOperationType.BoolSubtract, cutCircle);


                Solid3d sphere = tw.CreateEntity<Solid3d>();
                sphere.CreateSphere(sphereR / 2);

                sphere.TransformBy(Matrix3d.Displacement(new Vector3d(0, 0, l.EndPoint.Z - sphereR / 2)));

                shaftObject.BooleanOperation(BooleanOperationType.BoolIntersect, sphere);

                double topZ = shaftObject.GeometricExtents.MaxPoint.Z;
                Point3d topPt = new Point3d(0, 0, topZ);
                Solid3d top = CADSolid3dHelper.CreateCylinderAlignedTo(tw, topPt.Add(new Vector3d(0, 0, -0.08)).FromCADPoint(),
                                                                        topPt.FromCADPoint(), 1.5 * r);

                shaftObject.BooleanOperation(BooleanOperationType.BoolSubtract, top);


                Solid3d innerPipe = CADSolid3dHelper.CreateCylinderAlignedTo(tw, new MyUtilities.Geometry.Point3d(0, 0, 3),
                    new MyUtilities.Geometry.Point3d(2, 0, 3), 1);

                shaftObject.BooleanOperation(BooleanOperationType.BoolSubtract, innerPipe);


                topPt = new Point3d(0, 0, shaftObject.GeometricExtents.MaxPoint.Z);
                Solid3d topRing = CADSolid3dHelper.CreateCylinderAlignedTo(tw, topPt.FromCADPoint(),
                    topPt.FromCADPoint().Add(new MyUtilities.Geometry.Vector3d(0, 0, 0.1)),
                    r + 0.1);

                innerCircle = CADSolid3dHelper.CreateCylinderAlignedTo(tw, topPt.FromCADPoint(), topPt.Add(new Vector3d(0, 0, 2)).FromCADPoint(), r);

                topRing.BooleanOperation(BooleanOperationType.BoolSubtract, innerCircle);

                shaftObject.BooleanOperation(BooleanOperationType.BoolUnite, topRing);

                //innerCircle.Erase();
                //cutCircle.Erase();

                Solid3d bottom = CADSolid3dHelper.CreateCylinderAlignedTo(tw, new MyUtilities.Geometry.Point3d(),
                                                                new MyUtilities.Geometry.Point3d(0, 0, 1.5),
                                                                r);
                shaftObject.BooleanOperation(BooleanOperationType.BoolUnite, bottom);

                Solid3d bottomPipe = CADSolid3dHelper.CreateCylinderAlignedTo(tw,
                                    new MyUtilities.Geometry.Point3d(-2, 0, 1.5),
                                    new MyUtilities.Geometry.Point3d(2, 0, 1.5),
                                    1);
                shaftObject.BooleanOperation(BooleanOperationType.BoolSubtract, bottomPipe);
                List<Entity> entitiesToMove = new List<Entity>();
                entitiesToMove.Add(shaftObject);
                DBPoint pt = tw.CreateEntity<DBPoint>();
                pt.Position = new Point3d(r / 2 * 1.2, 0, 1.5);
                entitiesToMove.Add(pt);
                pt = tw.CreateEntity<DBPoint>();
                pt.Position = new Point3d(-r / 2 * 1.2, 0, 1.5);
                entitiesToMove.Add(pt);

                var helix = CADHelixHelper.DrawHelix(tw, new Point3d(-1 * r, -r / 2, 1.5), new Point3d(0, -r / 2, 1.5), -1);
                helix.Turns = 1;
                Solid3d bottomArc = CADSolid3dHelper.CreateCylinderAlongHelix(tw, helix, 1);
                helix.Erase();

                pt = tw.CreateEntity<DBPoint>();
                pt.Position = new Point3d(0, -r / 2 * 1.2, 1.5);
                entitiesToMove.Add(pt);

                shaftObject.BooleanOperation(BooleanOperationType.BoolSubtract, bottomArc);



                Matrix3d reposition = Matrix3d.Displacement(new Point3d().GetVectorTo(center));
                foreach (var e in entitiesToMove)
                    e.TransformBy(reposition);

                // Do commit even for readonly operations.
                tw.Commit();
            }

        }
    }
}
