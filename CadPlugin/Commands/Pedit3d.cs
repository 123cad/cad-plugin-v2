﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif


namespace CadPlugin.Commands
{
    class Pedit3d
    {
        public static void Invoke()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;
            Database db = doc.Database;

            //Select objects.
            List<ObjectId> pls = selectObjects(doc);

            if (pls == null)
                return;

            //Delete entities used to create 3dpolyline?
            string yes = "Ja";
            string no = "Nein";
            //string yes = "Yes";
            //string no = "No";

            //PromptKeywordOptions opt = new PromptKeywordOptions("Delete selected entities?");
            PromptKeywordOptions opt = new PromptKeywordOptions("Löschen des Ausgangs-Objekt: ");
            opt.Keywords.Add(yes);
            opt.Keywords.Add(no);
            opt.AllowNone = false;
            opt.AllowArbitraryInput = false;
            PromptResult res1 = ed.GetKeywords(opt);
            bool delete = false;
            if (res1.Status != PromptStatus.OK)
                return;
            delete = res1.StringResult == yes;

            //Choose layer name (leave blank for current layer)
            string layerName = null;//null means default layer
            PromptStringOptions layerNameOpt = new PromptStringOptions("Layernamen definieren oder <Keine Angabe für Aktuellen Layer>:");
            PromptResult layerNameResult = ed.GetString(layerNameOpt);
            if (layerNameResult.Status != PromptStatus.OK)
                return;
            if (layerNameResult.StringResult.Trim() != "")
                layerName = layerNameResult.StringResult.Trim();

            //Get curves from ObjectIds in Selection (and add layer if doesn't exist)
            List<VerticesHandler> curves = new List<VerticesHandler>();
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForRead) as BlockTableRecord;

                foreach (ObjectId id in pls)
                {
                    Curve c = tr.GetObject(id, OpenMode.ForRead) as Curve;
                    curves.Add(new VerticesHandler(c));
                }
                if (layerName != null)
                {
                    LayerTable layerTable = tr.GetObject(db.LayerTableId, OpenMode.ForWrite) as LayerTable;
                    if (!layerTable.Has(layerName))
                    {
                        LayerTableRecord ltr = new LayerTableRecord();
                        ltr.Name = layerName;
                        layerTable.Add(ltr);
                        tr.AddNewlyCreatedDBObject(ltr, true);
                        tr.Commit();
                    }
                }
            }
            //Run the algorithm which calculates points for 3d polylines.
            List<VerticesHandler> result = run(curves);
            //Add newly created 3d polylines.
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

                foreach (VerticesHandler h in result)
                {
                    Polyline3d p3d = new Polyline3d();
                    p3d.SetDatabaseDefaults();
                    if (layerName != null)
                        p3d.Layer = layerName;
                    btr.AppendEntity(p3d);
                    tr.AddNewlyCreatedDBObject(p3d, true);

                    // Not disposed vertex?
                    Point3d? last = null;
                    foreach (Point3d p in h.Points)
                    {
                        if (last != null)
                        {
                            if (last.Value.GetVectorTo(p).Length < 0.0001)
                                continue;
                        }
                        var vertex = new PolylineVertex3d(p);
                        p3d.AppendVertex(vertex);
                        vertex.Dispose();
                        last = p;
                    }
                }
                if (delete)
                    foreach (var v in pls)
                    {
                        DBObject obj = tr.GetObject(v, OpenMode.ForWrite);
                        obj.Erase(true);
                    }

                tr.Commit();
            }
            //ed.WriteMessage("Created: " + result.Count + " 3d polyline(s)." + (delete ? " Deleted: " + pls.Count + " entities\n" : ""));
            ed.WriteMessage("Erstellt: " + result.Count + " 3d Polylinie(n)." + (delete ? " Gelöscht: " + pls.Count + " entities\n" : ""));
        }
        /// <summary>
        /// Returns objects that need to be processed (ignores all objects 
        /// which are not Line, Polyline or Polyline3d.
        /// </summary>
        private static List<ObjectId> selectObjects(Document doc)
        {
            //doc.Editor.WriteMessage("Select Line, Polyline or 3d Polyline (other entities will be ignored):\n");
            doc.Editor.WriteMessage("Linie, Polylinie oder 3D-Polylinie wählen:\n");
            PromptSelectionOptions ps = new PromptSelectionOptions();
            ps.AllowDuplicates = false;
            //ps.MessageForAdding = "Select next entity:";
            ps.MessageForAdding = "Objekte wählen: ";

            TypedValue[] tvs = new TypedValue[]
            {
                new TypedValue((int)DxfCode.Operator,"<or"),
                new TypedValue((int)DxfCode.Start, "LINE"),
                new TypedValue((int)DxfCode.Start, "*POLYLINE*"),
                /*
                    new TypedValue(Convert.ToInt32(DxfCode.Start), "POLYLINE"),
                    new TypedValue(Convert.ToInt32(DxfCode.Start), "LWPOLYLINE"),
                    new TypedValue(Convert.ToInt32(DxfCode.Start), "POLYLINE2D"),
                    new TypedValue(Convert.ToInt32(DxfCode.Start), "POLYLINE3d"), 
                */
                new TypedValue((int)DxfCode.Operator,"or>")
            };
            SelectionFilter filter = new SelectionFilter(tvs);

            //Allows user to select multiple entities (to SelectionSet), using filter.
            //Filter filter selected objects, so only allowed ones can make it to SelectionSet
            PromptSelectionResult res = doc.Editor.GetSelection(ps, filter);
            if (res.Status == PromptStatus.OK)
            {
                //doc.Editor.WriteMessage("Selected: " + res.Value.Count + " entities.\n");
                doc.Editor.WriteMessage("Objekte im Satz: " + res.Value.Count + "\n");
                List<ObjectId> ids = new List<ObjectId>();
                ObjectId[] idsArray = res.Value.GetObjectIds();
                ids.AddRange(idsArray);
                return ids;
            }
            return null;
        }
        private static List<VerticesHandler> run(List<VerticesHandler> vertices)
        {
            List<VerticesHandler> completed = new List<VerticesHandler>();

            VerticesHandler[] ts = vertices.ToArray();
            VerticesHandler current = ts[0];
            if (vertices.Count == 1)
                completed.Add(current);
            current.IterationStart();
            int currentIndex = 0;
            int count = ts.Length;
            for (int i = 1; i < count; i++)
            {
                VerticesHandler t = ts[i];
                //In ts used VerticesHandler is marked as null, so here iteration lasts until end
                //of array is reached or first not null entry is found.
                while (t == null)
                {
                    i++;
                    if (i == count)
                        break;
                    t = ts[i];
                }
                if (t != null)
                {
                    bool pr = current.IsPreviousToStartPoint(t);
                    bool nx = current.IsNextToEndPoint(t);
                    if (pr)
                        current.AddPrevious(t);
                    if (nx)
                        current.AddNext(t);
                    if (pr || nx)
                        ts[i] = null;
                }
                bool end = false;
                if (i + 1 >= count)
                {
                    current.IterationEnd();
                    if (current.Completed)
                    {
                        completed.Add(current);
                        while (true)
                        {
                            if (currentIndex + 1 >= count)
                            {
                                end = true;
                                break;
                            }
                            current = ts[++currentIndex];
                            if (current != null)
                                break;
                        }
                        if (end)
                            break;
                    }
                    current.IterationStart();
                    i = currentIndex;
                    //If current index is last index in array, loop is finished
                    if (i + 1 >= count)
                    {
                        current.IterationEnd();
                        //Nothing is added because iteration is started and ended instantly
                        completed.Add(current);
                    }
                }
            }

            return completed;
        }

    }
}
