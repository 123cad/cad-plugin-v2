﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using Inventory.CAD.GeometryAssignment.Data;
using System.Diagnostics;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Commands
{
    class InventoryCommands
    {
        /// <summary>
        /// When Inventory running, this document 
        /// </summary>
        private static Document current = null;
        public static void StartCommand(Document doc)
        {
            // Force new Application is opened, when user double click on .dwg file.
            // (this is needed because while inventory is opened, user can choose .dwg file. If SDI not set,
            // dwg will be opened in the same instance, but program is blocked because inventory is opened)
            /* In 2012 autocad blog announced that SDI mode is deprecated and will be removed in some of the future versions. 
			 for now it works. 11.01.2019. */
            object old = Application.GetSystemVariable("SDI");
            try
            {
                if (Inventory.RunFacade.ActiveForm != null)
                {
                    Inventory.RunFacade.ActiveForm.Visible = true;
                    Inventory.RunFacade.ActiveForm.BringToFront();
                    return;
                }
                current = doc;
                Application.SetSystemVariable("SDI", 1);

                Action<System.Windows.Forms.Form> inventoryFinished = (form) =>
                {
					// Hide Inventary form (since it does not have any impact anymore).
					form.Visible = false;
                    using (DocumentLock l = doc.LockDocument())
                    {
                        if (Inventory.RunFacade.PerformAction == Inventory.InventoryAction.DrawData)
                            Inventories.InventoryManager.DrawData(doc, Inventory.RunFacade.CadDrawingData);
						doc.SendStringToExecute("._REGENALL\n", false, false, false);
						//doc.Editor.Regen();
                    }
                    current = null;
                };
                System.Windows.Forms.Form f = Inventory.RunFacade.PrepareForm(AssignGeometry);
                f.FormClosed += (_, __) => inventoryFinished((System.Windows.Forms.Form)_);
                //f.Show();
                //Application.ShowModelessDialog(f);
                try
                {
                    f.ShowDialog();
                }
                catch(System.Exception e)
                {
                    MyLog.MyTraceSource.Error("Inventory crashed. " + e);
                    f.Dispose();
					System.Windows.Forms.MessageBox.Show("Error happended which prevented commmand from finishing.\n Check log for details.", "123CAD Error");
				}
                //Application.ShowModalDialog(f);
                //Inventory.RunFacade.Start(inventoryFinished, AssignGeometry); 

            }
            catch(System.Exception e)
            {
				MyLog.MyTraceSource.Error($"Error while running inventary module. {e.Message}");
															 // In case of unhandled exception finish current command.
				Application.SetSystemVariable("SDI", old);
                current = null;
				doc.SendStringToExecute("._REGENALL \n", false, false, false);
				System.Windows.Forms.MessageBox.Show("Error with inventary! Form closed.", "",System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
			}
		}
		[Conditional("TEST")]
		public static void GetDrawingDataFromInventory(Document doc, ref Inventory.CAD.DisplayData data)
		{
			try
			{
				System.Windows.Forms.Form f = Inventory.RunFacade.PrepareForm();
				f.ShowDialog();
			}
			catch
			{
				System.Windows.Forms.MessageBox.Show("Testing inventory failed!");
			}
			data = Inventory.RunFacade.CadDrawingData;
		}
		private static void AssignGeometry(DataManagerEventArgs ab)
        {
            // Inventory can run on a different document than current one.
            using (Inventories.GeometryAssignment.CadManager mgr = new Inventories.GeometryAssignment.CadManager(current))
            {
                // Start GeometryAssignment form.
                Inventories.GeometryAssignment.View.MainForm f = new Inventories.GeometryAssignment.View.MainForm(mgr);
                f.Initialize(ab);

                Inventory.RunFacade.ActiveForm.Visible = false;
                f.FormClosed += (_, __) =>
                {
                    Inventory.RunFacade.ActiveForm.Visible = true;
                    Inventory.RunFacade.ActiveForm.BringToFront();
                };
                f.VisibleChanged += (_, e) =>
                {
                    if (!f.Visible)
                        current.Window.Focus();
                };
#if BRICSCAD
				//f.Show(Application.MainWindow);
				//f.ShowDialog();
				//Application.ShowModelessDialog(f);
				f.ShowDialog(Application.MainWindow);
				//toolbar.ShowDialog( Application.MainWindow);
				//toolbar.ShowDialog(Application.MainWindow);
				//Application.ShowModelessDialog(Application.MainWindow, toolbar, false);
				//Application.ShowModalDialog(toolbar); 


				//Application.ShowModelessDialog(Application.MainWindow, toolbar);
#endif
#if AUTOCAD

            //Application.ShowModalDialog(Application.MainWindow.Handle, f);
            //Application.ShowModelessDialog(Application.MainWindow.Handle, f);
            //f.Show();
            //Inventory.RunFacade.ActiveForm.Visible = true;
            //return ;

            //Application.ShowModelessDialog(Application.MainWindow.Handle, f);
            //f.Show();
            //f.ShowDialog();
            Application.ShowModalDialog(Application.MainWindow.Handle, f);
            //f.ShowDialog(Application.MainWindow.Handle);
#endif
				f.Dispose();
            }
            
        }
        public static void StartCommand(Document doc, Inventory.CAD.CadToInventories.Datas.DataManager mgr)
        {
            object old = Application.GetSystemVariable("SDI");
            Application.SetSystemVariable("SDI", 1);
            Inventory.RunFacade.Start(mgr);
            if (Inventory.RunFacade.PerformAction == Inventory.InventoryAction.DrawData)
				Inventories.InventoryManager.DrawData(doc, Inventory.RunFacade.CadDrawingData);
            Application.SetSystemVariable("SDI", old);
        }
    }
}
