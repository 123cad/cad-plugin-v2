﻿using System;
using System.Text;
using System.Diagnostics;
using System.Reflection;
using System.Collections.Generic;
using System.IO;
//using System.Windows.Forms;
using Size = System.Drawing.Size;

using WindowsForms = System.Windows.Forms;
using Format066Holder;
using CadPlugin.Common;
using System.Linq;
using CadPlugin.Profilers;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
using StyleNamespace = Teigha.GraphicsInterface;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
using StyleNamespace = Autodesk.AutoCAD.GraphicsInterface;
#endif


namespace CadPlugin.Commands
{
    class _Profiler
    {
        public static void Invoke(Document document)
        {
            Document doc = document;
            Database db = doc.Database;
            Dictionary<int, Tuple<string, double>> axesNames = new Dictionary<int, Tuple<string, double>>();//used so we don't get axis name and distance from database every time

            Dictionary<int, ObjectId> stationQsectIds = new Dictionary<int, ObjectId>();//we use this so can connect stations in drawing format and profiler format. saved is objectId of qsect, but it also can be handle or name

            AxesGroup ag = new AxesGroup();
            using (Transaction tran = db.TransactionManager.StartTransaction())
            {
                DBDictionary nod = tran.GetObject(db.NamedObjectsDictionaryId, OpenMode.ForRead) as DBDictionary;
                DBDictionary qsects = null;
                if (nod.Contains("C123_QSECT"))
                    qsects = tran.GetObject(nod.GetAt("C123_QSECT"), OpenMode.ForRead) as DBDictionary;
                if (qsects == null || qsects.Count == 0)
                {
                    /*System.Windows.MessageBox.Show("Drawing contains no profiles!", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                    tran.Abort();
                    return;*/
                }
                BlockTable blockTable = tran.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tran.GetObject(blockTable[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

                //List<string> messages = new List<string>();

                if (qsects != null)
                    foreach (DBDictionaryEntry entry in qsects)
                    {
                        int res;
                        if (!int.TryParse(entry.Key, out res))
                            continue;
                        stationQsectIds.Add(res, entry.Value);
                        DBDictionary qsect = tran.GetObject(entry.Value, OpenMode.ForRead) as DBDictionary;
                        //u qsect is current profile? , we need to extract data
                        double distance;//of station
                        string name;
                        int index = res;
                        double height;
                        List<Point3d> gl = null;
                        List<Point3d> cotr = null;
                        int axisIndex;//axis id. can we find axis name?
                        Point3d cp;//center point of profile
                                   //Line3d cl;
                        double dx;
                        TypedValue[] values;
                        double startDistanceOfAxis = 0;
                        //get start distance of station
                        {

                        }
                        Xrecord rec = tran.GetObject(qsect.GetAt("MORE"), OpenMode.ForRead) as Xrecord;
                        values = rec.Data.AsArray();
                        dx = (double)values[0].Value;
                        distance = (double)values[1].Value;
                        name = (string)values[2].Value;

                        #region For testing
                        /*if (distance > temp)
                        {
                            PromptEntityOptions ent = new PromptEntityOptions("Select line");
                            PromptEntityResult rest = doc.Editor.GetEntity(ent);
                            Handle h;
                            string han = "";
                            if (rest.Status == PromptStatus.OK)
                            {
                                h = ((Entity)tran.GetObject(rest.ObjectId, OpenMode.ForRead)).Handle;
                                han = h.ToString();
                            }
                            foreach (DBDictionaryEntry en1 in qsect)
                            {
                                DBObject obj = tran.GetObject(qsect.GetAt(en1.Key), OpenMode.ForRead);
                                DBDictionary dic = obj as DBDictionary;
                                Xrecord rec1 = obj as Xrecord;
                                bool temp1 = false;
                                if (temp1)
                                {
                                    string val1 = (string)rec1.Data.AsArray()[0].Value;
                                    string handleName1 = (string)rec1.Data.AsArray()[0].Value;
                                    long handleValue1 = long.Parse(handleName1, System.Globalization.NumberStyles.AllowHexSpecifier);
                                    Entity e = tran.GetObject(db.GetObjectId(false, new Handle(handleValue1), 0), OpenMode.ForRead) as Entity;
                                    temp1 = true;
                                }
                            }
                        }*/
                        #endregion
                        rec = tran.GetObject(qsect.GetAt("SECT_DATA"), OpenMode.ForRead) as Xrecord;
                        values = rec.Data.AsArray();
                        axisIndex = (short)values[0].Value;
                        height = (double)values[2].Value;
                        //messages.Add("Axis index: " + axisIndex + " , distance: " + distance.ToString("0:00"));
                        //continue;
                        //get axis name
                        string axisName = "";
                        if (!axesNames.ContainsKey(axisIndex))
                        {
                            DBDictionary axesDic = tran.GetObject(nod.GetAt("C123_AXIS"), OpenMode.ForRead) as DBDictionary;
                            DBDictionary axisDic = tran.GetObject(axesDic.GetAt(axisIndex.ToString()), OpenMode.ForRead) as DBDictionary;
                            Xrecord axisData = tran.GetObject(axisDic.GetAt("DATA"), OpenMode.ForRead) as Xrecord;
                            DBDictionary axesInfoDic = tran.GetObject(nod.GetAt("C123_AXIS_DATA"), OpenMode.ForRead) as DBDictionary;
                            DBDictionary axisInfoDic = tran.GetObject(axesInfoDic.GetAt(axisIndex.ToString()), OpenMode.ForRead) as DBDictionary;
                            Xrecord axisInfo = tran.GetObject(axisInfoDic.GetAt("AXIS2D"), OpenMode.ForRead) as Xrecord;
                            startDistanceOfAxis = (double)axisInfo.Data.AsArray()[0].Value;
                            axesNames.Add(axisIndex, new Tuple<string, double>((string)axisData.Data.AsArray()[2].Value, startDistanceOfAxis));
                        }
                        distance += axesNames[axisIndex].Item2;
                        axisName = axesNames[axisIndex].Item1;


                        rec = tran.GetObject(qsect.GetAt("START_POS_MARKER"), OpenMode.ForRead) as Xrecord;
                        string handleName = (string)rec.Data.AsArray()[0].Value;
                        long handleValue = long.Parse(handleName, System.Globalization.NumberStyles.AllowHexSpecifier);
                        Circle c = tran.GetObject(db.GetObjectId(false, new Handle(handleValue), 0), OpenMode.ForRead) as Circle;
                        cp = new Point3d(c.Center.X, c.Center.Y, c.Center.Z);

                        string cotrName = "";
                        if (qsect.Contains("GRAD_0_ROAD_POLY"))
                        {
                            rec = tran.GetObject(qsect.GetAt("GRAD_0_ROAD_POLY"), OpenMode.ForRead) as Xrecord;
                            //Autodesk.AutoCAD.GraphicsInterface.Polyline pl;
                            handleName = (string)rec.Data.AsArray()[0].Value;
                            handleValue = long.Parse(handleName, System.Globalization.NumberStyles.AllowHexSpecifier);
                            Polyline pl = tran.GetObject(db.GetObjectId(false, new Handle(handleValue), 0), OpenMode.ForRead) as Polyline;
                            cotr = new List<Point3d>();
                            for (int i = 0; i < pl.NumberOfVertices; i++)
                                cotr.Add(pl.GetPoint3dAt(i));
                            //get name of levelline. because we assume gl is on grad_0_poly, i can assume that levelline name is on  CHILD_DATA_GRAD_0 
                            /*rec = tran.GetObject(qsect.GetAt("CHILD_DATA_GRAD_0 "), OpenMode.ForRead) as Xrecord;
                            cotrName = (string)rec.Data.AsArray()[6].Value;*/
                        }
                        string glName = "ground";
                        if (qsect.Contains("TERR_0_POLY"))
                        {
                            rec = tran.GetObject(qsect.GetAt("TERR_0_POLY"), OpenMode.ForRead) as Xrecord;
                            Polyline pl;
                            handleName = (string)rec.Data.AsArray()[0].Value;
                            handleValue = long.Parse(handleName, System.Globalization.NumberStyles.AllowHexSpecifier);
                            pl = tran.GetObject(db.GetObjectId(false, new Handle(handleValue), 0), OpenMode.ForRead) as Polyline;
                            gl = new List<Point3d>();
                            for (int i = 0; i < pl.NumberOfVertices; i++)
                                gl.Add(pl.GetPoint3dAt(i));
                            //get name of levelline. because we assume gl is on terr_0_poly, i can assume that levelline name is on  CHILD_DATA_TERR_0 
                            DBDictionary dic = tran.GetObject(qsect.GetAt("CHILD_DATA_TERR_0"), OpenMode.ForRead) as DBDictionary;
                            rec = tran.GetObject(dic.GetAt("DATA"), OpenMode.ForRead) as Xrecord;
                            glName = (string)rec.Data.AsArray()[6].Value;//TEST - null exception
                        }
                        ProfileData pd = new ProfileData(distance, name, index, height, gl, glName, cotr, cotrName, cp, dx);
                        ag.AddAxis(axisIndex, axisName);
                        ag.GetAxis(axisIndex).AddProfile(pd);

                        //adding test line from center to the right
                        /*Point3d p1 = new Point3d(cp.X + Math.Abs(dx), cp.Y, 0);
                        Point3d p2 = new Point3d(cp.X + Math.Abs(dx) + 15, cp.Y + 15, 0);
                        Line l = new Line(p1, p2);
                        btr.AppendEntity(l);
                        tran.AddNewlyCreatedDBObject(l, true);*/
                    }
                ag.FinishImport();
                tran.Commit();
            }
            //int groundlevel = CustomSettings.GroundLevelIndex;
            //int crownOfTheRoad = CustomSettings.CrownOfTheRoadIndex;

            //string content = "";// ImportExport066.SaveFile(ag.GetStationsForExport(), ref groundlevel, ref crownOfTheRoad);
            string nameFile = document.Name;
            nameFile = nameFile.Substring(nameFile.LastIndexOf("\\") + 1);

            AxesHolder axes = CadProfilerDataAdapter.CADInterfaceToProfilerInterface(ag);
#if BRICSCAD
            Profiler.ProgramType type = Profiler.ProgramType.BRICSCAD;
#endif
#if AUTOCAD
            Profiler.ProgramType type = Profiler.ProgramType.AUTOCAD;
#endif
            if (!Profiler.Program.TestStartProfilerExternally(nameFile, axes, type))
                return;//operation canceled

            Dictionary<int, Dictionary<int, string>> labelData = new Dictionary<int, Dictionary<int, string>>();//all axes with their levellines are here

            //gl and cotr are not updated. we use level number from profiler.settings 
            int groundLevel = Profiler.Settings.AppSettings.GroundlevelDefaultLevel;
            int cotrLevel = Profiler.Settings.AppSettings.CrownOfTheRoadDefaultLevel;

            CadProfilerDataAdapter.UpdateAxesFromProfiler(axes, ag);
            DrawReturnedDataToDrawing(document, stationQsectIds, ag, labelData, groundLevel, cotrLevel);

            //RefreshLabels();

            //NOTE for now polyline can't be deleted ???
        }

        private static void DrawReturnedDataToDrawing(Document doc, Dictionary<int, ObjectId> stationQsectIds, AxesGroup ag, Dictionary<int, Dictionary<int, string>> labelData, int groundLevel, int cotrLevel)
        {
            AxisEntities entities = new AxisEntities();
            //iterate through all stations in each axis
            //we get qsect objectId for station from dictionary
            //get profile of current station, and add/update data 
            //dont update gl and cotr
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                //BlockTable bt = tw.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                //BlockTableRecord btr = tw.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

                //iterate through all axes
                foreach (int index in ag.GetAxesIndexes())
                {
                    labelData.Add(index, new Dictionary<int, string>());
                    SingleAxis axis = ag[index];
                    foreach (ProfileData data in axis.GetAllProfiles())
                    {
                        ObjectId qsectID = stationQsectIds[data.Index];
                        DBDictionary qsect = tw.GetObjectWrite<DBDictionary>(qsectID);
                        DBDictionary qsectProfilerEntry;
                        string profilerPolylineDictionary = "PROFILER_POLYLINES";
                        if (qsect.Contains(profilerPolylineDictionary))
                            qsectProfilerEntry = tw.GetObjectWrite<DBDictionary>(qsect.GetAt(profilerPolylineDictionary));// tw.GetObject(qsect.GetAt(profilerPolylineDictionary), OpenMode.ForWrite) as DBDictionary;
                        else
                        {
                            qsectProfilerEntry = new DBDictionary();
                            qsect.SetAt(profilerPolylineDictionary, qsectProfilerEntry);
                            tw.Tr.AddNewlyCreatedDBObject(qsectProfilerEntry, true);
                        }
                        Polyline temp;
                        if (data.OtherPoints != null)
                        {
                            foreach (LevelLine levelline in data.OtherPoints)
                            {
                                if (levelline.Points.Count == 0)
                                    continue;
                                //gl and cotr are not added nor updated
                                if (levelline.Level == groundLevel || levelline.Level == cotrLevel)
                                    continue;
                                int level = (int)levelline.Level;
                                if (!labelData[index].ContainsKey(level))//add level to axis levels
                                    labelData[index].Add(level, levelline.Name);
                                string layername = CustomSettings.ProfilerPolylinesName + string.Format("{0,2}", level);
                                ObjectId layerid = CADLayerHelper.AddLayerIfDoesntExist(doc.Database, layername);
                                
                                bool existingPolyline = false;//this is also used to determine if xrecord exists
                                string levellineName = level.ToString();
                                //TEMP
                                ObjectId tempId = new ObjectId();
                                //TEMP
                                if (qsectProfilerEntry.Contains(levellineName))
                                {
                                    existingPolyline = true;
                                    Xrecord pl = tw.GetObjectRead<Xrecord>(qsectProfilerEntry.GetAt(levellineName));
                                    string handle = (string)pl.Data.AsArray()[0].Value;
                                    long handleValue = long.Parse(handle, System.Globalization.NumberStyles.AllowHexSpecifier);
                                    //TEMP
                                    tempId = doc.Database.GetObjectId(false, new Handle(handleValue), 0);
                                }
                                if (!tempId.IsNull)
                                {
                                    temp = tw.GetObjectWrite<Polyline>(tempId);
                                    temp.Reset(false, 0);
                                }
                                else
                                {
                                    existingPolyline = false;
                                    temp = tw.CreateEntity<Polyline>();
                                }

                                temp.SetLayerId(layerid, false);
                                temp.AddPoints(levelline.Points.Select(x=>x.To2d()).ToArray());
                                if (!existingPolyline)
                                {
                                    string handleStringPolyline = temp.ObjectId.Handle.ConvertToString();//.Value.ToString("x");
                                    string stringLevelName = levelline.Name;
                                    string labelHandle = "";//used only to reserve entry for this in xrecord
                                    string textSize = "";
                                    //pl.Data.Add(new TypedValue(code, handleString));
                                    ResultBufferBuilder builder = new ResultBufferBuilder();
                                    builder
                                        .Add(CADTypedValueHelpers.Create(handleStringPolyline))
                                        .Add(CADTypedValueHelpers.Create(stringLevelName))
                                        .Add(CADTypedValueHelpers.Create(labelHandle))
                                        .Add(CADTypedValueHelpers.Create(textSize));
                                    Xrecord xrec = new Xrecord();
                                    xrec.Data = builder.GetResultBuffer();
                                    qsectProfilerEntry.SetAt(levellineName, xrec);
                                    tw.Tr.AddNewlyCreatedDBObject(xrec, true);
                                }
                                entities.Add(axis, data, levelline, temp.ObjectId);
                            }
                        }
                    }
                }
#if USE_LABELS
                //remove all axes which doesn't have levellines displayed
                List<int> remove = new List<int>();
                foreach(KeyValuePair<int, Dictionary<int, string>> pair in labelData)
                    if (pair.Value.Count == 0)
                        remove.Add(pair.Key);
                foreach (int i in remove)
                    labelData.Remove(i);

                //add data for labels to c123_axis_data
                DBDictionary nod = tran.GetObject(db.NamedObjectsDictionaryId, OpenMode.ForRead) as DBDictionary;
                if (!nod.Contains("C123_AXIS_DATA"))
                    return;
                DBDictionary axesData = tran.GetObject(nod.GetAt("C123_AXIS_DATA"), OpenMode.ForWrite) as DBDictionary;
                foreach(DBDictionaryEntry entry in axesData)
                {
                    int index = 0;//axis index
                    if (!int.TryParse(entry.Key, out index))
                        continue;
                    //if axis doens't have anything displayed, don't update information
                    if (!labelData.ContainsKey(index))
                        continue;
                    string profilerEntry = "PROFILER_DATA";
                    DBDictionary currentAxis = tran.GetObject(entry.Value, OpenMode.ForWrite) as DBDictionary;
                    string labelDecimals = "LABEL_DECIMAL_PLACES";
                    if (!currentAxis.Contains(labelDecimals))
                    {
                        Xrecord decimals = new Xrecord();
                        //(90, ratioDecimals), (90, slopeDecimals)
                        TypedValue[] vals = new TypedValue[2];
                        vals[0] = new TypedValue(90, 2);
                        vals[1] = new TypedValue(90, 2);
                        decimals.Data = new ResultBuffer(vals);
                        currentAxis.SetAt(labelDecimals, decimals);
                        tran.AddNewlyCreatedDBObject(decimals, true);
                    }
                    DBDictionary profilerData = null;
                    if (!currentAxis.Contains(profilerEntry))
                    {
                        profilerData = new DBDictionary();
                        currentAxis.SetAt(profilerEntry, profilerData);
                        tran.AddNewlyCreatedDBObject(profilerData, true);
                    }
                    else
                        profilerData = tran.GetObject(currentAxis.GetAt(profilerEntry), OpenMode.ForWrite) as DBDictionary;
                    //iterate through all levellines for current axis
                    foreach (KeyValuePair<int, string> level in labelData[index])
                    {
                        //if levelline already exists, don't change it
                        if (profilerData.Contains(level.Key.ToString()))
                            continue;
                        //otherwise add it
                        Xrecord rec = new Xrecord();
                        //(bool - is displayed) ( string - labeltype ) ( double - text size) (string - levelline name)
                        rec.Data = new ResultBuffer(new TypedValue(291, false), new TypedValue(1, ""), new TypedValue(40, 0.2), new TypedValue(1, level.Value));
                        profilerData.SetAt(level.Key.ToString(), rec);
                        tran.AddNewlyCreatedDBObject(rec, true);
                    }
                }
#endif
                tw.Commit();
            }

            entities.ApplyDataToExistingAxes(); 
        }
    }
}
