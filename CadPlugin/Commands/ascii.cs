﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsForms = System.Windows.Forms;
using _123ASCII;
using MyLog;
using System.Diagnostics;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif


namespace CadPlugin.Commands
{
	class Ascii
	{
		[Obsolete("Old ascii command")]
		public void Invoke_old(Document document)
		{
			//if (!IsActivated("123ASCI"))
			//return;
			var layers = CADLayerHelper.GetLayers(document);
			var lineTypes = CADLineTypeHelper.GetLineTypes(document);
#if BRICSCAD
			MyBlocks.BlockManager manager = new MyBlocks.BlockManagerBCAD();
#endif
#if AUTOCAD
			MyBlocks.BlockManager manager = new MyBlocks.BlockManagerACAD();
#endif
			if (!_123ASCII.Program.ShowDialog(manager, layers, lineTypes))
			{
				document.Editor.WriteMessage("Abbruch");//Closed without applying!");
				return;
			}

			//ici kroz svaki red u tabeli, citati koordinate i postavljati vrednosti atributa, uz postavljanje nove pozicije
			//prva verzija bez namestanja atributa
			//List<Entity> entitiesToApply = new List<Entity>();
			_123ASCII._123ASCIIController ascii = _123ASCII.Program.ASCIIController;
			_123ASCII.MaskController mask = _123ASCII.Program.MyMaskController;
			//ako je u pitanju tacka, onda samo naci koordinate i ucitati tacke
			//ako je u pitanju blok
			////ako je single onda ista podesavanja za sve redove
			////ako je groups onda prvo videti kojoj grupi pripada red, pa primeniti na njemu podesavanja odgovarajuce grupe

			//pri ucitavanju u cad, proveravati da li postoji layer - ako ne postoji onda ga kreirati pre dodavanja entiteta
			//List<List<Entity>> entities = new List<List<Entity>>();
			List<Entity> entitiesRow;

			int xCol = mask.XCoordinate;
			int yCol = mask.YCoordinate;
			int zCol = mask.ZCoordinate;

			StringBuilder AssignedZeroHeight = new StringBuilder();//lista redova koji nemaju z koordinatu

			int rowCount = 0;
			document.LockDocument();
			if (mask.Type == _123ASCII.MaskType.POINT)
			{
				Document doc = document;

				Database db = doc.Database;
                
				using (Transaction tr = db.TransactionManager.StartTransaction())
				{
					doc.LockDocument();
					BlockTable bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForWrite);
					BlockTableRecord ms = (BlockTableRecord)tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite);
					try
					{
						int cur = -1;
						List<bool> comments = ascii.GetCommentMask();
						foreach (List<string> row in ascii.GetTable())
						{
							cur++;
							if (comments[cur])
								continue;
							double x = double.Parse(row[xCol], System.Globalization.CultureInfo.InvariantCulture);
							double y = double.Parse(row[yCol], System.Globalization.CultureInfo.InvariantCulture);
							double z = 0;
							if (!double.TryParse(row[zCol], System.Globalization.NumberStyles.Float | System.Globalization.NumberStyles.AllowThousands, System.Globalization.CultureInfo.InvariantCulture, out z))
							{
								z = 0;
								AssignedZeroHeight.Append(rowCount + ", ");
							}
							Point3d p3d = new Point3d(x, y, z);
							DBPoint point = new DBPoint(p3d);
							ms.AppendEntity(point);
							tr.AddNewlyCreatedDBObject(point, true);
							rowCount++;
						}

					}
					catch (System.FormatException e)
					{
						System.Windows.Forms.MessageBox.Show("X or Y column contains invalid number format!/n" + e.StackTrace);
						throw new System.Exception();
					}
					tr.Commit();
				}

				if (AssignedZeroHeight.Length > 0)
					System.Windows.Forms.MessageBox.Show("Default height 0 iz assigned to following rows:\r\n" + AssignedZeroHeight.ToString(), "", WindowsForms.MessageBoxButtons.OK, WindowsForms.MessageBoxIcon.Information);
			}
			else
			{
				if (mask.GroupBlock == _123ASCII.BlockGoup.SINGLE)
				{
					_123ASCII.Group group = mask.SingleGroup;
					if (group.Layer == "Layer 0")
						group.Layer = "0";
#if BRICSCAD
					MyBlocks.SingleBlockBCAD single;
#endif
#if AUTOCAD
					MyBlocks.SingleBlockACAD single;
#endif
					try
					{
#if BRICSCAD
						single = (MyBlocks.SingleBlockBCAD)manager.GetSingleBlock(group.BlockName);
#endif
#if AUTOCAD
						single = (MyBlocks.SingleBlockACAD)manager.GetSingleBlock(group.BlockName);
#endif
					}
					catch (System.Exception)
					{
						single = null;
					}
					Document doc = document;
					Database db = doc.Database;
					using (Transaction tr = db.TransactionManager.StartTransaction())
					{
						BlockTable bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForWrite);
						BlockTableRecord ms = (BlockTableRecord)tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite);
						LayerTable lt = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForWrite);
						BlockTableRecord btr = null;
						List<ObjectId> attsIds = new List<ObjectId>();
						if (!lt.Has(group.Layer))
						{
							LayerTableRecord rec = new LayerTableRecord();
							rec.Name = group.Layer;
							lt.Add(rec);
							tr.AddNewlyCreatedDBObject(rec, true);
						}
						if (single != null)
						{
							if (!bt.Has(single.BlockName))
							{
								Database blockDB = new Database(false, true);
								blockDB.ReadDwgFile(single.FullBlockPath, FileOpenMode.OpenForReadAndAllShare, false, "");
								ObjectId btrId = db.Insert(single.BlockName, blockDB, false);
							}
							btr = tr.GetObject(bt[single.BlockName], OpenMode.ForWrite) as BlockTableRecord;
							foreach (ObjectId id in btr)
							{
								DBObject dbo = tr.GetObject(id, OpenMode.ForRead);
								if ((dbo as AttributeDefinition) != null)
									attsIds.Add(id);
							}
						}
						//[svakom entitetu u bloku postaviti nove koordinate]mozda ne treba, jer se to izgleda radi automatski, a svakom atributu vrednost
						int cur = -1;
						List<bool> comments = ascii.GetCommentMask();
						foreach (List<string> row in ascii.GetTable())
						{
							cur++;
							if (comments[cur])
								continue;
							double x = double.Parse(row[xCol], System.Globalization.CultureInfo.InvariantCulture);
							double y = double.Parse(row[yCol], System.Globalization.CultureInfo.InvariantCulture);
							double z = 0;
							if (!double.TryParse(row[zCol], System.Globalization.NumberStyles.Float | System.Globalization.NumberStyles.AllowThousands, System.Globalization.CultureInfo.InvariantCulture, out z))
							{
								z = 0;
								AssignedZeroHeight.Append(rowCount + ", ");
							}
							if (single == null)
							{
								DBPoint p = new DBPoint(new Point3d(x, y, z));

								ms.AppendEntity(p);
								p.Layer = group.Layer;
								tr.AddNewlyCreatedDBObject(p, true);
							}
							else
							{
								BlockReference br = new BlockReference(new Point3d(x, y, z), btr.Id);
								ms.AppendEntity(br);
								br.Layer = group.Layer;
								tr.AddNewlyCreatedDBObject(br, true);
								entitiesRow = new List<Entity>(single.Attributes.Count);

								foreach (ObjectId id in attsIds)
								{
									AttributeDefinition att = tr.GetObject(id, OpenMode.ForWrite) as AttributeDefinition;
									_123ASCII.MyAttribute matt = group.GetAttribute(att.Tag);

									string val = matt.Prefix + (matt.FixedValue ? matt.Value : (row.Count > matt.Column ? row[matt.Column] : "")) + matt.Sufix;
									AttributeReference atRef = new AttributeReference();
									att.TextString = val;
									Matrix3d matrix = br.BlockTransform;
									atRef.SetAttributeFromBlock(att, matrix);
									br.AttributeCollection.AppendAttribute(atRef);
									tr.AddNewlyCreatedDBObject(atRef, true);
								}
								//entities.Add(entitiesRow);
								rowCount++;
							}
						}
						tr.Commit();
						if (AssignedZeroHeight.Length > 0)
							System.Windows.Forms.MessageBox.Show("Default height 0 iz assigned to following rows:\r\n" + AssignedZeroHeight.ToString(), "", WindowsForms.MessageBoxButtons.OK, WindowsForms.MessageBoxIcon.Information);

					}
				}
				else
				{
					int cur = -1;
					List<bool> comments = ascii.GetCommentMask();
					foreach (List<string> row in ascii.GetTable())
					{
						cur++;
						if (comments[cur])
							continue;
						//dohvati grupu kojoj row pripada
						_123ASCII.Group mgroup = null;
						string grn = "";
						if (row.Count >= mask.SelectedGroupingColumn)
							grn = row[mask.SelectedGroupingColumn];
						foreach (_123ASCII.Group gr in mask.Groups)
							if (gr.GroupName == grn)
								mgroup = gr;
						if (mgroup.Layer == "Layer 0")
							mgroup.Layer = "0";
#if BRICSCAD
						MyBlocks.SingleBlockBCAD single;
#endif
#if AUTOCAD
						MyBlocks.SingleBlockACAD single;
#endif
						try
						{
#if BRICSCAD
							single = (MyBlocks.SingleBlockBCAD)manager.GetSingleBlock(mgroup.BlockName);
#endif
#if AUTOCAD
							single = (MyBlocks.SingleBlockACAD)manager.GetSingleBlock(mgroup.BlockName);
#endif
						}
						catch (System.Exception)
						{
							single = null;
						}

						Document doc = document;
						Database db = doc.Database;
						using (Transaction tr = db.TransactionManager.StartTransaction())
						{
							//doc.LockDocument();
							BlockTable bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForWrite);
							BlockTableRecord ms = (BlockTableRecord)tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite);
							LayerTable lt = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForWrite);
							BlockTableRecord btr = null;
							List<ObjectId> attsIds = new List<ObjectId>();
							if (!lt.Has(mgroup.Layer))
							{
								LayerTableRecord rec = new LayerTableRecord();
								rec.Name = mgroup.Layer;
								lt.Add(rec);
								tr.AddNewlyCreatedDBObject(rec, true);
							}
							if (single != null)
							{
								if (!bt.Has(single.BlockName))
								{
									Database blockDB = new Database(false, true);
									blockDB.ReadDwgFile(single.FullBlockPath, FileOpenMode.OpenForReadAndAllShare, false, "");
									ObjectId btrId = db.Insert(single.BlockName, blockDB, false);
								}
								btr = tr.GetObject(bt[single.BlockName], OpenMode.ForWrite) as BlockTableRecord;
								foreach (ObjectId id in btr)
								{
									DBObject dbo = tr.GetObject(id, OpenMode.ForRead);
									if ((dbo as AttributeDefinition) != null)
										attsIds.Add(id);
								}

							}
							{
								double x = double.Parse(row[xCol], System.Globalization.CultureInfo.InvariantCulture);
								double y = double.Parse(row[yCol], System.Globalization.CultureInfo.InvariantCulture);
								double z = 0;
								if (!double.TryParse(row[zCol], System.Globalization.NumberStyles.Float | System.Globalization.NumberStyles.AllowThousands, System.Globalization.CultureInfo.InvariantCulture, out z))
								{
									z = 0;
									AssignedZeroHeight.Append(rowCount + ", ");
								}
								if (single == null)
								{
									DBPoint p = new DBPoint(new Point3d(x, y, z));
									ms.AppendEntity(p);
									p.Layer = mgroup.Layer;
									tr.AddNewlyCreatedDBObject(p, true);
								}
								else
								{
									BlockReference br = new BlockReference(new Point3d(x, y, z), btr.Id);
									ms.AppendEntity(br);
									br.Layer = mgroup.Layer;
									tr.AddNewlyCreatedDBObject(br, true);
									entitiesRow = new List<Entity>(single.Attributes.Count);

									foreach (ObjectId id in attsIds)
									{
										AttributeDefinition att = tr.GetObject(id, OpenMode.ForWrite) as AttributeDefinition;
										_123ASCII.MyAttribute matt = mgroup.GetAttribute(att.Tag);

										string val = matt.Prefix + (matt.FixedValue ? matt.Value : (row.Count > matt.Column ? row[matt.Column] : "")) + matt.Sufix;
										AttributeReference atRef = new AttributeReference();
										att.TextString = val;
										Matrix3d matrix = br.BlockTransform;
										atRef.SetAttributeFromBlock(att, matrix);
										br.AttributeCollection.AppendAttribute(atRef);
										tr.AddNewlyCreatedDBObject(atRef, true);
									}
									//entities.Add(entitiesRow);
									rowCount++;
								}
							}
							tr.Commit();

						}
					}
					if (AssignedZeroHeight.Length > 0)
						System.Windows.Forms.MessageBox.Show("Default height 0 iz assigned to following rows:\r\n" + AssignedZeroHeight.ToString(), "", WindowsForms.MessageBoxButtons.OK, WindowsForms.MessageBoxIcon.Information);


				}
			}
			//u entities imamo sve spremno, treba samo da dodamo u acad
			document.Editor.Regen();
			//Bricscad.ApplicationServices.Application.DocumentManager.MdiActiveDocument.SendStringToExecute("\n", true, false, false);
			document.SendStringToExecute("ZOOM ", true, false, false);
			document.SendStringToExecute("_e ", true, false, false);
		}

		public static void Invoke(Document document)
		{
			//Get data from dialog. Then first add all lines. If layer doesn't exist, it is created. If linetype is not loaded, it is loaded.
			//After that points/blocks are inserted. If selected option is points, then we create points on drawing for all points we have.
			//If selected option is groups but one group for all (single group), then it is created if it doesn't exist, and for every point one block reference is added.
			//If selected option is groups and points are grouped, then for every point group is get, created if not exists and blockreference is added.

			var layers = CADLayerHelper.GetLayers(document);//Get all layers
			var lineTypes = CADLineTypeHelper.GetLineTypes(document);//Get loaded linetypes
#if BRICSCAD
			MyBlocks.BlockManager manager = new MyBlocks.BlockManagerBCAD();
			MyBlocks.SingleBlockBCAD single = null;
#endif
#if AUTOCAD
			MyBlocks.BlockManager manager = new MyBlocks.BlockManagerACAD();
			MyBlocks.SingleBlockACAD single = null;
#endif
			if (!_123ASCII.Program.ShowDialog(manager, layers, lineTypes))
			{
				Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage("Abbruch");//Closed without applying!");
				return;
			}
			_123ASCII._123ASCIIController ascii = _123ASCII.Program.ASCIIController;
			_123ASCII.MaskController mask = _123ASCII.Program.MyMaskController;
			//Columns for x,y,z coordinates
			int xCol = mask.XCoordinate;
			int yCol = mask.YCoordinate;
			int zCol = mask.ZCoordinate;

			double lineScale = 1;
			int rowCount = 0;
			Document doc = Application.DocumentManager.MdiActiveDocument;
			//I had some problems once, so locking document fixed them
			doc.LockDocument();
			Database db = doc.Database;
			List<bool> comments = ascii.GetCommentMask();//Get comment mask, which tells us if table row is comment
			//When adding lines, we keep information if point should be rotated, and we keep rotation direction line, and starting point.
			//Better solution because i will have to duplicate a lot of code for block insertion. This way, i will only add some operations on good places when adding block reference.
			Dictionary<int, ObjectId> blockRotationPointLines = new Dictionary<int, ObjectId>();
			Dictionary<int, BlockDirection> blockRotationDirection = new Dictionary<int, BlockDirection>();

			#region Add lines
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				BlockTable bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForWrite);
				BlockTableRecord ms = (BlockTableRecord)tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite);
				//LayerTable lt = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForWrite);
				//BlockTableRecord btr = null;//block is created as blocktablerecord in blocktable
				List<ObjectId> attsIds = new List<ObjectId>();
				LineConnectionData lineData = ascii.LineData;
				LineConnectionSettings lineSettings = mask.LineConnectionSettings;

				// If all groups are drawn on a single layer, same ObjectId is assigned.
				// If groups have their own layer, then ObjectId represents that layer.
				Dictionary<string, ObjectId> groupsLayers = new Dictionary<string, ObjectId>();
				if (mask.LineConnectionSettings.IsLineConnectionUsed)
				{
					if (mask.LineConnectionSettings.DrawOnSingleLayer)
					{
						ObjectId layerId = CADLayerHelper.AddLayerIfDoesntExist(db, lineSettings.Layer);
						foreach (KeyValuePair<string, string> pair in mask.LineConnectionSettings.GroupsLayers)
						{
							groupsLayers.Add(pair.Key, layerId);
						}
					}
					else
					{
						foreach (KeyValuePair<string, string> pair in mask.LineConnectionSettings.GroupsLayers)
						{
							ObjectId layerId = CADLayerHelper.AddLayerIfDoesntExist(db, pair.Value);
							groupsLayers.Add(pair.Key, layerId);
						}
					}
				}

				ObjectId singleLayerId = CADLayerHelper.AddLayerIfDoesntExist(db, lineSettings.Layer);
				ObjectId linetypeId = CADLineTypeHelper.GetLineTypeId(db, lineSettings.Linetype);

				//Add all lines
				foreach (var v in lineData.GetAllLines())
				{
					//create line
					//get start and end point
					SinglePoint p1 = lineData[v.Point1Index];
					SinglePoint p2 = lineData[v.Point2Index];
					Line l = new Line(new Point3d(p1.X, p1.Y, p1.Z), new Point3d(p2.X, p2.Y, p2.Z));
					if (!mask.LineConnectionSettings.IsLineConnectionUsed)
						l.LayerId = singleLayerId;
					else
						l.LayerId = groupsLayers[v.GroupName];
					l.LinetypeId = linetypeId;
					//not sure about this scale, but without it distance between dashed lines is very big
					l.LinetypeScale = lineScale;
					ms.AppendEntity(l);
					tr.AddNewlyCreatedDBObject(l, true);

					//if insert block, insert it and set direction
					//TODO Problems. We have to check if points have blocks at all. If they does, we have to find block of current point.
					//Then we check if it exists, if not we create new one. 
					//Maybe i can add new method which searches for block of given point, and creates it if it doesn't exist.
					if (v.BlockInsertionPointIndex > -1)
					{
						blockRotationPointLines.Add(v.BlockInsertionPointIndex, l.ObjectId);
						blockRotationDirection.Add(v.BlockInsertionPointIndex, v.Direction);
					}

				}
				tr.Commit();

				doc.Editor.WriteMessage(/*"Lines created: "*/ "Linien wurden erstellt: " + lineData.GetAllLines().Count);
			}

			#endregion

			if (mask.Type == _123ASCII.MaskType.POINT)
			{
				#region Insert only points
				using (Transaction tr = db.TransactionManager.StartTransaction())
				{
					BlockTable bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForWrite);
					BlockTableRecord ms = (BlockTableRecord)tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite);

					int cur = -1;
					foreach (List<string> row in ascii.GetTable())
					{
						cur++;
						//If we add line, there is no need to add another point. If we add blocks, then it makes sense to have both.
						//So we check if point is already used in line
						if (comments[cur] || ascii.LineData.IsPointUsedInLine(cur))
							continue;
						SinglePoint point = ascii.LineData[cur];
						//CADCommonHelper.AddPoint(tr, new Point3d(point.X, point.Y, point.Z), ms, null);
						DBPoint pt = tr.CreateEntity<DBPoint>(ms);
						pt.Position = new Point3d(point.X, point.Y, point.Z);
						rowCount++;
					}
					tr.Commit();
				}
				#endregion
			}
			else
			{
				#region All points use same block
				if (mask.GroupBlock == _123ASCII.BlockGoup.SINGLE)
				{
					_123ASCII.Group group = mask.SingleGroup;
					if (group.Layer == "Layer 0")
						group.Layer = "0";
#if BRICSCAD
					GetSingleBlock(manager, group.BlockName, ref single);
#endif

					using (Transaction tr = db.TransactionManager.StartTransaction())
					{
						BlockTable bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForWrite);
						BlockTableRecord ms = (BlockTableRecord)tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite);
						BlockTableRecord btr = null;//block is created as blocktablerecord in blocktable
						List<ObjectId> attsIds = new List<ObjectId>();

						CADLayerHelper.AddLayerIfDoesntExist(db, group.Layer);

						//TODO load linetype if not exists

						if (single != null)
							CADBlockHelper.LoadBlockAttributes(bt, ref btr, single.BlockName, single.FullBlockPath, db, attsIds, tr);

						//[svakom entitetu u bloku postaviti nove koordinate]mozda ne treba, jer se to izgleda radi automatski, a svakom atributu vrednost
						int cur = -1;
						foreach (List<string> row in ascii.GetTable())
						{
							cur++;
							if (comments[cur])// || ascii.LineData.IsPointUsedInLine(cur))
								continue;
							SinglePoint point = ascii.LineData[cur];
							Point3d pt3 = new Point3d(point.X, point.Y, point.Z);
							if (single == null)
							{
								//CADCommonHelper.AddPoint(tr, pt3, ms, group.Layer);
								DBPoint pt = tr.CreateEntity<DBPoint>(ms);
								pt.Position = pt3;
								pt.Layer = group.Layer;
							}
							else
							{
								BlockReference br = CreateBlockReferenceASCII(pt3, ms, btr, attsIds, group, tr, row);
								if (blockRotationPointLines.ContainsKey(cur))
								{
									double rotation = ((Line)tr.GetObject(blockRotationPointLines[cur], OpenMode.ForRead)).Angle;
									Matrix3d m = br.BlockTransform;
									if (blockRotationDirection[cur] == BlockDirection.FROM_POINT_2)//If from point 2, then add/subtract Pi (it is in radians)
										rotation += Math.PI;
									Matrix3d r = Matrix3d.Rotation(rotation, new Vector3d(0, 0, 1), pt3);
									br.TransformBy(r);
								}
								rowCount++;
							}
						}
						tr.Commit();

					}
#endregion
				}
				else
				{
#region Points are using different blocks, depending on some grouping settings
					//mask.GroupBlock == MULTIPLE
					int cur = -1;
					foreach (List<string> row in ascii.GetTable())
					{
						cur++;
						if (comments[cur])// || ascii.LineData.IsPointUsedInLine(cur))
							continue;
						_123ASCII.Group mgroup = null;
						string grn = "";
						if (row.Count >= mask.SelectedGroupingColumn)
							grn = row[mask.SelectedGroupingColumn];
						mgroup = mask.GetGroup(grn);
						if (mgroup.Layer == "Layer 0")
							mgroup.Layer = "0";

						GetSingleBlock(manager, mgroup.BlockName, ref single);

						using (Transaction tr = db.TransactionManager.StartTransaction())
						{
							BlockTable bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForWrite);
							BlockTableRecord ms = (BlockTableRecord)tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite);
							BlockTableRecord btr = null;
							List<ObjectId> attsIds = new List<ObjectId>();

							CADLayerHelper.AddLayerIfDoesntExist(db, mgroup.Layer);

							//TODO load linetype if not exists
							if (single != null)
								CADBlockHelper.LoadBlockAttributes(bt, ref btr, single.BlockName, single.FullBlockPath, db, attsIds, tr);

							SinglePoint point = ascii.LineData[cur];
							Point3d pt3 = new Point3d(point.X, point.Y, point.Z);
							if (single == null)
							{
								//CADCommonHelper.AddPoint(tr, pt3, ms, mgroup.Layer);
								DBPoint dbp = tr.CreateEntity<DBPoint>(ms);
								dbp.Position = pt3;
								dbp.Layer = mgroup.Layer;
							}
							else
							{
								BlockReference br = CreateBlockReferenceASCII(pt3, ms, btr, attsIds, mgroup, tr, row);
								if (blockRotationPointLines.ContainsKey(cur))
								{
									double rotation = ((Line)tr.GetObject(blockRotationPointLines[cur], OpenMode.ForRead)).Angle;
									Matrix3d m = br.BlockTransform;
									if (blockRotationDirection[cur] == BlockDirection.FROM_POINT_2)//If from point 2, then add/subtract Pi (it is in radians)
										rotation += Math.PI;
									Matrix3d r = Matrix3d.Rotation(rotation, new Vector3d(0, 0, 1), pt3);
									br.TransformBy(r);
								}
								rowCount++;
							}
							tr.Commit();

						}
					}
#endregion
				}
			}
			//u entities imamo sve spremno, treba samo da dodamo u acad
			Application.DocumentManager.MdiActiveDocument.Editor.Regen();
			//Bricscad.ApplicationServices.Application.DocumentManager.MdiActiveDocument.SendStringToExecute("\n", true, false, false);
			Application.DocumentManager.MdiActiveDocument.SendStringToExecute("ZOOM ", true, false, false);
			Application.DocumentManager.MdiActiveDocument.SendStringToExecute("_e ", true, false, false);


		}

        

		private static void GetSingleBlock(MyBlocks.BlockManager manager, string name, ref MyBlocks.SingleBlockBCAD obj)
		{
			try
			{
				 obj = (MyBlocks.SingleBlockBCAD)manager.GetSingleBlock(name);
			}
			catch (System.Exception e)
			{
				MyTraceSource.Current.Error("Get single block error <" + name + ">: " + e);
				throw e;
			}
		}
		private static void GetSingleBlock(MyBlocks.BlockManager manager, string name, ref MyBlocks.SingleBlockACAD obj)
		{
			try
			{
				obj = (MyBlocks.SingleBlockACAD)manager.GetSingleBlock(name);
			}
			catch (System.Exception e)
			{
				MyTraceSource.Current.Error("Get single block error <" + name + ">: " + e);
				throw e;
			}
		}
		/// <summary>
		/// Used only by ASCII! 
		/// (this should have been generlized method, but ascii is included. don't want to change current code, so new method is created)
		/// </summary>
		private static BlockReference CreateBlockReferenceASCII(Point3d pt3, BlockTableRecord modelSpace, BlockTableRecord block, List<ObjectId> attributesId, _123ASCII.Group group, Transaction tr, List<string> row)
		{
			BlockReference br = new BlockReference(pt3, block.Id);
			modelSpace.AppendEntityCurrentLayer(br);
			br.Layer = group.Layer;
			tr.AddNewlyCreatedDBObject(br, true);

			foreach (ObjectId id in attributesId)
			{
				AttributeDefinition att = tr.GetObject(id, OpenMode.ForWrite) as AttributeDefinition;
				_123ASCII.MyAttribute matt = group.GetAttribute(att.Tag);

				string val = matt.Prefix + (matt.FixedValue ? matt.Value : (row.Count > matt.Column ? row[matt.Column] : "")) + matt.Sufix;
				AttributeReference atRef = new AttributeReference();
				att.TextString = val;
				Matrix3d matrix = br.BlockTransform;
				atRef.SetAttributeFromBlock(att, matrix);
				br.AttributeCollection.AppendAttribute(atRef);
				tr.AddNewlyCreatedDBObject(atRef, true);
			}
			return br;
		}
	}
}
