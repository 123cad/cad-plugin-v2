﻿using CadPlugin.BlockAttributeEdit.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.BlockAttributeEdit.View.UpdateAttributeGroup
{
    public partial class View : Form
    {
        private List<string> sourceList;
        public View()
        {
            InitializeComponent();
            dgv.AutoGenerateColumns = false;
        }
        public void Initialize(List<string> items)
        {
            sourceList = items;

            foreach(string item in items)
            {
                int index = dgv.Rows.Add();
                var row = dgv.Rows[index];
                row.Cells[0].Value = item;
            }

            dgv.RowsAdded += (_, e) =>
            {
                var row = dgv.Rows[e.RowIndex];
                row.Cells[0].Value = "Value";
            };
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            sourceList.Clear();
            foreach(DataGridViewRow row in dgv.Rows)
            {
                string val = row.Cells[0].Value as string;
                sourceList.Add(val);
            }
            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            dgv.Rows.Add();
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (dgv.CurrentCell == null)
                return;
            if (dgv.CurrentCell.RowIndex == -1)
                return;
            dgv.Rows.RemoveAt(dgv.CurrentCell.RowIndex);
        }
    }
}
