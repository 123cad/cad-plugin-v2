﻿namespace CadPlugin.BlockAttributeEdit.View.MainForm
{
    partial class MainFormView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFormView));
            this.dgv = new System.Windows.Forms.DataGridView();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonApply = new System.Windows.Forms.Button();
            this.buttonSelectBlock = new System.Windows.Forms.Button();
            this.textBoxBlockName = new System.Windows.Forms.TextBox();
            this.ColumnAttributeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAttributeValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAttributeValues = new Inventory.View.DataGridViewColumns.DataGridViewComboBoxCustomColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnAttributeName,
            this.ColumnAttributeValue,
            this.ColumnAttributeValues});
            this.dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv.Location = new System.Drawing.Point(12, 41);
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.Size = new System.Drawing.Size(455, 192);
            this.dgv.TabIndex = 0;
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.Location = new System.Drawing.Point(393, 289);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 1;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonApply
            // 
            this.buttonApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonApply.Location = new System.Drawing.Point(392, 239);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(75, 23);
            this.buttonApply.TabIndex = 2;
            this.buttonApply.Text = "Apply";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // buttonSelectBlock
            // 
            this.buttonSelectBlock.Location = new System.Drawing.Point(12, 12);
            this.buttonSelectBlock.Name = "buttonSelectBlock";
            this.buttonSelectBlock.Size = new System.Drawing.Size(75, 23);
            this.buttonSelectBlock.TabIndex = 3;
            this.buttonSelectBlock.Text = "Select block";
            this.buttonSelectBlock.UseVisualStyleBackColor = true;
            this.buttonSelectBlock.Click += new System.EventHandler(this.buttonSelectBlock_Click);
            // 
            // textBoxBlockName
            // 
            this.textBoxBlockName.Location = new System.Drawing.Point(93, 14);
            this.textBoxBlockName.Name = "textBoxBlockName";
            this.textBoxBlockName.ReadOnly = true;
            this.textBoxBlockName.Size = new System.Drawing.Size(140, 20);
            this.textBoxBlockName.TabIndex = 4;
            // 
            // ColumnAttributeName
            // 
            this.ColumnAttributeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnAttributeName.HeaderText = "Name";
            this.ColumnAttributeName.Name = "ColumnAttributeName";
            this.ColumnAttributeName.ReadOnly = true;
            // 
            // ColumnAttributeValue
            // 
            this.ColumnAttributeValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnAttributeValue.HeaderText = "Value";
            this.ColumnAttributeValue.Name = "ColumnAttributeValue";
            // 
            // ColumnAttributeValues
            // 
            this.ColumnAttributeValues.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnAttributeValues.HeaderText = "Values";
            this.ColumnAttributeValues.Name = "ColumnAttributeValues";
            // 
            // MainFormView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 324);
            this.Controls.Add(this.textBoxBlockName);
            this.Controls.Add(this.buttonSelectBlock);
            this.Controls.Add(this.buttonApply);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.dgv);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainFormView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.Button buttonSelectBlock;
        private System.Windows.Forms.TextBox textBoxBlockName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAttributeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAttributeValue;
        private Inventory.View.DataGridViewColumns.DataGridViewComboBoxCustomColumn ColumnAttributeValues;
    }
}