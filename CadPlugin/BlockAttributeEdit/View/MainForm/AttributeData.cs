﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.BlockAttributeEdit.View.MainForm
{
    public class AttributeData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _Name;
        private string _Text;
        private string _Values;
        public string Name => _Name;
        public string Text
        {
            get { return _Text; }
            set
            {
                bool changed = _Text != value;
                _Text = value;
                if (changed)
                    OnPropertyChanged();
            }
        }
        public string Values
        {
            get { return _Values; }
            set
            {
                bool changed = _Values != value;
                _Values = value;
                if (changed)
                    OnPropertyChanged();
                Text = value;
            }
        }
        public AttributeData(string name)
        {
            _Name = name;
        }





        private void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
