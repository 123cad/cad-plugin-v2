﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Inventory.View.DataGridViewColumns;
using CadPlugin.BlockAttributeEdit.Data;

namespace CadPlugin.BlockAttributeEdit.View.MainForm
{

    public partial class MainFormView : Form
    {
        public readonly BlockData CurrentBlock;

        private Data.Manager mgr;

        public MainFormView(Data.Manager manager)
        {
            this.mgr = manager;
            InitializeComponent();
            dgv.AutoGenerateColumns = false;
            dgv.CellMouseClick += (_, e) =>
            {
                if (e.RowIndex == -1 || e.ColumnIndex == -1)
                    return;
                if (e.ColumnIndex == ColumnAttributeValue.Index ||
                    e.ColumnIndex == ColumnAttributeValues.Index)
                {
                    if (dgv.IsCurrentCellInEditMode)
                        dgv.EndEdit();
                    dgv.CurrentCell = dgv[e.ColumnIndex, e.RowIndex];

                    dgv.BeginEdit(false);
                }
            };
            ColumnAttributeValues.DataSourceNeeded += x =>
            {
                // Update new data source.
                string attName = dgv[ColumnAttributeName.Index, x.RowIndex].Value as string;
                DataTable dt = new DataTable();
                dt.Columns.Add();
                dt.Columns.Add();

                foreach (var v in AttributeGroupCollection.Instance.GetAttributeGroup(attName))
                {
                    dt.Rows.Add(v, "");//, v.Description);
                }
                x.DataSource = dt;
            };
            CurrentBlock = new BlockData();

            ColumnAttributeName.DataPropertyName = nameof(AttributeData.Name);
            ColumnAttributeValue.DataPropertyName = nameof(AttributeData.Text);
            ColumnAttributeValues.DataPropertyName = nameof(AttributeData.Values);

            textBoxBlockName.DataBindings.Add(new Binding(nameof(TextBox.Text), CurrentBlock, nameof(BlockData.BlockName)));
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            mgr.ApplyChanges(CurrentBlock);
            // Select next block?
            foreach (var d in CurrentBlock.Data)
            {
                AttributeGroupCollection.Instance.UpdateFromAttributeGroup(d.Name, d.Text);
            }
        }

        private void buttonSelectBlock_Click(object sender, EventArgs e)
        {
            BlockData bd = mgr.GetBlock();
            if (bd == null)
                return;
            CurrentBlock.ApplyFromBlock(bd);
            /*foreach(var d in bd.Data)
            {  
                AttributeGroupCollection.Instance.UpdateFromAttributeGroup(d.Name, d.Text);
            }*/
            dgv.DataSource = new BindingSource(new BindingList<AttributeData>(bd.Data), null);

        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
