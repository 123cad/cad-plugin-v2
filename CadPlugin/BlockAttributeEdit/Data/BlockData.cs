﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.BlockAttributeEdit.View.MainForm;
using System.ComponentModel;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.BlockAttributeEdit.Data
{

    public class BlockData:INotifyPropertyChanged
    {
        private string _BlockName;
        public string BlockName { get { return _BlockName; }
            set
            {
                bool changed = _BlockName != value;
                _BlockName = value;
                if (changed)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(BlockName)));
            }
        }
        public ObjectId BlockInstanceId;

        public List<AttributeData> Data;

        public event PropertyChangedEventHandler PropertyChanged;

        public void ApplyFromBlock(BlockData bd)
        {
            BlockName = bd.BlockName;
            BlockInstanceId = bd.BlockInstanceId;
            Data = bd.Data;
        }
    }
}
