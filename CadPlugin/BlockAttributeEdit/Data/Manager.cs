﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.BlockAttributeEdit.View.MainForm;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.BlockAttributeEdit.Data
{
    public class Manager
    {
        private readonly Document doc;

        public Manager(Document doc)
        {
            this.doc = doc;
        }
        /// <summary>
        /// Users selects a block.
        /// </summary>
        /// <returns></returns>
        public BlockData GetBlock()
        {
            BlockData bd = new BlockData();
            bd.Data = new List<AttributeData>();

            PromptEntityOptions opt = new PromptEntityOptions("Select a block: ");
            opt.SetRejectMessage("Not a block!");
            opt.AddAllowedClass(typeof(BlockReference), true);
            PromptEntityResult res = doc.Editor.GetEntity(opt);

            if (res.Status == PromptStatus.OK)
            {
                using (Transaction tr = doc.Database.TransactionManager.StartTransaction())
                {
                    BlockReference br = tr.GetObject(res.ObjectId, OpenMode.ForRead) as BlockReference;
                    bd.BlockInstanceId = br.ObjectId;
                    bd.BlockName = br.Name;
                    foreach(ObjectId oid in br.AttributeCollection)
                    {
                        AttributeReference ar = tr.GetObject(oid, OpenMode.ForRead) as AttributeReference;
                        AttributeData ad = new AttributeData(ar.Tag);
                        ad.Text = ar.TextString;
                        bd.Data.Add(ad);
                    }

                    tr.Commit();
                }
            }

            return bd;
        }

        /// <summary>
        /// Commits values to block.
        /// </summary>
        /// <param name="data"></param>
        public void ApplyChanges(BlockData data)
        {
            if (data.BlockInstanceId == ObjectId.Null)
                return;
            using (Transaction tr = doc.Database.TransactionManager.StartTransaction())
            {
                BlockReference br = tr.GetObject(data.BlockInstanceId, OpenMode.ForRead) as BlockReference;
                foreach (ObjectId id in br.AttributeCollection)
                {
                    AttributeReference ar = tr.GetObject(id, OpenMode.ForWrite) as AttributeReference;
                    AttributeData ad = data.Data.FirstOrDefault(x => x.Name == ar.Tag);
                    if (ad != null)
                    {
                        ar.TextString = ad.Text;
                    }
                }
                tr.Commit();
            }
            doc.Editor.Regen();
        }
    }
}
