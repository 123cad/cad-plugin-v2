﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.BlockAttributeEdit.Data
{
    public class AttributeGroupCollection
    {
        /// <summary>
        /// Attribute name / items related to this attribute.
        /// </summary>
        private Dictionary<string, List<string>> attributes = new Dictionary<string, List<string>>();

        private static AttributeGroupCollection _Instance;

        public static AttributeGroupCollection Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new AttributeGroupCollection();
                return _Instance;
            }
        }

        private AttributeGroupCollection()
        {
        }

        public List<string> GetAttributeGroup(string attributeName)
        {
            if (!attributes.ContainsKey(attributeName))
                attributes.Add(attributeName, new List<string>());
            return attributes[attributeName];
        }

        /// <summary>
        /// Adds attributValue if it doesn't already exist.
        /// </summary>
        /// <param name="items"></param>
        public void UpdateFromAttributeGroup(string attributeName, string attributeValue)
        {
            attributeValue = attributeValue.Trim();
            if (string.IsNullOrEmpty(attributeValue))
                return;
            var list = GetAttributeGroup(attributeName);
            bool exists = false;
            foreach(var v in list)
            {
                if (v == attributeValue)
                {
                    exists = true;
                    break;
                }
            };
            if (!exists)
                list.Add(attributeValue);
        }
    }
}
