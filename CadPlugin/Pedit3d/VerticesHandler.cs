﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Teigha.DatabaseServices;
using Teigha.Geometry;
#endif
#if AUTOCAD
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
#endif

namespace CadPlugin
{
    public class VerticesHandler
    {
        public List<Point3d> Points { get; private set; }

        public VerticesHandler(Curve c)
        {
            Points = new List<Point3d>();
            int count = (int)c.EndParam;
            if (c is Line)
            {
                //If curve is a line, then parameter represents lenght from point to point
                Points.Add(c.StartPoint);
                Points.Add(c.EndPoint);
            }
            else
                for (int i = 0; i <= count; i++)
                    Points.Add(c.GetPointAtParameter(i));

            startPointSingle = false;
            endPointSingle = false;
        }

        public Point3d StartPoint { get { return Points[0]; } }
        public Point3d EndPoint { get { return Points[Points.Count - 1]; } }

        private bool startPointSingle;
        private bool endPointSingle;

        private bool startPointChanged;
        private bool endPointChanged;
        public void IterationStart()
        {
            startPointChanged = false;
            endPointChanged = false;
        }
        public void IterationEnd()
        {
            if (!(startPointChanged || endPointChanged))
                Completed = true;
            else
                Completed = false;
            if (!startPointChanged)
                startPointSingle = true;
            if (!endPointChanged)
                endPointSingle = true;
        }
        private bool invertedNext;
        private bool invertedPrevious;
        public bool IsNextToEndPoint(VerticesHandler v)
        {
            if (endPointSingle)
                return false;
            if (EndPoint.IsEqualTo(v.StartPoint))
            {
                invertedNext = false;
                return true;
            }
            else if (EndPoint.IsEqualTo(v.EndPoint))
            {
                invertedNext = true;
                return true;
            }
            return false;
        }

        public bool IsPreviousToStartPoint(VerticesHandler v)
        {
            if (startPointSingle)
                return false;
            if (StartPoint.IsEqualTo(v.StartPoint))
            {
                invertedPrevious = false;
                return true;
            }
            else if (StartPoint.IsEqualTo(v.EndPoint))
            {
                invertedPrevious = true;
                return true;
            }
            return false;
        }

        public void AddNext(VerticesHandler v)
        {
            int count = v.Points.Count;
            if (!invertedNext)
                for (int i = 0; i < count; i++)
                    Points.Add(v.Points[i]);
            else
                for (int i = count - 1; i >= 0; i--)
                    Points.Add(v.Points[i]);
            endPointChanged = true;
        }
        public void AddPrevious(VerticesHandler v)
        {
            int count = v.Points.Count;
            if (!invertedPrevious)
                for (int i = 0; i < count; i++)
                    Points.Insert(0, v.Points[i]);
            else
                for (int i = count - 1; i >= 0; i--)
                    Points.Insert(0, v.Points[i]);

            startPointChanged = true;
        }

        public bool Completed { get; private set; }

    }
}
