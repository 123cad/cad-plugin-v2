﻿
//#define USE_LABELS

using System;
using System.Text;
using System.Diagnostics;
using System.Reflection;
using System.Collections.Generic;
using System.IO;
//using System.Windows.Forms;
using Size = System.Drawing.Size;

using WindowsForms = System.Windows.Forms;
using Format066Holder;
using MyLog;
using CadPlugin.Common;
using Ninject;
using System.Globalization;
using System.Threading;
using CadPlugin.Commands;
using SharedUtilities.GlobalDependencyInjection;
using CadPlugin.KHNeu.Views.KHNeuModels;
using CadPlugin.DTMBalance;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
using StyleNamespace = Teigha.GraphicsInterface;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
using StyleNamespace = Autodesk.AutoCAD.GraphicsInterface;
#endif

/*GUIDANCE
 * //NOTE - contains text which gives explanation
 * //TODO - things need to be done
 * //DISABLE - temporarily disabled, for example for test purposes
 */

// This line is not mandatory, but improves loading performances
[assembly: CommandClass(typeof(CadPlugin.MyCommands))]

namespace CadPlugin
{

	// This class is instantiated by AutoCAD for each document when
	// a command is called by the user the first time in the context
	// of a given document. In other words, non static data in this class
	// is implicitly per-document!
	public partial class MyCommands
	{
		#region InvariantCultureRunner

		interface IInvoke
		{
			void Invoke();
		}
		abstract class InvokerBase:IInvoke
        {
			protected CultureInfo oldCulture { get; set; }
			public void Invoke()
            {
				// save old culture
				// surround with try/catch

				InvokeInner();

				// restore old culture

            }
			protected abstract void InvokeInner();

        }
		class Invoker : InvokerBase
		{
			public Action A1 { get; }
			public Invoker(Action a) => A1 = a;
            protected override void InvokeInner()
            {
				A1.Invoke();
            }
		}
		class Invoker<T> : InvokerBase
		{
			public Action<T> A { get; }
			public T Parameter { get; }
			public Invoker(Action<T> a, T doc)
			{
				A = a;
				Parameter = doc;
			}
            protected override void InvokeInner()
			{
				A.Invoke(Parameter);
			}
		}
		class Invoker<T, R> : InvokerBase
		{
			public Action<T, R> A { get; }
			public T Parameter1 { get; }
			public R Parameter2 { get; }
			public Invoker(Action<T, R> a, T t, R r)
			{
				A = a;
				Parameter1 = t;
				Parameter2 = r;
			}
            protected override void InvokeInner()
			{
				A.Invoke(Parameter1, Parameter2);
			}
		}
		private static CultureInfo oldCulture = null;
		/// <summary>
		/// Wrapps command culture.
		/// </summary>
		/// <param name="a"></param>
		private static void RunWithInvariantCulture(IInvoke i)
		{
			oldCulture = Thread.CurrentThread.CurrentCulture;
			Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
			i.Invoke();
			Thread.CurrentThread.CurrentCulture = oldCulture;
			oldCulture = null;
		}
		#endregion


		enum Modules
		{
			NotSet = -1,
			Layout = 0,
			Axis = 1,
			Longitudinal = 2,
			Crossection = 3,
			Inventary = 5
		}
		#region Automatically generated data
		// The CommandMethod attribute can be applied to any public  member 
		// function of any public class.
		// The function should take no arguments and return nothing.
		// If the method is an intance member then the enclosing class is 
		// intantiated for each document. If the member is a static member then
		// the enclosing class is NOT intantiated.
		//
		// NOTE: CommandMethod has overloads where you can provide helpid and
		// context menu.

		// Modal Command with localized name
		[CommandMethod("MyGroup", "MyCommand", "MyCommandLocal", CommandFlags.Modal)]
		public void MyCommand() // This method can have any name
		{
			// Put your command code here

		}

		// Modal Command with pickfirst selection
		[CommandMethod("MyGroup", "MyPickFirst", "MyPickFirstLocal", CommandFlags.Modal | CommandFlags.UsePickSet)]
		public void MyPickFirst() // This method can have any name
		{
			PromptSelectionResult result = Application.DocumentManager.MdiActiveDocument.Editor.GetSelection();
			if (result.Status == PromptStatus.OK)
			{
				// There are selected entities
				// Put your command using pickfirst set code here
			}
			else
			{
				// There are no selected entities
				// Put your command code here


			}
		}

		// Application Session Command with localized name
		[CommandMethod("MyGroup", "MySessionCmd", "MySessionCmdLocal", CommandFlags.Modal | CommandFlags.Session)]
		public void MySessionCmd() // This method can have any name
		{
			// Put your command code here
		}
		#endregion

		#region Commands

		#region Module 0 - Layout
		/// <summary>
		/// Transforms arc to 3d polyline.
		/// </summary>
		private const string _123Bogen3d = "123Bogen3d";
		/// <summary>
		/// Exports blocks (not sure exactly how).
		/// </summary>
		private const string _123BlockExport = "123BlockExport";
		/// <summary>
		/// 
		/// </summary>
		private const string _123PEdit3d = "123PEdit3d";
		private const string _123QDrape = "123QDrape";
		private const string _123ASCII = "123ASCII";
		private const string _123EsriGridImport = "123ESRIGridImport";
		private const string _123Align = "123Align";
		private const string _123AttRegenAll = "123AttRegenAll";
		private const string _123AttLabel = "123AttLabel";
		private const string _123AttReset = "123AttReset";
		private const string _123AttMove = "123AttMove";
		private const string _123AttMoveCenter = "123AttMoveCenter";
		private const string _123AttSize = "123AttSize";
		private const string _123AttSymbolScale = "123AttSymbolScale";
		private const string _123AttNameGenerator = "123AttNameGenerator";
        private const string _123BlockRotate = "123AttBlockRotate";
		private const string _123CreateShaft = "123KSNEU";
		private const string _123PipeNetworkCreate = "123KS";
		private const string _123PipeNetworkCreateFromBlocks = "123KSBlocks";
		private const string _123PipeNetworkCreateRefresh = "123KSRegen";
		private const string _123PipeNetworkIsybauLoad= "123KSIsybau";
		private const string _123PipeNetworkEdit = "123KSOPT";
        private const string _123RenameShaft= "123KSNAME";
		private const string _123CreatePipe = "123KDraw";
		private const string _123KBlock = "123KBlock";
		private const string _123FlaecheToolbar = "123FlaecheToolbar";
		private const string _123LaengeToolbar = "123LaengeToolbar";
		private const string _123Gripeinfueg = "123Gripeinfueg";
		private const string _123Gripentf = "123Gripentf";
        private const string _123Attr2 = "123Attr2";
        private const string _123ReducePoints = "123ReduceXYZ";
        private const string _123ReducePointsTest = "123ReduceXYZPerformanceTest";
		private const string _123BenchCreatePipe = "123VisRohr3d"; // Visual rohr.
		private const string _123BenchCreateSchaft = "123VisSchacht3d"; // Visual scacht.
        private const string _123DtmBalance = "123DtmBalance";
		#endregion Module 0 

		#region Module 1 - Axis
		private const string _123PipeCreate = "123KHNEU";//"123PipeCreate";
		//private const string _123PipeCreate2 = "123KHNEU2";//"123PipeCreate";
		private const string _123AxOrtho66 = "123AxOrtho66";
		private const string _123Autotrack = "123Track";
		#endregion Module 1

		#region Module 3
		private const string _123Prof = "123Prof";
		private const string _123QSLabel = "123QSLabel";
		#endregion Module 3

		#region Module 5
		public const string _123Inventory = "123Inventary";
        private const string _123CadToInventary = "123CadToInventary";
		public const string _123Positions = "123Positions";
        #endregion Module 5

        private const string _123ClearProfilerData = "123ClearProfilerData";
		private const string _123ClearLabels = "123ClearLabels";
		private const string _123VersionInfo = "123VersionInfo";
		private const string _123Help = "123Help";
		private const string _123BATMAN = "123Battman";
        private const string _123BlockTextExplode = "123ATTURSPRUNG";
        private const string _123LogFile = "123LogFile";

        #region Not user commands
        private const string _123NOD_XML = "123_NOD_XML";
		private const string _123RectangleTest = "123_RectangleTest";
		private const string _123HatchExtensionExport = "123_HatchExtensionExport";
		private const string _123TransactionLog = "123_TransactionLog";
		#endregion



		#region Dusan Module 0
		public const string _123Dc = "123DC";
		#endregion
		#region Dusan Module 1
		private const string _123Island = "123Island";
		private const string _123Axis = "123Axis";
		private const string _123Auto = "123Auto";
		private const string _123DTM = "123Dtm";
		#endregion

		#region Dusan
		private const string _123Hatches = "123Hatches";
		#endregion
		#endregion

		/// <summary>
		/// Gets MdiActiveDocument.
		/// </summary>
		public static Document CurrentDocument { get { return Application.DocumentManager.MdiActiveDocument; } }
		/// <summary>
		/// Gets database of the MdiActiveDocument.
		/// </summary>
		public static Database CurrentDatabase { get { return CurrentDocument.Database; } }
		/// <summary>
		/// Gets editos of the MdiActiveDcoument.
		/// </summary>
		public static Editor CurrentEditor { get { return CurrentDocument.Editor; } }

		public static void MdiActiveDocument_CommandEnded(object sender, CommandEventArgs e)
		{
			switch (e.GlobalCommandName)
			{
				case "_123AXOPT":
				case "123AXOPT":
				case "123PROF":
				case "_123PROF":
				case "_123CLEARLABELS":
				case "123CLEARLABELS":
				case "_123QSLABEL":
				case "123QSLABEL":
				case "_123CLEARPROFILERDATA":
				case "123CLEARPROFILERDATA":
#if USE_LABELS
					Commands.Labels.Refresh(Application.DocumentManager.MdiActiveDocument);
#endif
					break;
				case "123LSOPT":
				case "_123LSOPT":
					Commands.KsoptCommands.RefreshAllLS();
					break;
			}
		}

		private static int GetCommandModule(string command)
		{
			// Layout = 0, Axis = 1, Longitudinal = 2, Cross = 3, Inventory = 5
			//int module = -1;
			Modules mod = Modules.NotSet;
			switch (command)
			{
				case _123Bogen3d:// "123POLY3D":
				case _123BlockExport:
				case _123PEdit3d:
				case _123DTM:
				case _123QDrape:
				case _123ASCII:
				case _123EsriGridImport:
				case _123Align:
				case _123AttRegenAll:
				case _123AttLabel:
				case _123AttReset:
				case _123AttMove:
				case _123AttMoveCenter:
				case _123AttSize:
				case _123AttSymbolScale:
				case _123BlockRotate:
				case _123AttNameGenerator:
				case _123CreateShaft:
				case _123PipeNetworkCreate:
				case _123PipeNetworkCreateFromBlocks:
				case _123PipeNetworkCreateRefresh:
				case _123PipeNetworkIsybauLoad:
				case _123PipeNetworkEdit:
                case _123RenameShaft:
				case _123CreatePipe:
				case _123KBlock:
				case _123FlaecheToolbar:
				case _123LaengeToolbar:
				case _123Gripeinfueg:
				case _123Gripentf:
                case _123Attr2:
                case _123ReducePoints:
                case _123ReducePointsTest:
				case _123Positions:
				case _123BenchCreatePipe:
				case _123BenchCreateSchaft:
				case _123DtmBalance:
					//module = 0;
					mod = Modules.Layout;
					break;
				case _123PipeCreate:
				//case _123PipeCreate2:
				case _123AxOrtho66:
				case _123Island:
				case _123Hatches:
				case _123Axis:
				case _123Auto:
				case _123Dc:
				case _123Autotrack:
					//module = 1;
					mod = Modules.Axis;
					break;
				case _123Prof:
				case _123QSLabel:
					//module = 3;
					mod = Modules.Crossection;
					break;
				case _123Inventory:
                case _123CadToInventary:
					//module = 5;
					mod = Modules.Inventary;
					
					break;
				default:
					//module = -1;
					mod = Modules.NotSet;
					break;
			}
			//return module;
			return (int)mod;
		}
		public static bool IsAutotrackActivated()
        {
			return IsCommandActivated(_123Autotrack);
        }
		public static bool IsCommandActivated(string command)
		{

			int module = GetCommandModule(command);

			C123CADCManager.C123CADCManager m = new C123CADCManager.C123CADCManager();
			int result = -1;
			if (module != -1)
				result = m.IsModuleActivated(Version.Number, module);
			bool activated = false;
			switch (result)
			{
				case 0:
					activated = true;
					break; // Activated
				case 1:
					Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage(" 123CAD ist nicht aktiv.\n" /*" Module not activated\n"*/);
					break; // Activation failed
				case 2:
					Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage(" Dieses Modul ist nicht aktiv.\n" /*" Module not activated\n"*/);
					break; // Activated software, but module isn't
				case 3:
					Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage(" " + Version.ToString() + " ist nicht aktiv.\n" /*" Module not activated\n"*/);
					break; // Activated, but version of module is different from activated version
			}


			MyLog.MyTraceSource.Information("Command(" + Version.ToString() + "): <" + command + "> is " + (activated ? "" : "not ") + " activated.");
			return activated;
		}

		#region Test commands
		//[CommandMethod("123TESTNOD")]
		public static void NOD()
		{
			Commands.Private.Nod.InvokeOld(Application.DocumentManager.MdiActiveDocument);
		}
		[CommandMethod("123ObjectHandle")]
		public static void _123objectHandle()
		{
			CADHandleHelpers.WriteObjectHandle(Application.DocumentManager.MdiActiveDocument);
		}
		[CommandMethod("123ObjectSelectHandle")]
		public static void _123objectSelectHandle()
		{
			CADHandleHelpers.SelectObjectByHandle(Application.DocumentManager.MdiActiveDocument);
		}
		[CommandMethod("123ExtensionDic")]
		public static void ExtensionDic()
		{
			Commands.Private.ExtensionDic.ShowForObject(Application.DocumentManager.MdiActiveDocument);
		}
		[CommandMethod("123XData")]
		public static void XData()
		{
			Commands.XDataView.CheckXData(Application.DocumentManager.MdiActiveDocument);
		}
		//[CommandMethod("123loadcui")]
		public static void LoadCUI()
		{
			Commands.Private.Unclassified.LoadCUI();
		}
#if DEBUG
		[CommandMethod("123CheckEntityCreator")]
#endif
		public static void CheckObjectSource()
		{
			// Checks where object has been created.
			// Works only with inventory.
			Commands.CheckEntityCreator.Start();
		}
		#endregion

		[CommandMethod(_123Autotrack, CommandFlags.Modal)]
		public void Command_123Autotrack()
        {
			if (!IsCommandActivated(_123Autotrack))
				return;
			new Autotrack.myCommands().Command_123DCNeu();
        }

		[CommandMethod(_123LogFile)]
        public void Command_123LogFile()
        {
            MyLog.MyTraceSource.Current.Flush();
            string name = MyLog.MyTraceSource.CurrentSourceFileName;
            Process.Start(name);
        }
		//[CommandMethod("komanda")]
		public void Commanda()
		{
			var d = GlobalDI.Kernel.Get<Document>();

			using (TransactionWrapper tw = d.StartTransaction())
			{
				tw.CreateEntity<Circle>().Diameter = 2;

				// Do commit even for readonly operations.
				tw.Commit();
			}

		}


		[CommandMethod(_123Prof)]
		public void Command_123Profiler()
		{
			if (!IsCommandActivated(_123Prof))
				return; 
			Commands._Profiler.Invoke(CurrentDocument);
		}

		/// <summary>
		/// Sets content and position of mtext, must be inside transaction
		/// </summary>
		[CommandMethod(_123QSLabel)]
		public void Command_123QsLabel()
		{
			if (!IsCommandActivated(_123QSLabel))
				return;
			Commands.Labels.DisplayLabel(CurrentDocument);
		}

		[CommandMethod(_123ClearProfilerData)]
		public void Command_123ClearProfilerData()
		{
			Commands.Labels.ClearProfilerData(CurrentDocument);
		}

		/// <summary>
		/// Deletes labels from program, and optionally deletes all profiler related data
		/// </summary>
		[CommandMethod(_123ClearLabels)]
		public void Command_123ClearLabels()
		{
			Commands.Labels.Clear(CurrentDocument);
		}

		[CommandMethod(_123AxOrtho66)]
		public static void Command_123AxOrtho66()
		{
			if (!IsCommandActivated(_123AxOrtho66))
				return;
			Commands.AxOrtho66.LoadStation3DPolylines(CurrentDocument);
		}

		[CommandMethod(_123Bogen3d)]
		public static void Command_123Bogen3d()
		{
			if (!IsCommandActivated(_123Bogen3d))
				return;
			Commands.Bogen3d.ArcTo3dPolyline(CurrentDocument);
		}

		[CommandMethod(_123BlockExport)]
		public void Command_123BlockExport()
		{
			if (!IsCommandActivated(_123BlockExport))
				return;
			Commands.BlockExport.Invoke(CurrentDocument);
		}

		[CommandMethod(_123ASCII)]
		public static void Command_123ascii()
		{
			if (!IsCommandActivated(_123ASCII))
				return;



			// Just test.
			IInvoke i = new Invoker<Document>(Commands.Ascii.Invoke, CurrentDocument);
			i.Invoke();
			//RunWithInvariantCulture(i);

			//Commands.Ascii.Invoke(CurrentDocument);
		}

		[CommandMethod(_123Align)]
		public static void Command_123Align()
		{
			if (!IsCommandActivated(_123Align))
				return;
			Commands.Align.Invoke(CurrentDocument);
		}

		[CommandMethod(_123VersionInfo)]
		public static void Command_123VersionInfo()
		{
			Func<Assembly, string> getFileAttribute = (a1) =>
				{
					AssemblyFileVersionAttribute fileAtt = a1.GetCustomAttribute(typeof(AssemblyFileVersionAttribute)) as AssemblyFileVersionAttribute;
					if (fileAtt != null)
						return fileAtt.Version;
					return "";
				};
			Assembly current = Assembly.GetExecutingAssembly();
			
			CurrentDocument.Editor.WriteMessage( current.FullName + " " + getFileAttribute(current) + "\n");
			var assemblies = current.GetReferencedAssemblies();
			Dictionary<string, AssemblyName> names = new Dictionary<string, AssemblyName>();
			foreach (var v in assemblies)
			{
				names.Add(v.FullName, v);
				//CurrentDocument.Editor.WriteMessage(v.Name + " : " + v.Version.ToString() + "\n");
			}
			Assembly[] loaded = AppDomain.CurrentDomain.GetAssemblies();
			foreach(Assembly a in loaded)
			{
				if (names.ContainsKey(a.FullName))
				{
					
					CurrentDocument.Editor.WriteMessage(a.FullName + " " + getFileAttribute(a) + "(" +names[a.FullName].Version.ToString() + ")\n");
				}
			}
		}

        [CommandMethod(_123BlockTextExplode)]
        public void command_123BlockTextExplode()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Commands.BlockExplode.StartSelectedBlock(doc);
        }

		[CommandMethod(_123Help)]
		public void command_123Help()
		{
			string path = "HKEY_CURRENT_USER\\SOFTWARE\\123cad";
			string fileEntry = "Help.chm";
			string val = (string)Microsoft.Win32.Registry.GetValue(path, fileEntry, null);
			Process.Start(val);
		}

		[CommandMethod(_123PEdit3d)]
		public void Command_123Pedit()
		{
			if (!IsCommandActivated(_123PEdit3d))
				return;
			Commands.Pedit3d.Invoke();
		}

        [CommandMethod(_123DtmBalance)]
        public void Command_123DtmBalance()
        {
            if (!IsCommandActivated(_123DtmBalance))
                return;
            var doc = Application.DocumentManager.MdiActiveDocument;
            new DTMBalancer().Command_DTMBalance(doc);
        }


		#region Block commands

		[CommandMethod(_123AttRegenAll)]
		public void command_123AttRegenAll()
		{
			BlocksAttributes.BlockCommands.AllBlocksRefresh(CurrentDocument, CurrentDatabase, CurrentEditor);
		}

		[CommandMethod(_123AttLabel)]
		public void Command_123AttLabel()
		{
			if (!IsCommandActivated(_123AttLabel))
				return;
			BlocksAttributes.BlockCommands.BlockCreate(CurrentDocument, CurrentDatabase, CurrentEditor);
		}

		[CommandMethod(_123AttReset)]
		public void Command_123AttReset()
		{
			if (!IsCommandActivated(_123AttReset))
				return;
			BlocksAttributes.BlockCommands.BlockAttributesReset(CurrentDocument, CurrentDatabase, CurrentEditor);
		}

		[CommandMethod(_123AttMove)]
		public void Command_123AttMove()
		{
			if (!IsCommandActivated(_123AttMove))
				return;
			BlocksAttributes.BlockCommands.BlockMove(CurrentDocument, CurrentDatabase, CurrentEditor);
		}

		[CommandMethod(_123AttMoveCenter)]
		public void Command_123AttMoveCenter()
		{
			if (!IsCommandActivated(_123AttMoveCenter))
				return;
			BlocksAttributes.BlockCommands.BlockCenterMove(CurrentDocument, CurrentDatabase, CurrentEditor);
		}

		[CommandMethod(_123AttSize)]
		public void Command_123AttSize()
		{
			if (!IsCommandActivated(_123AttSize))
				return;
			BlocksAttributes.AttributeEdit.AttributeEditController.AttributeEdit(CurrentDocument, CurrentDatabase, CurrentEditor);
		}

		[CommandMethod(_123NOD_XML)]
		public static void _123NodXML()
		{
			using (Transaction tr = CurrentDatabase.TransactionManager.StartTransaction())
			{
				DBDictionary nod = tr.GetObject(CurrentDatabase.NamedObjectsDictionaryId, OpenMode.ForRead) as DBDictionary;
				Commands.Private.Nod.InvokeNew(tr, nod);
			}
		}
#if DEBUG
		/// <summary>
		/// Enabled only in Debug mode, because of perfromance.
		/// Writes location for each created entity.
		/// </summary>
		[CommandMethod(_123TransactionLog)]
		public static void _123TransactionLogger()
		{
			TransactionWrapper.IsConsoleOutputEnabled = !TransactionWrapper.IsConsoleOutputEnabled;
			using (Transaction tr = CurrentDatabase.TransactionManager.StartTransaction())
			{
				DBDictionary nod = tr.GetObject(CurrentDatabase.NamedObjectsDictionaryId, OpenMode.ForRead) as DBDictionary;
				Commands.Private.Nod.InvokeNew(tr, nod);
			}
		}
#endif

		#region Not used
		//[CommandMethod("123AttRegen")]
		public void BlockRefresh()
		{
			if (!IsCommandActivated("123AttRegen"))
				return;
			BlocksAttributes.BlockCommands.BlockRefresh(CurrentDocument, CurrentDatabase, CurrentEditor);
		}
		//[CommandMethod("123BlockBoxRemove")]
		public void BlockBoxRemove()
		{
			BlocksAttributes.BlockCommands.BlockBoxRemove(CurrentDocument, CurrentDatabase, CurrentEditor);
		}
		//[CommandMethod("123BlockBoxCreate")]
		public void BlockBoxCreate()
		{
			BlocksAttributes.BlockCommands.BlockBoxCreate(CurrentDocument, CurrentDatabase, CurrentEditor);
		}
		//[CommandMethod("123BlockLeaderCreate")]
		public void BlockLeaderCreate()
		{
			// At.BlockCommands.BlockLeaderCreate(currentDocument, currentDatabase, currentEditor);
		}
		//[CommandMethod("123BlockLeaderRemove")]
		public void BlockLeaderRemove()
		{
			//At.BlockCommands.BlockLeaderRemove(currentDocument, currentDatabase, currentEditor);
		}
		#endregion

		#endregion

		[CommandMethod(_123AttSymbolScale)]
		public static void CommandAttSymbolScale()
		{
			if (!IsCommandActivated(_123AttSymbolScale))
				return;
			SewageCommands.MyCommands.scaleSymbol();
		}

        [CommandMethod(_123BlockRotate)]
        public static void CommandAttRotate()
        {
            if (!IsCommandActivated(_123BlockRotate))
                return;
            BlockRotationCommand.Run(CurrentDocument);
        }
		[CommandMethod(_123AttNameGenerator)]
		public static void Command_123AttNameGenerator()
		{
			if (!IsCommandActivated(_123AttNameGenerator))
				return;
			SewageCommands.MyCommands.ShaftRename();
        }
        [CommandMethod(_123CreateShaft)]
        public static void Command_123CreateShaft()
        {
            if (!IsCommandActivated(_123CreateShaft))
                return;
            SewageCommands.MyCommands.CreateShaft();
		}
		[CommandMethod(_123PipeNetworkCreate)]
		public static void Command_123PipeNetwork()
		{
			if (!IsCommandActivated(_123PipeNetworkCreate))
				return;
			Commands.KsoptCommands.StartKSoptNew(Application.DocumentManager.MdiActiveDocument);
			//Commands.PipeNetwork.Run(Application.DocumentManager.MdiActiveDocument);
		}
		[CommandMethod(_123PipeNetworkCreateFromBlocks)]
		public static void Command_123PipeNetworkBlocks()
		{
			if (!IsCommandActivated(_123PipeNetworkCreateFromBlocks))
				return;
			Commands.KsoptCommands.StartKSoptNewFromBlocks(Application.DocumentManager.MdiActiveDocument);
			//Commands.PipeNetwork.Run(Application.DocumentManager.MdiActiveDocument);
		}
		[CommandMethod(_123PipeNetworkCreateRefresh)]
		public static void Command_123PipeNetworkRefresh()
		{
			if (!IsCommandActivated(_123PipeNetworkCreateRefresh))
				return;
			Commands.KsoptCommands.RefreshAllLS();
			//Commands.PipeNetwork.Run(Application.DocumentManager.MdiActiveDocument);
		}
		[CommandMethod(_123PipeNetworkIsybauLoad)]
		public static void Command_123PipeNetworkIsybauLoad()
		{
			if (!IsCommandActivated(_123PipeNetworkIsybauLoad))
				return;
			Commands.KsoptCommands.CommandKSOPT_LoadIsybau();
			//Commands.PipeNetwork.Run(Application.DocumentManager.MdiActiveDocument);
		}
		[CommandMethod(_123PipeNetworkEdit)]
		public static void Command_123PipeNetworkEdit()
		{
			if (!IsCommandActivated(_123PipeNetworkEdit))
				return;
			Commands.KsoptCommands.StartKSoptEdit(Application.DocumentManager.MdiActiveDocument);
			//Commands.PipeNetwork.Run(Application.DocumentManager.MdiActiveDocument);
		}

		[CommandMethod(_123RenameShaft)]
		public static void Command_123RenameShaft()
		{
			if (!IsCommandActivated(_123RenameShaft))
				return;
			SewageCommands.MyCommands.ShaftRename();
		}
		[CommandMethod(_123CreatePipe)]
		public static void Command_123CreatePipe()
		{
			if (!IsCommandActivated(_123CreatePipe))
				return;
			SewageCommands.MyCommands.CreatePipe();
		}
		[CommandMethod(_123KBlock)]
		public static void Command_123KBlock()
		{
			if (!IsCommandActivated(_123KBlock))
				return;
			SewageCommands.MyCommands.CreatePolylineBlocks();
		}

		[CommandMethod(_123BenchCreatePipe)]
		public static void Command_123VisualCreatePipe()
        {
			if (!IsCommandActivated(_123BenchCreatePipe))
				return;
			SewageElementsStandaloneDrawer.CreatePipe(Application.DocumentManager.MdiActiveDocument);
        }
		[CommandMethod(_123BenchCreateSchaft)]
		public static void Command_123VisualCreateShaft()
        {
			if (!IsCommandActivated(_123BenchCreateSchaft))
				return;
			SewageElementsStandaloneDrawer.CreateShaft(Application.DocumentManager.MdiActiveDocument);
        }


		[CommandMethod(_123Attr2)]
        public static void CommandAttr()
        {
            if (!IsCommandActivated(_123Attr2))
                return;
            Commands.Attr.Start(Application.DocumentManager.MdiActiveDocument);
            
        }

        [CommandMethod(_123ReducePoints)]
        public static void ReducePoints()
        {
            if (!IsCommandActivated(_123ReducePoints))
                return;
            CadPlugin.PointReduction.PointReductionCommand.Run(Application.DocumentManager.MdiActiveDocument);
        }
        [CommandMethod(_123ReducePointsTest)]
        public static void ReducePointsTest()
        {
            if (!IsCommandActivated(_123ReducePointsTest))
                return;
            CadPlugin.PointReduction.PointReductionCommand.RunPerformanceTests(Application.DocumentManager.MdiActiveDocument);
        }

        //[CommandMethod(_123RectangleTest)]
        public static void Command_RectangleTest()
		{
			Commands.Private.RectangleAlgorithmTest.TestRectangle();
		}
		[CommandMethod("123SolidTest")]
		public static void CommandSolidTest()
		{
			Commands.Private.SolidPipeTransformTest.Invoke();
		}
		[CommandMethod("123MeshTest")]
		public static void CommandMeshTest()
		{
			Commands.Private.MeshPipeTest.Invoke();
		}
		#region Sewage commands


		// Inventory command, loaded only if dll exists.
		// Do not change the name of method, because it is hardcoded in string for compilation!
		//[CommandMethod("123Inventory")]
		public static void Command_123Inventory() 
		{
			if (!IsCommandActivated(_123Inventory))
				return;
            Commands.InventoryCommands.StartCommand(Application.DocumentManager.MdiActiveDocument);

		}

		[CommandMethod(_123EsriGridImport)]
		public static void Command_123LoadSpecialAscii()
		{
			if (!IsCommandActivated(_123EsriGridImport))
				return;
			Commands.SpecialAsciiLoader.Start(Application.DocumentManager.MdiActiveDocument);
		}
#if TEST

        [CommandMethod(_123CadToInventary)]
#endif
        public static void Command_123CadToInventory()
        {
            if (!IsCommandActivated(_123CadToInventary))
                return;
            Commands.CadToInventory.Invoke(Application.DocumentManager.MdiActiveDocument);
        }


		[CommandMethod(_123Positions)]
		public static void Command_123Positions()
		{
			if (!IsCommandActivated(_123Positions))
				return;
			PositionMaker.Models.CadInterface.CadPositionManager.Start(Application.DocumentManager.MdiActiveDocument);
		}
		[CommandMethod(_123BATMAN)]
		public static void Command_123Batman()
		{
		Commands.BlockAttributeManager.Invoke(Application.DocumentManager.MdiActiveDocument);
		}

		//static DocumentLock dl;
#if TEST
        [CommandMethod("123TEST")]
#endif
        public static void test()
        {
            Tests.TestManager.RunTests();
        }

#if TEST
        [CommandMethod("123TESTKeyword")]
#endif
		public static void TestKeyword()
		{
			Commands.Private.KeywordTests.TestKeyword();
		}

		//[CommandMethod(_123PipeCreate)]
		//public static void Pipecreate()
		//{
		//	if (!IsCommandActivated(_123PipeCreate))
		//		return;
		//	Commands.PipeBySegment.CreateShaftPipe(Application.DocumentManager.MdiActiveDocument);
		//}
		[CommandMethod(_123PipeCreate)]
		public static void Pipecreate2()
		{
			if (!IsCommandActivated(_123PipeCreate))
				return;
			//Commands.PipeBySegment.CreateShaftPipe(Application.DocumentManager.MdiActiveDocument);
			KHNeuController.StartWindow(Application.DocumentManager.MdiActiveDocument);
		}


		#endregion

		#region Hatch commands

		[CommandMethod(_123FlaecheToolbar, CommandFlags.Redraw)]
		public static void HatchToolbarShow()
		{
			if (!IsCommandActivated(_123FlaecheToolbar))
				return;
			Commands.HatchToolbar.ToolbarShow();
		}
		[CommandMethod(_123LaengeToolbar, CommandFlags.Redraw)]
		public static void LinetypeToolbarShow()
		{
			if (!IsCommandActivated(_123LaengeToolbar))
				return;
			Commands.LinetypeToolbar.ToolbarShow();
		}
		/*
		[CommandMethod("123HatchTableCreate", CommandFlags.Redraw)]
		public static void HatchCreateTable()
		{
			Commands.HatchToolbar.HatchToolbar_CreateTable();
		}

		[CommandMethod("123HatchTableDelete", CommandFlags.Redraw)]
		public static void HatchDeleteTable()
		{
			Commands.HatchToolbar.HatchToolbar_DeleteTable();
		}
		[CommandMethod("123HatchTableAddArea", CommandFlags.Redraw)]
		public static void HatchTableAddArea()
		{
			Commands.HatchToolbar.HatchToolbar_AppendAreaToTable();
		}
		[CommandMethod("123HatchTableRemoveArea", CommandFlags.Redraw)]
		public static void HatchTableRemoveArea()
		{
			Commands.HatchToolbar.HatchToolbar_RemoveAreaFromtTable();
		}
		[CommandMethod("123HatchSelectAreaTables", CommandFlags.Redraw)]
		public static void HatchSelectAreaTables()
		{
			HatchToolbars.HatchCommands.AreaTablesToSelection(Application.DocumentManager.MdiActiveDocument);
		}
		[CommandMethod("123HatchSelectTableArea", CommandFlags.Redraw)]
		public static void HatchSelectTableArea()
		{
			HatchToolbars.HatchCommands.AreaToSelection(Application.DocumentManager.MdiActiveDocument);
		}

		[CommandMethod("123HatchSelectTableAllAreas", CommandFlags.Redraw)]
		public static void HatchSelectTableAreas()
		{
			HatchToolbars.HatchCommands.AllAreasToSelection(Application.DocumentManager.MdiActiveDocument);
		}
		*/


		[CommandMethod(_123HatchExtensionExport, CommandFlags.Redraw)]
		public static void Command_ExportExtensionDic()
		{
			Commands.HatchToolbar.ExportExtensionDic(Application.DocumentManager.MdiActiveDocument);
		}

		[CommandMethod(_123Gripeinfueg)]
		public static void Command_InsertPolylineVertex()
		{
			if (!IsCommandActivated(_123Gripeinfueg))
				return;
			Commands.PolylineVertex.Insert(Application.DocumentManager.MdiActiveDocument);
		}
		[CommandMethod(_123Gripentf)]
		public static void Command_ReemovePolylineVertex()
		{
			if (!IsCommandActivated(_123Gripentf))
				return;
			Commands.PolylineVertex.Remove(Application.DocumentManager.MdiActiveDocument);
		}

		#endregion

#if OLD_SIMPLE_ACTIVATION
		public static bool CheckActivation()
		{
			try
			{
				C123CADCManager.C123CADCManager m = new C123CADCManager.C123CADCManager();
				Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage(m.IsModuleActivated(0) == 0?"Activated":"Not activated");

				return m.IsModuleActivated(0) == 0;

				//32 bita
				string path = "HKEY_CURRENT_USER\\SOFTWARE\\123cad\\Activation";
				string val = (string)Microsoft.Win32.Registry.GetValue(path, "Customer_Key322", null);
				int version = int.Parse(val.Substring(12, 2) + val.Substring(19, 2));
				if (version != int.Parse(Version))
				{
					Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage("Nur Version "/*"Only version "*/ + int.Parse(Version) + " ist erlaubt! Aktuelle Version "/*" is allowed! Current version "*/ + version);
					return false;
				}

				if (!dtm_protection.ActivationManager.CheckActivation())
					return false;
				return true;
			}
			catch (System.Exception)
			{
				/*try
				{
					//64 bita - ima li potrebe???
					string path = "HKEY_CURRENT_USER\\SOFTWARE\\123cad\\Activation";
					string val = (string)Microsoft.Win32.Registry.GetValue(path, "Customer_Key322", null);
					int version = int.Parse(val.Substring(12, 2) + val.Substring(19, 2));
					if (version != int.Parse(Version))
						return false;
					return true;
				}
				catch (System.Exception ee)
				{*/
				Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage("Error occured, following commands will not work! \n1232PROF \n123POLY3D \n123ASCIINEW \n123BlockExport");
				//}
			}
			return false;
		}
#endif

		#region DUSAN

		[CommandMethod(_123Island, _123Island, _123Island, CommandFlags.Modal | CommandFlags.Session)]
		public void Command_123Island()
		{
			if (!IsCommandActivated(_123Island))
				return;
			var v = new Arc2013.MyCommands();
			v.IslandsCommand();
		}

		[CommandMethod(_123Hatches, _123Hatches, _123Hatches, CommandFlags.Modal | CommandFlags.Session)]
		public static void Command_123Hatches()
		{
			if (!IsCommandActivated(_123Hatches))
				return;
			var v = new Arc2013.MyCommands();
			v.OurHatchesCommand();
		}

		[CommandMethod(_123Axis)]
		public static void Command_123axis()
		{
			if (!IsCommandActivated(_123Axis))
				return;
			if (Klotoida.MyCommands.version < 2)
				return;
			Klotoida.MyCommands.CommandAxis();
			//new Klotoida.MyCommands().CommandAxis();           
		}

		[CommandMethod(_123Auto)]
		public static void Command_123Auto()
		{
			if (!IsCommandActivated(_123Auto))
				return;
			if (Klotoida.MyCommands.version < 2)
				return;
			Klotoida.MyCommands.CommandAuto();
			//new Klotoida.MyCommands().CommandAuto();
		}

		public static bool loadedDTM = true;

		[CommandMethod(_123DTM)]
		public static void _123Dtm()
		{
			if (!IsCommandActivated(_123DTM))
				return;
#if !DEBUG
			if (!loadedDTM)
			{
				WindowsForms.MessageBox.Show("DTM Programm nicht geladen!");
				return;
			}
#endif
			new DTM.MyCommands().CommandDtm();
		}

		[CommandMethod(_123QDrape)]
		public static void Command_123QDrape()
		{
			if (!IsCommandActivated(_123QDrape))
				return;

			new DTM.MyCommands().DrapeCommand();
		}

		// Dynamical curves command, loaded only if dll exists.
		// Do not rename metod name, because it is hardcoded in string for compilation!
		//[CommandMethod("123DC")]
		public static void Command_123DynamicalCurves()
		{
			if (!IsCommandActivated("123DC"))
				return;
			new DynamicCurves.MyCommands().CommandDC();

		}
		#endregion


	}
	namespace My
	{
		enum Reason
		{
			CANCEL,
			OTHER
		}
		class UnsuportedOperationException : System.Exception
		{
			internal string message;
			internal UnsuportedOperationException(string _message = "")
			{
				message = _message;
			}
		}
		class CanceledOperationException : System.Exception
		{

		}
		class InvalidDataException : System.Exception
		{

		}
		enum ObjectType
		{
			ARC,
			POLYLINE,//kasnije mozda treba da podrzim i polyline2d i polyline3d
			UNSUPORTED
		}

	}
}