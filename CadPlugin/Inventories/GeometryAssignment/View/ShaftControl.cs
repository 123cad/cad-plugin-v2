﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using CadPlugin.Inventories.GeometryAssignment.Data;
using Inventory.CAD.GeometryAssignment.Data;
using System.Reflection;

namespace CadPlugin.Inventories.GeometryAssignment.View
{
    public partial class ShaftControl : UserControl
    {
        private readonly MainForm form;
        private BindingSource shaftSource; //= new BindingSource();
        private BindingList<ShaftAdapter> shaftList;// = new BindingList<ShaftAdapter>();

        internal event Action<IAdapter> SelectionChanged;

        /// <summary>
        /// Currently selected pipe.
        /// </summary>
        internal ShaftAdapter SelectedShaft { get; private set; }

        /// <summary>
        /// Shaft is not selected any more.
        /// </summary>
        public event Action CancelShaftSelection;
        

        private CadManager cadMgr;

        /// <summary>
        /// Raised when user assigns new geometry to a pipe.
        /// </summary>
        internal event Action<ShaftAdapter> ShaftGeometryAssigned;


        internal ShaftControl(MainForm toolbar, CadManager cadMgr)
        {
            form = toolbar;
            this.cadMgr = cadMgr;
            form.GlobalKeyUp += Form_GlobalKeyUp;
            InitializeComponent();

            dgvShafts.AutoGenerateColumns = false;
            //dgvShafts.DataSource = new BindingSource(shaftList, null);

            typeof(DataGridView).InvokeMember(
               "DoubleBuffered",
               BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty,
               null,
               dgvShafts,
               new object[] { true });

            InitializeColumns();

            dgvShafts.DataError += (_, e) =>
            {
                errorProviderDgvCell.Clear();

                // This is enough to 
                if (e.Context.HasFlag(DataGridViewDataErrorContexts.LeaveControl))
                {
                    dgvShafts.RefreshEdit();
                    dgvShafts.EndEdit();
                    e.Cancel = false;
                    return;
                }

                var t = dgvShafts.EditingControl as TextBox;

                errorProviderDgvCell.SetError(t, "Invalid value! Press Esc to cancel.");
                // Move the icon so it is visible inside TextBox.
                errorProviderDgvCell.SetIconPadding(t, -20);
            };
            // To support DataError event - if user clicks inside dgv, no LeaveControl flag
            // is set, so we handle that here.
            dgvShafts.MouseDown += (_, e) =>
            {

                if (!dgvShafts.IsCurrentCellInEditMode)
                    return;
                var info = dgvShafts.HitTest(e.X, e.Y);
                if (info.ColumnIndex != dgvShafts.CurrentCell.ColumnIndex ||
                    info.RowIndex != dgvShafts.CurrentCell.RowIndex)
                {
                    dgvShafts.CommitEdit(DataGridViewDataErrorContexts.Commit | DataGridViewDataErrorContexts.LeaveControl);
                }
                if (info.Type == DataGridViewHitTestType.None)
                {
                    // If no data error occurs, cell will still be in edit mode.
                    if (dgvShafts.IsCurrentCellInEditMode)
                        dgvShafts.EndEdit();
                    if (info.Type == DataGridViewHitTestType.None)
                        dgvShafts.CurrentCell = null;
                }
            };

            dgvShafts.CellEndEdit += (_, e) =>
            {
                errorProviderDgvCell.Clear();
            };

            dgvShafts.CurrentCellChanged += DgvShafts_CurrentCellChanged;

            Action<MouseButtons> enterEditModeOnClick = (b) =>
            {
                if (b == MouseButtons.Left)
                {
                    if (!dgvShafts.IsCurrentCellInEditMode)
                        dgvShafts.BeginEdit(false);
                }
            };
            dgvShafts.CellMouseDoubleClick += (_, e) =>
            {
                enterEditModeOnClick(e.Button);
            };
            dgvShafts.CellContentClick += (_, e) =>
            {
                DataGridViewColumn c = dgvShafts.Columns[e.ColumnIndex];
                if (c is DataGridViewButtonColumn && e.RowIndex >= 0)
                {
                    ShaftAdapter sa = dgvShafts.Rows[e.RowIndex].DataBoundItem as ShaftAdapter;
                    // Select new object for a shaft
                    form.Visible = false;
                    if (cadMgr.AssignShaftEntity(sa))
                    {
                        //ShaftUpdated?.Invoke(sa);
                        ShaftGeometryAssigned?.Invoke(sa);
                    }
                    form.Visible = true;
                }
                else if (c is DataGridViewComboBoxColumn)
                {
                    dgvShafts.BeginEdit(false);
                }
            };

            //ShaftAdapter currentAdapter = null;
            /*dgvShafts.CellMouseClick += (_, e) =>
            {
                if (e.Button != MouseButtons.Right)
                    return;

                if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    if (dgvShafts.CurrentCell != dgvShafts[e.ColumnIndex, e.RowIndex] ||
                        !dgvShafts.IsCurrentCellInEditMode)
                    {
                        dgvShafts.CurrentCell = dgvShafts[e.ColumnIndex, e.RowIndex];
                        currentAdapter = dgvShafts.CurrentRow.DataBoundItem as ShaftAdapter;
                        var rec = dgvShafts.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                        contextMenuStripShaft.Show(dgvShafts, new Point(e.Location.X + rec.X, e.Location.Y + rec.Y));

                    }
                }
            };*/
            dgvShafts.CellMouseClick += (_, e) =>
            {
                if (e.ColumnIndex == -1)
                    return;
                if (e.RowIndex == -1)
                {
                    var column = dgvShafts.Columns[e.ColumnIndex];
                    string sort = $"{column.DataPropertyName} ASC";
                    if (sort == shaftSource.Sort)
                    {
                        sort = $"{column.DataPropertyName} DESC";
                    }

                    shaftSource.Sort = sort;

                    /*ListSortDirection dir = ListSortDirection.Ascending;
                    if (dgvPipes.SortedColumn != null && e.ColumnIndex == dgvPipes.SortedColumn.Index)
                    {
                        if (dgvPipes.SortOrder == SortOrder.Ascending)
                            dir = ListSortDirection.Descending;
                    }
                    dgvPipes.Sort(dgvPipes.Columns[e.ColumnIndex], dir);*/
                    //pipeSource.DataSource = pipeList.OrderBy(x=>x.)
                }
            };
            dgvShafts.LostFocus += (_, __) =>
            {
                if (!form.IsSelectionMode)
                    CancelShaftSelection?.Invoke();
            };
            dgvShafts.RowsAdded += (_, e) =>
            {
                int columnIndex = ColumnShaftEntity.Index;
                for (int i = e.RowIndex; i < e.RowIndex + e.RowCount; i++)
                {
                    dgvShafts[columnIndex, i].Value = "...";
                    dgvShafts[columnIndex, i].ToolTipText = "Select new entity for shaft";
                };
            };
            // Needed, so user can close the form (cancel operation) with invalid value.
            /*form.FormClosing += (_, e) =>
            {
                if (dgvShafts.IsCurrentCellInEditMode)
                {
                    dgvShafts.CancelEdit();
                    dgvShafts.EndEdit();
                    // It has been set to true - suppose DGV set that.
                    e.Cancel = false;
                }
            };*/
            dgvShafts.CellPainting += (_, e) =>
            {
                // Button column does not have possibility of disabling the button.
                // To keep it simple, we just don't draw the button.

                if (e.RowIndex == -1)
                    return;
                if (e.ColumnIndex == ColumnShaftEntity.Index)
                {
                    e.PaintBackground(e.CellBounds, false);
                    e.PaintContent(e.CellBounds);

                    Rectangle res = new Rectangle(e.CellBounds.X + e.CellBounds.Width / 2 - 8,
                                                    e.CellBounds.Y + e.CellBounds.Height / 2 - 8,
                                                    16,
                                                    16);
                    e.Graphics.DrawImage(Properties.ResourceInventory.select, res);
                    e.Handled = true;
                }
            };
        }
        public void Initialize(IEnumerable<Shaft> shafts)
        {
            if (shafts != null)
                shaftList = new SortableBindingList<ShaftAdapter>(shafts.Select(x => new ShaftAdapter(x)).ToList());
            else
                shaftList = new SortableBindingList<ShaftAdapter>(); 
            shaftSource = new BindingSource(shaftList, null);
            dgvShafts.DataSource = shaftSource;
        }
        private ShaftAdapter GetCurrent()
        {
            if (dgvShafts.CurrentCell == null)
                return null;
            if (dgvShafts.CurrentCell.RowIndex == -1)
                return null;
            return dgvShafts.CurrentRow.DataBoundItem as ShaftAdapter;
        }

        private void DgvShafts_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dgvShafts.CurrentCell == null)
            {
                SelectedShaft = null;
                CancelShaftSelection?.Invoke();
            }
            else
            {
                ShaftAdapter pd = dgvShafts.CurrentRow.DataBoundItem as ShaftAdapter;
                SelectedShaft = pd;
                // This caused flickering on Show form.
                //formVisibility.TempHide();
                //cadMgr.SetObjectSelected(pd);
                //formVisibility.TempShow();
                SelectionChanged?.Invoke(pd);
            }
        }

        internal List<ShaftAdapter> GetAllShafts()
        {
            return shaftList.ToList();
        }

        private void Form_GlobalKeyUp(Keys obj)
        {
            switch (obj)
            {
                case Keys.Escape:
                    if (dgvShafts.IsCurrentCellInEditMode)
                    {
                        // Revert to old value (doesn't end CurrentCellEditMode)
                        dgvShafts.CancelEdit();
                        dgvShafts.EndEdit();
                    }
                    break;
                case Keys.Enter:
                    if (dgvShafts.IsCurrentCellInEditMode)
                    {
                        // Try to end editing mode. 
                        dgvShafts.EndEdit();
                    }
                    break;
            }
        }

        private void InitializeColumns()
        {
            DataGridView shafts = dgvShafts;

            DataGridViewTextBoxColumn columnText;
            DataGridViewComboBoxColumn columnCombo;
            DataGridViewCheckBoxColumn columnCheck;


            columnCheck = shafts.Columns[nameof(ColumnShaftGeometryExists)] as DataGridViewCheckBoxColumn;
            Debug.Assert(columnCheck != null);
            columnCheck.DataPropertyName = nameof(Data.PipeAdapter.IsybauGeometryExists);

            columnCheck = shafts.Columns[nameof(ColumnShaftGeometryAssigned)] as DataGridViewCheckBoxColumn;
            Debug.Assert(columnCheck != null);
            columnCheck.DataPropertyName = nameof(Data.PipeAdapter.IsGeometryAssigned);

            columnText = shafts.Columns[nameof(ColumnShaftName)] as DataGridViewTextBoxColumn;
            Debug.Assert(columnText != null);
            columnText.DataPropertyName = nameof(Data.ShaftAdapter.Name);// "Name";

            columnCombo = shafts.Columns[nameof(ColumnShaftObjektArt)] as DataGridViewComboBoxColumn;
            Debug.Assert(columnCombo != null);
            columnCombo.DataSource = Isybau2015.ReferenceTables.Common.Objektart.Get().Values;
            columnCombo.DisplayMember = nameof(Isybau2015.ReferenceTables.ReferencePair.Value);// "Value";
            columnCombo.ValueMember = nameof(Isybau2015.ReferenceTables.ReferencePair.Key); //"Key";
            columnCombo.DataPropertyName = nameof(Data.ShaftAdapter.ObjektArt);// "ObjektArt";
            columnCombo.ReadOnly = true;

            columnCombo = shafts.Columns[nameof(ColumnShaftGeoobjektArt)] as DataGridViewComboBoxColumn;
            Debug.Assert(columnCombo != null);
            // Pipe objects can have only specific GeoObjektArt
            HashSet<string> geoObjektArts = new HashSet<string>();
            geoObjektArts.Add("1");
            geoObjektArts.Add("2");
            geoObjektArts.Add("3");
            geoObjektArts.Add("8");
            geoObjektArts.Add("9");
            geoObjektArts.Add("10");
            geoObjektArts.Add("11");
            geoObjektArts.Add("12");
            geoObjektArts.Add("13");
            columnCombo.DataSource = Isybau2015.ReferenceTables.Common.GeoobjektArt.Get().Values.Where(x => geoObjektArts.Contains(x.Key)).ToList();
            columnCombo.DisplayMember = nameof(Isybau2015.ReferenceTables.ReferencePair.Value);// "Value";
            columnCombo.ValueMember = nameof(Isybau2015.ReferenceTables.ReferencePair.Key); //"Key";
            columnCombo.DataPropertyName = nameof(Data.ShaftAdapter.GeoobjektArt);// "GeoobjektArt";

            columnText = shafts.Columns[nameof(ColumnShaftDiameter)] as DataGridViewTextBoxColumn;
            Debug.Assert(columnText != null);
            columnText.DataPropertyName = nameof(Data.ShaftAdapter.DiameterMM);// "Diameter";
            columnText.HeaderText = "Diameter (mm)";
            columnText.InheritedStyle.Format = "N0";
        }

        private void dgvShafts_MouseEnter(object sender, EventArgs e)
        {
            dgvShafts.Focus();
        }

        private void dgvShafts_MouseLeave(object sender, EventArgs e)
        {
            ParentForm.Focus();
        }

        /*private void buttonAddKnoten_Click(object sender, EventArgs e)
        {
            ShaftAdapter sa = new ShaftAdapter();
            form.Visible = false;
            if (cadMgr.AssignShaftEntity(sa))
            {
                shaftList.Add(sa);
                var row = dgvShafts.Rows[dgvShafts.RowCount - 1];
                row.Cells[ColumnShaftEntity.Index].Value = "Select";

                int currentColumn = 0;
                if (dgvShafts.CurrentCell != null)
                    currentColumn = dgvShafts.CurrentCell.ColumnIndex;
                dgvShafts.CurrentCell = null;// dgvShafts[currentColumn, row.Index];
            }
            form.Visible = true;

        }*/

        /* private void buttonRemoveKnoten_Click(object sender, EventArgs e)
         {
             ShaftAdapter sa = GetCurrent();
             if (sa != null)
             {
                 ShaftRemoved?.Invoke(sa);

                 int currentRow = dgvShafts.CurrentCell.RowIndex;
                 int currentColumn = dgvShafts.CurrentCell.ColumnIndex;
                 shaftList.Remove(sa);
                 if (shaftList.Count > 0)
                 {
                     if (currentRow > 0)
                     {
                         if (currentRow >= shaftList.Count)
                             currentRow--;
                     }
                     dgvShafts.CurrentCell = dgvShafts[currentColumn, currentRow];
                 }
             }
         }*/
    }
}
