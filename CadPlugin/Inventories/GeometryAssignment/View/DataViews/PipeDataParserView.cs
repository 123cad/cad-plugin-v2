﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.Inventories.GeometryAssignment.DataFetch.DataViews
{
    public partial class PipeDataParserView : Form, IPipeDataParserView
    {
        internal PipeDataParserView(PipeLoadedData d)
        {
            InitializeComponent();

            buttonCancel.Click += (_, __) => Cancel?.Invoke();
            buttonApply.Click += (_, __) => Apply?.Invoke();

            buttonDiameter.Click += (_, __) => SelectDiameter?.Invoke();
            buttonMaterial.Click += (_, __) => SelectMaterial?.Invoke();
            buttonSlope.Click += (_, __) => SelectSlope?.Invoke();

            buttonSelectAllText.Click += (_, __) => SelectAll?.Invoke();

            string textBoxProperty = nameof(TextBox.Text);
            // Used like this because we want to not display anything if data has not been entered.
            Binding b = new Binding(textBoxProperty, d, nameof(PipeLoadedData.Diameter));
            b.Format += (_, e) =>
            {
                bool isSet = d.IsDiameterSet;
                e.Value = isSet ? e.Value : "";
            };
            textBoxDiameter.DataBindings.Add(b);
            // Used like this because we want to not display anything if data has not been entered.
            b = new Binding(textBoxProperty, d, nameof(PipeLoadedData.Material));
            b.Format += (_, e) =>
            {
                bool isSet = d.IsMaterialSet;
                e.Value = isSet ? e.Value : "";
            };
            textBoxMaterial.DataBindings.Add(b);
            // Used like this because we want to not display anything if data has not been entered.
            b = new Binding(textBoxProperty, d, nameof(PipeLoadedData.Slope));
            b.Format += (_, e) =>
            {
                bool isSet = d.IsSlopeSet;
                e.Value = isSet ? e.Value : "";
            };
            textBoxSlope.DataBindings.Add(b);
            
        }

        public TextBox Diameter => textBoxDiameter;


        public TextBox Material => textBoxMaterial;

        public TextBox Slope => textBoxSlope;

        public event Action Apply;
        public event Action Cancel;
        public event Action SelectAll;
        public event Action SelectDiameter;
        public event Action SelectMaterial;
        public event Action SelectSlope;

        private const int WM_KEYUP = 0x0101;


        /// <summary> 
        /// NOTE Only this combination worked for editing control ESC key press.
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyPreview(ref Message m)
        {
            if (m.Msg == WM_KEYUP)
            {
                switch (m.WParam.ToInt32())
                {
                    case (int)Keys.Enter:
                        TextBox focused = null;
                        if (textBoxDiameter.Focused)
                            focused = textBoxDiameter;
                        if (textBoxMaterial.Focused)
                            focused = textBoxMaterial;
                        if (textBoxSlope.Focused)
                            focused = textBoxSlope;
                        if (focused != null)
                            focused.SelectNextControl(focused, true, true, false, true);
                        break;
                    case (int)Keys.Escape:
                        break;
                }
            }
            return base.ProcessKeyPreview(ref m);
        }
    }
}
