﻿namespace CadPlugin.Inventories.GeometryAssignment.View
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPagePipe = new System.Windows.Forms.TabPage();
            this.tabPageShaft = new System.Windows.Forms.TabPage();
            this.buttonLoadToInventory = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.checkBoxAutoZoom = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sliderControlZoom = new CadPlugin.Inventories.GeometryAssignment.View.SliderControl();
            this.labelInfo = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPagePipe);
            this.tabControl.Controls.Add(this.tabPageShaft);
            this.tabControl.Location = new System.Drawing.Point(12, 49);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(732, 312);
            this.tabControl.TabIndex = 1;
            // 
            // tabPagePipe
            // 
            this.tabPagePipe.Location = new System.Drawing.Point(4, 22);
            this.tabPagePipe.Name = "tabPagePipe";
            this.tabPagePipe.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePipe.Size = new System.Drawing.Size(724, 286);
            this.tabPagePipe.TabIndex = 0;
            this.tabPagePipe.Text = "Kante";
            this.tabPagePipe.UseVisualStyleBackColor = true;
            // 
            // tabPageShaft
            // 
            this.tabPageShaft.Location = new System.Drawing.Point(4, 22);
            this.tabPageShaft.Name = "tabPageShaft";
            this.tabPageShaft.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageShaft.Size = new System.Drawing.Size(724, 286);
            this.tabPageShaft.TabIndex = 1;
            this.tabPageShaft.Text = "Knoten";
            this.tabPageShaft.UseVisualStyleBackColor = true;
            // 
            // buttonLoadToInventory
            // 
            this.buttonLoadToInventory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLoadToInventory.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonLoadToInventory.Location = new System.Drawing.Point(570, 367);
            this.buttonLoadToInventory.Name = "buttonLoadToInventory";
            this.buttonLoadToInventory.Size = new System.Drawing.Size(93, 23);
            this.buttonLoadToInventory.TabIndex = 3;
            this.buttonLoadToInventory.Text = "To Inventary";
            this.buttonLoadToInventory.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonCancel.Location = new System.Drawing.Point(669, 367);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // checkBoxAutoZoom
            // 
            this.checkBoxAutoZoom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxAutoZoom.AutoSize = true;
            this.checkBoxAutoZoom.Location = new System.Drawing.Point(6, 16);
            this.checkBoxAutoZoom.Name = "checkBoxAutoZoom";
            this.checkBoxAutoZoom.Size = new System.Drawing.Size(56, 17);
            this.checkBoxAutoZoom.TabIndex = 5;
            this.checkBoxAutoZoom.Text = "Active";
            this.checkBoxAutoZoom.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxAutoZoom);
            this.groupBox1.Controls.Add(this.sliderControlZoom);
            this.groupBox1.Location = new System.Drawing.Point(464, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(244, 44);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Autozoom";
            // 
            // sliderControlZoom
            // 
            this.sliderControlZoom.Location = new System.Drawing.Point(71, 11);
            this.sliderControlZoom.Name = "sliderControlZoom";
            this.sliderControlZoom.Size = new System.Drawing.Size(170, 25);
            this.sliderControlZoom.TabIndex = 6;
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.ForeColor = System.Drawing.Color.Maroon;
            this.labelInfo.Location = new System.Drawing.Point(13, 372);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(37, 13);
            this.labelInfo.TabIndex = 7;
            this.labelInfo.Text = "{label}";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 402);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonLoadToInventory);
            this.Controls.Add(this.tabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Geometry assignment";
            this.tabControl.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPagePipe;
        private System.Windows.Forms.TabPage tabPageShaft;
        private System.Windows.Forms.Button buttonLoadToInventory;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.CheckBox checkBoxAutoZoom;
        private System.Windows.Forms.GroupBox groupBox1;
        private SliderControl sliderControlZoom;
        private System.Windows.Forms.Label labelInfo;
    }
}