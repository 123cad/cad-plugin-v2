﻿using CadPlugin.Inventories.GeometryAssignment.CadInterface;
using Inventory.CAD.GeometryAssignment.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.Inventories.GeometryAssignment.View
{
    public partial class MainForm : Form
    {
        class MyMessageFilter : IMessageFilter
        {
            public const int WM_KEYUP = 0x0101;
            public const int WM_LBUTTONDOWN = 0x0201;

            public event Action EscapeClicked;
            private void OnEscapeClicked()
            {
                EscapeClicked?.Invoke();
            }
            public bool PreFilterMessage(ref Message m)
            {
                if (m.Msg == WM_KEYUP && m.WParam.ToInt32() == (int)Keys.Escape)
                {
                    OnEscapeClicked();
                }
                return false;
            }
        }


        /// <summary>
        /// Only single instance can exist.
        /// </summary>
        private static MainForm Instance = null;
        public bool FormAlreadyOpened { get; private set; }

        /// <summary>
        /// Stays indicator after form is closed, what user have chosen.
        /// </summary>
        public static bool AssignGeometry;

        /*public static MainForm GetForm()
        {
            if (Instance == null)
                new MainForm();
            else
            {
                if (!Instance.Visible)
                    Instance.Visible = true;
            }
            return Instance;
        }*/
        private CadSelectionManager selectionManager;
        private CadManager cadMgr = null;
        private MainFormDataManager dataMgr;
        private ZoomManager zoomManager;
        /// <summary>
        /// When in selection mode, form is invisible.
        /// LostFocus event is fired, but has to be ignored (currently selected
        /// object is highlighted on the drawing, and has to remain like that - no 
        /// selected object changed event should be fired).
        /// </summary>
        public bool IsSelectionMode { get { return !Visible; } }
        /// <summary>
        /// Used this because of some problems.
        /// </summary>
        public event Action<Keys> GlobalKeyUp;
        private readonly PipeControl pipeControl;
        private readonly ShaftControl shaftControl;

        internal MainForm(CadManager cadMgr)
        {
            if (Instance != null)
                throw new InvalidOperationException("Only 1 instance of GeometryAssignment form is allowed!");
            Instance = this;
            // We need exception because we need user to create cadMgr, so he can dispose it properly.
            // For this reason it is not possible to use Singleton.
            // (maybe we can attach to event which is invoked when form exception occurs, but not want to test it for now
            // Only 1 instance allowed is fine for now).
            this.cadMgr = cadMgr;
            AssignGeometry = false;

            InitializeComponent();
            #region Designer code
            // Has to be done manually, because designer was unable to 
            // add components from current assembly (BrxMgd load problem when adding from toolbox).
            //
            // PipeControl
            //
            pipeControl = new PipeControl(this, cadMgr);
            pipeControl.Dock = DockStyle.Fill;
            tabPagePipe.Controls.Add(pipeControl);

            //
            // ShaftControl
            //
            shaftControl = new ShaftControl(this, cadMgr);
            shaftControl.Dock = DockStyle.Fill;
            tabPageShaft.Controls.Add(shaftControl);
            #endregion

            buttonLoadToInventory.Click += (_, __) =>
            {
                AssignGeometry = true;
                Close();
            };
            buttonCancel.Click += (_, __) =>
            {
                // Not mandatory because this is set to false on start.
                AssignGeometry = false;
                Close();
            };

            FormClosing += (_, __) =>
            {
                cadMgr.UnselectCurrentObject();
            };
            this.FormClosed += (_, __) =>
            {
                //Application.RemoveMessageFilter(myFilter);
                Instance = null;

            };
            Shown += (_, __) =>
            {
                FormAlreadyOpened = true;
            };
        }
        public void Initialize(DataManagerEventArgs data)
        {
            pipeControl.Initialize(data.Pipes);

            shaftControl.Initialize(data.Shafts);
            dataMgr = new MainFormDataManager(pipeControl, shaftControl, data, cadMgr);
            zoomManager = new ZoomManager(cadMgr.document);
            selectionManager = new CadSelectionManager(cadMgr.document, data, zoomManager);
            checkBoxAutoZoom.DataBindings.Add(nameof(CheckBox.Checked), zoomManager, nameof(ZoomManager.ZoomActive));
            // Refreshes selection of updated object, without changing it.
            Action refreshUpdatedObject = () =>
            {
                if (tabPagePipe == tabControl.SelectedTab)
                    selectionManager.SelectionChanged(pipeControl.SelectedPipe);
                else if (tabPageShaft == tabControl.SelectedTab)
                    selectionManager.SelectionChanged(shaftControl.SelectedShaft);

            };
            tabControl.SelectedIndexChanged += (_, __) =>
            {
                refreshUpdatedObject();
            };
            Action<string> showWarningLabel = (s) =>
             {
                 string msg = $"No text exist for <{s}>!";
                 labelInfo.Text = msg;
             };
            labelInfo.Text = "";
            pipeControl.SelectionChanged += (e) =>
            {
                labelInfo.Text = "";
                bool anythingExist = selectionManager.SelectionChanged(e);
                if (!anythingExist)
                    showWarningLabel(e.Name);
            };
            shaftControl.SelectionChanged += (e) =>
            {
                labelInfo.Text = "";
                bool anythingExist = selectionManager.SelectionChanged(e);
                if (!anythingExist)
                    showWarningLabel(e.Name);
            };
            cadMgr.PipeUpdated += (e) =>
            {
                refreshUpdatedObject();
            };
            cadMgr.ShaftUpdated += (e) =>
            {
                refreshUpdatedObject();
            };
            FormClosed += (_, __) =>
            {
                data.CreateGeometry = AssignGeometry;
                selectionManager.Dispose();
            };
            checkBoxAutoZoom.CheckedChanged += (_, __) =>
            {
                bool isChecked = checkBoxAutoZoom.Checked;
                sliderControlZoom.Enabled = isChecked;
            };
            ((ISliderControl)sliderControlZoom).ZoomChanged += (e) =>
            {
                zoomManager.SetNewZoomLevel(e.Value);
                //cadMgr.RefreshZoom(e.Value);
            };
            sliderControlZoom.Current = new ZoomValue(ZoomManager.InitialZoomLevel);
        }

        /// <summary> 
        /// NOTE Only this combination worked for editing control ESC key press.
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyPreview(ref Message m)
        {
            if (m.Msg == MyMessageFilter.WM_KEYUP)
            {
                switch (m.WParam.ToInt32())
                {
                    case (int)Keys.Escape:
                        GlobalKeyUp?.Invoke(Keys.Escape);
                        break;
                    case (int)Keys.Enter:
                        GlobalKeyUp?.Invoke(Keys.Enter);
                        break;
                }
            }
            return base.ProcessKeyPreview(ref m);
        }
    }
}
