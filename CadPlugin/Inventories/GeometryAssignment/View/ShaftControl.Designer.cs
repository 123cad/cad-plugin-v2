﻿namespace CadPlugin.Inventories.GeometryAssignment.View
{
    partial class ShaftControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvShafts = new System.Windows.Forms.DataGridView();
            this.ColumnShaftEntity = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ColumnShaftGeometryExists = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColumnShaftGeometryAssigned = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColumnShaftName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnShaftObjektArt = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColumnShaftGeoobjektArt = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColumnShaftDiameter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnShaftEmpty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errorProviderDgvCell = new System.Windows.Forms.ErrorProvider(this.components);
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShafts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderDgvCell)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvShafts
            // 
            this.dgvShafts.AllowUserToAddRows = false;
            this.dgvShafts.AllowUserToDeleteRows = false;
            this.dgvShafts.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvShafts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvShafts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvShafts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnShaftEntity,
            this.ColumnShaftGeometryExists,
            this.ColumnShaftGeometryAssigned,
            this.ColumnShaftName,
            this.ColumnShaftObjektArt,
            this.ColumnShaftGeoobjektArt,
            this.ColumnShaftDiameter,
            this.ColumnShaftEmpty});
            this.dgvShafts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvShafts.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvShafts.Location = new System.Drawing.Point(0, 0);
            this.dgvShafts.MultiSelect = false;
            this.dgvShafts.Name = "dgvShafts";
            this.dgvShafts.RowHeadersVisible = false;
            this.dgvShafts.Size = new System.Drawing.Size(723, 335);
            this.dgvShafts.TabIndex = 2;
            this.dgvShafts.MouseEnter += new System.EventHandler(this.dgvShafts_MouseEnter);
            this.dgvShafts.MouseLeave += new System.EventHandler(this.dgvShafts_MouseLeave);
            // 
            // ColumnShaftEntity
            // 
            this.ColumnShaftEntity.HeaderText = "Entity";
            this.ColumnShaftEntity.Name = "ColumnShaftEntity";
            this.ColumnShaftEntity.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnShaftEntity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColumnShaftEntity.Text = "Update";
            this.ColumnShaftEntity.Width = 55;
            // 
            // ColumnShaftGeometryExists
            // 
            this.ColumnShaftGeometryExists.HeaderText = "Geometry";
            this.ColumnShaftGeometryExists.Name = "ColumnShaftGeometryExists";
            this.ColumnShaftGeometryExists.ReadOnly = true;
            this.ColumnShaftGeometryExists.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnShaftGeometryExists.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColumnShaftGeometryExists.Width = 60;
            // 
            // ColumnShaftGeometryAssigned
            // 
            this.ColumnShaftGeometryAssigned.HeaderText = "Assigned";
            this.ColumnShaftGeometryAssigned.Name = "ColumnShaftGeometryAssigned";
            this.ColumnShaftGeometryAssigned.ReadOnly = true;
            this.ColumnShaftGeometryAssigned.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnShaftGeometryAssigned.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColumnShaftGeometryAssigned.Width = 60;
            // 
            // ColumnShaftName
            // 
            this.ColumnShaftName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnShaftName.FillWeight = 40F;
            this.ColumnShaftName.HeaderText = "Bezeichnung";
            this.ColumnShaftName.MinimumWidth = 80;
            this.ColumnShaftName.Name = "ColumnShaftName";
            // 
            // ColumnShaftObjektArt
            // 
            this.ColumnShaftObjektArt.HeaderText = "ObjektArt";
            this.ColumnShaftObjektArt.Name = "ColumnShaftObjektArt";
            this.ColumnShaftObjektArt.ReadOnly = true;
            // 
            // ColumnShaftGeoobjektArt
            // 
            this.ColumnShaftGeoobjektArt.HeaderText = "GeoobjektArt";
            this.ColumnShaftGeoobjektArt.Name = "ColumnShaftGeoobjektArt";
            this.ColumnShaftGeoobjektArt.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnShaftGeoobjektArt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ColumnShaftDiameter
            // 
            this.ColumnShaftDiameter.HeaderText = "Diameter (mm)";
            this.ColumnShaftDiameter.Name = "ColumnShaftDiameter";
            // 
            // ColumnShaftEmpty
            // 
            this.ColumnShaftEmpty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnShaftEmpty.HeaderText = "";
            this.ColumnShaftEmpty.Name = "ColumnShaftEmpty";
            this.ColumnShaftEmpty.ReadOnly = true;
            // 
            // errorProviderDgvCell
            // 
            this.errorProviderDgvCell.ContainerControl = this;
            // 
            // dataGridViewButtonColumn1
            // 
            this.dataGridViewButtonColumn1.HeaderText = "Entity";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewButtonColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewButtonColumn1.Text = "Update";
            this.dataGridViewButtonColumn1.Width = 55;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.FillWeight = 40F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Bezeichnung";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 65;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Diameter (mm)";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 50;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.FillWeight = 40F;
            this.dataGridViewTextBoxColumn3.HeaderText = "";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 65;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.HeaderText = "ObjektArt";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.HeaderText = "GeoobjektArt";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Diameter (mm)";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.HeaderText = "";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // ShaftControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgvShafts);
            this.Name = "ShaftControl";
            this.Size = new System.Drawing.Size(723, 335);
            ((System.ComponentModel.ISupportInitialize)(this.dgvShafts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderDgvCell)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvShafts;
        private System.Windows.Forms.ErrorProvider errorProviderDgvCell;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewButtonColumn ColumnShaftEntity;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnShaftGeometryExists;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnShaftGeometryAssigned;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnShaftName;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnShaftObjektArt;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnShaftGeoobjektArt;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnShaftDiameter;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnShaftEmpty;
    }
}
