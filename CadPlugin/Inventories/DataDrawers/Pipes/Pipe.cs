﻿using my = MyUtilities.Geometry;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using Isybau2015.Identifikations.Datenkollektives.Zustandsdatenkollektivs;
using Isybau2015.Identifikations.Datenkollektives.Zustandsdatenkollektivs.InspizierteAbwassertechnischeAnlagens.OptischeInspektions;
using Isybau2015.Identifikations.Datenkollektives.Zustandsdatenkollektivs.InspizierteAbwassertechnischeAnlagens.OptischeInspektions.Rohrleitungs.Inspektionsdaten;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isybau2015.Isybau2015;
using CadPlugin.Inventories.Damages;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens;
using geometry = Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries;
using CadPlugin.Inventories.Pipes.Segments;
using CadPlugin.Inventories.DataDrawers.Damages;
using Isybau2015.Isybau2015.ReferenceTableEnums;
using CadPlugin.Inventories.Shafts;
using CadPlugin.Inventories.DataDrawers;
using Isybau2015;
using CadPlugin.SewageHelpers;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.Pipes
{
	public class Pipe : SewageObject
	{
		public static double DefaultDiameter = 0.8;
		public double Diameter { get; private set; }
		public my.Point3d StartPoint { get; private set; }
		public my.Point3d EndPoint { get; private set; }
		public double Length { get; private set; }
		/// <summary>
		/// Segments are chained, this is first one in the chain.
		/// </summary>
		private List<Segment> segments { get; set; }
		private DamagesCollection damages { get; set; }
		private List<Damages.DamageStation> damageStations { get; set; }

		public DamageClass MaxAutoDamageClass { get; private set; }
		/// <summary>
		/// Indicates if pipe can be drawn (it contains 2 or more points).
		/// </summary>
		public bool IsPipeComplete { get; private set; } = false;

		#region Manual class
		private DamageClass __ManualDamageClass = 0;
		public DamageClass ManualDamageClass
		{
			get
			{
				if (!IsManualDamageClassSet)
					throw new FieldAccessException("ManualDamageClass is not set!");
				return __ManualDamageClass;
			}
			private set
			{
				__ManualDamageClass = value;
				IsManualDamageClassSet = true;
			}

		}
		public bool IsManualDamageClassSet { get; private set; }
		#endregion

		#region Damages iterator
		/// <summary>
		/// This is used so we can have iterator over collection.
		/// Without this GetEnumerator has to be defined, which seems to be confusing
		/// because Damages are not intuitive collection of Pipe.
		/// </summary>
		public class DamagesCollection
		{
			private Pipe p;
			public DamagesCollection(Pipe p)
			{
				this.p = p;
			}
			public IEnumerator<Damages.DamageStation> GetEnumerator()
			{
				return p.damageStations.GetEnumerator();
			}
		}
		#endregion

		public Pipe(SewageObjectsCollection owner, AbwassertechnischeAnlagen ab, InspizierteAbwassertechnischeAnlagen iab, InspizierteAbwassertechnischeAnlagen iabUpstream) : base(owner, ab, iab, iabUpstream)
		{
			damages = new DamagesCollection(this);
			damageStations = new List<Damages.DamageStation>();
			segments = new List<Segment>();
			Initialize();

		}
		private void Initialize()
		{
			// Get diameter.

			Diameter = DefaultDiameter;
			double d = Diameter;
			if (PipeSewageObjectHelpers.GetPipeDiameter(Abwasser, ref d))
				Diameter = d;

			// Initialize segments.
			InitializeSegments();
			// Initialize damages
			if (segments.Any())
				InitalizeDamages();

			MaxAutoDamageClass = GetHighestDamageClass();
		}
		private void InitializeSegments()
		{
			segments.Clear();
			if (Abwasser.Geometrie == null)
			{
				MyLog.MyTraceSource.Warning("No geometry data found for pipe: " + Abwasser.Objektbezeichnung + " . Damages also are not drawn");
				return;
			}
			Geometrie gm = Abwasser.Geometrie;
			geometry.Geometriedaten gmd = gm.GeometrieDaten;
			if (gmd == null)
				return;
			/*switch (gm.GeoObjektart)
			{
				case 4:
				case 5:
				case 6:
				case 7:
					break;
				default:
					return;
			}*/
			// Only Kante draw for pipes.
			{
				List<geometry.Kante> kantes = null;
				if (gm.GeometrieDaten.Kantes.Count > 0)
				{
					kantes = gm.GeometrieDaten.Kantes;
				}
				/* Polygon is not supported, but exists in some isybau file.
				 * else if (gm.GeometrieDaten.Polygone.Count > 0)
				{
					foreach (Polygon pol in gm.GeometrieDaten.Polygone)
						if (pol.Kantes.Count > 0)
						{
							kantes = pol.Kantes;
							break;
						}
				}*/
				if (kantes == null)
					return;
				geometry.Kante last = null;
				double currentDistance = 0;
				// It can happen that RAP point does not have neight.
				// But height can be written in Abwasser/Kante node.
				if (kantes.Count >= 1)
				{
					var pipeKante = (Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Kante)(Abwasser.Auswahlelement);
					if (!kantes[0].Start.PunkthoeheIsSet && pipeKante.SohlhoeheZulaufFormatted != "")
						kantes[0].Start.Punkthoehe = pipeKante.SohlhoeheZulauf;
					if (!kantes[kantes.Count - 1].Ende.PunkthoeheIsSet && pipeKante.SohlhoeheAblaufIsSet)
						kantes[kantes.Count - 1].Ende.Punkthoehe = pipeKante.SohlhoeheAblauf;
				}
				foreach (geometry.Kante k in kantes)
				{
					if (last != null)
					{
						// If current Kante start point is not same as previous kante end point, add line segment.
						// Tested length is 3cm
						if (last.Ende.GetPoint3d().GetVectorTo(k.Start.GetPoint3d()).Length > 0.03)
						{
							LineSegment l = new LineSegment(last.Ende.GetPoint3d(), k.Start.GetPoint3d(), currentDistance);
							segments.Add(l);
							currentDistance += l.Length;
						}
					}
					else
					{
						StartPoint = k.Start.GetPoint3d();
					}
					Segment seg = null;
					if (k.Mitte == null)
					{
						seg = new LineSegment(k.Start.GetPoint3d(), k.Ende.GetPoint3d(), currentDistance);
					}
					else
					{
						// If we have an arc segment (poly3d supports only line segments, so we use interpolated arc).
						seg = new InterpolatedArc(this, k.Start.GetPoint3d().ToCADPoint(), k.Mitte.GetPoint3d().ToCADPoint(), k.Ende.GetPoint3d().ToCADPoint(), currentDistance);
					}
					currentDistance += seg.Length;
					segments.Add(seg);
					last = k;
				}
				EndPoint = segments.Last().EndPoint;
				Length = currentDistance;
			}
			if (segments.Count >= 1 && Length > 0.001)
				IsPipeComplete = true;
		}
		private void InitalizeDamages()
		{
			Dictionary<int, List<SingleDamage>> stations = new Dictionary<int, List<SingleDamage>>();
			Action<InspizierteAbwassertechnischeAnlagen> func = (status) =>
			{
				// Line damages draw with line.
				if (status.OptischeInspektion == null ||
					status.OptischeInspektion.Auswahlelement as Rohrleitung == null)
					return;
				Rohrleitung r = status.OptischeInspektion.Auswahlelement as Rohrleitung;
				if (r.Bewertung != null && r.Bewertung.KlasseManuellIsSet)
					this.ManualDamageClass = DamageClassConverter.Convert(r.Bewertung.KlasseManuell);
				foreach (RZustand rzus in r.RZustands)
				{
					//if (rzus.InspektionsKode == "BCA")
						//continue;
					int stationDistance = (int)(rzus.Station * 1000);
					if (!stations.ContainsKey(stationDistance))
						stations.Add(stationDistance, new List<SingleDamage>());
					List<SingleDamage> damages = stations[stationDistance];

					DamageClass maxClass = rzus.Klassifizierung != null ? DamageClassConverter.Convert(rzus.Klassifizierung.MaxSKeAuto) : DamageClass.ClassNotSet;
					// Is line damage
					bool lineDamage = false;
					double length = 0.3;// Default for point damage.
					if (rzus.IsLineDamageStart())
					{
						lineDamage = true;
						RZustand lineEnd = Isybau2015.PipeHelper.FindLineDamageEnd(rzus, r.RZustands);
                        // Noted in log file.
                        //Debug.Assert(lineEnd != null, "Line end damage not found!");
                        MyLog.MyTraceSource.Warning($"Line damage end not found for pipe {Abwasser.Objektbezeichnung} on station {stationDistance.ToString("0.000")} for damage {rzus.InspektionsKode}");
						if (lineEnd != null)
							length = lineEnd.Station - rzus.Station;
					}

					SingleDamage dmg = new SingleDamagePipe(rzus);
					dmg.DamageClass = maxClass;
					dmg.Length = length;
					dmg.IsLineDamage = lineDamage;
                    //dmg.IsDrawn = dmg.Code != "BCA" && dmg.Code != "BCC";
                    dmg.ClockPosition = rzus.PositionVon;

					damages.Add(dmg);
				}
			};
			InspizierteAbwassertechnischeAnlagen st = Inspizierte;
			if (st != null)
				func(st);
			st = InspizierteUpstream;
			if (st != null)
				func(st);
			foreach(KeyValuePair<int, List<SingleDamage>> station in stations)
			{
				double distance = station.Key / 1000.0;
				PositionVector? pv = GetPositionAtDistance(distance);
				if (pv == null)
				{
					MyLog.MyTraceSource.Warning("Position on pipe not found for station: " + distance.ToString("0.000"));
					continue;
				}
				DamageStation ds = new DamageStation(this, distance, pv.Value);
				foreach (SingleDamage sd in station.Value)
					ds.AddDamage(sd);
                damageStations.Add(ds);
			}
		}

		/// <summary>
		/// Returns points which belong to provided interval.
		/// Note that if interval is longer than pipe, only point which belong to pipe are returned.
		/// </summary>
		/// <param name="startDistance"></param>
		/// <param name="endDistance"></param>
		/// <returns></returns>
		public IEnumerable<my.Point3d> GetPoints(double startDistance, double endDistance)
		{
			if (startDistance < 0)
			{
				//startDistance = 0 + 0.00001;
				MyLog.MyTraceSource.Warning("Start distance of pipe must not be less than 0! Current: " + startDistance.ToString("0.000") + " for pipe: " + Abwasser.Objektbezeichnung);
			}
			if (endDistance > Length)
			{
				//endDistance = Length - 0.00001;
				MyLog.MyTraceSource.Warning("End distance of pipe must not be bigger than length! Current: " + endDistance.ToString("0.000")
					+ " length:" + Length.ToString("0.000") + " for pipe: " + Abwasser.Objektbezeichnung);
			}
			if (endDistance < 0 || startDistance > Length)
			{
				// Desired interval is outside of pipe.
				return null;
			}

			List<my.Point3d> pts = new List<my.Point3d>();
			foreach (Segment sg in segments)
			{
				// If we know this is not our segment, skip it.
				if (startDistance > sg.EndDistance)
					continue;
				IEnumerable<my.Point3d> pt = sg.GetPoints(startDistance, endDistance);
				if (pt != null && pt.Any())
				{
					bool endStartAreSame = false;
					int currentCount = pts.Count;
					var lastPt = pts.LastOrDefault();
					// Remove duplicated points (current end point, and new next point
					// [start point of new sequence]).
					if (pts.Count > 0)
					{
						var nextPt = pt.First();
						if (lastPt.GetVectorTo(nextPt).Length < 0.001)
							endStartAreSame = true;
					}
					pts.AddRange(pt);

					if (endStartAreSame)
					{
						pts.RemoveAt(currentCount);
					}
				}
				// When reached end distance, no purpose of going on.
				if (endDistance < sg.EndDistance)
					break;
			}

			List<my.Point3d> temp = new List<my.Point3d>();
			bool first = true;
			my.Point3d last = new my.Point3d();
			// Remove duplicates.
			foreach (my.Point3d p in pts)
			{
				if (first)
				{
					// First point is added to new list.
					first = false;
					last = p;
					temp.Add(p);
					continue;
				}
				// Current point (including last one in the sequence) is added only if it is different
				// than previous one (start segment is same like end of previous segment - we don't need it).
				if (last.GetVectorTo(p).Length > 0.0001)
					temp.Add(p);
			}
			return temp;
		}
		/// <summary>
		/// Returns all points of the pipe (all points of all segments).
		/// </summary>
		/// <returns></returns>
		public IEnumerable<my.Point3d> GetAllPoints()
		{
			return GetPoints(0, Length);
		}

		/// <summary>
		/// Iterator through damage stations.
		/// </summary>
		/// <returns></returns>
		public DamagesCollection GetDamagesCollection()
		{
			return damages;
		}

		/// <summary>
		/// Returns position for provided distance.
		/// </summary>
		/// <param name="distance"></param>
		/// <returns></returns>
		public PositionVector? GetPositionAtDistance(double distance)
		{
			PositionVector? pp = null;
			foreach (Segment sg in segments)
				if (sg.DoesBelongToSegment(distance))
				{
					pp = sg.GetPositionAtDistance(distance);
					break;
				}
			return pp;
		}

		public PositionVector? GetPositionForMiddlePoint()
		{
			return GetPositionAtDistance(Length / 2);
		}

		public DamageClass GetHighestDamageClass()
		{
			DamageClass dmg = DamageClass.ClassNotSet;
			foreach(Damages.DamageStation st in damageStations)
			{
				DamageClass dc = st.GetHighestDamageClass();
				if (dmg < dc)
					dmg = dc;
			}
			return dmg;
		}
		/// <summary>
		/// Gets start and end shaft for current pipe, and returns if operation is valid.
		/// </summary>
		/// <param name="startShaft"></param>
		/// <param name="endShaft"></param>
		/// <returns></returns>
		public bool GetStartEndShaft(ref Shaft startShaft, ref Shaft endShaft)
		{
			var kante = Abwasser.Auswahlelement as Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Kante;
			startShaft = OwningCollection.GetShaft(kante.KnotenZulauf);
			endShaft = OwningCollection.GetShaft(kante.KnotenAblauf);
			bool allFound = startShaft != null && endShaft != null;
			
			return allFound;
		}
	}
}
