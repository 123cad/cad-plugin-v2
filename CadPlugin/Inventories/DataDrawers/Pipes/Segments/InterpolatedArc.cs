﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CadPlugin.Common;
using System.Diagnostics;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Inventories.Pipes.Segments
{
	class InterpolatedArc : ArcSegment
	{
		/// <summary>
		/// Gets or sets number of interpolation lines, arc is interpolated to.
		/// </summary>
		public static int NumberOfInterpolationLines = 10;
		/// <summary>
		/// Include start and end point of the arc.
		/// </summary>
		public virtual IList<MyUtilities.Geometry.Point3d> InterpolatedPoints
		{
			get;
			private set;
		}

		/// <summary>
		/// true if points are interpolated, false if interpolation is needed.
		/// </summary>
		private bool isInterpolated
		{
			get;
			set;
		}
		

		/// <summary>
		/// For every interpolation line, step represents arc lenght on that line.
		/// (not length of interpolation line!).
		/// </summary>
		private double interpolationStep = 0;

		public InterpolatedArc(Pipe pipe, Point3d start, Point3d middle, Point3d end, double startDistance):base(pipe, start, end, middle, startDistance)
		{
			isInterpolated = false;
			InterpolatedPoints = new List<MyUtilities.Geometry.Point3d>();

			interpolate();

			Debug.WriteLine(false, "Not handled length of the InterpolatedArc - length of the arc is not the same as length of interpolated arc - this can make problems with Station distance.");
            throw new NotImplementedException("Interpolated arc is not implemented!");

			//NOTE One options is to increase diameter of interpolated arc, so that both lengths match 
			// For half of circle: r1 * Pi = 2 * SQRT(2) * r2 ; where r2 is diameter of new arc.
		}
		

		/// <summary>
		/// Reinterpolate.
		/// </summary>
		public virtual void Refresh()
		{
			isInterpolated = false;
			InterpolatedPoints.Clear();
		}

		public void Draw(Polyline3d polyline)
		{
			if (!isInterpolated)
				interpolate();
			bool vertexExist = false;
			foreach(PolylineVertex3d pl in polyline)
			{
				vertexExist = true;
				break;
			}
			// If no vertex exists, start vertex is needed.
			if (!vertexExist)
			{
				polyline.AppendPoint(StartPoint.ToCADPoint());
			}
			// Represents first iteration.
			bool flag = false;
			// Adds only second point of the line.
			foreach (MyUtilities.Geometry.Point3d p in InterpolatedPoints)
			{
				// First point is start point. If vertex exists, it is from last segment, 
				// which means start point is not needed because it is the same as last vertex.
				if (!flag)
				{
					flag = true;
					if (vertexExist)
						continue;
				}

				polyline.AppendPoint(p.ToCADPoint());
			}
		}

		public void Draw(double startDistance, double endDistance, Polyline3d entity)
		{
			if (!isInterpolated)
				interpolate();
			if (DoesBelongToSegment(startDistance) || DoesBelongToSegment(EndDistance))
			{
				// check if start distance is before this segment - then start from first point 
				// and continue

				int startLineIndex = 0;
				int endLineIndex = NumberOfInterpolationLines - 1;
				Point3d start = StartPoint.ToCADPoint();
				Point3d end = EndPoint.ToCADPoint();
				if (startDistance > StartDistance)
				{
					// Relative to beggining of the segment.
					double relativeDistance = (startDistance - StartDistance);
					// Index of line where this distance belongs to.
					startLineIndex = (int)(relativeDistance / interpolationStep);
					start = GetPointAtDistance(startDistance).Value.Position.ToCADPoint();
				}
				if (endDistance < EndDistance)
				{
					double relativeDistance = (endDistance - StartDistance);
					endLineIndex = (int)(relativeDistance / interpolationStep);
					end = GetPointAtDistance(endDistance).Value.Position.ToCADPoint();
				}
				entity.AppendPoint(start);
				// Index of start point of line is same as line index.
				if (endLineIndex > startLineIndex)
					entity.AppendPoint(InterpolatedPoints[startLineIndex+1].ToCADPoint());
				for (int i = startLineIndex + 1; i < endLineIndex; i++)
				{
					entity.AppendPoint(InterpolatedPoints[i+1].ToCADPoint());
				}
				entity.AppendPoint(end);
			}
		}
		/*
		// NOTE Old method which calculates distance from arc (2d) distance.
		// Which can be very incorrect, if start and end height is big.

		public override PointPosition GetPointAtDistance(double distance)
		{
			if (!isInterpolated)
				interpolate();
			if (DoesBelongToSegment(distance))
			{

				// Relative to beggining of the segment.
				double relativeDistance = (distance - StartDistance);
				// Index of line where this distance belongs.
				int lineIndex = (int)(relativeDistance / interpolationStep);

				// Start and end point of interpolated line.
				Point3d start = InterpolatedPoints[lineIndex];
				Point3d end = InterpolatedPoints[lineIndex + 1];
				Vector3d lineVector = end.GetAsVector() - start.GetAsVector();
				Vector3d unitVector = lineVector.GetNormal();

				// Length percentage on arc is the same on the line.
				double arcScale = (relativeDistance - lineIndex * interpolationStep) / interpolationStep;
				// Location of the point on the interpolated line.
				double pointLocation = lineVector.Length * arcScale;
				Vector3d v = unitVector.MultiplyBy(pointLocation);

				PointPosition pp = new PointPosition();
				pp.DirectionVector = unitVector;
				pp.Position = start.Add(v);

				return pp;
			}
			return null;
		}*/
		/// <summary>
		/// Calculates stations from interpolated lines (in 3d)
		/// </summary>
		/// <param name="distance"></param>
		/// <returns></returns>
		public PositionVector? GetPointAtDistance(double distance)
		{
			if (!isInterpolated)
				interpolate();
			if (DoesBelongToSegment(distance))
			{

				// Relative to beggining of the segment.
				double relativeDistance = (distance - StartDistance);
				// Index of line where this distance belongs.
				int lineIndex = (int)(relativeDistance / interpolationStep);

				// Start and end point of interpolated line.
				MyUtilities.Geometry.Point3d start = InterpolatedPoints[lineIndex];
				MyUtilities.Geometry.Point3d end = InterpolatedPoints[lineIndex + 1];
				MyUtilities.Geometry.Vector3d lineVector = end.GetAsVector() - start.GetAsVector();
				MyUtilities.Geometry.Vector3d unitVector = lineVector.GetUnitVector();

				/*// Length percentage on arc is the same on the line.
				double arcScale = (relativeDistance - lineIndex * interpolationStep) / interpolationStep;
				// Location of the point on the interpolated line.
				double pointLocation = lineVector.Length * arcScale;*/
				MyUtilities.Geometry.Vector3d v = unitVector.Multiply(relativeDistance - lineIndex * interpolationStep);

				PositionVector pp = new PositionVector();
				pp.Direction = unitVector;
				pp.Position = start.Add(v);

				return pp;
			}
			return null;
		}

		private void interpolate()
		{
			//NOTE steps number can be adjusted, for example depending on interpolation step

			// Steps are interpolated 2 times (because we want to include middle point
			// at the joint of 2 lines).
			int stepsNr = 10;
			// l = r * Pi * deg / 180 ; deg = rad * 180 / Pi;
			Length = 0;
			// This is 2d step. It is only helper, because interpolation lines are calculated and just heigth is added, 
			// and then interpolationStep is recalculated. In this way, we keep 3d arc points just above 2d arc points
			// (same x/y coordinates).
			interpolationStep = Radius * ArcAngleRad / stepsNr;

			// l = r * Pi * angleDeg / 180;
			double stepAngleDeg = interpolationStep * 180 / Radius / Math.PI;
			double stepAngleRad = stepAngleDeg * Math.PI / 180;
			if (CounterClockwiseDirection)
				stepAngleRad *= -1;
			double height = EndPoint.Z - StartPoint.Z;
			double heightStep = height / (stepsNr);
			double currentHeight = StartPoint.Z;
			Vector2d v = new Vector2d(StartPoint.X, StartPoint.Y) - CenterPoint.GetAsVector();
			InterpolatedPoints.Add(StartPoint);
			for (int i = 0; i < stepsNr; i++)
			{
				v = v.RotateBy(-stepAngleRad);
				currentHeight += heightStep;
				MyUtilities.Geometry.Point3d point = new MyUtilities.Geometry.Point3d(CenterPoint.X + v.X, CenterPoint.Y + v.Y, currentHeight);
				InterpolatedPoints.Add(point);				
			}
			// ArcSegment has 2d Length, interpolated has 3d - length is different so it has to be recalculated.
			for(int i = 1; i < InterpolatedPoints.Count; i++)
			{
				double distance = InterpolatedPoints[i].GetVectorTo(InterpolatedPoints[i - 1]).Length;
				Length += distance;
			}
			InterpolatedPoints.Add(EndPoint);
			// Step has changed, because Length is now 3d.
			interpolationStep = Length / stepsNr;

			isInterpolated = true;
		}

	}
}

