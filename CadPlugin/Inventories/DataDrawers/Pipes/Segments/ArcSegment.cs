﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Inventories.Pipes.Segments
{
	/// <summary>
	/// Arc can't be used as 3d polyline segment, so we will use interpolation.
	/// </summary>
	class ArcSegment : Segment
	{ 
		public virtual Point3d MiddlePoint
		{
			get;
			private set;
		}

		public virtual Point2d CenterPoint
		{
			get;
			private set;
		}
		/// <summary>
		/// Gets angle in rad of the arc (regarding to center of the arc).
		/// </summary>
		public double ArcAngleRad { get; private set; }
		public double Radius { get; private set; }
		/// <summary>
		/// Gets flag indicating if Start point is to be rotated in Clockwise or Counterclockwise direction
		/// (this depends on middle point).
		/// </summary>
		public bool CounterClockwiseDirection { get; private set; }

		public ArcSegment(Pipe pipe, Point3d start, Point3d end, Point3d middle, double startDistance):base(start.FromCADPoint(), end.FromCADPoint(), startDistance)
		{
			MiddlePoint = middle;
			CenterPoint = get3PointsCircleCenter(start, middle, end);
			Vector2d center = CenterPoint.GetAsVector();
			Vector2d startVector = new Vector2d(start.X, start.Y) - center;
			Vector2d endVector = new Vector2d(end.X, end.Y) - center;
			Vector2d middleVector = new Vector2d(middle.X, middle.Y) - center;
			// Angle is returned in region [0-Pi], so we calculate angle as sum of 2 angles.
			// Could be done by calculating one time and multiplying by 2, but this assumes that 
			// middle point is idealy in the half. This is more safer way.
			FindAngleAndDirection(startVector, middleVector, endVector);
			//ArcAngleRad = startVector.GetAngleTo(middleVector) + endVector.GetAngleTo(middleVector);
			// Check if 
			double tempAngleRad = startVector.GetAngleTo(endVector);
			Radius = startVector.Length;
			double angleDeg = ArcAngleRad * 180 / Math.PI;
			Length = Radius * Math.PI * angleDeg/ 180;
			EndDistance = StartDistance + Length;

			Debug.WriteLine(false, "Not handled length of the InterpolatedArc - length of the arc is not the same as length of interpolated arc - this can make problems with Station distance.");
		}


		private static Point2d get3PointsCircleCenter(Point3d p1, Point3d p2, Point3d p3)
		{
			return get3PointsCircleCenter(p1.X, p1.Y, p2.X, p2.Y, p3.X, p3.Y);
		}
		private static Point2d get3PointsCircleCenter(Point2d p1, Point2d p2, Point2d p3)
		{
			return get3PointsCircleCenter(p1.X, p1.Y, p2.X, p2.Y, p3.X, p3.Y);
		}


		/// <summary>
		/// Creates 2d point of circle center.
		/// </summary>
		/// <exception cref="ArithmeticException"></exception>
		/// <returns></returns>
		private static Point2d get3PointsCircleCenter(double x1, double y1, double x2, double y2, double x3, double y3)
		{
			double q = 0;
			q = (x1 - x2) * (x1 * x1 - x3 * x3 + y1 * y1 - y3 * y3) - (x1 - x3) * (x1 * x1 - x2 * x2 + y1 * y1 - y2 * y2);
			q /= 2 * ((y1 - y3) * (x1 - x2) - (x1 - x3) * (y1 - y2));

			double p = (x1 * x1 - x2 * x2 + y1 * y1 - y2 * y2 - 2 * q * (y1 - y2)) / (2 * (x1 - x2));

			// According to Microsoft, "Floating-point arithmetic overflow or division by zero never throws an exception, 
			// because floating-point types are based on IEEE 754 and so have provisions for representing infinity and NaN (Not a Number)."
			if (double.IsInfinity(q) || double.IsNaN(q))
				throw new ArithmeticException("Not possible to create a circle!");
			if (double.IsInfinity(p) || double.IsNaN(p))
				throw new ArithmeticException("Not possible to create a circle!");

			return new Point2d(p, q);
		}

		/// <summary>
		/// Finds length of an arc (from start to end point, going through middle point), and direction
		///  (start always go to the end, but question is: by moving clockwise or counterclockwise).
		/// </summary>
		/// <param name="start"></param>
		/// <param name="middle"></param>
		/// <param name="end"></param>
		/// <param name="arcAngle"></param>
		/// <param name="counterClockwiseDirection">Start goes to the End, and this tells if start is to be rotated in clockwise direction or opposite.</param>
		private void FindAngleAndDirection(Vector2d start, Vector2d middle, Vector2d end)
		{
			// Angle between vectors returns [0-Pi].
			// Rotate start vector by this angle. If new angle between start and middle is ~0, then it is clockwise direction.
			// If angle is !=0 , this means start should be rotated in counterclockwise direction, so arc goes through middle point.
			double startMiddleAngle = start.GetAngleTo(middle);
			Vector2d rotatedStart = start.RotateBy(-startMiddleAngle);
			CounterClockwiseDirection = false;
			// Check if value is 0, with some precision. 
			double tempAngle = rotatedStart.GetAngleTo(middle);
			if (tempAngle > 0.01)
				CounterClockwiseDirection = true;
			double endMiddleAngle = end.GetAngleTo(middle);
			ArcAngleRad = startMiddleAngle + endMiddleAngle;
		}

		public override PositionVector? GetPositionAtDistance(double distance)
		{
			throw new NotImplementedException();
		}

		public override IEnumerable<MyUtilities.Geometry.Point3d> GetPoints(double startDistance, double endDistance)
		{
			throw new NotImplementedException();
		}

		public override IEnumerable<MyUtilities.Geometry.Point3d> GetAllPoints()
		{
			throw new NotImplementedException();
		}
	}
}

