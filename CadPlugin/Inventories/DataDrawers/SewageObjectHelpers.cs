﻿using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using Isybau2015.Ownerships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Knotens;
using objektarts = Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries.Geometriedatens;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Knotens.Schachts;
using static Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries.Geometriedaten;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries;
using CadPlugin.Common;
using CadPlugin.SewageHelpers;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories
{

	static class SewageObjectHelpers
	{
        public static string GetOwner(this AbwassertechnischeAnlagen ab)
        {
            string s = null;

            OwnershipType? ot = ab?.Parent?.Parent?.Parent?.Owners?.GetOwnership(ab);
            if (ot.HasValue)
            {
                s = ot.Value.Owner.Trim();
            }
            return s;
        }
		/// <summary>
		/// Returns point which matches provided PunktattributAbwasser (only points are searched).
		/// </summary>
		/// <param name="ab"></param>
		/// <param name="att"></param>
		/// <returns></returns>
		public static Punkt GetPunktByAttribut(AbwassertechnischeAnlagen ab, string att)
		{
			if (ab.Geometrie == null)
				return null;
			if (ab.Geometrie.GeometrieDaten == null)
				return null;
			if (ab.Geometrie.GeometrieDaten.Punkts == null)
				return null;
			Punkt pk = null;
			foreach (var p in ab.Geometrie.GeometrieDaten.Punkts)
			{
				if (p.PunktattributAbwasser == att)
				{
					pk = p;
					break;
				}
			}
			return pk;
		}


		private static List<Point3d> GetPolygonPoints(Polygon pl)
		{
			List<Point3d> points = new List<Point3d>();
			foreach (Kante k in pl.Kantes)
			{
                MyUtilities.Geometry.Point3d p = new MyUtilities.Geometry.Point3d();
				if (points.Count == 0)
				{
					p = k.Start.GetPoint3d();// new Geometry.Elements.Point(k.Start);
					points.Add(new Point3d(p.X, p.Y, p.Z));
				}
				p = k.Ende.GetPoint3d();//  new Geometry.Elements.Point(k.Ende);
				points.Add(new Point3d(p.X, p.Y, p.Z));
			}
			return points;
		}
		/// <summary>
		/// Draws Polygon as Polyline with elevation (all points on single height)
		/// and returns object id for newly created polyline.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="modelSpace"></param>
		/// <param name="pl"></param>
		/// <returns></returns>
		public static Polyline DrawPolygon2d(TransactionData data, Polygon pg)
		{
			Polyline pl = data.Tr.CreateEntity<Polyline>(data.Btr);

			double height = 0;
			bool heightSet = false;
			foreach (Point3d p in GetPolygonPoints(pg))
			{
				if (!heightSet)
				{
					height = p.Z;
					heightSet = true;
				}

				pl.AddPoints(p.GetAs2d());
			}
			pl.Elevation = height;
			return pl;
		}
		/// <summary>
		/// Draws Polygon as Polyline3d and returns object id for newly created polyline.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="modelSpace"></param>
		/// <param name="pl"></param>
		/// <returns></returns>
		public static Polyline3d DrawPolygon3d(TransactionData data, Polygon pg)
		{
			Polyline3d pl = new Polyline3d();
			data.Btr.AppendEntity(pl);
			data.Tr.AddNewlyCreatedDBObject(pl, true);

			foreach (Point3d p in GetPolygonPoints(pg))
				pl.AppendPoint(p);

			return pl;
		}
	}


	class PipeSewageObjectHelpers
	{
		/// <summary>
		/// If value is not found, default one can be used.
		/// </summary>
		/// <param name="ab"></param>
		/// <param name="d"></param>
		/// <returns></returns>
		public static bool GetPipeDiameter(AbwassertechnischeAnlagen ab, ref double d)
		{
			if (ab.Auswahlelement == null)
				return false;
			objektarts.Kante k = ab.Auswahlelement as objektarts.Kante;
			if (k.Profil == null)
				return false;
			if (!k.Profil.ProfilbreiteIsSet)
				return false;
			d = k.Profil.Profilbreite / 1000.0;
			return true;
		}

	}
	class ShaftSewageObjectHelper
	{
		/// <summary>
		/// Easier fetching Schact from Ab.
		/// </summary>
		public static Schacht GetSchacht(AbwassertechnischeAnlagen ab)
		{
			if (ab == null)
				return null;

			if (ab.Auswahlelement == null)
				return null;

			objektarts.Knoten k = ab.Auswahlelement as objektarts.Knoten;
			if (k == null)
				return null;

			if (k.Auswahlelement == null)
				return null;

			Schacht sc = k.Auswahlelement as Schacht;
			return sc;
		}
		public static bool GetSchachtDepth(AbwassertechnischeAnlagen ab, ref double d)
		{
			if (ab.Auswahlelement == null)
				return false;
			objektarts.Knoten k = ab.Auswahlelement as objektarts.Knoten;
			if (k == null)
				return false;
			Schacht sc = k.Auswahlelement as Schacht;
			if (sc == null)
				return false;
			if (!sc.SchachttiefeIsSet)
				return false;
			d = sc.Schachttiefe;
			return true;
		}


		public class SMPData
		{
			public double Laenge { get; set; }
			public double Breite { get; set; }
			public double Hoehe { get; set; }
			public string Material { get; set; }
			public SMPData()
			{
				Laenge = 1;
				Breite = 1;
				Hoehe = 0;
				Material = "";
			}
		}

		/// <summary>
		/// If data found, it is updated. Otherwise, parameters are not changed.
		/// </summary>
		/// <param name="laenge"></param>
		/// <param name="breite"></param>
		/// <param name="hoehe"></param>
		/// <param name="material"></param>
		public static SMPData Get_SMP_Data(Schacht sc, SMPData data = null)
		{
			// If no data provided, use default one.
			if (data == null)
				data = new SMPData();
			// Assign Unterteil, and if not found use Aufbau.
			// We assign in reverse order - this allows not to use flags to indicate if field has been set.
			// Set aufbau, then if unterteil is found overwrite.
			Aufbau af = sc.Aufbau;
			if (af != null)
			{
				if (af.LaengeAufbauIsSet)
					data.Laenge = af.LaengeAufbau;
				if (af.BreiteAufbauIsSet)
					data.Breite = af.BreiteAufbau;
				if (af.HoeheAufbauIsSet)
					data.Hoehe = af.HoeheAufbau;
				if (af.MaterialAufbauIsSet)
					data.Material = af.MaterialAufbau;
			}

			Unterteil ut = sc.Unterteil;
			if (ut != null)
			{
				if (ut.LaengeUnterteilIsSet)
					data.Laenge = ut.LaengeUnterteil;
				if (ut.BreiteUnterteilIsSet)
					data.Breite = ut.BreiteUnterteil;
				if (ut.HoeheUnterteilIsSet)
					data.Hoehe = ut.HoeheUnterteil;
				if (ut.MaterialUnterteilIsSet)
					data.Material = ut.MaterialUnterteil;
			}
			return data;
		}
	}

}