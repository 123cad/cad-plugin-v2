﻿using CadPlugin.Inventories.Pipes;
using CadPlugin.Inventories.Shafts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers
{
	public class SewageObjectsCollection : IEnumerable<SewageObject>
	{
		//NOTE Only single object is supported for now - pipe with single name and shaft.
		private Dictionary<string, Pipe> pipes = new Dictionary<string, Pipe>();
		private Dictionary<string, Shaft> shafts = new Dictionary<string, Shaft>();
		public void AddObject(SewageObject o)
		{
			if (o is Pipe)
			{
				if (!pipes.ContainsKey(o.Abwasser.Objektbezeichnung))
					pipes.Add(o.Abwasser.Objektbezeichnung, (Pipe)o);
				else
					throw new NotSupportedException("Pipe already exists in collection!");
			}
			else if (o is Shaft)
			{
				if (!shafts.ContainsKey(o.Abwasser.Objektbezeichnung))
					shafts.Add(o.Abwasser.Objektbezeichnung, (Shaft)o);
				else
					throw new NotSupportedException("Shaft already exists in collection!");
			}
			else
				throw new NotSupportedException($"Object of type {o.GetType()} is not supported!");
		}
		public void Clear() { pipes.Clear();shafts.Clear(); }
		public IEnumerable<Shaft> GetShafts() => shafts.Values;
		public IEnumerable<Pipe> GetPipes() => pipes.Values;
		public Shaft GetShaft(string name)
		{
			if (name == null)
				return null;
			if (shafts.ContainsKey(name))
				return shafts[name];
			return null;
		}
		public Pipe GetPipe(string name)
		{
			if (pipes.ContainsKey(name))
				return pipes[name];
			return null;
		}
		public IEnumerator<SewageObject> GetEnumerator()
		{
			return pipes.Values.Select(x=>(SewageObject)x).Concat(shafts.Values).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return pipes.Values.Select(x=>(SewageObject)x).Concat(shafts.Values).GetEnumerator();
		}
	}
}
