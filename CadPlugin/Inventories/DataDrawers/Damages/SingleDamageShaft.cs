﻿using CadPlugin.Inventories.Damages;
using Isybau2015.Identifikations.Datenkollektives.Zustandsdatenkollektivs.InspizierteAbwassertechnischeAnlagens.OptischeInspektions.Knotens.Inspektionsdaten;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.Damages
{
    class SingleDamageShaft : SingleDamage
    {
        public override string Ch1 => kZustand.Charakterisierung1;

        public override string Ch2 => kZustand.Charakterisierung2;

        public override string Code => kZustand.InspektionsKode;

        public override string Q1 => kZustand.Auswahlelement1NumerischFormatted;

        public override string Q2 => kZustand.Auswahlelement2NumerischFormatted;
        private KZustand kZustand { get; set; }
        public SingleDamageShaft(KZustand kzus)
        {
            kZustand = kzus;
        }
    }
}
