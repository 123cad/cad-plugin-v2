﻿using CadPlugin.Inventories.Linetypes;
using Isybau2015.Isybau2015.ReferenceTableEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.Drawers.Linetypes.PipeLinetypes
{
	class StatusLinetype : PipeLinetype
	{
		public override string GetLinetype()
		{
			string linetype = "";
            linetype = GetEntwaesserungsartLinetype(obj.Abwasser);
            //switch (obj.Abwasser.Status)
            //{
            //	case (int)G105_Status.vorhanden:
            //    case (int)G105_Status.geplant:
            //    case (int)G105_Status.ausserBetrieb:
            //		linetype = GetEntwaesserungsartLinetype(obj.Abwasser);
            //		break;
            //	default:
            //		linetype = LinetypeManager.Continuous;
            //		break;
            //}
            return linetype;
		}
	}
}
