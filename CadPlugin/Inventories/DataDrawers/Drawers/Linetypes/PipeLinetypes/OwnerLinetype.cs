﻿

using CadPlugin.Inventories.Linetypes;
using Isybau2015.Isybau2015.ReferenceTableEnums;
using Isybau2015.Ownerships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.Drawers.Linetypes.PipeLinetypes
{
	class OwnerLinetype : PipeLinetype
	{
		public override string GetLinetype()
		{
			/*string linetype = "";

			switch (obj.Abwasser.Status)
			{
				case (int)G105_Status.vorhanden:
					linetype = GetEntwaesserungsartLinetype(obj.Abwasser);
					break;
				default:
					linetype = LinetypeManager.Continuous;
					break;
			}
			return linetype;*/
			var owner = obj.Abwasser.Parent.Parent.Parent.Owners.GetOwnership(obj.Abwasser);
			string linetype = LinetypeManager.Continuous;
			if (owner?.Owner == "K")
				linetype = GetEntwaesserungsartLinetype(obj.Abwasser);
			return linetype;
		}
	}
}
