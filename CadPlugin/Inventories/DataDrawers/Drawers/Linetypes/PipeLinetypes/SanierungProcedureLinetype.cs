﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.Drawers.Linetypes.PipeLinetypes
{
	class SanierungProcedureLinetype : PipeLinetype
	{
		public override string GetLinetype()
		{
			return GetEntwaesserungsartLinetype(obj.Abwasser);
		}
	}
}
