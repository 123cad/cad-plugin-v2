﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace   CadPlugin.Inventories.Linetypes
{
	public class LinetypeManager
	{
		// Linetype description (first is lenght of line, next is lenght of space, p is for point)
		/// <summary>
		/// Line of 2, space of 1.
		/// </summary>
		public static readonly string Dashed_21 = "DASHED_21";
		/// <summary>
		/// Line of 3, space of 1, a point, space of 1.
		/// </summary>
		public static readonly string Dashed_31p1 = "DASHED_31p1";

		public static readonly string Continuous = "CONTINUOUS";
		/// <summary>
		/// Checks if used Linetypes are loaded and loads them if they are not.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="lt"></param>
		public static void LoadLinetypes(Transaction tr, Database db)
		{
			LinetypeTable lt = (LinetypeTable)tr.GetObject(db.LinetypeTableId, OpenMode.ForRead);

			double scale = 2;
			if (!lt.Has(Dashed_21))
			{
				lt.UpgradeOpen();
				LinetypeTableRecord ltr = new LinetypeTableRecord();
				ltr.Name = Dashed_21;
				ltr.Comments = Dashed_21 + " , __ __ __ ";
				lt.Add(ltr);
				tr.AddNewlyCreatedDBObject(ltr, true);
				ltr.NumDashes = 2;
				ltr.SetDashLengthAt(0, 1 / scale);
				ltr.SetDashLengthAt(1, -0.5 / scale);
				ltr.Dispose();
				lt.DowngradeOpen();
			}
			if (!lt.Has(Dashed_31p1))
			{
				lt.UpgradeOpen();
				LinetypeTableRecord ltr = new LinetypeTableRecord();
				ltr.Name = Dashed_31p1;
				lt.Add(ltr);
				tr.AddNewlyCreatedDBObject(ltr, true);
				ltr.Comments = Dashed_31p1 + " , ___ . ___ . ___ . ";
				ltr.NumDashes = 4;
				ltr.SetDashLengthAt(0, 1 / scale);
				ltr.SetDashLengthAt(1, -1.0 / 3 / scale);
				ltr.SetDashLengthAt(2, 0.05 / scale);
				ltr.SetDashLengthAt(3, -1.0 / 3 / scale);
				ltr.Dispose();
				lt.DowngradeOpen();
			}
		}
	}
}
