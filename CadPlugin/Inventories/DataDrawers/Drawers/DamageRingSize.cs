﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.Drawers
{
	//NOTE
	//NOTE To avoid overlapping of 2d/3d damage rings with pipe/shaft and
	//NOTE among damages themselves, each damage class has bigger diameter
	//NOTE than class before.
	
	/// <summary>
	/// Defines percentual ring size of each damage class.
	/// </summary>
	class DamageRingSize
	{
		/// <summary>
		/// Lowest damage class is drawn with this percentage of pipe diameter.
		/// </summary>
		public static double MinimumRingPercentageSize { get; private set; } = 1.005;

		private static double[] classesPercentage = new double[7];

		static DamageRingSize()
		{
			UpdateClasses();
		}
		/// <summary>
		/// 1.0 = 100%
		/// </summary>
		/// <param name="p"></param>
		public static void SetNewMinimumPercentage(double p)
		{
			MinimumRingPercentageSize = p;
			UpdateClasses();
		}
		private static void UpdateClasses()
		{
			double step = MinimumRingPercentageSize - 1.0;
			for (DamageClass d = DamageClass.ClassNotSet; d <= DamageClass.Class5; d++)
			{
				int current = (int)d;
				current++; // Set from [-1,5] to [0-6]
				double percent = MinimumRingPercentageSize + current * step;

				classesPercentage[current] = percent;
			}
		}
		/// <summary>
		/// Returns diameter of damage, related to damage class.
		/// </summary>
		/// <param name="pipeDiameter"></param>
		/// <param name="dmg"></param>
		/// <returns></returns>
		public static double GetRingDiameterForDamage(double pipeDiameter, DamageClass dmg)
		{
			int dmgClass = (int)dmg;
			dmgClass++;// Set from [-1,5] to [0-6]

			double d = pipeDiameter * classesPercentage[dmgClass];
			return d;
		}
		/// <summary>
		/// Complete pipe damage has to be below all other damages on the pipe (because of overlapping).
		/// </summary>
		/// <param name="pipeDiameter"></param>
		/// <param name="dmg"></param>
		/// <returns></returns>
		public static double GetRingDiamaterForCompletePipe(double pipeDiameter, DamageClass dmg)
		{
			// 2 options:	1 - fixed between pipe and ClassNotSet.
			//				2 - below damage class, but above all other classes on the pipe (allow preview?)
			double d = 0;
			int opt = 1;
			double step = MinimumRingPercentageSize - 1.0;
			switch (opt)
			{
				case 1: d = pipeDiameter * (1.0 + step / 2);  break;
				case 2:
					int dmgClass = (int)dmg;
					dmgClass++;// Set from [-1,5] to [0-6]
					d = pipeDiameter * (classesPercentage[dmgClass] - step);
					break;
			}
			return d;
		}
	}
}
