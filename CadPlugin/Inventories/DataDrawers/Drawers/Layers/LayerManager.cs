﻿using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts;
using Isybau2015.Isybau2015.ReferenceTableEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.DataDrawers.Drawers.Layers
{
	/// <summary>
	/// Status data can be drawn on different layers.
	/// </summary>
	class StatusDataLayers
	{
		/// <summary>
		/// Object which is drawn with color of highest damage.
		/// </summary>
		public readonly string HighestObjectClass;
		/// <summary>
		/// Station distance and damage class.
		/// </summary>
		public readonly string Station;
		public StatusDataLayers(string singleLayer):this(singleLayer, singleLayer)
		{
		}
		public StatusDataLayers(string highest, string station)
		{
			HighestObjectClass = highest;
			Station = station;
		}
	}
	class LayerManager
	{
		private static LayerManager _Instance = null;
		public static LayerManager Instance
		{
			get
			{
				if (_Instance == null)
					_Instance = new LayerManager();
				return _Instance;
			}
		}
		private const string UnknownLayer = "123CAD_inventary_others";

		private const string StatusPipeHighestObjectLayer = "123_Kanal_Zustand_Klassifizierung";
		private const string StatusPipeDamageLayer = "123_Kanal_Zustand";

		public const string SanierungConceptLayerPipe = "123_sanierung_konzept_kante";
		public const string SanierungConceptLayerShaft = "123_sanierung_konzept_knoten";
		public const string SanierungProcedureLayerPipe = "123_sanierung_verfahren_kante";
		public const string SanierungProcedureLayerShaft = "123_sanierung_verfahren_knoten";

		private const string StatusShaftHighestObjectLayer = StatusPipeHighestObjectLayer;
		private const string StatusShaftDamageLayer = StatusPipeDamageLayer;

		private StatusDataLayers UnknownLayers = new StatusDataLayers(UnknownLayer);
		private Dictionary<int, string> mainLayersRW = new Dictionary<int, string>();
		private Dictionary<int, string> mainLayersMW = new Dictionary<int, string>();
		private Dictionary<int, string> mainLayersSW = new Dictionary<int, string>();
		private Dictionary<int, string> mainLayersOthers = new Dictionary<int, string>();
		private Dictionary<int, string> mainLayersRWLabel = new Dictionary<int, string>();
		private Dictionary<int, string> mainLayersMWLabel = new Dictionary<int, string>();
		private Dictionary<int, string> mainLayersSWLabel = new Dictionary<int, string>();
		private Dictionary<int, string> mainLayersOthersLabel = new Dictionary<int, string>();
		private const string LabelSuffix = "_label";

		/// <summary>
		/// All layers that are defined in LayerManager.
		/// </summary>
		private static HashSet<string> allLayers;
		
		private StatusDataLayers pipeLayers;
		private StatusDataLayers shaftLayers;

		private LayerManager()
		{
			Action<Dictionary<int, string>, string, string> setup = (dic, suffix, entw) =>
			{
				dic.Add((int)G200_Kantentyp.Haltung, $"123_{entw}_Haltung{suffix}");
				dic.Add((int)G200_Kantentyp.Leitung, $"123_{entw}_Leitung{suffix}");
				dic.Add((int)G200_Kantentyp.Rinne,	 $"123_{entw}_Rinne{suffix}");
				dic.Add((int)G200_Kantentyp.Gerinne, $"123_{entw}_Gerinne{suffix}");
				
				dic.Add((int)G300_Knotentyp.Schacht+5, $"123_{entw}_Schacht{suffix}");
				dic.Add((int)G300_Knotentyp.Anschlusspunkt+5, $"123_{entw}_Anschlussspunkte{suffix}");
				dic.Add((int)G300_Knotentyp.Bauwerk+5, $"123_{entw}_Bauwerk{suffix}");
			};
			string s = "";
			var d = mainLayersRW; setup(d, s, "RW");
			d = mainLayersMW; setup(d, s, "MW");
			d = mainLayersSW; setup(d, s, "SW");
			d = mainLayersOthers; setup(d, s, "_Sonstige");
			s = LabelSuffix;
			d = mainLayersRWLabel; setup(d, s, "RW");
			d = mainLayersMWLabel; setup(d, s, "MW");
			d = mainLayersSWLabel; setup(d, s, "SW");
			d = mainLayersOthersLabel; setup(d, s, "_Sonstige");

			pipeLayers = new StatusDataLayers(StatusPipeHighestObjectLayer, StatusPipeDamageLayer);
			shaftLayers = new StatusDataLayers(StatusShaftHighestObjectLayer, StatusShaftDamageLayer);

			if (allLayers == null)
			{
				allLayers = new HashSet<string>(
				 mainLayersRW.Values
				.Concat(mainLayersSW.Values)
				.Concat(mainLayersMW.Values)
				.Concat(mainLayersOthers.Values)
				.Concat(mainLayersSWLabel.Values)
				.Concat(mainLayersSWLabel.Values)
				.Concat(mainLayersMWLabel.Values)
				.Concat(mainLayersOthersLabel.Values)
				.Distinct()
				.ToArray());
				allLayers.Add(UnknownLayer);
				allLayers.Add(StatusPipeDamageLayer);
				allLayers.Add(StatusPipeHighestObjectLayer);
				allLayers.Add(SanierungConceptLayerPipe);
				allLayers.Add(SanierungConceptLayerShaft);
				allLayers.Add(SanierungProcedureLayerPipe);
				allLayers.Add(SanierungProcedureLayerShaft);
				if (!allLayers.Contains(StatusShaftDamageLayer))
					allLayers.Add(StatusShaftDamageLayer);
				if (!allLayers.Contains(StatusShaftHighestObjectLayer))
					allLayers.Add(StatusShaftHighestObjectLayer);
			}
		}
		/// <summary>
		/// Must be called before drawing data from inventary.
		/// In case some layers are deleted.
		/// </summary>
		/// <param name="db"></param>
		public void Initialize(Database db)
		{
			allLayers = new HashSet<string>()
			{
				UnknownLayer,
				SanierungProcedureLayerPipe,
				SanierungProcedureLayerShaft,
				SanierungConceptLayerPipe,
				SanierungConceptLayerShaft,
				StatusPipeHighestObjectLayer,
				StatusPipeDamageLayer,
				StatusShaftHighestObjectLayer,
				StatusShaftDamageLayer,
			};
			allLayers = new HashSet<string>(allLayers.Concat(mainLayersRW.Select(x1 => x1.Value))
								.Concat(mainLayersMW.Select(x2 => x2.Value))
								.Concat(mainLayersSW.Select(x3 => x3.Value)
								.Concat(mainLayersOthers.Select(x4 => x4.Value))
								.Concat(mainLayersRWLabel.Select(x4 => x4.Value))
								.Concat(mainLayersMWLabel.Select(x4 => x4.Value))
								.Concat(mainLayersSWLabel.Select(x4 => x4.Value))
								.Concat(mainLayersOthersLabel.Select(x4 => x4.Value))						
								));
			foreach (string s in allLayers)
				Common.CADLayerHelper.AddLayerIfDoesntExist(db, s);
		}
		/// <summary>
		/// Removes unused layers.
		/// </summary>
		/// <param name="db"></param>
		public void Clean(Database db)
		{
			System.Diagnostics.Debug.WriteLine("Starting layers cleaning.");

			Dictionary<string, bool> layersUsage = allLayers.ToDictionary(x => x, y=>false);
			
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				BlockTable bt = tr.GetObjectRead<BlockTable>(db.BlockTableId);
				BlockTableRecord btr = tr.GetObjectRead<BlockTableRecord>(bt[BlockTableRecord.ModelSpace]);
				foreach (ObjectId oid in btr)
				{
					Entity e = tr.GetObjectRead<Entity>(oid);
					if (e == null)
						continue;
					if (layersUsage.ContainsKey(e.Layer))
						layersUsage[e.Layer] = true;
				}
				// On the forum told that it helps for performance, although no changes were made.
				tr.Commit();
			}
			Common.CADLayerHelper.DeleteLayers(db, layersUsage.Where(x=>!x.Value).Select(x=>x.Key).ToArray());
			System.Diagnostics.Debug.WriteLine("Finished layers cleaning");

		}
		/// <summary>
		/// All entities for main data for single object are on the single layer.
		/// </summary>
		/// <returns></returns>
		public string GetMainDataLayer(AbwassertechnischeAnlagen ab, bool labelLayer = false)
		{
			string res = UnknownLayer;
			var dic = labelLayer?mainLayersRWLabel:mainLayersRW;
			string ent = (ab?.Entwaesserungsart ?? "").ToUpper();
			switch (ent)
			{
				case "KR":
				case "DR":
				case "GR":
					break;
				case "KM":
				case "DM":
				case "GM":
					dic = labelLayer?mainLayersMWLabel:mainLayersMW; break;
				case "KS":
				case "DS":
				case "GS":
					dic = labelLayer?mainLayersSWLabel:mainLayersSW; break;
				default: dic = mainLayersOthers; break;
			}
			switch (ab.Objektart)
			{
				case (int)G100_Objektart.Kante:
					var kante = ab.Auswahlelement as Kante;
					if (kante != null)
					{
						G200_Kantentyp typ = G200_Kantentyp.Haltung;
						switch (kante.KantenTyp)
						{
							case (int)G200_Kantentyp.Haltung: break;
							case (int)G200_Kantentyp.Leitung: typ = G200_Kantentyp.Leitung; break;
							case (int)G200_Kantentyp.Rinne: typ = G200_Kantentyp.Rinne; break;
							case (int)G200_Kantentyp.Gerinne: typ = G200_Kantentyp.Gerinne; break;
						}
						res = dic[(int)typ];
					}
					break;
				case (int)G100_Objektart.Knoten:
					var knoten = ab.Auswahlelement as Knoten;
					if (knoten != null)
					{
						G300_Knotentyp typ = G300_Knotentyp.Schacht;
						switch(knoten.KnotenTyp)
						{
							case (int)G300_Knotentyp.Schacht: break;
							case (int)G300_Knotentyp.Anschlusspunkt: typ = G300_Knotentyp.Anschlusspunkt;  break;
							case (int)G300_Knotentyp.Bauwerk: typ = G300_Knotentyp.Bauwerk;  break;
						}
						res = dic[(int)typ+5];
					}
					break;
				default:
					return UnknownLayer;
			}
			return res;
		}

		public StatusDataLayers GetStatusDataLayer(AbwassertechnischeAnlagen ab)
		{
			switch (ab.Objektart)
			{
				case (int)G100_Objektart.Kante:
					return pipeLayers;
				case (int)G100_Objektart.Knoten:
					return shaftLayers;
				default:
					return UnknownLayers;
			}
		}

	}
}
