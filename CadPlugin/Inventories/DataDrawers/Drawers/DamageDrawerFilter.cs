﻿using CadPlugin.Inventories.Damages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.Drawers
{
	abstract class DamageDrawerFilter
	{
		protected Dictionary<DamageStation, List<SingleDamage>> Data = new Dictionary<DamageStation, List<SingleDamage>>();
		/// <summary>
		/// Uses all damages from the station.
		/// </summary>
		/// <param name="station"></param>
		public void AddStation(DamageStation station)
		{
			AddDamages(station, station);
		}
		public void AddDamages(DamageStation station, IEnumerable<SingleDamage> damages)
		{
			if (!Data.ContainsKey(station))
				Data.Add(station, new List<SingleDamage>());
			var existingStation = Data[station];
			existingStation.AddRange(damages.ToArray());
		}
		public void Clear()
		{
			Data.Clear();
		}
		public abstract FilteredDrawingData GetFilteredData();
	}
}
