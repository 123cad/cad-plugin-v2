﻿using CadPlugin.Inventories.Damages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.Drawers
{
	class FilteredStation
	{
		public DamageStation Station { get; set; }
		public List<SingleDamage> Damages { get; private set; } = new List<SingleDamage>();
	}

	/// <summary>
	/// Holds data which is drawn.
	/// </summary>
	class FilteredDrawingData
	{
		/// <summary>
		/// Damages grouped by stations.
		/// </summary>
		public List<FilteredStation> Stations { get; private set; } = new List<FilteredStation>();
	}
}
