﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.Drawers.DamageDrawerFilters
{
	class SelectedDamages : DamageDrawerFilter
	{
		/*
		 
			/// <summary>
		/// Only this class has color - all others are of the same color.
		/// </summary>
		private HashSet<DamageClass> selectedClasses { get; set; }
		/// <summary>
		/// Color for classes which are not selected.
		/// </summary>
		public static Color NonSelectedClass { get; private set; }
		public PipeDamageColorSelectedClass(IEnumerable<DamageClass> selected)
		{
			selectedClasses = new HashSet<DamageClass>();
			foreach (DamageClass dc in selected)
				if (!selectedClasses.Contains(dc))
					selectedClasses.Add(dc);
			NonSelectedClass = Color.FromColor(System.Drawing.Color.Gray);
		}
		public override Color GetDamageColor(DamageClass dg)
		{
			if (selectedClasses.Contains(dg))
				return ColorAssociations.GetDamageClassColor(dg);
			else
				return NonSelectedClass;
		}
	*/
		/// <summary>
		/// Only this class has color - all others are of the same color.
		/// </summary>
		private HashSet<DamageClass> selectedClasses { get; set; } = new HashSet<DamageClass>();
		public SelectedDamages(IEnumerable<DamageClass> selected)
		{
			foreach (var dc in selected)
				selectedClasses.Add(dc);
		}
		public override FilteredDrawingData GetFilteredData()
		{
			var data = Data.Select(x =>
			{
				var t = new FilteredStation() { Station = x.Key };
				t.Damages.AddRange(x.Value.Where(m => selectedClasses.Contains(m.DamageClass)));
				return t;
			}).Where(x=>x.Damages.Count > 0);
			var p = new FilteredDrawingData();
			p.Stations.AddRange(data);
			return p;
		}
	}
}
