﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.Drawers.DamageDrawerFilters
{
	class AllDamages : DamageDrawerFilter
	{
		public override FilteredDrawingData GetFilteredData()
		{
			var data = Data.Select(x => {
				var t = new FilteredStation() { Station = x.Key };
				t.Damages.AddRange(x.Value);
				return t;
			});
			var p = new FilteredDrawingData();
			p.Stations.AddRange(data);
			return p;
		}
	}
}
