﻿using CadPlugin.Common;
using CadPlugin.Inventories.Damages;
using CadPlugin.Inventories.Drawers.Colors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Inventories.Drawers.DamageDescriptionAlignments;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.Drawers
{
	/// <summary>
	/// Draws aligned text for specified station.
	/// </summary>
	abstract class DamageTextDrawer
	{
		/// <summary>
		/// Represents text size, if no other size is used (also can be scaled, 
		/// depending on user selection).
		/// </summary>
		public static double DefaultTextHeight { get; set; } = 0.5;
		protected TransactionData Tr { get; set; }

		protected List<StationTextData> Stations { get; private set; }
        protected StationDamagesDrawer damagesDescriptiondrawer { get; private set; }
		public DamageTextDrawer(TransactionData tr, StationDamagesDrawer damageDrawer)
		{
			Tr = tr;
			Stations = new List<StationTextData>();
            damagesDescriptiondrawer = damageDrawer;
		}
		/// <summary>
		/// Draws text for filtered subset of (single) Station damages (provided as parameter).
		/// </summary>
		public abstract IEnumerable<Entity> DrawText(DamageColor colorMgr, double textSize,  bool draw3d, DamageStation st, IEnumerable<SingleDamage> filteredStationDamages, PositionVector position);
	}
}
