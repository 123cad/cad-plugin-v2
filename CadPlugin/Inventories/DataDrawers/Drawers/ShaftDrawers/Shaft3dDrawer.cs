﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.Shafts;
using CadPlugin.Inventories.Damages;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Knotens;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.DataDrawers.Drawers;
using CadPlugin.Inventories.DataDrawers.Drawers.Layers;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.Drawers.ShaftDrawers
{
	class Shaft3dDrawer : ShaftDrawer
	{
		public Shaft3dDrawer(TransactionData data, SewageObjectColorManager colorManager, DamageTextDrawer damageTxt, TextSizeDefinitions textDef, DamageDrawerFilter damageFilter, InventoryManager.CreatedCADEntities createdEntities, bool draw3d) : base(data, colorManager, damageTxt, textDef, damageFilter, createdEntities, draw3d)
		{
		}

		public override void DrawDamages(Shaft shaft, ShaftType shaftType)
		{
			DamageClass highest = DamageClass.ClassNotSet;
			DamageFilter.Clear();
			foreach (DamageStation st in shaft.GetDamagesCollection())
				DamageFilter.AddDamages(st, st);
			FilteredDrawingData filteredData = DamageFilter.GetFilteredData();
			var layer = LayerManager.Instance.GetStatusDataLayer(shaft.Abwasser);

			foreach (FilteredStation fst in filteredData.Stations)
			{
				DamageStation st = fst.Station;
				PositionVector position = shaft.GetPositionAtStation(st.Distance).Value;
				Point3d point = position.Position.ToCADPoint();
				/*if (IsDrawn3d)
					point = new Point3d(point.X, point.Y, 0);*/
				foreach (SingleDamage dmg in st)
				{
					if (highest < dmg.DamageClass)
						highest = dmg.DamageClass;
					Entity e = null;
					switch (shaftType)
					{
						case ShaftType.Circle:
							e = ShaftDrawer.CreateShaftBase(Tr, ShaftType.Circle, point, shaft.Radius * 2 * percentageOfDamageRing);
							break;
						case ShaftType.Rectangle:
							Schacht s = ShaftSewageObjectHelper.GetSchacht(shaft.Abwasser);
							ShaftSewageObjectHelper.SMPData dt = ShaftSewageObjectHelper.Get_SMP_Data(s);
							e = ShaftDrawer.CreateShaftBase(Tr, ShaftType.Rectangle, point, dt.Laenge * percentageOfDamageRing, dt.Breite * percentageOfDamageRing);
							break;
					}
					// e is on (0,0,0), move it to final position.
					Matrix3d move = Matrix3d.Displacement(point.FromCADPoint().GetAsVector()
															.ToCADVector()
															.Add(new Vector3d(0,0,fst.Station.Distance)));
															//.Add(new MyUtilities.Geometry.Vector3d(0, 0, fst.Station.Distance))
					e.TransformBy(move);
					// Set layer.
					// Set line weight.
					Color col = ColorManager.DamageColor.GetDamageColor(dmg.DamageClass);
					e.Color = col;
					e.Layer = layer.Station;
				}
				foreach(var e in DamageTextManager.DrawText(ColorManager.DamageColor, TextSize.DamageText, IsDrawn3d, st, fst.Damages, position))
				{
					e.Layer = layer.Station;
					CreatedEntities.AddShaftEntity(e);
				}
				//DamageTextManager.AddText(st, position);
			}
			//DamageTextManager.DrawAllText(ColorManager.DamageColor, IsDrawn3d);
			//DamageTextManager.Clear();


			if (DrawDamageShaft && shaft.IsShaftComplete)
			{
				IEnumerable<MyUtilities.Geometry.Point3d> pts = shaft.GetAllPoints();
				//if (pts.Count() >= 2 && pts.ElementAt(0).GetVectorTo(pts.ElementAt(1)).Length > 0.001)
				if (shaft.IsShaftComplete)
				{
					Color c = Inventories.Colors.ColorAssociations.GetDamageClassColor(highest);

					Polyline pl = Tr.CreateEntity<Polyline>();
					pl.AddPoints(pts.Select(x => x.ToCADPoint().GetAs2d()).ToArray());

					pl.Elevation = pts.First().Z;
					/*Polyline3d pl = Tr.Tr.CreateEntity<Polyline3d>(Tr.Btr);
					foreach (MyUtilities.Geometry.Point3d p in pts)
					{
						var pt = p;
						/*if (IsDrawn3d)
							pt = pt.GetOnHeight0();* /
						pl.AppendPoint(p.ToCADPoint());
					}*/
					CreatedEntities.AddShaftEntity(pl, true);
					pl.Layer = layer.HighestObjectClass;
				}
			}
		}

		public override void DrawShaft(Shaft shaft, ShaftType shaftType)
		{
			// Do nothing. Only BlockReference represents shaft, and it is drawn in MainDrawer.
		}
	}
}
