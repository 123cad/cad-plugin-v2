﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.Shafts;
using CadPlugin.Inventories.Damages;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Knotens;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.DataDrawers.Drawers;
using CadPlugin.Inventories.DataDrawers.Drawers.Layers;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.Drawers.ShaftDrawers
{
	class ShaftVisual3dDrawer : ShaftDrawer
	{
		public ShaftVisual3dDrawer(TransactionData data, SewageObjectColorManager colorManager, DamageTextDrawer damageTxt, TextSizeDefinitions textDef, DamageDrawerFilter damageFilter, InventoryManager.CreatedCADEntities createdEntities) : base(data, colorManager, damageTxt, textDef, damageFilter, createdEntities, true)
		{
		}

		public override void DrawDamages(Shaft shaft, ShaftType shaftType)
		{
			DamageClass highest = DamageClass.ClassNotSet;
			DamageFilter.Clear();
			foreach (DamageStation st in shaft.GetDamagesCollection())
				DamageFilter.AddDamages(st, st);
			FilteredDrawingData filteredData = DamageFilter.GetFilteredData();
			var layer = LayerManager.Instance.GetStatusDataLayer(shaft.Abwasser);

			foreach (FilteredStation fst in filteredData.Stations)
			{
				DamageStation st = fst.Station;
				double startDistance = st.Distance;
				PositionVector position = shaft.GetPositionAtStation(startDistance).Value;
				foreach (SingleDamage dmg in st)
				{
					if (highest < dmg.DamageClass)
						highest = dmg.DamageClass;
					double endDistance = startDistance + dmg.Length;
					IEnumerable<MyUtilities.Geometry.Point3d> pts = shaft.GetPoints(startDistance, endDistance);
					Color c = ColorManager.DamageColor.GetDamageColor(dmg.DamageClass);
					Solid3d solid = null;
					Entity baseEntity = null;
					switch (shaftType)
					{
						case ShaftType.Circle:
							baseEntity = ShaftDrawer.CreateShaftBase(Tr, ShaftType.Circle, pts.First().ToCADPoint(), shaft.Radius * 2 * percentageOfDamageRing);
							break;
						case ShaftType.Rectangle:
							Schacht s = ShaftSewageObjectHelper.GetSchacht(shaft.Abwasser);
							ShaftSewageObjectHelper.SMPData dt = ShaftSewageObjectHelper.Get_SMP_Data(s);
							baseEntity = ShaftDrawer.CreateShaftBase(Tr, ShaftType.Rectangle, pts.First().ToCADPoint(), dt.Laenge * percentageOfDamageRing, dt.Breite * percentageOfDamageRing);
							break;
					}
					solid = CADSolid3dHelper.CreateSolidAlongPath(Tr.GetAsWrapper(), pts, new ComplexSolidProfileManager(new ComplexSolidProfile(baseEntity)));
					baseEntity.Erase();
					solid.Color = c;
					solid.Layer = layer.Station;

				}
				foreach(var e in DamageTextManager.DrawText(ColorManager.DamageColor, TextSize.DamageText, IsDrawn3d, st, fst.Damages, position))
				{
					e.Layer = layer.Station;
					CreatedEntities.AddShaftEntity(e);
				}
				//DamageTextManager.AddText(st, position);
			}
			//DamageTextManager.DrawAllText(ColorManager.DamageColor, IsDrawn3d);
			//DamageTextManager.Clear();


			if (DrawDamageShaft && shaft.IsShaftComplete)
			{
				IEnumerable<MyUtilities.Geometry.Point3d> pts = shaft.GetAllPoints();
				double diameter = DamageRingSize.GetRingDiamaterForCompletePipe(shaft.Radius * 2, highest);
				Solid3d solid = Common.CADSolid3dHelper.CreateCylinderAlongPath(Tr.GetAsWrapper(), pts, diameter);
				solid.Color = Inventories.Colors.ColorAssociations.GetDamageClassColor(highest);
				solid.Layer = layer.HighestObjectClass;

				// Should it be drawn if class not set?
				if (highest == DamageClass.ClassNotSet)
					solid.Color = Color.FromColor(System.Drawing.Color.Yellow);
			}
		}

		public override void DrawShaft(Shaft shaft, ShaftType shaftType)
		{
			IEnumerable<MyUtilities.Geometry.Point3d> pts = shaft.GetAllPoints();
			Solid3d solid = null;
			Entity baseEntity = null;
			switch (shaftType)
			{
				case ShaftType.Circle:
					baseEntity = ShaftDrawer.CreateShaftBase(Tr, ShaftType.Circle, pts.First().ToCADPoint(), shaft.Radius * 2);
					break;
				case ShaftType.Rectangle:
					Schacht s = ShaftSewageObjectHelper.GetSchacht(shaft.Abwasser);
					ShaftSewageObjectHelper.SMPData dt = ShaftSewageObjectHelper.Get_SMP_Data(s);
					baseEntity = ShaftDrawer.CreateShaftBase(Tr, ShaftType.Rectangle, pts.First().ToCADPoint(), dt.Laenge, dt.Breite);
					break;
			}
			solid = CADSolid3dHelper.CreateSolidAlongPath(Tr.GetAsWrapper(), pts, new ComplexSolidProfileManager(new ComplexSolidProfile(baseEntity)));
			baseEntity.Erase();
			solid.Color = ColorManager.ObjectColor.GetObjectColor();

			CreatedEntities.AddShaftEntity(solid, true);

			string layer = LayerManager.Instance.GetMainDataLayer(shaft.Abwasser);
			solid.Layer = layer;
		}
	}
}
