﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.Shafts;
using CadPlugin.Inventories.Damages;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Knotens;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.DataDrawers.Drawers;
using CadPlugin.Inventories.DataDrawers.Drawers.Layers;
using Inventory.CAD;
using CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers.Sanierung;
using CadPlugin.Inventories.DataDrawers.Drawers.Sanierungs;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.Drawers.ShaftDrawers
{
	class  SanierungShaftVisual3dDrawer : ShaftVisual3dDrawer, ISanierungDataSource
	{
		private bool drawConcept;
		private bool drawProcedures;
		private Dictionary<Shaft, ObjectMassnahmes> objectMassnahmesCollection = new Dictionary<Shaft, ObjectMassnahmes>();
		public SanierungShaftVisual3dDrawer(TransactionData data, SewageObjectColorManager colorManager, DamageTextDrawer damageTxt, 
									TextSizeDefinitions textDef, DamageDrawerFilter damageFilter, InventoryManager.CreatedCADEntities createdEntities,
									bool drawConcept, bool drawProcedures
									)
			: base(data, colorManager, damageTxt, textDef, damageFilter, createdEntities)
		{
			this.drawConcept = drawConcept;
			this.drawProcedures = drawProcedures;
		}

		public void SetObjectMassnahmes(IEnumerable<Tuple<SewageObject, ObjectMassnahmes>> objs)
		{
			objectMassnahmesCollection = objs.Where(x => x.Item1 is Shaft).ToDictionary(x => x.Item1 as Shaft, x => x.Item2);
		}
		public override void DrawDamages(Shaft shaft, ShaftType shaftType)
		{
			if (!drawProcedures)
				return;
			if (objectMassnahmesCollection == null
				|| !objectMassnahmesCollection.ContainsKey(shaft))
				return;
			ObjectMassnahmes o = objectMassnahmesCollection[shaft];
			IEnumerable<Entity> entities = ProcedureDrawer.DrawShaftSingleProcedure(Tr, o.Object, shaft, o.Massnahmes);
			foreach (Entity e in entities)
			{
				e.Layer = LayerManager.SanierungProcedureLayerShaft;
				CreatedEntities.AddShaftEntity(e);
			}
		}

		public override void DrawShaft(Shaft shaft, ShaftType shaftType)
		{
			if (!drawConcept)
				return;
			base.DrawShaft(shaft, shaftType);
			foreach (var v in DataDrawers.Drawers.Sanierungs.DrawSymbol.DrawShaftSymbol(Tr, shaft, IsDrawn3d))
			{
				v.Layer = LayerManager.SanierungConceptLayerShaft;
				CreatedEntities.AddShaftEntity(v);
			}
		}
	}
}
