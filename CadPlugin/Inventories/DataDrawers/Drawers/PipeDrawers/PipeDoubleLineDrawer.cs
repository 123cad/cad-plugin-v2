﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.Pipes;
using System.Diagnostics;
using CadPlugin.Inventories.Damages;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.DataDrawers.Drawers.Linetypes;
using CadPlugin.Inventories.DataDrawers.Drawers;
using CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers;
using CadPlugin.Inventories.DataDrawers.Drawers.Layers;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace   CadPlugin.Inventories.Drawers.PipeDrawers
{
	class PipeDoubleLineDrawer : PipeDrawerNonVisual
	{
		public PipeDoubleLineDrawer(TransactionData data, SewageObjectColorManager pipeColor, PipeLinetype pipeLinetype, DamageTextDrawer dmgText, TextSizeDefinitions textDef, DamageDrawerFilter damageFilter, InventoryManager.CreatedCADEntities createdEntities, bool drawIn3d)
							: base(data, pipeColor, pipeLinetype, dmgText, textDef, damageFilter, createdEntities, drawIn3d)
		{
		}

		public override void DrawDamages(Pipe pipe)
		{
			DamageFilter.Clear();
			foreach (DamageStation st in pipe.GetDamagesCollection())
				DamageFilter.AddDamages(st, st/*.Where(x => x.Code != "BCA" && x.Code != "BCC")*/);
			FilteredDrawingData filteredData = DamageFilter.GetFilteredData();

			DrawPipeDamages(pipe, filteredData);
		}
		protected override IEnumerable<Entity> CreatePipeLineWithSettings(Pipe pipe, string linetype, Color c, string layerName = null)
		{
			MyUtilities.Geometry.Point3d[] pts = pipe.GetAllPoints().ToArray();

			IList<Point3d> newPtsL = CADCommonHelper.CreateOffsetPoints(pts.Select(x => new Point3d(x.X, x.Y, x.Z)).ToList(), -pipe.Diameter / 2);
			IList<Point3d> newPtsR = CADCommonHelper.CreateOffsetPoints(pts.Select(x => new Point3d(x.X, x.Y, x.Z)).ToList(), pipe.Diameter / 2);
			/*if (IsDrawn3d)
			{
				newPtsL = newPtsL.Select(x => new Point3d(x.X, x.Y, 0)).ToList();
				newPtsR = newPtsR.Select(x => new Point3d(x.X, x.Y, 0)).ToList();
			}*/

			// Left and Right are not good, since method interpetates negative as making curve radius smaller - we can't know that 
			// (and it is not important).
			Entity left = null;
			Entity right = null;
			left = CreateFromPoints(newPtsL.Select(x => x.FromCADPoint()));
			right = CreateFromPoints(newPtsR.Select(x => x.FromCADPoint()));

			left.Color = c;
			right.Color = c;
			left.Linetype = linetype;
			right.Linetype = linetype;
			left.Layer = layerName;
			right.Layer = layerName;

			yield return left;
			yield return right;
		}
		public override void DrawPipe(Pipe pipe)
		{
			if (!pipe.IsPipeComplete)
				return;
			
			Color c = ColorManager.ObjectColor.GetObjectColor();
			string lineStyle = PipeLinetypeManager.GetLinetype();// GetEntwaesserungsartLinetype(pipe.Abwasser);
			string layerName = LayerManager.Instance.GetMainDataLayer(pipe.Abwasser);
			IEnumerable<Entity> created = CreatePipeLineWithSettings(pipe, lineStyle, c, layerName);

			bool first = true;// Only first affects view
			foreach (var ent in created)
			{
				CreatedEntities.AddPipeEntity(ent, first);
				first = false;
			}
			
			singleLineDrawer.DrawPipeWithLabelOffset(pipe);
			//singleLineDrawer.DrawPipe(pipe);

		}
	}
}
