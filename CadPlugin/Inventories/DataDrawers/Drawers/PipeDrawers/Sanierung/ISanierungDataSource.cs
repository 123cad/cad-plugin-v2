﻿using CadPlugin.Inventories.Pipes;
using Inventory.CAD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers.Sanierung
{
	interface ISanierungDataSource
	{

		/// <summary>
		/// Sets collection of associations between pipe/shaft and massnahmes that are drawn
		/// (belong to current order).
		/// Not all massnahmes in object belong to single order, that is why we need this.
		/// </summary>
		void SetObjectMassnahmes(IEnumerable<Tuple<SewageObject, ObjectMassnahmes>> objs);

	}
}
