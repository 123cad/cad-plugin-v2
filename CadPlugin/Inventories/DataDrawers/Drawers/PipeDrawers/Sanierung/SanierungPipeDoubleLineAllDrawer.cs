﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.Pipes;
using System.Diagnostics;
using CadPlugin.Inventories.Damages;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.DataDrawers.Drawers.Linetypes;
using CadPlugin.Inventories.DataDrawers.Drawers;
using CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers;
using CadPlugin.Inventories.DataDrawers.Drawers.Layers;
using Inventory.CAD;
using CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers.Sanierung;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Sanierungs;
using CadPlugin.Inventories.DataDrawers.Drawers.Sanierungs;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace   CadPlugin.Inventories.Drawers.PipeDrawers
{
	/// <summary>
	/// Draws pipe as 2 polyline with some width, distant by diameter and filled between.
	/// </summary>
	class SanierungPipeDoubleLineAllDrawer:PipeDoubleLineAllDrawer, ISanierungDataSource
	{
		public override bool DrawPipeLabel => false;

		private bool drawConcept;
		private bool drawProcedures;
		private Dictionary<Pipe, ObjectMassnahmes> objectMassnahmesCollection = new Dictionary<Pipe, ObjectMassnahmes>();
		public SanierungPipeDoubleLineAllDrawer(TransactionData data, SewageObjectColorManager pipeColor, PipeLinetype pipeLinetype, 
											DamageTextDrawer dmgText, TextSizeDefinitions textDef, DamageDrawerFilter damageFilter, 
											InventoryManager.CreatedCADEntities createdEntities, bool drawIn3d,
											bool drawConcept, bool drawProcedures)
							: base(data, pipeColor, pipeLinetype, dmgText, textDef, damageFilter, createdEntities, drawIn3d)
		{
			this.drawConcept = drawConcept;
			this.drawProcedures = drawProcedures;
		}
		public void SetObjectMassnahmes(IEnumerable<Tuple<SewageObject, ObjectMassnahmes>> objs)
		{
			objectMassnahmesCollection = objs.Where(x=>x.Item1 is Pipe).ToDictionary(x => x.Item1 as Pipe, x => x.Item2);
		}
		public override void DrawDamages(Pipe pipe)
		{
			if (!drawProcedures)
				return;

			if (objectMassnahmesCollection == null
				|| !objectMassnahmesCollection.ContainsKey(pipe))
				return;
			ObjectMassnahmes o = objectMassnahmesCollection[pipe];
			foreach (Massnahme m in o.Massnahmes)
			{
				IEnumerable<Entity> entities = ProcedureDrawer.DrawPipeSingleProcedure(Tr, o.Object, pipe, m);
				foreach (var e in entities)
				{
					e.Layer = LayerManager.SanierungProcedureLayerPipe;
					CreatedEntities.AddPipeEntity(e);
				}
			}


			string lineStyle = PipeLinetypeManager.GetLinetype();// GetEntwaesserungsartLinetype(pipe.Abwasser);
			string layerName = LayerManager.SanierungProcedureLayerPipe;
			Color c = ColorManager.ObjectColor.GetObjectColor();
			// For Procedure we draw also line.
			IEnumerable<Entity> created = CreatePipeLineWithSettings(pipe, lineStyle, c, layerName);
			foreach (Entity e in created)
				CreatedEntities.AddPipeEntity(e, true);
		}

		public override void DrawPipe(Pipe pipe)
		{
			if (!drawConcept)
				return;
			base.DrawPipe(pipe);

			// Draw rectangle with ArtMassnahme text code.
			//foreach (var v in DataDrawers.Drawers.Sanierungs.DrawSymbol.DrawPipeSymbol(Tr, pipe, true, IsDrawn3d))
			//{
			//	v.Layer = LayerManager.SanierungConceptLayer;
			//	CreatedEntities.AddPipeEntity(v);
			//}
		}
	}
}
