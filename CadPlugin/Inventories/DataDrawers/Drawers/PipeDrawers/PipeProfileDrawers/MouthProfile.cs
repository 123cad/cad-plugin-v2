﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers.PipeProfileDrawers
{
	/// <summary>
	/// Creates 2d mouth profile (Maul) which is H/B=1.66/2 where B is diameter in horizontal position, and H is vertical height.
	/// </summary>
	class MouthProfile
	{
		public static ComplexSolidProfile CreateProfile(double diameter)
		{
			// Distance from center of the pipe, to the bottom (where water flows).
			double bottomHeightPart = 0;
			var hollowEntity = CreateBaseInner(diameter, ref bottomHeightPart);
			var baseEntity = CreateBaseOuter(diameter, bottomHeightPart);
			ComplexSolidProfile complex = new ComplexSolidProfile(baseEntity);
			complex.InnerEntities.Add(hollowEntity);
			return complex;
		}

		/// <summary>
		/// Returns outer diameter for provided inner pipe diameter.
		/// </summary>
		/// <param name="diameter"></param>
		/// <returns></returns>
		public static double GetOuterDiameter(double diameter)
		{
			// Another way which is faster?
			double bottomHeightPart = 0;
			var hollowEntity = CreateBaseInner(diameter, ref bottomHeightPart);
			var e = CreateBaseOuter(diameter, bottomHeightPart);
			double w = e.GeometricExtents.MaxPoint.X - e.GeometricExtents.MinPoint.X;
			hollowEntity.Dispose();
			e.Dispose();
			return w;
		}

		/*
		 * Model profile: 
		 *	d (diameter) = 5.5486
		 *	h (height) = 3.3587
		 *	P1 (-2.52, 0.56) bulge: 0.5
		 *	P2 (2.52, 0.56) bulge: 0.35
		 *	P3 (-2.5, -0.79) bulge: 0.3
		 *	P4 (2.5, -0.79) bulge: 0.35
		 * 
		 */
		public static Entity CreateBaseInner(double diameter, ref double bottomHeightPart)
		{
			double defaultDiameter = 5.5486;

			double topX = 2.52;
			double topY = 0.56;
			double bottomX = 2.5;
			double bottomY = 0.79;
			//double xTop = diameter * topX;
			//double yTop = diameter * topY;
			//double xBottom = diameter * bottomX;
			//double yBottom = diameter * bottomY;

			double topBulge = 0.5;
			// 0.5 makes arc smaller - also transition (to keep height unchanged, add offset).
			double bottomBulge = 0.3;
			double sideBulge = 0.35;

			Point2d[] pts = new Point2d[4];
			pts[0] = new Point2d(-topX, topY);
			pts[1] = new Point2d(topX, topY);
			pts[2] = new Point2d(bottomX, -bottomY);
			pts[3] = new Point2d(-bottomX, -bottomY);

			Polyline pl = new Polyline();
			pl.Closed = true;
			//t.BaseProfileEntity = pl;
			// Need to add in counterclockwise direction (otherwise it is rotated by 90deg).
			pl.AddVertexAt(0, pts[0], -topBulge, 0, 0);
			pl.AddVertexAt(1, pts[1], -sideBulge, 0, 0);
			pl.AddVertexAt(2, pts[2], -bottomBulge, 0, 0);
			pl.AddVertexAt(3, pts[3], -sideBulge, 0, 0);


			// Pipe center is positioned at the inner bottom of the pipe.
			bottomHeightPart = bottomY + pts[2].GetVectorTo(pts[3]).Length / 2 * bottomBulge;
			double scaleValue = diameter / defaultDiameter;
			var scale = Matrix3d.Scaling(scaleValue, new Point3d());
			pl.TransformBy(scale);

			// Pipe center is positioned at the inner bottom of the pipe.
			Point2d pipeCenter = new Point2d(0, bottomHeightPart * scaleValue);
			pl.TransformBy(Matrix3d.Displacement(pipeCenter.GetAs3d().GetAsVector()));



			return pl;
		}
		public static Entity CreateBaseOuter(double diameter, double bottomHeightPart)
		{
			double defaultDiameter = 5.5486;

			double topX = 3.1228;
			double topY = 0.5;
			double bottomX1 = 3.1228;
			double bottomY1 = 0.787;
			double bottomX2 = 2.5;
			double bottomY2 = 2.16;
			//double xTop = diameter * topX;
			//double yTop = diameter * topY;
			//double x1Bottom = diameter * bottomX1;
			//double y1Bottom = diameter * bottomY1;
			//double x2Bottom = diameter * bottomX2;
			//double y2Bottom = diameter * bottomY2;

			double topBulge = 0.5;
			// 0.5 makes arc smaller - also transition (to keep height unchanged, add offset).
			double sideBulge = 0.35;

			Point2d[] pts = new Point2d[6];
			pts[0] = new Point2d(-topX, topY);
			pts[1] = new Point2d(topX, topY);
			pts[2] = new Point2d(bottomX1, -bottomY1);
			pts[3] = new Point2d(bottomX2, -bottomY2);
			pts[4] = new Point2d(-bottomX2, -bottomY2);
			pts[5] = new Point2d(-bottomX1, -bottomY1);

			Polyline pl = new Polyline();
			pl.Closed = true;
			//t.BaseProfileEntity = pl;
			// Need to add in counterclockwise direction (otherwise it is rotated by 90deg).
			pl.AddVertexAt(0, pts[0], -topBulge, 0, 0);
			pl.AddVertexAt(1, pts[1], -sideBulge, 0, 0);
			pl.AddVertexAt(2, pts[2], 0, 0, 0);
			pl.AddVertexAt(3, pts[3], 0, 0, 0);
			pl.AddVertexAt(4, pts[4], 0, 0, 0);
			pl.AddVertexAt(5, pts[5], -sideBulge, 0, 0);

			double scaleValue = diameter / defaultDiameter;
			var scale = Matrix3d.Scaling(scaleValue, new Point3d());
			pl.TransformBy(scale);

			// Pipe center is positioned at the inner bottom of the pipe.
			Point2d pipeCenter = new Point2d(0, bottomHeightPart * scaleValue);
			pl.TransformBy(Matrix3d.Displacement(pipeCenter.GetAs3d().GetAsVector()));


			return pl;
		}
		/*
		public static Entity CreateBaseInner(double diameter, ref double bottomHeightPart)
		{
			double topXScale = 2.52 / 5.5486;
			double topYScale = 0.56 / 3.3587;
			double bottomXScale = 2.5 / 5.5486;
			double bottomYScale = 0.79 / 3.3587;
			double xTop = diameter * topXScale;
			double yTop = diameter * topYScale;
			double xBottom = diameter * bottomXScale;
			double yBottom = diameter * bottomYScale;

			double topBulge = 0.5;
			// 0.5 makes arc smaller - also transition (to keep height unchanged, add offset).
			double bottomBulge = 0.3;
			double sideBulge = 0.35;

			Point2d[] pts = new Point2d[4];
			pts[0] = new Point2d(-xTop, yTop);
			pts[1] = new Point2d(xTop, yTop);
			pts[2] = new Point2d(xBottom, -yBottom);
			pts[3] = new Point2d(-xBottom, -yBottom);

			Polyline pl = new Polyline();
			pl.Closed = true;
			//t.BaseProfileEntity = pl;
			// Need to add in counterclockwise direction (otherwise it is rotated by 90deg).
			pl.AddVertexAt(0, pts[0], -topBulge, 0, 0);
			pl.AddVertexAt(1, pts[1], -sideBulge, 0, 0);
			pl.AddVertexAt(2, pts[2], -bottomBulge, 0, 0);
			pl.AddVertexAt(3, pts[3], -sideBulge, 0, 0);


			// Pipe center is positioned at the inner bottom of the pipe.
			bottomHeightPart = yBottom + pts[2].GetVectorTo(pts[3]).Length / 2 * bottomBulge;
			Point2d pipeCenter = new Point2d(0, bottomHeightPart);
			pl.TransformBy(Matrix3d.Displacement(pipeCenter.GetAs3d().GetAsVector()));

			return pl;
		}
		public static Entity CreateBaseOuter(double diameter, double bottomHeightPart)
		{
			double defaultDiameter = 5.5486;

			double topXScale = 3.1228 / 5.5486;
			double topYScale = 0.5 / 3.3587;
			double bottomX1Scale = 3.1228 / 5.5486;
			double bottomY1Scale = 0.787 / 3.3587;
			double bottomX2Scale = 2.5 / 5.5486;
			double bottomY2Scale = 2.16 / 3.3587;
			double xTop = diameter * topXScale;
			double yTop = diameter * topYScale;
			double x1Bottom = diameter * bottomX1Scale;
			double y1Bottom = diameter * bottomY1Scale;
			double x2Bottom = diameter * bottomX2Scale;
			double y2Bottom = diameter * bottomY2Scale;

			double topBulge = 0.5;
			// 0.5 makes arc smaller - also transition (to keep height unchanged, add offset).
			double sideBulge = 0.35;

			Point2d[] pts = new Point2d[6];
			pts[0] = new Point2d(-xTop, yTop);
			pts[1] = new Point2d(xTop, yTop);
			pts[2] = new Point2d(x1Bottom, -y1Bottom);
			pts[3] = new Point2d(x2Bottom, -y2Bottom);
			pts[4] = new Point2d(-x2Bottom, -y2Bottom);
			pts[5] = new Point2d(-x1Bottom, -y1Bottom);

			Polyline pl = new Polyline();
			pl.Closed = true;
			//t.BaseProfileEntity = pl;
			// Need to add in counterclockwise direction (otherwise it is rotated by 90deg).
			pl.AddVertexAt(0, pts[0], -topBulge, 0, 0);
			pl.AddVertexAt(1, pts[1], -sideBulge, 0, 0);
			pl.AddVertexAt(2, pts[2], 0, 0, 0);
			pl.AddVertexAt(3, pts[3], 0, 0, 0);
			pl.AddVertexAt(4, pts[4], 0, 0, 0);
			pl.AddVertexAt(5, pts[5], -sideBulge, 0, 0);


			// Pipe center is positioned at the inner bottom of the pipe.
			Point2d pipeCenter = new Point2d(0, bottomHeightPart);
			pl.TransformBy(Matrix3d.Displacement(pipeCenter.GetAs3d().GetAsVector()));

			var scale = Matrix3d.Scaling(defaultDiameter, new Point3d());

			return pl;
		}
		 */

	}
}
