﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.Drawers
{
	class DrawerManager
	{
		public PipeDrawer Pipe { get; set; }
		public ShaftDrawer Shaft { get; set; }
		public DrawerManager(PipeDrawer pipe, ShaftDrawer shaft)
		{
			Pipe = pipe;
			Shaft = shaft;
		}
	}
}
