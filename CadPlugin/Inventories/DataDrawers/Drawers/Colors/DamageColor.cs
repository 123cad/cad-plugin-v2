﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using   CadPlugin.Inventories.Pipes;
using   CadPlugin.Inventories.Colors;

#if BRICSCAD
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Colors;
#endif
namespace   CadPlugin.Inventories.Drawers.Colors
{
	class DamageColor : ColorBase
	{
		public virtual Color GetDamageColor(DamageClass dg)
		{
			return ColorAssociations.GetDamageClassColor(dg);
		}
	}
}
