﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using   CadPlugin.Inventories.Pipes;

#if BRICSCAD
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Colors;
#endif

namespace   CadPlugin.Inventories.Drawers.Colors
{
	abstract class SewageObjectColor : ColorBase
	{
		public abstract Color GetObjectColor();
	}
}
