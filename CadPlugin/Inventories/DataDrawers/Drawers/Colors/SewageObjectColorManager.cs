﻿using   CadPlugin.Inventories.Drawers.Colors;
using   CadPlugin.Inventories.Pipes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace   CadPlugin.Inventories.Drawers.Colors
{
	class SewageObjectColorManager
	{
		protected SewageObject obj { get; private set; }
		public SewageObjectColor ObjectColor { get; private set; }
		public DamageColor DamageColor { get; private set; }

		public SewageObjectColorManager(SewageObjectColor p, DamageColor dmg)
		{
			ObjectColor = p;
			DamageColor = dmg;
		}
		/// <summary>
		/// Initializes this instance with new Pipe object.
		/// </summary>
		/// <param name="pipe"></param>
		public virtual void Initialize(SewageObject obj)
		{
			this.obj = obj;
			ObjectColor.SetSewageObject(obj);
			DamageColor.SetSewageObject(obj);
		}
		


	}
}
