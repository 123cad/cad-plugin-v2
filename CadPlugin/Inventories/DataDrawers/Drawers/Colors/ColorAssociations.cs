﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#if BRICSCAD
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Colors;
#endif




namespace CadPlugin.Inventories.Colors
{
	class ColorAssociations
	{
		public static Color DamageClassNotSet { get; set; }
		public static Color DamageClass0 { get; set; }
		public static Color DamageClass1 { get; set; }
		public static Color DamageClass2 { get; set; }
		public static Color DamageClass3 { get; set; }
		public static Color DamageClass4 { get; set; }
		public static Color DamageClass5 { get; set; }

		public static Color DefaultPipeColor { get; set; }
		public static Color DefaultShaftColor { get; set; }

		/// <summary>
		/// Regenwasser.
		/// </summary>
		public static Color RainWater { get; set; }
		/// <summary>
		/// Mischwasser.
		/// </summary>
		public static Color MixedWater { get; set; }
		/// <summary>
		/// Schmutzwasser.
		/// </summary>
		public static Color SewageWater { get; set; }

		/// <summary>
		/// When owner is inactive (not entwaesserungsart color).
		/// </summary>
		public static Color OwnerInactive{ get; set; }
		static ColorAssociations()
		{
			RainWater = Color.FromRgb(173, 216, 230);
			SewageWater = Color.FromRgb(210, 146, 134);
			MixedWater = Color.FromRgb(241, 154, 242);

			DamageClassNotSet = Color.FromColor(Inventory.CAD.DamageClassColors.DamageClassNotSet);
			DamageClass0 = Color.FromColor(Inventory.CAD.DamageClassColors.DamageClass0);
			DamageClass1 = Color.FromColor(Inventory.CAD.DamageClassColors.DamageClass1);
			DamageClass2 = Color.FromColor(Inventory.CAD.DamageClassColors.DamageClass2);
			DamageClass3 = Color.FromColor(Inventory.CAD.DamageClassColors.DamageClass3);
			DamageClass4 = Color.FromColor(Inventory.CAD.DamageClassColors.DamageClass4);
			DamageClass5 = Color.FromColor(Inventory.CAD.DamageClassColors.DamageClass5);

			OwnerInactive = Color.FromColor(System.Drawing.Color.Gray);

			//NOTE When saved in settings, load colors in here.
		}

		/// <summary>
		/// Returns color for provided damage class.
		/// </summary>
		/// <param name="dmg"></param>
		/// <returns></returns>
		public static Color GetDamageClassColor(DamageClass dmg)
		{
			Color c = DamageClassNotSet;
			switch (dmg)
			{
				case DamageClass.Class0:
					c = DamageClass0;
					break;
				case DamageClass.Class1:
					c = DamageClass1;
					break;
				case DamageClass.Class2:
					c = DamageClass2;
					break;
				case DamageClass.Class3:
					c = DamageClass3;
					break;
				case DamageClass.Class4:
					c = DamageClass4;
					break;
				case DamageClass.Class5:
					c = DamageClass5;
					break;
			}
			return c;
		}
		public static Color GetEntwaesserungColor(string s)
		{
			Color c = Color.FromColorIndex(ColorMethod.ByColor, 7);
			switch (s)
			{
				case "KR":
				case "DR":
				case "GR":
					c = Color.FromRgb(14, 81, 141);//Color.FromRgb(173, 216, 230);
					break;
				case "KS":
				case "DS":
				case "GS":
					c = Color.FromRgb(90, 58, 41);//Color.FromRgb(210, 146, 134);
					break;
				case "KM":
				case "DM":
				case "GM":
					c = Color.FromRgb(255, 0, 255);//Color.FromRgb(241, 154, 242);
					break;
				case "KW":
				case "GW":
					c = Color.FromRgb(14, 81, 141);
					break;
			}
			return c;
		}

	}
}
