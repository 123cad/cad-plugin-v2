﻿using   CadPlugin.Inventories.Pipes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace   CadPlugin.Inventories.Drawers.Colors
{
	abstract class ColorBase
	{
		protected SewageObject obj { get; private set; }
		public void SetSewageObject(SewageObject s)
		{
			obj = s;
			Refresh();
		}
		/// <summary>
		/// Derrived classes can override this method to perform 
		/// initialization when SewageObject is set.
		/// </summary>
		protected virtual void Refresh()
		{

		}
	}
}
