﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using   CadPlugin.Inventories.Pipes;
using   CadPlugin.Inventories.Colors;
using Isybau2015.Isybau2015.ReferenceTableEnums;

#if BRICSCAD
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Colors;
#endif

namespace   CadPlugin.Inventories.Drawers.Colors
{
	/// <summary>
	/// Color of pipe depending on Sanierung ArtMassnahme
	/// </summary>
	class PipeColorSanierungConcept : SewageObjectColor
	{
		public override Color GetObjectColor()
		{
			System.Drawing.Color c = Isybau2015.Isybau2015.Colors.ArtMassnahmeColors.NoMassnahmeColor;
			if (obj.Abwasser.Sanierung != null)
				c = Isybau2015.Isybau2015.Colors.ArtMassnahmeColors.GetArtMassnahmeColor(obj.Abwasser.Sanierung.ArtMassnahme);
			
			return Color.FromColor(c);
		}
	}
}
