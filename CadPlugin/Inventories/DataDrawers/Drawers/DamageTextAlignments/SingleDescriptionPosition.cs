﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.Drawers.DamageDescriptionAlignments
{
	class SingleDescriptionPosition
	{ 
		public virtual Point3d Position
		{
			get;
			set;
		}

		public virtual string Text
		{
			get;
			set;
		}
		public DamageClass DamageClass { get; set; }

		/// <summary>
		/// Distance from PositionTextMerge to middle of text heigth.
		/// </summary>
		public double MiddleTextDistance { get; set; }

		public Color TextColor { get; set; }

        //NOTE Rotation is applied to the description object itself.
        //NOTE It if first positioned, and then it can be rotated around its
        //NOTE center point.

        /// <summary>
        /// Clockwise rotation around center position.
        /// Angle of 0 is parallel to X-axis.
        /// </summary>
        public double RotationRad { get; set; }
        /// <summary>
        /// Clockwise rotation around center position.
        /// Angle of 0 is parallel to X-axis.
        /// </summary>
        public double RotationDeg
        {
            get
            {
                return RotationRad * 180 / Math.PI;
            }
            set
            {
                RotationRad = value * Math.PI / 180;
            }
        }
	}
}

