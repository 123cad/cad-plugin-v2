﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Inventories.Drawers.DamageDescriptionAlignments;
using CadPlugin.Common;
using CadPlugin.Inventories.Colors;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.DataDrawers.Drawers.DamageTextAlignments.StationDamageDrawerManagerTypes
{
	/// <summary>
	/// Draws damage codes as text.
	/// </summary>
	class DamageTextDrawer:StationDamagesDrawer
	{


		/// <summary>
		/// Sets position of all text in station.
		/// Alignment idea:
		///		||	
		///		||__|-DAA
		///		||	|-DBA
		///		||	
		///	Displacement from pipe, line (with displacement) to text
		/// </summary>
		public override StationDamageDescriptionAligned AlignStationTexts(double stationDistance, double radius, PositionVector position, IEnumerable<Inventories.Damages.SingleDamage> damages)// IEnumerable<DamageDrawingProperties> station)
		{
			var drawnDamages = damages.Where(x => x.IsDrawn).ToList();
			//Damages.SingleDamage first = drawnDamages.First();
			//PointPosition position = first.PointPosition;// All text has same position.
			StationDamageDescriptionAligned t = new StationDamageDescriptionAligned(position, stationDistance);
			int numberOfText = drawnDamages.Count;

			// Pipe is considered to be vertical. 
			// All is drawn according to this, and then angle for rotation is calculated.
			// In this way, when complete station text is drawn, all elements are just rotated by the angle.
			Vector3d directionVector = Vector3d.YAxis;

			// Moves from pipe to the right side.
			Vector3d textDirection = Vector3d.XAxis;

			// Calculate heigth of text
			double totalHeigth = (TextHeight + TextPadding) * numberOfText - TextPadding;

			double step = TextHeight + TextPadding;
			double startDistance = totalHeigth / 2;
			double distance = startDistance;
			Point3d textCenter = position.Position.Add(textDirection.FromCADVector().Multiply(TextOffset + radius)).ToCADPoint();
			t.PositionTextMerge = textCenter;
			foreach (Inventories.Damages.SingleDamage dp in drawnDamages)
			{
				SingleDescriptionPosition p = new SingleDescriptionPosition();
				p.Text = dp.GetDisplayText();
				p.DamageClass = dp.DamageClass;
				var temp = textCenter.Add(directionVector * distance).Add(textDirection * TextDistance);
				p.Position = new Point3d(temp.X, temp.Y, position.Position.Z);
				p.MiddleTextDistance = distance - TextHeight / 2;
				t.Texts.Add(p);

				distance -= step;
			}

			Vector3d tV = new Vector3d(position.Direction.X, position.Direction.Y, 0);
			double angleRad = directionVector.GetAngleTo(tV);
			// Yaxis (relative vector to which everything is drawn) is 
			if (position.Direction.X > 0)
				angleRad *= -1;
			// Text must be -Pi/2 to Pi/2 respectively to the user horizont.
			if (position.Direction.Y < 0)
				angleRad += Math.PI;
			t.AngleDeg = angleRad * 180 / Math.PI;
			// Calculate positions of all text (include distance from pipe, and align them all to center)
			// Include that text must always be aligned (-Pi/2 : Pi/2 - aligned to the user)
			// Set angle to which this whole construction (with respect to center point) should be rotated.


			return t;
		}

		/// <summary>
		/// Draws text and applies rotation
		/// </summary>
		/// <param name="station"></param>
		public override IEnumerable<Entity> DrawText(TransactionData data, StationDamageDescriptionAligned station, bool draw3d)
		{
			List<Entity> ents = new List<Entity>();
			if (station.Texts.Count == 0)
				return ents;
			AdjustHeight adjustZ = new AdjustHeight(draw3d);
			// All created entities.
			List<Entity> entities = new List<Entity>();

			// All text is on level 0.
			Point3d stationPosition = station.Position.Position.ToCADPoint();// new Point3d(station.Position.Position.X, station.Position.Position.Y, 0);
			Point3d textMerge = station.PositionTextMerge;// new Point3d(station.PositionTextMerge.X, station.PositionTextMerge.Y, 0);

			Line l = new Line(adjustZ.GetPoint(stationPosition), adjustZ.GetPoint(textMerge));
			data.Btr.AppendEntity(l);
			data.Tr.AddNewlyCreatedDBObject(l, true);
			entities.Add(l);

			DamageClass maxDamageClass = station.Texts.Max(d => d.DamageClass);
			foreach (SingleDescriptionPosition text in station.Texts)
			{
				// Polyline which goes from Merge point of text (vertical), to middle of the text itself (horizontal, to the text).
				Polyline pl = data.CreateEntity<Polyline>();// new Polyline3d();
															//data.Btr.AppendEntity(pl);
															//data.Tr.AddNewlyCreatedDBObject(pl, true);
				entities.Add(pl);
				pl.AddPoints(textMerge.GetAs2d());
				//pl.AppendPoint(adjustHeight(textMerge));
				// Middle point of text height (by y coordinate)
				Point3d pt = textMerge.Add((new Vector3d(0, 1, 0) * text.MiddleTextDistance));
				// Elevate to good height.
				//pt = pt.Add(new Vector3d(0, 0, station.PositionTextMerge.Z));
				//pl.AppendPoint(adjustHeight(pt));
				pl.AddPoints(pt.GetAs2d());
				// Move polyline to the text.
				pt = pt.Add(new Vector3d(1, 0, 0) * TextPadding);
				// Elevate to good height.
				//pt = pt.Add(new Vector3d(0, 0, station.PositionTextMerge.Z));
				//pl.AppendPoint(adjustHeight(pt));
				pl.AddPoints(pt.GetAs2d());
				pl.Elevation = adjustZ.GetPoint(textMerge).Z;

				MText mtxt = new MText();
				data.Btr.AppendEntity(mtxt);
				data.Tr.AddNewlyCreatedDBObject(mtxt, true);
				entities.Add(mtxt);
				Color textColor = text.TextColor;
				mtxt.Color = textColor;
				mtxt.Contents = text.Text;
				mtxt.TextHeight = TextHeight;
				mtxt.Location = adjustZ.GetPoint(text.Position);// new Point3d(text.Position.X, text.Position.Y, 0);
			}

			DrawDamageStationAndClass(data, station, adjustZ, entities, stationPosition, maxDamageClass);

			Matrix3d rotate = Matrix3d.Rotation(station.AngleDeg * Math.PI / 180, Vector3d.ZAxis, station.Position.Position.ToCADPoint());
			foreach (Entity e in entities)
			{
				/*string layer = "";
				if (SewageObject is Pipe)
					layer = Pipe.TextDamageLayer;
				else
					layer = Shaft.TextLayer;
				CADLayerHelper.AddLayerIfDoesntExist(SewageObject.TransactionData.Db, layer);
				e.Layer = layer;*/
				e.TransformBy(rotate);
			}

			foreach (var e in entities)
				ents.Add(e);
			return ents;
		}

		
	}
}
