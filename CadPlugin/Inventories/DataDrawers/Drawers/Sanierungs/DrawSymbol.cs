﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.DataDrawers.Drawers.Sanierungs
{
	class DrawSymbol
	{
		private static double TextHeight = 0.4;

		/// <summary>
		/// Create text and sets center of it at (0, 0, 0)
		/// </summary>
		private static MText CreateTextOnCenter(TransactionData tr, string content, double textHeight)
		{
			MText t = tr.CreateEntity<MText>();
			t.Contents = content;
			t.TextHeight = textHeight;
			t.Location = new Point3d(0, 0, 0);

			var rec = t.GeometricExtents.ToRectangle();
			t.TransformBy(Matrix3d.Displacement(new Vector3d(-rec.Width / 2, rec.Height / 2, 0)));
			return t;
		}
		/// <summary>
		/// Creates square with center at (0,0,0)
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="side"></param>
		/// <returns></returns>
		private static Polyline CreateBoundingSquareOnCenter(TransactionData tr, double side)
		{
			double halfSide = side / 2;
			MyUtilities.Geometry.Point3d[] points = new MyUtilities.Geometry.Point3d[4];
			points[0] = new MyUtilities.Geometry.Point3d(-halfSide, halfSide, 0);
			points[1] = new MyUtilities.Geometry.Point3d(-halfSide, -halfSide, 0);
			points[2] = new MyUtilities.Geometry.Point3d(halfSide, -halfSide, 0);
			points[3] = new MyUtilities.Geometry.Point3d(halfSide, halfSide, 0);
			Polyline pl = tr.CreateEntity<Polyline>();
			pl.AddPoints(points.Select(x => x.GetAs2d().ToCADPoint()).ToArray());
			pl.Closed = true;

			return pl;
		}
		private static Circle CreateBoundingCircleOnCenter(TransactionData tr, double diameter)
		{
			Circle circle = tr.CreateEntity<Circle>();
			circle.Diameter = diameter;
			circle.Center = new Point3d();
			return circle;
		}
		private static void MoveSymbolFromCenter(IEnumerable<Entity> entities, double diameter, double symbolWidth, Vector3d directionVector)
		{
			var perpendicularUnit = directionVector.GetWithZ().CrossProduct(Vector3d.ZAxis).FromCADVector().GetUnitVector();
			double diagonal = symbolWidth * Math.Sqrt(2);
			double padding = diagonal * 0.2; // From pipe to symbol.
			double totalOffset = diameter / 2 + padding + diagonal / 2;

			Matrix3d move = Matrix3d.Displacement(perpendicularUnit.Multiply(totalOffset).ToCADVector());
			foreach (var e in entities)
				e.TransformBy(move);
		}
		/// <summary>
		/// Draws Concept symbol (rectangle) for pipe.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="pipe"></param>
		/// <param name="zHeight"></param>
		/// <returns></returns>
		public static IEnumerable<Entity> DrawPipeSymbol(TransactionData tr, Pipes.Pipe pipe, bool useRadiusOffset, bool is3d = false)
		{
			char code;
			Color c = Color.FromColor(Isybau2015.Isybau2015.Colors.ArtMassnahmeColors.GetArtMassnahmeColor(pipe.Abwasser.Sanierung?.ArtMassnahme ?? 7, out code));

			// Text is surrounded by imagined square (all sides are same and depend on text height).
			double boundingBoxSide = TextHeight + 2 * (TextHeight * 0.2);
			// Draw text
			MText t = CreateTextOnCenter(tr, code.ToString(), TextHeight);
			t.Color = c;
			
			Polyline pl = CreateBoundingSquareOnCenter(tr, boundingBoxSide);
			pl.Color = c;
			//pl.Elevation = zHeight;
			List<Entity> entities = new List<Entity>() { t, pl };

			var pipeVector = pipe.StartPoint.GetVectorTo(pipe.EndPoint);
			var centerOffsetVector = pipeVector.Multiply(0.5);
			var moveVector = new MyUtilities.Geometry.Point3d().Add(pipe.StartPoint.GetAsVector().GetWithZ().Add(centerOffsetVector.GetWithZ())).GetAsVector();
			Matrix3d move = Matrix3d.Displacement(moveVector.ToCADVector());
			if (is3d)
			{
				double z = pipe.StartPoint.Z + centerOffsetVector.Z;
				//TODO Test here - maybe need to inverse.
				move = move.PreMultiplyBy(Matrix3d.Displacement(new Vector3d(0, 0, z)));
				//entities.ForEach(x => x.TransformBy(move));
			}
			entities.ForEach(x => x.TransformBy(move));

			MoveSymbolFromCenter(entities, useRadiusOffset ? pipe.Diameter : 0, boundingBoxSide, pipeVector.ToCADVector());
			
						
			return entities;
		}
		public static IEnumerable<Entity> DrawShaftSymbol(TransactionData tr, Shafts.Shaft shaft, bool is3d = false)
		{
			char code;
			Color c = Color.FromColor(Isybau2015.Isybau2015.Colors.ArtMassnahmeColors.GetArtMassnahmeColor(shaft.Abwasser.Sanierung?.ArtMassnahme ?? 7, out code));

			// Text is surrounded by imagined square (all sides are same and depend on text height).
			double boundingBoxSide = TextHeight + 2 * (TextHeight * 0.3);
			// Draw text
			MText t = CreateTextOnCenter(tr, code.ToString(), TextHeight);
			t.Color = c;

			Circle circle = CreateBoundingCircleOnCenter(tr, boundingBoxSide);
			circle.Color = c;
			//pl.Elevation = zHeight;


			Circle symbolCircle = tr.CreateEntity<Circle>();
			symbolCircle.Diameter = shaft.Radius * 2 * 1.00;// Make it little bit bigger than diameter.
			symbolCircle.Color = c;

			List<Entity> entities = new List<Entity>() { t, circle };


			var moveVector = shaft.Center.GetAs2d().GetWithHeight(0).GetAsVector();
			Matrix3d move = Matrix3d.Displacement(moveVector.ToCADVector());
			double coverHeight = 0;
			if (is3d)
			{
				double z = shaft.Center.Z;
				coverHeight = shaft.CoverPoint.Z;
				//TODO Test here - maybe need to inverse.
				move = move.PreMultiplyBy(Matrix3d.Displacement(new Vector3d(0, 0, z)));
				//entities.ForEach(x => x.TransformBy(move));
			}
			entities.ForEach(x => x.TransformBy(move));
			symbolCircle.Center = new Point3d(shaft.Center.X, shaft.Center.Y, coverHeight);

			MoveSymbolFromCenter(entities, shaft.Radius * 2, boundingBoxSide, new Vector3d(1, 1, 0));


			return new List<Entity> { t, circle, symbolCircle};
		}
	}
}
