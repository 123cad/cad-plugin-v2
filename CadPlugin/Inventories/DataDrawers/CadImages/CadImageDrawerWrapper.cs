﻿using CadPlugin.Common;
using CadPlugin.Inventories.DataDrawers.Drawers.DamageTextAlignments.StationDamageDrawerManagerTypes;
using CadPlugin.Inventories.Drawers.DamageDescriptionAlignments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.DataDrawers
{
    class DamageKodeImageAssociator
    {
        private static Dictionary<string, string> kodeFileName = new Dictionary<string, string>();
		static DamageKodeImageAssociator()
		{
			kodeFileName["default_pipe"] = null;
			kodeFileName["default_shaft"] = null;
			//kodeFileName.Add("BBA", "damage_roots.png");
			//kodeFileName.Add("BAJ", "damage_move.png");
			//kodeFileName.Add("BAB", "damage_crack.png");
			//kodeFileName.Add("BAG", "damage_add_pipe.png");
			//kodeFileName.Add("BBC", "damage_sediment.png");
			var dic = CadImageAssociationsLoader.Load();
			foreach (var d in dic)
				kodeFileName[d.Key] = d.Value;

			if (kodeFileName["default_pipe"] == null)
				throw new FileNotFoundException("Default icons for pipe damage has not been found! Check definition.txt.");
			if (kodeFileName["default_shaft"] == null)
				throw new FileNotFoundException("Default icons for shaft damage has not been found! Check definition.txt.");

		}
        public static IEnumerable<string> GetAllImageFileNames()
        {
            //yield return DefaultPipeImageFileName;
            foreach (var v in kodeFileName.Values)
                yield return v;
        }
        public static string GetImageName(string kode)
        {
            if (kodeFileName.ContainsKey(kode))
                return kodeFileName[kode];
			if (kode[0] == 'B')
				return kodeFileName["default_pipe"];
			else if (kode[0] == 'D')
				return kodeFileName["default_shaft"];
			return null;
			//throw new NotSupportedException($"Kode {kode} does not support damage icons!");
        }
    }


    class CadImageDrawerWrapper
    {
        private CADImageManager cadDrawer;
        private Dictionary<string, CADImageManager.ImageInfo> imageIds = new Dictionary<string, CADImageManager.ImageInfo>();
        public CadImageDrawerWrapper(CADImageManager drawer)
        {
            cadDrawer = drawer;
			string dir = GlobalConfig.DirectoryManager.Instance.InstallationSewageImagesDir;//  GetDamageImageDirectory();
            foreach(string s in DamageKodeImageAssociator.GetAllImageFileNames())
            {
                string fullName = dir + s;
                var info = drawer.LoadImage(fullName);
				if (!imageIds.ContainsKey(s))
					imageIds.Add(s, info);
            }
        }

		/*private string GetDamageImageDirectory()
		{
			using (Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\123cad", true))
			{
				string location = key.GetValue("LocationDir") as string;
				string support = location + "Support\\DamageImages\\";
				return support;
			}
		}*/

		public void DrawDamages(TransactionData data, List<Entity> createdEntities, StationDamageDescriptionAligned station, double imageWidth, double imageHeight)
        {

			// Add multiple codes to draw.
			// Add size update.

			foreach (SingleDescriptionPosition t in station.Texts)
			{
				string kode = t.Text.Substring(0, 3);
				string s = DamageKodeImageAssociator.GetImageName(kode);
				if (s != null)
				{
					var info = imageIds[s];
					info.Properties.BottomLeftPosition = t.Position.FromCADPoint();
					info.Properties.Width = imageWidth;
					info.Properties.Height = imageHeight;
					info.Properties.ImageRotationRad = t.RotationRad;
					info.Properties.DirectionVector = new MyUtilities.Geometry.Vector2d(1, 0).ToCADVector().RotateBy(station.AngleDeg * Math.PI / 180).FromCADVector().GetAs3d();// //station.TopLeft.GetVectorTo(station.BottomLeft).RotateBy(Math.PI / 180 * 90.0).FromCADVector().GetAs3d();

					cadDrawer.DrawImage(createdEntities, info);
				}

				//NOTE If needed to remove image border, command is: IMAGEFRAME<0> (0-no frame, 1-frame)

				// Draw rectangle around image.
				Polyline pl = data.CreateEntity<Polyline>();
				pl.Closed = true;
				createdEntities.Add(pl);
				// Set color
				pl.Color = Colors.ColorAssociations.GetDamageClassColor(t.DamageClass);
				// Set width
				double plWidth = 0.02;
				double plCornerBulge = 0.5;
				double cornerOffset = imageWidth * 0.05;
				var points = new List<MyUtilities.Geometry.Point2d>();
				pl.Elevation = t.Position.Z;
				var imageXVector = new MyUtilities.Geometry.Vector2d(1, 0);// info.Properties.DirectionVector.GetAs2d().GetUnitVector();
				var imageYVector = imageXVector.ToCADVector().RotateBy(Math.PI / 2).FromCADVector().GetUnitVector();
				var cornerX = imageXVector.Multiply(cornerOffset);
				var cornerY = imageYVector.Multiply(cornerOffset);
				MyUtilities.Geometry.Point2d last = new MyUtilities.Geometry.Point2d();


				/*
				 	 5____4
					/	   \
					6		3
					|		|
					|		|
					7		2
					\0_____1/
				*/
				last = cornerX.GetAsPoint();
				points.Add(last);
				last = last.Add(imageXVector.Multiply(imageWidth - 2 * cornerOffset));
				points.Add(last);
				last = last.Add(cornerX).Add(cornerY);
				points.Add(last);
				last = last.Add(imageYVector.Multiply(imageHeight - 2 * cornerOffset));
				points.Add(last);
				last = last.Add(cornerY).Add(cornerX.Negate());
				points.Add(last);
				last = last.Add(imageXVector.Multiply(-(imageWidth - 2 * cornerOffset)));
				points.Add(last);
				last = last.Add(cornerX.Negate()).Add(cornerY.Negate());
				points.Add(last);
				last = last.Add(imageYVector.Multiply(-(imageHeight - 2 * cornerOffset)));
				points.Add(last);



				/*points.Add(imageYVector.Multiply(-cornerOffset).GetAsPoint());
				points.Add(imageYVector.Multiply(-(imageHeight - cornerOffset)).GetAsPoint());
				points.Add(imageYVector.Multiply(-imageHeight).Add(imageXVector.Multiply(cornerOffset)).GetAsPoint());
				points.Add(imageYVector.Multiply(-imageHeight).Add(imageXVector.Multiply(imageWidth - cornerOffset)).GetAsPoint());
				points.Add(imageYVector.Multiply(-(imageHeight - cornerOffset)).Add(imageXVector.Multiply(imageWidth)).GetAsPoint());
				points.Add(imageYVector.Multiply(-(cornerOffset)).Add(imageXVector.Multiply(imageWidth)).GetAsPoint());
				points.Add(imageYVector.Multiply(-(0)).Add(imageXVector.Multiply(imageWidth - cornerOffset)).GetAsPoint());
				points.Add(imageYVector.Multiply(-(0)).Add(imageXVector.Multiply(cornerOffset)).GetAsPoint());*/

				var startPosition = t.Position.FromCADPoint().GetAs2d().GetAsVector();
				for (int i = 0; i < points.Count; i++)
					points[i] = points[i].Add(startPosition);

				int index = 0;
				pl.AddVertexAt(index, points[index].ToCADPoint(), 0, plWidth, plWidth);
				index++;
				pl.AddVertexAt(index, points[index].ToCADPoint(), plCornerBulge, plWidth, plWidth);
				index++;
				pl.AddVertexAt(index, points[index].ToCADPoint(), 0, plWidth, plWidth);
				index++;
				pl.AddVertexAt(index, points[index].ToCADPoint(), plCornerBulge, plWidth, plWidth);
				index++;
				pl.AddVertexAt(index, points[index].ToCADPoint(), 0, plWidth, plWidth);
				index++;
				pl.AddVertexAt(index, points[index].ToCADPoint(), plCornerBulge, plWidth, plWidth);
				index++;
				pl.AddVertexAt(index, points[index].ToCADPoint(), 0, plWidth, plWidth);
				index++;
				pl.AddVertexAt(index, points[index].ToCADPoint(), plCornerBulge, plWidth, plWidth);
			}

		}

    }
}
