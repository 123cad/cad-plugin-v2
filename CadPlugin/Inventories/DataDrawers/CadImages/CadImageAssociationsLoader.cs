﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers
{
	/* Definition file contains kode/icon association.
	 * # at the beggining is a comment line.
	 * BAB;filename (local)
	 * 
	 */ 
	/// <summary>
	/// Loads associations from the file, if found.
	/// </summary>
	class CadImageAssociationsLoader
	{
		public static Dictionary<string, string> Load()
		{
			var dic = new Dictionary<string, string>();
			string pipesDir = "PipeDamages\\";
			string shaftsDir = "ShaftDamages\\";
			string definitionFile = "definition.txt";
			string pipeFolder = GlobalConfig.DirectoryManager.Instance.InstallationSewageImagesDir + pipesDir;
			string shaftFolder = GlobalConfig.DirectoryManager.Instance.InstallationSewageImagesDir + shaftsDir;
			string pipeDefinitionFile = pipeFolder + definitionFile;
			string shaftDefinitionFile = shaftFolder + definitionFile;
			bool allSpecifiedIconsExist = true;
			if (!Directory.Exists(pipeFolder) || !Directory.Exists(shaftFolder))
				allSpecifiedIconsExist = false;
			if (File.Exists(pipeDefinitionFile))
			{
				using (FileStream fs = new FileStream(pipeDefinitionFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
				{
					StreamReader sr = new StreamReader(fs);
					while (!sr.EndOfStream)
					{
						string line = sr.ReadLine();
						if (line.Length < 5 || line[0] == '#')
							continue;
						string[] s = line.Split(';');
						if (s.Length < 2)
							continue;
						string kode = s[0];
						string file = s[1];
						if (!File.Exists(pipeFolder + file))
						{
							allSpecifiedIconsExist = false;
							MyLog.MyTraceSource.Warning($"Icon \"{ file } \" for pipe code {kode} not found!");
						}
						else
						{
							dic[kode] = pipesDir + file;
						}
					}
				}
			}
			else
			{
				MyLog.MyTraceSource.Warning($"File \"{pipeDefinitionFile}\" has not been found!");
				allSpecifiedIconsExist = false;
			}
			if (File.Exists(shaftDefinitionFile))
			{
				using (FileStream fs = new FileStream(shaftDefinitionFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
				{
					StreamReader sr = new StreamReader(fs);
					while (!sr.EndOfStream)
					{
						string line = sr.ReadLine();
						if (line.Length < 5 || line[0] == '#')
							continue;
						string[] s = line.Split(';');
						if (s.Length < 2)
							continue;
						string kode = s[0];
						string file = s[1];
						if (!File.Exists(shaftFolder + file))
						{
							allSpecifiedIconsExist = false;
							MyLog.MyTraceSource.Warning($"Icon \"{ file } \" for pipe code {kode} not found!");
						}
						else
						{
							dic[kode] = shaftsDir + file;
						}
					}
				}
			}
			else
			{
				MyLog.MyTraceSource.Warning($"File \"{shaftDefinitionFile}\" has not been found!");
				allSpecifiedIconsExist = false;
			}
			if (!allSpecifiedIconsExist)
				System.Windows.Forms.MessageBox.Show("Some problems appeared with loading Sewage Damage icons. Check log file for details.", "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);


			return dic;
		}
	}
}
