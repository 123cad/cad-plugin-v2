﻿using CadPlugin.Common;
using CadPlugin.Inventories.DataDrawers;
using Inventory.CAD;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using Isybau2015.Identifikations.Datenkollektives.Zustandsdatenkollektivs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CadPlugin.Inventories
{
	public class SewageObject
	{
		/// <summary>
		/// Parent collection of this object (collection which owns it).
		/// </summary>
		public SewageObjectsCollection OwningCollection { get; private set; }
		public virtual AbwassertechnischeAnlagen Abwasser
		{
			get;
			private set;
		}

		public virtual InspizierteAbwassertechnischeAnlagen Inspizierte
		{
			get;
			private set;
		}
		public virtual InspizierteAbwassertechnischeAnlagen InspizierteUpstream
		{
			get;
			private set;
		}


		protected SewageObject(SewageObjectsCollection owner, AbwassertechnischeAnlagen abwasser, InspizierteAbwassertechnischeAnlagen insp):this(owner, abwasser, insp, null)
		{
		}
		protected SewageObject(SewageObjectsCollection owner, AbwassertechnischeAnlagen abwasser, InspizierteAbwassertechnischeAnlagen insp, InspizierteAbwassertechnischeAnlagen inspUp)
		{
			OwningCollection = owner;
			Abwasser = abwasser;
			Inspizierte = insp;
			InspizierteUpstream = inspUp;
			owner.AddObject(this);
		}

		/// <summary>
		/// Creates Pipe or Shaft, depending on ObjektArt.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static SewageObject CreateByType(SewageObjectsCollection owner, SingleObject obj)
		{
			if (obj.Abwasser == null ||
				!obj.Abwasser.ObjektartIsSet)
				return null;
			SewageObject o = null;
			if (obj.Abwasser.Objektart == 1)
				o = new Pipes.Pipe(owner, obj.Abwasser, obj.Inspizierte, obj.PipeInspizierteUpstream);
			else
				o = new Shafts.Shaft(owner, obj.Abwasser, obj.Inspizierte, obj.PipeInspizierteUpstream);
			return o;
		}

	}
}

