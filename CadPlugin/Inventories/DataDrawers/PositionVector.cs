﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories
{
	public struct PositionVector
	{
		public Point3d Position { get; set; }
		public Vector3d Direction { get; set; }
	}
}
