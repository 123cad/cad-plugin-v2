﻿namespace CadPlugin.Inventories.View.SelectDrawingOptionsTypes
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.radioButton2d = new System.Windows.Forms.RadioButton();
			this.radioButton3d = new System.Windows.Forms.RadioButton();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.radioButtonVisual3d = new System.Windows.Forms.RadioButton();
			this.buttonOk = new System.Windows.Forms.Button();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.label1 = new System.Windows.Forms.Label();
			this.comboBoxPipeLinestyle = new System.Windows.Forms.ComboBox();
			this.panelMainData = new System.Windows.Forms.Panel();
			this.panelStatusData = new System.Windows.Forms.Panel();
			this.checkBoxDrawSingleLine = new System.Windows.Forms.CheckBox();
			this.checkBoxContinuousOnly = new System.Windows.Forms.CheckBox();
			this.label2 = new System.Windows.Forms.Label();
			this.comboBoxTextScaling = new System.Windows.Forms.ComboBox();
			this.groupBoxSanierung = new System.Windows.Forms.GroupBox();
			this.checkBoxDrawSanierungProcedures = new System.Windows.Forms.CheckBox();
			this.checkBoxDrawSanierungConcept = new System.Windows.Forms.CheckBox();
			this.checkBoxDrawSanierung = new System.Windows.Forms.CheckBox();
			this.groupBox1.SuspendLayout();
			this.groupBoxSanierung.SuspendLayout();
			this.SuspendLayout();
			// 
			// radioButton2d
			// 
			this.radioButton2d.AutoSize = true;
			this.radioButton2d.Checked = true;
			this.radioButton2d.Location = new System.Drawing.Point(15, 17);
			this.radioButton2d.Name = "radioButton2d";
			this.radioButton2d.Size = new System.Drawing.Size(37, 17);
			this.radioButton2d.TabIndex = 4;
			this.radioButton2d.TabStop = true;
			this.radioButton2d.Text = "2d";
			this.toolTip1.SetToolTip(this.radioButton2d, "Height (z) is 0");
			this.radioButton2d.UseVisualStyleBackColor = true;
			// 
			// radioButton3d
			// 
			this.radioButton3d.AutoSize = true;
			this.radioButton3d.Location = new System.Drawing.Point(15, 41);
			this.radioButton3d.Name = "radioButton3d";
			this.radioButton3d.Size = new System.Drawing.Size(37, 17);
			this.radioButton3d.TabIndex = 5;
			this.radioButton3d.Text = "3d";
			this.radioButton3d.UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.radioButtonVisual3d);
			this.groupBox1.Controls.Add(this.radioButton2d);
			this.groupBox1.Controls.Add(this.radioButton3d);
			this.groupBox1.Location = new System.Drawing.Point(25, 23);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(99, 95);
			this.groupBox1.TabIndex = 8;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Draw style";
			// 
			// radioButtonVisual3d
			// 
			this.radioButtonVisual3d.AutoSize = true;
			this.radioButtonVisual3d.Location = new System.Drawing.Point(15, 65);
			this.radioButtonVisual3d.Name = "radioButtonVisual3d";
			this.radioButtonVisual3d.Size = new System.Drawing.Size(68, 17);
			this.radioButtonVisual3d.TabIndex = 6;
			this.radioButtonVisual3d.Text = "Visual 3d";
			this.toolTip1.SetToolTip(this.radioButtonVisual3d, "Pipes and shafts are drawn with solid entities.");
			this.radioButtonVisual3d.UseVisualStyleBackColor = true;
			// 
			// buttonOk
			// 
			this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonOk.Location = new System.Drawing.Point(478, 348);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(75, 23);
			this.buttonOk.TabIndex = 9;
			this.buttonOk.Text = "Ok";
			this.buttonOk.UseVisualStyleBackColor = true;
			this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(144, 44);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 13);
			this.label1.TabIndex = 16;
			this.label1.Text = "Additional pipe lines";
			// 
			// comboBoxPipeLinestyle
			// 
			this.comboBoxPipeLinestyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxPipeLinestyle.FormattingEnabled = true;
			this.comboBoxPipeLinestyle.Location = new System.Drawing.Point(250, 40);
			this.comboBoxPipeLinestyle.Name = "comboBoxPipeLinestyle";
			this.comboBoxPipeLinestyle.Size = new System.Drawing.Size(121, 21);
			this.comboBoxPipeLinestyle.TabIndex = 17;
			// 
			// panelMainData
			// 
			this.panelMainData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.panelMainData.Location = new System.Drawing.Point(25, 202);
			this.panelMainData.Name = "panelMainData";
			this.panelMainData.Size = new System.Drawing.Size(252, 128);
			this.panelMainData.TabIndex = 18;
			// 
			// panelStatusData
			// 
			this.panelStatusData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.panelStatusData.Location = new System.Drawing.Point(283, 202);
			this.panelStatusData.Name = "panelStatusData";
			this.panelStatusData.Size = new System.Drawing.Size(269, 128);
			this.panelStatusData.TabIndex = 19;
			// 
			// checkBoxDrawSingleLine
			// 
			this.checkBoxDrawSingleLine.AutoSize = true;
			this.checkBoxDrawSingleLine.Location = new System.Drawing.Point(250, 17);
			this.checkBoxDrawSingleLine.Name = "checkBoxDrawSingleLine";
			this.checkBoxDrawSingleLine.Size = new System.Drawing.Size(123, 17);
			this.checkBoxDrawSingleLine.TabIndex = 20;
			this.checkBoxDrawSingleLine.Text = "Draw single pipe line";
			this.checkBoxDrawSingleLine.UseVisualStyleBackColor = true;
			// 
			// checkBoxContinuousOnly
			// 
			this.checkBoxContinuousOnly.AutoSize = true;
			this.checkBoxContinuousOnly.Location = new System.Drawing.Point(147, 75);
			this.checkBoxContinuousOnly.Name = "checkBoxContinuousOnly";
			this.checkBoxContinuousOnly.Size = new System.Drawing.Size(140, 17);
			this.checkBoxContinuousOnly.TabIndex = 21;
			this.checkBoxContinuousOnly.Text = "Continuous linetype only";
			this.checkBoxContinuousOnly.UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(453, 18);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 13);
			this.label2.TabIndex = 22;
			this.label2.Text = "Text scaling";
			// 
			// comboBoxTextScaling
			// 
			this.comboBoxTextScaling.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxTextScaling.FormattingEnabled = true;
			this.comboBoxTextScaling.Location = new System.Drawing.Point(456, 41);
			this.comboBoxTextScaling.Name = "comboBoxTextScaling";
			this.comboBoxTextScaling.Size = new System.Drawing.Size(96, 21);
			this.comboBoxTextScaling.TabIndex = 23;
			// 
			// groupBoxSanierung
			// 
			this.groupBoxSanierung.Controls.Add(this.checkBoxDrawSanierungProcedures);
			this.groupBoxSanierung.Controls.Add(this.checkBoxDrawSanierungConcept);
			this.groupBoxSanierung.Controls.Add(this.checkBoxDrawSanierung);
			this.groupBoxSanierung.Location = new System.Drawing.Point(25, 129);
			this.groupBoxSanierung.Name = "groupBoxSanierung";
			this.groupBoxSanierung.Size = new System.Drawing.Size(399, 53);
			this.groupBoxSanierung.TabIndex = 24;
			this.groupBoxSanierung.TabStop = false;
			this.groupBoxSanierung.Text = "Sanierung";
			// 
			// checkBoxDrawSanierungProcedures
			// 
			this.checkBoxDrawSanierungProcedures.AutoSize = true;
			this.checkBoxDrawSanierungProcedures.Checked = true;
			this.checkBoxDrawSanierungProcedures.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxDrawSanierungProcedures.Enabled = false;
			this.checkBoxDrawSanierungProcedures.Location = new System.Drawing.Point(277, 19);
			this.checkBoxDrawSanierungProcedures.Name = "checkBoxDrawSanierungProcedures";
			this.checkBoxDrawSanierungProcedures.Size = new System.Drawing.Size(108, 17);
			this.checkBoxDrawSanierungProcedures.TabIndex = 2;
			this.checkBoxDrawSanierungProcedures.Text = "Draw Procedures";
			this.checkBoxDrawSanierungProcedures.UseVisualStyleBackColor = true;
			// 
			// checkBoxDrawSanierungConcept
			// 
			this.checkBoxDrawSanierungConcept.AutoSize = true;
			this.checkBoxDrawSanierungConcept.Checked = true;
			this.checkBoxDrawSanierungConcept.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxDrawSanierungConcept.Enabled = false;
			this.checkBoxDrawSanierungConcept.Location = new System.Drawing.Point(148, 19);
			this.checkBoxDrawSanierungConcept.Name = "checkBoxDrawSanierungConcept";
			this.checkBoxDrawSanierungConcept.Size = new System.Drawing.Size(94, 17);
			this.checkBoxDrawSanierungConcept.TabIndex = 1;
			this.checkBoxDrawSanierungConcept.Text = "Draw Concept";
			this.checkBoxDrawSanierungConcept.UseVisualStyleBackColor = true;
			// 
			// checkBoxDrawSanierung
			// 
			this.checkBoxDrawSanierung.AutoSize = true;
			this.checkBoxDrawSanierung.Location = new System.Drawing.Point(15, 19);
			this.checkBoxDrawSanierung.Name = "checkBoxDrawSanierung";
			this.checkBoxDrawSanierung.Size = new System.Drawing.Size(100, 17);
			this.checkBoxDrawSanierung.TabIndex = 0;
			this.checkBoxDrawSanierung.Text = "Draw sanierung";
			this.checkBoxDrawSanierung.UseVisualStyleBackColor = true;
			this.checkBoxDrawSanierung.CheckedChanged += new System.EventHandler(this.checkBoxDrawSanierung_CheckedChanged);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(565, 383);
			this.Controls.Add(this.groupBoxSanierung);
			this.Controls.Add(this.comboBoxTextScaling);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.checkBoxContinuousOnly);
			this.Controls.Add(this.checkBoxDrawSingleLine);
			this.Controls.Add(this.panelStatusData);
			this.Controls.Add(this.panelMainData);
			this.Controls.Add(this.comboBoxPipeLinestyle);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.buttonOk);
			this.Controls.Add(this.groupBox1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(16, 315);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Drawing preferences";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBoxSanierung.ResumeLayout(false);
			this.groupBoxSanierung.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.RadioButton radioButton2d;
		private System.Windows.Forms.RadioButton radioButton3d;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.RadioButton radioButtonVisual3d;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox comboBoxPipeLinestyle;
		private System.Windows.Forms.Panel panelMainData;
		private System.Windows.Forms.Panel panelStatusData;
		private System.Windows.Forms.CheckBox checkBoxDrawSingleLine;
		private System.Windows.Forms.CheckBox checkBoxContinuousOnly;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox comboBoxTextScaling;
		private System.Windows.Forms.GroupBox groupBoxSanierung;
		private System.Windows.Forms.CheckBox checkBoxDrawSanierungProcedures;
		private System.Windows.Forms.CheckBox checkBoxDrawSanierungConcept;
		private System.Windows.Forms.CheckBox checkBoxDrawSanierung;
	}
}