﻿using CadPlugin.Inventories.DataDrawers.View.SelectDrawingOption.MainFormControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.Inventories.View.SelectDrawingOptionsTypes
{
	partial class MainForm : Form
	{
		private bool DrawMainData { get; }
		private bool DrawStatusData { get; }
		private static SelectedDrawingOptions options { get; set; }
		[Conditional("TEST")]
		public static void ShowForm()
		{
			options = new SelectedDrawingOptions();
			MainForm f = new MainForm(true, true, true);
			f.Show();
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="drawMainData"></param>
		/// <param name="drawStatusData"></param>
		/// <param name="includeSanierung">Is there sanierung data for drawing.</param>
		/// <returns></returns>
		public static SelectedDrawingOptions Show(bool drawMainData, bool drawStatusData, bool includeSanierung = false)
		{
			options = new SelectedDrawingOptions();

			MainForm f = new MainForm(drawMainData, drawStatusData, includeSanierung);
			f.ShowDialog();
			return options;
		}
		/// <summary>
		/// Has user clicked OK.
		/// </summary>
		private bool resultOk;
		private MainForm(bool drawMainData, bool drawStatusData, bool includeSanierung)
		{
			resultOk = false;
			DrawMainData = drawMainData;
			DrawStatusData = drawStatusData;
			InitializeComponent();
			Initialize(drawMainData, drawStatusData, includeSanierung);
		}
		private void Initialize(bool drawMainData, bool drawStatusData, bool includeSanierung)
		{
#region User controls
			MainDataView mainView = new MainDataView();
			mainView.Dock = DockStyle.Fill;
			panelMainData.Controls.Add(mainView);


			StatusDataView statusView = new StatusDataView();
			statusView.Dock = DockStyle.Fill;
			panelStatusData.Controls.Add(statusView);

#endregion
			MainDataController mainC = new MainDataController(mainView, options, drawMainData);
			StatusDataController statusC = new StatusDataController(statusView, options, drawStatusData);

			Action<DrawDamage> statusSelectionChanged = e =>
			{
				int height = 0;
				switch (e)
				{
					case DrawDamage.All:
						options.DrawDamageSource = DrawDamage.All;
						height = statusC.HideClassesSelection();
						break;
					case DrawDamage.HighestPipeClassOnly:
						options.DrawDamageSource = DrawDamage.HighestPipeClassOnly;
						height = statusC.HideClassesSelection();
						break;
					case DrawDamage.HighestStationClassOnly:
						options.DrawDamageSource = DrawDamage.HighestStationClassOnly;
						height = statusC.HideClassesSelection();
						break;
					case DrawDamage.SelectedClasses:
						options.DrawDamageSource = DrawDamage.SelectedClasses;
						height = statusC.ShowClassesSelection();
						break;
				}
				Height += height;
			};
			statusC.DamageDrawDrawTypeChanged += statusSelectionChanged;

			//statusSelectionChanged(statusC.CurrentDrawDamageType);


			var enumType = typeof(AdditionalPipeLinetype);
			comboBoxPipeLinestyle.DisplayMember = "Key"; 
			comboBoxPipeLinestyle.ValueMember = "Value";
			comboBoxPipeLinestyle.DataSource = Enum.GetNames(enumType).
								Select(x =>
											new KeyValuePair<string, AdditionalPipeLinetype>(x,
												(AdditionalPipeLinetype)Enum.Parse(enumType, x))
										).ToList();
			comboBoxPipeLinestyle.SelectedIndex = 0;

			enumType = typeof(Scale);
			comboBoxTextScaling.DisplayMember = "Key";
			comboBoxTextScaling.ValueMember = "Value";
			comboBoxTextScaling.DataSource = Enum.GetNames(enumType).
								Select(x =>
											new KeyValuePair<string, Scale>(x.Substring(1),
												(Scale)Enum.Parse(enumType, x))
										).ToList();
			comboBoxTextScaling.SelectedValue = options.TextScale;

			comboBoxPipeLinestyle.DataBindings.Add(nameof(ComboBox.SelectedValue), options, nameof(SelectedDrawingOptions.DrawAdditionalLines));
			comboBoxTextScaling.DataBindings.Add(nameof(ComboBox.SelectedValue), options, nameof(SelectedDrawingOptions.TextScale));
			checkBoxDrawSingleLine.DataBindings.Add(nameof(CheckBox.Checked), options, nameof(SelectedDrawingOptions.DrawSingleLine));
			checkBoxContinuousOnly.DataBindings.Add(nameof(CheckBox.Checked), options, nameof(SelectedDrawingOptions.UseOnlyContinuousLinetype));

			if (includeSanierung)
			{
				options.DrawOnlySanierung = true;
				Binding b;
				b = BindingHelper.Create(nameof(CheckBox.Checked), options, nameof(SelectedDrawingOptions.DrawOnlySanierung));
				checkBoxDrawSanierung.DataBindings.Add(b);
				b = BindingHelper.Create(nameof(CheckBox.Checked), options, nameof(SelectedDrawingOptions.DrawSanierungConcept));
				checkBoxDrawSanierungConcept.DataBindings.Add(b);
				b = BindingHelper.Create(nameof(CheckBox.Checked), options, nameof(SelectedDrawingOptions.DrawSanierungProcedures));
				checkBoxDrawSanierungProcedures.DataBindings.Add(b);
			}
			else
			{
				checkBoxDrawSanierung.Checked = false;
				groupBoxSanierung.Enabled = false;
				options.DrawOnlySanierung = false;
			}

			Action radioButtonChanged = () =>
			{
				bool enabled = radioButton2d.Checked || radioButton3d.Checked;
				checkBoxDrawSingleLine.Enabled = enabled;
				comboBoxPipeLinestyle.Enabled = enabled;
				checkBoxContinuousOnly.Enabled = enabled;
				if (radioButton2d.Checked)
					options.DrawType = DrawView.View2d;
				else if (radioButton3d.Checked)
					options.DrawType = DrawView.View3d;
				else if (radioButtonVisual3d.Checked)
					options.DrawType = DrawView.ViewVisual3d;
			};
			radioButton2d.CheckedChanged += (_, __) => radioButtonChanged();
			radioButton3d.CheckedChanged += (_, __) => radioButtonChanged();
			radioButtonVisual3d.CheckedChanged += (_, __) => radioButtonChanged();
			radioButton3d.Checked = true;

		}
		protected override void OnFormClosed(FormClosedEventArgs e)
		{
			base.OnFormClosed(e);
			if (!resultOk)
				options = null;
		}

		private void buttonOk_Click(object sender, EventArgs e)
		{
			resultOk = true;
            Close();
		}

		private void checkBoxDrawSanierung_CheckedChanged(object sender, EventArgs e)
		{
			bool enabled = checkBoxDrawSanierung.Checked;
			panelMainData.Enabled = !enabled && DrawMainData;
			panelStatusData.Enabled = !enabled && DrawStatusData;
			checkBoxDrawSanierungConcept.Enabled = enabled;
			checkBoxDrawSanierungProcedures.Enabled = enabled;
		}
	}
}
