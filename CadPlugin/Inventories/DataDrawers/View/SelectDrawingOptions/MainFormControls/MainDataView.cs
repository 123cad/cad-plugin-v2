﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CadPlugin.Inventories.View.SelectDrawingOptionsTypes;

namespace CadPlugin.Inventories.DataDrawers.View.SelectDrawingOption.MainFormControls
{
	public partial class MainDataView : UserControl
	{
		public Control ContainerControl => this;

		public MainDataView()
		{
			InitializeComponent();
		}
		public void Initialize(SelectedDrawingOptions opts)
		{
			

			List<KeyValuePair<string, ObjectColors>> damages = new List<KeyValuePair<string, ObjectColors>>();
			damages.Add(new KeyValuePair<string, ObjectColors>("Default", ObjectColors.None));
			damages.Add(new KeyValuePair<string, ObjectColors>("Entwaesserung", ObjectColors.Status));
			damages.Add(new KeyValuePair<string, ObjectColors>("Owner", ObjectColors.Owner));
			comboBoxSewageObjectColor.ValueMember = "Value";
			comboBoxSewageObjectColor.DisplayMember = "Key";
			comboBoxSewageObjectColor.DataSource = damages;
			comboBoxSewageObjectColor.SelectedIndex = 0;

			comboBoxSewageObjectColor.DataBindings.Add(nameof(ComboBox.SelectedValue), opts, nameof(SelectedDrawingOptions.ColorOfObjects));
		}
	}
}
