﻿using CadPlugin.Inventories.View.SelectDrawingOptionsTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.View.SelectDrawingOption.MainFormControls
{
	class MainDataController
	{
		private MainDataView view { get; set; }
		private SelectedDrawingOptions options { get; set; }
		public MainDataController(MainDataView v, SelectedDrawingOptions opt, bool drawMainData)
		{
			view = v;
			options = opt;
			view.ContainerControl.Enabled = drawMainData;

			view.Initialize(opt);
		}

	}
}
