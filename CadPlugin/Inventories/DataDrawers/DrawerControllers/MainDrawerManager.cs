﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DrawerControllers
{
	class MainDrawerManager
	{
		private static Dictionary<int, MainDrawer> factories = new Dictionary<int, MainDrawer>();
		static MainDrawerManager()
		{
			/*factories.Add(1, new MainDrawers.V_101_1s.V_101_1());
			factories.Add(2, new MainDrawers.V_101_2s.V_101_2());
			factories.Add(3, new MainDrawers.V_101_3s.V_101_3());
			factories.Add(4, new MainDrawers.V_101_4s.V_101_4());
			factories.Add(5, new MainDrawers.V_101_5s.V_101_5());
			factories.Add(6, new MainDrawers.V_101_6s.V_101_6());
			factories.Add(7, new MainDrawers.V_101_7s.V_101_7());
			factories.Add(8, new MainDrawers.V_101_8s.V_101_8());
			factories.Add(9, new MainDrawers.V_101_9s.V_101_9());
			factories.Add(10, new MainDrawers.V_101_10s.V_101_10());
			factories.Add(11, new MainDrawers.V_101_11s.V_101_11());
			factories.Add(12, new MainDrawers.V_101_12s.V_101_12());
			factories.Add(13, new MainDrawers.V_101_13s.V_101_13());
			factories.Add(30, new MainDrawers.V_101_30s.V_101_13());
			factories.Add(32, new MainDrawers.V_101_32s.V_101_13());
			factories.Add(36, new MainDrawers.V_101_36s.V_101_13());
			factories.Add(37, new MainDrawers.V_101_37s.V_101_13());
			factories.Add(39, new MainDrawers.V_101_39s.V_101_13());*/

			var type = typeof(MainDrawers.V_101_01s.V_101_01);
			// Remove .V101_1 from the namespace (we need namespace for all drawers).
			string drawerNamespace = type.Namespace.Remove(type.Namespace.LastIndexOf('.'));//.Replace(".V_101_1",' ').Trim() ;// Namespace containing all V101 classes.
			int namespaceLength = drawerNamespace.Length;
			var typeCollection = Assembly.GetExecutingAssembly().GetTypes().Where(a =>
												{
													bool valid = a.IsClass
																 && a.Namespace != null
																 && a.Namespace.Length > namespaceLength;
													if (valid)
													{
														var s = a.Namespace.Substring(0, namespaceLength);
														if (!s.Equals(drawerNamespace) || !a.Name.Contains("V_101"))
															valid = false;
													}
													return valid;
												})
												.OrderBy(x=>int.Parse(x.Name.Substring(6)))
												.ToList();
			foreach(var t in typeCollection)
			{
				int id = int.Parse(t.Name.Substring(6));
				factories.Add(id, (MainDrawer)Activator.CreateInstance(t));
			}
		}
		public static MainDrawer GetFactory(int objektArt)
		{
			if (factories.ContainsKey(objektArt))
				return factories[objektArt];
			return null;
		}
	}
}
