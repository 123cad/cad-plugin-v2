﻿using CadPlugin.Common;
using CadPlugin.Inventories.Drawers;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Inventories.DrawerControllers.MainDrawers.V_101_14s
{
	class V_101_14 : MainDrawer
	{
		public override int GeoObjektArt => 14;

		public override void Draw(TransactionData data, SewageObject obj, BlockAssociations blocks, DrawerManager drawer)
		{
			DrawPointsAsBlockReference(data, obj, blocks, drawer);
			DrawPolygon(data, obj, blocks, drawer);
		}
	}
}
