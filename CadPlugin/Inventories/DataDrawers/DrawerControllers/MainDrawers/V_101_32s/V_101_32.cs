﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens;
using geometry = Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries;
using CadPlugin.Common;

using Inventory.CAD;
using CadPlugin.Inventories.Drawers;
using CadPlugin.Inventories.Pipes;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.DrawerControllers.MainDrawers.V_101_32s
{
    class V_101_32 : MainDrawer
    {
        public override int GeoObjektArt
        {
            get
            {
                return 32;
            }
        }

        public override void Draw(TransactionData data, SewageObject obj, BlockAssociations blocks, DrawerManager drawer)
        {
			SharedDrawPipe(obj, drawer);

		}
	}
}
