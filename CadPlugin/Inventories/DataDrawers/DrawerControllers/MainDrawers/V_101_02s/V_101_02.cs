﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens;
using geometry = Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries;
using CadPlugin.Common;

using Inventory.CAD;
using   CadPlugin.Inventories.Drawers;
using   CadPlugin.Inventories.Shafts;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace   CadPlugin.Inventories.DrawerControllers.MainDrawers.V_101_02s
{
	class V_101_02:MainDrawer
	{
		public override int GeoObjektArt { get { return 2; } }

		public override void Draw(TransactionData data, SewageObject obj, BlockAssociations blocks, DrawerManager drawer)
		{
			DrawPointsAsBlockReference(data, obj, blocks, drawer, typeof(Drawers.SMP));
			drawer.Shaft.DrawShaft((Shaft)obj, ShaftDrawer.ShaftType.Rectangle);
		}
	}
}
