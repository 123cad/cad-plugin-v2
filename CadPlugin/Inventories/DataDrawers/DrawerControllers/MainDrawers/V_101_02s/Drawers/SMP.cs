﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries.Geometriedatens;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Knotens;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Knotens.Schachts;
using CadPlugin.Common;
using CadPlugin.Inventories.Drawers;
using CadPlugin.SewageHelpers;

using Inventory.CAD;


#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif


namespace CadPlugin.Inventories.DrawerControllers.MainDrawers.V_101_02s.Drawers
{
	class SMP : ICustomDrawer, ICustomDrawerPunkt
	{
		public BlockReference Draw(TransactionData data, SewageObject obj, Punkt point, Point3d position, BlockAssociations blocks, DrawerManager drawer)
		{

			AbwassertechnischeAnlagen ab = obj.Abwasser;
			//string blockName = blocks.GetBlockPath(typeof(V_101_2), GetType()).Name;
			//Point3d p3 = new Point3d(p.X, p.Y, p.Z);
			double deckelH = 0;
			double sohlH = position.Z;
			Dictionary<string, string> atts = new Dictionary<string, string>();
			Punkt pk = SewageObjectHelpers.GetPunktByAttribut(ab, "DMP");
			atts.Add("BEZEICHNUNG", ab.Objektbezeichnung);
			atts.Add("TYPE", GetType().Name);
			if (point.HochwertIsSet)
				atts.Add("SOHLHOEHE", DoubleHelper.ToStringInvariant(position.Z, 3));
			if (pk != null)
			{
                MyUtilities.Geometry.Point3d ep = pk.GetPoint3d();
				deckelH = ep.Z;
				if (pk.HochwertIsSet)
					atts.Add("DECKELHOEHE", DoubleHelper.ToStringInvariant(deckelH, 3));
			}
			double d = 0;
			if (ShaftSewageObjectHelper.GetSchachtDepth(ab, ref d))
				atts.Add("TIEFE", DoubleHelper.ToStringInvariant(d, 3));
			Schacht s = ShaftSewageObjectHelper.GetSchacht(ab);
			ShaftSewageObjectHelper.SMPData dt = ShaftSewageObjectHelper.Get_SMP_Data(s);
			if (s != null && s.SchachtFunktionIsSet)
			{
				string val = s.SchachtFunktion.ToString();
				atts.Add("FUNKTION", val + "- " + Isybau2015.ReferenceTables.G.G301.Instance.GetValue(val));
				if (s.SchachtFunktion == 7 && atts.ContainsKey("TIEFE"))
					atts.Remove("TIEFE");
			}
			// MATERIAL - Aufbau/aufbaumaterial
			atts.Add("MATERIAL", dt.Material);

            // OWNER
            var owner = ab?.GetOwner();
			if (owner != null)
				atts.Add("BETREIBER", owner);

			atts.Add("STATUS", ab.Status.ToString() + " - " + Isybau2015.ReferenceTables.G.G105.Instance.GetValue(ab.Status.ToString()));

			if (obj.Inspizierte != null && obj.Inspizierte.OptischeInspektion != null)
			{
				atts.Add("DATE_INSP", obj.Inspizierte.OptischeInspektion.InspektionsdatumFormatted);
			}

			double length = dt.Laenge;
			double width = dt.Breite;

			//BlockReference br = Common.CADBlockHelper.CreateBlockReference(data.Tr, data.Bt, data.ModelSpace, blockName, p3, atts);
			//BlockReference br = MainDrawer.CreateBlockReference(data, typeof(V_101_2), GetType(), blocks, ab, point, p, atts);

			//MainDrawer.ApplyAttributeColor(data.Tr, br.AttributeCollection, "BEZEICHNUNG", ab);
			
			//Common.CADBlockHelper.ScaleBlockReferencePolyline(data.Tr, br, length, width);


			string blockName = blocks.GetBlockPath(typeof(V_101_02), GetType()).Name;
			double height = s.Schachttiefe;
			Dictionary<string, Color> attColors = new Dictionary<string, Color>();
			Color c = drawer.Shaft.ColorManager.ObjectColor.GetObjectColor();
			attColors.Add("BEZEICHNUNG", c);
			//BlockScaler scaler = scalerFactory.GetNonUniformScaler(data, length, width, height);// new BlockScalings.NonUniformScaler2d(length, width);
			//BlockReference br = drawer.ShaftDrawer.DrawShaft(data, blockName, ab, scaler, position, atts, attColors);

			BlockReference br = CADBlockHelper.CreateBlockReference(data.Tr, data.Bt, data.Btr, blockName, position, atts, attColors);
			CADBlockHelper.ScaleBlockReference(data.Tr, br, length, width);

			return br;

		}
	}
}
