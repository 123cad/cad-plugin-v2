﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using static Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries.Geometriedaten;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries.Geometriedatens;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Knotens;
using objektarts = Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens;//Objektarts.Knotens.Schachts;
using CadPlugin.Common;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Knotens.Schachts;
using Inventory.CAD;
using static CadPlugin.Inventories.DrawerControllers.BlockAssociations;
using CadPlugin.Inventories.Drawers;
using CadPlugin.Inventories.DataDrawers.Drawers;
using CadPlugin.Inventories.DataDrawers.Drawers.Layers;
using geometry = Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries;
using CadPlugin.Inventories.Pipes;
using CadPlugin.Inventories.Colors;
using CadPlugin.SewageHelpers;

#if BRICSCAD
using Teigha.Colors;
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Inventories.DrawerControllers
{
	/// <summary>
	/// Determines which objects are drawn. Then they are drawn, or they are transfered to ICustomDrawer to be drawn.
	/// If object does not depend on ICustomDrawer-punktattributabwasser (all object for single GeoObjektArt are drawn in the same way - Kante as a line), 
	/// then no ICustomDrawer is used (it can be only used to determine if point is supported by GeoObjektArt). 
	/// If object depends on PunktattributAbwasser, like 1/SMP, then ICustomDrawer does the drawing.
	/// </summary>
	abstract class MainDrawer
	{
		// Possible improvement is to do proxy instantiation of drawers - create one only when it is needed.
		private static HashSet<string> allowedDrawerClasses = new HashSet<string>();
		private Dictionary<string, ICustomDrawer> drawerCollection = new Dictionary<string, ICustomDrawer>();

		/// <summary>
		/// To which GeoObjektArt this factory belongs.
		/// </summary>
		public abstract int GeoObjektArt { get; }
		static MainDrawer()
		{			
			string[] classes = new string[]
			{
				"DMP"
				,"SMP"
				,"RAP"
				,"LHP"
				,"AP"
				,"NN"
				,"ER"
				,"GA"
				,"RR"
				,"SE"
				,"SBD"
				,"SBW"
				,"KOP"
				,"FLP"
				,"ZLK"
				,"KP"
				,"HP"
				,"GOK"
				,"KMP"
				,"PAU"
				,"AV"
				,"RV"
				,"EG"
				,"BA"
				,"ZG"
				,"DR"
				,"GP"
				,"SCH"
				,"AUS"
				,"PU"
				,"WE"
				,"ZIS"
				,"REC"
				,"SIE"
				,"HE"
				, "Shared"
			};
			foreach (var v in classes.Distinct())
				allowedDrawerClasses.Add(v);
		}
		protected MainDrawer()
		{

			Assembly currentAssembly = Assembly.GetExecutingAssembly();
			Type currentType = GetType();
			string ns = currentType.Namespace;
			string drawerNs = ns + ".Drawers";

			Type drawer = typeof(ICustomDrawer);
			List<Type> classes = currentAssembly.GetTypes().Where(x => 
																x.IsClass 
																&& allowedDrawerClasses.Contains(x.Name) 
																&& x.Namespace == drawerNs)
																.ToList();
			foreach (Type t in classes)
			{
				ICustomDrawer d = (ICustomDrawer)Activator.CreateInstance(t);
				drawerCollection.Add(t.Name, d);
			}

		}

		public ICustomDrawer GetDrawer(string s)
		{
			if (drawerCollection.ContainsKey(s))
				return drawerCollection[s];
			else if (drawerCollection.ContainsKey("Shared"))
				return drawerCollection["Shared"];
			return null;
		}

		public abstract void Draw(TransactionData data, SewageObject obj, BlockAssociations blocks, DrawerManager drawer);


		/*public static BlockReference CreateBlockReference(TransactionData data, Type t, BlockAssociations blocks, AbwassertechnischeAnlagen ab, Punkt pt, Point3d position, Dictionary<string, string> attributes, Dictionary<string, Color> attColors = null)
		{
			BlockReference br = null;
			SingleBlock sb = blocks.GetBlockPathShared(t);
			if (sb == null)
				return null;
			string blockName = sb.Name;
			Point3d p3 = position;
			br = Common.CADBlockHelper.CreateBlockReference(data.Tr, data.Bt, data.Btr, blockName, p3, attributes, attColors);
			return br;
		}*/
		public static BlockReference CreateBlockReference(TransactionData data, Type t, Type subType, BlockAssociations blocks, AbwassertechnischeAnlagen ab, Punkt pt, Point3d position, Dictionary<string, string> attributes, Dictionary<string, Color> attColors = null)
		{
			BlockReference br = null;
			SingleBlock sb = blocks.GetBlockPath(t, subType);
			if (sb == null)
				return null;
			string blockName = sb.Name;
			Point3d p3 = position;
			br = Common.CADBlockHelper.CreateBlockReference(data.Tr, data.Bt, data.Btr, blockName, p3, attributes, attColors);
			return br;
		}
		/// <summary>
		/// Adjust attribute text size, with predefined attribute.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="br"></param>
		/// <param name="textDef"></param>
		public static void AdjustBlockReferenceAttributes(TransactionData data, BlockReference br, TextSizeDefinitions textDef)
		{
			//SetAttributesTextSize(this BlockReference br, Transaction tr, double defaultSize, Dictionary<string, double> attributes = null)
			br.SetAttributesTextSize(data.Tr, textDef.ShaftBlockAttributeNotName, new Dictionary<string, double>
			{
				{ "NAME",textDef.ShaftBlockAttributeName }
			});

		}
		/// <summary>
		/// Returns layer name for the object.
		/// </summary>
		/// <param name="ab"></param>
		/// <returns></returns>
		protected string GetLayerName(AbwassertechnischeAnlagen ab)
		{
			string layerName = LayerManager.Instance.GetMainDataLayer(ab);
			return layerName;
		}
		/// <summary>
		/// Method which is shared between multiple classes (to avoid code duplication).
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="drawer"></param>
		protected static void SharedDrawPipe(SewageObject obj, DrawerManager drawer)
		{
			AbwassertechnischeAnlagen ab = obj.Abwasser;
			Geometrie gm = ab.Geometrie;
			geometry.Geometriedaten gmd = gm.GeometrieDaten;

			// All vertices of the pipe.
			List<MyUtilities.Geometry.Point3d> pipePoints = new List<MyUtilities.Geometry.Point3d>();
			if (gmd.Kantes != null)
			{
				foreach (geometry.Kante k in gmd.Kantes)
				{
					double d = 0;
					PipeSewageObjectHelpers.GetPipeDiameter(ab, ref d);
					drawer.Pipe.DrawPipe((Pipe)obj);// data, start, end, ab, d);
					MyUtilities.Geometry.Point3d start = k.Start.GetPoint3d();
					MyUtilities.Geometry.Point3d end = k.Ende.GetPoint3d();

					if (pipePoints.Count == 0)
						pipePoints.Add(start);
					else
					{
						if (pipePoints.Last().GetVectorTo(start).Length > 0.0001)
							pipePoints.Add(start);
					}
					pipePoints.Add(end);

				}
				//Point3d middle = new Point3d((start.X + end.X) / 2, (start.Y + end.Y) / 2, (start.Z + end.Z) / 2);
				/* Dictionary<string, string> atts = new Dictionary<string, string>();
				 atts.Add("NAME", ab.Objektbezeichnung);
				 Color c = drawer.Pipe.ColorManager.AttributeColor.GetAttributeColor(obj);
				 Dictionary<string, Color> attColors = new Dictionary<string, Color>();
				 attColors.Add("NAME", c);
				 BlockReference br = MainDrawer.CreateBlockReference(data, typeof(V_101_4), blocks, ab, k.Start, middle, atts, attColors);*/

				drawer.Pipe.DrawMiddleBlock((Pipe)obj, pipePoints);
				//drawer.Pipe.DrawMiddleBlock((Pipe)obj, new PositionVector() { Position = middle.FromCADPoint(), Direction = start.GetVectorTo(end).FromCADVector() });

			}

		}
		/// <summary>
		/// Draw object points as blocks. 
		/// smpDrawerType - blocks created by this type will be added to collection of 
		/// objects for which AttLabel is created.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="obj"></param>
		/// <param name="blocks"></param>
		/// <param name="drawer"></param>
		/// <param name="smpType"></param>
		protected void DrawPointsAsBlockReference(TransactionData data, SewageObject obj, BlockAssociations blocks, DrawerManager drawer, Type smpDrawerType = null)
		{
			AbwassertechnischeAnlagen ab = obj.Abwasser;
			Geometrie gm = ab.Geometrie;
			geometry.Geometriedaten gmd = gm.GeometrieDaten;
			string layerName = GetLayerName(obj.Abwasser);

			MyUtilities.Geometry.Point3d last = new MyUtilities.Geometry.Point3d();
			bool lastExists = false;
			// Draw only points.
			foreach (var p in gmd.Punkts)
			{
				MyUtilities.Geometry.Point3d position = p.GetPoint3d();
				// If X is not set (assume y is not set).
				if (!p.RechtswertIsSet)
				{
					if (!lastExists)
						continue;
					double z = position.Z;
					position = last;
					if (p.HochwertIsSet)
						position = new MyUtilities.Geometry.Point3d(position.X, position.Y, p.Hochwert);
				}
				else
				{
					last = position;
					lastExists = true;
				}
				ICustomDrawerPunkt dp = GetDrawer(p.PunktattributAbwasser) as ICustomDrawerPunkt;
				if (dp != null)
				{
					BlockReference br = dp.Draw(data, obj, p, position.ToCADPoint(), blocks, drawer);
					br.Layer = layerName;
					if (dp.GetType() == smpDrawerType)// is Drawers.SMP)
						drawer.Shaft.CreatedEntities.AddAttributeBlock(br);
					AdjustBlockReferenceAttributes(data, br, drawer.Shaft.TextSize);
					drawer.Shaft.CreatedEntities.AddShaftEntity(br);
				}
			}
		}

		protected void DrawPolygon(TransactionData data, SewageObject obj, BlockAssociations blocks, DrawerManager drawer)
		{
			AbwassertechnischeAnlagen ab = obj.Abwasser;
			Geometrie gm = ab.Geometrie;
			geometry.Geometriedaten gmd = gm.GeometrieDaten;
			string layerName = GetLayerName(obj.Abwasser);

			List<Point3d> polygonPoints = new List<Point3d>();

			bool first = true;
			foreach (var pg in gmd.Polygone)
			{
				List<Tuple<geometry.Geometriedatens.Punkt, Point3d>> allPoints = new List<Tuple<geometry.Geometriedatens.Punkt, Point3d>>();
				// Draw polygon
				if (pg.Kantes != null)
				{
					foreach (geometry.Kante kt in pg.Kantes)
					{
						geometry.Geometriedatens.Punkt pk;
						//ICustomDrawerPunkt dp;
						if (first)
						{
							first = false;
							pk = kt.Start;
							allPoints.Add(new Tuple<geometry.Geometriedatens.Punkt, Point3d>(pk, pk.GetPoint3d().ToCADPoint()));

							//polygonPoints.Add(pk.Get3dPoint().ToCADPoint());
							//dp = GetDrawer(pk.PunktattributAbwasser) as ICustomDrawerPunkt;
							//if (dp != null)
							//{
							//	var pt = pk.Get3dPoint().ToCADPoint();
							//	var br = dp.Draw(data, obj, pk, pt, blocks, drawer);
							//	AdjustBlockReferenceAttributes(data, br, drawer.Shaft.TextSize);
							//	drawer.Shaft.CreatedEntities.AddShaftEntity(br);
							//}
						}
						pk = kt.Ende;
						allPoints.Add(new Tuple<geometry.Geometriedatens.Punkt, Point3d>(pk, pk.GetPoint3d().ToCADPoint()));
						//polygonPoints.Add(pk.Get3dPoint().ToCADPoint());
						//dp = GetDrawer(pk.PunktattributAbwasser) as ICustomDrawerPunkt;
						//if (dp != null)
						//{
						//	var pt = pk.Get3dPoint().ToCADPoint();
						//	var br = dp.Draw(data, obj, pk, pt, blocks, drawer);
						//	AdjustBlockReferenceAttributes(data, br, drawer.Shaft.TextSize);
						//	drawer.Shaft.CreatedEntities.AddShaftEntity(br);
						//}

					}
					Action<Punkt, Point3d> addBlock = (pk, pt) =>
					{
						var dp = GetDrawer(pk.PunktattributAbwasser) as ICustomDrawerPunkt;
						if (dp == null)
							return;
						var br = dp.Draw(data, obj, pk, pt, blocks, drawer);
						AdjustBlockReferenceAttributes(data, br, drawer.Shaft.TextSize);
						drawer.Shaft.CreatedEntities.AddShaftEntity(br);
					};
					for (int i = 0; i < allPoints.Count; i++)
					{
						var t = allPoints[i];
						if (i < allPoints.Count - 1)
							addBlock(t.Item1, t.Item2);
						else
						{
							// Check if last one is same as start one.
							if (allPoints[0].Item2.GetVectorTo(t.Item2).Length > 0.01)
								addBlock(t.Item1, t.Item2);
						}
					}
					polygonPoints = allPoints.Select(x => x.Item2).ToList();
				}

				Polyline pl = SewageObjectHelpers.DrawPolygon2d(data, pg);
				//Polyline3d pl3 = SewageObjectHelpers.DrawPolygon3d(data, pg);
				if (pg.PolygonArt != 3)
					pl.Closed = true;
				pl.Layer = layerName;
				drawer.Shaft.CreatedEntities.AddShaftEntity(pl, true);
				int pointsCount = polygonPoints.Count;
				if (pointsCount > 2)
				{
					// Draw polygon block.
					Point3d polygonCenter;/* = new Point3d(
						polygonPoints.Sum(x => x.X) / pointsCount,
						polygonPoints.Sum(x => x.Y) / pointsCount,
						polygonPoints.Sum(x => x.Z) / pointsCount);*/
					var v1 = polygonPoints[0].GetVectorTo(polygonPoints[1]);
					var v2 = polygonPoints[1].GetVectorTo(polygonPoints[2]);
					polygonCenter = polygonPoints[0].Add(v1.MultiplyBy(0.5).Add(v2.MultiplyBy(0.5)));

					
					Dictionary<string, string> atts = new Dictionary<string, string>();
					atts.Add("NAME", obj.Abwasser.Objektbezeichnung);
					Bauwerk bw = (obj.Abwasser.Auswahlelement as objektarts.Knoten)?.Auswahlelement as Bauwerk;
					string s = Isybau2015.ReferenceTables.G.G400.Instance.GetValue(bw?.Bauwerkstyp.ToString()) ?? "";
					atts.Add("TYPE", s);
					Dictionary<string, Color> attColors = new Dictionary<string, Color>();
					attColors.Add("NAME", ColorAssociations.GetEntwaesserungColor(ab.Entwaesserungsart));
					// Add NAME color.
					var bref = Common.CADBlockHelper.CreateBlockReference(data.Tr, data.Bt, data.Btr, BlockAssociations.PolygonBlockName, polygonCenter, atts, attColors);
					bref.Layer = layerName;
					drawer.Shaft.CreatedEntities.AddShaftEntity(bref, true);
				}
			}
		}
	}
}
