﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using md = CadPlugin.Inventories.DrawerControllers.MainDrawers;
using System.Diagnostics;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Inventories.DrawerControllers
{
	
	/// <summary>
	/// Holds current associations with blocks (which block is drawn for current IDrawer)
	/// </summary>
	class BlockAssociations
	{
		public class SingleBlock
		{
			/// <summary>
			/// Directory path to the block (including trailing "/" );
			/// </summary>
			public string Path { get; set; }
			/// <summary>
			/// Name of the block without path.
			/// </summary>
			public string Name { get; set; }
		}

		/// <summary>
		/// Types where subtypes have different blocks.
		/// </summary>
		private Dictionary<Type, Dictionary<Type, SingleBlock>> types = new Dictionary<Type, Dictionary<Type, SingleBlock>>();
		/// <summary>
		/// Name of the block which is drawn in the middle of the polygon.
		/// </summary>
		public static string PolygonBlockName = "Bauwerk";

		public BlockAssociations()
		{
		}
		public void Initialize2d(Database db)
		{
			Initialize(db, false);
		}
		public void Initialize3d(Database db)
		{
			Initialize(db, true);
		}
		private void Initialize(Database db, bool is3d)
		{

			Assembly currentAssembly = Assembly.GetExecutingAssembly();
			Type currentType = GetType();
			string ns = currentType.Namespace;
			string drawerNs = ns + ".MainDrawers";

			types.Add(typeof(MainDrawers.V_101_01s.V_101_01), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_02s.V_101_02), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_03s.V_101_03), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_08s.V_101_08), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_09s.V_101_09), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_10s.V_101_10), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_11s.V_101_11), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_12s.V_101_12), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_13s.V_101_13), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_14s.V_101_14), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_15s.V_101_15), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_16s.V_101_16), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_17s.V_101_17), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_18s.V_101_18), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_19s.V_101_19), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_20s.V_101_20), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_21s.V_101_21), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_22s.V_101_22), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_23s.V_101_23), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_24s.V_101_24), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_25s.V_101_25), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_26s.V_101_26), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_27s.V_101_27), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_28s.V_101_28), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_29s.V_101_29), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_30s.V_101_30), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_31s.V_101_31), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_33s.V_101_33), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_34s.V_101_34), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_35s.V_101_35), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_36s.V_101_36), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_37s.V_101_37), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_38s.V_101_38), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_39s.V_101_39), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_40s.V_101_40), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_41s.V_101_41), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_42s.V_101_42), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_50s.V_101_50), new Dictionary<Type, SingleBlock>());
			types.Add(typeof(MainDrawers.V_101_51s.V_101_51), new Dictionary<Type, SingleBlock>());

            //sharedTypes.Add(typeof(MainDrawers.V_101_04s.V_101_04), new SingleBlock());
			//sharedTypes.Add(typeof(MainDrawers.V_101_05s.V_101_05), new SingleBlock());
			//sharedTypes.Add(typeof(MainDrawers.V_101_06s.V_101_06), new SingleBlock());
			//sharedTypes.Add(typeof(MainDrawers.V_101_07s.V_101_07), new SingleBlock());
            //sharedTypes.Add(typeof(MainDrawers.V_101_8s.V_101_8), new SingleBlock());
            //sharedTypes.Add(typeof(MainDrawers.V_101_9s.V_101_9), new SingleBlock());
            //sharedTypes.Add(typeof(MainDrawers.V_101_10s.V_101_10), new SingleBlock());
            //sharedTypes.Add(typeof(MainDrawers.V_101_11s.V_101_11), new SingleBlock());
            //sharedTypes.Add(typeof(MainDrawers.V_101_12s.V_101_12), new SingleBlock());
            //sharedTypes.Add(typeof(MainDrawers.V_101_13s.V_101_13), new SingleBlock());
            //sharedTypes.Add(typeof(MainDrawers.V_101_30s.V_101_30), new SingleBlock());
            //sharedTypes.Add(typeof(MainDrawers.V_101_32s.V_101_32), new SingleBlock());

            string str = typeof(MainDrawers.V_101_01s.V_101_01).Namespace;
			drawerNs = str.Substring(0, str.LastIndexOf('.'));

			List<Type> classes = currentAssembly.GetTypes().Where(x => x.IsClass && x.Namespace != null && x.Namespace.Contains(drawerNs)).ToList();
			Type drawer = typeof(ICustomDrawer);
			// Initialize types (sharedTypes are already initialized) with all subtypes from subnamespace.
			foreach (KeyValuePair<Type, Dictionary<Type, SingleBlock>> k in types)
			{
				string tNs = k.Key.Namespace + ".Drawers";
				Type[] drawers = classes.Where(x => x.Namespace == tNs && x.GetInterface(drawer.Name) != null).ToArray();
				if (drawers != null && drawers.Length > 0)
				{
					foreach (Type t in drawers)
						k.Value.Add(t, new SingleBlock());
				}

			}
			Action<Type, Type, string, string> addBlockS = (type, subtype, name, path) =>
			{
				var dic = types[type];

				SingleBlock sb = dic[subtype];
				sb.Name = name;
				sb.Path = path;
				Common.CADBlockHelper.LoadBlockFromOutside(db, sb.Path, sb.Name);
			};
			/*Action<Type, string, string> addBlock = (type, name, path) =>
			{
				SingleBlock sb = sharedTypes[type];
				sb.Name = name;
				sb.Path = path;
				Common.CADBlockHelper.LoadBlockFromOutside(db, sb.Path, sb.Name);
			};*/

			string blocksPath = @"C:\Program Files\Common Files\123CAD Blöcke\";

			string s = BlockAssociations.PolygonBlockName;
			Common.CADBlockHelper.LoadBlockFromOutside(db, blocksPath, s);



			// Separate attribut drawers
			// 1
			s = "RegelSchacht";
			if (is3d)
				s = "cylinder";
			addBlockS(typeof(md.V_101_01s.V_101_01), typeof(md.V_101_01s.Drawers.SMP), s, blocksPath);
			addBlockS(typeof(md.V_101_01s.V_101_01), typeof(md.V_101_01s.Drawers.DMP), "Schachtdeckel", blocksPath);

			// 2
			s = "Schacht_Eckig";
			if (is3d)
				s = "quader";
			addBlockS(typeof(md.V_101_02s.V_101_02), typeof(md.V_101_02s.Drawers.SMP), s, blocksPath);
			addBlockS(typeof(md.V_101_02s.V_101_02), typeof(md.V_101_02s.Drawers.DMP), "Schachtdeckel_Eckig", blocksPath);

			// 3
			s = "RegelSchacht";
			if (is3d)
				s = "cylinder";
			addBlockS(typeof(md.V_101_03s.V_101_03), typeof(md.V_101_03s.Drawers.SMP), s, blocksPath);
			addBlockS(typeof(md.V_101_03s.V_101_03), typeof(md.V_101_03s.Drawers.DMP), "Schachtdeckel", blocksPath);
			addBlockS(typeof(md.V_101_03s.V_101_03), typeof(md.V_101_03s.Drawers.SBW), "Bauwerkspunkt", blocksPath);

			// 4, 5, 6, 7
			//addBlock(typeof(md.V_101_04s.V_101_04), "Rohr_Daten", blocksPath);
			//addBlock(typeof(md.V_101_05s.V_101_05), "Rohr_Daten", blocksPath);
			//addBlock(typeof(md.V_101_06s.V_101_06), "Rohr_Daten", blocksPath);
			//addBlock(typeof(md.V_101_07s.V_101_07), "Rohr_Daten", blocksPath);
			//addBlock(typeof(md.V_101_30s.V_101_30), "Rohr_Daten", blocksPath);
			//addBlock(typeof(md.V_101_32s.V_101_32), "Rohr_Daten", blocksPath);

			// 8
			addBlockS(typeof(md.V_101_08s.V_101_08), typeof(md.V_101_08s.Drawers.AP), "Anschlusspunkt_Haltung", blocksPath);

			// 9
			addBlockS(typeof(md.V_101_09s.V_101_09), typeof(md.V_101_09s.Drawers.NN), "Anschlusspunkt", blocksPath);

			// 10
			addBlockS(typeof(md.V_101_10s.V_101_10), typeof(md.V_101_10s.Drawers.GA), "Anschlusspunkt", blocksPath);

			// 11
			addBlockS(typeof(md.V_101_11s.V_101_11), typeof(md.V_101_11s.Drawers.RA), "Anschlusspunkt", blocksPath);

			// 12
			addBlockS(typeof(md.V_101_12s.V_101_12), typeof(md.V_101_12s.Drawers.SE), "Anschlusspunkt_Strasseneinlauf", blocksPath);

			// 13
			addBlockS(typeof(md.V_101_13s.V_101_13), typeof(md.V_101_13s.Drawers.ER), "Rinne", blocksPath);

			// 14
			addBlockS(typeof(md.V_101_14s.V_101_14), typeof(md.V_101_14s.Drawers.SBD), "Bauwerkspunkt_Haupt", blocksPath);
			addBlockS(typeof(md.V_101_14s.V_101_14), typeof(md.V_101_14s.Drawers.KOP), "Bauwerkspunkt", blocksPath);
			addBlockS(typeof(md.V_101_14s.V_101_14), typeof(md.V_101_14s.Drawers.SBW), "Bauwerkspunkt", blocksPath);

			// 15
			addBlockS(typeof(md.V_101_15s.V_101_15), typeof(md.V_101_15s.Drawers.SBD), "Bauwerkspunkt_Haupt", blocksPath);
			addBlockS(typeof(md.V_101_15s.V_101_15), typeof(md.V_101_15s.Drawers.KOP), "Bauwerkspunkt", blocksPath);
			addBlockS(typeof(md.V_101_15s.V_101_15), typeof(md.V_101_15s.Drawers.SBW), "Bauwerkspunkt", blocksPath);

			// 16
			addBlockS(typeof(md.V_101_16s.V_101_16), typeof(md.V_101_16s.Drawers.ZLK), "Klaeranlage", blocksPath);
			addBlockS(typeof(md.V_101_16s.V_101_16), typeof(md.V_101_16s.Drawers.KOP), "Bauwerkspunkt", blocksPath);

			// 17
			addBlockS(typeof(md.V_101_17s.V_101_17), typeof(md.V_101_17s.Drawers.SBD), "Bauwerkspunkt_Haupt", blocksPath);
			addBlockS(typeof(md.V_101_17s.V_101_17), typeof(md.V_101_17s.Drawers.KOP), "Bauwerkspunkt", blocksPath);
			addBlockS(typeof(md.V_101_17s.V_101_17), typeof(md.V_101_17s.Drawers.SBW), "Bauwerkspunkt", blocksPath);

			// 18
			addBlockS(typeof(md.V_101_18s.V_101_18), typeof(md.V_101_18s.Drawers.Shared), "Pumpe", blocksPath);

			// 19
			addBlockS(typeof(md.V_101_19s.V_101_19), typeof(md.V_101_19s.Drawers.Shared), "Bauwerkspunkt", blocksPath);

			// 20
			addBlockS(typeof(md.V_101_20s.V_101_20), typeof(md.V_101_20s.Drawers.Shared), "Bauwerkspunkt", blocksPath);

			// 21
			addBlockS(typeof(md.V_101_21s.V_101_21), typeof(md.V_101_21s.Drawers.Shared), "Bauwerkspunkt", blocksPath);

			// 22
			addBlockS(typeof(md.V_101_22s.V_101_22), typeof(md.V_101_22s.Drawers.Shared), "Bauwerkspunkt", blocksPath);

			// 23
			addBlockS(typeof(md.V_101_23s.V_101_23), typeof(md.V_101_23s.Drawers.Shared), "Bauwerkspunkt", blocksPath);
			addBlockS(typeof(md.V_101_23s.V_101_23), typeof(md.V_101_23s.Drawers.SBW), "Bauwerkspunkt", blocksPath);

			// 24
			addBlockS(typeof(md.V_101_24s.V_101_24), typeof(md.V_101_24s.Drawers.Shared), "Bauwerkspunkt", blocksPath);

			// 25
			addBlockS(typeof(md.V_101_25s.V_101_25), typeof(md.V_101_25s.Drawers.Shared), "Bauwerkspunkt", blocksPath);
			addBlockS(typeof(md.V_101_25s.V_101_25), typeof(md.V_101_25s.Drawers.SBW), "Bauwerkspunkt", blocksPath);

			// 26
			addBlockS(typeof(md.V_101_26s.V_101_26), typeof(md.V_101_26s.Drawers.Shared), "Bauwerkspunkt", blocksPath);

			// 27
			addBlockS(typeof(md.V_101_27s.V_101_27), typeof(md.V_101_27s.Drawers.Shared), "Bauwerkspunkt", blocksPath);

			// 28
			addBlockS(typeof(md.V_101_28s.V_101_28), typeof(md.V_101_28s.Drawers.Shared), "Bauwerkspunkt", blocksPath);

			// 29
			addBlockS(typeof(md.V_101_29s.V_101_29), typeof(md.V_101_29s.Drawers.Shared), "Zisterne", blocksPath);

			// 31
			addBlockS(typeof(md.V_101_31s.V_101_31), typeof(md.V_101_31s.Drawers.Shared), "Versickerungsanlage", blocksPath);
			addBlockS(typeof(md.V_101_31s.V_101_31), typeof(md.V_101_31s.Drawers.SBW), "Bauwerkspunkt", blocksPath);

			// 33
			addBlockS(typeof(md.V_101_33s.V_101_33), typeof(md.V_101_33s.Drawers.Shared), "Versickerungsanlage", blocksPath);

			// 34
			addBlockS(typeof(md.V_101_34s.V_101_34), typeof(md.V_101_34s.Drawers.Shared), "Rohrende_Verschlossen", blocksPath);

			// 35
			addBlockS(typeof(md.V_101_35s.V_101_35), typeof(md.V_101_35s.Drawers.Shared), "Entwaesserungspunkt_Gebaeude", blocksPath);

			// 36
			addBlockS(typeof(md.V_101_36s.V_101_36), typeof(md.V_101_36s.Drawers.Shared), "Bodenablauf", blocksPath);

			// 37
			addBlockS(typeof(md.V_101_37s.V_101_37), typeof(md.V_101_37s.Drawers.Shared), "Gerinne_Zulauf", blocksPath);

			// 38
			addBlockS(typeof(md.V_101_38s.V_101_38), typeof(md.V_101_38s.Drawers.Shared), "Drainage", blocksPath);

			// 39
			addBlockS(typeof(md.V_101_39s.V_101_39), typeof(md.V_101_39s.Drawers.Shared), "Gerinnepunkt", blocksPath);

			// 40
			addBlockS(typeof(md.V_101_40s.V_101_40), typeof(md.V_101_40s.Drawers.Shared), "Bauwerkspunkt", blocksPath);

			// 41
			addBlockS(typeof(md.V_101_41s.V_101_41), typeof(md.V_101_41s.Drawers.Shared), "Bauwerkspunkt", blocksPath);

			// 42
			addBlockS(typeof(md.V_101_42s.V_101_42), typeof(md.V_101_42s.Drawers.Shared), "Bauwerkspunkt", blocksPath);

			// 50
			addBlockS(typeof(md.V_101_50s.V_101_50), typeof(md.V_101_50s.Drawers.Shared), "Hofeinlauf", blocksPath);

			// 51
			addBlockS(typeof(md.V_101_51s.V_101_51), typeof(md.V_101_51s.Drawers.Shared), "Gelaende_Oberkante", blocksPath);

			

            // Create blocks which doesn't exist.

            // Read from settings if found - if not assign default blocks.
        }
        /// <summary>
        /// Returns block path for type/subtype
        /// </summary>
        /// <param name="type"></param>
        /// <param name="subtype"></param>
        /// <returns></returns>
        public SingleBlock GetBlockPath(Type type, Type subtype)
		{
			SingleBlock b = null;
			//if (subtype == null || !types.ContainsKey(type))
			{
				//return null;// GetBlockPath(type);
			}

			if (types.ContainsKey(type) && subtype != null)
			{
				var dic = types[type];
				if (dic.ContainsKey(subtype))
					b = dic[subtype];
				//else if (dic.ContainsKey("Shared"))
					//b = dic["Shared"];
			}
			if (b == null)
				MyLog.MyTraceSource.Error($"No block found for type {type.Name} and subtype{subtype?.Name}");
			return b;
		}
		/// <summary>
		/// Returns shared block, used for all subtypes of a type.
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		/*public SingleBlock GetBlockPathShared(Type t)
		{
			SingleBlock b = null;
			if (sharedTypes.ContainsKey(t))
			{
				b = sharedTypes[t];
			}
			return b;
		}*/
	}
}
