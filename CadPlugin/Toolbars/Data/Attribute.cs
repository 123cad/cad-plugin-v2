﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Toolbars.Data
{
	class Attribute
	{
		/// <summary>
		/// Name for ExtensionDictionary when referencing attribute from outside.
		/// </summary>
		public static readonly string ExtensionId = ExtensionDictionaryEntries.AttributeTextLinkId;
		public static string DefaultSuffixText = "m²";
		public TransactionData Transaction { get; private set; }
		/// <summary>
		/// Area to which this attribute is related with.
		/// </summary>
		public Area Area { get; private set; }

		private List<Table> __Tables = null;
		/// <summary>
		/// Tables in which this attribute is appearing.
		/// </summary>
		public IList<Table> Tables {
			get
			{
				if (__Tables == null)
					throw new InvalidOperationException("Tables are not initialized!");
				return __Tables;
			}
			private set { __Tables = (List<Table>)value; }
		}

		/// <summary>
		/// Entity used as reference from other classes.
		/// </summary>
		public Entity MainEntity { get { return Text; } }
		/// <summary>
		/// Used also as reference point to this attribute.
		/// </summary>
		public MText Text { get; private set; }
		public Polyline Boundary { get; private set; }
		public Hatch Hatch { get; private set; }
		public string Prefix { get; private set; }
		public string Suffix { get; private set; }
		/// <summary>
		/// Returns content part value.
		/// </summary>
		public double Content { get { return Area.Boundary.Area; } }
		/// <summary>
		/// Prefix concatenated to Content by "=", Content concatenated to Suffix by " ".
		/// "=" and " " are very important! From single string we need to find 3 parts (prefix, content, suffix),
		/// and these 2 characters are used.
		/// "Flaeche = 12.76 m²"
		/// </summary>
		private string AllText { get { return Prefix + " = " + DoubleHelper.ToStringInvariant(Content, 2) + " " + Suffix; } }

		private Attribute()
		{
			Prefix = "Fläche";
			Suffix = DefaultSuffixText;
			//Tables = new List<Table>();
		}
		/// <summary>
		/// Returns ObjectId for text object of attribute.
		/// </summary>
		/// <returns></returns>
		public ObjectId GetAreaId()
		{
			if (Text != null)
			{
				ObjectId? oid = Text.LoadFromExtensionDataObjectId(Transaction.Transaction, Transaction.Database, 
									Area.ExtensionId);
				if (oid.HasValue)
					return oid.Value;
			}
			return ObjectId.Null;
		}
		/// <summary>
		/// Adds new area and creates a link to it.
		/// </summary>
		public void AddNewArea(Area ar)
		{
			if (ar.Boundary == null)
				throw new ArgumentException("Area does not contain Polyline object!");
			ObjectId atIdStored = GetAreaId();
			if (atIdStored != ObjectId.Null)
				throw new InvalidOperationException("Adding new area when there is existing one!");
			Area = ar;
			if (!Text.IsWriteEnabled)
				Text.UpgradeOpen();
			Text.SaveExtensionDataObjectId(Transaction.Transaction, ar.MainEntity.ObjectId,
												Area.ExtensionId);
		}
		/// <summary>
		/// Just adds existing area, with checking if id matches to the stored one.
		/// </summary>
		public void AddExistingArea(Area a)
		{
			if (!(Text.LoadFromExtensionDataObjectId(
						Transaction.Transaction,
						Transaction.Database,
						Area.ExtensionId).Value == a.MainEntity.ObjectId))
			{
				throw new ArgumentException("Attribute provided to Area is not the one referenced by the Area");
			}
			Area = a;
		}

		public void AddTable(Table t)
		{
			if (!Tables.Contains(t))
			{
				Tables.Add(t);
				Text.AppendExtensionDataValue(Transaction.Transaction, ExtensionDictionaryEntries.TableId, new TypedValue((int)DxfCode.Text, t.Id));
			}
		}
		public void RemoveTable(Table t)
		{
			if (Tables.Contains(t))
			{
				Tables.Remove(t);
				Text.RemoveExtensionDataValue(Transaction.Transaction, ExtensionDictionaryEntries.TableId, new TypedValue((int)DxfCode.Text, t.Id));
			}
		}
		
		/// <summary>
		/// Sets new prefix for text and refreshes.
		/// </summary>
		public void SetNewPrefix(string prefix, bool refresh = true)
		{
			Prefix = prefix;
			if (!Text.IsWriteEnabled)
				Text.UpgradeOpen();
			Text.SaveExtensionDataString(Transaction.Transaction, ExtensionDictionaryEntries.AttributePrefix,
											prefix);
			Text.RemoveFromExtensionData(Transaction.Transaction, ExtensionDictionaryEntries.AttributeCurrentText);
			if (refresh)
				Refresh();
		}
		public void SetNewSuffix(string suffix, bool refresh = true)
		{
			Suffix = suffix;
			if (!Text.IsWriteEnabled)
				Text.UpgradeOpen();
			Text.SaveExtensionDataString(Transaction.Transaction, ExtensionDictionaryEntries.AttributeSuffix,
											suffix);
			Text.RemoveFromExtensionData(Transaction.Transaction, ExtensionDictionaryEntries.AttributeCurrentText);
			if (refresh)
				Refresh();
		}

		/// <summary>
		/// Loads talbes related to attribute.
		/// </summary>
		public void InitializeTables()
		{
			Tables = new List<Table>();
			Dictionary<string, TypedValue[]> values = Text.LoadExtensionData(Transaction.Transaction, ExtensionDictionaryEntries.TableId);
			if (values.ContainsKey(ExtensionDictionaryEntries.TableId))
			{
				TypedValue[] vals = values[ExtensionDictionaryEntries.TableId];
				foreach (TypedValue v in vals)
				{
					Table t = Transaction.ObjectManager.LoadExistingTable(v.Value as string);
					if (t == null)
						System.Diagnostics.Debug.WriteLine("Requested table not found!!!");

					if (t != null)
						Tables.Add(t);
				}
			}
		}
		/// <summary>
		/// Returns all entities which are displayed in Attribute.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<Entity> GetAllEntities ()
		{
			List<Entity> entities = new List<Entity>();
			entities.Add(Boundary);
			entities.Add(Hatch);
			entities.Add(Text);
			return entities;
		}
		/// <summary>
		/// Sets content, recalculates geometry and repositions Attribute based on point.
		/// If point is not specified, last location is used.
		/// </summary>
		public void Refresh(Point3d? point = null)
		{
			if (!Text.IsWriteEnabled)
				Text.UpgradeOpen();
			if (point.HasValue)
			{
				Text.Location = point.Value;
			}
			string currentText = Text.Contents;
			string oldText = Text.LoadExtensionString(Transaction.Transaction,
														ExtensionDictionaryEntries.AttributeCurrentText);
			if (oldText != null && oldText != currentText)
			{
				bool suffixExists = true;
				int length = currentText.LastIndexOf("=");
				if (length < 1)
				{
					length = currentText.Length;
					// If no "=" is found, there is no way that m2 is found.
					suffixExists = false;
				}
				SetNewPrefix(currentText.Substring(0, length), false);
				if (suffixExists)
				{
					// If there is possibility that suffix exists, check it.
					// After last index of " " is text which is represented as suffix.
					currentText = currentText.Trim();
					int suffixIndex = currentText.LastIndexOf(" ");
					if (suffixIndex >= 1 && suffixIndex < currentText.Length + 1)
					{
						string suffix = currentText.Substring(suffixIndex + 1);
						SetNewSuffix(suffix, false);
					}							
				}
			}
			Point3d location = Text.Location;
			string contents = AllText;
			Text.Contents = contents;
			Text.SaveExtensionDataString(Transaction.Transaction,
											ExtensionDictionaryEntries.AttributeCurrentText,
											contents);
			Extents3d extents = Text.GeometricExtents;

			double deltaX = Text.TextHeight * 1.2;
			double deltaY = Text.TextHeight * 1.2;
			Point2d start = new Point2d(location.X - deltaX, location.Y + deltaY);
			double width = (extents.MaxPoint.X - extents.MinPoint.X) + 2 * deltaX;
			double height = (extents.MaxPoint.Y - extents.MinPoint.Y) + 2 * deltaY;

			if (!Boundary.IsWriteEnabled)
				Boundary.UpgradeOpen();
			Boundary.SetPointAt(0, start);
			Boundary.SetPointAt(1, start.Add(new Vector2d(width, 0)));
			Boundary.SetPointAt(2, start.Add(new Vector2d(width, -height)));
			Boundary.SetPointAt(3, start.Add(new Vector2d(0, -height)));

			if (!Hatch.IsWriteEnabled)
				Hatch.UpgradeOpen();
			if (Hatch.NumberOfLoops > 0)
				Hatch.RemoveLoopAt(0);

			Hatch.InsertLoopAt(0, HatchLoopTypes.Outermost, new ObjectIdCollection(new ObjectId[] { Boundary.ObjectId }));
			
		}
		/// <summary>
		/// Erases all entities in the attribute.
		/// </summary>
		public void Delete()
		{
			ToolbarNodManager.RemoveAttribute(Transaction, Text.ObjectId);
			if (!Text.IsErased)
			{
				if (!Text.IsWriteEnabled)
					Text.UpgradeOpen();
				Text.Erase();
			}
			if (!Boundary.IsErased)
			{
				if (!Boundary.IsWriteEnabled)
					Boundary.UpgradeOpen();
				Boundary.Erase();
			}
			if (!Hatch.IsErased)
			{
				if (!Hatch.IsWriteEnabled)
					Hatch.UpgradeOpen();
				Hatch.Erase();
			}
			
		}

		/// <summary>
		/// Determines ONLY if entity type is used in Area.
		/// </summary>
		public static bool IsAttributeObjectType(Entity ent)
		{
			return ent is Polyline || ent is Hatch || ent is MText;
		}
		public static bool IsAttributeEntity(TransactionData data, Entity e)
		{
			var v = GetMainObject(data, e, true);
			return v != null;
		}
		/// <summary>
		/// Creates attribute (create text, polyline boundary and hatch).
		/// </summary>
		/// <param name="data"></param>
		/// <param name="text"></param>
		/// <returns></returns>
		public static Attribute Create(TransactionData data, SingleLinetypeData hatchData)
		{

			Attribute at = new Attribute();
			at.Transaction = data;
			// Order of creation is important. (newer ObjectId is drawn above older one).
			// Text must be above hatch.
			// DrawTable is not working quite well.
			Hatch hatch = data.Transaction.CreateEntity(data.ModelSpace, EntityToCreate.Hatch) as Hatch;
			Polyline pl = data.Transaction.CreateEntity(data.ModelSpace, EntityToCreate.Polyline) as Polyline;
			MText text = data.Transaction.CreateEntity(data.ModelSpace, EntityToCreate.MText) as MText;
			hatch.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId, hatch.ObjectId.Handle.ConvertToString());
			pl.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId, pl.ObjectId.Handle.ConvertToString());
			text.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId, text.ObjectId.Handle.ConvertToString());

			at.Text = text;
			at.Hatch = hatch;
			at.Boundary = pl;
			text.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId, text.ObjectId.Handle.ConvertToString());
			pl.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId, pl.ObjectId.Handle.ConvertToString());
			hatch.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId, hatch.ObjectId.Handle.ConvertToString());

			text.TextHeight = hatchData.TextSize;
			text.BackgroundFill = true;
			text.BackgroundScaleFactor = 1.5;

			pl.Closed = true;
			pl.Color = Color.FromColor(hatchData.LinetypeType.PatternColor.ToColor());
			pl.AddVertexAt(0, new Point2d(), 0, 0, 0);
			pl.AddVertexAt(1, new Point2d(1, 0), 0, 0, 0);
			pl.AddVertexAt(2, new Point2d(1, -1), 0, 0, 0);
			pl.AddVertexAt(3, new Point2d(0, -1), 0, 0, 0);

			//hatchData.ApplyPattern(hatch, pl);
			hatchData.ApplyPattern(pl);

			ToolbarNodManager.AddAttribute(data, text.ObjectId);

			at.Text.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectGroupTypeDescription,
												ExtensionDictionaryEntries.DescriptionAttributeText);
			at.Text.SaveExtensionDataObjectId(data.Transaction, hatch.ObjectId,
													ExtensionDictionaryEntries.AttributeHatchLinkId);
			at.Text.SaveExtensionDataObjectId(data.Transaction, pl.ObjectId,
												ExtensionDictionaryEntries.AttributePolylineLinkId);
			at.Text.SaveExtensionDataString(data.Transaction,
											ExtensionDictionaryEntries.AttributePrefix,
											at.Prefix);
			at.Text.SaveExtensionDataString(data.Transaction,
											ExtensionDictionaryEntries.AttributeSuffix,
											at.Suffix);
			at.Text.SaveExtensionData(data.Transaction, ExtensionDictionaryEntries.TableId);

			at.Hatch.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectGroupTypeDescription,
												ExtensionDictionaryEntries.DescriptionAttributeHatch);
			at.Hatch.SaveExtensionDataObjectId(data.Transaction, text.ObjectId,
												ExtensionDictionaryEntries.AttributeTextLinkId);
			at.Boundary.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectGroupTypeDescription,
													ExtensionDictionaryEntries.DescritptionAttributePolyline);
			at.Boundary.SaveExtensionDataObjectId(data.Transaction, text.ObjectId,
												ExtensionDictionaryEntries.AttributeTextLinkId);
			data.Editor.Regen();

			return at;
		}
		/// <summary>
		/// Returns main object (MText). Any Attribute object can be provided.
		/// </summary>
		public static MText GetMainObject(TransactionData data, Entity ent, bool openErased = false)
		{
			if (ent is MText)
			{
				if (ent.LoadExtensionString(data.Transaction, 
							ExtensionDictionaryEntries.ObjectGroupTypeDescription) == ExtensionDictionaryEntries.DescriptionAttributeText)
					return ent as MText;
			}
			else if (ent is Hatch)
			{
				ObjectId? oid = ent.LoadFromExtensionDataObjectId(data.Transaction, data.Database, ExtensionDictionaryEntries.AttributeTextLinkId);
				if (oid.HasValue && oid.Value != ObjectId.Null)
				{
					MText tx = (MText)data.Transaction.GetObject(oid.Value, OpenMode.ForRead, openErased);
					return tx;
				}
			}
			else if (ent is Polyline)
			{
				ObjectId? oid = ent.LoadFromExtensionDataObjectId(data.Transaction, data.Database, ExtensionDictionaryEntries.AttributeTextLinkId);
				if (oid.HasValue && oid.Value != ObjectId.Null)
				{
					MText tx = (MText)data.Transaction.GetObject(oid.Value, OpenMode.ForRead, openErased);
					return tx;
				}
			}
			return null;
		}
		/// <summary>
		/// Loads existing data for an attribute.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="openErased">Need when one of attributes is deleted - to delete other.</param>
		/// <returns></returns>
		public static Attribute Load(TransactionData data, Entity ent, bool openErased = false)
		{

			MText text = GetMainObject(data, ent, openErased);
			if (text == null)
				return null;
			{
				// If handle of entity does not match to the one written in ExtensionDictionary, 
				// object has been copied and attribute entris from ExtensionDictionary are deleted.
				string[] atts = new string[]
				{
					ExtensionDictionaryEntries.AttributeHatchLinkId,
					ExtensionDictionaryEntries.AttributePolylineLinkId,
					ExtensionDictionaryEntries.AttributeTextLinkId,
					ExtensionDictionaryEntries.AttributeCurrentText,
					ExtensionDictionaryEntries.AttributePrefix,
					ExtensionDictionaryEntries.AttributeSuffix,
					ExtensionDictionaryEntries.AreaPolylineLinkId,
					ExtensionDictionaryEntries.TableId,
					ExtensionDictionaryEntries.ObjectGroupTypeDescription, 
					ExtensionDictionaryEntries.ObjectPersonalId
				};
				bool valid = true;
				string handle = text.ObjectId.Handle.ConvertToString();
				if (text.LoadExtensionString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId) != handle)
				{

					text.RemoveFromExtensionData(data.Transaction, atts);
					System.Diagnostics.Debug.WriteLine("Removed from ExtensionDictionary! (attribute not found in attribute). id= " + ent.ObjectId);
					valid = false;
				}
				if (text != ent)
				{
					handle = ent.ObjectId.Handle.ConvertToString();
					if (ent.LoadExtensionString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId) != handle)
					{
						ent.RemoveFromExtensionData(data.Transaction, atts);
						valid = false;
					}
				}
				if (!valid)
					return null;
			}

			Attribute at = new Attribute();
			at.Prefix = text.LoadExtensionString(data.Transaction, ExtensionDictionaryEntries.AttributePrefix);
			at.Suffix = text.LoadExtensionString(data.Transaction, ExtensionDictionaryEntries.AttributeSuffix);
			at.Transaction = data;
			at.Text = text;

			ObjectId? polyId = text.LoadFromExtensionDataObjectId(data.Transaction, data.Database,
																	ExtensionDictionaryEntries.AttributePolylineLinkId);

			if (polyId.HasValue && polyId.Value != ObjectId.Null)
			{
				at.Boundary = data.Transaction.GetObject(polyId.Value, OpenMode.ForRead, openErased) as Polyline;
			}
			ObjectId? hatchId = text.LoadFromExtensionDataObjectId(data.Transaction, data.Database,
																   ExtensionDictionaryEntries.AttributeHatchLinkId);
			if (hatchId.HasValue && hatchId.Value != ObjectId.Null)
			{
				at.Hatch = data.Transaction.GetObject(hatchId.Value, OpenMode.ForRead, openErased) as Hatch;
			}
			
			if (at.Boundary == null)
				at = null;
			

			return at;
		}

		public override bool Equals(object obj)
		{
			Attribute at = obj as Attribute;
			if (at == null)
				return false;
			return at.MainEntity.ObjectId == MainEntity.ObjectId;
		}
		public override int GetHashCode()
		{
			return MainEntity.ObjectId.Handle.Value.GetHashCode();
		}
		public override string ToString()
		{
			return "Attribute: " + MainEntity.ObjectId + " Tables: " + Tables.Count;
		}
	}
}
