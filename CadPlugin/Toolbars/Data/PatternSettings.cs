﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Toolbars.Data
{
	abstract class PatternSettings
	{
		public string PatternName { get; set; }
		public ColorPersistence PatternColor { get; set; }

		private int __Transparency;
		/// <summary>
		/// Transparency level [0-90] (defined by CAD). 0 - not transparent, 90 - completely transparent.
		/// NOTE: However, struct Transparency is more like opacity (inverse value from transparency), 
		/// so when instantiating Transparency it is needed to invert the desired value (percentage).
		/// </summary>
		public int TransparencyLevel
		{
			get { return __Transparency; }
			set
			{
				__Transparency = value;
				if (value < 0)
					__Transparency = 0;
				else if (value > 90)
					__Transparency = 90;
			}
		}


		protected PatternSettings()
		{
			PatternColor = new ColorPersistence(ToolbarColorSettings.Instance.TextColor);
		}
		protected PatternSettings(PatternSettings t)
		{
			PatternColor = t.PatternColor;
			PatternName = t.PatternName;
			TransparencyLevel = t.TransparencyLevel;
		}

		/// <summary>
		/// Creates pattern for provided hatch.
		/// </summary>
		public void ApplyPattern(Entity ent)
		{
			SetHatchPattern(h);
			int transparency = 255 - (int)(TransparencyLevel * (255 / 100.0));
			if (transparency > 255)
				transparency = 255;
			h.Transparency = new Transparency((byte)transparency);
			h.Color = Color.FromColor(PatternColor.ToColor());
			h.Associative = true;
		}
		protected abstract void SetHatchPattern(Hatch h);
		public abstract PatternSettings Clone();
	}
}
