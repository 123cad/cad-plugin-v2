﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Toolbars
{
	class ToolbarColorSettings
	{
		private static ToolbarColorSettings __Instance;
		private static bool colorsSet = false;
		public static ToolbarColorSettings Instance
		{
			get
			{
				if (__Instance == null)
					__Instance = new ToolbarColorSettings();
				return __Instance;
			}
		}

		/// <summary>
		/// Current color depending on color theme (light or dark).
		/// </summary>
		public Color BackgroundColor = Color.Gray;
		/// <summary>
		/// Current color of text, depending on color theme ("opposite" of BackgroundColor)
		/// </summary>
		public Color TextColor = Color.White;
		private ToolbarColorSettings()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			using (Transaction tr = doc.TransactionManager.StartTransaction())
			{
				MText text = new MText();
				if (text.Color.ColorValue.R < 100)
				{
					// Light background
					BackgroundColor = Color.White;
					TextColor = Color.DarkGray;
				}
				else
				{
					// Dark background
					BackgroundColor = Color.DarkGray;
					TextColor = Color.White;
				}
				text.Dispose();
				tr.Abort();
			}
		}
	}
}
