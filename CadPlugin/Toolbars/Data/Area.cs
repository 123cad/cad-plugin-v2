﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Toolbars.Data
{
	abstract class Area
	{
		/// <summary>
		/// ExtensionDictionary name used to reference Area.
		/// </summary>
		//public static string ExtensionId { get { Transaction.Extension.AreaPolylineLinkId; } }
		public TransactionData Transaction { get; private set; }
		public Attribute Attribute { get; set; }
		public Polyline Boundary { get; protected set; }
		public Entity MainEntity { get { return Boundary; } }
		protected Area(TransactionData data)
		{
			Transaction = data;
		}
		/// <summary>
		/// Returns ObjectId for text object of attribute.
		/// </summary>
		/// <returns></returns>
		public ObjectId GetAttributeId()
		{
			if (Boundary != null)
			{
				ObjectId? oid = Boundary.LoadFromExtensionDataObjectId(Transaction.Transaction, Transaction.Database, 
											Attribute.ExtensionId);
				if (oid.HasValue)
					return oid.Value;
			}
			return ObjectId.Null;
		}

		/// <summary>
		/// Assigns new attribute to this Area.
		/// </summary>
		public void AddNewAttribute(Attribute at)
		{
			if (at.Text == null)
				throw new ArgumentException("Attribute does not contain Text object!");
			ObjectId atIdStored = GetAttributeId();
			if (atIdStored != ObjectId.Null)
				throw new InvalidOperationException("Adding new attribute when there is existing one!");
			Attribute = at;
			if (!Boundary.IsWriteEnabled)
				Boundary.UpgradeOpen();
			Boundary.SaveExtensionDataObjectId(Transaction.Transaction, at.MainEntity.ObjectId, 
										Attribute.ExtensionId);
		}
		/// <summary>
		/// Just sets attribute reference (and checks if provided Attribute is the one referenced by the Area)
		/// </summary>
		public void AddExistingAttribute(Attribute at)
		{
			if (!(Boundary.LoadFromExtensionDataObjectId(
						Transaction.Transaction, 
						Transaction.Database, 
						Attribute.ExtensionId).Value == at.Text.ObjectId))
			{
				throw new ArgumentException("Attribute provided to Area is not the one referenced by the Area");
			}
			Attribute = at;
		}
		/// <summary>
		/// Removes the attribute from 
		/// </summary>
		public void DeleteAttribute()
		{

			if (Attribute != null)
				Attribute.Delete();
			if (Boundary.ExtensionDictionary != ObjectId.Null)
			{
				if (!Boundary.IsWriteEnabled)
					Boundary.UpgradeOpen();
				Boundary.RemoveFromExtensionData(Transaction.Transaction, 
													Attribute.ExtensionId,
													Transaction.Extension.ObjectGroupTypeDescription);

			}
		}

		/// <summary>
		/// Returns all entities which are displayed in Area (attribute not included).
		/// </summary>
		/// <returns></returns>
		public IEnumerable<Entity> GetAllEntities()
		{
			List<Entity> entities = new List<Entity>();
			entities.Add(Boundary);
			return entities;
		}
		public abstract void ApplyPattern(PatternSettings pattern);

		/// <summary>
		/// Determines ONLY if entity type is used in Area.
		/// </summary>
		public static bool IsAreaObjectType(Entity ent)
		{
			return ent is Polyline;
		}
		public static bool IsAreaEntity(TransactionData data, Entity ent)
		{
			var v = GetMainObject(data, ent, true);
			return v != null;
		}
		
		/// <summary>
		/// Returns main object (polyline). Any area object can be provided.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="ent"></param>
		/// <returns></returns>
		public static Polyline GetMainObject(TransactionData data, Entity ent, bool openErased)
		{
			if (ent is Polyline)
			{
				if (ent.LoadExtensionString(data.Transaction,
					data.Extension.ObjectGroupTypeDescription) == data.Extension.DescriptionAreaPolyline)
					return ent as Polyline;
			}			
			return null;
		}
		
		public override bool Equals(object obj)
		{
			Area ar = obj as Area;
			if (ar == null)
				return false;
			return ar.MainEntity.ObjectId.Handle == MainEntity.ObjectId.Handle;
		}
		public override int GetHashCode()
		{
			return MainEntity.ObjectId.Handle.Value.GetHashCode();
		}
		public override string ToString()
		{
			return "Area: " + MainEntity.ObjectId;
		}
	}
}
