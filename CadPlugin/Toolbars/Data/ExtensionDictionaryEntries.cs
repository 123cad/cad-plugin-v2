﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Toolbars
{
	public abstract class ExtensionDictionaryEntries
	{
		/// <summary>
		/// Every object which is created by toolbar, saves it's own handle to ExtensionDic.
		/// So when object is copied, it gets different ObjectID, and we can remove ExtensionDic.
		/// </summary>
		public readonly string ObjectPersonalId = "_PersonalHandle";
		public readonly string ObjectGroupTypeDescription = "_GroupObject";

		/// <summary>
		/// Bounding polyline around hatch.
		/// </summary>
		public readonly string DescriptionAreaPolyline = "AreaPolyline";
		/// <summary>
		/// Hatch filling inside of area.
		/// </summary>
		public readonly string DescriptionAreaHatch = "AreaHatch";
		/// <summary>
		/// Polyline around text.
		/// </summary>
		public readonly string DescritptionAttributePolyline = "AttPolyline";
		/// <summary>
		/// Hatch inside of text bounding polyline.
		/// </summary>
		public readonly string DescriptionAttributeHatch = "AttHatch";
		/// <summary>
		/// Attribute text.
		/// </summary>
		public readonly string DescriptionAttributeText = "AttText";


		/// <summary>
		/// Entry in Extension dictionary which stores prefix value for attribute.
		/// </summary>
		public readonly string AttributePrefix = "_AttPrefix";
		/// <summary>
		/// Suffix value for attribute.
		/// </summary>
		public readonly string AttributeSuffix = "_AttSuffix";
		public readonly string AttributeCurrentText = "_AttCurrText";

		/// <summary>
		/// Reference to the attribute text.
		/// </summary>
		public readonly string AttributeTextLinkId = "_AttText_Link";
		/// <summary>
		/// Reference to the attribute hatch.
		/// </summary>
		public readonly string AttributeHatchLinkId = "_AttHatch_Link";
		public readonly string AttributePolylineLinkId = "_AttPoly_Link";
		public readonly string AreaPolylineLinkId = "_AreaPoly_Link";
		public readonly string AreaHatchLinkId = "_AreaHatch_Link";

		public readonly string TableId = "_Table";

		/// <summary>
		/// Prefix is used as unique entry in ExtensionDictionary for a module.
		/// </summary>
		/// <param name="uniquePrefix"></param>
		protected ExtensionDictionaryEntries(string uniquePrefix)
		{
			ObjectPersonalId = uniquePrefix + ObjectPersonalId;
			ObjectGroupTypeDescription = uniquePrefix + ObjectGroupTypeDescription;

			AttributePrefix = uniquePrefix + AttributePrefix;
			AttributeSuffix = uniquePrefix + AttributeSuffix;
			AttributeCurrentText = uniquePrefix + AttributeCurrentText;
			AttributeTextLinkId = uniquePrefix + AttributeTextLinkId;
			AttributeHatchLinkId = uniquePrefix + AttributeHatchLinkId;
			AttributePolylineLinkId = uniquePrefix + AttributePolylineLinkId;
			AreaPolylineLinkId = uniquePrefix + AreaPolylineLinkId;
			AreaHatchLinkId = uniquePrefix + AreaHatchLinkId;
			TableId = uniquePrefix + TableId;
		}
	}
}
