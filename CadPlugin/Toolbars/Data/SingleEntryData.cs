﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Toolbars.Data
{
	/// <summary>
	/// Data for single entry in the toolbar.
	/// </summary>
	abstract class SingleEntryData
	{
		/// <summary>
		/// Assigned name by the user.
		/// </summary>
		public string Name { get; set; }
		public double TextSize { get; set; }
		/// <summary>
		/// Data for associated pattern.
		/// </summary>
		public PatternSettings Pattern { get; set; }
		protected SingleEntryData(string name)
		{
			Name = name;
			TextSize = 1;
		}
	}
}
