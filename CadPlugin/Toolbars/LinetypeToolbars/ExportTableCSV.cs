﻿using CadPlugin.LinetypeToolbars.Persistence;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.LinetypeToolbars
{
	class ExportTableCSV
	{
		/// <summary>
		/// User selects a file to which data (table to csv) is saved. Returns if operation is successfull.
		/// </summary>
		public static bool Start(Data.Table table)
		{
			System.Windows.Forms.SaveFileDialog saveDlg = new System.Windows.Forms.SaveFileDialog();
			saveDlg.InitialDirectory = DataPersistence.LoadCsvLocation();
			saveDlg.OverwritePrompt = true;
			saveDlg.FileName = table.GetTableName() + ".csv";
			saveDlg.Filter = "CSV files (*.csv)|*csv|All files (*.*)|*.*";
			if (saveDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				string dir = Path.GetDirectoryName(saveDlg.FileName);
				DataPersistence.SaveCsvLocation(dir);
				string content = GetTableCSV(table);
				File.WriteAllText(saveDlg.FileName, content);
				System.Diagnostics.Process.Start("explorer.exe", "/select, \"" + saveDlg.FileName + "\"");
				return true;
			}
			return false;
		}

		private static string GetTableCSV(Data.Table table)
		{
			StringBuilder builder = new StringBuilder();
			foreach(Data.Attribute at in table.Attributes)
			{
				string s = at.Prefix + "," +
							DoubleHelper.ToStringInvariant(at.Area.Boundary.Area, 2) + "," +
							DoubleHelper.ToStringInvariant(at.Area.Boundary.Length, 2);
				builder.AppendLine(s);
			}

			return builder.ToString();
		}
	}
}
