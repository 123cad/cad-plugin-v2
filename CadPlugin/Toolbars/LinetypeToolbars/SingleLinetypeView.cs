﻿using CadPlugin.LinetypeToolbars.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif


namespace CadPlugin.LinetypeToolbars
{
	public partial class SingleLinetypeView : Form
	{

		/// <summary>
		/// Patterns from default CAD .pat file.
		/// </summary>
		private static List<string> loadedLinetypes;


		private SingleLinetypeData data;
		private ErrorProvider error = null;
		internal SingleLinetypeView(SingleLinetypeData d)
		{
			InitializeComponent();
			error = new ErrorProvider();
			data = d;
			SetData();
			DialogResult = DialogResult.Cancel;
			SetGermanText();

		}
		private void SetGermanText()
		{
			Text = "Linientyp-Definition";
			labelTextSize.Text = "Text-Höhe";
			labelColor.Text = "Farbe";
			buttonColorChange.Text = "Ändern";
			labelTransparency.Text = "Transparenz";
			labelLinetype.Text = "Linientyp";
			labelScale.Text = "Skalierung";
			labelWeight.Text = "Linienstärke";
		}
		private void SetData()
		{
			textBoxName.Text = data.Name;
			textBoxColor.BackColor = data.LinetypeType.PatternColor.ToColor();
			textBoxTextSize.Text = DoubleHelper.ToStringInvariant(data.TextSize, 2);
			trackBarTransparency.Value = data.LinetypeType.TransparencyLevel;

			loadedLinetypes = Common.CADLineTypeHelper.GetLineTypes();

			System.Diagnostics.Debug.WriteLine("Number of linetypes: " + loadedLinetypes.Count);

			Dictionary<string, LinetypeType> items = new Dictionary<string, LinetypeType>();
			foreach (string s in loadedLinetypes)
			{
				items.Add(s, new LinetypeType() { PatternName = s });
			}
			if (!items.ContainsKey(data.LinetypeType.PatternName))
				items.Add(data.LinetypeType.PatternName, new LinetypeType() { PatternName = data.LinetypeType.PatternName });

			comboBoxPatterns.DataSource = new BindingSource(items, null);
			comboBoxPatterns.DisplayMember = "Key";
			comboBoxPatterns.ValueMember = "Value";

			comboBoxPatterns.AutoCompleteCustomSource = new AutoCompleteStringCollection();
			comboBoxPatterns.AutoCompleteCustomSource.AddRange(items.Select(x=>x.Key).ToArray());

			//if(data.LinetypeType.PatternName != null && items.ContainsKey(data.LinetypeType.PatternName))
			{
				comboBoxPatterns.Text = data.LinetypeType.PatternName;// = items[data.HatchType.PatternName];
			}

			textBoxScale.Text = DoubleHelper.ToStringInvariant(data.LinetypeType.Scale, 2);

			List<KeyValuePair<string, LineWeight>> weights = new List<KeyValuePair<string, LineWeight>>();
			List<LineWeight> enumWeights = ((LineWeight[])Enum.GetValues(typeof(LineWeight))).ToList();
			bool preselected = false;
			weights.Add(new KeyValuePair<string, LineWeight>("VonBlock"/*"ByBlock"*/, LineWeight.ByBlock));
			weights.Add(new KeyValuePair<string, LineWeight>("VonLayer"/*"ByLayer"*/, LineWeight.ByLayer));
			weights.Add(new KeyValuePair<string, LineWeight>("VonLinientypVorgabe"/*"ByLineWeightDefault"*/, LineWeight.ByLineWeightDefault));
			foreach (LineWeight w in enumWeights)
			{
				if (w == data.LinetypeType.Weight)
					preselected = true;
				string name = w.ToString();
				if (name[0] == 'L')
				{
					name = name.Substring(10);
					name = name.Insert(1, ".");
					name += " mm";
					weights.Add(new KeyValuePair<string, LineWeight>(name, w));
				}
				
			}
			comboBoxWeight.DataSource = weights;
			comboBoxWeight.ValueMember = "Value";
			comboBoxWeight.DisplayMember = "Key";

			if (preselected)
				comboBoxWeight.SelectedValue = data.LinetypeType.Weight;
			else
				comboBoxWeight.SelectedIndex = 0;
			
			InitializeComboBoxDrawing();
			
		}
		private void InitializeComboBoxDrawing()
		{
			comboBoxPatterns.DrawMode = DrawMode.OwnerDrawVariable;
			comboBoxPatterns.MeasureItem += ComboBoxPatterns_MeasureItem;
			comboBoxPatterns.DrawItem += ComboBoxPatterns_DrawItem;
			comboBoxPatterns.SelectedIndexChanged += ComboBoxPatterns_SelectedIndexChanged;
			comboBoxPatterns.TextChanged += ComboBoxPatterns_SelectedIndexChanged;
		}

		private void ComboBoxPatterns_SelectedIndexChanged(object sender, EventArgs e)
		{
			// Don't alow line item to be selected (line item is measured to 1px height, and draw as a single line (fill rectangle).
			KeyValuePair<string, LinetypeType> item = (KeyValuePair<string, LinetypeType>)comboBoxPatterns.SelectedItem;
			if (item.Value == null)
				comboBoxPatterns.SelectedItem = comboBoxPatterns.Items[comboBoxPatterns.SelectedIndex - 1];
			else
			{
				//RefreshHatchType(item.Value);
			}
		}


		private void ComboBoxPatterns_DrawItem(object sender, DrawItemEventArgs e)
		{
			KeyValuePair<string, LinetypeType> item = (KeyValuePair<string, LinetypeType>)comboBoxPatterns.Items[e.Index];
			if (item.Value == null)
			{
				e.Graphics.FillRectangle(new SolidBrush(e.ForeColor), e.Bounds);
			}
			else
			{
				if (e.Index >= 0)
				{
					Graphics g = e.Graphics;
					Brush brush = (e.State.HasFlag(DrawItemState.Selected) && item.Value != null)?
								   new SolidBrush(SystemColors.Highlight) : new SolidBrush(e.BackColor);
					Brush tBrush = new SolidBrush(e.ForeColor);

					g.FillRectangle(brush, e.Bounds);
					e.Graphics.DrawString(item.Key, e.Font, tBrush, e.Bounds, StringFormat.GenericDefault);
					brush.Dispose();
					tBrush.Dispose();
				}
				e.DrawFocusRectangle();
			}
		}

		private void ComboBoxPatterns_MeasureItem(object sender, MeasureItemEventArgs e)
		{
			KeyValuePair<string, LinetypeType> item = (KeyValuePair<string, LinetypeType>)comboBoxPatterns.Items[e.Index];
			if (item.Value == null)
				e.ItemHeight = 1;
		}

		private void UpdateData()
		{
			string s = textBoxName.Text;
			data.Name = s;
			s = textBoxTextSize.Text;
			data.TextSize = DoubleHelper.ParseInvariantDouble(s);
			LinetypeType type = ((LinetypeType)comboBoxPatterns.SelectedValue).Clone();
			type.TransparencyLevel = trackBarTransparency.Value;

			type.PatternColor = new ColorPersistence(textBoxColor.BackColor);
			s = textBoxScale.Text;
			type.Scale = DoubleHelper.ParseInvariantDouble(s);
			type.Weight = (LineWeight)comboBoxWeight.SelectedValue;
			data.LinetypeType = type.Clone();
			
		}
		private bool CheckData()
		{
			error.Clear();
			bool valid = true;
			string s = textBoxName.Text;
			if (string.IsNullOrEmpty(s))
			{
				error.SetError(textBoxName, "Please enter non empty name!");
				valid = false;
			}
			double d;
			s = textBoxTextSize.Text;
			if (!DoubleHelper.TryParseInvariant(s, out d) || d <= 0)
			{
				string text = "Please enter decimal value!";
				if (d <= 0)
					text = "Text size must be greater than 0!";
				error.SetError(textBoxTextSize, text);
				valid = false;
			}
			if (!(comboBoxPatterns.SelectedValue is LinetypeType))
			{
				error.SetError(comboBoxPatterns, "Please select a hatch");
				valid = false;
			}
			s = textBoxScale.Text;
			if (!DoubleHelper.TryParseInvariant(s, out d) || d < 0.01)
			{
				string text = "Please enter decimal value!";
				if (d <= 0)
					text = "Scale must be greater than 0!";
				error.SetError(textBoxScale, text);
				valid = false;
			}
			if (!(comboBoxWeight.SelectedValue is LineWeight))
			{
				error.SetError(comboBoxWeight, "Please select line weight");
				valid = false;
			}

			
			return valid;
		}
		private void buttonOk_Click(object sender, EventArgs e)
		{
			if (!CheckData())
				return;
			UpdateData();
			DialogResult = DialogResult.OK;
			Close();
		}

		private void buttonColorChange_Click(object sender, EventArgs e)
		{
			colorDialog1.Color = textBoxColor.BackColor; 
			if (colorDialog1.ShowDialog() == DialogResult.OK)
			{
				textBoxColor.BackColor = colorDialog1.Color;
			}
		}

		private void trackBarTransparency_ValueChanged(object sender, EventArgs e)
		{
			labelTransparencyLevel.Text = trackBarTransparency.Value.ToString();
		}
		
	}

	public class SingleLinetypeViewController
	{
		/// <summary>
		/// Returns flag which indicates if data has been updated.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		internal static bool Show(SingleLinetypeData data)
		{
			bool val = new SingleLinetypeView(data).ShowDialog() == DialogResult.OK;
			return val;
		}
	}
}
