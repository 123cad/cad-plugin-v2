﻿using CadPlugin.LinetypeToolbars.Data.TableDatas;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.LinetypeToolbars.Persistence
{
	class DataPersistence
	{
		public static void SaveLinetypeData(IEnumerable<CadPlugin.LinetypeToolbars.SingleLinetypeData> data)
		{
			Properties.LinetypeToolbar.Default.LinetypeDataCollection = data.ToList();
			Properties.LinetypeToolbar.Default.Save();
		}
		public static List<CadPlugin.LinetypeToolbars.SingleLinetypeData> GetLinetypeData()
		{
			try
			{
				return Properties.LinetypeToolbar.Default.LinetypeDataCollection;
			}
			catch
			{
				return null;
			}
		}

		public static void SaveSingleLinetypeDialogHeight(int height)
		{
			Properties.LinetypeToolbar.Default.SingleLinetypeDialogHeight = height;
			Properties.LinetypeToolbar.Default.Save();
		}
		public static int GetSingleLinetypeDialogHeight()
		{
			return Properties.LinetypeToolbar.Default.SingleLinetypeDialogHeight;
		}

		public static void SaveSingleLinetypeDialogWidth(int width)
		{
			Properties.LinetypeToolbar.Default.SingleLinetypeDialogWidth = width;
			Properties.LinetypeToolbar.Default.Save();
		}
		public static int GetSingleLinetypeDialogWidth()
		{
			return Properties.LinetypeToolbar.Default.SingleLinetypeDialogWidth;
		}

		public static void SaveSingleLinetypeDialogPositionX(int positionX)
		{
			Properties.LinetypeToolbar.Default.SingleLinetypeDialogPositionX = positionX;
			Properties.LinetypeToolbar.Default.Save();
		}
		public static int GetSingleLinetypeDialogPositionX()
		{
			return Properties.LinetypeToolbar.Default.SingleLinetypeDialogPositionX;
		}

		public static void SaveSingleLinetypeDialogPositionY(int positionY)
		{
			Properties.LinetypeToolbar.Default.SingleLinetypeDialogPositionY = positionY;
			Properties.LinetypeToolbar.Default.Save();
		}
		public static int GetSingleLinetypeDialogPositionY()
		{
			return Properties.LinetypeToolbar.Default.SingleLinetypeDialogPositionY;
		}
		public static void SaveButtonsPanelCollapsed(bool collapsed)
		{
			Properties.LinetypeToolbar.Default.ButtonsPanelCollapsed = collapsed;
			Properties.LinetypeToolbar.Default.Save();
		}
		public static bool LoadButtonsPanelCollapsed()
		{
			return Properties.LinetypeToolbar.Default.ButtonsPanelCollapsed;
		}

		public static void SaveCsvLocation(string dir)
		{
			Properties.LinetypeToolbar.Default.CsvSaveFolder = dir;
			Properties.LinetypeToolbar.Default.Save();
		}
		public static string LoadCsvLocation()
		{
			return Properties.LinetypeToolbar.Default.CsvSaveFolder;
		}

		public static List<string> GetPrefixHistory()
		{
			List<string> l = new List<string>();
			if (Properties.LinetypeToolbar.Default.UserPrefixHistory != null)
				foreach (string s in Properties.LinetypeToolbar.Default.UserPrefixHistory)
					l.Add(s);
			return l;
		}
		public static void SavePrefixHistory(IList<string> collection)
		{
			StringCollection sc = new StringCollection();
			// Save only limited number of prefix texts.
			int count = collection.Count;
			if (count > 20)
				count = 20;
			for (int i = 0; i < count; i++)
				sc.Add(collection[i]);
			//sc.AddRange(collection.ToArray());
			Properties.LinetypeToolbar.Default.UserPrefixHistory = sc;
			Properties.LinetypeToolbar.Default.Save();
		}

		public static void SaveTableColumns(IEnumerable<TableColumn> columns)
		{
			List<TableColumnsProxy> list = columns.Select(x => 
											new TableColumnsProxy()
											{
												Name = x.Name,
												Id = x.ColumnId,
												Type = (int)x.Column
											}).ToList();
			Properties.LinetypeToolbar.Default.SelectedColumn = list;
			Properties.LinetypeToolbar.Default.Save();
		}
		public static List<TableColumn> LoadTableColumns()
		{
			if (Properties.LinetypeToolbar.Default.SelectedColumn == null)
				return new List<TableColumn>();
			List<TableColumn> columns = Properties.LinetypeToolbar.Default.SelectedColumn.Select(x =>
						new TableColumn(x.Id, (ColumnType)x.Type, x.Name)).ToList();
			return columns;
		}

		public static void SaveShowTableSumRow(bool sumRow)
		{
			Properties.LinetypeToolbar.Default.ShowTableSumRow = sumRow;
			Properties.LinetypeToolbar.Default.Save();
		}
		public static bool LoadShowTableSumRow()
		{
			return Properties.LinetypeToolbar.Default.ShowTableSumRow;
		}


	}

	[Serializable]
	public class TableColumnsProxy
	{
		public string Name { get; set; }
		public string Id { get; set; }
		public int Type { get; set; }
	}
}
