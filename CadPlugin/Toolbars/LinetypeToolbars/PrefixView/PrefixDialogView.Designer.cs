﻿namespace CadPlugin.LinetypeToolbars.PrefixView
{
	partial class PrefixDialogView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonOk = new System.Windows.Forms.Button();
			this.comboBoxSuffix = new System.Windows.Forms.ComboBox();
			this.comboBoxPrefix = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// buttonOk
			// 
			this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonOk.Location = new System.Drawing.Point(118, 48);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(75, 23);
			this.buttonOk.TabIndex = 0;
			this.buttonOk.Text = "Ok";
			this.buttonOk.UseVisualStyleBackColor = true;
			// 
			// comboBoxSuffix
			// 
			this.comboBoxSuffix.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxSuffix.FormattingEnabled = true;
			this.comboBoxSuffix.Items.AddRange(new object[] {
            "km",
            "m",
            "cm",
            "mm"});
			this.comboBoxSuffix.Location = new System.Drawing.Point(151, 12);
			this.comboBoxSuffix.Name = "comboBoxSuffix";
			this.comboBoxSuffix.Size = new System.Drawing.Size(42, 21);
			this.comboBoxSuffix.TabIndex = 1;
			// 
			// comboBoxPrefix
			// 
			this.comboBoxPrefix.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
			this.comboBoxPrefix.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
			this.comboBoxPrefix.FormattingEnabled = true;
			this.comboBoxPrefix.Location = new System.Drawing.Point(12, 12);
			this.comboBoxPrefix.Name = "comboBoxPrefix";
			this.comboBoxPrefix.Size = new System.Drawing.Size(121, 21);
			this.comboBoxPrefix.TabIndex = 2;
			// 
			// PrefixDialogView
			// 
			this.AcceptButton = this.buttonOk;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(205, 83);
			this.ControlBox = false;
			this.Controls.Add(this.comboBoxPrefix);
			this.Controls.Add(this.comboBoxSuffix);
			this.Controls.Add(this.buttonOk);
			this.Name = "PrefixDialogView";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Name";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button buttonOk;
		private System.Windows.Forms.ComboBox comboBoxSuffix;
		private System.Windows.Forms.ComboBox comboBoxPrefix;
	}
}