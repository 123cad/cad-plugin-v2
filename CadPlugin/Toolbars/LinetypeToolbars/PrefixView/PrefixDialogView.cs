﻿//using System;
using CadPlugin.LinetypeToolbars.Persistence;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace CadPlugin.LinetypeToolbars.PrefixView
{
	internal partial class PrefixDialogView : Form
	{
		private Data.Attribute att;
		private List<string> prefixHistory;
		private static bool updatePrefix;
		private static bool updateSuffix;
		private static string prefixName;
		private static string suffixName;
		public static void ShowDialog(Data.Attribute at)
		{
			updatePrefix = false;
			updateSuffix = false;
			prefixName = "";
			suffixName = "";
			PrefixDialogView view = new PrefixDialogView(at);
			view.ShowDialog();
			if (updatePrefix)
				at.SetNewPrefix(prefixName, false);
			if (updateSuffix)
				at.SetNewSuffix(suffixName, false);
			if (updatePrefix || updateSuffix)
				at.Refresh();
		}
		private  PrefixDialogView(Data.Attribute at)
		{
			att = at;
			InitializeComponent();
			if (comboBoxSuffix.Items.Contains(att.Suffix))
				comboBoxSuffix.SelectedItem = att.Suffix;
			else
				comboBoxSuffix.SelectedIndex = 1;
			comboBoxPrefix.Text = att.Prefix;
			prefixHistory = DataPersistence.GetPrefixHistory();
			comboBoxPrefix.AutoCompleteCustomSource = new AutoCompleteStringCollection();
			comboBoxPrefix.AutoCompleteCustomSource.AddRange(prefixHistory.ToArray());
			comboBoxPrefix.Items.AddRange(prefixHistory.ToArray());
			comboBoxSuffix.SelectedIndexChanged += (_, __) =>
			  {
				  updateSuffix = true;
				  suffixName = comboBoxSuffix.Text;
			  };
			comboBoxPrefix.TextChanged += (_, __) =>
			{
				updatePrefix = true;
				prefixName = comboBoxPrefix.Text;
			};
			buttonOk.Click += (_, __) =>
			{
				Close();
			};
			FormClosing += (_, __) =>
			{
				if (Data.Attribute.DefaultSuffixText != comboBoxSuffix.Text)
					Data.Attribute.DefaultSuffixText = comboBoxSuffix.Text;
				if (prefixHistory == null)
					return;
				if (!prefixHistory.Contains(comboBoxPrefix.Text) && comboBoxPrefix.Text != "")
					prefixHistory.Insert(0, comboBoxPrefix.Text);
				DataPersistence.SavePrefixHistory(prefixHistory);
			};
			Shown += (_, __) =>
			{
				comboBoxPrefix.Focus();
				comboBoxPrefix.SelectionStart = comboBoxPrefix.Text.Length;
			};

			InitializeComboBoxDeleteItems();
		}
		private void InitializeComboBoxDeleteItems()
		{
			comboBoxPrefix.SelectedValueChanged += (_, __) =>
			{
				if (Keyboard.IsKeyDown(Key.Delete))
				{
					string value = (string)comboBoxPrefix.Text;
					comboBoxPrefix.AutoCompleteCustomSource.Remove(value);
					comboBoxPrefix.Items.Remove(value);
					prefixHistory.Remove(value);
					comboBoxPrefix.DroppedDown = true;
				}
			};
		}
	}
}
