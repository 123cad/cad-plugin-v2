﻿using CadPlugin.LinetypeToolbars;
using CadPlugin.Toolbars.ToolbarExportImportSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace CadPlugin.Toolbars.LinetypeToolbars.ImportExports
{
	class ImportExportManager
	{
		private static string XMLGlobalRootName = "LinetypeToolbar";
		public static void Export(List<SingleLinetypeData> data)
		{
			SaveFileDialog saveDlg = new SaveFileDialog();
			saveDlg.OverwritePrompt = true;
			saveDlg.Filter = "XML files (*.xml)|*.xml| All files (*.*)|*.*";
			saveDlg.FileName = "LängentoolbarMaske.xml";
			if (saveDlg.ShowDialog() == DialogResult.OK)
			{
				ExportImportManager mgr = new ExportImportManager(XMLGlobalRootName, new List<Type>() { typeof(SingleLinetypeData) });
				{
					mgr.SaveData(data, saveDlg.FileName);
				}
			}
		}
		public static List<SingleLinetypeData> Import()
		{
			OpenFileDialog openDlg = new OpenFileDialog();
			openDlg.CheckFileExists = true;
			openDlg.Filter = "XML files (*.xml)|*.xml| All files (*.*)|*.*";
			List<SingleLinetypeData> data = null;
			if (openDlg.ShowDialog() == DialogResult.OK)
			{
				ExportImportManager mgr = new ExportImportManager(XMLGlobalRootName, new List<Type>() { typeof(SingleLinetypeData) });
				try
				{
					List<object> obj = mgr.LoadData(openDlg.FileName);
					if (obj != null)
						data = obj.Select(x=>(SingleLinetypeData)x).ToList();
				}
				catch (XmlException xmlE)
				{
					System.Diagnostics.Debug.WriteLine("Invalid linetype file!" + xmlE.Message);
					// Show dialog.
					MessageBox.Show("Invalid file format!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return null;
				}
			}
			return data;
		}
	}
}
