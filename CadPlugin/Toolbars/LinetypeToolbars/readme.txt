﻿Flaeche block contains area and attribute.
Area is represented by polyline (main polyline) and hatch inside it.
Attribute is represented by polyline, hatch inside it and text.
Table is represented with group of objects, where rows are related to attributes.

LINETYPE and HATCH toolbars are same (code is copied and adapted to linetype [no hatch inside area])!