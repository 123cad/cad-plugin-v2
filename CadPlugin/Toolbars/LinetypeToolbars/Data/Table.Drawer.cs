﻿using system = System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.LinetypeToolbars.Data
{
	partial class Table
	{
		public static double TextSize = 1;
		/// <summary>
		/// Iterates through table entities and checks if everything is valid.
		/// If entity has been deleted, it is recreated.
		/// </summary>
		private void CheckAllEntities()
		{
			// When Entities are missing, they are recreated.
			// Only objects which could be deleted by user are checked, errors in nod table structure is not verified 
			// (it must exist).
			using (Transaction tr = Transaction.Database.TransactionManager.StartTransaction())
			{
				TransactionData data = TransactionData.Create(Transaction.Document, tr);
				DBDictionary table = tr.GetObject(nodEntry.ObjectId, OpenMode.ForWrite) as DBDictionary;
				if (table == null)
					system.Diagnostics.Debug.WriteLine("Table checking invoked on non table");
				string tableName = Id;

				CheckBoundary(tr, data, table, tableName);
				CheckHeader(tr, data, table, tableName);
				CheckColumnsHeader(tr, data, table, tableName, tableSettings);
				CheckRows(tr, data, table, tableName, tableSettings);
				CheckRowSum(tr, data, table, tableName, tableSettings);

				tr.Commit();
			}
		}
		private static void CheckRowSum(Transaction tr, TransactionData data, DBDictionary table, string tableName, TableDatas.TableSettings tableSettings)
		{
			TypedValue[] values;
			bool objectExists = false;
			bool forceCreate = false; // Object are created new.
			if (!table.Contains(ToolbarNodManager.SumRow))
				return;
			DBDictionary dic = tr.GetObject(table.GetAt(ToolbarNodManager.SumRow), OpenMode.ForRead) as DBDictionary;
			if (!dic.Contains(ToolbarNodManager.SumRowText))
				return;
			Xrecord rec = tr.GetObject(dic.GetAt(ToolbarNodManager.SumRowText), OpenMode.ForWrite) as Xrecord;
			values = rec.Data != null ? rec.Data.AsArray() : null;
			if (values== null || values.Length < tableSettings.TotalColumnsCount)
			{
				forceCreate = true;
				values = new TypedValue[tableSettings.TotalColumnsCount];
			}
			for (int i = 0; i < tableSettings.TotalColumnsCount; i++)
			{
				objectExists = false;
				if (!forceCreate)
				{
					ObjectId oid = values[i].GetObjectId(data.Database);
					if (oid != ObjectId.Null)
					{
						Entity ent = tr.GetObject(oid, OpenMode.ForRead) as Entity;
						if (ent != null)
							objectExists = true;
					}
				}
				if (forceCreate || !objectExists)
				{
					MText text = tr.CreateEntity<MText>(data.ModelSpace);
					text.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId, text.ObjectId.Handle.ConvertToString());
					text.TextHeight = TextSize;
					text.SaveExtensionDataString(tr, ExtensionDictionaryEntries.TableId, tableName);
					text.Contents = "Σ";
					values[i] = text.ObjectId.ToTypedValue();
				}
			}
			rec.Data = new ResultBuffer(values);

			objectExists = false;
			rec = tr.GetObject(dic.GetAt(ToolbarNodManager.SumRowLine), OpenMode.ForRead) as Xrecord;
			values = rec.Data != null ? rec.Data.AsArray() : null;
			if (values != null && values.Length > 0)
			{
				ObjectId oid = values[0].GetObjectId(data.Database);
				Entity ent = tr.GetObject(oid, OpenMode.ForRead) as Entity;
				if (ent != null)
					objectExists = true;
			}
			else
				values = new TypedValue[1];
			if (!objectExists)
			{
				Line l = tr.CreateEntity<Line>(data.ModelSpace);
				l.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId, l.ObjectId.Handle.ConvertToString());
				l.SaveExtensionDataString(tr, ExtensionDictionaryEntries.TableId, tableName);
				values[0] = l.ObjectId.ToTypedValue();

				rec.UpgradeOpen();
				rec.Data = new ResultBuffer(values);
			}			
		}
		private static void CheckRows(Transaction tr, TransactionData data, DBDictionary table, string tableName, TableDatas.TableSettings tableSettings)
		{
			TypedValue[] values;
			bool objectExists = false;
			DBDictionary rows = tr.GetObject(table.GetAt(ToolbarNodManager.Rows), OpenMode.ForRead) as DBDictionary;
			Xrecord order = tr.GetObject(rows.GetAt(ToolbarNodManager.RowsOrder), OpenMode.ForRead) as Xrecord;
			string[] rowNames = order.Data != null ? order.Data.AsArray().Select(x => (string)x.Value).ToArray() : null;
			if (rowNames != null)
				foreach (string name in rowNames)
				{
					DBDictionary row = tr.GetObject(rows.GetAt(name), OpenMode.ForRead) as DBDictionary;
					Xrecord text = tr.GetObject(row.GetAt(ToolbarNodManager.RowText), OpenMode.ForWrite) as Xrecord;
					values = text.Data != null ? text.Data.AsArray() : null;
					bool forceCreate = false;
					if (values == null || values.Length == 0)
					{
						forceCreate = true;
						values = new TypedValue[tableSettings.TotalColumnsCount];
					}
					for (int i = 0; i < tableSettings.TotalColumnsCount; i++)
					{
						bool exists = false;
						if (!forceCreate)
						{
							Entity ent = GetEntity(data, values[i]);
							if (ent != null && ent is MText)
								exists = true;
						}
						if (forceCreate || !exists)
						{
							MText txt = tr.CreateEntity<MText>(data.ModelSpace);
							txt.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId, txt.ObjectId.Handle.ConvertToString());
							txt.TextHeight = TextSize;
							txt.SaveExtensionDataString(tr, ExtensionDictionaryEntries.TableId, tableName);
							txt.SaveExtensionDataString(tr, ExtensionDictionaryEntries.AttributeTextLinkId, name);

							values[i] = txt.ObjectId.ToTypedValue();
						}
					}
					text.Data = new ResultBuffer(values);

					Xrecord line = tr.GetObject(row.GetAt(ToolbarNodManager.RowLine), OpenMode.ForRead) as Xrecord;
					values = line.Data != null ? line.Data.AsArray() : null;
					objectExists = false;
					if (values != null && values.Length != 0)
					{
						Entity ent = GetEntity(data, values[0]);
						if (ent != null && ent is Line)
							objectExists = true;
					}
					if (!objectExists)
					{
						Line l = tr.CreateEntity<Line>(data.ModelSpace);
						l.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId, l.ObjectId.Handle.ConvertToString());
						l.SaveExtensionDataString(tr, ExtensionDictionaryEntries.TableId, tableName);

						line.UpgradeOpen();
						line.Data = new ResultBuffer(l.ObjectId.ToTypedValue());
					}
				}
		}

		private static void CheckColumnsHeader(Transaction tr, TransactionData data, DBDictionary table, string tableName, TableDatas.TableSettings tableSetting)
		{
			TypedValue[] values;
			bool objectExists = false;
			DBDictionary columnsHeader = tr.GetObject(table.GetAt(ToolbarNodManager.TableColumns), OpenMode.ForRead) as DBDictionary;
			{
				IList<TableDatas.TableColumn> columns = tableSetting.GetAllColumns();
									
				objectExists = false;
				bool forceCreate = false; // Object are created new.
				for (int i = 0; i < tableSetting.TotalColumnsCount; i++)
				{
					TableDatas.TableColumn tc = columns[i];
					Xrecord column = tr.GetObject(columnsHeader.GetAt(tc.ColumnId), OpenMode.ForWrite) as Xrecord;
					values = column.Data != null ? column.Data.AsArray() : null;
					bool updateData = false;
					forceCreate = false;
					if (values == null || values.Length == 0)
					{
						forceCreate = true;
						// Last one doesn't contain line.
						values = new TypedValue[i < columns.Count - 1 ? 4 : 3];
					}

					objectExists = false;
					if (!forceCreate)
					{
						Entity ent = GetEntity(data, values[0]);
						if (ent != null && ent is MText)
							objectExists = true;

					}
					if (forceCreate || !objectExists)
					{
						MText text = tr.CreateEntity<MText>(data.ModelSpace);
						text.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId, text.ObjectId.Handle.ConvertToString());
						text.TextHeight = TextSize;
						text.SaveExtensionDataString(tr, ExtensionDictionaryEntries.TableId, tableName);
						text.Contents = tc.Name;

						values[0] = text.ObjectId.ToTypedValue();
						updateData = true;
					}

					values[1] = new TypedValue((int)DxfCode.Int32, (int)tc.Column);
					values[2] = new TypedValue((int)DxfCode.Text, tc.Name);

					objectExists = false;
					if (i < columns.Count - 1)
					{
						if (!forceCreate)
						{
							Entity ent = GetEntity(data, values[3]);
							if (ent != null && ent is Line)
								objectExists = true;
						}
						if (forceCreate || !objectExists)
						{
							Line l = tr.CreateEntity<Line>(data.ModelSpace);
							l.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId, l.ObjectId.Handle.ConvertToString());
							l.SaveExtensionDataString(tr, ExtensionDictionaryEntries.TableId, tableName);

							values[3] = l.ObjectId.ToTypedValue();
							updateData = true;
						}
					}
					if (updateData)
						column.Data = new ResultBuffer(values);

				}
				Xrecord lineRec = tr.GetObject(columnsHeader.GetAt(ToolbarNodManager.HeaderLine), OpenMode.ForRead) as Xrecord;
				values = lineRec.Data != null ? lineRec.Data.AsArray() : null;
				objectExists = false;
				if (values != null && values.Length != 0)
				{
					Entity ent = GetEntity(data, values[0]);
					if (ent != null && ent is Line)
						objectExists = true;
				}
				if (!objectExists)
				{
					Line above = tr.CreateEntity<Line>(data.ModelSpace);
					above.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId, above.ObjectId.Handle.ConvertToString());
					above.SaveExtensionDataString(tr, ExtensionDictionaryEntries.TableId, tableName);

					lineRec.UpgradeOpen();
					lineRec.Data = new ResultBuffer(above.ObjectId.ToTypedValue());
					lineRec.DowngradeOpen();
				}

			}
		}

		private static void CheckHeader(Transaction tr, TransactionData data, DBDictionary table, string tableName)
		{
			bool objectExists = false;
			TypedValue[] values;
			Xrecord headerRec = tr.GetObject(table.GetAt(ToolbarNodManager.TableHeader), OpenMode.ForRead) as Xrecord;
			values = headerRec.Data != null ? headerRec.Data.AsArray() : null;
			if (values != null && values.Length != 0)
			{
				Entity ent = GetEntity(data, values[0]);
				if (ent != null && ent is MText)
					objectExists = true;
			}
			if (!objectExists)
			{
				MText text = tr.CreateEntity<MText>(data.ModelSpace);
				text.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId, text.ObjectId.Handle.ConvertToString());
				text.TextHeight = TextSize;
				text.Contents = "Längenauszug";
				text.SaveExtensionDataString(tr, ExtensionDictionaryEntries.TableId, tableName);

				headerRec.UpgradeOpen();
				headerRec.Data = new ResultBuffer(text.ObjectId.ToTypedValue(), new TypedValue((int)DxfCode.Text, text.Contents));
				headerRec.DowngradeOpen();
			}
		}

		/// <summary>
		/// Checks boundary of table.
		/// </summary>
		private static void CheckBoundary(Transaction tr, TransactionData data, DBDictionary table, string tableName)
		{
			bool objectExists = false;
			TypedValue[] values;
			Xrecord boundaryRec = tr.GetObject(table.GetAt(ToolbarNodManager.TableBoundary), OpenMode.ForRead) as Xrecord;
			Polyline boundary = null;
			values = boundaryRec.Data != null ? boundaryRec.Data.AsArray() : null;
			if (values != null && values.Length != 0)
			{
				Entity ent = GetEntity(data, values[0]);
				if (ent != null && ent is Polyline)
				{
					objectExists = true;
					boundary = (Polyline)ent;
				}
			}
			if (!objectExists)
			{
				boundary = tr.CreateEntity<Polyline>(data.ModelSpace);
				boundary.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId, boundary.ObjectId.Handle.ConvertToString());
				boundary.Closed = true;
				boundary.SaveExtensionDataString(tr, ExtensionDictionaryEntries.TableId, tableName);

				boundaryRec.UpgradeOpen();
				boundaryRec.Data = new ResultBuffer(boundary.ObjectId.ToTypedValue());
			}
			if (boundary.NumberOfVertices != 4)
			{
				while (boundary.NumberOfVertices < 4) boundary.AddVertexAt(0, new Point2d(), 0, 0, 0);
				while (boundary.NumberOfVertices > 4) boundary.RemoveVertexAt(0);
				for (int i = 0; i < 4; i++) boundary.SetPointAt(i, new Point2d());
			}

		}


		/// <summary>
		/// Returns entity for which handle is saved in TypedValue.
		/// </summary>
		private static Entity GetEntity(TransactionData data, TypedValue val)
		{
			try
			{
				ObjectId oid = val.GetObjectId(data.Database);
				Entity ent = data.Transaction.GetObject(oid, OpenMode.ForWrite) as Entity;
				return ent;
			}
			catch (Exception)
			{
				return null;
			}
		}

		public void Refresh()
		{
			CheckAllEntities();
			Transaction tr = Transaction.Transaction;

			__Entities = new LoadedTableEntities();
			{
				TypedValue[] values;
				Xrecord rec = tr.GetObject(nodEntry.GetAt(ToolbarNodManager.TableBoundary), OpenMode.ForRead) as Xrecord;
				values = rec.Data.AsArray();
				Entities.Boundary = tr.GetObject(values[0].GetObjectId(Transaction.Database), OpenMode.ForWrite) as Polyline;
				//rec.Dispose();

				rec = tr.GetObject(nodEntry.GetAt(ToolbarNodManager.TableHeader), OpenMode.ForRead) as Xrecord;
				values = rec.Data.AsArray();
				Entities.Header = tr.GetObject(values[0].GetObjectId(Transaction.Database), OpenMode.ForWrite) as MText;
				//rec.Dispose();

				DBDictionary dic = tr.GetObject(nodEntry.GetAt(ToolbarNodManager.TableColumns), OpenMode.ForRead) as DBDictionary;
				{
					int count = 0;
					IList<TableDatas.TableColumn> columns = tableSettings.GetAllColumns();
					foreach(TableDatas.TableColumn tc in columns)
					{
						rec = tr.GetObject(dic.GetAt(tc.ColumnId), OpenMode.ForRead) as Xrecord;
						values = rec.Data.AsArray();
						Entities.ColumnHeaderTexts.Add(tr.GetObject(values[0].GetObjectId(Transaction.Database), OpenMode.ForWrite) as MText);
						// Last column doesn't have a line.
						if (count + 1 < columns.Count)
							Entities.ColumnLines.Add(tr.GetObject(values[3].GetObjectId(Transaction.Database), OpenMode.ForWrite) as Line);
						count++;
					}					
				}
				rec = tr.GetObject(dic.GetAt(ToolbarNodManager.HeaderLine), OpenMode.ForRead) as Xrecord;
				values = rec.Data.AsArray();
				Entities.HeaderLine = tr.GetObject(values[0].GetObjectId(Transaction.Database), OpenMode.ForWrite) as Line;

				DBDictionary rowsDic = tr.GetObject(nodEntry.GetAt(ToolbarNodManager.Rows), OpenMode.ForRead) as DBDictionary;
				rec = tr.GetObject(rowsDic.GetAt(ToolbarNodManager.RowsOrder), OpenMode.ForRead) as Xrecord;

				bool sumSet = false;
				string[] rowIds = rec.Data != null? rec.Data.AsArray().Select(x => (string)x.Value).ToArray() : null;
				double[] columnsSum = new double[tableSettings.Columns.Count];
				if (rowIds != null)
					foreach (string rowId in rowIds)
					{
						dic = tr.GetObject(rowsDic.GetAt(rowId), OpenMode.ForRead) as DBDictionary;
						LoadedRow r = new LoadedRow(Transaction, rowId, dic, tableSettings);
						Entities.Rows.Add(r);
						for (int i = 0; i < columnsSum.Length; i++)
						{
							double d = 0;
							bool valid = RowData.GetValueFromType(r.Attribute, tableSettings.Columns[i].Column, ref d);
							if (!valid)
								continue;
							columnsSum[i] += d;
						}
						sumSet = true;
					}

				if (nodEntry.Contains(ToolbarNodManager.SumRow))
				{
					DBDictionary sumRowDic = tr.GetObject(nodEntry.GetAt(ToolbarNodManager.SumRow), OpenMode.ForRead) as DBDictionary;
					if (sumRowDic != null)
					{
						if (sumRowDic.Contains(ToolbarNodManager.SumRowText))
						{
							Entities.RowSumTexts.Clear();
							rec = tr.GetObject(sumRowDic.GetAt(ToolbarNodManager.SumRowText), OpenMode.ForRead) as Xrecord;
							values = rec.Data.AsArray();
							//UPDATE value can exist only if values for column are existing.
							for (int i = 0; i < tableSettings.TotalColumnsCount; i++)
							{
								Entities.RowSumTexts.Add(tr.GetObject(values[i].GetObjectId(Transaction.Database), OpenMode.ForWrite) as MText);
								if (i == 0)
									continue;// Not sure why? Where is text for header column set?
								string content = "-";
								if (sumSet)
									content = DoubleHelper.ToStringInvariant(columnsSum[i - 1], 2);
								Entities.RowSumTexts.Last().Contents = content;
							}

							rec = tr.GetObject(sumRowDic.GetAt(ToolbarNodManager.SumRowLine), OpenMode.ForRead) as Xrecord;
							values = rec.Data.AsArray();
							Entities.RowSumLine = tr.GetObject(values[0].GetObjectId(Transaction.Database), OpenMode.ForWrite) as Line;
						}
					}
				}
			}

			double[] maxColumnsWidth = new double[tableSettings.TotalColumnsCount];
			Extents3d extents;
			for (int i = 0; i < tableSettings.TotalColumnsCount; i++)
			{
				extents = Entities.ColumnHeaderTexts[i].GeometricExtents;
				double w = extents.MaxPoint.X - extents.MinPoint.X;
				maxColumnsWidth[i] = w;
			}
			foreach (LoadedRow row in Entities.Rows)
			{
				for (int i = 0; i < tableSettings.TotalColumnsCount; i++)
				{
					extents = row.Texts[i].GeometricExtents;
					double w = extents.MaxPoint.X - extents.MinPoint.X;
					if (maxColumnsWidth[i] < w)
						maxColumnsWidth[i] = w;
				}
			}
			double deltaX = Entities.Header.TextHeight;
			double deltaY = Entities.Header.TextHeight;
			Vector2d deltaVector2d = new Vector2d(deltaX, -deltaY);
			Vector3d deltaVector3d = new Vector3d(deltaX, -deltaY, 0);

			// Set table header.
			double tableWidth = maxColumnsWidth.Sum() + 2 * tableSettings.TotalColumnsCount * deltaX;
			Point2d start = new Point2d(Entities.Header.Location.X, Entities.Header.Location.Y);
			Point2d upperLeft = start.Add(new Vector2d(-deltaX, deltaY));
			Entities.Boundary.SetPointAt(0, upperLeft);
			Entities.Boundary.SetPointAt(1, upperLeft.Add(new Vector2d(tableWidth, 0)));
			double rowHeight = Entities.Header.TextHeight + 2 * deltaY;

			// Set column headers.
			Point2d currentRowUpperLeft = upperLeft.Add(new Vector2d(0, -rowHeight));
			Entities.HeaderLine.StartPoint = new Point3d(currentRowUpperLeft.X, currentRowUpperLeft.Y, 0);
			Entities.HeaderLine.EndPoint = Entities.HeaderLine.StartPoint.Add(new Vector3d(tableWidth, 0, 0));
			Point3d currentCellUpperLeft = new Point3d(currentRowUpperLeft.X, currentRowUpperLeft.Y, 0);
			rowHeight = 0;
			for (int i = 0; i < tableSettings.TotalColumnsCount; i++)
			{
				double w = Entities.ColumnHeaderTexts[i].TextHeight + 2 * deltaY;
				if (rowHeight < w)
					rowHeight = w;
				Entities.ColumnHeaderTexts[i].Location = currentCellUpperLeft.Add(deltaVector3d);
				currentCellUpperLeft = currentCellUpperLeft.Add(new Vector3d(maxColumnsWidth[i] + 2 * deltaX, 0, 0));
				if (i + 1 < tableSettings.TotalColumnsCount)
					Entities.ColumnLines[i].StartPoint = currentCellUpperLeft;
			}

			// Set rows.
			currentRowUpperLeft = currentRowUpperLeft.Add(new Vector2d(0, -rowHeight));
			foreach (LoadedRow row in Entities.Rows)
			{
				currentCellUpperLeft = new Point3d(currentRowUpperLeft.X, currentRowUpperLeft.Y, 0);
				row.Line.StartPoint = currentCellUpperLeft;
				row.Line.EndPoint = currentCellUpperLeft.Add(new Vector3d(tableWidth, 0, 0));
				rowHeight = 0;
				for (int i = 0; i < tableSettings.TotalColumnsCount; i++)
				{
					MText cellText = row.Texts[i];
					double w = cellText.TextHeight + 2 * deltaY;
					if (rowHeight < w)
						rowHeight = w;
					cellText.Location = currentCellUpperLeft.Add(deltaVector3d);
					currentCellUpperLeft = currentCellUpperLeft.Add(new Vector3d(maxColumnsWidth[i] + 2 * deltaX, 0, 0));
				}
				currentRowUpperLeft = currentRowUpperLeft.Add(new Vector2d(0, -rowHeight));
			}

			// Set row sum.
			if (Entities.RowSumTexts.Count > 0)
			{
				currentCellUpperLeft = new Point3d(currentRowUpperLeft.X, currentRowUpperLeft.Y, 0);
				Entities.RowSumLine.StartPoint = currentCellUpperLeft;
				Entities.RowSumLine.EndPoint = currentCellUpperLeft.Add(new Vector3d(tableWidth, 0, 0));
				rowHeight = 0;
				for (int i = 0; i < tableSettings.TotalColumnsCount; i++)
				{
					MText cellText = Entities.RowSumTexts[i];
					cellText.Location = currentCellUpperLeft.Add(deltaVector3d);
					double w = cellText.TextHeight + 2 * deltaY;
					if (rowHeight < w)
						rowHeight = w;
					currentCellUpperLeft = currentCellUpperLeft.Add(new Vector3d(maxColumnsWidth[i] + 2 * deltaX, 0, 0));
				}

				currentRowUpperLeft = currentRowUpperLeft.Add(new Vector2d(0, -rowHeight));
			}

			Entities.Boundary.SetPointAt(2, new Point2d(currentRowUpperLeft.X + tableWidth, currentRowUpperLeft.Y));
			Entities.Boundary.SetPointAt(3, new Point2d(currentRowUpperLeft.X, currentRowUpperLeft.Y));
			currentCellUpperLeft = new Point3d(currentRowUpperLeft.X, currentRowUpperLeft.Y, 0);
			for (int i = 0; i < tableSettings.TotalColumnsCount - 1; i++)
			{
				currentCellUpperLeft = currentCellUpperLeft.Add(new Vector3d(maxColumnsWidth[i] + 2 * deltaX, 0, 0));
				Entities.ColumnLines[i].EndPoint = currentCellUpperLeft;
			}
		}
		/// <summary>
		/// Row data extractor for attribute.
		/// </summary>
		private class RowData
		{
			/// <summary>
			/// Returns if value is valid.
			/// </summary>
			/// <param name="at"></param>
			/// <param name="type"></param>
			/// <param name="value"></param>
			/// <returns></returns>
			public static bool GetValueFromType(Attribute at, TableDatas.ColumnType type, ref double value)
			{
				bool valid = true;
				switch(type)
				{
					case TableDatas.ColumnType.Area:
						value = at.Area.Boundary.Area;
						break;
					case TableDatas.ColumnType.Length:
						value = at.Area.Boundary.Length;
						break;
					case TableDatas.ColumnType.Hoehe:
					case TableDatas.ColumnType.Breite:
						double hoehe = 0;
						double breite = 0;
						List<Point3d> points = new List<Point3d>();
						for (int i = 0; i < at.Area.Boundary.NumberOfVertices; i++)
							points.Add(at.Area.Boundary.GetPoint3dAt(i));
						valid = HoheBreiteCalculator.Calculate(ref hoehe, ref breite, points.Select(x => new MyUtilities.Geometry.Point3d(x.X, x.Y, x.Z)).ToArray());
						if (type == TableDatas.ColumnType.Hoehe)
							value = hoehe;
						else
							value = breite;
						break;
				}

				return valid;
			}
		}
		private class LoadedRow : system.IDisposable
		{
			public Attribute Attribute { get; private set; }
			public Line Line { get; private set; }
			public List<MText> Texts { get; private set; }
			public string Id { get; private set; }
			public LoadedRow(TransactionData data, string id, DBDictionary rowDic, TableDatas.TableSettings settings)
			{
				Id = id;
				Texts = new List<MText>();
				Transaction tr = data.Transaction;
				TypedValue[] values;
				Handle att = id.ConvertToHandle().Value;// new Handle(long.Parse(id));
				ObjectId attId = data.Database.GetObjectId(false, att, 0);
				Attribute = data.ObjectManager.GetAttribute(attId);

				Xrecord rec = tr.GetObject(rowDic.GetAt(ToolbarNodManager.RowLine), OpenMode.ForWrite) as Xrecord;
				values = rec.Data.AsArray();
				ObjectId oid = values[0].GetObjectId(data.Database);
				Line = tr.GetObject(oid, OpenMode.ForWrite) as Line;
				
				for (int i = 0; i < settings.TotalColumnsCount; i++)
				{
					rec = tr.GetObject(rowDic.GetAt(ToolbarNodManager.RowText), OpenMode.ForWrite) as Xrecord;
					values = rec.Data.AsArray();
					oid = values[i].GetObjectId(data.Database);
					Texts.Add(tr.GetObject(oid, OpenMode.ForWrite) as MText);
				}

				Texts[0].Contents = Attribute.Prefix;
				for (int i = 0; i < settings.ValueColumnsCount; i++)
				{
					TableDatas.TableColumn tc = settings.Columns[i];
					double d = 0;
					bool valid = RowData.GetValueFromType(Attribute, tc.Column, ref d);
					if (!valid)
						continue;
					Texts[i + 1].Contents = DoubleHelper.ToStringInvariant(d, 2);
				}
			}
			public void Dispose()
			{
				Line.Dispose();
				foreach (var v in Texts)
					v.Dispose();
			}
			public IEnumerable<Entity> GetAllEntities()
			{
				List<Entity> entities = new List<Entity>();
				entities.Add(Line);
				entities.AddRange(Texts);
				return entities;
			}
		}
	}
}
