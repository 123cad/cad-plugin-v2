﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.LinetypeToolbars.Data
{
	class Area
	{
		/// <summary>
		/// ExtensionDictionary name used to reference Area.
		/// </summary>
		public static readonly string ExtensionId = ExtensionDictionaryEntries.AreaPolylineLinkId;
		public TransactionData Transaction { get; private set; }
		public Attribute Attribute { get; set; }
		public Polyline Boundary { get; private set; }
		public Entity MainEntity { get { return Boundary; } }
		/// <summary>
		/// Returns ObjectId for text object of attribute.
		/// </summary>
		/// <returns></returns>
		public ObjectId GetAttributeId()
		{
			if (Boundary != null)
			{
				ObjectId? oid = Boundary.LoadFromExtensionDataObjectId(Transaction.Transaction, Transaction.Database, 
											Attribute.ExtensionId);
				if (oid.HasValue)
					return oid.Value;
			}
			return ObjectId.Null;
		}

		/// <summary>
		/// Assigns new attribute to this Area.
		/// </summary>
		public void AddNewAttribute(Attribute at)
		{
			if (at.Text == null)
				throw new ArgumentException("Attribute does not contain Text object!");
			ObjectId atIdStored = GetAttributeId();
			if (atIdStored != ObjectId.Null)
				throw new InvalidOperationException("Adding new attribute when there is existing one!");
			Attribute = at;
			if (!Boundary.IsWriteEnabled)
				Boundary.UpgradeOpen();
			Boundary.SaveExtensionDataObjectId(Transaction.Transaction, at.MainEntity.ObjectId, 
										Attribute.ExtensionId);
		}
		/// <summary>
		/// Just sets attribute reference (and checks if provided Attribute is the one referenced by the Area)
		/// </summary>
		public void AddExistingAttribute(Attribute at)
		{
			if (!(Boundary.LoadFromExtensionDataObjectId(
						Transaction.Transaction, 
						Transaction.Database, 
						Attribute.ExtensionId).Value == at.Text.ObjectId))
			{
				throw new ArgumentException("Attribute provided to Area is not the one referenced by the Area");
			}
			Attribute = at;
		}
		/// <summary>
		/// Removes the attribute from 
		/// </summary>
		public void DeleteAttribute()
		{

			if (Attribute != null)
				Attribute.Delete();
			if (Boundary.ExtensionDictionary != ObjectId.Null)
			{
				if (!Boundary.IsWriteEnabled)
					Boundary.UpgradeOpen();
				Boundary.RemoveFromExtensionData(Transaction.Transaction, 
													Attribute.ExtensionId,
													ExtensionDictionaryEntries.ObjectGroupTypeDescription);

			}
		}

		/// <summary>
		/// Returns all entities which are displayed in Area (attribute not included).
		/// </summary>
		/// <returns></returns>
		public IEnumerable<Entity> GetAllEntities()
		{
			List<Entity> entities = new List<Entity>();
			entities.Add(Boundary);
			return entities;
		}

		/// <summary>
		/// Determines ONLY if entity type is used in Area.
		/// </summary>
		public static bool IsAreaObjectType(Entity ent)
		{
			return ent is Polyline;
		}
		public static bool IsAreaEntity(TransactionData data, Entity ent)
		{
			var v = GetMainObject(data, ent, true);
			return v != null;
		}

		/// <summary>
		/// Creates area for specified boundary.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="boundary"></param>
		/// <returns></returns>
		public static Area Create(TransactionData data, Polyline boundary, SingleLinetypeData linetypeData)
		{
			Area a = new Area();
			a.Transaction = data;
			a.Boundary = boundary;
			if (!boundary.IsWriteEnabled)
				boundary.UpgradeOpen();
			a.Boundary.SaveExtensionDataString(data.Transaction, 
												ExtensionDictionaryEntries.ObjectGroupTypeDescription, 
												ExtensionDictionaryEntries.DescriptionAreaPolyline);
			a.Boundary.SaveExtensionDataString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId, a.Boundary.ObjectId.Handle.ConvertToString());
			linetypeData.ApplyPattern(a.Boundary);
						
			return a;
		}

		/// <summary>
		/// Returns main object (polyline). Any area object can be provided.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="ent"></param>
		/// <returns></returns>
		public static Polyline GetMainObject(TransactionData data, Entity ent, bool openErased)
		{
			if (ent is Polyline)
			{
				if (ent.LoadExtensionString(data.Transaction,
					ExtensionDictionaryEntries.ObjectGroupTypeDescription) == ExtensionDictionaryEntries.DescriptionAreaPolyline)
					return ent as Polyline;
			}			
			return null;
		}

		/// <summary>
		/// Loads existing area for entity (hatch or polyline). Returns null if Boundary does not exist.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="ent"></param>
		/// <returns></returns>
		public static Area Load(TransactionData data, Entity ent, bool openErased = false)
		{
			Polyline polyline = GetMainObject(data, ent, openErased);
			if (polyline == null)
				return null;

			{

				// If handle of entity does not match to the one written in ExtensionDictionary, 
				// object has been copied and attribute entris from ExtensionDictionary are deleted.
				string[] atts = new string[]
				{
					ExtensionDictionaryEntries.AreaPolylineLinkId,
					ExtensionDictionaryEntries.ObjectGroupTypeDescription,
					ExtensionDictionaryEntries.ObjectGroupTypeDescription,
					Attribute.ExtensionId
				};
				bool valid = true;
				string handle = polyline.ObjectId.Handle.ConvertToString();
				if (polyline.LoadExtensionString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId) != handle)
				{

					polyline.RemoveFromExtensionData(data.Transaction, atts);
					System.Diagnostics.Debug.WriteLine("Removed from ExtensionDictionary! (attribute not found in area). id= " + ent.ObjectId);
					valid = false;
				}
				if (polyline != ent)
				{
					handle = ent.ObjectId.Handle.ConvertToString();
					if (ent.LoadExtensionString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId) != handle)
					{
						ent.RemoveFromExtensionData(data.Transaction, atts);
						valid = false;
					}
				}
				if (!valid)
					return null;
			}

			Area a = new Area();
			a.Transaction = data;
			a.Boundary = polyline;
			Dictionary<string, TypedValue[]> values = polyline.LoadExtensionData(data.Transaction, ExtensionDictionaryEntries.AreaPolylineLinkId);
			if (values.ContainsKey(ExtensionDictionaryEntries.AreaPolylineLinkId))
			{
				//CHECK - doesn't make sense!!!
				TypedValue pl = values[ExtensionDictionaryEntries.AreaPolylineLinkId][0];
				if (pl != null)
				{
					ObjectId plId = pl.GetObjectId(data.Database);
					if (plId != ObjectId.Null)
						a.Boundary = data.Transaction.GetObject(plId, OpenMode.ForRead, openErased) as Polyline;
				}
			}
			

			if (a.Boundary == null)
				a = null;
			return a;
		}
		public override bool Equals(object obj)
		{
			Area ar = obj as Area;
			if (ar == null)
				return false;
			return ar.MainEntity.ObjectId.Handle == MainEntity.ObjectId.Handle;
		}
		public override int GetHashCode()
		{
			return MainEntity.ObjectId.Handle.Value.GetHashCode();
		}
		public override string ToString()
		{
			return "Area: " + MainEntity.ObjectId;
		}
	}
}
