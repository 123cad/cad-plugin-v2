﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.LinetypeToolbars.Data;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.LinetypeToolbars
{
	public class SingleLinetypeData
	{
		public string Name { get; set; }

		public double TextSize { get; set; }
		public LinetypeType LinetypeType { get; set; }
		public SingleLinetypeData()
		{
			Name = "[Längen-Stempel]";// "[Linetype]";
			TextSize = 1;
			LinetypeType = new LinetypeType();
		}
		public SingleLinetypeData(SingleLinetypeData data)
		{
			Name = data.Name;
			TextSize = data.TextSize;
			LinetypeType = data.LinetypeType.Clone();
		}
		/// <summary>
		/// Sets linetype for provided entity.
		/// </summary>
		/// <param name="h"></param>
		public void ApplyPattern(Entity h)
		{
			LinetypeType.ApplyPattern(h);
		}
	}
}
