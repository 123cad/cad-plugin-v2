﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.LinetypeToolbars.Data.TableDatas
{
	/// <summary>
	/// Keeps current settings (header column data, and columns chosen by the user - order and appearance).
	/// </summary>
	class TableSettings
	{
		/// <summary>
		/// Header column - fixed index 0.
		/// </summary>
		public TableColumn Header { get; private set; }
		/// <summary>
		/// Columns in display order (id must be successive index)
		/// </summary>
		public IList<TableColumn> Columns { get; private set; }
		/// <summary>
		/// Number of columns including header column.
		/// </summary>
		public int TotalColumnsCount { get { return Columns.Count + 1; } }
		/// <summary>
		/// Number of columns not including header column.
		/// </summary>
		public int ValueColumnsCount { get { return Columns.Count; } }
		/// <summary>
		/// Should row with sum from all columns be displayed at the end?
		/// </summary>
		public bool ShowSumRow { get; set; }
		public TableSettings()
		{ 
			ShowSumRow = true;
			Header = new TableColumn(ToolbarNodManager.HeaderColumn, ColumnType.Header, "Name");
			Columns = new List<TableColumn>();
		}

		/// <summary>
		/// Returns all columns, where Header is the first one.
		/// </summary>
		/// <returns></returns>
		public IList<TableColumn> GetAllColumns()
		{
			return new TableColumn[] { Header }.Concat(Columns).ToList();
		}

		public TableSettings Clone()
		{
			TableSettings ts = new TableSettings();
			ts.Header = Header.Clone();
			foreach (TableColumn tc in Columns)
				ts.Columns.Add(tc.Clone());
			return ts;
		}
	}
}
