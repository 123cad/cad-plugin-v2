﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.LinetypeToolbars.Data.TableDatas
{
	class TableColumn
	{
		public ColumnType Column { get; private set; }
		public string Name { get; private set; }
		/// <summary>
		/// Id of the column - Prefix from ToolbarNodManager + index (Column0, Column1...).
		/// </summary>
		public string ColumnId { get; private set; }
		public TableColumn(string columnId, ColumnType type, string name)
		{
			ColumnId = columnId;
			Column = type;
			Name = name;
		}
		public TableColumn Clone()
		{
			TableColumn tc = new TableColumn(ColumnId, Column, Name);
			return tc;
		}
	}
}
