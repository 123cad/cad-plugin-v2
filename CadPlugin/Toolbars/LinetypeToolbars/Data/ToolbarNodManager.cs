﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.LinetypeToolbars.Data.TableDatas;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.LinetypeToolbars.Data
{
	class ColumnIdGenerator
	{
		private int current = -1;
		public ColumnIdGenerator()
		{
			current = 0;
		}
		/// <summary>
		/// Returns current name and moves to the next one.
		/// </summary>
		/// <returns></returns>
		public string GetNextName()
		{
			return ToolbarNodManager.HeaderColumnPrefix + current++;
		}
	}

	class ToolbarNodManager
	{
		// Attribute ids are stored inside NOD, so if some objects are copied we can check (extensionDictionary is copied with entity, so it is not good for this operation).
		#region Structure Definition
		/*
		 * <_123CAD_HATCHTOOLBAR>
			  <Attributes>
				<id/>																			<!-- handle of the MainObject of the attribute -->
			  </Attributes>
			  <Tables>
				  <TableId>																		<!-- unique id of table in this DBDictionary -->
					  <Boundary handle="pl" />													<!-- Border polyline of the table with its "handle" -->
					  <Header handle="mtext" content="" />										<!-- Header for table. "handle" is for mtext which displays the "content" -->
					  <HeaderColumns>
						  <Header handle1="mtext" handle2="line" />								<!-- "handle1" displays column name, "handle2" is vertical line on the right side of column-->
																								<!-- Column names must have consecutive indexes -->
						  <Column0 handle1="mtext" int="type" string="name" handle2="line"  />	<!-- "handle1" displays column name, "handle2" is vertical line on the right side of column, type is associated type with column-->
						  ...
						  <ColumnN handle1="mtext" int="type" string="name" />							<!-- "handle1" displays column name, LAST column doesn't have a line-->
						  <Line handle="line"/>													<!-- "handle" for the line which goes ABOVE-->
					  </HeaderColumns>
					  <Rows>
						  <!-- Every row is indexed as handle of associated attMText.Handle but in text form (id) -->
						  <Order id..id=""/>													<!-- Defines order of rows in the table -->
						  <id>																	<!-- id id the attMText.Handle as text -->
							  <Text handle="mtext" .../>										<!-- handle for mtext in every column -->
							  <Line handle="line"/>												<!-- line which goes ABOVE the row -->
						  </id>
						  <id..../>
					  </Rows>
					  <SumRow>													<!-- Last row, show the sum in every column -->
							<Text handle="mtext" handle="mtext" ... />
					  </SumRow>
				  </TableId>
			  </Tables>
			</_123CAD_HATCHTOOLBAR>
		 */
		#endregion

		/// <summary>
		/// Main entry for toolbar
		/// </summary>
		public static readonly string ToolbarEntry = "C123CAD_LINETYPETOOLBAR";
		/// <summary>
		/// Tables entry in toolbar entry.
		/// </summary>
		public static readonly string Tables = "Tables";
		/// <summary>
		/// Attributes entry in toolbar entry.
		/// </summary>
		public static readonly string Attributes = "Attributes";

		public static readonly string TableBoundary = "Boundary";
		public static readonly string TableHeader = "Header";
		/// <summary>
		/// Entry which represents columns for this table.
		/// </summary>
		public static readonly string TableColumns = "HeaderColumns";
		/// <summary>
		/// Header or name column - first one.
		/// </summary>
		public static readonly string HeaderColumn = "Header";
		/// <summary>
		/// Prefix for all columns except header.
		/// </summary>
		public static readonly string HeaderColumnPrefix = "Column";
		public static readonly string HeaderLine = "Line";
		public static readonly string Rows = "Rows";
		public static readonly string RowsOrder = "Order";
		public static readonly string RowText = "Text";
		public static readonly string RowLine = "Line";
		public static readonly string SumRow = "SumRow";
		public static readonly string SumRowText = "Text";
		public static readonly string SumRowLine = "Line";

		public static void PrepareToolbarNODStructure(TransactionData data)
		{
			//CADCommonHelper.CreateSubDictionary(data.Transaction, data.NOD, ToolbarEntry, Attributes);
			//CADCommonHelper.CreateSubDictionary(data.Transaction, data.NOD, ToolbarEntry, Tables);
			data.NOD.OpenOrCreateSubDictionary(data.Transaction, ToolbarEntry, Attributes);
			data.NOD.OpenOrCreateSubDictionary(data.Transaction, ToolbarEntry, Tables);
		}

		public static bool AttributeExists(TransactionData data, ObjectId oid)
		{
			string s = oid.Handle.Value.ToString();
			return AttributeExists(data, s);
		}
		public static bool AttributeExists(TransactionData data, string handle)
		{
			if (data.ToolbarEntry == null)
				return false;
			DBDictionary res = data.ToolbarEntry.OpenSubDictionary(data.Transaction, Attributes);
				//CADCommonHelper.GetSubDictionary(data.Transaction, data.ToolbarEntry, Attributes);
			if (res != null)
				return res.Contains(handle);
			return false;
		}

		public static void AddAttribute(TransactionData data, ObjectId oid)
		{
			string s = oid.Handle.Value.ToString();
			AddAttribute(data, s);
		}
		public static void AddAttribute(TransactionData data, string handle)
		{
			if (data.ToolbarEntry == null)
				return;
			DBDictionary attributes = data.ToolbarEntry.OpenSubDictionary(data.Transaction, Attributes);
				//CADCommonHelper.GetSubDictionary(data.Transaction, data.ToolbarEntry, Attributes);
			if (!attributes.Contains(handle))
			{
				attributes.UpgradeOpen();
				Xrecord rec = data.Transaction.CreateXRecord(attributes, handle);
				attributes.DowngradeOpen();
			}
		}
		public static void RemoveAttribute(TransactionData data, ObjectId oid)
		{
			string s = oid.Handle.Value.ToString();
			RemoveAttribute(data, s);
		}
		/// <summary>
		/// Remove entry which represents handle for attribute.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="handle"></param>
		public static void RemoveAttribute(TransactionData data, string handle)
		{
			if (data.ToolbarEntry == null)
				return;
			DBDictionary attributes = data.ToolbarEntry.OpenSubDictionary(data.Transaction, Attributes);
				//CADCommonHelper.GetSubDictionary(data.Transaction, data.ToolbarEntry, Attributes);
			if (attributes.Contains(handle))
			{
				attributes.UpgradeOpen();
				attributes.Remove(handle);
				attributes.DowngradeOpen();
			}
		}

		/// <summary>
		/// Checks whether entity belongs to existing table.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="ent"></param>
		/// <returns></returns>
		public static bool IsTableEntity(TransactionData data, Entity ent)
		{
			string s = ent.LoadExtensionString(data.Transaction, ExtensionDictionaryEntries.TableId);

			if (ent.LoadExtensionString(data.Transaction, ExtensionDictionaryEntries.ObjectPersonalId) != ent.ObjectId.Handle.ConvertToString())
			{
				ent.RemoveFromExtensionData(data.Transaction,
												ExtensionDictionaryEntries.TableId,
												ExtensionDictionaryEntries.AttributeTextLinkId,
												ExtensionDictionaryEntries.ObjectPersonalId);
				//NOTE After checkout back few months, this causes StackOverflowException when 
				//NOTE started without debugger (via exploring dump file). With debugger, it works.
				//System.Diagnostics.Debug.WriteLine("Removed from ExtensionDictionary! (table not found). id= " + ent.ObjectId);
				return false;
			}

			return !string.IsNullOrEmpty(s);
		}

		/// <summary>
		/// Creates new TableId and creates entry in the NOD.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static string CreateAndInitializeTableId(TransactionData data, TableSettings table)
		{
			string assignedId = null;
			DBDictionary tables = data.NOD.OpenOrCreateSubDictionary(data.Transaction, ToolbarEntry, Tables);
				//CADCommonHelper.CreateSubDictionary(data.Transaction, data.NOD, ToolbarEntry, Tables);

			// Assign first available id.
			string tableIdPrefix = "T_";
			int step = 20;
			int start = 0;
			int end = step;
			while (true)
			{
				string name = tableIdPrefix + start;
				// If start of section is free, use it and stop.
				if (!tables.Contains(name))
				{
					assignedId = name;
					break;
				}
				name = tableIdPrefix + end;
				// If end of section is free, go from start of section until free space is found.
				// (it can happen that 1-19 tables are deleted, but we can't see it because 20 is taken).
				if (!tables.Contains(name))
				{
					int i = start;
					// Optimization: if middle id is taken, no need to iterate before it.
					name = tableIdPrefix + (end / 2);
					if (tables.Contains(name))
						i = end / 2;
					while (true)
					{
						// There must be available id.
						name = tableIdPrefix + i;
						if (!tables.Contains(name))
						{
							assignedId = name;
							break;
						}
						i++;
					}
					break;
				}
				start = end + 1;
				end += step;
			}
			DBDictionary newTable = InitializeSingleTableEntry(data, tables, assignedId, table);

			return assignedId;
		}
		/// <summary>
		/// Creates structure for single table 
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		private static DBDictionary InitializeSingleTableEntry(TransactionData data, DBDictionary parentDic, string id, TableSettings tableSetting)
		{
			if (!parentDic.IsWriteEnabled)
				parentDic.UpgradeOpen();
			// Needed here because SetAt assignes ObjectId also (not sure if must be here).
			DBDictionary table = data.Transaction.CreateDBDictionary(parentDic, id);

			Xrecord boundary = data.Transaction.CreateXRecord(table, TableBoundary);
			Xrecord header = data.Transaction.CreateXRecord(table, TableHeader);

			DBDictionary columnHeaderDic = data.Transaction.CreateDBDictionary(table, TableColumns);
			{
				SaveTableSettings(data, columnHeaderDic, tableSetting);
				Xrecord line = data.Transaction.CreateXRecord(columnHeaderDic, HeaderLine);
			}

			DBDictionary rows = data.Transaction.CreateDBDictionary(table, Rows);
			{
				Xrecord rowOrder = data.Transaction.CreateXRecord(rows, RowsOrder);
			}

			if (tableSetting.ShowSumRow)
			{
				DBDictionary rowSum = data.Transaction.CreateDBDictionary(table, SumRow);
				{
					Xrecord sumRowX = data.Transaction.CreateXRecord(rowSum, SumRowText);
					Xrecord sumRowLine = data.Transaction.CreateXRecord(rowSum, SumRowLine);
				}
			}
			return table;
		}
		/// <summary>
		/// Save TableSettings (creates columns).
		/// </summary>
		/// <param name="columnDic">DBDictionary for column headers.</param>
		private static void SaveTableSettings(TransactionData data, DBDictionary columnDic, TableSettings tableSettings)
		{
			Xrecord col = data.Transaction.CreateXRecord(columnDic, HeaderColumn);
			ColumnIdGenerator generator = new ColumnIdGenerator();
			for (int i = 0; i < tableSettings.Columns.Count; i++)
			{
				string column = generator.GetNextName();
				data.Transaction.CreateXRecord(columnDic, column);
			}
		}
		/// <summary>
		/// Loads TableSettings for provided table.
		/// </summary>
		/// <param name="tableDic"></param>
		public static TableSettings LoadTableSettings(TransactionData data, DBDictionary tableDic)
		{
			TableSettings ts = new TableSettings();
			if (!tableDic.Contains(TableColumns))
				return null;
			DBDictionary columnHeaderDic = data.Transaction.GetObject(tableDic.GetAt(TableColumns), OpenMode.ForRead) as DBDictionary;
			// Header column has fixed name.
			// Others don't.
			ColumnIdGenerator generator = new ColumnIdGenerator();
			for (int i = 0; ;i++)
			{
				string columnId = generator.GetNextName();
				if (columnHeaderDic.Contains(columnId))
				{
					Xrecord rec = data.Transaction.GetObject(columnHeaderDic.GetAt(columnId), OpenMode.ForRead) as Xrecord;
					if (rec.Data == null)
						continue;
					TypedValue[] values = rec.Data.AsArray();
					if (values.Length < 3)
						continue;
					TableDatas.ColumnType type = (TableDatas.ColumnType)values[1].Value;
					string columnName = (string)values[2].Value;
					TableColumn tc = new TableColumn(columnId, type, columnName);
					ts.Columns.Add(tc);
				}
				else
					break;
			}
			return ts;
		}

		/// <summary>
		/// Returns table entry (DBDictionary) in nod.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="name"></param>
		/// <returns></returns>
		public static DBDictionary GetTable(TransactionData data, string name)
		{
			DBDictionary tables = data.ToolbarEntry.OpenSubDictionary(data.Transaction, Tables);
			//CADCommonHelper.GetSubDictionary(data.Transaction, data.ToolbarEntry, Tables);
			if (tables == null)
				return null;

			DBDictionary table = data.Transaction.GetObject(tables.GetAt(name), OpenMode.ForWrite) as DBDictionary;
			return table;

		}
		/// <summary>
		/// Adds row to table in nod.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="table"></param>
		/// <param name="rowId"></param>
		public static void AddNewRow(TransactionData data, DBDictionary table, string rowId)
		{
			DBDictionary rows = data.Transaction.GetObject(table.GetAt(Rows), OpenMode.ForWrite) as DBDictionary;
			if (rows.Contains(rowId))
				throw new ArgumentException("Row already exists!");
			Xrecord order = data.Transaction.GetObject(rows.GetAt(RowsOrder), OpenMode.ForWrite) as Xrecord;
			ResultBuffer buffer = order.Data;
			if (buffer == null)
			{
				buffer = new ResultBuffer();
			}
			buffer.Add(new TypedValue((int)DxfCode.Text, rowId));

			DBDictionary row = data.Transaction.CreateDBDictionary(rows, rowId);

			Xrecord rowText = data.Transaction.CreateXRecord(row, RowText);

			Xrecord rowLine = data.Transaction.CreateXRecord(row, RowLine);

			order.Data = buffer;

		}
		/// <summary>
		/// Removes row from table in NOD, with related entities.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="table"></param>
		/// <param name="rowId"></param>
		public static void RemoveRows(TransactionData data, DBDictionary table, params string[] rowIds)
		{
			DBDictionary rows = data.Transaction.GetObject(table.GetAt(Rows), OpenMode.ForWrite) as DBDictionary;
			Xrecord order = data.Transaction.GetObject(rows.GetAt(RowsOrder), OpenMode.ForWrite) as Xrecord;
			if (order.Data == null)
				return;
			HashSet<string> rowIdsHash = new HashSet<string>(rowIds.Distinct().ToArray());
			List<TypedValue> keepValues = new List<TypedValue>();
			foreach (TypedValue val in order.Data.AsArray())
			{
				string s = val.Value as string;
				if (!rowIdsHash.Contains(s))
					keepValues.Add(val);
			}
			order.Data = new ResultBuffer(keepValues.ToArray());

			foreach (string rowId in rowIds)
			{
				if (!rows.Contains(rowId))
					continue;

				// Remove entities
				DBDictionary rowDic = data.Transaction.GetObject(rows.GetAt(rowId), OpenMode.ForWrite) as DBDictionary;
				Xrecord rec = data.Transaction.GetObject(rowDic.GetAt(ToolbarNodManager.RowText), OpenMode.ForWrite) as Xrecord;
				if (rec.Data != null)
				{
					TypedValue[] values = rec.Data.AsArray();
					if (values.Length > 0)
						for (int i = 0; i < values.Length; i++)
						{
							Entity ent = data.Transaction.GetObject(values[i].GetObjectId(data.Database), OpenMode.ForWrite, true) as Entity;
							if (ent != null && !ent.IsErased)
								ent.Erase();
						}
				}
				rec = data.Transaction.GetObject(rowDic.GetAt(ToolbarNodManager.RowLine), OpenMode.ForWrite) as Xrecord;
				if (rec.Data != null)
				{
					TypedValue[] values = rec.Data.AsArray();
					if (values.Length > 0)
					{
						Entity ent = data.Transaction.GetObject(values[0].GetObjectId(data.Database), OpenMode.ForWrite, true) as Entity;
						if (ent != null && !ent.IsErased)
							ent.Erase();
					}
				}

				rows.Remove(rowId);
			}
		}

		/// <summary>
		/// Returns ids for all rows.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="table"></param>
		/// <returns></returns>
		public static List<string> GetAllRowIds(TransactionData data, DBDictionary table)
		{
			List<string> ids = new List<string>();
			DBDictionary rows = data.Transaction.GetObject(table.GetAt(Rows), OpenMode.ForRead) as DBDictionary;
			Xrecord order = data.Transaction.GetObject(rows.GetAt(RowsOrder), OpenMode.ForRead) as Xrecord;
			if (order.Data != null)
				ids.AddRange(order.Data.AsArray().Select(x => (string)x.Value).ToArray());
			return ids;
		}
		/// <summary>
		/// Removes all table entities except rows.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="tableId"></param>
		public static void DeleteTable(TransactionData data, string tableId)
		{
			//DBDictionary toolbar = null;
			DBDictionary tables = data.ToolbarEntry.OpenSubDictionary(data.Transaction, Tables);
				//CADCommonHelper.GetSubDictionary(data.Transaction, data.ToolbarEntry, Tables);
			if (tables == null)
				return;
			if (!tables.IsWriteEnabled)
				tables.UpgradeOpen();
			tables = data.Transaction.GetObject(data.ToolbarEntry.GetAt(Tables), OpenMode.ForWrite) as DBDictionary;
			if (tables.Contains(tableId))
			{
				DBDictionary table = data.Transaction.GetObject(tables.GetAt(tableId), OpenMode.ForWrite) as DBDictionary;
				Xrecord rec = data.Transaction.GetObject(table.GetAt(ToolbarNodManager.TableBoundary), OpenMode.ForWrite) as Xrecord;
				TypedValue[] values = rec.Data != null ? rec.Data.AsArray() : null;
				if (values != null && values.Length > 0)
				{
					ObjectId oid = values[0].GetObjectId(data.Database);
					Entity ent = data.Transaction.GetObject(oid, OpenMode.ForWrite, true) as Entity;
					if (!ent.IsErased)
						ent.Erase();
				}

				rec = data.Transaction.GetObject(table.GetAt(ToolbarNodManager.TableHeader), OpenMode.ForRead) as Xrecord;
				values = rec.Data != null ? rec.Data.AsArray() : null;
				if (values != null && values.Length >0 )
				{
					ObjectId oid = values[0].GetObjectId(data.Database);
					Entity ent = data.Transaction.GetObject(oid, OpenMode.ForWrite, true) as Entity;
					if (!ent.IsErased)
						ent.Erase();
				}

				DBDictionary dic = data.Transaction.GetObject(table.GetAt(ToolbarNodManager.TableColumns), OpenMode.ForRead) as DBDictionary;
				{
					List<string> columnNames = new List<string>();
					columnNames.Add(HeaderColumn);
					ColumnIdGenerator generator = new ColumnIdGenerator();
					for (int i = 0; true; i++)
					{
						string name = generator.GetNextName();
						if (dic.Contains(name))
							columnNames.Add(name);
						else
							break;
					}
					for (int i = 0; i < columnNames.Count; i++)
					{
						string name = columnNames[i];
						rec = data.Transaction.GetObject(dic.GetAt(name), OpenMode.ForRead) as Xrecord;
						values = rec.Data != null ? rec.Data.AsArray() : null;
						if (values != null && values.Length > 0)
						{
							ObjectId oid = values[0].GetObjectId(data.Database);
							Entity ent = data.Transaction.GetObject(oid, OpenMode.ForWrite, true) as Entity;
							if (!ent.IsErased)
								ent.Erase();
							if (i < columnNames.Count - 1)
							{
								oid = values[3].GetObjectId(data.Database);
								ent = data.Transaction.GetObject(oid, OpenMode.ForWrite, true) as Entity;
								if (!ent.IsErased)
									ent.Erase();
							}
						}
					}
					rec = data.Transaction.GetObject(dic.GetAt(ToolbarNodManager.HeaderLine), OpenMode.ForRead) as Xrecord;
					values = rec.Data != null ? rec.Data.AsArray() : null;
					if (values != null && values.Length >0)
					{
						ObjectId oid = values[0].GetObjectId(data.Database);
						Entity ent = data.Transaction.GetObject(oid, OpenMode.ForWrite, true) as Entity;
						if (!ent.IsErased)
							ent.Erase();
					}
				}
				if (table.Contains(SumRow))
				{
					dic = data.Transaction.GetObject(table.GetAt(ToolbarNodManager.SumRow), OpenMode.ForRead) as DBDictionary;
					if (dic != null)
					{
						rec = data.Transaction.GetObject(dic.GetAt(SumRowText), OpenMode.ForRead) as Xrecord;
						values = rec.Data != null ? rec.Data.AsArray() : null;
						if (values != null && values.Length > 0)
						{
							for (int i = 0; i < values.Length; i++)
							{
								ObjectId oid = values[i].GetObjectId(data.Database);
								Entity ent = data.Transaction.GetObject(oid, OpenMode.ForWrite, true) as Entity;
								if (!ent.IsErased)
									ent.Erase();
							}
						}

						rec = data.Transaction.GetObject(dic.GetAt(SumRowLine), OpenMode.ForRead) as Xrecord;
						values = rec.Data != null ? rec.Data.AsArray() : null;
						if (values != null && values.Length > 0)
						{
							ObjectId oid = values[0].GetObjectId(data.Database);
							Entity ent = data.Transaction.GetObject(oid, OpenMode.ForWrite, true) as Entity;
							if (!ent.IsErased)
								ent.Erase();
						}
					}
				}

				tables.Remove(tableId);
			}

		}

	}
}
