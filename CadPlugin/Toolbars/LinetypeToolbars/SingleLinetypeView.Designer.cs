﻿namespace CadPlugin.LinetypeToolbars
{
	partial class SingleLinetypeView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.labelColor = new System.Windows.Forms.Label();
			this.textBoxName = new System.Windows.Forms.TextBox();
			this.colorDialog1 = new System.Windows.Forms.ColorDialog();
			this.textBoxColor = new System.Windows.Forms.TextBox();
			this.buttonColorChange = new System.Windows.Forms.Button();
			this.buttonOk = new System.Windows.Forms.Button();
			this.labelTextSize = new System.Windows.Forms.Label();
			this.textBoxTextSize = new System.Windows.Forms.TextBox();
			this.labelTransparency = new System.Windows.Forms.Label();
			this.labelTransparencyLevel = new System.Windows.Forms.Label();
			this.trackBarTransparency = new System.Windows.Forms.TrackBar();
			this.comboBoxPatterns = new System.Windows.Forms.ComboBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.textBoxScale = new System.Windows.Forms.TextBox();
			this.labelScale = new System.Windows.Forms.Label();
			this.labelWeight = new System.Windows.Forms.Label();
			this.comboBoxWeight = new System.Windows.Forms.ComboBox();
			this.labelLinetype = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.trackBarTransparency)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(19, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(35, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Name";
			// 
			// labelColor
			// 
			this.labelColor.AutoSize = true;
			this.labelColor.Location = new System.Drawing.Point(19, 78);
			this.labelColor.Name = "labelColor";
			this.labelColor.Size = new System.Drawing.Size(31, 13);
			this.labelColor.TabIndex = 1;
			this.labelColor.Text = "Color";
			// 
			// textBoxName
			// 
			this.textBoxName.Location = new System.Drawing.Point(85, 17);
			this.textBoxName.Name = "textBoxName";
			this.textBoxName.Size = new System.Drawing.Size(101, 20);
			this.textBoxName.TabIndex = 4;
			// 
			// textBoxColor
			// 
			this.textBoxColor.Enabled = false;
			this.textBoxColor.Location = new System.Drawing.Point(85, 74);
			this.textBoxColor.Name = "textBoxColor";
			this.textBoxColor.ReadOnly = true;
			this.textBoxColor.Size = new System.Drawing.Size(39, 20);
			this.textBoxColor.TabIndex = 7;
			this.textBoxColor.TabStop = false;
			// 
			// buttonColorChange
			// 
			this.buttonColorChange.Location = new System.Drawing.Point(131, 74);
			this.buttonColorChange.Name = "buttonColorChange";
			this.buttonColorChange.Size = new System.Drawing.Size(55, 23);
			this.buttonColorChange.TabIndex = 8;
			this.buttonColorChange.Text = "Change";
			this.buttonColorChange.UseVisualStyleBackColor = true;
			this.buttonColorChange.Click += new System.EventHandler(this.buttonColorChange_Click);
			// 
			// buttonOk
			// 
			this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonOk.Location = new System.Drawing.Point(111, 286);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(75, 23);
			this.buttonOk.TabIndex = 9;
			this.buttonOk.Text = "Ok";
			this.buttonOk.UseVisualStyleBackColor = true;
			this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
			// 
			// labelTextSize
			// 
			this.labelTextSize.AutoSize = true;
			this.labelTextSize.Location = new System.Drawing.Point(18, 47);
			this.labelTextSize.Name = "labelTextSize";
			this.labelTextSize.Size = new System.Drawing.Size(49, 13);
			this.labelTextSize.TabIndex = 12;
			this.labelTextSize.Text = "Text size";
			// 
			// textBoxTextSize
			// 
			this.textBoxTextSize.Location = new System.Drawing.Point(85, 44);
			this.textBoxTextSize.Name = "textBoxTextSize";
			this.textBoxTextSize.Size = new System.Drawing.Size(39, 20);
			this.textBoxTextSize.TabIndex = 13;
			// 
			// labelTransparency
			// 
			this.labelTransparency.AutoSize = true;
			this.labelTransparency.Location = new System.Drawing.Point(18, 110);
			this.labelTransparency.Name = "labelTransparency";
			this.labelTransparency.Size = new System.Drawing.Size(72, 13);
			this.labelTransparency.TabIndex = 14;
			this.labelTransparency.Text = "Transparency";
			// 
			// labelTransparencyLevel
			// 
			this.labelTransparencyLevel.AutoSize = true;
			this.labelTransparencyLevel.Location = new System.Drawing.Point(107, 110);
			this.labelTransparencyLevel.Name = "labelTransparencyLevel";
			this.labelTransparencyLevel.Size = new System.Drawing.Size(19, 13);
			this.labelTransparencyLevel.TabIndex = 15;
			this.labelTransparencyLevel.Text = "90";
			// 
			// trackBarTransparency
			// 
			this.trackBarTransparency.Location = new System.Drawing.Point(12, 126);
			this.trackBarTransparency.Maximum = 90;
			this.trackBarTransparency.Name = "trackBarTransparency";
			this.trackBarTransparency.Size = new System.Drawing.Size(174, 45);
			this.trackBarTransparency.TabIndex = 16;
			this.trackBarTransparency.TickStyle = System.Windows.Forms.TickStyle.None;
			this.trackBarTransparency.Value = 2;
			this.trackBarTransparency.ValueChanged += new System.EventHandler(this.trackBarTransparency_ValueChanged);
			// 
			// comboBoxPatterns
			// 
			this.comboBoxPatterns.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
			this.comboBoxPatterns.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxPatterns.DropDownWidth = 139;
			this.comboBoxPatterns.FormattingEnabled = true;
			this.comboBoxPatterns.Location = new System.Drawing.Point(95, 177);
			this.comboBoxPatterns.MaxDropDownItems = 10;
			this.comboBoxPatterns.Name = "comboBoxPatterns";
			this.comboBoxPatterns.Size = new System.Drawing.Size(91, 21);
			this.comboBoxPatterns.TabIndex = 18;
			// 
			// textBoxScale
			// 
			this.textBoxScale.Location = new System.Drawing.Point(95, 204);
			this.textBoxScale.Name = "textBoxScale";
			this.textBoxScale.Size = new System.Drawing.Size(39, 20);
			this.textBoxScale.TabIndex = 20;
			// 
			// labelScale
			// 
			this.labelScale.AutoSize = true;
			this.labelScale.Location = new System.Drawing.Point(19, 207);
			this.labelScale.Name = "labelScale";
			this.labelScale.Size = new System.Drawing.Size(34, 13);
			this.labelScale.TabIndex = 19;
			this.labelScale.Text = "Scale";
			// 
			// labelWeight
			// 
			this.labelWeight.AutoSize = true;
			this.labelWeight.Location = new System.Drawing.Point(19, 233);
			this.labelWeight.Name = "labelWeight";
			this.labelWeight.Size = new System.Drawing.Size(41, 13);
			this.labelWeight.TabIndex = 21;
			this.labelWeight.Text = "Weight";
			// 
			// comboBoxWeight
			// 
			this.comboBoxWeight.DropDownHeight = 80;
			this.comboBoxWeight.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxWeight.FormattingEnabled = true;
			this.comboBoxWeight.IntegralHeight = false;
			this.comboBoxWeight.Location = new System.Drawing.Point(95, 230);
			this.comboBoxWeight.Name = "comboBoxWeight";
			this.comboBoxWeight.Size = new System.Drawing.Size(91, 21);
			this.comboBoxWeight.TabIndex = 22;
			// 
			// labelLinetype
			// 
			this.labelLinetype.AutoSize = true;
			this.labelLinetype.Location = new System.Drawing.Point(18, 180);
			this.labelLinetype.Name = "labelLinetype";
			this.labelLinetype.Size = new System.Drawing.Size(47, 13);
			this.labelLinetype.TabIndex = 23;
			this.labelLinetype.Text = "Linetype";
			// 
			// SingleLinetypeView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(207, 322);
			this.Controls.Add(this.labelLinetype);
			this.Controls.Add(this.comboBoxWeight);
			this.Controls.Add(this.labelWeight);
			this.Controls.Add(this.textBoxScale);
			this.Controls.Add(this.labelScale);
			this.Controls.Add(this.trackBarTransparency);
			this.Controls.Add(this.labelTransparencyLevel);
			this.Controls.Add(this.comboBoxPatterns);
			this.Controls.Add(this.labelTransparency);
			this.Controls.Add(this.textBoxTextSize);
			this.Controls.Add(this.labelTextSize);
			this.Controls.Add(this.buttonOk);
			this.Controls.Add(this.buttonColorChange);
			this.Controls.Add(this.textBoxColor);
			this.Controls.Add(this.textBoxName);
			this.Controls.Add(this.labelColor);
			this.Controls.Add(this.label1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SingleLinetypeView";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "SingleLinetypeView";
			this.TopMost = true;
			((System.ComponentModel.ISupportInitialize)(this.trackBarTransparency)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label labelColor;
		private System.Windows.Forms.TextBox textBoxName;
		private System.Windows.Forms.ColorDialog colorDialog1;
		private System.Windows.Forms.TextBox textBoxColor;
		private System.Windows.Forms.Button buttonColorChange;
		private System.Windows.Forms.Button buttonOk;
		private System.Windows.Forms.Label labelTextSize;
		private System.Windows.Forms.TextBox textBoxTextSize;
		private System.Windows.Forms.Label labelTransparency;
		private System.Windows.Forms.Label labelTransparencyLevel;
		private System.Windows.Forms.TrackBar trackBarTransparency;
		private System.Windows.Forms.ComboBox comboBoxPatterns;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.TextBox textBoxScale;
		private System.Windows.Forms.Label labelScale;
		private System.Windows.Forms.Label labelWeight;
		private System.Windows.Forms.ComboBox comboBoxWeight;
		private System.Windows.Forms.Label labelLinetype;
	}
}