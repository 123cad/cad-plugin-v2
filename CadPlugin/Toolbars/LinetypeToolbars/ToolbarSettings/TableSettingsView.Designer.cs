﻿namespace CadPlugin.LinetypeToolbars.ToolbarSettings
{
	partial class TableSettingsView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TableSettingsView));
			this.dataGridViewColumns = new System.Windows.Forms.DataGridView();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnSelectColumns = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.labelSelectColumns = new System.Windows.Forms.Label();
			this.buttonOk = new System.Windows.Forms.Button();
			this.checkBoxSumRow = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewColumns)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridViewColumns
			// 
			this.dataGridViewColumns.AllowUserToAddRows = false;
			this.dataGridViewColumns.AllowUserToDeleteRows = false;
			this.dataGridViewColumns.AllowUserToResizeColumns = false;
			this.dataGridViewColumns.AllowUserToResizeRows = false;
			this.dataGridViewColumns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dataGridViewColumns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewColumns.ColumnHeadersVisible = false;
			this.dataGridViewColumns.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.ColumnSelectColumns});
			this.dataGridViewColumns.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
			this.dataGridViewColumns.Location = new System.Drawing.Point(15, 28);
			this.dataGridViewColumns.Name = "dataGridViewColumns";
			this.dataGridViewColumns.RowHeadersVisible = false;
			this.dataGridViewColumns.Size = new System.Drawing.Size(233, 150);
			this.dataGridViewColumns.TabIndex = 4;
			// 
			// Column2
			// 
			this.Column2.HeaderText = "Index";
			this.Column2.Name = "Column2";
			this.Column2.ReadOnly = true;
			this.Column2.Width = 20;
			// 
			// ColumnSelectColumns
			// 
			this.ColumnSelectColumns.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.ColumnSelectColumns.HeaderText = "Data";
			this.ColumnSelectColumns.Items.AddRange(new object[] {
            "None",
            "Area",
            "Length",
            "Höhe",
            "Length"});
			this.ColumnSelectColumns.Name = "ColumnSelectColumns";
			// 
			// labelSelectColumns
			// 
			this.labelSelectColumns.AutoSize = true;
			this.labelSelectColumns.Location = new System.Drawing.Point(12, 9);
			this.labelSelectColumns.Name = "labelSelectColumns";
			this.labelSelectColumns.Size = new System.Drawing.Size(79, 13);
			this.labelSelectColumns.TabIndex = 5;
			this.labelSelectColumns.Text = "Select columns";
			// 
			// buttonOk
			// 
			this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonOk.Location = new System.Drawing.Point(173, 227);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(75, 23);
			this.buttonOk.TabIndex = 6;
			this.buttonOk.Text = "Ok";
			this.buttonOk.UseVisualStyleBackColor = true;
			this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
			// 
			// checkBoxSumRow
			// 
			this.checkBoxSumRow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.checkBoxSumRow.AutoSize = true;
			this.checkBoxSumRow.Location = new System.Drawing.Point(15, 201);
			this.checkBoxSumRow.Name = "checkBoxSumRow";
			this.checkBoxSumRow.Size = new System.Drawing.Size(95, 17);
			this.checkBoxSumRow.TabIndex = 7;
			this.checkBoxSumRow.Text = "Show sum row";
			this.checkBoxSumRow.UseVisualStyleBackColor = true;
			// 
			// TableSettingsView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(260, 262);
			this.Controls.Add(this.checkBoxSumRow);
			this.Controls.Add(this.buttonOk);
			this.Controls.Add(this.labelSelectColumns);
			this.Controls.Add(this.dataGridViewColumns);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "TableSettingsView";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Settings";
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewColumns)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridViewColumns;
		private System.Windows.Forms.Label labelSelectColumns;
		private System.Windows.Forms.Button buttonOk;
		private System.Windows.Forms.CheckBox checkBoxSumRow;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
		private System.Windows.Forms.DataGridViewComboBoxColumn ColumnSelectColumns;
	}
}