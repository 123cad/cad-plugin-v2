﻿using CadPlugin.LinetypeToolbars.Data.TableDatas;
using CadPlugin.LinetypeToolbars.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.LinetypeToolbars.ToolbarSettings
{
	/// <summary>
	/// Keeps current TableSettings, and performs loading/saving to app settings.
	/// </summary>
	class TableSettingsManager
	{
		//UPDATE Add loading/saving to app settings.

		/// <summary>
		/// Current settings.
		/// </summary>
		public static TableSettings TableSettings { get; private set; }
		public static void SetNewSettings(TableSettings set)
		{
			TableSettings = set;
		}
		static TableSettingsManager()
		{
			TableSettings = new TableSettings();
			List<TableColumn> columns = DataPersistence.LoadTableColumns();
			foreach (TableColumn tc in columns)
				TableSettings.Columns.Add(tc);
			TableSettings.ShowSumRow = DataPersistence.LoadShowTableSumRow();
		}

	}
}
