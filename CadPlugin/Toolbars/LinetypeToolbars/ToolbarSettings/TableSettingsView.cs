﻿using CadPlugin.LinetypeToolbars.Data.TableDatas;
using CadPlugin.LinetypeToolbars.Persistence;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.LinetypeToolbars.ToolbarSettings
{
	public partial class TableSettingsView : Form
	{
		public static void ShowTable()
		{
			TableSettingsView view = new TableSettingsView();
			view.ShowDialog();
		}
		private TableSettingsView()
		{
			InitializeComponent();

			Text = "Tabelle Darstellung";
			labelSelectColumns.Text = "Auswahl Spalte";
			checkBoxSumRow.Text = "Σ";// "Summen -Zeile";

			//UPDATE Add name column.
			ColumnSelectColumns.Items.Clear();
			ColumnSelectColumns.Items.AddRange(ColumnTypesAssociation.Columns);
			int rowIndex = dataGridViewColumns.Rows.Add();
			//1, ColumnTypesAssociation.Columns[1]);
			dataGridViewColumns[1, rowIndex].Value = ColumnTypesAssociation.Columns[1];

			/*for (int i = 0; i < TableSettingsManager.TableSettings.Columns.Count; i++)
			{
				TableColumn tc = TableSettingsManager.TableSettings.Columns[i];
				string columnType = ColumnTypesAssociation.GetFromType(tc.Column);
				dataGridViewColumns[ColumnSelectColumns.DisplayIndex, i].Value = columnType;
			}*/
			IList<TableColumn> columns = TableSettingsManager.TableSettings.Columns;
			for (int i = 0; i < columns.Count; i++)
			{
				TableColumn tc = columns[i];
				string columnType = ColumnTypesAssociation.GetFromType(tc.Column);
				dataGridViewColumns[ColumnSelectColumns.DisplayIndex, i].Value = columnType;
			}

			checkBoxSumRow.Checked = TableSettingsManager.TableSettings.ShowSumRow;

			Action refreshIndexes = () =>
			{
				// Refresh indexes (if some column has "None" as column type, it is ignored.
				int index = 1;
				foreach (DataGridViewRow row in dataGridViewColumns.Rows)
				{
					string s = row.Cells[1].Value as string;
					if (dataGridViewColumns.IsCurrentCellInEditMode && 
							row.Index == dataGridViewColumns.CurrentCell.RowIndex && dataGridViewColumns.CurrentCell.ColumnIndex == 1)
						s = dataGridViewColumns.CurrentCell.EditedFormattedValue as string;
					ColumnType columnType;
					if (!ColumnTypesAssociation.GetFromString(s, out columnType))
					{
						row.Cells[0].Value = "";
						continue;
					}
					row.Cells[0].Value = index++;
				}
			};
			refreshIndexes();
			dataGridViewColumns.CurrentCellDirtyStateChanged += (_, __) =>
			{
				refreshIndexes();
			};
			FormClosing += (_, __) => UpdateSettings();
		}

		/// <summary>
		/// Called on close, when settings from the form should update actual settings.
		/// </summary>
		private void UpdateSettings()
		{
			TableSettings ts = TableSettingsManager.TableSettings;
			ts.Columns.Clear();
			Data.ColumnIdGenerator generator = new Data.ColumnIdGenerator();
			foreach (DataGridViewRow row in dataGridViewColumns.Rows)
			{
				// Check all rows
				string s = row.Cells[1].Value as string;
				if (!string.IsNullOrEmpty(s))
				{
					ColumnType columnType;
					if (ColumnTypesAssociation.GetFromString(s, out columnType))
					{
						ts.Columns.Add(new TableColumn(generator.GetNextName(), columnType, s));
					}
				}
			}
			ts.ShowSumRow = checkBoxSumRow.Checked;

			DataPersistence.SaveTableColumns(ts.Columns);
			DataPersistence.SaveShowTableSumRow(ts.ShowSumRow);
		}
		private class ColumnTypesAssociation
		{
			public static string[] Columns = new string[]
			{
				"Keine",//"None",
				"Länge"//"Length"
			};
			public static string GetFromType(ColumnType type)
			{
				int index = -1;
				switch(type)
				{
					case ColumnType.Length: index = 1; break;
					default: index = 0; break;
				}
				return Columns[index];
			}
			public static bool GetFromString(string s, out ColumnType type)
			{
				type = ColumnType.Area;
				if (!Columns.Contains(s))
					return false;
				int index = -1;
				foreach (string str in Columns)
				{
					index++;
					if (str == s)
						break;
				}

				switch (index)
				{
					case 0: return false;
					case 1: type = ColumnType.Length; break;
				}
				return true;
			}
		}

		private void buttonOk_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}
