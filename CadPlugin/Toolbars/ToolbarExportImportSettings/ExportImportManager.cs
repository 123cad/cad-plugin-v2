﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CadPlugin.Toolbars.ToolbarExportImportSettings
{
	class ExportImportManager
	{
		/// <summary>
		/// Types which are used for deserialization (instead
		/// of determining exact type, deserialization is attempted
		/// with all possible types, until good one is found)
		/// </summary>
		private IEnumerable<Type> usedTypes;
		private string rootElementName;
		public ExportImportManager(string rootElement, IEnumerable<Type> types)
		{
			usedTypes = types.ToList(); // Just make a copy.
			rootElementName = rootElement;
		}
		public void SaveData(IEnumerable<object> objs, string fileName)
		{
			XmlDocument doc = new XmlDocument();
			XmlConverter converter = new XmlConverter(usedTypes);
			using (XmlTextWriter writer = new XmlTextWriter(fileName, null))
			{
				writer.Indentation = 1;
				writer.IndentChar = '\t';
				writer.Formatting = Formatting.Indented;

				writer.WriteStartDocument();
				writer.WriteStartElement(rootElementName);
				{
					writer.WriteStartElement("datalist");
					foreach (object o in objs)
					{
						writer.WriteStartElement("item");
							string data = converter.ToXml(o, writer);
						writer.WriteEndElement();
					}
					writer.WriteEndElement();
				}
				writer.WriteEndElement();
				writer.WriteEndDocument();
			}
		}
		public List<object> LoadData(string fileName)
		{
			List<object> objs = new List<object>();
			XmlConverter converter = new XmlConverter(usedTypes);
			XmlDocument doc = new XmlDocument();
			doc.Load(fileName);
			XmlElement element = doc.DocumentElement;
			if (element.LocalName != rootElementName)
				return null;
			XmlNode node = element.SelectSingleNode("datalist");
			XmlNodeList list = node.SelectNodes("item");
			foreach (XmlNode n in list)
			{
				object obj = converter.ToObject(n.InnerXml);
				objs.Add(obj);
			}

			return objs;
		}
	}
}
