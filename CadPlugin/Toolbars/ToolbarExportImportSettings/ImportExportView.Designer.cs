﻿namespace CadPlugin.Toolbars.ToolbarExportImportSettings
{
	partial class ImportExportView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonClose = new System.Windows.Forms.Button();
			this.comboBoxImportOpt = new System.Windows.Forms.ComboBox();
			this.buttonImport = new System.Windows.Forms.Button();
			this.buttonExport = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// buttonClose
			// 
			this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonClose.Location = new System.Drawing.Point(132, 83);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.Size = new System.Drawing.Size(75, 23);
			this.buttonClose.TabIndex = 8;
			this.buttonClose.Text = "Close";
			this.buttonClose.UseVisualStyleBackColor = true;
			// 
			// comboBoxImportOpt
			// 
			this.comboBoxImportOpt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxImportOpt.FormattingEnabled = true;
			this.comboBoxImportOpt.Location = new System.Drawing.Point(93, 43);
			this.comboBoxImportOpt.Name = "comboBoxImportOpt";
			this.comboBoxImportOpt.Size = new System.Drawing.Size(114, 21);
			this.comboBoxImportOpt.TabIndex = 7;
			// 
			// buttonImport
			// 
			this.buttonImport.Location = new System.Drawing.Point(12, 41);
			this.buttonImport.Name = "buttonImport";
			this.buttonImport.Size = new System.Drawing.Size(75, 23);
			this.buttonImport.TabIndex = 6;
			this.buttonImport.Text = "Import";
			this.buttonImport.UseVisualStyleBackColor = true;
			// 
			// buttonExport
			// 
			this.buttonExport.Location = new System.Drawing.Point(12, 12);
			this.buttonExport.Name = "buttonExport";
			this.buttonExport.Size = new System.Drawing.Size(75, 23);
			this.buttonExport.TabIndex = 5;
			this.buttonExport.Text = "Export";
			this.buttonExport.UseVisualStyleBackColor = true;
			// 
			// ImportExportView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(219, 118);
			this.Controls.Add(this.buttonClose);
			this.Controls.Add(this.comboBoxImportOpt);
			this.Controls.Add(this.buttonImport);
			this.Controls.Add(this.buttonExport);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ImportExportView";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Import-Export";
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.ComboBox comboBoxImportOpt;
		private System.Windows.Forms.Button buttonImport;
		private System.Windows.Forms.Button buttonExport;
	}
}