﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.Toolbars.ToolbarExportImportSettings
{
	public partial class ImportExportView : Form
	{
		public ImportExportSettings Settings { get; private set; }
		public ImportExportView()
		{
			InitializeComponent();

			List<KeyValuePair<string, ImportRules>> data = new List<KeyValuePair<string, ImportRules>>();
			data.Add(new KeyValuePair<string, ImportRules>("Hinzu"/*"Append"*/, ImportRules.Append));
			data.Add(new KeyValuePair<string, ImportRules>("Ersetzen"/*"Replace"*/, ImportRules.Replace));
			comboBoxImportOpt.DataSource = data;
			comboBoxImportOpt.ValueMember = "Value";
			comboBoxImportOpt.DisplayMember = "Key";
			comboBoxImportOpt.SelectedIndex = 0;

			buttonClose.Click += (_, __) =>
			{
				Close();
			};
			buttonExport.Click += (_, __) =>
			{
				Settings = ImportExportSettings.GetExport();
				Close();
			};
			buttonImport.Click += (_, __) =>
			{

				Settings = ImportExportSettings.GetImport((ImportRules)comboBoxImportOpt.SelectedValue);
				Close();
			};
			SetGerman();
		}
		private void SetGerman()
		{
			Text = "Definitionen Import-Export";
			buttonClose.Text = "Schliessen";

		}
	}
}
