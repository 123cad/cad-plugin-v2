﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Toolbars.ToolbarExportImportSettings
{
	class ImportExportController
	{
		public static ImportExportSettings StartDialog()
		{
			ImportExportView view = new ImportExportView();
			view.ShowDialog();
			return view.Settings;
		}
	}
}
