﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Toolbars.ToolbarExportImportSettings
{
	public enum ImportRules
	{
		/// <summary>
		/// Deletes all existing types.
		/// </summary>
		Replace,
		/// <summary>
		/// Just add all imported.
		/// </summary>
		Append
	}
	public class ImportExportSettings
	{
		/// <summary>
		/// Is Imported.
		/// </summary>
		public bool Import { get; private set; }
		/// <summary>
		/// Is exported.
		/// </summary>
		public bool Export { get; private set; }
		/// <summary>
		/// If Importing define rules for import.
		/// </summary>
		public ImportRules Rules { get; private set; }
		private ImportExportSettings() {}
		public static ImportExportSettings GetExport(string suggestFileName = "")
		{
			return new ImportExportSettings() { Export = true};
		}
		public static ImportExportSettings GetImport(ImportRules rules)
		{
			return new ImportExportSettings() { Import = true, Rules = rules };
		}

	}
}
