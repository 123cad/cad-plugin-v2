﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace CadPlugin.Toolbars.ToolbarExportImportSettings
{
	class XmlConverter
	{
		private IEnumerable<Type> usedTypes;
		private List<XmlSerializer> serializers;
		public XmlConverter(IEnumerable<Type> usedTypes)
		{
			this.usedTypes = usedTypes;
			serializers = new List<XmlSerializer>();
			foreach (Type t in usedTypes)
				serializers.Add(new XmlSerializer(t));
		}
		public string ToXml(object obj, XmlWriter writer)
		{
			string xml = "";
			XmlSerializer serializer = new XmlSerializer(obj.GetType());
			//using (StringWriter sw = new StringWriter())
			{
				//using (XmlWriter xwr = XmlWriter.Create(sw))
				{
					serializer.Serialize(writer, obj);
					//xml = sw.ToString();
				}
				StringWriter sw = new StringWriter();
				XmlWriter wrt = XmlWriter.Create(sw);
				serializer.Serialize(wrt, obj);
				xml = sw.ToString();
			}
			return xml;
		}
		public object ToObject(string xml)
		{
			object obj = null;
			try
			{
				using (StringReader sReader = new StringReader(xml))
				{
					using (XmlReader xReader = XmlReader.Create(sReader))
					{
						foreach (XmlSerializer sr in serializers)
							if (sr.CanDeserialize(xReader))
							{
								obj = sr.Deserialize(xReader);
								break;
							}
					}
				}
			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.WriteLine(e);
				throw e;
			}
			return obj;
		}
	}
}
