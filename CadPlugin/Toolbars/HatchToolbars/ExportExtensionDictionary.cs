﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CadPlugin.HatchToolbars.Data;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.HatchToolbars
{
	class ExportExtensionDictionary
	{
		public static bool ExportToXML(Document doc, string fileName)
		{
			Database db = doc.Database;
			// Polyline which is monitored.
			PromptEntityOptions opt = new PromptEntityOptions("Select main polyline:");
			opt.SetRejectMessage("Not an entity!");
			PromptEntityResult res = doc.Editor.GetEntity(opt);
			bool valid = false;
			if (res.Status == PromptStatus.OK)
			{
				using (Transaction tr = db.TransactionManager.StartTransaction())
				{
					Entity ent = tr.GetObject(res.ObjectId, OpenMode.ForRead) as Entity;
					string type = ent.LoadExtensionString(tr, ExtensionDictionaryEntries.ObjectGroupTypeDescription);
					if (type != ExtensionDictionaryEntries.DescriptionAreaPolyline)
					{
						doc.Editor.WriteMessage("Polyline is not main polyline! Info: " + type);
						return false;
					}
					using (XmlTextWriter writer = new XmlTextWriter(fileName, Encoding.Unicode))
					{
						writer.Indentation = 1;
						writer.IndentChar = '\t';
						writer.Formatting = Formatting.Indented;

						writer.WriteStartDocument();
						writer.WriteStartElement("ExtensionDic");
						{
							writer.WriteStartElement("AreaPolyline");
							Polyline areaPl = tr.GetObject(res.ObjectId, OpenMode.ForRead, true) as Polyline;
							ExportSingleEntity(tr, areaPl, writer);
							writer.WriteEndElement();

							writer.WriteStartElement("AreaHatch");
							ObjectId? hatchId = areaPl.LoadFromExtensionDataObjectId(tr, db, ExtensionDictionaryEntries.AreaHatchLinkId);
							if (!hatchId.HasValue || hatchId.Value == ObjectId.Null)
								writer.WriteString("null");
							else
							{
								Hatch h = tr.GetObject(hatchId.Value, OpenMode.ForRead, true) as Hatch;
								if (h.IsErased)
									writer.WriteAttributeString("erased", "");
								ExportSingleEntity(tr, h, writer);
							}
							writer.WriteEndElement();

							writer.WriteStartElement("AttributeText");
							MText text = null;
							ObjectId? textId = areaPl.LoadFromExtensionDataObjectId(tr, db, ExtensionDictionaryEntries.AttributeTextLinkId);
							if (!textId.HasValue || textId.Value == ObjectId.Null)
								writer.WriteString("null");
							else
							{
								text = tr.GetObject(textId.Value, OpenMode.ForRead, true) as MText;
								if (text.IsErased)
									writer.WriteAttributeString("erased", "");
								ExportSingleEntity(tr, text, writer);
							}
							writer.WriteEndElement();
							if (text != null)
							{

								writer.WriteStartElement("AttributeHatch");
								hatchId = text.LoadFromExtensionDataObjectId(tr, db, ExtensionDictionaryEntries.AttributeHatchLinkId);
								if (!hatchId.HasValue || hatchId.Value == ObjectId.Null)
									writer.WriteString("null");
								else
								{
									Hatch h = tr.GetObject(hatchId.Value, OpenMode.ForRead, true) as Hatch;
									if (h.IsErased)
										writer.WriteAttributeString("erased", "");
									ExportSingleEntity(tr, h, writer);
								}
								writer.WriteEndElement();

								writer.WriteStartElement("AttributePolyline");
								ObjectId? plId = text.LoadFromExtensionDataObjectId(tr, db, ExtensionDictionaryEntries.AttributePolylineLinkId);
								if (!plId.HasValue || plId.Value == ObjectId.Null)
									writer.WriteString("null");
								else
								{
									Polyline h = tr.GetObject(plId.Value, OpenMode.ForRead, true) as Polyline;
									if (h.IsErased)
										writer.WriteAttributeString("erased", "");
									ExportSingleEntity(tr, h, writer);
								}
								writer.WriteEndElement();

								writer.WriteStartElement("Tables");
								Dictionary<string, TypedValue[]> extension = text.LoadExtensionData(tr);
								TypedValue[] values = extension[ExtensionDictionaryEntries.TableId];
								foreach(string tName in values.Select(x=>(string)x.Value))
								{
									DBDictionary nod = tr.GetObject(db.NamedObjectsDictionaryId, OpenMode.ForRead) as DBDictionary;
									if (!nod.Contains(ToolbarNodManager.ToolbarEntry))
										continue;
									DBDictionary toolbar = tr.GetObject(nod.GetAt(ToolbarNodManager.ToolbarEntry), OpenMode.ForRead) as DBDictionary;
									if (toolbar == null)
										continue;
									DBDictionary tables = tr.GetObject(toolbar.GetAt(ToolbarNodManager.Tables), OpenMode.ForRead) as DBDictionary;
									if (tables == null)
										continue;
									if (tables.GetAt(tName) == ObjectId.Null)
										continue;
									DBDictionary table = tr.GetObject(tables.GetAt(tName), OpenMode.ForRead) as DBDictionary;
									if (table != null)
									{
										writer.WriteStartElement(tName);
										ExportSingleDBDictionary(tr, table, writer);
										writer.WriteEndElement();
									}
								}
								writer.WriteEndElement();
							}


						}
						writer.WriteEndElement();
						writer.WriteEndDocument();
						valid = true;
					}

				}

			}
			return valid;
		}
		private static void ExportSingleDBDictionary(Transaction tr, DBDictionary dic, XmlTextWriter writer)
		{
			foreach(DBDictionaryEntry entry in dic)
			{
				DBObject obj = tr.GetObject(entry.Value, OpenMode.ForRead);
				Xrecord rec = obj as Xrecord;
				DBDictionary dbDic = obj as DBDictionary;
				if (rec != null)
				{
					writer.WriteStartElement(entry.Key);
					TypedValue[] values = rec.Data.AsArray();
					foreach (TypedValue val in values)
						writer.WriteAttributeString(((DxfCode)val.TypeCode).ToString(), val.Value.ToString());
					writer.WriteEndElement();
				}
				else if (dbDic != null)
				{
					writer.WriteStartElement(entry.Key);
					ExportSingleDBDictionary(tr, dbDic, writer);
					writer.WriteEndElement();
				}
			}
		}
		private static void ExportSingleEntity(Transaction tr, Entity ent, XmlTextWriter writer)
		{
			writer.WriteAttributeString("handle", ent.ObjectId.Handle.ToString());

			if (ent.ExtensionDictionary == ObjectId.Null)
			{
				writer.WriteElementString("msg", "ExtensionDitionary not found!");
			}
			else
			{
				DBDictionary dic = tr.GetObject(ent.ExtensionDictionary, OpenMode.ForRead) as DBDictionary;
				foreach (DBDictionaryEntry entry in dic)
				{
					writer.WriteStartElement(entry.Key);
					{
						if (entry.Value == ObjectId.Null)
							writer.WriteString("ObjectId.Null");
						else
						{
							Xrecord rec = tr.GetObject(entry.Value, OpenMode.ForRead) as Xrecord;
							if (rec == null)
								writer.WriteAttributeString("error", "handle=" + entry.Value.Handle + " no xrec");
							else
								if (rec.Data != null)
									foreach (TypedValue val in rec.Data.AsArray())
									{
										//writer.WriteStartElement("entry");
										writer.WriteAttributeString("type", val.TypeCode + ":" + ((DxfCode)val.TypeCode));
										writer.WriteAttributeString("val", val.Value.ToString());
										//writer.WriteEndElement();
									}
						}
					}
					writer.WriteEndElement();
				}
			}
		}
	}
}
