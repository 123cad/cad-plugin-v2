﻿namespace CadPlugin.HatchToolbars
{
	partial class SingleHatchView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.labelColor = new System.Windows.Forms.Label();
			this.labelAngle = new System.Windows.Forms.Label();
			this.labelSpacing = new System.Windows.Forms.Label();
			this.textBoxName = new System.Windows.Forms.TextBox();
			this.colorDialog1 = new System.Windows.Forms.ColorDialog();
			this.textBoxAngle = new System.Windows.Forms.TextBox();
			this.textBoxDistance = new System.Windows.Forms.TextBox();
			this.textBoxColor = new System.Windows.Forms.TextBox();
			this.buttonColorChange = new System.Windows.Forms.Button();
			this.buttonOk = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.labelTextSize = new System.Windows.Forms.Label();
			this.textBoxTextSize = new System.Windows.Forms.TextBox();
			this.labelTrasnparency = new System.Windows.Forms.Label();
			this.labelTransparencyLevel = new System.Windows.Forms.Label();
			this.trackBarTransparency = new System.Windows.Forms.TrackBar();
			this.groupBoxHatchData = new System.Windows.Forms.GroupBox();
			this.checkBoxCrossLines = new System.Windows.Forms.CheckBox();
			this.labelScale = new System.Windows.Forms.Label();
			this.textBoxHatchScale = new System.Windows.Forms.TextBox();
			this.comboBoxPatterns = new System.Windows.Forms.ComboBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			((System.ComponentModel.ISupportInitialize)(this.trackBarTransparency)).BeginInit();
			this.groupBoxHatchData.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(19, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(35, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Name";
			// 
			// labelColor
			// 
			this.labelColor.AutoSize = true;
			this.labelColor.Location = new System.Drawing.Point(19, 78);
			this.labelColor.Name = "labelColor";
			this.labelColor.Size = new System.Drawing.Size(31, 13);
			this.labelColor.TabIndex = 1;
			this.labelColor.Text = "Color";
			// 
			// labelAngle
			// 
			this.labelAngle.AutoSize = true;
			this.labelAngle.Location = new System.Drawing.Point(11, 80);
			this.labelAngle.Name = "labelAngle";
			this.labelAngle.Size = new System.Drawing.Size(34, 13);
			this.labelAngle.TabIndex = 2;
			this.labelAngle.Text = "Angle";
			// 
			// labelSpacing
			// 
			this.labelSpacing.AutoSize = true;
			this.labelSpacing.Location = new System.Drawing.Point(11, 107);
			this.labelSpacing.Name = "labelSpacing";
			this.labelSpacing.Size = new System.Drawing.Size(46, 13);
			this.labelSpacing.TabIndex = 3;
			this.labelSpacing.Text = "Spacing";
			// 
			// textBoxName
			// 
			this.textBoxName.Location = new System.Drawing.Point(82, 17);
			this.textBoxName.Name = "textBoxName";
			this.textBoxName.Size = new System.Drawing.Size(104, 20);
			this.textBoxName.TabIndex = 4;
			// 
			// textBoxAngle
			// 
			this.textBoxAngle.Location = new System.Drawing.Point(70, 77);
			this.textBoxAngle.Name = "textBoxAngle";
			this.textBoxAngle.Size = new System.Drawing.Size(39, 20);
			this.textBoxAngle.TabIndex = 5;
			this.textBoxAngle.Text = "0";
			// 
			// textBoxDistance
			// 
			this.textBoxDistance.Location = new System.Drawing.Point(70, 104);
			this.textBoxDistance.Name = "textBoxDistance";
			this.textBoxDistance.Size = new System.Drawing.Size(51, 20);
			this.textBoxDistance.TabIndex = 6;
			this.textBoxDistance.Text = "100.00";
			// 
			// textBoxColor
			// 
			this.textBoxColor.Enabled = false;
			this.textBoxColor.Location = new System.Drawing.Point(82, 75);
			this.textBoxColor.Name = "textBoxColor";
			this.textBoxColor.ReadOnly = true;
			this.textBoxColor.Size = new System.Drawing.Size(39, 20);
			this.textBoxColor.TabIndex = 7;
			this.textBoxColor.TabStop = false;
			// 
			// buttonColorChange
			// 
			this.buttonColorChange.Location = new System.Drawing.Point(131, 74);
			this.buttonColorChange.Name = "buttonColorChange";
			this.buttonColorChange.Size = new System.Drawing.Size(55, 23);
			this.buttonColorChange.TabIndex = 8;
			this.buttonColorChange.Text = "Change";
			this.buttonColorChange.UseVisualStyleBackColor = true;
			this.buttonColorChange.Click += new System.EventHandler(this.buttonColorChange_Click);
			// 
			// buttonOk
			// 
			this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonOk.Location = new System.Drawing.Point(111, 329);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(75, 23);
			this.buttonOk.TabIndex = 9;
			this.buttonOk.Text = "Ok";
			this.buttonOk.UseVisualStyleBackColor = true;
			this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(125, 107);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(21, 13);
			this.label5.TabIndex = 10;
			this.label5.Text = "cm";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(115, 80);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(11, 13);
			this.label6.TabIndex = 11;
			this.label6.Text = "°";
			// 
			// labelTextSize
			// 
			this.labelTextSize.AutoSize = true;
			this.labelTextSize.Location = new System.Drawing.Point(18, 47);
			this.labelTextSize.Name = "labelTextSize";
			this.labelTextSize.Size = new System.Drawing.Size(49, 13);
			this.labelTextSize.TabIndex = 12;
			this.labelTextSize.Text = "Text size";
			// 
			// textBoxTextSize
			// 
			this.textBoxTextSize.Location = new System.Drawing.Point(82, 44);
			this.textBoxTextSize.Name = "textBoxTextSize";
			this.textBoxTextSize.Size = new System.Drawing.Size(39, 20);
			this.textBoxTextSize.TabIndex = 13;
			// 
			// labelTrasnparency
			// 
			this.labelTrasnparency.AutoSize = true;
			this.labelTrasnparency.Location = new System.Drawing.Point(18, 110);
			this.labelTrasnparency.Name = "labelTrasnparency";
			this.labelTrasnparency.Size = new System.Drawing.Size(72, 13);
			this.labelTrasnparency.TabIndex = 14;
			this.labelTrasnparency.Text = "Transparency";
			// 
			// labelTransparencyLevel
			// 
			this.labelTransparencyLevel.AutoSize = true;
			this.labelTransparencyLevel.Location = new System.Drawing.Point(107, 110);
			this.labelTransparencyLevel.Name = "labelTransparencyLevel";
			this.labelTransparencyLevel.Size = new System.Drawing.Size(19, 13);
			this.labelTransparencyLevel.TabIndex = 15;
			this.labelTransparencyLevel.Text = "90";
			// 
			// trackBarTransparency
			// 
			this.trackBarTransparency.Location = new System.Drawing.Point(12, 126);
			this.trackBarTransparency.Maximum = 90;
			this.trackBarTransparency.Name = "trackBarTransparency";
			this.trackBarTransparency.Size = new System.Drawing.Size(174, 45);
			this.trackBarTransparency.TabIndex = 16;
			this.trackBarTransparency.TickStyle = System.Windows.Forms.TickStyle.None;
			this.trackBarTransparency.Value = 2;
			this.trackBarTransparency.ValueChanged += new System.EventHandler(this.trackBarTransparency_ValueChanged);
			// 
			// groupBoxHatchData
			// 
			this.groupBoxHatchData.Controls.Add(this.checkBoxCrossLines);
			this.groupBoxHatchData.Controls.Add(this.labelScale);
			this.groupBoxHatchData.Controls.Add(this.textBoxHatchScale);
			this.groupBoxHatchData.Controls.Add(this.comboBoxPatterns);
			this.groupBoxHatchData.Controls.Add(this.labelAngle);
			this.groupBoxHatchData.Controls.Add(this.textBoxAngle);
			this.groupBoxHatchData.Controls.Add(this.label6);
			this.groupBoxHatchData.Controls.Add(this.labelSpacing);
			this.groupBoxHatchData.Controls.Add(this.textBoxDistance);
			this.groupBoxHatchData.Controls.Add(this.label5);
			this.groupBoxHatchData.Location = new System.Drawing.Point(21, 164);
			this.groupBoxHatchData.Name = "groupBoxHatchData";
			this.groupBoxHatchData.Size = new System.Drawing.Size(164, 159);
			this.groupBoxHatchData.TabIndex = 17;
			this.groupBoxHatchData.TabStop = false;
			this.groupBoxHatchData.Text = "Hatch type";
			// 
			// checkBoxCrossLines
			// 
			this.checkBoxCrossLines.AutoSize = true;
			this.checkBoxCrossLines.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBoxCrossLines.Location = new System.Drawing.Point(12, 135);
			this.checkBoxCrossLines.Name = "checkBoxCrossLines";
			this.checkBoxCrossLines.Size = new System.Drawing.Size(76, 17);
			this.checkBoxCrossLines.TabIndex = 21;
			this.checkBoxCrossLines.Text = "Crosslines ";
			this.checkBoxCrossLines.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBoxCrossLines.UseVisualStyleBackColor = true;
			// 
			// labelScale
			// 
			this.labelScale.AutoSize = true;
			this.labelScale.Location = new System.Drawing.Point(11, 54);
			this.labelScale.Name = "labelScale";
			this.labelScale.Size = new System.Drawing.Size(34, 13);
			this.labelScale.TabIndex = 19;
			this.labelScale.Text = "Scale";
			// 
			// textBoxHatchScale
			// 
			this.textBoxHatchScale.Location = new System.Drawing.Point(70, 51);
			this.textBoxHatchScale.Name = "textBoxHatchScale";
			this.textBoxHatchScale.Size = new System.Drawing.Size(39, 20);
			this.textBoxHatchScale.TabIndex = 20;
			this.textBoxHatchScale.Text = "1";
			// 
			// comboBoxPatterns
			// 
			this.comboBoxPatterns.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
			this.comboBoxPatterns.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxPatterns.DropDownWidth = 139;
			this.comboBoxPatterns.FormattingEnabled = true;
			this.comboBoxPatterns.Location = new System.Drawing.Point(6, 19);
			this.comboBoxPatterns.MaxDropDownItems = 10;
			this.comboBoxPatterns.Name = "comboBoxPatterns";
			this.comboBoxPatterns.Size = new System.Drawing.Size(139, 21);
			this.comboBoxPatterns.TabIndex = 18;
			// 
			// SingleHatchView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(207, 365);
			this.Controls.Add(this.groupBoxHatchData);
			this.Controls.Add(this.trackBarTransparency);
			this.Controls.Add(this.labelTransparencyLevel);
			this.Controls.Add(this.labelTrasnparency);
			this.Controls.Add(this.textBoxTextSize);
			this.Controls.Add(this.labelTextSize);
			this.Controls.Add(this.buttonOk);
			this.Controls.Add(this.buttonColorChange);
			this.Controls.Add(this.textBoxColor);
			this.Controls.Add(this.textBoxName);
			this.Controls.Add(this.labelColor);
			this.Controls.Add(this.label1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SingleHatchView";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "SingleHatchView";
			this.TopMost = true;
			((System.ComponentModel.ISupportInitialize)(this.trackBarTransparency)).EndInit();
			this.groupBoxHatchData.ResumeLayout(false);
			this.groupBoxHatchData.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label labelColor;
		private System.Windows.Forms.Label labelAngle;
		private System.Windows.Forms.Label labelSpacing;
		private System.Windows.Forms.TextBox textBoxName;
		private System.Windows.Forms.ColorDialog colorDialog1;
		private System.Windows.Forms.TextBox textBoxAngle;
		private System.Windows.Forms.TextBox textBoxDistance;
		private System.Windows.Forms.TextBox textBoxColor;
		private System.Windows.Forms.Button buttonColorChange;
		private System.Windows.Forms.Button buttonOk;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label labelTextSize;
		private System.Windows.Forms.TextBox textBoxTextSize;
		private System.Windows.Forms.Label labelTrasnparency;
		private System.Windows.Forms.Label labelTransparencyLevel;
		private System.Windows.Forms.TrackBar trackBarTransparency;
		private System.Windows.Forms.GroupBox groupBoxHatchData;
		private System.Windows.Forms.Label labelScale;
		private System.Windows.Forms.TextBox textBoxHatchScale;
		private System.Windows.Forms.ComboBox comboBoxPatterns;
		private System.Windows.Forms.CheckBox checkBoxCrossLines;
		private System.Windows.Forms.ToolTip toolTip1;
	}
}