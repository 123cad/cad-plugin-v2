﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.HatchToolbars
{
	public class ExtensionDictionaryEntries
	{
		/// <summary>
		/// Every object which is created by toolbar, saves it's own handle to ExtensionDic.
		/// So when object is copied, it gets different ObjectID, and we can remove ExtensionDic.
		/// </summary>
		public static readonly string ObjectPersonalId = "123CAD_HT_PersonalHandle";
		public static readonly string ObjectGroupTypeDescription = "123CAD_HT_GroupObject";

		/// <summary>
		/// Bounding polyline around hatch.
		/// </summary>
		public static readonly string DescriptionAreaPolyline = "AreaPolyline";
		/// <summary>
		/// Hatch filling inside of area.
		/// </summary>
		public static readonly string DescriptionAreaHatch = "AreaHatch";
		/// <summary>
		/// Polyline around text.
		/// </summary>
		public static readonly string DescritptionAttributePolyline = "AttPolyline";
		/// <summary>
		/// Hatch inside of text bounding polyline.
		/// </summary>
		public static readonly string DescriptionAttributeHatch = "AttHatch";
		/// <summary>
		/// Attribute text.
		/// </summary>
		public static readonly string DescriptionAttributeText = "AttText";


		/// <summary>
		/// Entry in Extension dictionary which stores prefix value for attribute.
		/// </summary>
		public static readonly string AttributePrefix = "123CAD_HT_AttPrefix";
		/// <summary>
		/// Suffix value for attribute.
		/// </summary>
		public static readonly string AttributeSuffix = "123CAD_HT_AttSuffix";
		public static readonly string AttributeCurrentText = "123CAD_HT_AttCurrText";

		/// <summary>
		/// Reference to the attribute text.
		/// </summary>
		public static readonly string AttributeTextLinkId = "123CAD_HT_AttText_Link";
		/// <summary>
		/// Reference to the attribute hatch.
		/// </summary>
		public static readonly string AttributeHatchLinkId = "123CAD_HT_AttHatch_Link";
		public static readonly string AttributePolylineLinkId = "123CAD_HT_AttPoly_Link";
		public static readonly string AreaPolylineLinkId = "123CAD_HT_AreaPoly_Link";
		public static readonly string AreaHatchLinkId = "123CAD_HT_AreaHatch_Link";

		public static readonly string TableId = "123CAD_HT_Table";
	}
}
