﻿namespace CadPlugin.HatchToolbars
{
	partial class Toolbar
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Toolbar));
			this.dataGridView = new System.Windows.Forms.DataGridView();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewColumnSettings = new System.Windows.Forms.DataGridViewButtonColumn();
			this.buttonAddHatch = new System.Windows.Forms.Button();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.buttonCreateTable = new System.Windows.Forms.Button();
			this.buttonAppendTableRow = new System.Windows.Forms.Button();
			this.buttonRemoveArea = new System.Windows.Forms.Button();
			this.buttonSelectionSetSingle = new System.Windows.Forms.Button();
			this.buttonSelectionSetAll = new System.Windows.Forms.Button();
			this.buttonDeleteTable = new System.Windows.Forms.Button();
			this.buttonSelectAttributeTables = new System.Windows.Forms.Button();
			this.panelCommands = new System.Windows.Forms.Panel();
			this.buttonImportExport = new System.Windows.Forms.Button();
			this.buttonSettings = new System.Windows.Forms.Button();
			this.textBoxHelp = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.buttonToggleShow = new System.Windows.Forms.Button();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
			this.contextMenuStrip1.SuspendLayout();
			this.panelCommands.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// dataGridView
			// 
			this.dataGridView.AllowUserToAddRows = false;
			this.dataGridView.AllowUserToDeleteRows = false;
			this.dataGridView.AllowUserToResizeColumns = false;
			this.dataGridView.AllowUserToResizeRows = false;
			this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dataGridView.BackgroundColor = System.Drawing.Color.Gray;
			this.dataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView.ColumnHeadersVisible = false;
			this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.dataGridViewColumnSettings});
			dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Lime;
			dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridView.DefaultCellStyle = dataGridViewCellStyle4;
			this.dataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dataGridView.GridColor = System.Drawing.Color.Gray;
			this.dataGridView.Location = new System.Drawing.Point(0, 0);
			this.dataGridView.Margin = new System.Windows.Forms.Padding(10);
			this.dataGridView.Name = "dataGridView";
			this.dataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
			this.dataGridView.RowHeadersVisible = false;
			this.dataGridView.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.Red;
			this.dataGridView.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Yellow;
			this.dataGridView.RowTemplate.Height = 28;
			this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridView.Size = new System.Drawing.Size(190, 318);
			this.dataGridView.TabIndex = 0;
			// 
			// Column1
			// 
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray;
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gray;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
			this.Column1.DefaultCellStyle = dataGridViewCellStyle1;
			this.Column1.HeaderText = "Column1";
			this.Column1.Name = "Column1";
			this.Column1.Width = 30;
			// 
			// Column2
			// 
			this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle2.BackColor = System.Drawing.Color.Gray;
			dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
			dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gray;
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
			this.Column2.DefaultCellStyle = dataGridViewCellStyle2;
			this.Column2.HeaderText = "Column2";
			this.Column2.Name = "Column2";
			// 
			// dataGridViewColumnSettings
			// 
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gray;
			dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
			dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(2, 3, 2, 2);
			dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Gray;
			dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
			this.dataGridViewColumnSettings.DefaultCellStyle = dataGridViewCellStyle3;
			this.dataGridViewColumnSettings.HeaderText = "Column3";
			this.dataGridViewColumnSettings.Name = "dataGridViewColumnSettings";
			this.dataGridViewColumnSettings.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewColumnSettings.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.dataGridViewColumnSettings.Width = 30;
			// 
			// buttonAddHatch
			// 
			this.buttonAddHatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonAddHatch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonAddHatch.BackgroundImage")));
			this.buttonAddHatch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.buttonAddHatch.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonAddHatch.Location = new System.Drawing.Point(159, 323);
			this.buttonAddHatch.Name = "buttonAddHatch";
			this.buttonAddHatch.Size = new System.Drawing.Size(24, 24);
			this.buttonAddHatch.TabIndex = 2;
			this.buttonAddHatch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.buttonAddHatch.UseVisualStyleBackColor = true;
			this.buttonAddHatch.Click += new System.EventHandler(this.buttonAdd_Click);
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem});
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(108, 26);
			// 
			// deleteToolStripMenuItem
			// 
			this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
			this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
			this.deleteToolStripMenuItem.Text = "Delete";
			// 
			// buttonCreateTable
			// 
			this.buttonCreateTable.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonCreateTable.BackgroundImage")));
			this.buttonCreateTable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.buttonCreateTable.Location = new System.Drawing.Point(0, 36);
			this.buttonCreateTable.Name = "buttonCreateTable";
			this.buttonCreateTable.Size = new System.Drawing.Size(35, 35);
			this.buttonCreateTable.TabIndex = 0;
			this.buttonCreateTable.UseVisualStyleBackColor = true;
			this.buttonCreateTable.Click += new System.EventHandler(this.buttonCreateTable_Click);
			// 
			// buttonAppendTableRow
			// 
			this.buttonAppendTableRow.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonAppendTableRow.BackgroundImage")));
			this.buttonAppendTableRow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.buttonAppendTableRow.Location = new System.Drawing.Point(77, 36);
			this.buttonAppendTableRow.Name = "buttonAppendTableRow";
			this.buttonAppendTableRow.Size = new System.Drawing.Size(35, 35);
			this.buttonAppendTableRow.TabIndex = 1;
			this.buttonAppendTableRow.UseVisualStyleBackColor = true;
			this.buttonAppendTableRow.Click += new System.EventHandler(this.buttonAppendTableRow_Click);
			// 
			// buttonRemoveArea
			// 
			this.buttonRemoveArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonRemoveArea.BackgroundImage")));
			this.buttonRemoveArea.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.buttonRemoveArea.Location = new System.Drawing.Point(77, 77);
			this.buttonRemoveArea.Name = "buttonRemoveArea";
			this.buttonRemoveArea.Size = new System.Drawing.Size(35, 35);
			this.buttonRemoveArea.TabIndex = 2;
			this.buttonRemoveArea.UseVisualStyleBackColor = true;
			this.buttonRemoveArea.Click += new System.EventHandler(this.buttonRemoveArea_Click);
			// 
			// buttonSelectionSetSingle
			// 
			this.buttonSelectionSetSingle.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonSelectionSetSingle.BackgroundImage")));
			this.buttonSelectionSetSingle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.buttonSelectionSetSingle.Location = new System.Drawing.Point(155, 77);
			this.buttonSelectionSetSingle.Name = "buttonSelectionSetSingle";
			this.buttonSelectionSetSingle.Size = new System.Drawing.Size(35, 35);
			this.buttonSelectionSetSingle.TabIndex = 3;
			this.buttonSelectionSetSingle.UseVisualStyleBackColor = true;
			this.buttonSelectionSetSingle.Click += new System.EventHandler(this.buttonSelectionSetSingle_Click);
			// 
			// buttonSelectionSetAll
			// 
			this.buttonSelectionSetAll.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonSelectionSetAll.BackgroundImage")));
			this.buttonSelectionSetAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.buttonSelectionSetAll.Location = new System.Drawing.Point(155, 118);
			this.buttonSelectionSetAll.Name = "buttonSelectionSetAll";
			this.buttonSelectionSetAll.Size = new System.Drawing.Size(35, 35);
			this.buttonSelectionSetAll.TabIndex = 4;
			this.buttonSelectionSetAll.UseVisualStyleBackColor = true;
			this.buttonSelectionSetAll.Click += new System.EventHandler(this.buttonSelectionSetAll_Click);
			// 
			// buttonDeleteTable
			// 
			this.buttonDeleteTable.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonDeleteTable.BackgroundImage")));
			this.buttonDeleteTable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.buttonDeleteTable.Location = new System.Drawing.Point(0, 77);
			this.buttonDeleteTable.Name = "buttonDeleteTable";
			this.buttonDeleteTable.Size = new System.Drawing.Size(35, 35);
			this.buttonDeleteTable.TabIndex = 5;
			this.buttonDeleteTable.UseVisualStyleBackColor = true;
			this.buttonDeleteTable.Click += new System.EventHandler(this.buttonDeleteTable_Click);
			// 
			// buttonSelectAttributeTables
			// 
			this.buttonSelectAttributeTables.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonSelectAttributeTables.BackgroundImage")));
			this.buttonSelectAttributeTables.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.buttonSelectAttributeTables.Location = new System.Drawing.Point(155, 36);
			this.buttonSelectAttributeTables.Name = "buttonSelectAttributeTables";
			this.buttonSelectAttributeTables.Size = new System.Drawing.Size(35, 35);
			this.buttonSelectAttributeTables.TabIndex = 6;
			this.buttonSelectAttributeTables.UseVisualStyleBackColor = true;
			this.buttonSelectAttributeTables.Click += new System.EventHandler(this.buttonSelectAttributeTables_Click);
			// 
			// panelCommands
			// 
			this.panelCommands.Controls.Add(this.buttonImportExport);
			this.panelCommands.Controls.Add(this.buttonSettings);
			this.panelCommands.Controls.Add(this.textBoxHelp);
			this.panelCommands.Controls.Add(this.button1);
			this.panelCommands.Controls.Add(this.buttonSelectAttributeTables);
			this.panelCommands.Controls.Add(this.buttonDeleteTable);
			this.panelCommands.Controls.Add(this.buttonSelectionSetAll);
			this.panelCommands.Controls.Add(this.buttonSelectionSetSingle);
			this.panelCommands.Controls.Add(this.buttonRemoveArea);
			this.panelCommands.Controls.Add(this.buttonAppendTableRow);
			this.panelCommands.Controls.Add(this.buttonCreateTable);
			this.panelCommands.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelCommands.Location = new System.Drawing.Point(0, 0);
			this.panelCommands.Name = "panelCommands";
			this.panelCommands.Size = new System.Drawing.Size(190, 162);
			this.panelCommands.TabIndex = 3;
			// 
			// buttonImportExport
			// 
			this.buttonImportExport.Location = new System.Drawing.Point(155, 7);
			this.buttonImportExport.Name = "buttonImportExport";
			this.buttonImportExport.Size = new System.Drawing.Size(35, 23);
			this.buttonImportExport.TabIndex = 11;
			this.buttonImportExport.Text = ".xml";
			this.buttonImportExport.UseVisualStyleBackColor = true;
			this.buttonImportExport.Click += new System.EventHandler(this.buttonImportExport_Click);
			// 
			// buttonSettings
			// 
			this.buttonSettings.Location = new System.Drawing.Point(77, 8);
			this.buttonSettings.Name = "buttonSettings";
			this.buttonSettings.Size = new System.Drawing.Size(35, 23);
			this.buttonSettings.TabIndex = 9;
			this.buttonSettings.Text = "Opt";
			this.buttonSettings.UseVisualStyleBackColor = true;
			this.buttonSettings.Click += new System.EventHandler(this.buttonSettings_Click);
			// 
			// textBoxHelp
			// 
			this.textBoxHelp.BackColor = System.Drawing.SystemColors.Control;
			this.textBoxHelp.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBoxHelp.Location = new System.Drawing.Point(6, 118);
			this.textBoxHelp.Multiline = true;
			this.textBoxHelp.Name = "textBoxHelp";
			this.textBoxHelp.Size = new System.Drawing.Size(132, 39);
			this.textBoxHelp.TabIndex = 7;
			this.textBoxHelp.Text = "test row 1 should do a word wrap here, table should be seen";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(0, 7);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(35, 23);
			this.button1.TabIndex = 8;
			this.button1.Text = ".csv";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.buttonCsvSave_Click);
			// 
			// buttonToggleShow
			// 
			this.buttonToggleShow.BackgroundImage = global::CadPlugin.Properties.Resources.arrow_up;
			this.buttonToggleShow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.buttonToggleShow.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.buttonToggleShow.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			this.buttonToggleShow.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.buttonToggleShow.Location = new System.Drawing.Point(0, 533);
			this.buttonToggleShow.Name = "buttonToggleShow";
			this.buttonToggleShow.Size = new System.Drawing.Size(221, 23);
			this.buttonToggleShow.TabIndex = 4;
			this.buttonToggleShow.UseVisualStyleBackColor = true;
			this.buttonToggleShow.Click += new System.EventHandler(this.buttonToggleShow_Click);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
			this.splitContainer1.Location = new System.Drawing.Point(17, 11);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.dataGridView);
			this.splitContainer1.Panel1.Controls.Add(this.buttonAddHatch);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.panelCommands);
			this.splitContainer1.Size = new System.Drawing.Size(190, 516);
			this.splitContainer1.SplitterDistance = 350;
			this.splitContainer1.TabIndex = 5;
			// 
			// Toolbar
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(221, 556);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.buttonToggleShow);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximumSize = new System.Drawing.Size(800, 1000);
			this.MinimumSize = new System.Drawing.Size(237, 38);
			this.Name = "Toolbar";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Hatch Toolbar";
			this.TopMost = true;
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
			this.contextMenuStrip1.ResumeLayout(false);
			this.panelCommands.ResumeLayout(false);
			this.panelCommands.PerformLayout();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridView;
		private System.Windows.Forms.Button buttonAddHatch;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
		private System.Windows.Forms.DataGridViewButtonColumn dataGridViewColumnSettings;
		private System.Windows.Forms.Panel panelCommands;
		private System.Windows.Forms.Button buttonDeleteTable;
		private System.Windows.Forms.Button buttonSelectionSetAll;
		private System.Windows.Forms.Button buttonSelectionSetSingle;
		private System.Windows.Forms.Button buttonRemoveArea;
		private System.Windows.Forms.Button buttonAppendTableRow;
		private System.Windows.Forms.Button buttonCreateTable;
		private System.Windows.Forms.Button buttonSelectAttributeTables;
		private System.Windows.Forms.TextBox textBoxHelp;
		private System.Windows.Forms.Button buttonToggleShow;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button buttonSettings;
		private System.Windows.Forms.Button buttonImportExport;
	}
}