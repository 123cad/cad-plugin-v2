﻿using CadPlugin.HatchToolbars.Data.TableDatas;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.HatchToolbars.Persistence
{
	class DataPersistence
	{
		public static void SaveHatchData(IEnumerable<CadPlugin.HatchToolbars.SingleHatchData> data)
		{
			Properties.HatchToolbar.Default.HatchDataCollection = data.ToList();
			Properties.HatchToolbar.Default.Save();
		}
		public static List<CadPlugin.HatchToolbars.SingleHatchData> GetHatchData()
		{
			try
			{
				return Properties.HatchToolbar.Default.HatchDataCollection;
			}
			catch
			{
				return null;
			}
		}

		public static void SaveSingleHatchDialogHeight(int height)
		{
			Properties.HatchToolbar.Default.SingleHatchDialogHeight = height;
			Properties.HatchToolbar.Default.Save();
		}
		public static int GetSingleHatchDialogHeight()
		{
			return Properties.HatchToolbar.Default.SingleHatchDialogHeight;
		}

		public static void SaveSingleHatchDialogWidth(int width)
		{
			Properties.HatchToolbar.Default.SingleHatchDialogWidth= width;
			Properties.HatchToolbar.Default.Save();
		}
		public static int GetSingleHatchDialogWidth()
		{
			return Properties.HatchToolbar.Default.SingleHatchDialogWidth;
		}

		public static void SaveSingleHatchDialogPositionX(int positionX)
		{
			Properties.HatchToolbar.Default.SingleHatchDialogPositionX= positionX;
			Properties.HatchToolbar.Default.Save();
		}
		public static int GetSingleHatchDialogPositionX()
		{
			return Properties.HatchToolbar.Default.SingleHatchDialogPositionX;
		}

		public static void SaveSingleHatchDialogPositionY(int positionY)
		{
			Properties.HatchToolbar.Default.SingleHatchDialogPositionY= positionY;
			Properties.HatchToolbar.Default.Save();
		}
		public static int GetSingleHatchDialogPositionY()
		{
			return Properties.HatchToolbar.Default.SingleHatchDialogPositionY;
		}

		public static void SaveButtonsPanelCollapsed(bool collapsed)
		{
			Properties.HatchToolbar.Default.ButtonsPanelCollapsed = collapsed;
			Properties.HatchToolbar.Default.Save();
		}
		public static bool LoadButtonsPanelCollapsed()
		{
			return Properties.HatchToolbar.Default.ButtonsPanelCollapsed;
		}

		public static void SaveCsvLocation(string dir)
		{
			Properties.HatchToolbar.Default.CsvSaveFolder = dir;
			Properties.HatchToolbar.Default.Save();
		}
		public static string LoadCsvLocation()
		{
			return Properties.HatchToolbar.Default.CsvSaveFolder;
		}

		public static List<string> GetPrefixHistory()
		{
			List<string> l = new List<string>();
			if (Properties.HatchToolbar.Default.UserPrefixHistory != null)
				foreach (string s in Properties.HatchToolbar.Default.UserPrefixHistory)
					l.Add(s);
			return l;
		}
		public static void SavePrefixHistory(IList<string> collection)
		{
			StringCollection sc = new StringCollection();
			// Save only limited number of prefix texts.
			int count = collection.Count;
			if (count > 20)
				count = 20;
			for (int i = 0; i < count; i++)
				sc.Add(collection[i]);
			//sc.AddRange(collection.ToArray());
			Properties.HatchToolbar.Default.UserPrefixHistory = sc;
			Properties.HatchToolbar.Default.Save();
		}

		public static void SaveTableColumns(IEnumerable<TableColumn> columns)
		{
			List<TableColumnsProxy> list = columns.Select(x => 
											new TableColumnsProxy()
											{
												Name = x.Name,
												Id = x.ColumnId,
												Type = (int)x.Column
											}).ToList();
			Properties.HatchToolbar.Default.SelectedColumn = list;
			Properties.HatchToolbar.Default.Save();
		}
		public static List<TableColumn> LoadTableColumns()
		{
			if (Properties.HatchToolbar.Default.SelectedColumn == null)
				return new List<TableColumn>();
			List<TableColumn> columns = Properties.HatchToolbar.Default.SelectedColumn.Select(x =>
						new TableColumn(x.Id, (ColumnType)x.Type, x.Name)).ToList();
			return columns;
		}

		public static void SaveShowTableSumRow(bool sumRow)
		{
			Properties.HatchToolbar.Default.ShowTableSumRow = sumRow;
			Properties.HatchToolbar.Default.Save();
		}
		public static bool LoadShowTableSumRow()
		{
			return Properties.HatchToolbar.Default.ShowTableSumRow;
		}

	}

	[Serializable]
	public class TableColumnsProxy
	{
		public string Name { get; set; }
		public string Id { get; set; }
		public int Type { get; set; }
	}
}
