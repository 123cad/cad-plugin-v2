﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtilities.Geometry;


namespace CadPlugin.HatchToolbars.Data
{
	/// <summary>
	/// Calculates Hoehe and Breite.
	/// They can exist only when there is a rectangle with 4 vertices. And if rectangle 
	/// is not too much rotated (by horizontal line).
	/// Hoehe represents length of vertical line, Breite is length of horizontal line.
	/// </summary>
	class HoheBreiteCalculator
	{
		/// <summary>
		/// After confirming points form a rectangle, hoehe and breite are calculated.
		/// Returns true if values are calculated.
		/// </summary>
		/// <param name="hoehe">Median value of left and right side.</param>
		/// <param name="breite">Median value of top and bottom side</param>
		/// <param name="points">Must be exactly 4 points.</param>
		/// <returns></returns>
		public static bool Calculate(ref double hoehe, ref double breite, params Point3d[] points)
		{
			if (points == null)
				return false;
			if (points.Length != 4)
				return false;
			// First check if we have rectangle.
			// First top left point is found, and all others are aligned to maintain order.
			// Then apply algorithm to 2 cases (2nd point is to the right, or to the bottom)


			// Entity is rectangle if all sides are under angle 0<angle<Pi.
			Vector2d[] vectors = new Vector2d[4];
			vectors[0] = points[0].GetVectorTo(points[1]).GetAs2d();// points[0].GetAs2d().GetAsVector();
			vectors[1] = points[1].GetVectorTo(points[2]).GetAs2d();// GetAs2d().GetAsVector();
			vectors[2] = points[2].GetVectorTo(points[3]).GetAs2d();// .GetAs2d().GetAsVector();
			vectors[3] = points[3].GetVectorTo(points[0]).GetAs2d();// .GetAs2d().GetAsVector();

			// All angles must have same sign.
			double sign = 1;
			double angle = vectors[0].GetAngleDeg(vectors[1]);
			if (angle < 0)
				sign = -1;
			angle = sign * vectors[1].GetAngleDeg(vectors[2]);
			if (angle < 0)
				return false;
			angle = sign * vectors[2].GetAngleDeg(vectors[3]);
			if (angle < 0)
				return false;
			angle = sign * vectors[3].GetAngleDeg(vectors[0]);
			if (angle < 0)
				return false;

			// Here we know that we have rectangle.
			// Determine top and bottom sides.
			// Find highest point, and then check angle between vectors
			// which include this point to determine vector which is closest to xAxis.
			int highestPointIndex = 0;
			double highestY = points[0].Y;
			for (int i = 1; i < 4; i++)
				if (points[i].Y > points[highestPointIndex].Y)
				{
					highestPointIndex = i;
				}
			Vector2d v1 = vectors[(highestPointIndex + 4 - 1) % 4];// +4 is to protect from (-1)%4
			Vector2d v2 = vectors[highestPointIndex];

			if (v1.X < 0)
				v1 = new Vector2d(-v1.X, v1.Y);
			if (v2.X < 0)
				v2 = new Vector2d(-v2.X, v2.Y);
			Vector2d xAxis = new Vector2d(1, 0);
			double angle1 = Math.Abs(xAxis.GetAngleDeg(v1));
			double angle2 = Math.Abs(xAxis.GetAngleDeg(v2));
			if (angle1 > 90)
				angle1 = 180 - angle1;
			if (angle2 > 90)
				angle2 = 180 - angle2;

			int horizontalVectorIndex = -1;
			if (angle1 < angle2)
				horizontalVectorIndex = (highestPointIndex + 4 - 1) % 4;
			else
				horizontalVectorIndex = highestPointIndex;
			breite = (vectors[horizontalVectorIndex].Length + vectors[(horizontalVectorIndex + 2) % 4].Length) / 2;
			hoehe = (vectors[(horizontalVectorIndex + 1) % 4].Length + vectors[(horizontalVectorIndex + 1 + 2) % 4].Length) / 2;

			return true;
		}
	}
}
