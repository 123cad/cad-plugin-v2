﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.HatchToolbars.Data
{
	[Serializable]
	/// <summary>
	/// Line hatch which can be crossed. User defines angle and distance between lines.
	/// </summary>
	public class UserDefined : HatchType
	{
		public override string PatternName
		{
			get
			{
				// Autocad user defined hatch must be marked by _U.
				// Bricscad works with anything.
				return "_U";
			}

			set
			{
			}
		}
		/// <summary>
		/// Angle of the Hatch in degrees.
		/// </summary>
		public double AngleDeg { get; set; }
		/// <summary>
		/// Spacing between lines.
		/// </summary>
		public double Spacing { get; set; }
		/// <summary>
		/// Tells if lines are crossing under 90deg angle.
		/// </summary>
		public bool CrossLines { get; set; }

		public UserDefined()
		{
			Spacing = 0.3;
		}
		private UserDefined(UserDefined d) : base(d)
		{
			AngleDeg = d.AngleDeg;
			Spacing = d.Spacing;
			CrossLines = d.CrossLines;
		}
		protected override void SetHatchPattern(Hatch h)
		{
			h.SetHatchPattern(HatchPatternType.UserDefined, PatternName);
			h.PatternAngle = MyUtilities.Helpers.Calculations.DegreesToRadians(AngleDeg);
			h.PatternScale = Spacing;
			h.PatternDouble = CrossLines;
		}
		public override HatchType Clone()
		{
			return new UserDefined(this);
		}
	}
}
