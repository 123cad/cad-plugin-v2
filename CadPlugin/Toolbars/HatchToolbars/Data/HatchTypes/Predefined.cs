﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.HatchToolbars.Data
{
	[Serializable]
	/// <summary>
	/// Hatch patterns already loaded to CAD (custom and predefined).
	/// </summary>
	public class Predefined :HatchType
	{
		public double AngleDeg { get; set; }
		public double Scale { get; set; }
		protected virtual HatchPatternType hatchType { get { return HatchPatternType.PreDefined; } }
		public Predefined()
		{
			Scale = 1;
		}
		protected Predefined(Predefined p):base(p)
		{
			AngleDeg = p.AngleDeg;
			Scale = p.Scale;
		}
		protected override void SetHatchPattern(Hatch h)
		{
			h.SetHatchPattern(hatchType, PatternName);
			h.PatternAngle = MyUtilities.Helpers.Calculations.DegreesToRadians(AngleDeg);
			h.PatternScale = Scale;
		}
		public override HatchType Clone()
		{
			return new Predefined(this);
		}
	}
	[Serializable]

	public class Custom:Predefined
	{
		public Custom():base()
		{

		}
		private Custom(Custom c):base(c)
		{

		}
		protected override HatchPatternType hatchType
		{
			get
			{
				return HatchPatternType.CustomDefined;
			}
		}
		public override HatchType Clone()
		{
			return new Custom(this);
		}
	}
}
