﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.HatchToolbars.Data
{
	[Serializable]
	public class Solid : HatchType
	{
		public override string PatternName
		{
			get
			{
				return "SOLID";
			}

			set
			{
			}
		}
		public Solid()
		{

		}
		private Solid(Solid s):base(s)
		{

		}
		protected override void SetHatchPattern(Hatch h)
		{
			h.SetHatchPattern(HatchPatternType.PreDefined, PatternName);
		}
		public override HatchType Clone()
		{
			return new Solid(this);
		}
	}
}
