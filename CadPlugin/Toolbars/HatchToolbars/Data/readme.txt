﻿3 main groups: AREA, ATTRIBUTE, TABLE

AREA: arPolyline(main object), arHatch
ATTRIBUTE: attPolyline, attHatch, attText (main object)
TABLE: 

*(main object) means that it is used as a reference from another group.

arPolyline -> arHatch, attText
arHatch -> arPolyline

attPolyline -> attText
attHatch -> attText
attText -> arPolyline, attPolyline, attHatch, tables

Objects are identified by these rules (if there is need for, better system
can be used, where any object has its identification string [is it area pl, area hatch....])