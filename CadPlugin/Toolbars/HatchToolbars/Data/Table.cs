﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.HatchToolbars.Data.TableDatas;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.HatchToolbars.Data
{
	partial class Table
	{
		/// <summary>
		/// Unique id for this table.
		/// </summary>
		public string Id { get; private set; }

		public TransactionData Transaction { get; private set; }
		private List<Attribute> __Attributes = null;
		public IList<Attribute> Attributes
		{
			get
			{
				if (__Attributes == null)
					throw new InvalidOperationException("Table attributes have not been loaded!");
				return __Attributes;
			}
			set
			{
				__Attributes = (List<Attribute>)value;
			}
		}
		private LoadedTableEntities __Entities = null;
		/// <summary>
		/// Works as a proxy (loaded when Refresh called or upon first usage).
		/// Not loading multiple times, if refresh called multiple times.
		/// </summary>
		private LoadedTableEntities Entities
		{
			get
			{
				if (__Entities == null)
				{
					//__Entities = new LoadedTableEntities();
					Refresh();
				}
				return __Entities;
			}
		}
		public Polyline Boundary
		{
			get
			{
				return Entities.Boundary;
			}
		}
		/// <summary>
		/// Settings related to columns, etc... For existing tables, loaded from NOD.
		/// </summary>
		private TableSettings tableSettings { get; set; }
		/// <summary>
		/// Holds reference to entry in NOD for the Table.
		/// </summary>
		private DBDictionary nodEntry;
		private Table(TransactionData data, TableSettings tableSettings)
		{
			Transaction = data;
			Attributes = new List<Attribute>();
			this.tableSettings = tableSettings.Clone();
			Id = AssignNewId(data, tableSettings);
			nodEntry = ToolbarNodManager.GetTable(Transaction, Id);
			if (nodEntry == null)
				throw new ApplicationException("Table doesn't exist in nod!");
		}
		private Table(TransactionData data, string id)
		{
			Id = id;
			Transaction = data;
			Attributes = new List<Attribute>();
			nodEntry = ToolbarNodManager.GetTable(Transaction, Id);
			if (nodEntry == null)
				throw new ApplicationException("Table doesn't exist in nod!");
			this.tableSettings = ToolbarNodManager.LoadTableSettings(data, nodEntry);
			if (tableSettings == null)
				throw new NullReferenceException("TableSettings for existing table not found!");
		}
		public string GetTableName()
		{
			return Entities.Header.Text;
		}
		/// <summary>
		/// Loads all related attributes.
		/// Has to be done like this because of circular dependency.
		/// </summary>
		public void InitializeAttributes()
		{
			Attributes = new List<Attribute>();
			List<string> attIds = ToolbarNodManager.GetAllRowIds(Transaction, nodEntry);
			foreach (string sId in attIds)
			{
				Handle h = sId.ConvertToHandle().Value;// new Handle(long.Parse(sId));
				ObjectId oid = Transaction.Database.GetObjectId(false, h, 0);
				Attribute at = Transaction.ObjectManager.GetAttribute(oid);
				if (at != null)
					Attributes.Add(at);
			}
		}
		/// <summary>
		/// Updates any text in the table!
		/// </summary>
		/// <param name="text"></param>
		public void TableUpdated(MText text)
		{
			// If header text, update header.
			Xrecord rec = Transaction.Transaction.GetObject(nodEntry.GetAt(ToolbarNodManager.TableHeader), OpenMode.ForRead) as Xrecord;
			TypedValue[] values = rec.Data.AsArray();
			ObjectId headerId = values[0].GetObjectId(Transaction.Database);
			if (headerId == text.ObjectId)
			{
				values[1] = new TypedValue((int)DxfCode.Text, text.Contents);
				rec.UpgradeOpen();
				rec.Data = new ResultBuffer(values);
				Refresh();
				return;
			}

			string attId = text.LoadExtensionString(Transaction.Transaction, ExtensionDictionaryEntries.AttributeTextLinkId);
			if (string.IsNullOrEmpty(attId))
				return;
			DBDictionary rowsDic = Transaction.Transaction.GetObject(nodEntry.GetAt(ToolbarNodManager.Rows), OpenMode.ForRead) as DBDictionary;
			DBDictionary rowDic = Transaction.Transaction.GetObject(rowsDic.GetAt(attId), OpenMode.ForRead) as DBDictionary;
			if (rowDic == null)
			{
				System.Diagnostics.Debug.WriteLine("Row not found in the table!");
				return;
			}
			rec = Transaction.Transaction.GetObject(rowDic.GetAt(ToolbarNodManager.RowText), OpenMode.ForRead) as Xrecord;
			values = rec.Data.AsArray();
			ObjectId oid = values[0].GetObjectId(Transaction.Database);
			if (oid == text.ObjectId)
			{
				// First column is edited, update attribute prefix.
				Handle h = attId.ConvertToHandle().Value;//new Handle(long.Parse(attId));
				ObjectId attOId = Transaction.Database.GetObjectId(false, h, 0);
				Attribute at = Transaction.ObjectManager.GetAttribute(attOId);
				at.SetNewPrefix(text.Contents);
			}
			Refresh();

			// If first column text, update attribute.
			// Others doesn't matter.

		}
		/// <summary>
		/// Changes the name of the table.
		/// </summary>
		/// <param name="newName"></param>
		public void ChangeName(string newName)
		{
			Xrecord rec = Transaction.Transaction.GetObject(nodEntry.GetAt(ToolbarNodManager.TableHeader), OpenMode.ForWrite) as Xrecord;
			TypedValue[] values = rec.Data.AsArray();
			MText text = Transaction.Transaction.GetObject(values[0].GetObjectId(Transaction.Database), OpenMode.ForWrite) as MText;
			string currentText = text.Contents;
			text.Contents = newName;
			rec.Data = new ResultBuffer(values[0], new TypedValue((int)DxfCode.Text, newName));

			rec.Dispose();
			text.Dispose();
			if (newName.Length > currentText.Length * 1.5)
				Refresh();
		}
		public void AddAttribute(Attribute at, bool refresh = true)
		{
			if (Attributes.Contains(at))
				return;
			Attributes.Add(at);
			at.AddTable(this); 
			ToolbarNodManager.AddNewRow(Transaction, nodEntry, at.MainEntity.ObjectId.Handle.Value.ToString());
			if (refresh)
				Refresh();
		}
		public void RemoveAttribute(Attribute at)
		{
			if (!Attributes.Contains(at))
				return;
			Attributes.Remove(at);
			at.RemoveTable(this);
			ToolbarNodManager.RemoveRows(Transaction, nodEntry, at.MainEntity.ObjectId.Handle.Value.ToString());
			Refresh();
		}
		/// <summary>
		/// Returns Attribute for specified MText which exists in table.
		/// </summary>
		public ObjectId GetAttributeId(MText text)
		{
			ObjectId oid = ObjectId.Null;
			var v = text.LoadExtensionString(Transaction.Transaction, ExtensionDictionaryEntries.AttributeTextLinkId);
			if (string.IsNullOrEmpty(v))
				return oid;
			Handle? h = v.ConvertToHandle();
			if (!h.HasValue)
				return oid;
			oid = Transaction.Database.GetObjectId(false, h.Value, 0);
			return oid;
		}
		public IEnumerable<ObjectId> GetAllAttributeIds()
		{
			List<ObjectId> oids = new List<ObjectId>();
			List<string> ids = ToolbarNodManager.GetAllRowIds(Transaction, nodEntry);
			foreach (string s in ids)
			{
				Handle h = s.ConvertToHandle().Value;// new Handle(long.Parse(s));
				ObjectId id = Transaction.Database.GetObjectId(false, h, 0);
				oids.Add(id);
			}
			return oids;
		}
		public IEnumerable<Entity> GetAllEntities()
		{
			return Entities.GetAll();
		}

		public void Delete()
		{
			foreach(Attribute at in Attributes)
			{
				at.RemoveTable(this);
			}
			ToolbarNodManager.RemoveRows(Transaction, nodEntry, Attributes.Select(x => x.MainEntity.ObjectId.Handle.ConvertToString()).ToArray());
			Attributes.Clear();
			Attributes = null;
			ToolbarNodManager.DeleteTable(Transaction, Id);
		}
		/// <summary>
		/// Checks only object type.
		/// </summary>
		public static bool IsTableObjectType(Entity ent)
		{
			return ent is Line || ent is Polyline || ent is MText;
		}
		/// <summary>
		/// Checks whether entity belongs to actual table.
		/// </summary>
		/// <param name="ent"></param>
		/// <returns></returns>
		public static bool IsTableEntity(TransactionData data, Entity ent)
		{
			return ToolbarNodManager.IsTableEntity(data, ent);
		}
		/// <summary>
		/// Reads table id from the entity.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="ent"></param>
		/// <returns></returns>
		public static string GetTableId(TransactionData data, Entity ent)
		{
			string s = ent.LoadExtensionString(data.Transaction, ExtensionDictionaryEntries.TableId);
			if (string.IsNullOrEmpty(s))
				return s;
			DBDictionary tables = data.ToolbarEntry.OpenSubDictionary(data.Transaction, ToolbarNodManager.Tables);
				//CADCommonHelper.GetSubDictionary(data.Transaction, data.ToolbarEntry, ToolbarNodManager.Tables);
			if (tables == null)
				return null;

			return s;
		}
		private static string AssignNewId(TransactionData tr, TableSettings tableSetting)
		{
			string id = ToolbarNodManager.CreateAndInitializeTableId(tr, tableSetting);
			return id;
		}
		public static Table Create(TransactionData data, IEnumerable<Attribute> attributes, TableSettings tableSetting)
		{
			Table t = new Table(data, tableSetting);
			t.tableSettings = tableSetting.Clone();
			foreach (Attribute at in attributes)
				t.AddAttribute(at, false);
			t.Refresh();
			return t;
		}
		public static Table Load(TransactionData data, string id, bool openErased = false)
		{
			DBDictionary tables = data.ToolbarEntry.OpenSubDictionary(data.Transaction, ToolbarNodManager.Tables);
				//CADCommonHelper.GetSubDictionary(data.Transaction, data.ToolbarEntry, ToolbarNodManager.Tables);
			if (tables != null && !tables.Contains(id))
				return null;
			Table t = new Table(data, id);
			return t;
		}

		public override bool Equals(object obj)
		{
			Table t = obj as Table;
			if (t == null)
				return false;
			return t.Id == Id;
		}
		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}
		public override string ToString()
		{
			return "Table: " + Id;
		}



		private class LoadedTableEntities:IDisposable
		{
			public Polyline Boundary { get; set; }
			public MText Header { get; set; }
			/// <summary>
			/// Line between header and column headers.
			/// </summary>
			public Line HeaderLine { get; set; }
			public List<Line> ColumnLines { get; set; }
			public List<MText> ColumnHeaderTexts { get; set; }
			public List<LoadedRow> Rows { get; set; }
			public List<MText> RowSumTexts { get; set; }
			public Line RowSumLine { get; set; }
			
			public LoadedTableEntities()
			{
				ColumnLines = new List<Line>();
				ColumnHeaderTexts = new List<MText>();
				Rows = new List<LoadedRow>();
				RowSumTexts = new List<MText>();
			}
			public void Dispose()
			{
				Boundary.Dispose();
				Header.Dispose();
				HeaderLine.Dispose();
				foreach (var v in ColumnLines)
					v.Dispose();
				foreach (var v in ColumnHeaderTexts)
					v.Dispose();
				foreach (var v in Rows)
					v.Dispose();
				Boundary = null;
				Header = null;
				HeaderLine = null;
				ColumnLines = null;
				ColumnHeaderTexts = null;
				Rows = null;
				ColumnLines = null;
				RowSumLine = null;
			}
			public IEnumerable<Entity> GetAll()
			{
				List<Entity> entities = new List<Entity>();
				entities.Add(Boundary);
				entities.Add(Header);
				entities.Add(HeaderLine);
				entities.AddRange(ColumnLines);
				entities.AddRange(ColumnHeaderTexts);
				entities.AddRange(Rows.SelectMany(x => x.GetAllEntities()).ToArray());
				if (RowSumTexts.Count > 0)
				{
					entities.AddRange(RowSumTexts);
					entities.Add(RowSumLine);
				}
				return entities;
			}
		}
	}
}
