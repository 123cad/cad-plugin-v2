﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.HatchToolbars.Data.TableDatas
{
	/// <summary>
	/// Determines which data is associated with column.
	/// </summary>
	enum ColumnType
	{
		/// <summary>
		/// Header column.
		/// </summary>
		Header = 1,
		/// <summary>
		/// Surface of closed entity.
		/// </summary>
		Area, 
		/// <summary>
		/// Length of closed entity (for square it is 4 * a)
		/// </summary>
		Length,
		/// <summary>
		/// Height.
		/// </summary>
		Hoehe,
		/// <summary>
		/// ?
		/// </summary>
		Breite
	}
}
