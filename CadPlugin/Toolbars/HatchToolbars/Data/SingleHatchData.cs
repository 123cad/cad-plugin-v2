﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.HatchToolbars.Data;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.HatchToolbars
{
	public class SingleHatchData
	{
		public string Name { get; set; }

		public double TextSize { get; set; }
		public HatchType HatchType { get; set; }
		public SingleHatchData()
		{
			Name = "[Flächen-Stempel]";// "[Hatch]";
			TextSize = 1;
			HatchType = new UserDefined();
		}
		public SingleHatchData(SingleHatchData data)
		{
			Name = data.Name;
			TextSize = data.TextSize;
			HatchType = data.HatchType.Clone();
		}
		public void ApplyPattern(Hatch h)
		{
			HatchType.ApplyPattern(h);
		}
		public void ApplyPattern(Hatch h, Polyline boundary)
		{
			HatchType.ApplyPattern(h);
			h.AppendLoop(HatchLoopTypes.Outermost, new ObjectIdCollection(new ObjectId[] { boundary.ObjectId }));

			// Not sure if needed.
			//h.EvaluateHatch(true);
		}
	}
}
