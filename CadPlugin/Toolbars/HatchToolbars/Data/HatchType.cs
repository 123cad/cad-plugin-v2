﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using System.Xml.Serialization;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;

#endif

namespace CadPlugin.HatchToolbars.Data
{
	[Serializable]
	[XmlInclude(typeof(Predefined))]
	[XmlInclude(typeof(Custom))]
	[XmlInclude(typeof(Solid))]
	[XmlInclude(typeof(UserDefined))]
	public abstract class HatchType
	{
		private int __Transparency;
		/// <summary>
		/// Transparency level [0-90] (defined by CAD). 0 - not transparent, 90 - completely transparent.
		/// NOTE: However, struct Transparency is more like opacity (inverse value from transparency), 
		/// so when instantiating Transparency it is needed to invert the desired value (percentage).
		/// </summary>
		public int TransparencyLevel {
			get { return __Transparency; }
			set
			{
				__Transparency = value;
				if (value < 0)
					__Transparency = 0;
				else if (value > 90)
					__Transparency = 90;
			}
		}
		/// <summary>
		/// Color of the hatch lines.
		/// </summary>
		public ColorPersistence PatternColor { get; set; }

		/// <summary>
		/// Name of the hatch in CAD.
		/// </summary>
		public virtual string PatternName { get; set; }

		protected HatchType()
		{
			PatternColor = new ColorPersistence(ToolbarColorSettings.Instance.TextColor);
		}
		protected HatchType(HatchType t)
		{
			PatternColor = t.PatternColor;
			PatternName = t.PatternName;
			TransparencyLevel = t.TransparencyLevel;
		}
		/// <summary>
		/// Creates pattern for provided hatch.
		/// </summary>
		public void ApplyPattern(Hatch h)
		{
			SetHatchPattern(h);
			int transparency = 255 - (int)(TransparencyLevel * (255 / 100.0));
			if (transparency > 255)
				transparency = 255;
			h.Transparency = new Transparency((byte)transparency);
			h.Color = Color.FromColor(PatternColor.ToColor());
			h.Associative = true;
		}
		protected abstract void SetHatchPattern(Hatch h);
		public abstract HatchType Clone();
	}

}
