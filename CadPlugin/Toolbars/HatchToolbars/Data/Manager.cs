﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.HatchToolbars.Data
{
	class Manager
	{
		private TransactionData Transaction;
		private Dictionary<string, Table> Tables = new Dictionary<string, Table>();
		private Dictionary<ObjectId, Area> Areas = new Dictionary<ObjectId, Area>();
		private Dictionary<ObjectId, Attribute> Attributes = new Dictionary<ObjectId, Attribute>();
		public Manager(TransactionData transaction)
		{
			Transaction = transaction;
			
		}

		/// <summary>
		/// Checks only Attributes that have been previously loaded.
		/// </summary>
		/// <param name="id">ObjectId of main entity.</param>
		/// <returns></returns>
		public Attribute GetAttribute(ObjectId id)
		{
			if (Attributes.ContainsKey(id))
				return Attributes[id];
			Entity ent = Transaction.Transaction.GetObject(id, OpenMode.ForWrite) as Entity;
			if (ent == null)
				return null;
			 Area ar = Load(ent);
			if (ar == null)
				return null;
			return ar.Attribute;
		}
		/// <summary>
		/// Checks only Areas that have been previously loaded.
		/// </summary>
		/// <param name="id">ObjectId of main entity.</param>
		/// <returns></returns>
		public Area GetArea(ObjectId id)
		{
			if (Areas.ContainsKey(id))
				return Areas[id];
			Entity ent = Transaction.Transaction.GetObject(id, OpenMode.ForWrite) as Entity;
			if (ent == null)
				return null;
			Area ar = Load(ent);
			if (ar == null)
				return null;
			return ar;
		}
		/// <summary>
		/// Returns only tables that have been loaded previously.
		/// </summary>
		/// <returns></returns>
		public Table GetTable(string id)
		{
			if (Tables.ContainsKey(id))
				return Tables[id];
			Table t = LoadExistingTable(id);
			if (t == null)
				return null;
			return t;
		}
		/// <summary>
		/// Creates new Attribute for Area.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="areaBoundary"></param>
		/// <param name="hatchData"></param>
		/// <returns></returns>
		public Area CreateNew(Polyline areaBoundary, SingleHatchData hatchData)
		{
			Area ar = Area.Create(Transaction, areaBoundary, hatchData);
			Attribute at = Attribute.Create(Transaction, hatchData);
			ar.AddNewAttribute(at);
			at.AddNewArea(ar);
			at.InitializeTables();
			
			Areas.Add(ar.MainEntity.ObjectId, ar);
			Attributes.Add(at.MainEntity.ObjectId, at);

			return ar;
		}
		/// <summary>
		/// Determines type of object, and loads all related objects accordingly.
		/// </summary>
		/// <param name="data"></param>
		/// <param name=""></param>
		/// <param name="openErased">When delete is needed, load object and delete it.</param>
		/// <returns></returns>
		public Area Load(Entity ent, bool openErased = false)
		{
			if (!IsObjectType(ent))
				return null;
			Attribute at = null;
			Area ar = null; 
			MText text;
			Polyline pl;
			text = Attribute.GetMainObject(Transaction, ent, openErased);
			pl = Area.GetMainObject(Transaction, ent, openErased);
			if (text != null && Attributes.ContainsKey(text.ObjectId))
				return Attributes[text.ObjectId].Area;
			if (pl != null && Areas.ContainsKey(pl.ObjectId))
				return Areas[pl.ObjectId];
			if (text == null)
			{
				pl = Area.GetMainObject(Transaction, ent, openErased);
				if (pl == null)
					return null;
				ar = Area.Load(Transaction, pl, openErased);
				if (ar == null)
					return null;
				ObjectId textId = ar.GetAttributeId();
				if (textId == ObjectId.Null)
					return null;
				text = Transaction.Transaction.GetObject(textId, OpenMode.ForWrite, openErased) as MText;
			}
			else
			{
				at = Attribute.Load(Transaction, text, openErased);
				if (at == null)
					return null;
				ObjectId plId = at.GetAreaId();
				if (plId == ObjectId.Null)
					return null;
				pl = Transaction.Transaction.GetObject(plId, OpenMode.ForWrite, openErased) as Polyline;
			}
			if (at == null)
				at = Attribute.Load(Transaction, text, openErased);
			if (ar == null)
				ar = Area.Load(Transaction, pl, openErased);

			Areas.Add(ar.MainEntity.ObjectId, ar);
			Attributes.Add(at.MainEntity.ObjectId, at);

			at.AddExistingArea(ar);
			ar.AddExistingAttribute(at);

			at.InitializeTables();
			return ar;
		}
		public void DeleteAttribute(Entity ent)
		{
			Area ar = Load(ent, true);
			if (ar != null)
			{
				foreach (Table t in ar.Attribute.Tables.ToArray())
					t.RemoveAttribute(ar.Attribute);
				Areas.Remove(ar.MainEntity.ObjectId);
				Attributes.Remove(ar.Attribute.MainEntity.ObjectId);
				ar.DeleteAttribute();
			}
		}

		/// <summary>
		/// Creates new table and adds it to the nod.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="atts"></param>
		/// <returns></returns>
		public Table CreateNewTable(IEnumerable<Attribute> atts, TableDatas.TableSettings settings)
		{
			Table t = Table.Create(Transaction, atts, settings);
			Tables.Add(t.Id, t);
			return t;
		}
		/// <summary>
		/// Loads existing table from the nod.
		/// </summary>
		public Table LoadExistingTable(string id)
		{
			if (Tables.ContainsKey(id))
				return Tables[id];
			Table t = Table.Load(Transaction, id);
			if (t != null)
			{
				Tables.Add(id, t);
				t.InitializeAttributes();
			}
			
			return t;
		}

		/// <summary>
		/// Tells if object type is used (first level of filtering).
		/// If object type is used, then ExtensionDictionary tells if object is actually used, or not.
		/// </summary>
		private static bool IsObjectType(Entity ent)
		{
			if (ent.ExtensionDictionary == ObjectId.Null)
				return false;
			if (Area.IsAreaObjectType(ent) || Attribute.IsAttributeObjectType(ent))
				return true;
			return false;
		}
	}
}
