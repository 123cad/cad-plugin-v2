﻿using CadPlugin.HatchToolbars.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.HatchToolbars
{
	public partial class SingleHatchView : Form
	{

		/// <summary>
		/// Patterns from default CAD .pat file.
		/// </summary>
		private static List<string> defaultLoadedPatterns;
		/// <summary>
		/// Patterns from custom .pat files.
		/// </summary>
		private static List<string> customLoadedPatterns;

		private SingleHatchData data;
		private ErrorProvider error = null;
		internal SingleHatchView(SingleHatchData d)
		{
			InitializeComponent();
			error = new ErrorProvider();
			data = d;
			SetData();
			DialogResult = DialogResult.Cancel;


			SetGermanText();


		}
		private void SetGermanText()
		{
			Text = "Schraffur-Definition";
			labelTextSize.Text = "Text-Höhe";
			labelColor.Text = "Farbe";
			buttonColorChange.Text = "Ändern";
			labelTrasnparency.Text = "Transparenz";
			groupBoxHatchData.Text = "Schraffur-Muster";
			labelScale.Text = "Skalierung";
			labelAngle.Text = "Winkel";
			labelSpacing.Text = "Abstand";
			checkBoxCrossLines.Text = "Kreuz-Schraffur";
		}
		private void SetData()
		{
			textBoxName.Text = data.Name;
			textBoxColor.BackColor = data.HatchType.PatternColor.ToColor();
			textBoxTextSize.Text = DoubleHelper.ToStringInvariant(data.TextSize, 2);
			trackBarTransparency.Value = data.HatchType.TransparencyLevel;

			if (defaultLoadedPatterns == null)
			{
				defaultLoadedPatterns = new List<string>();
				customLoadedPatterns = new List<string>();
			}
			else
				customLoadedPatterns.Clear();

			Common.CADHatchHelpers.GetAllHatchPatterns(ref defaultLoadedPatterns, ref customLoadedPatterns, defaultLoadedPatterns.Count == 0);

			System.Diagnostics.Debug.WriteLine("Number of default hatch patterns: " + defaultLoadedPatterns.Count + " custom: " + customLoadedPatterns.Count);

			Dictionary<string, HatchType> items = new Dictionary<string, HatchType>();
			items.Add("User defined", new UserDefined());
			items.Add("SOLID", new Solid());
			items.Add("-", null);// Separator line.
			foreach (string s in customLoadedPatterns)
			{
				items.Add(s, new Custom() { PatternName = s });
			}
			items.Add("--", null); // Separator line
			foreach(string s in defaultLoadedPatterns)
			{
				if (items.ContainsKey(s))
					continue;
				items.Add(s, new Predefined() { PatternName = s });
			}

			comboBoxPatterns.DataSource = new BindingSource(items, null);
			comboBoxPatterns.DisplayMember = "Key";
			comboBoxPatterns.ValueMember = "Value";

			comboBoxPatterns.AutoCompleteCustomSource = new AutoCompleteStringCollection();
			comboBoxPatterns.AutoCompleteCustomSource.AddRange(items.Select(x=>x.Key).ToArray());
			RefreshHatchType(data.HatchType, true);

			if(data.HatchType.PatternName != null && items.ContainsKey(data.HatchType.PatternName))
			{
				comboBoxPatterns.Text = data.HatchType.PatternName;// = items[data.HatchType.PatternName];
				RefreshHatchType(data.HatchType);
			}
			
			
			InitializeComboBoxDrawing();
			
		}
		private void InitializeComboBoxDrawing()
		{
			comboBoxPatterns.DrawMode = DrawMode.OwnerDrawVariable;
			comboBoxPatterns.MeasureItem += ComboBoxPatterns_MeasureItem;
			comboBoxPatterns.DrawItem += ComboBoxPatterns_DrawItem;
			comboBoxPatterns.SelectedIndexChanged += ComboBoxPatterns_SelectedIndexChanged;
			comboBoxPatterns.TextChanged += ComboBoxPatterns_SelectedIndexChanged;
		}

		private void ComboBoxPatterns_SelectedIndexChanged(object sender, EventArgs e)
		{
			// Don't alow line item to be selected (line item is measured to 1px height, and draw as a single line (fill rectangle).
			KeyValuePair<string, HatchType> item = (KeyValuePair<string, HatchType>)comboBoxPatterns.SelectedItem;
			if (item.Value == null)
				comboBoxPatterns.SelectedItem = comboBoxPatterns.Items[comboBoxPatterns.SelectedIndex - 1];
			else
			{
				RefreshHatchType(item.Value);
			}
		}
		/// <summary>
		/// Sets states of HatchType part of the dialog, depending on input object.
		/// </summary>
		/// <param name="obj"></param>
		private void RefreshHatchType(HatchType obj, bool setData = false)
		{
			if (obj is UserDefined)
			{
				if (setData)
				{
					UserDefined u = (UserDefined)obj;
					textBoxDistance.Text = DoubleHelper.ToStringInvariant(u.Spacing, 2);
					textBoxAngle.Text = DoubleHelper.ToStringInvariant(u.AngleDeg, 2);
					checkBoxCrossLines.Checked = u.CrossLines;
				}
				textBoxHatchScale.Enabled = false;
				textBoxAngle.Enabled = true;
				textBoxDistance.Enabled = true;
				checkBoxCrossLines.Enabled = true;
			}
			if (obj is Solid)
			{
				textBoxHatchScale.Enabled = false;
				textBoxAngle.Enabled = false;
				textBoxDistance.Enabled = false;
				checkBoxCrossLines.Enabled = false;
			}
			if (obj is Predefined)
			{
				if (setData)
				{
					Predefined p = (Predefined)obj;
					textBoxHatchScale.Text = DoubleHelper.ToStringInvariant(p.Scale, 2);
					textBoxAngle.Text = DoubleHelper.ToStringInvariant(p.AngleDeg, 2);
				}
				textBoxHatchScale.Enabled = true;
				textBoxAngle.Enabled = true;
				textBoxDistance.Enabled = false;
				checkBoxCrossLines.Enabled = false;
			}
		}


		private void ComboBoxPatterns_DrawItem(object sender, DrawItemEventArgs e)
		{
			KeyValuePair<string, HatchType> item = (KeyValuePair<string, HatchType>)comboBoxPatterns.Items[e.Index];
			if (item.Value == null)
			{
				e.Graphics.FillRectangle(new SolidBrush(e.ForeColor), e.Bounds);
			}
			else
			{
				if (e.Index >= 0)
				{
					Graphics g = e.Graphics;
					Brush brush = (e.State.HasFlag(DrawItemState.Selected) && item.Value != null)?
								   new SolidBrush(SystemColors.Highlight) : new SolidBrush(e.BackColor);
					Brush tBrush = new SolidBrush(e.ForeColor);

					g.FillRectangle(brush, e.Bounds);
					e.Graphics.DrawString(item.Key, e.Font, tBrush, e.Bounds, StringFormat.GenericDefault);
					brush.Dispose();
					tBrush.Dispose();
				}
				e.DrawFocusRectangle();
			}
		}

		private void ComboBoxPatterns_MeasureItem(object sender, MeasureItemEventArgs e)
		{
			KeyValuePair<string, HatchType> item = (KeyValuePair<string, HatchType>)comboBoxPatterns.Items[e.Index];
			if (item.Value == null)
				e.ItemHeight = 1;
		}

		private void UpdateData()
		{
			string s = textBoxName.Text;
			data.Name = s;
			s = textBoxTextSize.Text;
			data.TextSize = DoubleHelper.ParseInvariantDouble(s);
			HatchType type = ((HatchType)comboBoxPatterns.SelectedValue).Clone();
			type.TransparencyLevel = trackBarTransparency.Value;
			if (type is Solid)
			{

			}
			else
			{
				s = textBoxAngle.Text;
				double angle = DoubleHelper.ParseInvariantDouble(s);
				UserDefined ud = type as UserDefined;
				Predefined pd = type as Predefined;
				if (ud != null)
				{
					ud.AngleDeg = angle;
					ud.CrossLines = checkBoxCrossLines.Checked;
					s = textBoxDistance.Text;
					double d = DoubleHelper.ParseInvariantDouble(s);
					ud.Spacing = d;
				}
				if (pd != null)
				{
					pd.AngleDeg = angle;
					s = textBoxHatchScale.Text;
					double d = DoubleHelper.ParseInvariantDouble(s);
					pd.Scale = d;
				}
				
			}

			type.PatternColor = new ColorPersistence(textBoxColor.BackColor);
			data.HatchType = type.Clone();
			
		}
		private bool CheckData()
		{
			error.Clear();
			bool valid = true;
			string s = textBoxName.Text;
			if (string.IsNullOrEmpty(s))
			{
				error.SetError(textBoxName, "Please enter non empty name!");
				valid = false;
			}
			double d;
			s = textBoxTextSize.Text;
			if (!DoubleHelper.TryParseInvariant(s, out d) || d <= 0)
			{
				string text = "Please enter decimal value!";
				if (d <= 0)
					text = "Text size must be greater than 0!";
				error.SetError(textBoxTextSize, text);
				valid = false;
			}
			if (!(comboBoxPatterns.SelectedValue is HatchType))
			{
				error.SetError(comboBoxPatterns, "Please select a hatch");
				valid = false;
			}

			if (textBoxAngle.Enabled)
			{
				s = textBoxAngle.Text;
				if (!DoubleHelper.TryParseInvariant(s, out d))
				{
					error.SetError(textBoxAngle, "Please enter decimal value!");
					valid = false;
				}
			}
			if (textBoxDistance.Enabled)
			{
				s = textBoxDistance.Text;
				if (!DoubleHelper.TryParseInvariant(s, out d))
				{
					error.SetError(textBoxDistance, "Please enter decimal value!");
					valid = false;
				}
			}
			if (textBoxHatchScale.Enabled)
			{
				s = textBoxHatchScale.Text;
				if (!DoubleHelper.TryParseInvariant(s, out d))
				{
					error.SetError(textBoxHatchScale, "Please enter decimal value!");
					valid = false;
				}
			}
			return valid;
		}
		private void buttonOk_Click(object sender, EventArgs e)
		{
			if (!CheckData())
				return;
			UpdateData();
			DialogResult = DialogResult.OK;
			Close();
		}

		private void buttonColorChange_Click(object sender, EventArgs e)
		{
			colorDialog1.Color = textBoxColor.BackColor; 
			if (colorDialog1.ShowDialog() == DialogResult.OK)
			{
				textBoxColor.BackColor = colorDialog1.Color;
			}
		}

		private void trackBarTransparency_ValueChanged(object sender, EventArgs e)
		{
			labelTransparencyLevel.Text = trackBarTransparency.Value.ToString();
		}
	}

	public class SingleHatchViewController
	{
		/// <summary>
		/// Returns flag which indicates if data has been updated.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		internal static bool Show(SingleHatchData data)
		{
			bool val = new SingleHatchView(data).ShowDialog() == DialogResult.OK;
			return val;
		}
	}
}
