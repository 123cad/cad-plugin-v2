﻿using CadPlugin.HatchToolbars;
using CadPlugin.Toolbars.ToolbarExportImportSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace CadPlugin.Toolbars.HatchToolbars.ImportExports
{
	class ImportExportManager
	{
		private static string XMLGlobalRootName = "HatchToolbar";
		public static void Export(List<SingleHatchData> data)
		{
			SaveFileDialog saveDlg = new SaveFileDialog();
			saveDlg.OverwritePrompt = true;
			saveDlg.Filter = "XML files (*.xml)|*.xml| All files (*.*)|*.*";
			saveDlg.FileName = "FlächentoolbarMaske.xml";
			if (saveDlg.ShowDialog() == DialogResult.OK)
			{
				ExportImportManager mgr = new ExportImportManager(XMLGlobalRootName, new List<Type>() { typeof(SingleHatchData) });
				{
					mgr.SaveData(data, saveDlg.FileName);
				}
			}
		}
		public static List<SingleHatchData> Import()
		{
			OpenFileDialog openDlg = new OpenFileDialog();
			openDlg.CheckFileExists = true;
			openDlg.Filter = "XML files (*.xml)|*.xml| All files (*.*)|*.*";
			List<SingleHatchData> data = null;
			if (openDlg.ShowDialog() == DialogResult.OK)
			{
				ExportImportManager mgr = new ExportImportManager(XMLGlobalRootName, new List<Type>() { typeof(SingleHatchData) });
				try
				{
					List<object> obj = mgr.LoadData(openDlg.FileName);
					if (obj != null)
						data = obj.Select(x=>(SingleHatchData)x).ToList();
				}
				catch (XmlException xmlE)
				{
					System.Diagnostics.Debug.WriteLine("Invalid linetype file!");
					// Show dialog.
					MessageBox.Show("Invalid file format!" + xmlE.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return null;
				}
			}
			return data;
		}
	}
}
