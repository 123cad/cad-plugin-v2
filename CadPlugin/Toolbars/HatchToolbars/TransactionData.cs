﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.HatchToolbars
{
	class TransactionData
	{
		public Document Document { get; private set; }
		public Database Database { get { return Document.Database; } }
		public Editor Editor { get { return Document.Editor; } }
		public Transaction Transaction { get; private set; }
		public BlockTable BlockTable { get; private set; }
		public BlockTableRecord ModelSpace { get; private set; }
		public DBDictionary NOD { get; private set; }
		public DBDictionary ToolbarEntry { get; private set; }
		/// <summary>
		/// Manager is related to Transaction because contains opened object within Transaction.
		/// Must not be used in other transactions!
		/// </summary>
		public Data.Manager ObjectManager { get; private set; }
		private TransactionData()
		{
			ObjectManager = new Data.Manager(this);
		}
		public static TransactionData Create(Document doc, Transaction tr)
		{
			TransactionData t = new TransactionData();
			t.Document = doc;
			t.Transaction = tr;
			t.BlockTable = tr.GetObject(t.Database.BlockTableId, OpenMode.ForRead) as BlockTable;
			t.ModelSpace = tr.GetObject(t.BlockTable[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
			t.NOD = tr.GetObject(doc.Database.NamedObjectsDictionaryId, OpenMode.ForWrite) as DBDictionary;
			Data.ToolbarNodManager.PrepareToolbarNODStructure(t);
			t.ToolbarEntry = tr.GetObject(t.NOD.GetAt(Data.ToolbarNodManager.ToolbarEntry), OpenMode.ForWrite) as DBDictionary;
			return t;
		}
	}
}
