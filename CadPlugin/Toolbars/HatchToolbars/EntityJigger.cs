﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.GraphicsInterface;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.GraphicsInterface;
#endif

namespace CadPlugin.HatchToolbars
{
	class EntityJigger : DrawJig
	{
		public virtual Point3d LastMousePosition { get; private set; }

		public virtual Point3d CurrentMousePosition { get; private set; }

		private IEnumerable<Entity> entities { get; set; }
		/// <summary>
		/// Entities that are being moved relative to end point.
		/// </summary>
		protected IEnumerable<Entity> entitiesCollection = null;

		public Matrix3d UCS { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="l"></param>
		/// <param name="entities"></param>
		/// <param name="calc"></param>
		/// <param name="ucs"></param>
		public EntityJigger(IEnumerable<Entity> entities, Point3d startPoint, Matrix3d ucs) : base()
		{
			LastMousePosition = startPoint;
			entitiesCollection = entities;
			UCS = ucs;
		}
		protected override bool WorldDraw(WorldDraw draw)
		{
			WorldGeometry geo = draw.Geometry;
			if (geo != null)
			{
				geo.PushModelTransform(UCS);

				Vector3d moveVector = LastMousePosition.GetVectorTo(CurrentMousePosition);
				LastMousePosition = CurrentMousePosition;

				Matrix3d moveMatrix = Matrix3d.Displacement(moveVector);
				foreach (Entity ent in entitiesCollection)
				{
					// For some reason, transforming AttributeReference downgrades 
					// BlockReference, and vice versa.
					if (!ent.IsWriteEnabled)
						ent.UpgradeOpen();
					ent.TransformBy(moveMatrix);

					geo.Draw(ent);
				}

				geo.PopModelTransform();
			}

			return true;
		}
		protected override SamplerStatus Sampler(JigPrompts prompts)
		{
			JigPromptPointOptions prOptions1 = new JigPromptPointOptions("\n");
			prOptions1.UseBasePoint = false;
			prOptions1.UserInputControls =
				UserInputControls.NullResponseAccepted | UserInputControls.Accept3dCoordinates |
				UserInputControls.GovernedByUCSDetect | UserInputControls.GovernedByOrthoMode/* |
				UserInputControls.AcceptMouseUpAsPoint*/;

			PromptPointResult prResult1 = prompts.AcquirePoint(prOptions1);
			if (prResult1.Status == PromptStatus.Cancel || prResult1.Status == PromptStatus.Error)
				return SamplerStatus.Cancel;

			Point3d tempPt = prResult1.Value.TransformBy(UCS.Inverse());


			if (prResult1.Value.Equals(CurrentMousePosition))
			{
				return SamplerStatus.NoChange;
			}
			else
			{

				// LastMousePosition has to be updated when data is moved on the screen.
				// This is done in the WorldDraw method.
				CurrentMousePosition = prResult1.Value;

				return SamplerStatus.OK;
			}

		}
	}
}
