﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.HatchToolbars.Data;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.HatchToolbars
{
	class HatchCommands
	{
		public static event Action<Document> CommandFinished;
		private static void OnCommandFinished(Document doc)
		{
			if (CommandFinished != null)
				CommandFinished.Invoke(doc);
		}
		/// <summary>
		/// Selection set requires to be executed inside command (with CommandFlags.Redraw).
		/// Toolbar works outside of command context.
		/// So internaly new command will be invoked, with data set as static variable.
		/// </summary>
		private static void SelectEntities(IEnumerable<ObjectId> entities)
		{
			if (entities == null)
				return;
			Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
			ed.SetImpliedSelection(new ObjectId[0]);
			ObjectId[] objs = entities.ToArray();
			ed.SetImpliedSelection(objs);
			PromptSelectionResult selRes = ed.SelectImplied();
		}
		/// <summary>
		/// Creates table with selected areas by the user.
		/// </summary>
		/// <param name="doc"></param>
		public static void CreateHatchTable(Document doc)
		{
			Database db = doc.Database;
			Editor ed = doc.Editor;

			List<string> blockTypeNames = new List<string>() { "PLINE", "HATCH", "MTEXT" };
			// Add other Entity type names, which is to be included in selection set.
			TypedValue[] filterCriteria = new TypedValue[blockTypeNames.Count + 2];
			int i = 0;
			// Operator must be specified, otherwise AND is implicitly used for all criteria elements.
			filterCriteria.SetValue(new TypedValue((int)DxfCode.Operator, "<OR"), i++);
			foreach (string s in blockTypeNames)
			{
				// If not include any element, then add only BlockReference.
				filterCriteria.SetValue(new TypedValue((int)DxfCode.Start, s), i++);
			}
			filterCriteria.SetValue(new TypedValue((int)DxfCode.Operator, "OR>"), i++);
			SelectionFilter filter = new SelectionFilter(filterCriteria);

			PromptSelectionResult res = Application.DocumentManager.MdiActiveDocument.Editor.GetSelection(filter);
			if (res.Status == PromptStatus.OK)
			{
				ObjectId[] selectedIds = res.Value.GetObjectIds();
				using (DocumentLock lockDoc = doc.LockDocument())
				using (Transaction tr = doc.TransactionManager.StartTransaction())
				{
					BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
					BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
					TransactionData data = TransactionData.Create(doc, tr);

					HashSet<HatchToolbars.Data.Attribute> attributes = new HashSet<HatchToolbars.Data.Attribute>();
					foreach (ObjectId id in selectedIds)
					{
						Entity ent = tr.GetObject(id, OpenMode.ForRead) as Entity;
						Area area = data.ObjectManager.Load(ent);
						if (area == null)
							continue;

						if (attributes.Contains(area.Attribute))
							continue;

						attributes.Add(area.Attribute);
					}

					HatchToolbars.Data.Table table = data.ObjectManager.CreateNewTable(attributes, ToolbarSettings.TableSettingsManager.TableSettings);

					double textSize = 1;
					double paddingX = textSize * 1;
					double paddingY = textSize * 0.7;
					Color tableColor = Color.FromColor(System.Drawing.Color.Black);
					Color textColor = Color.FromColor(System.Drawing.Color.FromArgb(110, 110, 110));
					//List<Entity> entities = TableDrawer.CreateTable(tr, btr, new Point3d(), values, textSize, paddingX, paddingY, textColor, tableColor);
					IEnumerable<Entity> entities = table.GetAllEntities();

					ed.Regen();

					EntityJigger jigger = new EntityJigger(entities, new Point3d(), ed.CurrentUserCoordinateSystem);
					PromptResult jigRes = ed.Drag(jigger);
					if (jigRes.Status == PromptStatus.OK)
						tr.Commit();
					else
						tr.Abort();

				}
			}
			ed.WriteMessage("\n:");
		}
		/// <summary>
		/// Deletes table from the drawing.
		/// </summary>
		/// <param name="doc"></param>
		public static void DeleteTable(Document doc)
		{
			Database db = doc.Database;
			Editor ed = doc.Editor;

			using (DocumentLock lockDoc = doc.LockDocument())
			using (Transaction tr = doc.TransactionManager.StartTransaction())
			{
				BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
				BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
				TransactionData data = TransactionData.Create(doc, tr);

				PromptEntityOptions opt = new PromptEntityOptions("Element der Tabelle wählen:");// "Select any entity of table:");
				opt.SetRejectMessage("Not an entity");
				PromptEntityResult res = ed.GetEntity(opt);
				if (res.Status == PromptStatus.OK)
				{
					Entity ent = tr.GetObject(res.ObjectId, OpenMode.ForWrite) as Entity;
					string tableId = HatchToolbars.Data.Table.GetTableId(data, ent);
					if (!string.IsNullOrEmpty(tableId))
					{
						HatchToolbars.Data.Table table = data.ObjectManager.LoadExistingTable(tableId);
						table.Delete();
						tr.Commit();
					}
					else
						ed.WriteMessage("Unable to find table for selected Entity.\nCanceled");
				}

			}
			ed.WriteMessage("\n:");
		}
		/// <summary>
		/// Exports table to CSV format.
		/// </summary>
		/// <param name="doc"></param>
		public static void ExportTable(Document doc)
		{
			Database db = doc.Database;
			Editor ed = doc.Editor;

			using (DocumentLock lockDoc = doc.LockDocument())
			using (Transaction tr = doc.TransactionManager.StartTransaction())
			{
				BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
				BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
				TransactionData data = TransactionData.Create(doc, tr);

				PromptEntityOptions opt = new PromptEntityOptions("Element der Tabelle wählen:");// "Select any entity of table:");
				opt.SetRejectMessage("Not an entity");
				PromptEntityResult res = ed.GetEntity(opt);
				if (res.Status == PromptStatus.OK)
				{
					Entity ent = tr.GetObject(res.ObjectId, OpenMode.ForWrite) as Entity;
					string tableId = HatchToolbars.Data.Table.GetTableId(data, ent);
					if (!string.IsNullOrEmpty(tableId))
					{
						HatchToolbars.Data.Table table = data.ObjectManager.LoadExistingTable(tableId);
						bool success = ExportTableCSV.Start(table);
						if (success)
							ed.WriteMessage("File saved.");
						else
							ed.WriteMessage("File not saved!");
					}
					else
						ed.WriteMessage("Unable to find table for selected Entity.\nCanceled");
				}

			}
			ed.WriteMessage("\n:");
		}
		/// <summary>
		/// Adds area to existing table, both are selected by the user.
		/// </summary>
		public static void AddAreaToTable(Document doc)
		{
			Database db = doc.Database;
			Editor ed = doc.Editor;

			SelectionFilter filter = new SelectionFilter(new TypedValue[1] { new TypedValue((int)DxfCode.Start, "MText") });

			ed.WriteMessage("Select MText of existing attribute:");
			PromptSelectionResult res = Application.DocumentManager.MdiActiveDocument.Editor.GetSelection(filter);
			using (DocumentLock lockDoc = doc.LockDocument())
			using (Transaction tr = doc.TransactionManager.StartTransaction())
			{
				BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
				BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
				TransactionData data = TransactionData.Create(doc, tr);
				HashSet<Area> areas = new HashSet<Area>();


				if (res.Status == PromptStatus.OK)
				{
					ObjectId[] selectedIds = res.Value.GetObjectIds();
					foreach (ObjectId id in selectedIds)
					{
						Entity ent = tr.GetObject(id, OpenMode.ForRead) as Entity;
						Area area = data.ObjectManager.Load(ent);
						if (area == null)
							continue;

						if (areas.Contains(area))
							continue;

						areas.Add(area);
					}
				}
				else
				{
					ed.WriteMessage("\n:");
					return;
				}


				HatchToolbars.Data.Table table = null;
				while (true)
				{
					PromptEntityOptions opt = new PromptEntityOptions("Element der Tabelle wählen:");// "Select any entity of table:");
					opt.SetRejectMessage("Not an entity");
					PromptEntityResult res1 = ed.GetEntity(opt);
					if (res1.Status == PromptStatus.OK)
					{
						Entity ent = tr.GetObject(res1.ObjectId, OpenMode.ForWrite) as Entity;
						string tableId = HatchToolbars.Data.Table.GetTableId(data, ent);
						if (!string.IsNullOrEmpty(tableId))
						{
							table = data.ObjectManager.LoadExistingTable(tableId);
							if (table != null)
								break;
							else
							{
								ed.WriteMessage("Selected entity does not belong to table!\n Select new entity:");
								continue;
							}
						}
						else
							ed.WriteMessage("Unable to find table for selected Entity!\n Select new entity:");
					}
					else
					{
						ed.WriteMessage("\n:");
						return;
					}
				}
				foreach (Area ar in areas)
					table.AddAttribute(ar.Attribute);
				tr.Commit();
			}
			ed.WriteMessage("\n:");
		}
		/// <summary>
		/// Remove selected area from the table.
		/// </summary>
		public static void RemoveAreaFromTable(Document doc)
		{
			Database db = doc.Database;
			Editor ed = doc.Editor;

			using (DocumentLock lockDoc = doc.LockDocument())
			using (Transaction tr = doc.TransactionManager.StartTransaction())
			{
				BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
				BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
				TransactionData data = TransactionData.Create(doc, tr);

				PromptEntityOptions opt = new PromptEntityOptions("Flächentext in Tabelle wählen:");// "Select MText object in table:");
				opt.SetRejectMessage("Not MText Entity");
				opt.AddAllowedClass(typeof(MText), true);
				PromptEntityResult res = ed.GetEntity(opt);
				if (res.Status == PromptStatus.OK)
				{
					Entity ent = tr.GetObject(res.ObjectId, OpenMode.ForWrite) as Entity;
					string tableId = HatchToolbars.Data.Table.GetTableId(data, ent);
					if (!string.IsNullOrEmpty(tableId))
					{
						HatchToolbars.Data.Table table = data.ObjectManager.LoadExistingTable(tableId);
						ObjectId attId = table.GetAttributeId((MText)ent);
						if (attId != ObjectId.Null)
						{
							MText attText = tr.GetObject(attId, OpenMode.ForWrite) as MText;
							Area ar = data.ObjectManager.Load(attText);
							table.RemoveAttribute(ar.Attribute);

							tr.Commit();
						}
					}
					else
						ed.WriteMessage("Unable to find table for selected MText.");
				}
			}
			ed.WriteMessage("\n:");
		}
		/// <summary>
		/// Adds to selection selected area in the table;
		/// </summary>
		public static void AreaToSelection(Document doc)
		{
			Database db = doc.Database;
			Editor ed = doc.Editor;
			List<ObjectId> selectionObjects = null;
			try
			{
				using (DocumentLock lockDoc = doc.LockDocument())
				{

					using (Transaction tr = doc.TransactionManager.StartTransaction())
					{
						BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
						BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
						TransactionData data = TransactionData.Create(doc, tr);

						PromptEntityOptions opt = new PromptEntityOptions("Flächentext in Tabelle wählen");// "Select MText object in table:");
						opt.SetRejectMessage("Not MText entity");
						opt.AddAllowedClass(typeof(MText), true);
						PromptEntityResult res = ed.GetEntity(opt);
						if (res.Status == PromptStatus.OK)
						{
							Entity ent = tr.GetObject(res.ObjectId, OpenMode.ForWrite) as Entity;
							string tableId = HatchToolbars.Data.Table.GetTableId(data, ent);
							if (!string.IsNullOrEmpty(tableId))
							{
								HatchToolbars.Data.Table table = data.ObjectManager.LoadExistingTable(tableId);
								ObjectId attId = table.GetAttributeId((MText)ent);
								if (attId != ObjectId.Null)
								{
									MText attText = tr.GetObject(attId, OpenMode.ForWrite) as MText;
									Area ar = data.ObjectManager.Load(attText);
									List<Entity> entities = ar.GetAllEntities().ToList();
									entities.AddRange(ar.Attribute.GetAllEntities().ToArray());

									selectionObjects = entities.Select(x => x.ObjectId).ToList();
									CADZoomHelper.ZoomToEntities(ed, entities);


									tr.Commit();
								}
							}
							else
								ed.WriteMessage("Unable to find table for selected MText.");
						}

					}

					if (selectionObjects != null)
					{
						SelectEntities(selectionObjects);
					}
				}
			}
			finally
			{
				OnCommandFinished(doc);
			}
			ed.WriteMessage("\n:");
		}
		/// <summary>
		/// Adds to selection all areas which reside in selected table.
		/// </summary>
		public static void AllAreasToSelection(Document doc)
		{
			Database db = doc.Database;
			Editor ed = doc.Editor;

			try
			{
				List<ObjectId> selectionObjects = null;
				using (DocumentLock lockDoc = doc.LockDocument())
				{
					using (Transaction tr = doc.TransactionManager.StartTransaction())
					{
						BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
						BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
						TransactionData data = TransactionData.Create(doc, tr);

						HatchToolbars.Data.Table table = null;
						PromptEntityOptions opt = new PromptEntityOptions("Element der Tabelle wählen:");// "Select any entity of table:");
						opt.SetRejectMessage("Not an entity");
						PromptEntityResult res = ed.GetEntity(opt);
						if (res.Status == PromptStatus.OK)
						{
							Entity ent = tr.GetObject(res.ObjectId, OpenMode.ForWrite) as Entity;
							string tableId = HatchToolbars.Data.Table.GetTableId(data, ent);
							if (!string.IsNullOrEmpty(tableId))
							{
								table = data.ObjectManager.LoadExistingTable(tableId);
							}
						}
						else
						{
							ed.WriteMessage("Unable to find table for selected Entity! \n");
							return;
						}
						if (table != null)
						{

							IEnumerable<Entity> entities = table.Attributes.SelectMany(x =>
							{
								var v = x.GetAllEntities().Concat(x.Area.GetAllEntities()).ToArray();
								return v;
							}).ToArray();
							if (entities.Any())
							{

								selectionObjects = entities.Select(x => x.ObjectId).ToList();
								CADZoomHelper.ZoomToEntities(ed, entities);
							}

							tr.Commit();
						}
						else
							ed.WriteMessage("Selected entity does not belong to table!");
					}

					if (selectionObjects != null)
					{
						SelectEntities(selectionObjects);
					}
				}
			}
			finally
			{
				OnCommandFinished(doc);
			}
			ed.WriteMessage("\n:");
		}
		/// <summary>
		/// Adds to selection all tables in which selected area exists.
		/// </summary>
		/// <param name="doc"></param>
		public static void AreaTablesToSelection(Document doc)
		{
			Database db = doc.Database;
			Editor ed = doc.Editor;

			try
			{
				List<ObjectId> selectionObjects = null;
				using (DocumentLock lockDoc = doc.LockDocument())
				{
					using (Transaction tr = doc.TransactionManager.StartTransaction())
					{
						BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
						BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
						TransactionData data = TransactionData.Create(doc, tr);

						Area area = null;
						PromptEntityOptions opt = new PromptEntityOptions("Flächenobjekt wählen:");// "Select attribute or area:");
						opt.SetRejectMessage("Not an entity");
						PromptEntityResult res = ed.GetEntity(opt);
						if (res.Status == PromptStatus.OK)
						{
							Entity ent = tr.GetObject(res.ObjectId, OpenMode.ForWrite) as Entity;
							area = data.ObjectManager.Load(ent);
						}
						else
						{
							ed.WriteMessage("Canceled! \n");
							return;
						}
						if (area != null)
						{

							IEnumerable<Entity> entities = area.Attribute.Tables.Select(x => x.Boundary).ToArray();
							if (entities.Any())
							{
								selectionObjects = entities.Select(x => x.ObjectId).ToList();
								CADZoomHelper.ZoomToEntities(ed, entities);
							}

							tr.Commit();
						}
						else
							ed.WriteMessage("Selected entity does is not area or attribute!");
					}

					if (selectionObjects != null)
					{
						SelectEntities(selectionObjects);
						//doc.SendStringToExecute("123HatchReselect\n", false, false, false);
					}
				}
			}
			finally
			{
				OnCommandFinished(doc);
			}
			ed.WriteMessage("\n:");
		}
		/// <summary>
		/// Creates attribute for selected points/polyline.
		/// </summary>
		public static void HatchDataSelected(Document doc, SingleHatchData data)
		{
			Database db = doc.Database;
			Editor ed = doc.Editor;
			PointMonitor monitor = PointMonitor.Start(ed);

			try
			{
				using (DocumentLock lockDoc = doc.LockDocument())
				{
					List<Point3d> vertices = new List<Point3d>();
					Point3d? lastPoint = null;
					List<ObjectId> tempObjects = new List<ObjectId>();
					bool canceled = false;
					bool selectPolyline = false;
					while (true)
					{
						if (!selectPolyline)
						{
							// Select points.
							PromptPointOptions opt = new PromptPointOptions("Punkt wählen:");// "Select point:");
							opt.Keywords.Add("Polylinie");
							opt.Keywords.Add("Zurück");
							opt.AllowNone = true;
							if (lastPoint.HasValue)
							{
								opt.UseBasePoint = true;
								opt.UseDashedLine = true;
								opt.BasePoint = lastPoint.Value;
							}


							PromptPointResult res = ed.GetPoint(opt);
							if (res.Status == PromptStatus.OK)
							{
								if (lastPoint.HasValue)
								{
									using (Transaction tr = db.TransactionManager.StartTransaction())
									{
										BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
										BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
										
										Line l = new Line();
										btr.AppendEntityCurrentLayer(l);
										tr.AddNewlyCreatedDBObject(l, true);
										l.StartPoint = lastPoint.Value;
										l.EndPoint = res.Value;
										l.Color = Color.FromColor(data.HatchType.PatternColor.ToColor());
										tempObjects.Add(l.ObjectId);
										tr.Commit();
									}
									ed.Regen();
								}

								vertices.Add(res.Value);
								lastPoint = res.Value;
							}
							else if (res.Status == PromptStatus.Keyword)
							{
								string str = res.StringResult.ToLower();
								if (str == "polylinie")
								{
									vertices.Clear();
									lastPoint = null;
									using (Transaction tr = db.TransactionManager.StartTransaction())
									{
										foreach (ObjectId oid in tempObjects)
											tr.GetObject(oid, OpenMode.ForWrite).Erase();
										tr.Commit();
									}
									tempObjects.Clear();
									selectPolyline = true;
									continue;
								}
								else if (str == "zurück")
								{
									if (vertices.Count > 0)
									{
										if (vertices.Count == 1)
										{
											lastPoint = null;
											vertices.Clear();
										}
										else
										{
											vertices.RemoveAt(vertices.Count - 1);
											lastPoint = vertices[vertices.Count - 1];
										}
										if (tempObjects.Count > 0)
											using (Transaction tr = db.TransactionManager.StartTransaction())
											{

												tr.GetObject(tempObjects.Last(), OpenMode.ForWrite).Erase();
												tempObjects.RemoveAt(tempObjects.Count - 1);
												tr.Commit();
											}
									}
									ed.Regen();
								}
							}
							else if (res.Status == PromptStatus.None)
							{
								// Enter is pressed.
								break;
							}
							else if (res.Status == PromptStatus.Cancel)
							{
								canceled = true;
								break;
							}
							}
							else
							{
							// Select polyline.

							PromptEntityOptions polylineSelection = new PromptEntityOptions("2D- oder 3D-Polylinie wählen:");// "Select polyline:");
								polylineSelection.Keywords.Add("Punkte");
								polylineSelection.AllowNone = true;
							polylineSelection.SetRejectMessage("2D- oder 3D-Polylinie wählen");// Please select polyline:");
								polylineSelection.AddAllowedClass(typeof(Polyline), true);
							polylineSelection.AddAllowedClass(typeof(Polyline2d), true);
							polylineSelection.AddAllowedClass(typeof(Polyline3d), true);


							PromptEntityResult res = ed.GetEntity(polylineSelection);
							if (res.Status == PromptStatus.OK)
							{
								using (Transaction tr = db.TransactionManager.StartTransaction())
								{
									Entity ent = tr.GetObject(res.ObjectId, OpenMode.ForRead) as Entity;
									Polyline pl = ent as Polyline;
									Polyline2d pl2 = ent as Polyline2d;
									Polyline3d pl3 = ent as Polyline3d;
									if (pl != null)
									{
										for (int i = 0; i < pl.NumberOfVertices; i++)
										{
											vertices.Add(pl.GetPoint3dAt(i));
										}
									}
									if (pl2 != null)
									{ 
										foreach (ObjectId v in pl2)
										{
											Vertex2d pv = tr.GetObject(v, OpenMode.ForRead) as Vertex2d;
											vertices.Add(new Point3d(pv.Position.X, pv.Position.Y, 0));
										}
									}
									if (pl3 != null)
									{
										foreach (ObjectId oid in pl3)
										{
											PolylineVertex3d pv = tr.GetObject(oid, OpenMode.ForRead) as PolylineVertex3d;
											vertices.Add(new Point3d(pv.Position.X, pv.Position.Y, 0));
										}
									}
								}
								break;
							}
							else if (res.Status == PromptStatus.Keyword)
							{
								if (res.StringResult.ToLower() == "punkte")
								{
									vertices.Clear();
									selectPolyline = false;
									continue;
								}
							}
							else if (res.Status == PromptStatus.None)
							{
								// Enter is pressed.
								break;
							}
							else if (res.Status == PromptStatus.Cancel)
							{
								canceled = true;
								break;
							}
							}
						}
						using (Transaction tr = db.TransactionManager.StartTransaction())
						{
							foreach (ObjectId t in tempObjects)
							{
								DBObject e = tr.GetObject(t, OpenMode.ForWrite);
								e.Erase();
							}
							tr.Commit();
						}
						if (vertices.Count < 3)
						{
						ed.WriteMessage("AbbrechenMinimum 3 Verteces sind erforderlich!\n");// "Minimum 3 vertices are required!\n");
							canceled = true;
						}
						if (canceled)
						{
							return;
						}
						Point3d current = monitor.CurrentPoint;
						ObjectId areaPlId;
						using (Transaction tr = db.TransactionManager.StartTransaction())
						{
							BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
							BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
							TransactionData trData = TransactionData.Create(doc, tr);

							Polyline areaPl = new Polyline();
							btr.AppendEntityCurrentLayer(areaPl);
							tr.AddNewlyCreatedDBObject(areaPl, true);
							areaPlId = areaPl.ObjectId;
							areaPl.Color = Color.FromColor(data.HatchType.PatternColor.ToColor());

							int i = 0;
							foreach (Point3d pt3 in vertices)
							{
								areaPl.AddVertexAt(i++, new Point2d(pt3.X, pt3.Y), 0, 0, 0);
							}
							areaPl.Closed = true;

							Area area = trData.ObjectManager.CreateNew(areaPl, data);
							area.Attribute.Refresh(current);

							tr.Commit();
						}
						// Must be separated because autocad doesn't show dragging entities except dragging ones.
						using (Transaction tr = db.TransactionManager.StartTransaction())
						{
							TransactionData trData = TransactionData.Create(doc, tr);
							Polyline pl = tr.GetObject(areaPlId, OpenMode.ForRead) as Polyline;
							Area area = trData.ObjectManager.Load(pl);
							HatchToolbars.Data.Attribute att = area.Attribute;
							current = monitor.CurrentPoint;
							att.Refresh(current);

							IEnumerable<Entity> dragEntities = new List<Entity>() { att.Text, att.Boundary, att.Hatch };
							HatchToolbars.EntityJigger jigger = new HatchToolbars.EntityJigger(dragEntities, current, ed.CurrentUserCoordinateSystem);
							PromptResult res = ed.Drag(jigger);

							tr.Commit();
						}

						using (Transaction tr = db.TransactionManager.StartTransaction())
						{
							TransactionData trData = TransactionData.Create(doc, tr);
							Polyline pl = tr.GetObject(areaPlId, OpenMode.ForRead) as Polyline;
							Area area = trData.ObjectManager.Load(pl);
							HatchToolbars.Data.Attribute att = area.Attribute;

							/*PromptStringOptions prefix = new PromptStringOptions("Enter prefix text:");
							PromptResult stringResult = ed.GetString(prefix);
							if ((stringResult.Status == PromptStatus.OK || stringResult.Status == PromptStatus.None) &&
								stringResult.StringResult != "")
								att.SetNewPrefix(stringResult.StringResult);*/
							PrefixView.PrefixDialogView.ShowDialog(att);

							tr.Commit();
						}

					}
			}
			finally
			{
				monitor.Finish();
			}
			ed.WriteMessage("\n:");
		}
	}
}

