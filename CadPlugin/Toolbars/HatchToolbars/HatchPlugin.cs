﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.HatchToolbars.Data;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.HatchToolbars
{
	/// <summary>
	/// Contains methods required for event handling on Document level.
	/// </summary>
	class HatchPlugin
	{
		private static HashSet<ObjectId> edited = new HashSet<ObjectId>();
		private static HashSet<ObjectId> erased = new HashSet<ObjectId>();
		private static bool Updating = false;
		private static bool Erasing = false;
	
		/// <summary>
		/// Starting transaction is impossible in ObjectModified, so it is done here.
		/// </summary>
		public static void ApplyUpdates()
		{
			//return;
			/*
			 * If exception occurs here, CAD will crash!
			 * (if object is not opened for write, when write is done, for example)
			 */
			try
			{
				Updating = true;
				Document doc = Application.DocumentManager.MdiActiveDocument;
				Database db = doc.Database;
				using (Transaction tr = db.TransactionManager.StartTransaction())
				{
					TransactionData data = TransactionData.Create(doc, tr);
					Erasing = true;
					foreach (ObjectId oid in erased.ToList())
					{
						if (edited.Contains(oid))
							edited.Remove(oid);

						Entity ent = tr.GetObject(oid, OpenMode.ForWrite, true) as Entity;

						if (HatchToolbars.Data.Attribute.IsAttributeEntity(data, ent) ||
							HatchToolbars.Data.Area.IsAreaEntity(data, ent))
							data.ObjectManager.DeleteAttribute(ent);
						else if (HatchToolbars.Data.Table.IsTableEntity(data, ent))
						{
							// What to do with table entities? 
							// Nothing, they are refreshed.
						}
					}
					tr.Commit();
					Erasing = false;
				}
				// Need to commit between erase/update, because in erase some extra objects
				// will be erased, so by commit we catch them in ObjectErased event, and can 
				// remove them from edited objects collection.
				using (Transaction tr = db.TransactionManager.StartTransaction())
				{
					TransactionData data = TransactionData.Create(doc, tr);
					// Optimization, update all tables just one time at the end.
					Dictionary<string, HatchToolbars.Data.Table> uniqueTables = new Dictionary<string, HatchToolbars.Data.Table>();
					foreach (ObjectId oid in edited.ToList())
					{
						Entity ent = tr.GetObject(oid, OpenMode.ForWrite) as Entity;

						if (HatchToolbars.Data.Attribute.IsAttributeEntity(data, ent) ||
							HatchToolbars.Data.Area.IsAreaEntity(data, ent))
						{
							Area ar = data.ObjectManager.Load(ent);
							if (ar != null)
							{
								ar.Attribute.Refresh();
								var vals = ar.Attribute.Tables.Where(x => !uniqueTables.ContainsKey(x.Id)).ToArray();
								foreach (var val in vals)
									uniqueTables.Add(val.Id, val);
							}
						}
						else if (HatchToolbars.Data.Table.IsTableEntity(data, ent))
						{
							// What to do with table entities?
							HatchToolbars.Data.Table t = data.ObjectManager.LoadExistingTable(HatchToolbars.Data.Table.GetTableId(data, ent));
							MText text = ent as MText;
							if (t != null && text != null)
							{
								t.TableUpdated(text);
							}
						}
					}
					foreach (var t in uniqueTables.Values)
						t.Refresh();
					tr.Commit();
				}
			}
			finally
			{
				Updating = false;
				Erasing = false;
				edited.Clear();
				erased.Clear();
			}
		}
		public static void ObjectErased(DBObject obj)
		{
			if (Updating)
			{
				if (Erasing && edited.Contains(obj.ObjectId))
					edited.Remove(obj.ObjectId);
				return;
			}
			Entity ent = obj as Entity;
			if (ent == null)
				return;
			// If object is not the one we use, or if it is but without ExtensionDictionary, do nothing.
			if (!HatchToolbars.Data.Attribute.IsAttributeObjectType(ent) &&
				!HatchToolbars.Data.Area.IsAreaObjectType(ent) &&
				!HatchToolbars.Data.Table.IsTableObjectType(ent))
				return;
			if (obj.ExtensionDictionary == ObjectId.Null)
				return;
			erased.Add(obj.ObjectId);
		}
		public static void ObjectUpdated(DBObject obj)
		{
			if (Updating)
				return;
			Entity ent = obj as Entity;
			if (ent == null)
				return;
			// If object is not the one we use, or if it is but without ExtensionDictionary, do nothing.
			if (!HatchToolbars.Data.Attribute.IsAttributeObjectType(ent) &&
				!HatchToolbars.Data.Area.IsAreaObjectType(ent) &&
				!HatchToolbars.Data.Table.IsTableObjectType(ent))
				return;
			if (obj.ExtensionDictionary == ObjectId.Null)
				return;
			edited.Add(obj.ObjectId);
		}
	}
}
