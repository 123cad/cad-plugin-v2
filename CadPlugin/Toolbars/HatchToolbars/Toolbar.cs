﻿using CadPlugin.HatchToolbars.Data;
using CadPlugin.HatchToolbars.Persistence;
using CadPlugin.Toolbars.HatchToolbars.ImportExports;
using CadPlugin.Toolbars.ToolbarExportImportSettings;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.HatchToolbars
{
	public partial class Toolbar : Form
	{
		private const int WM_SYSCOMMAND = 0x0112;
		private const int SC_MINIMIZE = 0xf020;
		private const int SC_MAXIMIZE = 0xF030;
		private const int SC_CLOSE = 0xF060;

		/// <summary>
		/// Keeps height before minimized.
		/// </summary>
		private int lastHeight;

		private Brush SettingsNoImageBackground;
		private DragRow dragRow;

		private bool UserPressedClose = false;

		/// <summary>
		/// Fires when user have selected hatch to be used.
		/// </summary>
		internal event Action<SingleHatchData> HatchSelected;
		public event Action CreateTable = delegate { };
		public event Action DeleteTable = delegate { };
		public event Action AppendAreaToTable = delegate { };
		public event Action RemoveAreaFromtTable = delegate { };
		public event Action AreaToSelection = delegate { };
		public event Action AllAreasToSelection = delegate { };
		public event Action AreaTablesToSelection = delegate { };
		public event Action SaveTableToCSV = delegate { };
		public event Action TableSettings = delegate { };

		public Toolbar()
		{
			InitializeComponent();

			InitializeDGV();
			dragRow = new DragRow(dataGridView);


			toggleBottomPanel(!DataPersistence.LoadButtonsPanelCollapsed());

			int dialogHeight = DataPersistence.GetSingleHatchDialogHeight();
			if (dialogHeight > 50)
				Height = dialogHeight;
			int dialogWidth = DataPersistence.GetSingleHatchDialogWidth();
			int positionX = DataPersistence.GetSingleHatchDialogPositionX();
			int positionY = DataPersistence.GetSingleHatchDialogPositionY();
			// If values in settings file not set, don't use them.
			if (dialogWidth > 0)
				Width = dialogWidth;
			// Because location is overriden in show method.
			if (positionX > 0 && positionY > 0)
			{
				Visible = false;
				Shown += (_, __) =>
				{
					Location = new Point(positionX, positionY);
					Visible = true;
				};
			}

			SizeChanged += (_, e) =>
			{
				if (Height > 70)
					DataPersistence.SaveSingleHatchDialogHeight(Height);
			};

			List<SingleHatchData> hatches = DataPersistence.GetHatchData();
			if (hatches != null)
				SetData(hatches);

			Action<Control, string> initButton = (c, s) =>
			{
				c.MouseEnter += (_, __) => textBoxHelp.Text = s;
				c.MouseLeave += (_, __) => textBoxHelp.Text = "";
			};
			Text = "Fläche Toolbar";
			textBoxHelp.Text = "";
			initButton(buttonAddHatch, "NEUER Flächen-Stempel");//"Add hatch style");
			initButton(buttonCreateTable, "Tabelle Erstellen");//"Create table with selected attributes");
			initButton(buttonDeleteTable, "Tabelle Löschen");//"Delete selected table");
			initButton(buttonAppendTableRow, "Flächentext zu Tabelle");// "Select attribute and add it to selected table");
			initButton(buttonRemoveArea, "Flächentext in Tabelle Löschen");// "Remove selected attribute from selected row");
			initButton(buttonSelectAttributeTables, "Flächenobjekt anwählen zur Anzeige der verknüpften Tabelle");// "Select all tables for an attribute.");
			initButton(buttonSelectionSetSingle, "Flächentext in Tabelle wählen zur Anzeige der verknüpften Fläche");//"Select attribute for selected row in table");
			initButton(buttonSelectionSetAll, "Objekt der Tabelle wählen zur Anzeige der verknüpften Flächen");//"Select all attributes for selected table");
			deleteToolStripMenuItem.Text = "Löschen";



			FormClosing += (_, e) =>
			{
				// This can't be used because we get UserClosing also when closing CAD itself.
				// And we need to know exactly close event of our form.
				//if (e.CloseReason == CloseReason.UserClosing)
				if (UserPressedClose)
					Properties.HatchToolbar.Default.ToolbarOpened = false;
				else
					Properties.HatchToolbar.Default.ToolbarOpened = true;
				// Save hatch data
				List<SingleHatchData> list = new List<SingleHatchData>();
				foreach (DataGridViewRow row in dataGridView.Rows)
					list.Add((SingleHatchData)row.Tag);
				DataPersistence.SaveHatchData(list);
				DataPersistence.SaveButtonsPanelCollapsed(splitContainer1.Panel2Collapsed);

				DataPersistence.SaveSingleHatchDialogWidth(Width);
				DataPersistence.SaveSingleHatchDialogPositionX(Location.X);
				DataPersistence.SaveSingleHatchDialogPositionY(Location.Y);
			};
		}
		protected override void WndProc(ref Message m)
		{
			// Change behavior for Minimize and Maximize buttons.
			// Minimize makes form smaller, Maximize makes bigger.
			if (m.Msg == WM_SYSCOMMAND)
			{
				if (m.WParam.ToInt32() == SC_MINIMIZE)
				{
					if (Height > 50)
						lastHeight = Height;
					m.Result = IntPtr.Zero;
					Height = 30;
					return;
				}
				if (m.WParam.ToInt32() == SC_MAXIMIZE)
				{
					if (lastHeight < 70)
						lastHeight = 300;
					Height = lastHeight;
					m.Result = IntPtr.Zero;
					return;
				}
				if (m.WParam.ToInt32() == SC_CLOSE)
				{
					UserPressedClose = true;
				}
			}
			base.WndProc(ref m);
		}

		/// <summary>
		/// Adds new hatch, by using same settings like previous hatch (or default values, if dgv empty).
		/// </summary>
		private void AddHatch()
		{
			SingleHatchData h = null;
			if (dataGridView.Rows.Count == 0)
				h = new SingleHatchData();
			else
			{
				int lastIndex = dataGridView.Rows.Count - 1;
				SingleHatchData sh = (SingleHatchData)dataGridView.Rows[lastIndex].Tag;
				//string oldName = h.Name;
				h = new SingleHatchData(sh);
				//h.Name = oldName;
			}
			AddHatch(h);
		}
		/// <summary>
		/// Adds existing hatch.
		/// </summary>
		private void AddHatch(SingleHatchData h)
		{
			int index = dataGridView.Rows.Add("", h.Name);
			dataGridView.Rows[index].Tag = h;
		}
		private void RemoveHatch(int index)
		{
			if (dataGridView.Rows.Count > index)
			{
				dataGridView.Rows.RemoveAt(index);
			}
		}
		private void RefreshRowData(int rowIndex)
		{
			DataGridViewRow row = dataGridView.Rows[rowIndex];
			SingleHatchData sh = (SingleHatchData)row.Tag;
			row.Cells[1].Value = sh.Name;
		}
		private void SetData(IEnumerable<SingleHatchData> hatches)
		{
			foreach (SingleHatchData h in hatches)
			{
				AddHatch(h);
			}
		}

		private void InitializeDGV()
		{
			Color background = ToolbarColorSettings.Instance.BackgroundColor;
			Color text = ToolbarColorSettings.Instance.TextColor;
			BackColor = background;
			dataGridView.BackgroundColor = background;
			dataGridView.GridColor = background;
			textBoxHelp.BackColor = background;

			foreach (DataGridViewColumn column in dataGridView.Columns)
			{
				column.DefaultCellStyle.BackColor = background;
				column.DefaultCellStyle.SelectionBackColor = background;

				column.DefaultCellStyle.ForeColor = text;
				column.DefaultCellStyle.SelectionForeColor = text;

			}

			dataGridView.RowTemplate.DefaultCellStyle.BackColor = background;
			dataGridView.RowTemplate.DefaultCellStyle.SelectionBackColor = background;
			dataGridView.RowTemplate.DefaultCellStyle.ForeColor = text;
			dataGridView.RowTemplate.DefaultCellStyle.SelectionForeColor = text;
			dataGridView.Cursor = Cursors.Default;
			Cursor last = Cursor;
			int currentRow = -1;
			dataGridView.CellMouseEnter += (_, e) =>
			{
				last = Cursor;
				currentRow = e.RowIndex;
				dataGridView.InvalidateRow(e.RowIndex);
				if (e.ColumnIndex == 2)
					dataGridView.Cursor = Cursors.Arrow;
				else
					dataGridView.Cursor = Cursors.Hand;
			};
			dataGridView.CellMouseLeave += (_, e) =>
			{
				currentRow = -1;
				dataGridView.InvalidateRow(e.RowIndex);
				// Only for last row, to prevent often cursor style changes.
				if (e.RowIndex == dataGridView.Rows.Count - 1)
					dataGridView.Cursor = Cursors.Default;
			};

			dataGridView.RowsAdded += (_, e) =>
			{
				/*for (int i = e.RowIndex; i < e.RowIndex + e.RowCount; i++)
				{
					DataGridViewRow row = dataGridView.Rows[i];
					foreach (DataGridViewCell cell in row.Cells)
					{
						string toolTipText = "";
						if (cell.OwningColumn is DataGridViewButtonColumn)
							toolTipText = "Neue Schraffur-Typ Definition";// "Click to edit settings";
						else
							toolTipText = "NEUER Flächen-Stempel";// "Click to create hatch";
						cell.ToolTipText = toolTipText;
					}
				}*/
			};

			((DataGridViewButtonColumn)dataGridView.Columns[2]).FlatStyle = FlatStyle.Standard;

			dataGridView.CellPainting += (_, e) =>
			{
				if (e.ColumnIndex == 2)
				{
					if (currentRow != e.RowIndex)
					{
						if (SettingsNoImageBackground == null)
							SettingsNoImageBackground = new SolidBrush(ToolbarColorSettings.Instance.BackgroundColor);
						e.Graphics.FillRectangle(SettingsNoImageBackground, e.CellBounds);
					}
					else
					{
						e.Paint(e.ClipBounds, e.PaintParts);
						Rectangle rec = new Rectangle(e.CellBounds.X + 6, e.CellBounds.Y + 6, e.CellBounds.Width - 12, e.CellBounds.Height - 12);
						if (ToolbarColorSettings.Instance.BackgroundColor.R < 100)
							e.Graphics.DrawImage(Properties.Resources.gear_black, rec);
						else
							e.Graphics.DrawImage(Properties.Resources.gear_white, rec);
					}
					e.Handled = true;
				}
				if (e.ColumnIndex == 0)
				{
					if (SettingsNoImageBackground == null)
						SettingsNoImageBackground = new SolidBrush(ToolbarColorSettings.Instance.BackgroundColor);

					Random r = new Random();
					SingleHatchData h = (SingleHatchData)dataGridView.Rows[e.RowIndex].Tag;
					Color color = h.HatchType.PatternColor.ToColor();
					Rectangle bounds = new Rectangle(e.CellBounds.X + 3, e.CellBounds.Y + 3, e.CellBounds.Width - 6, e.CellBounds.Height - 6);
					e.Graphics.FillRectangle(SettingsNoImageBackground, e.CellBounds);
					e.Graphics.FillRectangle(new SolidBrush(color), bounds);
					e.Handled = true;
				}
			};
			dataGridView.RowPostPaint += (_, e) =>
			{
				if (e.RowIndex == currentRow)
				{
					Rectangle bounds = new Rectangle(e.RowBounds.X + 1, e.RowBounds.Y + 1, e.RowBounds.Width - 2, e.RowBounds.Height - 2);
					e.Graphics.DrawRectangle(new Pen(Color.Black), bounds);
					//Cursor = Cursors.Hand;
				}
			};
			dataGridView.EditingControlShowing += (_, e) => e.Control.Cursor = Cursors.Hand;

			Disposed += (_, __) =>
			{
				if (SettingsNoImageBackground != null)
					SettingsNoImageBackground.Dispose();
				SettingsNoImageBackground = null;
			};

			dataGridView.CellContentClick += (_, e) =>
			{
				if (e.RowIndex >= 0 && dataGridView.Columns[e.ColumnIndex] is DataGridViewButtonColumn)
				{
					SingleHatchData h = (SingleHatchData)dataGridView.Rows[e.RowIndex].Tag;
					// Start settings dialog for provided hatch.
					bool changed = SingleHatchViewController.Show(h);
					if (changed)
					{
						dataGridView.InvalidateRow(e.RowIndex);
						RefreshRowData(e.RowIndex);
					}
				}
			};
			int contextRowIndex = 0;
			dataGridView.CellMouseClick += (_, e) =>
			{
				contextRowIndex = e.RowIndex;
				if (e.Button == MouseButtons.Right)
				{
					DataGridViewRow row = dataGridView.Rows[e.RowIndex];
					Point p = dataGridView.PointToScreen(e.Location);

					Rectangle rec = dataGridView.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
					p.X += rec.X;
					p.Y += rec.Y;
					contextMenuStrip1.DefaultDropDownDirection = ToolStripDropDownDirection.BelowRight;
					contextMenuStrip1.Show(p);
					//System.Diagnostics.Debug.WriteLine("dgv" + e.Location + "  ==  " + dataGridView.PointToScreen(e.Location));
				}
				if (e.RowIndex >= 0 && e.ColumnIndex >= 0 && e.Button == MouseButtons.Left)
				{
					if (e.ColumnIndex != dataGridViewColumnSettings.Index)
					{
						SingleHatchData h = (SingleHatchData)dataGridView.Rows[e.RowIndex].Tag;
						OnHatchSelected(h);
					}
				}
			};
			contextMenuStrip1.Items[0].Click += (_, e) =>
			{
				RemoveHatch(contextRowIndex);
			};
			//MouseMove += (_, e) => System.Diagnostics.Debug.WriteLine(e.Location + "  ==  " + PointToScreen(e.Location));
		}

		private void buttonAdd_Click(object sender, EventArgs e)
		{
			AddHatch();
		}
		private void OnHatchSelected(SingleHatchData data)
		{
			if (HatchSelected != null)
				HatchSelected.Invoke(data);
		}

		private void buttonCreateTable_Click(object sender, EventArgs e)
		{
			CreateTable.Invoke();
		}

		private void buttonDeleteTable_Click(object sender, EventArgs e)
		{
			DeleteTable.Invoke();
		}

		private void buttonAppendTableRow_Click(object sender, EventArgs e)
		{
			AppendAreaToTable.Invoke();
		}

		private void buttonRemoveArea_Click(object sender, EventArgs e)
		{
			RemoveAreaFromtTable.Invoke();
		}

		private void buttonSelectionSetSingle_Click(object sender, EventArgs e)
		{
			AreaToSelection.Invoke();
		}

		private void buttonSelectionSetAll_Click(object sender, EventArgs e)
		{
			AllAreasToSelection.Invoke();
		}

		private void buttonSelectAttributeTables_Click(object sender, EventArgs e)
		{
			AreaTablesToSelection.Invoke();
		}

		private void buttonCsvSave_Click(object sender, EventArgs e)
		{
			SaveTableToCSV.Invoke();
		}

		private void buttonToggleShow_Click(object sender, EventArgs e)
		{
			toggleBottomPanel(splitContainer1.Panel2Collapsed);
		}
		private void toggleBottomPanel(bool currentCollapsed)
		{
			bool nextCollapsed = !currentCollapsed;
			int height = splitContainer1.Panel2.Bounds.Height;
			//splitContainer1.Panel2Collapsed = nextCollapsed;
			string contents = "";
			if (nextCollapsed)
			{
				//contents = @"\/";
				splitContainer1.Panel2Collapsed = nextCollapsed;
				buttonToggleShow.BackgroundImage = Properties.Resources.arrow_down;
				Height -= height;
			}
			else
			{
				//contents = @"/\";
				buttonToggleShow.BackgroundImage = Properties.Resources.arrow_up;
				Height += height;
				splitContainer1.Panel2Collapsed = nextCollapsed;
			}
			buttonToggleShow.Text = contents;
		}

		private void buttonSettings_Click(object sender, EventArgs e)
		{
			TableSettings.Invoke();
		}


		private void buttonImportExport_Click(object sender, EventArgs e)
		{
			ImportExportSettings settings = ImportExportController.StartDialog();
			if (settings == null)
				return;

			if (settings.Export)
			{
				List<SingleHatchData> list = new List<SingleHatchData>();
				foreach (DataGridViewRow row in dataGridView.Rows)
					list.Add((SingleHatchData)row.Tag);
				ImportExportManager.Export(list);
			}
			if (settings.Import)
			{
				List<SingleHatchData> data = ImportExportManager.Import();
				if (settings.Rules == ImportRules.Replace)
					dataGridView.Rows.Clear();
				if (data != null)
					foreach (SingleHatchData d in data)
						AddHatch(d);

			}
		}
	}
	class DragRow
	{
		/// <summary>
		/// Indicates if dragging operation is in progress.
		/// </summary>
		public bool IsDragging { get; set; }
		private int lastRowIndex = -1;
		private DataGridViewRow moveRow;
		private Point previous;
		public DragRow(DataGridView dgv)
		{
			dgv.AllowDrop = true;
			dgv.MouseDown += (_, e) =>
			{
				previous = e.Location;
			};
			dgv.MouseUp += (_, e) =>
			{
			};
			dgv.MouseMove += (_, e) =>
			{
				//System.Diagnostics.Debug.WriteLine("mouse moved: x: " + (e.X - previous.X) + " y: " + (e.Y - previous.Y));
				if ((e.Button & MouseButtons.Left) != MouseButtons.Left)
					return;
				double deltaX = e.X - previous.X;
				double deltaY = e.Y - previous.Y;
				previous = e.Location;
				if (Math.Abs(deltaX) < 2 && Math.Abs(deltaY) < 2)
					return;

				if (!IsDragging)
				//if (dgv.SelectedRows.Count == 1)
				{
					//if (e.Button == MouseButtons.Left)
					{

						Point clientPoint = dgv.PointToClient(new Point(e.X, e.Y));
						int index = dgv.HitTest(e.X, e.Y).RowIndex;
						if (index != -1)
						{
							moveRow = dgv.Rows[index];
							lastRowIndex = moveRow.Index;
							IsDragging = true;
							DragDropEffects effexts = dgv.DoDragDrop(moveRow, DragDropEffects.Move);
						}
					}
				}
			};
			dgv.DragLeave += (_, e) =>
			{
				IsDragging = false;

			};
			dgv.DragOver += (_, e) =>
			{
				if (IsDragging)
				{
					e.Effect = DragDropEffects.Move;
					Point p = dgv.PointToClient(new Point(e.X, e.Y));
					int index = dgv.HitTest(p.X, p.Y).RowIndex;
					if (index > -1 && index < dgv.Rows.Count && index != lastRowIndex)
					{
						dgv.Rows.RemoveAt(lastRowIndex);
						dgv.Rows.Insert(index, moveRow);
						lastRowIndex = index;
					}
				}
				else
					e.Effect = DragDropEffects.None;
			};
			dgv.DragDrop += (_, e) =>
			{
				//dgv.
				if (!IsDragging)
					return;
				int endIndex;
				Point clientPoint = dgv.PointToClient(new Point(e.X, e.Y));
				endIndex = dgv.HitTest(clientPoint.X, clientPoint.Y).RowIndex;
				if (e.Effect == DragDropEffects.Move)
				{
					//dgv.Rows.RemoveAt(startRowIndex);
					//dgv.Rows.Insert(endIndex, moveRow);
				}
				IsDragging = false;
			};
		}
	}
}