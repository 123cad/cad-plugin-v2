﻿using CadPlugin.Common;
using GlobalDependencyInjection;
using Ninject;
using Ninject.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin
{
    /// <summary>
    /// Dependency injection.
    /// (Loading all NinjectModules on startup)
    /// </summary>
    public class DI
    {
        public static IKernel Kernel { get; private set; }

        //NOTE Assembly.GetType() resolves all references in the Assembly (including
        //NOTE CAD libraries, which causes DI load to fail). We don't want CAD when running regular unit tests.
        //NOTE This is why we need to predefine type names, and load them directly.
        private static Type[] DiModules = new Type[]
        {
            typeof(BaseDIModule), // Just for reference.
            typeof(DIModule),
            typeof(KSopt.DIModule),
        };
        private static Type[] DiModulesCad = new Type[]
        {
            typeof(BaseDiModuleCad), // Just for reference, don't use it.
            typeof(DIModuleCad),
            typeof(KSopt.DiModuleCad),
        };


        static DI()
        {
            Kernel = new StandardKernel();
            Load();
        }
        private static void Load()123
        {
            var allTypes = DiModules;
            var t = allTypes[0];
            var moduleTypes = allTypes.Where(x => x.IsClass && !x.IsAbstract && t.IsAssignableFrom(x));
            foreach (var type in moduleTypes)
            {
                Kernel.Load((BaseDIModule)Activator.CreateInstance(type));
            }
        }

        /// <summary>
        /// Loads CAD Ninject module (MdiActiveDocument).
        /// </summary>
        public static void LoadCad()
        {
            var allTypes = DiModulesCad;
            var t = allTypes[0];
            var moduleTypes = allTypes.Where(x => x.IsClass && !x.IsAbstract && t.IsAssignableFrom(x));
            foreach (var type in moduleTypes)
            {
                Kernel.Load((BaseDiModuleCad)Activator.CreateInstance(type));
            }
        }

        public static T Get<T>()
        {
            T v = default(T);

            // Bricscad throws NotSupported exception on first invoke of
            // DI. But everything seem to work.
            // Bricscad.ApplicationServices.AssemblyLoader.OnLoad
            v = Kernel.Get<T>();
            return v;
        }
        public static T Get<T>(params IParameter[] parameters)
        {
            return Kernel.Get<T>(parameters);
        }

        public static void LoadModule(Type t)
        {
            if (!typeof(BaseDIModule).IsAssignableFrom(t))
            {
                throw new InvalidCastException($"{t.Name} is not subclass of {nameof(BaseDIModule)}");
            }

            Kernel.HasModule(t.Name);
        }
    }
}
