﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.BlockRotations
{
    class BlockState
    {
        private class AttributeState
        {
            public ObjectId Id { get; set; }
            public double RotationRad{ get; set; }
            public Point3d Position { get; set; }
        }

        private ObjectId blockId;
        private double rotation;
        private Point3d position;
        private readonly List<AttributeState> attributes = new List<AttributeState>();
        private Document doc;
        private BlockState (){}

        public static BlockState Create(Document doc, ObjectId blockId)
        {
            var state = new BlockState()
            {
                blockId = blockId,
                doc = doc
            };

            using (var tw = doc.StartTransaction(true))
            {
                var br = tw.GetObjectRead<BlockReference>(blockId);
                state.rotation = br.Rotation;
                state.position = br.Position;
                foreach (ObjectId aid in br.AttributeCollection)
                {
                    var ar = tw.GetObjectRead<AttributeReference>(aid);
                    state.attributes.Add(new AttributeState()
                    {
                        Id = aid,
                        RotationRad = ar.Rotation,
                        Position = ar.Position
                    });
                }
            }
            return state;
        }

        /// <summary>
        /// Set 0 for all rotations.
        /// </summary>
        public void Reset()
        {
            using (var tw = doc.StartTransaction(true))
            {
                var br = tw.GetObjectWrite<BlockReference>(blockId);
                Matrix3d? m = null;
                foreach (var att in attributes)
                {
                    var ar = tw.GetObjectWrite<AttributeReference>(att.Id);
                    if (!m.HasValue)
                        m = Matrix3d.Rotation(-ar.Rotation, Vector3d.ZAxis, br.Position);
                    ar.Position = att.Position;
                    ar.TransformBy(m.Value);
                    //ar.Rotation = 0;
                }
                br.Position = position;
                br.Rotation = 0;
            }
        }
        public void Restore()
        {
            using (var tw = doc.StartTransaction(true))
            {
                var br = tw.GetObjectWrite<BlockReference>(blockId);
                br.Position = position;
                br.Rotation = rotation;
                foreach (var att in attributes)
                {
                    var ar = tw.GetObjectWrite<AttributeReference>(att.Id);
                    ar.Position = att.Position;
                    ar.Rotation = att.RotationRad;
                }
            }
        }
    }
}
