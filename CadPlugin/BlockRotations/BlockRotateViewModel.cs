﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.BlockRotations
{
    public class BlockRotateViewModel : Notify
    {
        public double AngleDeg
        {
            get => _angleDeg;
            set
            {
                while (value < -180)
                    value += 360;
                while (180 < value)
                    value -= 360;
                OnPropertyChanged(() => _angleDeg = value);
            }
        }

        public bool IsSymbolRotated
        {
            get => _isSymbolRotated;
            set => OnPropertyChanged(()=> _isSymbolRotated = value);
        }

        public bool IsAttributeRotated
        {
            get => _isAttributeRotated;
            set => OnPropertyChanged(()=> _isAttributeRotated = value);
        }

        public bool IsAlignedTo
        {
            get => _isAlignedTo;
            set
            {
                OnPropertyChanged(() => _isAlignedTo = value);
                mgr.UpdateAngle();
            }
        }

        public AlignmentSource AlignmentSource { get; } 

        private double _angleDeg;
        private BlockRotationManager mgr;
        private bool _isSymbolRotated;
        private bool _isAttributeRotated;
        private bool _isAlignedTo;

        public static void ShowWindow(Document doc, ObjectId bId)
        {
            var m = new BlockRotateViewModel(doc, bId);
            var view = new BlockRotateView(m);

            Application.ShowModalWindow(view);
        }
        private BlockRotateViewModel(Document doc, ObjectId blockId)
        {
            mgr = new BlockRotationManager(doc, blockId, this);
            AlignmentSource = new AlignmentSource(doc);
            _isSymbolRotated = true;
            _isAttributeRotated = true;

            PropertyChanged += BlockRotateViewModel_PropertyChanged;
        }


        private void BlockRotateViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            mgr.UpdateAngle();
        }

        public void ResetRotation()
        {
            mgr.ResetRotation();
        }

        public void SelectAlignmentEntity()
        {
            AlignmentSource.SelectObject(mgr.ReferencePoint);
            mgr.UpdateAngle();
        }
    }
}
