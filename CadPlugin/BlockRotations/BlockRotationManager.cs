﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.BlockRotations
{
    class BlockRotationManager : IDisposable
    {
        public Point3d ReferencePoint { get; private set; }

        private Document doc;
        private ObjectId blockId;
        private BlockRotateViewModel model;
        private BlockState state;
        private bool isDisposed;
        private bool recalculateFlag;
        
        public BlockRotationManager(Document doc, ObjectId blockId, BlockRotateViewModel model)
        {
            this.doc = doc;
            this.blockId = blockId;
            this.model = model;

            state = BlockState.Create(doc, blockId);

            UpdateRunner();

            using (var tw = doc.StartTransaction(true))
            {
                var br = tw.GetObjectWrite<BlockReference>(blockId);
                ReferencePoint = br.Position;
                model.AngleDeg = MyUtilities.Helpers.Calculations.RadiansToDegrees(br.Rotation);
            }
        }

        private async void UpdateRunner()
        {
            // Runner is to prevent too many calls to redraw.
            while (!isDisposed)
            {
                await Task.Delay(50);

                if (isDisposed)
                    return;

                if (recalculateFlag)
                {
                    recalculateFlag = false;

                    Recalculate();
                }

            }
        }

        private void Recalculate()
        {
            state.Restore();
            using (var tw = doc.StartTransaction(true))
            {
                var br = tw.GetObjectWrite<BlockReference>(blockId);
                double angle = model.AngleDeg;
                if (model.IsAlignedTo)
                    angle = model.AlignmentSource.GetAngleRelativeToObject(angle);
                var pars = BlockRotationParameters.CreateRotateTo(angle, model.IsSymbolRotated,
                    model.IsAttributeRotated);
                br.Rotate(tw, pars);
            }
        }

        public void UpdateAngle()
        {
            recalculateFlag = true;
        }

        public void ResetRotation()
        {
            state.Reset();
            model.AngleDeg = 0;
        }

        public void Dispose()
        {
            isDisposed = true;
        }
    }
}
