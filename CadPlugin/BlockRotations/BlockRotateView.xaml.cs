﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CadPlugin.BlockRotations
{
    /// <summary>
    /// Interaction logic for BlockRotateView.xaml
    /// </summary>
    public partial class BlockRotateView : Window
    {
        private BlockRotateViewModel model;
        public BlockRotateView(BlockRotateViewModel model)
        {
            InitializeComponent();
            this.model = model;
            DataContext = model;
        }

        private void resetClick(object sender, RoutedEventArgs e)
        {
            model.ResetRotation();
        }

        private void closeClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void selectEntity_Click(object sender, RoutedEventArgs e)
        {
            model.SelectAlignmentEntity();
        }
    }
}
