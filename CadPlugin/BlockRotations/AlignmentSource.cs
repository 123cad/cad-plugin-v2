﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using my = MyUtilities.Geometry;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.BlockRotations
{
    public class AlignmentSource
    {
        public bool IsObjectSelected => oid != ObjectId.Null;

        private ObjectId oid = ObjectId.Null;
        private Document doc;
        private my.Vector2d direction;

        public AlignmentSource(Document doc)
        {
            this.doc = doc;
        }

        public void SelectObject(Point3d referencePoint)
        {
            // Select BlockReference.
            var opts = new PromptEntityOptions("Select line/poly");
            opts.SetRejectMessage("Not a Line!");
            opts.AddAllowedClass(typeof(Line), true);
            opts.AddAllowedClass(typeof(Polyline), true);
            opts.AddAllowedClass(typeof(Polyline2d), true);
            opts.AddAllowedClass(typeof(Polyline3d), true);
            opts.AllowNone = true;
            var res = doc.Editor.GetEntity(opts);
            if (res.Status != PromptStatus.OK)
            {
                doc.Editor.WriteMessage("Canceled");
                return;
            }

            oid = res.ObjectId;
            using (var tw = doc.StartTransaction(true))
            {
                var e = tw.GetObjectWrite<Entity>(oid);
                List<Point3d> pts;
                if (e is Line l)
                    pts = new List<Point3d>() {l.StartPoint, l.EndPoint};
                else
                    pts = CADPolylineHelper.GetPoints(e, tw.Tr);
                using (var pl = new Polyline(pts.Count))
                {
                    pl.AddPoints(pts.Select(x => x.GetAs2d()));
                    var p1 = pl.GetClosestPointTo(referencePoint, false);
                    var parameter = pl.GetParameterAtPoint(p1);
                    double nextParameter = parameter - 0.01;
                    if (nextParameter < 0.1)
                        nextParameter = parameter + 0.01;
                    var p2 = pl.GetPointAtParameter(nextParameter);

                    direction = p2.GetVectorTo(p1).To2d().FromCADVector();

                }
            }
        }

        /// <summary>
        /// Returns new angle, aligning it to direction instead of
        /// axis.
        /// </summary>
        /// <example>
        /// angleRad = 30deg, direction=(1,1).
        /// new angle = 75deg
        /// </example>
        public double GetAngleRelativeToObject(double angleDeg)
        {
            if (!IsObjectSelected)
                return angleDeg;

            double zeroAngle = my.Vector2d.XAxis.GetAngleDeg(direction);
            double newAngle = zeroAngle + angleDeg;

            return newAngle;
        }
    }
}
