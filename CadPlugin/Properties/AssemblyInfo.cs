// (C) Copyright 2014 by Microsoft 

//
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CadPlugin")]
[assembly: AssemblyDescription("Plugin for CAD")]
[assembly: AssemblyConfiguration("")]
// Debug library has D as suffix.
#if DEBUG
[assembly: AssemblyCompany("123CAD ingenieursoftware D")]
#else
[assembly: AssemblyCompany("123CAD ingenieursoftware")]
#endif
[assembly: AssemblyProduct("CadPlugin")]
[assembly: AssemblyCopyright("Copyright © 123CAD 2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]


// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("3.8.0.1")]
#pragma warning disable CS7035 // The specified version string does not conform to the recommended format - major.minor.build.revision
[assembly: AssemblyFileVersion("3.8.0.1")]
#pragma warning restore CS7035 // The specified version string does not conform to the recommended format - major.minor.build.revision
[assembly: AssemblyInformationalVersion("123CAD 3.8")]

// In order to sign your assembly you must specify a key to use. Refer to the 
// Microsoft .NET Framework documentation for more information on assembly signing.
//
// Use the attributes below to control which key is used for signing. 
//
// Notes: 
//   (*) If no key is specified, the assembly is not signed.
//   (*) KeyName refers to a key that has been installed in the Crypto Service
//       Provider (CSP) on your machine. KeyFile refers to a file which contains
//       a key.
//   (*) If the KeyFile and the KeyName values are both specified, the 
//       following processing occurs:
//       (1) If the KeyName can be found in the CSP, that key is used.
//       (2) If the KeyName does not exist and the KeyFile does exist, the key 
//           in the KeyFile is installed into the CSP and used.
//   (*) In order to create a KeyFile, you can use the sn.exe (Strong Name) utility.
//       When specifying the KeyFile, the location of the KeyFile should be
//       relative to the project output directory which is
//       %Project Directory%\obj\<configuration>. For example, if your KeyFile is
//       located in the project directory, you would specify the AssemblyKeyFile 
//       attribute as [assembly: AssemblyKeyFile("..\\..\\mykey.snk")]
//   (*) Delay Signing is an advanced option - see the Microsoft .NET Framework
//       documentation for more information on this.
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]
[assembly: AssemblyKeyName("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c870ec62-1fd3-4848-b4c5-2791b68e57e0")]

[assembly:InternalsVisibleTo("CadPlugin_Test")]
[assembly:InternalsVisibleTo("CadPlugin_RunTester")]
[assembly:InternalsVisibleTo("CadPlugin_RunTester_CAD")]
[assembly: InternalsVisibleTo("CadPlugin_NUnitTests")]
[assembly: InternalsVisibleTo("CadPlugin__NUnitTests")]
[assembly:InternalsVisibleTo("CadPlugin_UnitTests_CAD")]
[assembly:InternalsVisibleTo("CadPlugin_NUnitTests_CAD")]
[assembly:InternalsVisibleTo("CadPlugin_IntegrationTests_CAD")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
[assembly: InternalsVisibleTo("CadBench")]
[assembly: InternalsVisibleTo("Autotrack")]

