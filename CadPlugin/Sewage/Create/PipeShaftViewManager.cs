﻿using CadPlugin.Sewage.Create.Settings;
using CadPlugin.Sewage.Plannings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Sewage.Create
{
	/// <summary>
	/// Controls visibility of of fields.
	/// </summary>
	interface IPipeShaftViewManager
	{
		/// <summary>
		/// Sets Enable and Visible state for related controls.
		/// </summary>
		void InitializeStartShaft();
		/// <summary>
		/// Sets Enable and Visible state for related controls.
		/// </summary>
		void InitializeEndShaft();

		/// <summary>
		/// Sets Enable state for ShaftControl (complete control is enabled or disabled).
		/// </summary>
		void ToggleMinDeltaStart(bool enabled);
		void ToggleMinDeltaEnd(bool enabled);
	}

	class PipeShaftViewManagerDownstream : IPipeShaftViewManager
	{
		private PipeShaftView View { get; set; }
		private PipeShaftDataManager Manager { get; set; }
		private IShaftData StartShaft { get { return Manager.Data.StartShaft; } }
		private ShaftControls.IShaftControlView StartShaftView { get { return View.StartShaft; } }
		private ShaftControls.IShaftControlView EndShaftView { get { return View.EndShaft; } }
		private DataViewController Controller;
		public PipeShaftViewManagerDownstream(PipeShaftView view, PipeShaftDataManager mgr, DataViewController controller)
		{
			View = view;
			Manager = mgr;
			Controller = controller;
		}

		public void InitializeStartShaft()
		{
			bool first = !Manager.PreviousDataExists;
			StartShaftView.MinDelta.Visible = false;
			bool minDeltaUsed = Manager.Data.ApplyStartShaftPipeOutDelta;
			//StartShaftView.MinPipeOutDeltaUsed.Visible = false;
			if (first)
			{
				StartShaftView.CoverHeight.Enabled = true;
				StartShaftView.CoverTerrainDelta.Enabled = true;
				StartShaftView.TerrainHeight.Enabled = false;
				StartShaftView.TerrainPipeOutDelta.Enabled = !minDeltaUsed;
				StartShaftView.PipeOutHeight.Enabled = !minDeltaUsed;
				StartShaftView.PipeOutBottomDelta.Enabled = true;
				StartShaftView.BottomHeight.Enabled = true;

				StartShaftView.MinPipeOutDelta.Enabled = minDeltaUsed;
			}
			else
			{
				StartShaftView.Diameter.Enabled = false;
				StartShaftView.InnerSlope.Enabled = false;
				StartShaftView.CoverHeight.Enabled = false;
				StartShaftView.CoverTerrainDelta.Enabled = false;
				StartShaftView.TerrainHeight.Enabled = false;
				StartShaftView.TerrainPipeOutDelta.Enabled = true;
				StartShaftView.PipeOutHeight.Enabled = true;
				StartShaftView.PipeOutBottomDelta.Enabled = false;
				StartShaftView.BottomHeight.Enabled = false;

				StartShaftView.MinPipeOutDelta.Enabled = Manager.Data.ApplyStartShaftPipeOutDelta;
			}
			ToggleMinDeltaStart(Manager.Data.ApplyStartShaftPipeOutDelta);
		}
		public void InitializeEndShaft()
		{
			bool first = !Manager.PreviousDataExists;
			bool pointSelected = Manager.Data.EndPointSet;
			EndShaftView.MinDelta.Visible = false;
			if (first)
			{
				EndShaftView.CoverHeight.Enabled = true;
				EndShaftView.CoverTerrainDelta.Enabled = true;
				EndShaftView.TerrainHeight.Enabled = false;
				EndShaftView.TerrainPipeOutDelta.Enabled = false;
				EndShaftView.PipeOutHeight.Enabled = pointSelected;
				EndShaftView.PipeOutBottomDelta.Enabled = true;
				EndShaftView.BottomHeight.Enabled = true;

				EndShaftView.MinPipeOutDelta.Enabled = Manager.Data.ApplyEndShaftPipeOutDelta;
			}
			else
			{
				EndShaftView.CoverHeight.Enabled = true;
				EndShaftView.CoverTerrainDelta.Enabled = true;
				EndShaftView.TerrainHeight.Enabled = false;
				EndShaftView.TerrainPipeOutDelta.Enabled = false;
				EndShaftView.PipeOutHeight.Enabled = pointSelected;
				EndShaftView.PipeOutBottomDelta.Enabled = true;
				EndShaftView.BottomHeight.Enabled = true;

				EndShaftView.MinPipeOutDelta.Enabled = Manager.Data.ApplyEndShaftPipeOutDelta;

			}
			ToggleMinDeltaEnd(Manager.Data.ApplyEndShaftPipeOutDelta);
		}

		public void ToggleMinDeltaStart(bool enabled)
		{
			StartShaftView.TerrainPipeOutDelta.Enabled = !enabled;
			StartShaftView.PipeOutHeight.Enabled = !enabled;
			StartShaftView.MinPipeOutDelta.Enabled = enabled;
		}

		public void ToggleMinDeltaEnd(bool enabled)
		{
			//EndShaftView.TerrainPipeOutDelta.Enabled = !enabled;
			EndShaftView.PipeOutHeight.Enabled = enabled && (Manager.Data.EndPointSet || Controller.PointWasSelected);
			EndShaftView.MinPipeOutDelta.Enabled = enabled;
		}
	}
	class PipeShaftViewManagerUpstream : IPipeShaftViewManager
	{
		private PipeShaftView View { get; set; }
		private PipeShaftDataManager Manager { get; set; }

		private IShaftData StartShaft { get { return Manager.Data.StartShaft; } }
		private ShaftControls.IShaftControlView StartShaftView { get { return View.StartShaft; } }
		private ShaftControls.IShaftControlView EndShaftView { get { return View.EndShaft; } }
		private IShaftData EndShaft { get { return Manager.Data.EndShaft; } }
		private DataViewController Controller;
		public PipeShaftViewManagerUpstream(PipeShaftView view, PipeShaftDataManager mgr, DataViewController controller)
		{
			View = view;
			Manager = mgr;
			Controller = controller;
		}


		public void InitializeStartShaft()
		{
			bool first = !Manager.PreviousDataExists;
			StartShaftView.MinDelta.Visible = true;
			StartShaftView.MinPipeOutDeltaUsed.Visible = false;
			bool pointSelected = Manager.Data.StartPointSet;
			if (first)
			{
				StartShaftView.CoverHeight.Enabled = true;
				StartShaftView.CoverTerrainDelta.Enabled = true;
				StartShaftView.TerrainHeight.Enabled = false;
				StartShaftView.TerrainPipeOutDelta.Enabled = false;
				StartShaftView.PipeOutHeight.Enabled = pointSelected;
				StartShaftView.PipeOutBottomDelta.Enabled = true;
				StartShaftView.BottomHeight.Enabled = true;

				StartShaftView.MinPipeOutDelta.Enabled = Manager.Data.ApplyStartShaftPipeOutDelta;
			}
			else
			{
				StartShaftView.CoverHeight.Enabled = true;
				StartShaftView.CoverTerrainDelta.Enabled = true;
				StartShaftView.TerrainHeight.Enabled = false;
				StartShaftView.TerrainPipeOutDelta.Enabled = false;
				StartShaftView.PipeOutHeight.Enabled = pointSelected;
				StartShaftView.PipeOutBottomDelta.Enabled = true;
				StartShaftView.BottomHeight.Enabled = true;

				StartShaftView.MinPipeOutDelta.Enabled = Manager.Data.ApplyStartShaftPipeOutDelta;
			}
			ToggleMinDeltaStart(Manager.Data.ApplyStartShaftPipeOutDelta);
		}
		public void InitializeEndShaft()
		{
			bool first = !Manager.PreviousDataExists;
			bool pointSelected = Manager.Data.EndPointSet;
			EndShaftView.MinDelta.Visible = false;
			bool minDeltaUsed = Manager.Data.ApplyEndShaftPipeOutDelta;
			if (first)
			{
				EndShaftView.CoverHeight.Enabled = true;
				EndShaftView.CoverTerrainDelta.Enabled = true;
				EndShaftView.TerrainHeight.Enabled = false;
				EndShaftView.TerrainPipeOutDelta.Enabled = false;
				EndShaftView.PipeOutHeight.Enabled = pointSelected;
				EndShaftView.PipeOutBottomDelta.Enabled = true;
				EndShaftView.BottomHeight.Enabled = true;

				EndShaftView.MinPipeOutDelta.Enabled = minDeltaUsed;
			}
			else
			{
				EndShaftView.Diameter.Enabled = false;
				EndShaftView.InnerSlope.Enabled = false;
				EndShaftView.CoverHeight.Enabled = false;
				EndShaftView.CoverTerrainDelta.Enabled = false;
				EndShaftView.TerrainHeight.Enabled = false;
				EndShaftView.TerrainPipeOutDelta.Enabled = false;
				EndShaftView.PipeOutHeight.Enabled = true;
				EndShaftView.PipeOutBottomDelta.Enabled = false;
				EndShaftView.BottomHeight.Enabled = false;

				EndShaftView.MinPipeOutDeltaUsed.Enabled = minDeltaUsed;
			}

			ToggleMinDeltaEnd(minDeltaUsed);
		}

		public void ToggleMinDeltaStart(bool enabled)
		{
			StartShaftView.PipeOutHeight.Enabled = Manager.Data.StartPointSet;
		}

		public void ToggleMinDeltaEnd(bool enabled)
		{
			EndShaftView.PipeOutHeight.Enabled = enabled;
		}
	}

}
