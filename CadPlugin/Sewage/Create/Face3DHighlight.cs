﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Sewage.Create
{
	class Face3DHighlight
	{
		//NOTE Regular object highlight is removed when jigging.
		// So i create polyline which acts as a highlight.
		// Other option is to use lineweight (which is good because its size is fixed comparing to 
		// zoom level.
		// Maybe original 3dface can be edited (by lineweight, so we don't have to create additional object.

		/// <summary>
		/// Polyline which represent highlighted face.
		/// </summary>
		private ObjectId? createdPolyline;
		public Face3DHighlight()
		{
			createdPolyline = null;
		}
		/// <summary>
		/// Highlights object if it is 3d face.
		/// </summary>
		public void Highlight(Editor ed, Database db, ObjectId faceId)
		{
			
			createdPolyline = null;
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
				BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
				Entity ent = tr.GetObject(faceId, OpenMode.ForRead) as Entity;
				Face f = ent as Face;
				if (f == null)
					return;
				Point3d[] pts = new Point3d[4];
				for (short i = 0; i < 4; i++)
					pts[i] = f.GetVertexAt(i);
				Polyline pl = new Polyline();
				btr.AppendEntityCurrentLayer(pl);
				tr.AddNewlyCreatedDBObject(pl, true);
				pl.Closed = true;
				pl.Color = Color.FromColor(System.Drawing.Color.FromArgb(0, 0, 202, 202));

				double lineWidth = 0;
				using (ViewTableRecord acView = ed.GetCurrentView())
				{
					lineWidth = acView.Height / 100.0 / 4.0;
				}
				for (int i = 0; i < 4; i++)
				{
					Point2d pt2d = new Point2d(pts[i].X, pts[i].Y);
					pl.AddVertexAt(i, pt2d, 0, lineWidth, lineWidth);
				}
				createdPolyline = pl.ObjectId;
				tr.Commit();
			}
		}
		public void Unhighlight(Database db)
		{
			if (!createdPolyline.HasValue)
				return;
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				DBObject obj = tr.GetObject(createdPolyline.Value, OpenMode.ForWrite);
				obj.Erase();
				tr.Commit();
			}
		}
			
	}
}
