﻿using CadPlugin.PipeEntitiesJigs;
using CadPlugin.Sewage.Create.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Sewage.Create
{
	/// <summary>
	/// Creates start and end circle (if they exists) and line which represents the pipe.
	/// Can create additional entities if needed.
	/// </summary>
	class EntitiesManager
	{
		private IPipeShaftData data;
		/// <summary>
		/// All entities that are jigged (excluding end shaft circle).
		/// </summary>
		public IList<Entity> jiggingEntities = new List<Entity>();
		public Line Line;
		public Circle StartShaft;
		public Circle EndShaft;
		public Transaction Transaction { get; private set; }
		public Database Db { get; private set; }
		/// <summary>
		/// Actions which are performed when update is invoked.
		/// </summary>
		private List<Action<CalculatedData>> updateActions = new List<Action<CalculatedData>>();
		internal EntitiesManager(IPipeShaftData data, Transaction tr, Database db)
		{
			this.data = data;
			Transaction = tr;
			Db = db;
		}
		/// <summary>
		/// Creates all entities needed for jigging.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="modelSpace"></param>
		internal void InitializeJig(BlockTableRecord modelSpace, Point3d start)
		{
			// Create jigging line.
			Line l = new Line(start, start);
			modelSpace.AppendEntityCurrentLayer(l);
			Transaction.AddNewlyCreatedDBObject(l, true);
			Line = l;

			// Create start/end circle if needed.
			if (data.StartShaft != null)
			{
				ObjectId StartShaftId ;
				StartShaftId = CADCircleHelper.CreateCircle(Db, start, data.StartShaft.Radius);
				StartShaft = Transaction.GetObject(StartShaftId, OpenMode.ForWrite) as Circle;
			}
			if (data.EndShaft != null)
				EndShaft = CADCircleHelper.CreateCircle(Transaction, modelSpace, start, data.EndShaft.Radius);

			// Create text entities.
			jiggingEntities.Clear();
			
		}

		internal void Update(CalculatedData data)
		{
			foreach (var v in updateActions)
				v.Invoke(data);
		}

		/// <summary>
		/// Jigging finished - clear all up.
		/// </summary>
		/// <param name="tr"></param>
		internal void Finish()
		{
			Line.Erase();
			if (StartShaft != null)
				StartShaft.Erase();
			if (EndShaft != null)
				EndShaft.Erase();
			foreach (Entity e in jiggingEntities)
				e.Erase();
			updateActions.Clear();
		}

		/// <summary>
		/// Draws all data needed after jigging (not same entities like for jigging).
		/// </summary> 
		/// <param name="modelSpace"></param>
		internal static List<Entity> DrawEntities(Database db, Transaction tr, BlockTableRecord modelSpace, IPipeShaftData data)
		{
			
			Common.CADLayerHelper.AddLayerIfDoesntExist(db, CreatePipe.ShaftCoverLayer);
			Common.CADLayerHelper.AddLayerIfDoesntExist(db, CreatePipe.ShaftTerrainLayer);
			Common.CADLayerHelper.AddLayerIfDoesntExist(db, CreatePipe.ShaftPipeInLayer);
			Common.CADLayerHelper.AddLayerIfDoesntExist(db, CreatePipe.ShaftPipeOutLayer);
			Common.CADLayerHelper.AddLayerIfDoesntExist(db, CreatePipe.ShaftBottomLayer);
			Common.CADLayerHelper.AddLayerIfDoesntExist(db, CreatePipe.ShaftLabelLayer);

			Common.CADLayerHelper.AddLayerIfDoesntExist(db, CreatePipe.Pipe3dPolyLayer);
			Common.CADLayerHelper.AddLayerIfDoesntExist(db, CreatePipe.Pipe3dDoubleLineLayer);


			List<Entity> entities = new List<Entity>();
			Point3d startLinePoint = data.StartPoint.ToCADPoint();
			Point3d endLinePoint = data.EndPoint.ToCADPoint();
			if (data.StartShaft != null)
			{
				startLinePoint = data.Pipe.StartPoint.ToCADPoint();
				Circle pipeOutCircle = CADCircleHelper.CreateCircle(tr, modelSpace, data.StartShaft.CenterPipeOutPoint.ToCADPoint(), data.StartShaft.Radius);
				pipeOutCircle.Layer = CreatePipe.ShaftPipeOutLayer;
				entities.Add(pipeOutCircle);

				Point3d terrain = new Point3d(data.StartShaft.Center.X, data.StartShaft.Center.Y, data.StartShaft.TerrainHeight);
				DBPoint terrainPoint = new DBPoint(terrain);
				modelSpace.AppendEntityCurrentLayer(terrainPoint);
				tr.AddNewlyCreatedDBObject(terrainPoint, true);
				terrainPoint.Layer = CreatePipe.ShaftTerrainLayer;
				entities.Add(terrainPoint);

				Point3d bottom = new Point3d(data.StartShaft.Center.X, data.StartShaft.Center.Y, data.StartShaft.BottomHeight);
				Circle bottomCircle = CADCircleHelper.CreateCircle(tr, modelSpace, bottom, data.StartShaft.Radius);
				bottomCircle.Layer = CreatePipe.ShaftBottomLayer;
				entities.Add(bottomCircle);

				Point3d pipeOut = startLinePoint;
				Circle pipeOutSmallCircle = CADCircleHelper.CreateCircle(tr, modelSpace, pipeOut, 0.1);
				pipeOutSmallCircle.Layer = CreatePipe.ShaftPipeOutLayer;
				entities.Add(pipeOutSmallCircle);
			}
			if (data.EndShaft != null)
			{
				endLinePoint = data.Pipe.EndPoint.ToCADPoint();
				Circle pipeInCircle = CADCircleHelper.CreateCircle(tr, modelSpace, data.EndShaft.CenterPipeInPoint.ToCADPoint(), data.EndShaft.Radius);
				pipeInCircle.Layer = CreatePipe.ShaftPipeInLayer;
				entities.Add(pipeInCircle);

				Point3d terrain = new Point3d(data.EndShaft.Center.X, data.EndShaft.Center.Y, data.EndShaft.TerrainHeight);
				DBPoint terrainPoint = new DBPoint(terrain);
				modelSpace.AppendEntityCurrentLayer(terrainPoint); 
				tr.AddNewlyCreatedDBObject(terrainPoint, true);
				terrainPoint.Layer = CreatePipe.ShaftTerrainLayer;
				entities.Add(terrainPoint);

				Point3d bottom = new Point3d(data.EndShaft.Center.X, data.EndShaft.Center.Y, data.EndShaft.BottomHeight);
				Circle bottomCircle = CADCircleHelper.CreateCircle(tr, modelSpace, bottom, data.EndShaft.Radius);
				bottomCircle.Layer = CreatePipe.ShaftBottomLayer;
				entities.Add(bottomCircle);

				Point3d pipeIn = endLinePoint;
				Circle pipeInSmallCircle = CADCircleHelper.CreateCircle(tr, modelSpace, pipeIn, 0.1);
				pipeInSmallCircle.Layer = CreatePipe.ShaftPipeInLayer;
				entities.Add(pipeInSmallCircle);

				Point2d shaftCenter = data.EndShaft.Center.ToCADPoint();
				Point2d pipeIn2d = new Point2d(pipeIn.X, pipeIn.Y);
				Vector2d coverVector = shaftCenter.GetVectorTo(pipeIn2d);
				coverVector = coverVector.MultiplyBy(0.5);
				Point3d coverPoint = new Point3d(shaftCenter.X, shaftCenter.Y, data.EndShaft.CoverHeight).Add(new Vector3d(coverVector.X, coverVector.Y, 0));
				Circle coverCircle = CADCircleHelper.CreateCircle(tr, modelSpace, coverPoint, 0.3125 / 2);
				coverCircle.Layer = CreatePipe.ShaftCoverLayer;
				entities.Add(coverCircle);
			}
			MyUtilities.Geometry.Point3d startCenter = startLinePoint.FromCADPoint();
			MyUtilities.Geometry.Point3d endCenter = endLinePoint.FromCADPoint();
			MyUtilities.Geometry.Vector3d pipe = startCenter.GetVectorTo(endCenter);
			MyUtilities.Geometry.Vector3d ort = pipe.CrossProduct(Vector3d.ZAxis.FromCADVector());
			ort = ort.GetUnitVector();
			ort = ort.Multiply(data.Pipe.Diameter / 2);
			MyUtilities.Geometry.Point3d rightPoint = startCenter.Add(ort);
			MyUtilities.Geometry.Point3d leftPoint = startCenter.Add(ort.Negate());
			Line leftPipe = new Line(leftPoint.ToCADPoint(), leftPoint.Add(pipe).ToCADPoint());
			modelSpace.AppendEntityCurrentLayer(leftPipe);
			tr.AddNewlyCreatedDBObject(leftPipe, true);
			Line rightPipe = new Line(rightPoint.ToCADPoint(), rightPoint.Add(pipe).ToCADPoint());
			modelSpace.AppendEntityCurrentLayer(rightPipe);
			tr.AddNewlyCreatedDBObject(rightPipe, true);
			leftPipe.Layer = CreatePipe.Pipe3dDoubleLineLayer;
			rightPipe.Layer = CreatePipe.Pipe3dDoubleLineLayer;
			entities.Add(leftPipe);
			entities.Add(rightPipe);
			

			return entities;
		}
	}
}
