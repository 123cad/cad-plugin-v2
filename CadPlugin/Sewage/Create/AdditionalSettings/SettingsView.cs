﻿using CadPlugin.Common;
using CadPlugin.Sewage.Plannings;
using CadPlugin.Sewage.Plannings.Texts;
using CadPlugin.Sewage.Plannings.Texts.Pipes;
using CadPlugin.Sewage.Plannings.Texts.Shafts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.Sewage.Create.AdditionalSettings
{
	public partial class SettingsView : Form
	{
		private Dictionary<string, TextDisplaySingle> pipeDisplayData = new Dictionary<string, TextDisplaySingle>();
		private Dictionary<string, TextDisplaySingle> shaftDisplayData = new Dictionary<string, TextDisplaySingle>();
		public SettingsView()
		{
			System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
			InitializeComponent();
			InitializeData();
			dataGridViewShaftTextData.CellClick += DataGridViewShaftTextData_CellContentClick;
			dataGridViewPipeTextData.CellClick += DataGridViewPipeTextData_CellContentClick;
			dataGridViewShaftTextData.CellValueChanged += DataGridViewShaftTextData_CellValueChanged;
			dataGridViewPipeTextData.CellValueChanged += DataGridViewPipeTextData_CellValueChanged;
			FormClosing += SettingsView_FormClosing;

			dataGridViewShaftTextData.CellMouseEnter += (_, e) => { if (e.ColumnIndex == 1) System.Windows.Forms.Cursor.Current = Cursors.Hand; };
			dataGridViewPipeTextData.CellMouseEnter += (_, e) => { if (e.ColumnIndex == 1) System.Windows.Forms.Cursor.Current = Cursors.Hand; };
			dataGridViewShaftTextData.CellMouseLeave += (_, e) => { if (e.ColumnIndex == 1) System.Windows.Forms.Cursor.Current = Cursors.Arrow; };
			dataGridViewPipeTextData.CellMouseLeave += (_, e) => { if (e.ColumnIndex == 1) System.Windows.Forms.Cursor.Current = Cursors.Arrow; };

			dataGridViewPipeTextData.CurrentCellDirtyStateChanged += (_, __) => { if (dataGridViewPipeTextData.IsCurrentCellDirty) dataGridViewPipeTextData.EndEdit(); };
			dataGridViewShaftTextData.CurrentCellDirtyStateChanged += (_, __) => { if (dataGridViewShaftTextData.IsCurrentCellDirty) dataGridViewShaftTextData.EndEdit(); };

			textBoxArrowColor.Click += (_, __) =>
			  {
				  colorDialog.Color = AdditionalSettings.Instance.ArrowColor;
				  if (colorDialog.ShowDialog() == DialogResult.OK)
				  {
					  AdditionalSettings.Instance.ArrowColor = colorDialog.Color;
					  textBoxArrowColor.BackColor = colorDialog.Color;
				  }
			  };
			checkBoxShowAngle.CheckedChanged += (_, __) =>
			{
				AdditionalSettings.Instance.ShowAngle = checkBoxShowAngle.Checked;
			};
			checkBoxPipeArrow.CheckedChanged += (_, __) =>
			{
				AdditionalSettings.Instance.DrawArrowOnPipe = checkBoxPipeArrow.Checked;
			};
			checkBoxTextArrow.CheckedChanged += (_, __) =>
			{
				AdditionalSettings.Instance.DrawArrowInText = checkBoxTextArrow.Checked;
			};

			Action<DataGridView> removeCurrentRow = (dgv) =>
			{
				if (dgv.Rows.Count < 1 || dgv.CurrentCell.RowIndex < 0)
					return;
				dgv.Rows.RemoveAt(dgv.CurrentCell.RowIndex);
			};
			Action<DataGridView> rowUp = (dgv) =>
			{
				int index = dgv.CurrentCell.RowIndex;
				if (index < 1)
					return;
				DataGridViewRow row = dgv.CurrentRow;
				dgv.Rows.RemoveAt(index);
				dgv.Rows.Insert(index - 1, row);
				dgv.CurrentCell = dgv.Rows[row.Index].Cells[0];
			};
			Action<DataGridView> rowDown = (dgv) =>
			{
				int index = dgv.CurrentCell.RowIndex;
				if (index > dgv.Rows.Count - 2)
					return;
				DataGridViewRow row = dgv.CurrentRow;
				dgv.Rows.RemoveAt(index);
				dgv.Rows.Insert(index + 1, row);
				dgv.CurrentCell = dgv.Rows[row.Index].Cells[0];
			};

			DataGridViewCellPaintingEventHandler readonlyCell = (_, e) =>
			{
				if (e.ColumnIndex < 5 || 6 < e.ColumnIndex)
					return;
				if (e.RowIndex < 0)
					return;
				DataGridView dgv = _ as DataGridView;
				if (!dgv[e.ColumnIndex, e.RowIndex].ReadOnly)
					return;
				e.Graphics.FillRectangle(Brushes.LightGray, e.CellBounds);
				e.Graphics.DrawRectangle(new Pen(Brushes.LightSlateGray, 1), e.CellBounds);
				e.Handled = true;
			};

			dataGridViewShaftTextData.CellPainting += readonlyCell;
			dataGridViewPipeTextData.CellPainting += readonlyCell;


			buttonShaftAdd.Click += (_, __) => { dataGridViewShaftTextData.Rows.Add(); };
			buttonShaftRemove.Click += (_, __) => { removeCurrentRow(dataGridViewShaftTextData); };
			buttonShaftUp.Click += (_, __) => { rowUp(dataGridViewShaftTextData); };
			buttonShaftDown.Click += (_, __) => { rowDown(dataGridViewShaftTextData); };
			buttonPipeAdd.Click += (_, __) => { dataGridViewPipeTextData.Rows.Add(); };
			buttonPipeRemove.Click += (_, __) => { removeCurrentRow(dataGridViewPipeTextData); };
			buttonPipeUp.Click += (_, __) => { rowUp(dataGridViewPipeTextData); };
			buttonPipeDown.Click += (_, __) => { rowDown(dataGridViewPipeTextData); };
			
		}

		private void DataGridViewPipeTextData_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex < 0)
				return;
			DataGridView dgv = (DataGridView)sender;
			DataGridView_CellValueChanged(dgv, (x) => GetPipeTextData(x), e.RowIndex, e.ColumnIndex);
		}

		private void DataGridViewShaftTextData_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex < 0)
				return;
			DataGridView dgv = (DataGridView)sender;
			DataGridView_CellValueChanged(dgv, (x) => GetShaftTextData(x), e.RowIndex, e.ColumnIndex);
		}
		private void DataGridView_CellValueChanged(DataGridView dgv, Func<string, TextDisplaySingle> f, int row, int column)
		{
			if (row < 0)
				return;
			string name = dgv[0, row].Value as string;
			TextDisplaySingle data = f(name);
			if (data == null)
				return;
			if (column == 0)
				SetRowData(dgv, data, row);
			if (column == 1)
				data.Description = (string)dgv[column, row].Value;
			if (column == 2)
				data.Suffix = (string)dgv[column, row].Value;
			if (column == 4)
			{
				string s = dgv[column, row].Value as string;
				double d;
				if (DoubleHelper.TryParseInvariant(s, out d))
					data.TextSize = d;
			}
			if (column == 5)
			{
				if (!dgv[column, row].ReadOnly)
					((ITextDecimals)data).Digits = (int)dgv[column, row].Value;
			}
			if (column == 6)
			{
				if (!dgv[column, row].ReadOnly)
					((IDisplayScaled)data).Scale = (DisplayScale)dgv[column, row].Value;
			}
			if (column == 7)
			{
				data.DrawInJig = (bool)dgv[column, row].Value;
			}
			if (column == 8)
			{
				data.DrawAsFinal = (bool)dgv[column, row].Value;
			}
		}
		private void SetRowData(DataGridView dgv, TextDisplaySingle data, int row)
		{
			dgv[0, row].Value = data.Name;
			dgv[1, row].Value = data.Description;
			dgv[2, row].Value = data.Suffix;
			dgv[3, row].Style.BackColor = data.TextColor;
			dgv[4, row].Value = DoubleHelper.ToStringInvariant(data.TextSize, 2);
			if (data is ITextDecimals)
				dgv[5, row].Value = ((ITextDecimals)data).Digits;
			else
				dgv[5, row].ReadOnly = true;
			if (data is IDisplayScaled)
				dgv[6, row].Value = ((IDisplayScaled)data).Scale;
			else
				dgv[6, row].ReadOnly = true; 

			dgv[7, row].Value = data.DrawInJig;
			dgv[8, row].Value = data.DrawAsFinal;
		}

		private void DataGridViewPipeTextData_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.ColumnIndex < 0 || e.RowIndex < 0)
				return;
			DataGridView dgv = (DataGridView)sender;
			TextDisplaySingle data = GetPipeTextData(dgv[0, e.RowIndex].Value as string);
			if (data == null)
				return;
			if (e.ColumnIndex == 3)
				SelectColor(dgv, e.RowIndex, e.ColumnIndex, data);
		}

		private void DataGridViewShaftTextData_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.ColumnIndex < 0 || e.RowIndex < 0)
				return;
			DataGridView dgv = (DataGridView)sender;
			TextDisplaySingle data = GetShaftTextData(dgv[0, e.RowIndex].Value as string);
			if (data == null)
				return;
			if (e.ColumnIndex == 3)
				SelectColor(dgv, e.RowIndex, e.ColumnIndex, data);
		}
		private void SelectColor(DataGridView dgv, int row, int column, TextDisplaySingle data)
		{
			//if (dgv.Columns[column] is DataGridViewButtonColumn)
			{
				colorDialog.Color = data.TextColor;
				if (colorDialog.ShowDialog() == DialogResult.OK)
				{
					Color selectedColor = colorDialog.Color;
					dgv[column, row].Style.BackColor = selectedColor;
					dgv[column, row].Style.ForeColor = selectedColor;
					dgv[column, row].Style.SelectionBackColor = selectedColor;
					dgv[column, row].Style.SelectionForeColor = selectedColor;
					data.TextColor = colorDialog.Color;
				}
			}
		}

		private TextDisplaySingle GetShaftTextData(string name)
		{
			if (string.IsNullOrEmpty(name) || !shaftDisplayData.ContainsKey(name))
				return null;
			return shaftDisplayData[name];
		}
		private TextDisplaySingle GetPipeTextData(string name)
		{
			if (string.IsNullOrEmpty(name) || !pipeDisplayData.ContainsKey(name))
				return null;
			return pipeDisplayData[name];
		}

		private void SettingsView_FormClosing(object sender, FormClosingEventArgs e)
		{
			AdditionalSettings.Instance.TextOrder.ShaftDisplayOrder.Clear();
			AdditionalSettings.Instance.TextOrder.PipeDisplayOrder.Clear();
			foreach (DataGridViewRow row in this.dataGridViewShaftTextData.Rows)
			{
				if (row.Cells.Count < 2)
					continue;
				string s = row.Cells[0].Value as string;
				if (s == null || !shaftDisplayData.ContainsKey(s))
					continue;
				AdditionalSettings.Instance.TextOrder.ShaftDisplayOrder.Add(shaftDisplayData[s]);
			}
			foreach (DataGridViewRow row in this.dataGridViewPipeTextData.Rows)
			{
				if (row.Cells.Count < 2)
					continue;
				string s = row.Cells[0].Value as string;
				if (s == null || !pipeDisplayData.ContainsKey(s))
					continue;
				AdditionalSettings.Instance.TextOrder.PipeDisplayOrder.Add(pipeDisplayData[s]);
			}
		}

		private void InitializeData()
		{
			//AdditionalSettings.Instance.TextOrder.PipeDisplayOrder

			foreach (TextDisplaySingle display in TextFactory.Instance.GetAllPipes())
			{
				pipeDisplayData.Add(display.Name, display);
			}
			foreach (TextDisplaySingle display in TextFactory.Instance.GetAllShafts())
			{
				shaftDisplayData.Add(display.Name, display);
			}

			List<KeyValuePair<string, DisplayScale>> scale = new List<KeyValuePair<string, DisplayScale>>();
			scale.Add(new KeyValuePair<string, DisplayScale>("%", DisplayScale.Percent));
			scale.Add(new KeyValuePair<string, DisplayScale>("‰", DisplayScale.Permille));
			dataGridViewPipeTypeScaleColumn.DataSource = scale;
			dataGridViewShaftTypeScaleColumn.DataSource = scale.ToList();
			dataGridViewPipeTypeScaleColumn.DisplayMember = "Key";
			dataGridViewShaftTypeScaleColumn.DisplayMember = "Key";
			dataGridViewPipeTypeScaleColumn.ValueMember = "Value";
			dataGridViewShaftTypeScaleColumn.ValueMember = "Value";

			List<KeyValuePair<string, int>> digit = new List<KeyValuePair<string, int>>();
			for (int i = 0; i < 5; i++)
			{
				digit.Add(new KeyValuePair<string, int>(i.ToString(), i));
			}
			dataGridViewPipeTypeDecimalsColumn.DataSource = digit;
			dataGridViewShaftTypeDecimalsColumn.DataSource = digit;
			dataGridViewPipeTypeDecimalsColumn.DisplayMember = "Key";
			dataGridViewShaftTypeDecimalsColumn.DisplayMember = "Key";
			dataGridViewPipeTypeDecimalsColumn.ValueMember = "Value";
			dataGridViewShaftTypeDecimalsColumn.ValueMember = "Value";
			

			

			foreach (var v in AdditionalSettings.Instance.TextOrder.ShaftDisplayOrder)
				if (shaftDisplayData.ContainsKey(v.Name))
					shaftDisplayData[v.Name] = v;
			foreach (var v in AdditionalSettings.Instance.TextOrder.PipeDisplayOrder)
				if (pipeDisplayData.ContainsKey(v.Name))
					pipeDisplayData[v.Name] = v;


			foreach (var v in shaftDisplayData)
				this.dataGridViewShaftTypeColumn.Items.Add(v.Key);
			foreach (var v in pipeDisplayData)
				this.dataGridViewPipeTypeColumn.Items.Add(v.Key);
			


			if (AdditionalSettings.Instance.TextOrder.ShaftDisplayOrder.Count > 0)
				foreach (TextDisplaySingle single in AdditionalSettings.Instance.TextOrder.ShaftDisplayOrder)
				{
					int row = dataGridViewShaftTextData.Rows.Add();
					SetRowData(dataGridViewShaftTextData, single, row);
				}
			if (AdditionalSettings.Instance.TextOrder.PipeDisplayOrder.Count > 0)
				foreach (TextDisplaySingle single in AdditionalSettings.Instance.TextOrder.PipeDisplayOrder)
				{
					int row = dataGridViewPipeTextData.Rows.Add();
					SetRowData(dataGridViewPipeTextData, single, row);
				}
			textBoxArrowColor.BackColor = AdditionalSettings.Instance.ArrowColor;
			checkBoxPipeArrow.Checked = AdditionalSettings.Instance.DrawArrowOnPipe;
			checkBoxTextArrow.Checked = AdditionalSettings.Instance.DrawArrowInText;
			checkBoxShowAngle.Checked = AdditionalSettings.Instance.ShowAngle;

			Action<DataGridView, int> textColumnValidation = (dgv, column) =>
			{

				dgv.EditingControlShowing += (_, __) =>
				{
					if (dgv.CurrentCell.ColumnIndex != column)
						return;
					__.Control.TextChanged += (_1, __1) =>
					{
						string val = __.Control.Text;
						double d;
						toolTip.Hide(__.Control);
						dgv.CurrentRow.ErrorText = null;
						//dgv.CurrentCell.ErrorText = "";
						if (!DoubleHelper.TryParseInvariant(val, out d))
						{
							dgv.CurrentRow.ErrorText = "Text size is invalid!";
							//dgv.CurrentCell.ErrorText = "Text size is invalid!";
						}
						__.Control.Text = string.Concat(val.Where(x => x != ',').ToArray());
						if (__.Control is TextBox)
							((TextBox)__.Control).SelectionStart = __.Control.Text.Length;
					};
				};
				dgv.CellEndEdit += (_, __) =>
				{
					if (__.ColumnIndex != column)
						return;
					DataGridViewCell cell = dgv[__.ColumnIndex, __.RowIndex];
					string val = cell.Value as string;
					double d;
					if (!DoubleHelper.TryParseInvariant(val, out d))
					{
						cell.Value = DoubleHelper.ToStringInvariant(0.3, 2);
						dgv.CurrentRow.ErrorText = "";
						//dgv.CurrentCell.ErrorText = "erropr";
					}
				};
			};
			textColumnValidation.Invoke(dataGridViewPipeTextData, dataGridViewPipeSizeColumn.Index);
			textColumnValidation.Invoke(dataGridViewShaftTextData, dataGridViewShaftSizeColumn.Index);

		}


		private void buttonOk_Click(object sender, EventArgs e)
		{
			Close();

		}
		private bool CheckData()
		{
			ErrorProvider error = new ErrorProvider();
			bool valid = true;
			foreach (DataGridViewRow row in dataGridViewPipeTextData.Rows)
			{
				DataGridViewCell cell = row.Cells[dataGridViewPipeSizeColumn.Index];
				string textSize = cell.Value as string;
				double d;
				if (!DoubleHelper.TryParseInvariant(textSize, out d))
				{
					valid = false;
					cell.ErrorText = "Invalid ";
					break;
				}
			}
			return valid;
		}
	}
}
