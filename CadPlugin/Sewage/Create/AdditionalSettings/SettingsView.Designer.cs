﻿namespace CadPlugin.Sewage.Create.AdditionalSettings
{
	partial class SettingsView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.dataGridViewShaftTextData = new System.Windows.Forms.DataGridView();
			this.dataGridViewShaftTypeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.ColumnShaftText = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewShaftColorColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewShaftSizeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewPipeTypeDecimalsColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.dataGridViewPipeTypeScaleColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.buttonOk = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label2 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.colorDialog = new System.Windows.Forms.ColorDialog();
			this.dataGridViewPipeTextData = new System.Windows.Forms.DataGridView();
			this.dataGridViewPipeTypeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.ColumnPipeText = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewPipeColorColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewPipeSizeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewShaftTypeDecimalsColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.dataGridViewShaftTypeScaleColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.Column4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.buttonShaftAdd = new System.Windows.Forms.Button();
			this.buttonShaftRemove = new System.Windows.Forms.Button();
			this.buttonShaftUp = new System.Windows.Forms.Button();
			this.buttonShaftDown = new System.Windows.Forms.Button();
			this.buttonPipeDown = new System.Windows.Forms.Button();
			this.buttonPipeUp = new System.Windows.Forms.Button();
			this.buttonPipeRemove = new System.Windows.Forms.Button();
			this.buttonPipeAdd = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.textBoxArrowColor = new System.Windows.Forms.TextBox();
			this.checkBoxShowAngle = new System.Windows.Forms.CheckBox();
			this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this.toolTip = new System.Windows.Forms.ToolTip(this.components);
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.checkBoxPipeArrow = new System.Windows.Forms.CheckBox();
			this.checkBoxTextArrow = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewShaftTextData)).BeginInit();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewPipeTextData)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// dataGridViewShaftTextData
			// 
			this.dataGridViewShaftTextData.AllowUserToAddRows = false;
			this.dataGridViewShaftTextData.AllowUserToDeleteRows = false;
			this.dataGridViewShaftTextData.AllowUserToResizeRows = false;
			this.dataGridViewShaftTextData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dataGridViewShaftTextData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewShaftTextData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewShaftTypeColumn,
            this.ColumnShaftText,
            this.Column5,
            this.dataGridViewShaftColorColumn,
            this.dataGridViewShaftSizeColumn,
            this.dataGridViewPipeTypeDecimalsColumn,
            this.dataGridViewPipeTypeScaleColumn,
            this.Column1,
            this.Column2});
			this.dataGridViewShaftTextData.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
			this.dataGridViewShaftTextData.Location = new System.Drawing.Point(3, 0);
			this.dataGridViewShaftTextData.MultiSelect = false;
			this.dataGridViewShaftTextData.Name = "dataGridViewShaftTextData";
			this.dataGridViewShaftTextData.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dataGridViewShaftTextData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.dataGridViewShaftTextData.ShowEditingIcon = false;
			this.dataGridViewShaftTextData.Size = new System.Drawing.Size(640, 119);
			this.dataGridViewShaftTextData.TabIndex = 0;
			// 
			// dataGridViewShaftTypeColumn
			// 
			this.dataGridViewShaftTypeColumn.HeaderText = "Shaft Data";
			this.dataGridViewShaftTypeColumn.Name = "dataGridViewShaftTypeColumn";
			this.dataGridViewShaftTypeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewShaftTypeColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.dataGridViewShaftTypeColumn.Width = 90;
			// 
			// ColumnShaftText
			// 
			this.ColumnShaftText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.ColumnShaftText.HeaderText = "Text";
			this.ColumnShaftText.Name = "ColumnShaftText";
			// 
			// Column5
			// 
			this.Column5.HeaderText = "Suffix";
			this.Column5.Name = "Column5";
			this.Column5.Width = 50;
			// 
			// dataGridViewShaftColorColumn
			// 
			this.dataGridViewShaftColorColumn.HeaderText = "Color";
			this.dataGridViewShaftColorColumn.Name = "dataGridViewShaftColorColumn";
			this.dataGridViewShaftColorColumn.ReadOnly = true;
			this.dataGridViewShaftColorColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewShaftColorColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.dataGridViewShaftColorColumn.Width = 50;
			// 
			// dataGridViewShaftSizeColumn
			// 
			this.dataGridViewShaftSizeColumn.HeaderText = "Size";
			this.dataGridViewShaftSizeColumn.Name = "dataGridViewShaftSizeColumn";
			this.dataGridViewShaftSizeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewShaftSizeColumn.Width = 50;
			// 
			// dataGridViewPipeTypeDecimalsColumn
			// 
			this.dataGridViewPipeTypeDecimalsColumn.HeaderText = "Decimals";
			this.dataGridViewPipeTypeDecimalsColumn.Name = "dataGridViewPipeTypeDecimalsColumn";
			this.dataGridViewPipeTypeDecimalsColumn.Width = 55;
			// 
			// dataGridViewPipeTypeScaleColumn
			// 
			this.dataGridViewPipeTypeScaleColumn.HeaderText = "Scale";
			this.dataGridViewPipeTypeScaleColumn.Name = "dataGridViewPipeTypeScaleColumn";
			this.dataGridViewPipeTypeScaleColumn.Visible = false;
			this.dataGridViewPipeTypeScaleColumn.Width = 50;
			// 
			// Column1
			// 
			this.Column1.HeaderText = "Preview";
			this.Column1.Name = "Column1";
			this.Column1.Width = 50;
			// 
			// Column2
			// 
			this.Column2.HeaderText = "Draw";
			this.Column2.Name = "Column2";
			this.Column2.Width = 40;
			// 
			// buttonOk
			// 
			this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonOk.Location = new System.Drawing.Point(580, 437);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(75, 23);
			this.buttonOk.TabIndex = 1;
			this.buttonOk.Text = "Ok";
			this.buttonOk.UseVisualStyleBackColor = true;
			this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.textBox2);
			this.groupBox1.Controls.Add(this.textBox1);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(26, 359);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(281, 67);
			this.groupBox1.TabIndex = 2;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Naming";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(135, 27);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(70, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Start number:";
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(211, 24);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(44, 20);
			this.textBox2.TabIndex = 2;
			this.textBox2.Text = "0";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(54, 24);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(60, 20);
			this.textBox1.TabIndex = 1;
			this.textBox1.Text = "Shaft";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(15, 27);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(33, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Prefix";
			// 
			// dataGridViewPipeTextData
			// 
			this.dataGridViewPipeTextData.AllowUserToAddRows = false;
			this.dataGridViewPipeTextData.AllowUserToDeleteRows = false;
			this.dataGridViewPipeTextData.AllowUserToResizeRows = false;
			this.dataGridViewPipeTextData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dataGridViewPipeTextData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewPipeTextData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewPipeTypeColumn,
            this.ColumnPipeText,
            this.Column6,
            this.dataGridViewPipeColorColumn,
            this.dataGridViewPipeSizeColumn,
            this.dataGridViewShaftTypeDecimalsColumn,
            this.dataGridViewShaftTypeScaleColumn,
            this.Column3,
            this.Column4});
			this.dataGridViewPipeTextData.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
			this.dataGridViewPipeTextData.Location = new System.Drawing.Point(3, 3);
			this.dataGridViewPipeTextData.Name = "dataGridViewPipeTextData";
			this.dataGridViewPipeTextData.ShowEditingIcon = false;
			this.dataGridViewPipeTextData.Size = new System.Drawing.Size(637, 117);
			this.dataGridViewPipeTextData.TabIndex = 4;
			// 
			// dataGridViewPipeTypeColumn
			// 
			this.dataGridViewPipeTypeColumn.HeaderText = "Pipe Data";
			this.dataGridViewPipeTypeColumn.Name = "dataGridViewPipeTypeColumn";
			this.dataGridViewPipeTypeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewPipeTypeColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.dataGridViewPipeTypeColumn.Width = 90;
			// 
			// ColumnPipeText
			// 
			this.ColumnPipeText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.ColumnPipeText.HeaderText = "Text";
			this.ColumnPipeText.Name = "ColumnPipeText";
			// 
			// Column6
			// 
			this.Column6.HeaderText = "Suffix";
			this.Column6.Name = "Column6";
			// 
			// dataGridViewPipeColorColumn
			// 
			dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Transparent;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Transparent;
			this.dataGridViewPipeColorColumn.DefaultCellStyle = dataGridViewCellStyle1;
			this.dataGridViewPipeColorColumn.HeaderText = "Color";
			this.dataGridViewPipeColorColumn.Name = "dataGridViewPipeColorColumn";
			this.dataGridViewPipeColorColumn.ReadOnly = true;
			this.dataGridViewPipeColorColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewPipeColorColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.dataGridViewPipeColorColumn.Width = 50;
			// 
			// dataGridViewPipeSizeColumn
			// 
			this.dataGridViewPipeSizeColumn.HeaderText = "Size";
			this.dataGridViewPipeSizeColumn.Name = "dataGridViewPipeSizeColumn";
			this.dataGridViewPipeSizeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewPipeSizeColumn.Width = 50;
			// 
			// dataGridViewShaftTypeDecimalsColumn
			// 
			this.dataGridViewShaftTypeDecimalsColumn.HeaderText = "Decimals";
			this.dataGridViewShaftTypeDecimalsColumn.Name = "dataGridViewShaftTypeDecimalsColumn";
			this.dataGridViewShaftTypeDecimalsColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewShaftTypeDecimalsColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.dataGridViewShaftTypeDecimalsColumn.Width = 55;
			// 
			// dataGridViewShaftTypeScaleColumn
			// 
			this.dataGridViewShaftTypeScaleColumn.HeaderText = "Scale";
			this.dataGridViewShaftTypeScaleColumn.Name = "dataGridViewShaftTypeScaleColumn";
			this.dataGridViewShaftTypeScaleColumn.Width = 50;
			// 
			// Column3
			// 
			this.Column3.HeaderText = "Preview";
			this.Column3.Name = "Column3";
			this.Column3.Width = 50;
			// 
			// Column4
			// 
			this.Column4.HeaderText = "Draw";
			this.Column4.Name = "Column4";
			this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.Column4.Width = 40;
			// 
			// buttonShaftAdd
			// 
			this.buttonShaftAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonShaftAdd.Location = new System.Drawing.Point(581, 125);
			this.buttonShaftAdd.Name = "buttonShaftAdd";
			this.buttonShaftAdd.Size = new System.Drawing.Size(28, 23);
			this.buttonShaftAdd.TabIndex = 5;
			this.buttonShaftAdd.Text = "+";
			this.buttonShaftAdd.UseVisualStyleBackColor = true;
			// 
			// buttonShaftRemove
			// 
			this.buttonShaftRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonShaftRemove.Location = new System.Drawing.Point(612, 125);
			this.buttonShaftRemove.Name = "buttonShaftRemove";
			this.buttonShaftRemove.Size = new System.Drawing.Size(28, 23);
			this.buttonShaftRemove.TabIndex = 6;
			this.buttonShaftRemove.Text = "-";
			this.buttonShaftRemove.UseVisualStyleBackColor = true;
			// 
			// buttonShaftUp
			// 
			this.buttonShaftUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonShaftUp.Location = new System.Drawing.Point(3, 125);
			this.buttonShaftUp.Name = "buttonShaftUp";
			this.buttonShaftUp.Size = new System.Drawing.Size(28, 23);
			this.buttonShaftUp.TabIndex = 7;
			this.buttonShaftUp.Text = "/\\";
			this.buttonShaftUp.UseVisualStyleBackColor = true;
			// 
			// buttonShaftDown
			// 
			this.buttonShaftDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonShaftDown.Location = new System.Drawing.Point(37, 125);
			this.buttonShaftDown.Name = "buttonShaftDown";
			this.buttonShaftDown.Size = new System.Drawing.Size(28, 23);
			this.buttonShaftDown.TabIndex = 8;
			this.buttonShaftDown.Text = "\\/";
			this.buttonShaftDown.UseVisualStyleBackColor = true;
			// 
			// buttonPipeDown
			// 
			this.buttonPipeDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonPipeDown.Location = new System.Drawing.Point(37, 126);
			this.buttonPipeDown.Name = "buttonPipeDown";
			this.buttonPipeDown.Size = new System.Drawing.Size(28, 23);
			this.buttonPipeDown.TabIndex = 12;
			this.buttonPipeDown.Text = "\\/";
			this.buttonPipeDown.UseVisualStyleBackColor = true;
			// 
			// buttonPipeUp
			// 
			this.buttonPipeUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonPipeUp.Location = new System.Drawing.Point(3, 126);
			this.buttonPipeUp.Name = "buttonPipeUp";
			this.buttonPipeUp.Size = new System.Drawing.Size(28, 23);
			this.buttonPipeUp.TabIndex = 11;
			this.buttonPipeUp.Text = "/\\";
			this.buttonPipeUp.UseVisualStyleBackColor = true;
			// 
			// buttonPipeRemove
			// 
			this.buttonPipeRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonPipeRemove.Location = new System.Drawing.Point(612, 126);
			this.buttonPipeRemove.Name = "buttonPipeRemove";
			this.buttonPipeRemove.Size = new System.Drawing.Size(28, 23);
			this.buttonPipeRemove.TabIndex = 10;
			this.buttonPipeRemove.Text = "-";
			this.buttonPipeRemove.UseVisualStyleBackColor = true;
			// 
			// buttonPipeAdd
			// 
			this.buttonPipeAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonPipeAdd.Location = new System.Drawing.Point(581, 126);
			this.buttonPipeAdd.Name = "buttonPipeAdd";
			this.buttonPipeAdd.Size = new System.Drawing.Size(28, 23);
			this.buttonPipeAdd.TabIndex = 9;
			this.buttonPipeAdd.Text = "+";
			this.buttonPipeAdd.UseVisualStyleBackColor = true;
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(382, 362);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(63, 13);
			this.label3.TabIndex = 13;
			this.label3.Text = "Arrow color:";
			// 
			// textBoxArrowColor
			// 
			this.textBoxArrowColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.textBoxArrowColor.Cursor = System.Windows.Forms.Cursors.Hand;
			this.textBoxArrowColor.Location = new System.Drawing.Point(451, 359);
			this.textBoxArrowColor.Name = "textBoxArrowColor";
			this.textBoxArrowColor.ReadOnly = true;
			this.textBoxArrowColor.Size = new System.Drawing.Size(34, 20);
			this.textBoxArrowColor.TabIndex = 14;
			// 
			// checkBoxShowAngle
			// 
			this.checkBoxShowAngle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.checkBoxShowAngle.AutoSize = true;
			this.checkBoxShowAngle.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBoxShowAngle.Checked = true;
			this.checkBoxShowAngle.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxShowAngle.Location = new System.Drawing.Point(382, 389);
			this.checkBoxShowAngle.Name = "checkBoxShowAngle";
			this.checkBoxShowAngle.Size = new System.Drawing.Size(82, 17);
			this.checkBoxShowAngle.TabIndex = 15;
			this.checkBoxShowAngle.Text = "Show angle";
			this.checkBoxShowAngle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.checkBoxShowAngle.UseVisualStyleBackColor = true;
			// 
			// errorProvider
			// 
			this.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this.errorProvider.ContainerControl = this;
			// 
			// splitContainer1
			// 
			this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.splitContainer1.Location = new System.Drawing.Point(12, 20);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.dataGridViewShaftTextData);
			this.splitContainer1.Panel1.Controls.Add(this.buttonShaftUp);
			this.splitContainer1.Panel1.Controls.Add(this.buttonShaftDown);
			this.splitContainer1.Panel1.Controls.Add(this.buttonShaftRemove);
			this.splitContainer1.Panel1.Controls.Add(this.buttonShaftAdd);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.dataGridViewPipeTextData);
			this.splitContainer1.Panel2.Controls.Add(this.buttonPipeAdd);
			this.splitContainer1.Panel2.Controls.Add(this.buttonPipeRemove);
			this.splitContainer1.Panel2.Controls.Add(this.buttonPipeDown);
			this.splitContainer1.Panel2.Controls.Add(this.buttonPipeUp);
			this.splitContainer1.Size = new System.Drawing.Size(643, 311);
			this.splitContainer1.SplitterDistance = 155;
			this.splitContainer1.TabIndex = 16;
			// 
			// checkBoxPipeArrow
			// 
			this.checkBoxPipeArrow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.checkBoxPipeArrow.AutoSize = true;
			this.checkBoxPipeArrow.Checked = true;
			this.checkBoxPipeArrow.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxPipeArrow.Location = new System.Drawing.Point(529, 356);
			this.checkBoxPipeArrow.Name = "checkBoxPipeArrow";
			this.checkBoxPipeArrow.Size = new System.Drawing.Size(92, 17);
			this.checkBoxPipeArrow.TabIndex = 17;
			this.checkBoxPipeArrow.Text = "On pipe arrow";
			this.toolTip.SetToolTip(this.checkBoxPipeArrow, "Arrow which is displayed inside pipe diameter");
			this.checkBoxPipeArrow.UseVisualStyleBackColor = true;
			// 
			// checkBoxTextArrow
			// 
			this.checkBoxTextArrow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.checkBoxTextArrow.AutoSize = true;
			this.checkBoxTextArrow.Checked = true;
			this.checkBoxTextArrow.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxTextArrow.Location = new System.Drawing.Point(529, 379);
			this.checkBoxTextArrow.Name = "checkBoxTextArrow";
			this.checkBoxTextArrow.Size = new System.Drawing.Size(84, 17);
			this.checkBoxTextArrow.TabIndex = 18;
			this.checkBoxTextArrow.Text = "In text arrow";
			this.toolTip.SetToolTip(this.checkBoxTextArrow, "Arrow which is displayed in text size.");
			this.checkBoxTextArrow.UseVisualStyleBackColor = true;
			// 
			// SettingsView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(667, 472);
			this.ControlBox = false;
			this.Controls.Add(this.checkBoxTextArrow);
			this.Controls.Add(this.checkBoxPipeArrow);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.checkBoxShowAngle);
			this.Controls.Add(this.textBoxArrowColor);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.buttonOk);
			this.MinimumSize = new System.Drawing.Size(644, 38);
			this.Name = "SettingsView";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Settings";
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewShaftTextData)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewPipeTextData)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridViewShaftTextData;
		private System.Windows.Forms.Button buttonOk;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ColorDialog colorDialog;
		private System.Windows.Forms.DataGridView dataGridViewPipeTextData;
		private System.Windows.Forms.Button buttonShaftAdd;
		private System.Windows.Forms.Button buttonShaftRemove;
		private System.Windows.Forms.Button buttonShaftUp;
		private System.Windows.Forms.Button buttonShaftDown;
		private System.Windows.Forms.Button buttonPipeDown;
		private System.Windows.Forms.Button buttonPipeUp;
		private System.Windows.Forms.Button buttonPipeRemove;
		private System.Windows.Forms.Button buttonPipeAdd;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textBoxArrowColor;
		private System.Windows.Forms.CheckBox checkBoxShowAngle;
		private System.Windows.Forms.ErrorProvider errorProvider;
		private System.Windows.Forms.ToolTip toolTip;
		private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewShaftTypeColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnShaftText;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewShaftColorColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewShaftSizeColumn;
		private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewPipeTypeDecimalsColumn;
		private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewPipeTypeScaleColumn;
		private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
		private System.Windows.Forms.DataGridViewCheckBoxColumn Column2;
		private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewPipeTypeColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPipeText;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewPipeColorColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewPipeSizeColumn;
		private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewShaftTypeDecimalsColumn;
		private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewShaftTypeScaleColumn;
		private System.Windows.Forms.DataGridViewCheckBoxColumn Column3;
		private System.Windows.Forms.DataGridViewCheckBoxColumn Column4;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.CheckBox checkBoxTextArrow;
		private System.Windows.Forms.CheckBox checkBoxPipeArrow;
	}
}