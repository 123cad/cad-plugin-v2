﻿using CadPlugin.Sewage.Create.ShaftControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.Sewage.Create
{
	internal partial class PipeShaftView : Form, IPipeShaftView
	{
		public PipeShaftView()
		{
			InitializeComponent();
			CloseReason = CloseReason.Cancel;

			/*KeyDown += (e, ee) =>
			{
				if (ee.KeyCode == Keys.Escape)
				{
					CloseReason = CloseReason.Cancel;
					Close();
				}
			};*/
			MaximumSize = MinimumSize = Size;
			SetGermanLanguage();
		}

		private void SetGermanLanguage()
		{
			labelSlope.Text = "Neigung:";
			checkBoxMinPipeSlope.Text = "Min.-Neigung:";
			labelDiameter.Text = "Durchmesser:";
			labelSegmentLength.Text = "Segment:";
			labelStartEndSegmentLength.Text = "Start/End Segment:";
			labelAngleStep.Text = "Winkel-Segment:";
			labelPipeAngleText.Text = "Winkel:";
			labelHeightDelta.Text = "Höhen-Differenz:";

			Text = "Abwassernetz zeichnen";

			StartShaft.Description = "Start-Schacht";
			endShaftView.Description = "End-Schacht";


		}


		public CloseReason CloseReason { get; private set; }


		public IShaftControlView StartShaft { get { return startShaftView; } }
		public IShaftControlView EndShaft { get { return endShaftView; } }
		public Totals.Totals Totals { get { return totalsView; } }


		public TextBox MinPipeSlope { get { return textBoxMinPipeSlope; } }
		public Label PipeSlope { get { return labelPipeSlope; } }

		public TextBox PipeStartEndSegmentLength { get { return textBoxPipeStartEndSegment; } }

		public TextBox PipeSegmentLength { get { return textBoxPipeSegmentLength; } }
		public TextBox StepAngle { get { return textBoxAngleStep; } }

		public TextBox PipeDiameter { get { return textBoxPipeDiameter; } }
		public Button DrawRequest { get { return buttonDraw; } }
		public Button SelectPoint { get { return buttonSelectEndPoint; } }
		public Panel DrawRequestPanel { get { return panelDrawRequest; } }


		public CheckBox MinPipeSlopeUsed { get { return checkBoxMinPipeSlope; } }
		public CheckBox MinStartShaftDeltaUsed { get { return StartShaft.MinPipeOutDeltaUsed; } }
		public TextBox MinStartShaftDelta { get { return StartShaft.MinPipeOutDelta; } }
		public CheckBox MinEndShaftDeltaUsed { get { return EndShaft.MinPipeOutDeltaUsed; } }
		public TextBox MinEndShaftDelta { get { return EndShaft.MinPipeOutDelta; } }

		public CheckBox SegmentsUsed { get { return checkBoxSegmentsUsed; } }
		public CheckBox StepAngleUsed { get { return checkBoxAngleStepUsed; } }

		public PictureBox WaterDirectionPicture { get { return pictureBoxWaterDirection; } }

		public string PipeAngle
		{
			set
			{
				labelPipeAngle.Text = value;
			}
		}
		public string PipeLength
		{
			set
			{
				labelPipeLength.Text = value;
			}
		}
		public string PipeLength2d
		{
			set
			{
				labelPipeLength2d.Text = value;
			}
		}

		public string SegmentCount
		{
			set
			{
				labelPipeSegments.Text = value;
			}
		}

		public string PipeHeightDelta
		{
			set
			{
				labelPipeHeightDelta.Text = value;
			}
		}

		private void buttonDraw_Click(object sender, EventArgs e)
		{
			CloseReason = CloseReason.Draw;
			Close();
		}

		private void buttonFinish_Click(object sender, EventArgs e)
		{
			CloseReason = CloseReason.Finish;
			Close();
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			CloseReason = CloseReason.Cancel;
			Close();
		}

		private void buttonSelectEndPoint_Click(object sender, EventArgs e)
		{
			CloseReason = CloseReason.SelectEndPoint;
			Close();
		}
		public event Action OpenSettings;
		private void OnOpenSettings()
		{
			if (OpenSettings != null)
				OpenSettings.Invoke();
		}

		private void buttonSettings_Click(object sender, EventArgs e)
		{
			OnOpenSettings();
		}
	}
}
