﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.Sewage.Create.Totals
{
	public partial class Totals : UserControl
	{
		public Totals()
		{
			InitializeComponent();
		}
		public string PipesCount { set { labelPipesCreated.Text = value; } }
		public string PipesLength { set { labelPipeLength.Text = value; } }
		public string PipesSegments { set { labelSegmentsCount.Text = value; } }
		public string PipesStartEndSegments { set { labelStartEndSegment.Text = value; } }
		public string ShaftsCount { set { labelShaftsCount.Text = value; } }
	}
}
