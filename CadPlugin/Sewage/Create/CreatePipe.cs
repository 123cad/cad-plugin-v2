﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.PipeEntitiesJigs;
using CadPlugin.Sewage.Create.Settings;
using System.Diagnostics;
using System.Threading;
using CadPlugin.Sewage.Plannings;
using CadPlugin.Sewage.Planning;
using CadPlugin.Common;
using CadPlugin.Sewage.Planning.Calculations.SegmentDecoraters;
using CadPlugin.Sewage.Planning.Calculations.OffsetCalculations;
using CadPlugin.KHNeu.Views.KHNeuModels;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Windows;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Sewage.Create
{
    public enum WaterDirection
	{
		Downstream,
		Upstream
	}
	class CreatePipe
	{
		public static string ShaftLabelLayer = "123_Schacht_InfoTexte";
		public static string ShaftCoverLayer = "123_Schacht_DeckelHoehe";
		public static string ShaftTerrainLayer = "123_Schacht_GelaendeHoehe";
		public static string ShaftPipeInLayer = "123_Schacht_EinlaufHoehe";
		public static string ShaftPipeOutLayer = "123_Schacht_SohlHoehe";
		public static string ShaftBottomLayer = "123_Schacht_BodenHoehe";

		public static string PipeLabelLayer = "123_Haltung_InfoTexte";
		public static string Pipe3dDoubleLineLayer = "123_Haltung_3D_Rohr";
		public static string Pipe3dPolyLayer = "123_Strang_SohlLinie";
		public static string Pipe2dHelpLayer = "123_Strang_Hilfslinie";
		public static string Pipe3dHelpLayer = "123_Strang_Gelaendelinie";

		private class PointMonitor
		{
			internal Func<Point3d, string> SetTooltipText = null;
			internal Point3d CurrentPoint { get; private set; }
			//static int counter = 0;
			internal PointMonitor()
			{
			}
			internal void PointMonitorEvent(object sender, PointMonitorEventArgs e)
			{
				CurrentPoint = new Point3d(e.Context.ComputedPoint.X, e.Context.ComputedPoint.Y, e.Context.ComputedPoint.Z);
				if (SetTooltipText != null)
					e.AppendToolTipText(SetTooltipText.Invoke(CurrentPoint));
			}
		}
		private abstract class TerrainCalculatorFactory
		{
			public Database Database { get; private set; }
			public Editor Editor { get; private set; }
			public abstract TerrainCalculator GetCalculator();
			protected TerrainCalculatorFactory(Database db, Editor ed)
			{
				Editor = ed;
				Database = db;
			}
		}
		/// <summary>
		/// Only start point is used.
		/// </summary>
		private class StartPointHeightFactory : TerrainCalculatorFactory
		{
			private Plannings.TerrainCalculator calc;
			public StartPointHeightFactory (Database db, Editor ed, Point3d p):base(db, ed)
			{
				this.calc = new PointTerrainHeight(p.FromCADPoint());
			}
			public override TerrainCalculator GetCalculator()
			{
				return calc;
			}
		}
		/// <summary>
		/// Any time select new 3d face.
		/// </summary>
		private class Face3dFactory : TerrainCalculatorFactory
		{
			public ObjectId? FaceId { get; private set; }
			public Face3dFactory(Database db, Editor ed):base (db, ed)
			{

			}
			public override TerrainCalculator GetCalculator()
			{
				Plannings.TerrainCalculator calc = null;
				FaceId = Select3dFace(Editor);
				calc = null;
				if (FaceId.HasValue)
				{
					using (Transaction tr = Database.TransactionManager.StartTransaction())
					{
						Face f = tr.GetObject(FaceId.Value, OpenMode.ForRead) as Face;
						Point3d[] pts = new Point3d[4];
						for (short i = 0; i < 4; i++)
							pts[i] = f.GetVertexAt(i);
						// Face has 4 vertices (i find that 3 and 4 are the same).
						// But it can happen that they are not(?) .

						Face3dHeight faceHeight = new Face3dHeight(pts[0].FromCADPoint(), pts[1].FromCADPoint(), pts[2].FromCADPoint());
						calc = faceHeight;
					}
				}
				return calc;
			}
		}
		private class PointHeightFactory : TerrainCalculatorFactory
		{
			public PointHeightFactory (Database db, Editor ed):base(db, ed)
			{

			}
			public override TerrainCalculator GetCalculator()
			{
				while (true)
				{
					PromptPointOptions opt = new PromptPointOptions("\nSelect height point or type height: ");
					opt.AllowNone = false;
					opt.AllowArbitraryInput = true;
					PromptPointResult res = Editor.GetPoint(opt);
					if (!(res.Status == PromptStatus.Keyword || res.Status == PromptStatus.OK))
						return null;
					
					double d = res.Value.Z;
					if (res.Status == PromptStatus.Keyword)
					{
						if (!DoubleHelper.TryParseInvariant(res.StringResult, out d))
						{
							Editor.WriteMessage("Value is not a number!");
							continue;
						}
					}


					TerrainCalculator calc = new PointTerrainHeight(new MyUtilities.Geometry.Point3d(res.Value.X, res.Value.Y, d));
					return calc;
				}
			}
		}

		internal static void StartUpstream_Test(Document doc)
		{
			Editor ed = doc.Editor;
			Database db = doc.Database;
			PipeShaftDataManager dataManager = new Planning.PipeShaftDataManagers.UpstreamManager();
			dataManager.CreateStartShaft(true);
			dataManager.CreateEndShaft(true);
			dataManager.Calculation = new OffsetUpstream(0, 0);
			((ShaftData)dataManager.Data.EndShaft).MinPipeOutDeltaUsed = true;
			((ShaftData)dataManager.Data.EndShaft).MinPipeOutDelta = 1.5;
			((ShaftData)dataManager.Data.StartShaft).MinPipeOutDeltaUsed = true;
			((ShaftData)dataManager.Data.StartShaft).MinPipeOutDelta = 0.3;

			PromptPointOptions pointOpt = new PromptPointOptions("\nSelect start point: ");
			PromptPointResult res = ed.GetPoint(pointOpt);
			if (res.Status != PromptStatus.OK)
				return;
			Point3d selectedStartPoint = res.Value;
			dataManager.SetStartPoint(selectedStartPoint.FromCADPoint());
			dataManager.SetEndPoint(selectedStartPoint.FromCADPoint());

			DashedObjects dashed = new DashedObjects(db, ed);
			dashed.Create(dataManager, null);
			ed.Regen();
			TerrainCalculator tCalc = new PointTerrainHeight(new MyUtilities.Geometry.Point3d());
			

			double startOffset = dataManager.Data.StartShaft != null ? dataManager.Data.StartShaft.Radius : 0;
			double endOffset = dataManager.Data.EndShaft != null ? dataManager.Data.EndShaft.Radius : 0;
			PipeCalculation calculation;
			OffsetCalculator offsetCalc;

			offsetCalc = new OffsetUpstream(startOffset, endOffset);
			calculation = offsetCalc;
			if (dataManager.Data.Pipe.SegmentsUsed)
			{
				SegmentedDecorater segmentedCalc = new SegmentUpstream(offsetCalc, dataManager.Data.Pipe.SegmentLength, dataManager.Data.Pipe.StartEndSegmentLength);
				calculation = segmentedCalc;
			}
			dataManager.Calculation = calculation;
			dataManager.TerrainHeightCalculator = tCalc;

			JiggerCalculation calc = new OffsetJiggerUpstream(dataManager.Data.StartPoint.ToCADPoint(),
													startOffset, endOffset,
													dataManager.Data.Pipe.Slope, dataManager.Data.Pipe.SegmentLength);

			JigDataUpdater updater = new JigDataUpdater(calc.Data, dataManager);
			TextDisplayOrdered textOrdered = AdditionalSettings.AdditionalSettings.Instance.TextOrder;

			// If reselecting end point, draw current data with dashed lines.
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
				BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
				EntitiesManager entitiesManager = new EntitiesManager(dataManager.Data, tr, db);
				entitiesManager.InitializeJig(btr, dataManager.Data.StartPoint.ToCADPoint());
				JigTextManager text = new JigTextManager(dataManager, tr, btr, WaterDirection.Upstream, updater);
				text.Initialize(textOrdered, dataManager.Data.StartPoint.ToCADPoint());
				List<Entity> startShaftLabels = new List<Entity>();
				if (!dataManager.PreviousDataExists)
					startShaftLabels = DrawTextManager.DrawStartShaftText(db, tr, btr, dataManager, textOrdered);
				ed.Regen();

				List<Entity> jiggingEntities = new List<Entity>();
				if (entitiesManager.EndShaft != null)
					jiggingEntities.Add(entitiesManager.EndShaft);
				jiggingEntities.AddRange(text.Texts);
				jiggingEntities.AddRange(entitiesManager.jiggingEntities);

				LineWithEntitiesJigger jigger = new LineWithEntitiesJigger(entitiesManager.Line, jiggingEntities, calc, ed.CurrentUserCoordinateSystem);
				string[] keywords = new string[] { "Newface", "toBack" };
				jigger.SetMessage("\nSelect point", string.Join(" ", keywords));
				PromptResult jigResult = ed.Drag(jigger);
				

				if (jigResult.Status != PromptStatus.OK)
				{
					// Nothing is updated.
					//entitiesManager.Finish();
					//text.Finish();
				}
				else
				{
					// All data is updated to PipeShaftData.
				}
				
				startShaftLabels.ForEach(x => x.Erase());
				startShaftLabels.Clear();
				entitiesManager.Finish();
				text.Finish();
				tr.Commit();
			}
			dashed.Delete();

			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
				BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

				List<Entity> e1 = EntitiesManager.DrawEntities(db, tr, btr, dataManager.Data);
				List<Entity> e2 = DrawTextManager.DrawPipeText(db, tr, btr, dataManager, textOrdered);
				List<Entity> e3 = new List<Entity>();
				if (!dataManager.PreviousDataExists)
					e3 = DrawTextManager.DrawStartShaftText(db, tr, btr, dataManager, textOrdered);

				List<ObjectId> shaftGroup = new List<ObjectId>();
				shaftGroup.AddRange(e1.Select(x => x.ObjectId));
				shaftGroup.AddRange(e2.Select(x => x.ObjectId));
				shaftGroup.AddRange(e3.Select(x => x.ObjectId));

				tr.Commit();
			}
		}

		internal static void Start(Document doc, WaterDirection waterDir)
		{

			// All objects created in this method.
			//List<ObjectId> allEntities = new List<ObjectId>();


			// Incoming pipe and all entities for a shaft are one group - this is done to support Undo.
			// All entities are inside these collections.
			List<List<ObjectId>> entitiesByShafts = new List<List<ObjectId>>();
			Editor ed = doc.Editor;
			Database db = doc.Database;

			{
				// Set default text color (black for bricscad, white for autocad).
				MText test = new MText();
				AdditionalSettings.AdditionalSettings.DefaultTextColor = test.Color.ColorValue;
				//AdditionalSettings.AdditionalSettings.Instance.ArrowColor = test.Color.ColorValue;
				test.Dispose();
			}

			PipeShaftDataManager dataManager = null;
			PipeCalculation pipeCalc = null;
			switch (waterDir)
			{
				case WaterDirection.Downstream:
					dataManager = new Planning.PipeShaftDataManagers.DownstreamManager();
					pipeCalc = new OffsetDownstream(0,0);
					break;
				case WaterDirection.Upstream:
					dataManager = new Planning.PipeShaftDataManagers.UpstreamManager();
					pipeCalc = new OffsetUpstream(0, 0);
					break;
			}
			
			//PipeShaftDataHistory history = new PipeShaftDataHistory();
			TerrainCalculatorFactory factory = null;
			dataManager.CreateStartShaft(true);
			dataManager.CreateEndShaft(true);
			dataManager.Calculation = pipeCalc;

			string msg = "";
			switch (waterDir)
			{
				case WaterDirection.Downstream:msg = "Select start point: ";break;
				case WaterDirection.Upstream:msg = "Select end point: "; break;
			}
			PromptPointOptions pointOpt = new PromptPointOptions("\n" + msg);
			//Autodesk.AutoCAD.Internal.Utils.SetFocusToDwgView();
			PromptPointResult res = ed.GetPoint(pointOpt);
			if (res.Status != PromptStatus.OK)
				return;
			Point3d selectedStartPoint = res.Value;

			while (true)
			{
				PromptKeywordOptions heightTypeOpt = new PromptKeywordOptions("\nSelect height type (Current = " + DoubleHelper.ToStringInvariant(selectedStartPoint.Z, 2) + ") : ");
				heightTypeOpt.AllowArbitraryInput = true;
				string opt1 = "Current";
				string opt2 = "Point";
				string opt3 = "Face3d";
				heightTypeOpt.Keywords.Add(opt1);
				heightTypeOpt.Keywords.Add(opt2);
				heightTypeOpt.Keywords.Add(opt3);
				heightTypeOpt.Keywords.Default = opt1;
				PromptResult heightRes = ed.GetKeywords(heightTypeOpt);
				if (heightRes.Status != PromptStatus.OK)
					return;
				string resultStr = heightRes.StringResult;
				if (resultStr != opt1 && resultStr != opt2 && resultStr != opt3)
				{
					double d;
					if (!DoubleHelper.TryParseInvariant(resultStr, out d))
					{
						ed.WriteMessage("New height is not a number!\n");
						continue;
					}
					selectedStartPoint = new Point3d(selectedStartPoint.X, selectedStartPoint.Y, d);
					resultStr = opt1;
				}

				if (resultStr == opt1)
					factory = new StartPointHeightFactory(db, ed, selectedStartPoint);
				else if (resultStr == opt2)
					factory = new PointHeightFactory(db, ed);
				else if (resultStr == opt3)
					factory = new Face3dFactory(db, ed);
				/*else
				{
					ed.WriteMessage("\nSelection canceled");
					return;
				}*/
				break;
			}

			switch(waterDir)
			{
				case WaterDirection.Downstream: dataManager.SetStartPoint(selectedStartPoint.FromCADPoint()); break;
				case WaterDirection.Upstream: dataManager.SetEndPoint(selectedStartPoint.FromCADPoint()); break;
			}

			TextDisplayOrdered textOrdered = AdditionalSettings.AdditionalSettings.Instance.TextOrder;

			// If data is changed which caused selected point to be canceled, this is canceled point.
			//Geometry.Point3d? canceledPoint = null;
			bool reselect3dFace = false;
			ViewData viewData = new ViewData();
			while (true)
			{
				CloseReason reason;
				if (!reselect3dFace)
				{
					switch (waterDir)
					{
						case WaterDirection.Downstream:
							reason = PipeShaftViewController.RunDownstream(dataManager, viewData);
							break;
						case WaterDirection.Upstream:
							reason = PipeShaftViewController.RunUpstream(dataManager, viewData);
							break;
						default:
							reason = CloseReason.Cancel;
							break;
					}

				}
				else
					reason = CloseReason.SelectEndPoint;
				reselect3dFace = false;

				switch (reason)
				{
					case CloseReason.SelectEndPoint:
						SelectEndPoint(waterDir, entitiesByShafts, ed, db, dataManager, factory, textOrdered, ref reselect3dFace, viewData);
						break; 
					case CloseReason.Draw:
						// Performs drawing of current data.
						// Add created objects to all objects.
						using (Transaction tr = db.TransactionManager.StartTransaction())
						{
							BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
							BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

							List<Entity> entities = EntitiesManager.DrawEntities(db, tr, btr, dataManager.Data);
							List<Entity> pipeTexts = new List<Entity>();
							List<Entity> startShaftTexts = new List<Entity>();
							List<Entity> endShaftTexts = new List<Entity>();
							if (viewData.DrawPipeLabels)
								pipeTexts = DrawTextManager.DrawPipeText(db, tr, btr, dataManager, textOrdered);
							if (viewData.DrawStartShaftLabels)
								startShaftTexts = DrawTextManager.DrawStartShaftText(db, tr, btr, dataManager, textOrdered);
							if (viewData.DrawEndShaftLabels)
								endShaftTexts = DrawTextManager.DrawEndShaftText(db, tr, btr, dataManager, textOrdered);
							//if (!dataManager.PreviousDataExists && viewData.DrawStartShaftLabels)
								//endShaftTexts = DrawTextManager.DrawStartShaftText(db, tr, btr, dataManager, textOrdered);

							List<ObjectId> shaftGroup = new List<ObjectId>();
							shaftGroup.AddRange(entities.Select(x => x.ObjectId));
							shaftGroup.AddRange(pipeTexts.Select(x => x.ObjectId));
							shaftGroup.AddRange(startShaftTexts.Select(x => x.ObjectId));
							shaftGroup.AddRange(endShaftTexts.Select(x => x.ObjectId));
							entitiesByShafts.Add(shaftGroup);

							tr.Commit();
						}
						//dataManager.MoveToNextPipe();
						dataManager.MoveNextShaft();
						switch (waterDir)
						{
							case WaterDirection.Downstream:viewData.DrawStartShaftLabels = viewData.DrawEndShaftLabels;break;
							case WaterDirection.Upstream:viewData.DrawEndShaftLabels = viewData.DrawStartShaftLabels;break;
						}
						
						// Add delay
						PromptKeywordOptions opt = new PromptKeywordOptions("\nPress Enter to continue: ");
						opt.AllowNone = true;
						opt.AllowArbitraryInput = true;
						PromptResult res1 = ed.GetKeywords(opt);
						//if (res1.Status != PromptStatus.OK)
							//return;
						break;
					case CloseReason.Finish:
						CreateFinish(db, dataManager, waterDir); 
							return;
					case CloseReason.Cancel:
						// Delete everything created in command.
						using (Transaction tr = db.TransactionManager.StartTransaction())
						{
							foreach (List<ObjectId> list in entitiesByShafts)
								foreach (ObjectId id in list)
								{
									(tr.GetObject(id, OpenMode.ForWrite) as Entity).Erase();
								}

							tr.Commit();
						}
						return;
				}
			}
		}

		private static void SelectEndPoint(WaterDirection waterDir, List<List<ObjectId>> entitiesByShafts, Editor ed, Database db, PipeShaftDataManager dataManager, TerrainCalculatorFactory factory, TextDisplayOrdered textOrdered, ref bool reselect3dFace, ViewData viewData)
		{
			DashedObjects dashed = new DashedObjects(db, ed);
			dashed.Create(dataManager, viewData.CanceledPoint);
			ed.Regen();
			TerrainCalculator tCalc = factory.GetCalculator();
			if (tCalc == null)
			{
				ed.WriteMessage("\nUser canceled");
				dashed.Delete();
				return;
				//break;
			}
			Face3DHighlight highlight = new Face3DHighlight();
			ObjectId? faceId = null;
			if (factory is Face3dFactory)
			{
				Face3dFactory fc = factory as Face3dFactory;
				if (fc.FaceId.HasValue)
				{
					faceId = fc.FaceId.Value;
					highlight.Highlight(ed, db, faceId.Value);
				}
			}

			double startOffset = dataManager.Data.StartShaft != null ? dataManager.Data.StartShaft.Radius : 0;
			double endOffset = dataManager.Data.EndShaft != null ? dataManager.Data.EndShaft.Radius : 0;
			PipeCalculation calculation;
			OffsetCalculator offsetCalc = null;

			switch (waterDir)
			{
				case WaterDirection.Downstream:
					offsetCalc = new OffsetDownstream(startOffset, endOffset);
					break;
				case WaterDirection.Upstream:
					offsetCalc = new OffsetUpstream(startOffset, endOffset);
					break;
			}
			//offsetCalc = new OffsetCalculator(startOffset, endOffset);
			calculation = offsetCalc;
			if (dataManager.Data.Pipe.SegmentsUsed)
			{

				SegmentedDecorater segmentedCalc = null;
				switch (waterDir)
				{
					case WaterDirection.Downstream:
						segmentedCalc = new SegmentDownstream(offsetCalc, dataManager.Data.Pipe.SegmentLength, dataManager.Data.Pipe.StartEndSegmentLength);
						break;
					case WaterDirection.Upstream:
						segmentedCalc = new SegmentUpstream(offsetCalc, dataManager.Data.Pipe.SegmentLength, dataManager.Data.Pipe.StartEndSegmentLength);
						break;
				}
				calculation = segmentedCalc;
			}
			dataManager.Calculation = calculation;
			dataManager.TerrainHeightCalculator = tCalc;

			JiggerCalculation calc = null;

			switch (waterDir)
			{
				case WaterDirection.Downstream:
					calc = new OffsetJiggerDownstream(dataManager.Data.StartPoint.ToCADPoint(),
													startOffset, endOffset,
													dataManager.Data.Pipe.Slope, dataManager.Data.Pipe.SegmentLength);
					break;
				case WaterDirection.Upstream:
					calc = new OffsetJiggerUpstream(dataManager.Data.EndPoint.ToCADPoint(),
													startOffset, endOffset,
													dataManager.Data.Pipe.Slope, dataManager.Data.Pipe.SegmentLength);
					break;
			}

			JigDataUpdater updater = new JigDataUpdater(calc.Data, dataManager);

			// If reselecting end point, draw current data with dashed lines.
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
				BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
				EntitiesManager entitiesManager = new EntitiesManager(dataManager.Data, tr, db);
				Point3d fixedPoint = new Point3d();

				switch (waterDir)
				{
					case WaterDirection.Downstream: fixedPoint = dataManager.Data.StartPoint.ToCADPoint(); break;
					case WaterDirection.Upstream: fixedPoint = dataManager.Data.EndPoint.ToCADPoint(); break;
				}

				entitiesManager.InitializeJig(btr, fixedPoint);
				JigTextManager text = new JigTextManager(dataManager, tr, btr, waterDir, updater);

				text.Initialize(textOrdered, fixedPoint);
				List<Entity> startShaftLabels = new List<Entity>();
				if (!dataManager.PreviousDataExists)
					startShaftLabels = DrawTextManager.DrawStartShaftText(db, tr, btr, dataManager, textOrdered);
				ed.Regen();

				List<Entity> jiggingEntities = new List<Entity>();
				if (waterDir == WaterDirection.Downstream && entitiesManager.EndShaft != null)
					jiggingEntities.Add(entitiesManager.EndShaft);
				else if (waterDir == WaterDirection.Upstream && entitiesManager.StartShaft != null)
					jiggingEntities.Add(entitiesManager.StartShaft);
				jiggingEntities.AddRange(text.Texts);
				jiggingEntities.AddRange(entitiesManager.jiggingEntities);

				LineWithEntitiesJigger jigger = new LineWithEntitiesJigger(entitiesManager.Line, jiggingEntities, calc, ed.CurrentUserCoordinateSystem);

				//TODO This keyword should depend on height object type (point/3dface)
				string[] keywords = new string[] { "Newface", "toBack" };
				jigger.SetMessage("\nSelect point", string.Join(" ", keywords));
				PromptResult jigResult = ed.Drag(jigger);

				// If highlighted face, unhighlight it.
				highlight.Unhighlight(db);

				if (jigResult.Status != PromptStatus.OK)
				{
					// Nothing is updated.
					//entitiesManager.Finish();
					//text.Finish();
				}
				else
				{
					// All data is updated to PipeShaftData.
				}

				if (jigger.JigKeywordEntered)
				{
					if (jigger.EnteredKeyword == keywords[0])
						reselect3dFace = true;
					if (jigger.EnteredKeyword == keywords[1])
					{
						if (faceId.HasValue)
						{
							using (Transaction in1Tr = db.TransactionManager.StartTransaction())
							{
								BlockTable btIn = in1Tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
								BlockTableRecord btrModelSpace = in1Tr.GetObject(btIn[BlockTableRecord.ModelSpace], OpenMode.ForRead) as BlockTableRecord;
								DrawOrderTable dot = in1Tr.GetObject(btrModelSpace.DrawOrderTableId, OpenMode.ForWrite) as DrawOrderTable;

								ObjectIdCollection objToMove = new ObjectIdCollection();
								objToMove.Add(faceId.Value);
								dot.MoveToBottom(objToMove);
								in1Tr.Commit();
							}
							reselect3dFace = true;
						}
					}
					if (jigger.EnteredKeyword == "back")
					{
						// Requires more work. Entities are deleted, but inner structure
						// which supports data is not updated accordingly.
						if (entitiesByShafts.Count > 0)
						{
							foreach (ObjectId oid in entitiesByShafts.Last())
								tr.GetObject(oid, OpenMode.ForWrite).Erase();
							entitiesByShafts.RemoveAt(entitiesByShafts.Count - 1);
						}
					}
				}

				startShaftLabels.ForEach(x => x.Erase());
				startShaftLabels.Clear();
				entitiesManager.Finish();
				text.Finish();
				tr.Commit();
			}
			dashed.Delete();
			return;
			//break;
		}

		private static bool GetSegmentLength(Editor ed, ref double length)
		{
			PromptDoubleOptions doubleOpt = new PromptDoubleOptions("\nType segment length:");
			PromptDoubleResult res = ed.GetDouble(doubleOpt);
			length = res.Value;
			return res.Status == PromptStatus.OK;
		}
		private static bool GetDiameter(Editor ed, ref double diameter)
		{
			PromptDoubleOptions doubleOpt = new PromptDoubleOptions("\nType shaft radius:");
			doubleOpt.DefaultValue = 1;
			PromptDoubleResult res = ed.GetDouble(doubleOpt);
			diameter = res.Value;
			// Diameter is returned.
			diameter *= 2;
			if (diameter == 0)
				return false;
			return res.Status == PromptStatus.OK;
		}
		private static bool GetSlope(Editor ed, ref double slope, bool slopeExists = false)
		{
			PromptDoubleOptions doubleOpt = new PromptDoubleOptions("\nType slope in % " + (slopeExists ? " (current slope: " + DoubleHelper.ToStringInvariant(slope, 2) + ")" : "") + ":");
			if (slopeExists)
				doubleOpt.DefaultValue = slope;
			PromptDoubleResult res = ed.GetDouble(doubleOpt);
			if (res.Status != PromptStatus.OK)
				return false;
			slope = res.Value / 100.0;
			return true;
		}
		
		private static ObjectId? Select3dFace(Editor ed)
		{
			Database db = ed.Document.Database;
			while (true)
			{
				PromptEntityOptions opt = new PromptEntityOptions("\nSelect 3d Face");
				opt.SetRejectMessage("Not a 3d Face object!");
				opt.AddAllowedClass(typeof(Face), true);
				opt.Keywords.Add("toBack");
				PromptEntityResult res = ed.GetEntity(opt);

				if (res.Status == PromptStatus.Keyword) 
				{
					opt = new PromptEntityOptions("\nSelect 3d Face object to move to the back:");
					opt.SetRejectMessage("Not a 3d Face object!");
					opt.AddAllowedClass(typeof(Face), true);
					res = ed.GetEntity(opt);
					using (Transaction tr = db.TransactionManager.StartTransaction())
					{
						BlockTable btIn = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
						BlockTableRecord btrModelSpace = tr.GetObject(btIn[BlockTableRecord.ModelSpace], OpenMode.ForRead) as BlockTableRecord;
						DrawOrderTable dot = tr.GetObject(btrModelSpace.DrawOrderTableId, OpenMode.ForWrite) as DrawOrderTable;

						ObjectIdCollection objToMove = new ObjectIdCollection();
						objToMove.Add(res.ObjectId);
						dot.MoveToBottom(objToMove);
						tr.Commit();
					}
				}
				else if (res.Status == PromptStatus.OK)
				{
					return res.ObjectId;
				}
				else break;
			}
			return null;
		}
		/// <summary>
		/// Helper for fetching drawing data. For downstream it goes from start to end point
		/// for upstream it goes opposite.
		/// Otherwise, for upstream we will go from start to end point of next shaft, which is same shaft.
		/// (By default, downstream is considered. Start=Start for downstream. For upstream start=end).
		/// </summary>
		private struct DataHelper
		{
			private IPipeShaftData data;
			private WaterDirection water;
			public MyUtilities.Geometry.Point3d StartPoint
			{
				get
				{
					if (water == WaterDirection.Downstream)
						return data.StartPoint;
					return data.EndPoint;
				}
			}
			public MyUtilities.Geometry.Point3d EndPoint
			{
				get
				{
					if (water == WaterDirection.Downstream)
						return data.EndPoint;
					return data.StartPoint;
				}
			}
			public IShaftData StartShaft
			{
				get
				{
					if (water == WaterDirection.Downstream)
						return data.StartShaft;
					return data.EndShaft;
				}
			}
			public IShaftData EndShaft
			{
				get
				{
					if (water == WaterDirection.Downstream)
						return data.EndShaft;
					return data.StartShaft;
				}
			}
			public MyUtilities.Geometry.Point3d StartPipePoint
			{
				get
				{
					if (water == WaterDirection.Downstream)
						return data.Pipe.StartPoint;
					return data.Pipe.EndPoint;
				}
			}
			public MyUtilities.Geometry.Point3d EndPipePoint
			{
				get
				{
					if (water == WaterDirection.Downstream)
						return data.Pipe.EndPoint;
					return data.Pipe.StartPoint;
				}
			}

			public DataHelper(IPipeShaftData data, WaterDirection dir)
			{
				this.data = data;
				water = dir;
			}
		}

		/// <summary>
		/// When drawing is finished, post draw all needed.
		/// </summary>
		/// <param name="db"></param>
		/// <param name="mgr"></param>
		/// <param name="waterDir"></param>
		private static void CreateFinish(Database db, PipeShaftDataManager mgr, WaterDirection waterDir)
		{
			CADLayerHelper.AddLayerIfDoesntExist(db, Pipe2dHelpLayer);
			CADLayerHelper.AddLayerIfDoesntExist(db, Pipe3dHelpLayer);
			CADLayerHelper.AddLayerIfDoesntExist(db, Pipe3dPolyLayer);

			// Create 3d and 2d terrain points polyline.
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
				BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

				Point3dCollection pts = new Point3dCollection();
				bool firstIteration = true;
				foreach (IPipeShaftData data in mgr.DataHistory)
				{
					MyUtilities.Geometry.Point3d p = new MyUtilities.Geometry.Point3d();
					DataHelper helper = new DataHelper(data, waterDir);
					if (firstIteration)
					{
						p = helper.StartPoint;
						
						if (helper.StartShaft != null)
							p = new MyUtilities.Geometry.Point3d(helper.StartShaft.Center.X, helper.StartShaft.Center.Y, helper.StartShaft.TerrainHeight);
						pts.Add(p.ToCADPoint());
						firstIteration = false;
					}
					p = helper.EndPoint;
					if (helper.EndShaft != null)
						p = new MyUtilities.Geometry.Point3d(helper.EndShaft.Center.X, helper.EndShaft.Center.Y, helper.EndShaft.TerrainHeight);
					pts.Add(p.ToCADPoint());
				}
				Polyline3d p3d = new Polyline3d(Poly3dType.SimplePoly, pts, false);
				btr.AppendEntityCurrentLayer(p3d);
				tr.AddNewlyCreatedDBObject(p3d, true);
				p3d.Layer = Pipe3dHelpLayer;


				Polyline pl = new Polyline();
				btr.AppendEntityCurrentLayer(pl);
				tr.AddNewlyCreatedDBObject(pl, true);
				pl.Layer = Pipe2dHelpLayer;
				for (int i = 0; i < pts.Count; i++)
				{
					pl.AddVertexAt(i, new Point2d(pts[i].X, pts[i].Y), 0, 0, 0);
				}

				// Create 3d polyline for pipe (pipe out-pipe in- center of shaft (z is interpolated to pipe out height)
				List<Point3d> pipePoints = new List<Point3d>();
				Point3dCollection pipePointsCollection = new Point3dCollection();
				firstIteration = true;
				Point3d previous = new Point3d();
				 foreach (IPipeShaftData data in mgr.DataHistory)
				{
					MyUtilities.Geometry.Point3d p;
					// Helper with water direction. 
					DataHelper helper = new DataHelper(data, waterDir);

					if (!firstIteration)
					{
						// Vertical point if in/out Z is different.
						if (previous.Z != helper.StartPoint.Z)
						{
							p = previous.FromCADPoint().GetAs2d().GetWithHeight(helper.StartPoint.Z);
							//pipePoints.Add(p.ToCADPoint());
						}
					}
					firstIteration = false;

					p = helper.StartPoint.GetAs2d().GetWithHeight(helper.StartPipePoint.Z);
					pipePoints.Add(p.ToCADPoint());
					if (helper.StartShaft != null)
					{
						p = helper.StartPipePoint;
						pipePoints.Add(p.ToCADPoint());
					}

					if (helper.EndShaft != null)
					{
						p = helper.EndPipePoint;
						pipePoints.Add(p.ToCADPoint());
					}
					p = helper.EndPoint.GetAs2d().GetWithHeight(helper.EndPipePoint.Z);
					pipePoints.Add(p.ToCADPoint());

					previous = p.ToCADPoint();


					/*if (firstIteration)
					{
						p = helper.StartPoint;
						pipePoints.Add(p.ToCADPoint());
						firstIteration = false;
					}
					if (helper.StartShaft != null)
					{
						p = helper.StartPipePoint;
						//pipePoints.Add(p.ToCADPoint());
					}
					double interpolatedZ = data.EndPoint.Z;
					if (helper.EndShaft != null)
					{
						p = helper.EndPipePoint;
						//pipePoints.Add(p.ToCADPoint());
						interpolatedZ = (p.Z + helper.EndShaft.PipeOutHeight) / 2;
					}
					p = helper.EndPoint;
					pipePoints.Add(new Point3d(p.X, p.Y, interpolatedZ));*/
				}
				foreach (var p in pipePoints)
					pipePointsCollection.Add(p);
				Polyline3d pipe3d = new Polyline3d(Poly3dType.SimplePoly, pipePointsCollection, false);
				btr.AppendEntityCurrentLayer(pipe3d);
				tr.AddNewlyCreatedDBObject(pipe3d, true);
				pipe3d.Layer = Pipe3dPolyLayer;

				tr.Commit();
			}
		}
	}
}
