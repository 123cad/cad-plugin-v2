﻿using CadPlugin.Sewage.Create.ShaftControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.Sewage.Create
{
	public enum CloseReason
	{
		SelectEndPoint,
		/// <summary>
		/// Draws current data.
		/// </summary>
		Draw, 
		Finish, 
		Cancel
	}
	internal interface IPipeShaftView
	{
		CloseReason CloseReason { get; }

		IShaftControlView StartShaft { get; }
		IShaftControlView EndShaft { get; }
		Totals.Totals Totals { get; }

		TextBox PipeDiameter { get; }
		TextBox MinPipeSlope { get; }
		Label PipeSlope { get; }
		TextBox PipeStartEndSegmentLength { get; }
		TextBox PipeSegmentLength { get; }
		TextBox StepAngle { get; }

		string PipeAngle { set; }
		string PipeLength2d { set; }
		string PipeLength { set; } 
		string SegmentCount { set; }
		string PipeHeightDelta { set; }

		event Action OpenSettings;

		Button DrawRequest { get; }
		Button SelectPoint { get; }
		/// <summary>
		/// Used to enable tooltip on disabled DrawRequest button.
		/// </summary>
		Panel DrawRequestPanel { get; }
		
		CheckBox MinPipeSlopeUsed { get; }

		CheckBox MinStartShaftDeltaUsed { get; }
		TextBox MinStartShaftDelta { get; }
		CheckBox MinEndShaftDeltaUsed { get; }
		TextBox MinEndShaftDelta { get; }

		CheckBox SegmentsUsed { get; }
		CheckBox StepAngleUsed { get; }

		PictureBox WaterDirectionPicture { get; }


	}
}
