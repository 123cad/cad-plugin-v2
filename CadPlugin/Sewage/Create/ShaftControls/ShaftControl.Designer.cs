﻿namespace CadPlugin.Sewage.Create.ShaftControls
{
	partial class ShaftControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.groupBoxShaft = new System.Windows.Forms.GroupBox();
            this.labelMinDelta = new System.Windows.Forms.Label();
            this.checkBoxMinShaftDelta = new System.Windows.Forms.CheckBox();
            this.textBoxMinShaftDelta = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.textBoxShaftTerrain = new System.Windows.Forms.TextBox();
            this.labelTerrainHeight = new System.Windows.Forms.Label();
            this.textBoxShaftPipeOut = new System.Windows.Forms.TextBox();
            this.labelPipeOut = new System.Windows.Forms.Label();
            this.textBoxShaftTerrainPipeOutDelta = new System.Windows.Forms.TextBox();
            this.labelDelta = new System.Windows.Forms.Label();
            this.textBoxShaftInnerSlope = new System.Windows.Forms.TextBox();
            this.labelInnerSlope = new System.Windows.Forms.Label();
            this.textBoxShaftDiameter = new System.Windows.Forms.TextBox();
            this.labelDiameter = new System.Windows.Forms.Label();
            this.labelInnerDelta = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxShaftDepth = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxShaftBottom = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxShaftPipeOutBottomDelta = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxShaftCover = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxShaftCoverTerrainDelta = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.checkBoxCreateShaft = new System.Windows.Forms.CheckBox();
            this.checkBoxDrawLabels = new System.Windows.Forms.CheckBox();
            this.groupBoxShaft.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxShaft
            // 
            this.groupBoxShaft.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxShaft.Controls.Add(this.labelMinDelta);
            this.groupBoxShaft.Controls.Add(this.checkBoxMinShaftDelta);
            this.groupBoxShaft.Controls.Add(this.textBoxMinShaftDelta);
            this.groupBoxShaft.Controls.Add(this.label48);
            this.groupBoxShaft.Controls.Add(this.label46);
            this.groupBoxShaft.Controls.Add(this.label32);
            this.groupBoxShaft.Controls.Add(this.label29);
            this.groupBoxShaft.Controls.Add(this.label28);
            this.groupBoxShaft.Controls.Add(this.textBoxShaftTerrain);
            this.groupBoxShaft.Controls.Add(this.labelTerrainHeight);
            this.groupBoxShaft.Controls.Add(this.textBoxShaftPipeOut);
            this.groupBoxShaft.Controls.Add(this.labelPipeOut);
            this.groupBoxShaft.Controls.Add(this.textBoxShaftTerrainPipeOutDelta);
            this.groupBoxShaft.Controls.Add(this.labelDelta);
            this.groupBoxShaft.Controls.Add(this.textBoxShaftInnerSlope);
            this.groupBoxShaft.Controls.Add(this.labelInnerSlope);
            this.groupBoxShaft.Controls.Add(this.textBoxShaftDiameter);
            this.groupBoxShaft.Controls.Add(this.labelDiameter);
            this.groupBoxShaft.Location = new System.Drawing.Point(3, 28);
            this.groupBoxShaft.Name = "groupBoxShaft";
            this.groupBoxShaft.Size = new System.Drawing.Size(317, 171);
            this.groupBoxShaft.TabIndex = 5;
            this.groupBoxShaft.TabStop = false;
            this.groupBoxShaft.Text = "Start Shaft Settings";
            // 
            // labelMinDelta
            // 
            this.labelMinDelta.AutoSize = true;
            this.labelMinDelta.Location = new System.Drawing.Point(197, 115);
            this.labelMinDelta.Name = "labelMinDelta";
            this.labelMinDelta.Size = new System.Drawing.Size(52, 13);
            this.labelMinDelta.TabIndex = 94;
            this.labelMinDelta.Text = "Min Delta";
            // 
            // checkBoxMinShaftDelta
            // 
            this.checkBoxMinShaftDelta.AutoSize = true;
            this.checkBoxMinShaftDelta.Location = new System.Drawing.Point(182, 114);
            this.checkBoxMinShaftDelta.Name = "checkBoxMinShaftDelta";
            this.checkBoxMinShaftDelta.Size = new System.Drawing.Size(71, 17);
            this.checkBoxMinShaftDelta.TabIndex = 92;
            this.checkBoxMinShaftDelta.TabStop = false;
            this.checkBoxMinShaftDelta.Text = "Min Delta";
            this.checkBoxMinShaftDelta.UseVisualStyleBackColor = true;
            // 
            // textBoxMinShaftDelta
            // 
            this.textBoxMinShaftDelta.Location = new System.Drawing.Point(267, 112);
            this.textBoxMinShaftDelta.Name = "textBoxMinShaftDelta";
            this.textBoxMinShaftDelta.Size = new System.Drawing.Size(37, 20);
            this.textBoxMinShaftDelta.TabIndex = 9;
            this.textBoxMinShaftDelta.Text = "5";
            this.textBoxMinShaftDelta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(164, 45);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(15, 13);
            this.label48.TabIndex = 61;
            this.label48.Text = "%";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(162, 19);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(23, 13);
            this.label46.TabIndex = 61;
            this.label46.Text = "mm";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(164, 141);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(15, 13);
            this.label32.TabIndex = 26;
            this.label32.Text = "m";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(164, 115);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(15, 13);
            this.label29.TabIndex = 25;
            this.label29.Text = "m";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(164, 89);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(15, 13);
            this.label28.TabIndex = 24;
            this.label28.Text = "m";
            // 
            // textBoxShaftTerrain
            // 
            this.textBoxShaftTerrain.Enabled = false;
            this.textBoxShaftTerrain.Location = new System.Drawing.Point(103, 86);
            this.textBoxShaftTerrain.Name = "textBoxShaftTerrain";
            this.textBoxShaftTerrain.Size = new System.Drawing.Size(55, 20);
            this.textBoxShaftTerrain.TabIndex = 6;
            this.textBoxShaftTerrain.Text = "2";
            this.textBoxShaftTerrain.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelTerrainHeight
            // 
            this.labelTerrainHeight.AutoSize = true;
            this.labelTerrainHeight.Location = new System.Drawing.Point(6, 86);
            this.labelTerrainHeight.Name = "labelTerrainHeight";
            this.labelTerrainHeight.Size = new System.Drawing.Size(72, 13);
            this.labelTerrainHeight.TabIndex = 9;
            this.labelTerrainHeight.Text = "Terrain height";
            this.labelTerrainHeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxShaftPipeOut
            // 
            this.textBoxShaftPipeOut.Location = new System.Drawing.Point(103, 138);
            this.textBoxShaftPipeOut.Name = "textBoxShaftPipeOut";
            this.textBoxShaftPipeOut.Size = new System.Drawing.Size(55, 20);
            this.textBoxShaftPipeOut.TabIndex = 8;
            this.textBoxShaftPipeOut.Text = "3750";
            this.textBoxShaftPipeOut.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelPipeOut
            // 
            this.labelPipeOut.AutoSize = true;
            this.labelPipeOut.Location = new System.Drawing.Point(6, 141);
            this.labelPipeOut.Name = "labelPipeOut";
            this.labelPipeOut.Size = new System.Drawing.Size(46, 13);
            this.labelPipeOut.TabIndex = 7;
            this.labelPipeOut.Text = "Pipe out";
            this.labelPipeOut.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxShaftTerrainPipeOutDelta
            // 
            this.textBoxShaftTerrainPipeOutDelta.Location = new System.Drawing.Point(116, 112);
            this.textBoxShaftTerrainPipeOutDelta.Name = "textBoxShaftTerrainPipeOutDelta";
            this.textBoxShaftTerrainPipeOutDelta.Size = new System.Drawing.Size(42, 20);
            this.textBoxShaftTerrainPipeOutDelta.TabIndex = 7;
            this.textBoxShaftTerrainPipeOutDelta.Text = "-22.542";
            this.textBoxShaftTerrainPipeOutDelta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelDelta
            // 
            this.labelDelta.AutoSize = true;
            this.labelDelta.Location = new System.Drawing.Point(65, 115);
            this.labelDelta.Name = "labelDelta";
            this.labelDelta.Size = new System.Drawing.Size(30, 13);
            this.labelDelta.TabIndex = 5;
            this.labelDelta.Text = "delta";
            this.labelDelta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxShaftInnerSlope
            // 
            this.textBoxShaftInnerSlope.Location = new System.Drawing.Point(112, 42);
            this.textBoxShaftInnerSlope.Name = "textBoxShaftInnerSlope";
            this.textBoxShaftInnerSlope.Size = new System.Drawing.Size(46, 20);
            this.textBoxShaftInnerSlope.TabIndex = 3;
            this.textBoxShaftInnerSlope.Text = "2.5";
            this.textBoxShaftInnerSlope.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelInnerSlope
            // 
            this.labelInnerSlope.AutoSize = true;
            this.labelInnerSlope.Location = new System.Drawing.Point(6, 45);
            this.labelInnerSlope.Name = "labelInnerSlope";
            this.labelInnerSlope.Size = new System.Drawing.Size(59, 13);
            this.labelInnerSlope.TabIndex = 3;
            this.labelInnerSlope.Text = "Inner slope";
            this.labelInnerSlope.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxShaftDiameter
            // 
            this.textBoxShaftDiameter.Location = new System.Drawing.Point(103, 16);
            this.textBoxShaftDiameter.Name = "textBoxShaftDiameter";
            this.textBoxShaftDiameter.Size = new System.Drawing.Size(55, 20);
            this.textBoxShaftDiameter.TabIndex = 2;
            this.textBoxShaftDiameter.Text = "1000";
            this.textBoxShaftDiameter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelDiameter
            // 
            this.labelDiameter.AutoSize = true;
            this.labelDiameter.Location = new System.Drawing.Point(6, 19);
            this.labelDiameter.Name = "labelDiameter";
            this.labelDiameter.Size = new System.Drawing.Size(49, 13);
            this.labelDiameter.TabIndex = 1;
            this.labelDiameter.Text = "Diameter";
            this.labelDiameter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelInnerDelta
            // 
            this.labelInnerDelta.AutoSize = true;
            this.labelInnerDelta.Location = new System.Drawing.Point(502, 70);
            this.labelInnerDelta.Name = "labelInnerDelta";
            this.labelInnerDelta.Size = new System.Drawing.Size(73, 13);
            this.labelInnerDelta.TabIndex = 63;
            this.labelInnerDelta.Text = "Delta:   12 cm";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(536, 304);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(15, 13);
            this.label54.TabIndex = 62;
            this.label54.Text = "m";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(457, 264);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(15, 13);
            this.label34.TabIndex = 28;
            this.label34.Text = "m";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(457, 238);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(21, 13);
            this.label33.TabIndex = 27;
            this.label33.Text = "cm";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(555, 143);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(21, 13);
            this.label27.TabIndex = 23;
            this.label27.Text = "cm";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(555, 117);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(15, 13);
            this.label26.TabIndex = 22;
            this.label26.Text = "m";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(441, 304);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 13);
            this.label15.TabIndex = 21;
            this.label15.Text = "Depth";
            // 
            // textBoxShaftDepth
            // 
            this.textBoxShaftDepth.Location = new System.Drawing.Point(483, 301);
            this.textBoxShaftDepth.Name = "textBoxShaftDepth";
            this.textBoxShaftDepth.ReadOnly = true;
            this.textBoxShaftDepth.Size = new System.Drawing.Size(47, 20);
            this.textBoxShaftDepth.TabIndex = 20;
            this.textBoxShaftDepth.TabStop = false;
            this.textBoxShaftDepth.Text = "10.54";
            this.textBoxShaftDepth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(477, 167);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(68, 121);
            this.panel1.TabIndex = 19;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(-33, 9);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(91, 103);
            this.panel2.TabIndex = 20;
            // 
            // textBoxShaftBottom
            // 
            this.textBoxShaftBottom.Location = new System.Drawing.Point(396, 261);
            this.textBoxShaftBottom.Name = "textBoxShaftBottom";
            this.textBoxShaftBottom.Size = new System.Drawing.Size(55, 20);
            this.textBoxShaftBottom.TabIndex = 11;
            this.textBoxShaftBottom.Text = "3750";
            this.textBoxShaftBottom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(348, 264);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 13);
            this.label13.TabIndex = 17;
            this.label13.Text = "Bottom";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxShaftPipeOutBottomDelta
            // 
            this.textBoxShaftPipeOutBottomDelta.Location = new System.Drawing.Point(409, 235);
            this.textBoxShaftPipeOutBottomDelta.Name = "textBoxShaftPipeOutBottomDelta";
            this.textBoxShaftPipeOutBottomDelta.Size = new System.Drawing.Size(42, 20);
            this.textBoxShaftPipeOutBottomDelta.TabIndex = 10;
            this.textBoxShaftPipeOutBottomDelta.Text = "2";
            this.textBoxShaftPipeOutBottomDelta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(358, 238);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 13);
            this.label14.TabIndex = 15;
            this.label14.Text = "delta";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxShaftCover
            // 
            this.textBoxShaftCover.Location = new System.Drawing.Point(494, 114);
            this.textBoxShaftCover.Name = "textBoxShaftCover";
            this.textBoxShaftCover.Size = new System.Drawing.Size(55, 20);
            this.textBoxShaftCover.TabIndex = 4;
            this.textBoxShaftCover.Text = "2";
            this.textBoxShaftCover.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(397, 117);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "Cover height";
            // 
            // textBoxShaftCoverTerrainDelta
            // 
            this.textBoxShaftCoverTerrainDelta.Location = new System.Drawing.Point(507, 140);
            this.textBoxShaftCoverTerrainDelta.Name = "textBoxShaftCoverTerrainDelta";
            this.textBoxShaftCoverTerrainDelta.Size = new System.Drawing.Size(42, 20);
            this.textBoxShaftCoverTerrainDelta.TabIndex = 5;
            this.textBoxShaftCoverTerrainDelta.Text = "2";
            this.textBoxShaftCoverTerrainDelta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(456, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "delta";
            // 
            // checkBoxCreateShaft
            // 
            this.checkBoxCreateShaft.AutoSize = true;
            this.checkBoxCreateShaft.Checked = true;
            this.checkBoxCreateShaft.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCreateShaft.Location = new System.Drawing.Point(12, 5);
            this.checkBoxCreateShaft.Name = "checkBoxCreateShaft";
            this.checkBoxCreateShaft.Size = new System.Drawing.Size(108, 17);
            this.checkBoxCreateShaft.TabIndex = 0;
            this.checkBoxCreateShaft.Text = "Create Start shaft";
            this.checkBoxCreateShaft.UseVisualStyleBackColor = true;
            // 
            // checkBoxDrawLabels
            // 
            this.checkBoxDrawLabels.AutoSize = true;
            this.checkBoxDrawLabels.Checked = true;
            this.checkBoxDrawLabels.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDrawLabels.Location = new System.Drawing.Point(477, 5);
            this.checkBoxDrawLabels.Name = "checkBoxDrawLabels";
            this.checkBoxDrawLabels.Size = new System.Drawing.Size(81, 17);
            this.checkBoxDrawLabels.TabIndex = 1;
            this.checkBoxDrawLabels.Text = "Draw labels";
            this.checkBoxDrawLabels.UseVisualStyleBackColor = true;
            // 
            // ShaftControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.checkBoxDrawLabels);
            this.Controls.Add(this.groupBoxShaft);
            this.Controls.Add(this.checkBoxCreateShaft);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.labelInnerDelta);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.textBoxShaftCoverTerrainDelta);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.textBoxShaftDepth);
            this.Controls.Add(this.textBoxShaftCover);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textBoxShaftPipeOutBottomDelta);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textBoxShaftBottom);
            this.Controls.Add(this.label13);
            this.Name = "ShaftControl";
            this.Size = new System.Drawing.Size(632, 202);
            this.groupBoxShaft.ResumeLayout(false);
            this.groupBoxShaft.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBoxShaft;
		private System.Windows.Forms.Label labelInnerDelta;
		private System.Windows.Forms.Label label54;
		private System.Windows.Forms.Label label48;
		private System.Windows.Forms.Label label46;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox textBoxShaftDepth;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.TextBox textBoxShaftBottom;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox textBoxShaftPipeOutBottomDelta;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox textBoxShaftCover;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox textBoxShaftCoverTerrainDelta;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox textBoxShaftTerrain;
		private System.Windows.Forms.Label labelTerrainHeight;
		private System.Windows.Forms.TextBox textBoxShaftPipeOut;
		private System.Windows.Forms.Label labelPipeOut;
		private System.Windows.Forms.TextBox textBoxShaftTerrainPipeOutDelta;
		private System.Windows.Forms.Label labelDelta;
		private System.Windows.Forms.TextBox textBoxShaftInnerSlope;
		private System.Windows.Forms.Label labelInnerSlope;
		private System.Windows.Forms.TextBox textBoxShaftDiameter;
		private System.Windows.Forms.Label labelDiameter;
		private System.Windows.Forms.CheckBox checkBoxCreateShaft;
		private System.Windows.Forms.TextBox textBoxMinShaftDelta;
		private System.Windows.Forms.CheckBox checkBoxMinShaftDelta;
		private System.Windows.Forms.CheckBox checkBoxDrawLabels;
		private System.Windows.Forms.Label labelMinDelta;
	}
}
