﻿namespace CadPlugin.Sewage.Create
{
	partial class PipeShaftView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label50 = new System.Windows.Forms.Label();
            this.checkBoxSegmentsUsed = new System.Windows.Forms.CheckBox();
            this.checkBoxAngleStepUsed = new System.Windows.Forms.CheckBox();
            this.checkBoxMinPipeSlope = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.labelSlope = new System.Windows.Forms.Label();
            this.textBoxMinPipeSlope = new System.Windows.Forms.TextBox();
            this.textBoxPipeSegmentLength = new System.Windows.Forms.TextBox();
            this.labelSegmentLength = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonDraw = new System.Windows.Forms.Button();
            this.buttonFinish = new System.Windows.Forms.Button();
            this.textBoxPipeDiameter = new System.Windows.Forms.TextBox();
            this.labelDiameter = new System.Windows.Forms.Label();
            this.textBoxPipeStartEndSegment = new System.Windows.Forms.TextBox();
            this.labelStartEndSegmentLength = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.labelHeightDelta = new System.Windows.Forms.Label();
            this.labelPipeLength = new System.Windows.Forms.Label();
            this.labelPipeHeightDelta = new System.Windows.Forms.Label();
            this.labelPipeSegments = new System.Windows.Forms.Label();
            this.buttonSelectEndPoint = new System.Windows.Forms.Button();
            this.buttonSettings = new System.Windows.Forms.Button();
            this.panelDrawRequest = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelPipeLength2d = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelPipeAngleText = new System.Windows.Forms.Label();
            this.labelPipeAngle = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxAngleStep = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.labelAngleStep = new System.Windows.Forms.Label();
            this.labelPipeSlope = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.pictureBoxWaterDirection = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.endShaftView = new CadPlugin.Sewage.Create.ShaftControls.ShaftControl();
            this.startShaftView = new CadPlugin.Sewage.Create.ShaftControls.ShaftControl();
            this.totalsView = new CadPlugin.Sewage.Create.Totals.Totals();
            this.panelDrawRequest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWaterDirection)).BeginInit();
            this.SuspendLayout();
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 100;
            this.toolTip1.ReshowDelay = 100;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(340, 518);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(82, 13);
            this.label50.TabIndex = 63;
            this.label50.Text = "Total segments:";
            this.toolTip1.SetToolTip(this.label50, "Not including start and end segment!");
            // 
            // checkBoxSegmentsUsed
            // 
            this.checkBoxSegmentsUsed.AutoSize = true;
            this.checkBoxSegmentsUsed.Checked = true;
            this.checkBoxSegmentsUsed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSegmentsUsed.Location = new System.Drawing.Point(543, 128);
            this.checkBoxSegmentsUsed.Name = "checkBoxSegmentsUsed";
            this.checkBoxSegmentsUsed.Size = new System.Drawing.Size(73, 17);
            this.checkBoxSegmentsUsed.TabIndex = 79;
            this.checkBoxSegmentsUsed.TabStop = false;
            this.checkBoxSegmentsUsed.Text = "Segments";
            this.toolTip1.SetToolTip(this.checkBoxSegmentsUsed, "Segments used or not");
            this.checkBoxSegmentsUsed.UseVisualStyleBackColor = true;
            // 
            // checkBoxAngleStepUsed
            // 
            this.checkBoxAngleStepUsed.AutoSize = true;
            this.checkBoxAngleStepUsed.Checked = true;
            this.checkBoxAngleStepUsed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAngleStepUsed.Location = new System.Drawing.Point(543, 167);
            this.checkBoxAngleStepUsed.Name = "checkBoxAngleStepUsed";
            this.checkBoxAngleStepUsed.Size = new System.Drawing.Size(59, 17);
            this.checkBoxAngleStepUsed.TabIndex = 86;
            this.checkBoxAngleStepUsed.TabStop = false;
            this.checkBoxAngleStepUsed.Text = "Enable";
            this.toolTip1.SetToolTip(this.checkBoxAngleStepUsed, "Segments used or not");
            this.checkBoxAngleStepUsed.UseVisualStyleBackColor = true;
            // 
            // checkBoxMinPipeSlope
            // 
            this.checkBoxMinPipeSlope.Location = new System.Drawing.Point(367, 62);
            this.checkBoxMinPipeSlope.Name = "checkBoxMinPipeSlope";
            this.checkBoxMinPipeSlope.Size = new System.Drawing.Size(101, 17);
            this.checkBoxMinPipeSlope.TabIndex = 87;
            this.checkBoxMinPipeSlope.TabStop = false;
            this.checkBoxMinPipeSlope.Text = "Min Slope";
            this.checkBoxMinPipeSlope.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxMinPipeSlope.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(468, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Pipe";
            this.label5.Visible = false;
            // 
            // labelSlope
            // 
            this.labelSlope.Location = new System.Drawing.Point(395, 40);
            this.labelSlope.Name = "labelSlope";
            this.labelSlope.Size = new System.Drawing.Size(69, 13);
            this.labelSlope.TabIndex = 6;
            this.labelSlope.Text = "Slope";
            this.labelSlope.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBoxMinPipeSlope
            // 
            this.textBoxMinPipeSlope.Location = new System.Drawing.Point(476, 60);
            this.textBoxMinPipeSlope.Name = "textBoxMinPipeSlope";
            this.textBoxMinPipeSlope.Size = new System.Drawing.Size(42, 20);
            this.textBoxMinPipeSlope.TabIndex = 1;
            this.textBoxMinPipeSlope.Text = "5";
            this.textBoxMinPipeSlope.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxPipeSegmentLength
            // 
            this.textBoxPipeSegmentLength.Location = new System.Drawing.Point(476, 113);
            this.textBoxPipeSegmentLength.Name = "textBoxPipeSegmentLength";
            this.textBoxPipeSegmentLength.Size = new System.Drawing.Size(42, 20);
            this.textBoxPipeSegmentLength.TabIndex = 3;
            this.textBoxPipeSegmentLength.Text = "5";
            this.textBoxPipeSegmentLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelSegmentLength
            // 
            this.labelSegmentLength.Location = new System.Drawing.Point(340, 116);
            this.labelSegmentLength.Name = "labelSegmentLength";
            this.labelSegmentLength.Size = new System.Drawing.Size(129, 13);
            this.labelSegmentLength.TabIndex = 8;
            this.labelSegmentLength.Text = "Segment length";
            this.labelSegmentLength.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(841, 309);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 10;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonDraw
            // 
            this.buttonDraw.BackColor = System.Drawing.SystemColors.Control;
            this.buttonDraw.Location = new System.Drawing.Point(3, 3);
            this.buttonDraw.Name = "buttonDraw";
            this.buttonDraw.Size = new System.Drawing.Size(75, 23);
            this.buttonDraw.TabIndex = 8;
            this.buttonDraw.Text = "Draw";
            this.buttonDraw.UseVisualStyleBackColor = true;
            this.buttonDraw.Click += new System.EventHandler(this.buttonDraw_Click);
            // 
            // buttonFinish
            // 
            this.buttonFinish.Location = new System.Drawing.Point(760, 309);
            this.buttonFinish.Name = "buttonFinish";
            this.buttonFinish.Size = new System.Drawing.Size(75, 23);
            this.buttonFinish.TabIndex = 9;
            this.buttonFinish.Text = "Finish";
            this.buttonFinish.UseVisualStyleBackColor = true;
            this.buttonFinish.Click += new System.EventHandler(this.buttonFinish_Click);
            // 
            // textBoxPipeDiameter
            // 
            this.textBoxPipeDiameter.Location = new System.Drawing.Point(476, 87);
            this.textBoxPipeDiameter.Name = "textBoxPipeDiameter";
            this.textBoxPipeDiameter.Size = new System.Drawing.Size(42, 20);
            this.textBoxPipeDiameter.TabIndex = 2;
            this.textBoxPipeDiameter.Text = "5";
            this.textBoxPipeDiameter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelDiameter
            // 
            this.labelDiameter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDiameter.Location = new System.Drawing.Point(349, 90);
            this.labelDiameter.Name = "labelDiameter";
            this.labelDiameter.Size = new System.Drawing.Size(120, 13);
            this.labelDiameter.TabIndex = 24;
            this.labelDiameter.Text = "Diameter";
            this.labelDiameter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxPipeStartEndSegment
            // 
            this.textBoxPipeStartEndSegment.Location = new System.Drawing.Point(476, 139);
            this.textBoxPipeStartEndSegment.Name = "textBoxPipeStartEndSegment";
            this.textBoxPipeStartEndSegment.Size = new System.Drawing.Size(42, 20);
            this.textBoxPipeStartEndSegment.TabIndex = 4;
            this.textBoxPipeStartEndSegment.Text = "1";
            this.textBoxPipeStartEndSegment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelStartEndSegmentLength
            // 
            this.labelStartEndSegmentLength.Location = new System.Drawing.Point(333, 142);
            this.labelStartEndSegmentLength.Name = "labelStartEndSegmentLength";
            this.labelStartEndSegmentLength.Size = new System.Drawing.Size(136, 13);
            this.labelStartEndSegmentLength.TabIndex = 27;
            this.labelStartEndSegmentLength.Text = "Start/End segment length";
            this.labelStartEndSegmentLength.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(520, 116);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(15, 13);
            this.label42.TabIndex = 57;
            this.label42.Text = "m";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(520, 90);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(23, 13);
            this.label43.TabIndex = 58;
            this.label43.Text = "mm";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(520, 142);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(15, 13);
            this.label44.TabIndex = 59;
            this.label44.Text = "m";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(514, 40);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(15, 13);
            this.label45.TabIndex = 60;
            this.label45.Text = "%";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(361, 491);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(63, 13);
            this.label30.TabIndex = 61;
            this.label30.Text = "Pipe length:";
            // 
            // labelHeightDelta
            // 
            this.labelHeightDelta.Location = new System.Drawing.Point(349, 216);
            this.labelHeightDelta.Name = "labelHeightDelta";
            this.labelHeightDelta.Size = new System.Drawing.Size(121, 13);
            this.labelHeightDelta.TabIndex = 62;
            this.labelHeightDelta.Text = "Height delta:";
            this.labelHeightDelta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPipeLength
            // 
            this.labelPipeLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPipeLength.Location = new System.Drawing.Point(419, 491);
            this.labelPipeLength.Name = "labelPipeLength";
            this.labelPipeLength.Size = new System.Drawing.Size(54, 13);
            this.labelPipeLength.TabIndex = 64;
            this.labelPipeLength.Text = "100.54333";
            this.labelPipeLength.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPipeHeightDelta
            // 
            this.labelPipeHeightDelta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPipeHeightDelta.Location = new System.Drawing.Point(468, 216);
            this.labelPipeHeightDelta.Name = "labelPipeHeightDelta";
            this.labelPipeHeightDelta.Size = new System.Drawing.Size(51, 13);
            this.labelPipeHeightDelta.TabIndex = 65;
            this.labelPipeHeightDelta.Text = "0";
            this.labelPipeHeightDelta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPipeSegments
            // 
            this.labelPipeSegments.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPipeSegments.Location = new System.Drawing.Point(423, 518);
            this.labelPipeSegments.Name = "labelPipeSegments";
            this.labelPipeSegments.Size = new System.Drawing.Size(48, 13);
            this.labelPipeSegments.TabIndex = 66;
            this.labelPipeSegments.Text = "9";
            this.labelPipeSegments.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonSelectEndPoint
            // 
            this.buttonSelectEndPoint.Location = new System.Drawing.Point(841, 228);
            this.buttonSelectEndPoint.Name = "buttonSelectEndPoint";
            this.buttonSelectEndPoint.Size = new System.Drawing.Size(75, 49);
            this.buttonSelectEndPoint.TabIndex = 7;
            this.buttonSelectEndPoint.Text = "End point";
            this.buttonSelectEndPoint.UseVisualStyleBackColor = true;
            this.buttonSelectEndPoint.Click += new System.EventHandler(this.buttonSelectEndPoint_Click);
            // 
            // buttonSettings
            // 
            this.buttonSettings.Location = new System.Drawing.Point(12, 312);
            this.buttonSettings.Name = "buttonSettings";
            this.buttonSettings.Size = new System.Drawing.Size(75, 23);
            this.buttonSettings.TabIndex = 69;
            this.buttonSettings.TabStop = false;
            this.buttonSettings.Text = "Settings";
            this.buttonSettings.UseVisualStyleBackColor = true;
            this.buttonSettings.Click += new System.EventHandler(this.buttonSettings_Click);
            // 
            // panelDrawRequest
            // 
            this.panelDrawRequest.Controls.Add(this.buttonDraw);
            this.panelDrawRequest.Location = new System.Drawing.Point(757, 276);
            this.panelDrawRequest.Name = "panelDrawRequest";
            this.panelDrawRequest.Size = new System.Drawing.Size(81, 29);
            this.panelDrawRequest.TabIndex = 70;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(521, 216);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 74;
            this.label1.Text = "cm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(475, 491);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 13);
            this.label2.TabIndex = 75;
            this.label2.Text = "m";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(475, 468);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 13);
            this.label3.TabIndex = 78;
            this.label3.Text = "m";
            // 
            // labelPipeLength2d
            // 
            this.labelPipeLength2d.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPipeLength2d.Location = new System.Drawing.Point(419, 468);
            this.labelPipeLength2d.Name = "labelPipeLength2d";
            this.labelPipeLength2d.Size = new System.Drawing.Size(54, 13);
            this.labelPipeLength2d.TabIndex = 77;
            this.labelPipeLength2d.Text = "100.54333";
            this.labelPipeLength2d.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(343, 468);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 76;
            this.label8.Text = "Pipe length 2D:";
            // 
            // labelPipeAngleText
            // 
            this.labelPipeAngleText.Location = new System.Drawing.Point(346, 194);
            this.labelPipeAngleText.Name = "labelPipeAngleText";
            this.labelPipeAngleText.Size = new System.Drawing.Size(123, 13);
            this.labelPipeAngleText.TabIndex = 80;
            this.labelPipeAngleText.Text = "Pipe angle:";
            this.labelPipeAngleText.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPipeAngle
            // 
            this.labelPipeAngle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPipeAngle.Location = new System.Drawing.Point(465, 194);
            this.labelPipeAngle.Name = "labelPipeAngle";
            this.labelPipeAngle.Size = new System.Drawing.Size(54, 13);
            this.labelPipeAngle.TabIndex = 81;
            this.labelPipeAngle.Text = "5";
            this.labelPipeAngle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(521, 194);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(11, 13);
            this.label9.TabIndex = 82;
            this.label9.Text = "°";
            // 
            // textBoxAngleStep
            // 
            this.textBoxAngleStep.Location = new System.Drawing.Point(476, 165);
            this.textBoxAngleStep.Name = "textBoxAngleStep";
            this.textBoxAngleStep.Size = new System.Drawing.Size(42, 20);
            this.textBoxAngleStep.TabIndex = 5;
            this.textBoxAngleStep.Text = "5";
            this.textBoxAngleStep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(521, 168);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(11, 13);
            this.label10.TabIndex = 84;
            this.label10.Text = "°";
            // 
            // labelAngleStep
            // 
            this.labelAngleStep.Location = new System.Drawing.Point(343, 168);
            this.labelAngleStep.Name = "labelAngleStep";
            this.labelAngleStep.Size = new System.Drawing.Size(127, 13);
            this.labelAngleStep.TabIndex = 85;
            this.labelAngleStep.Text = "Angle step";
            this.labelAngleStep.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelPipeSlope
            // 
            this.labelPipeSlope.Location = new System.Drawing.Point(468, 38);
            this.labelPipeSlope.Name = "labelPipeSlope";
            this.labelPipeSlope.Size = new System.Drawing.Size(44, 17);
            this.labelPipeSlope.TabIndex = 92;
            this.labelPipeSlope.Text = "5.00";
            this.labelPipeSlope.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(519, 63);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 13);
            this.label12.TabIndex = 93;
            this.label12.Text = "%";
            // 
            // pictureBoxWaterDirection
            // 
            this.pictureBoxWaterDirection.Image = global::CadPlugin.Properties.Resources.pipe_downstream;
            this.pictureBoxWaterDirection.Location = new System.Drawing.Point(380, 244);
            this.pictureBoxWaterDirection.Name = "pictureBoxWaterDirection";
            this.pictureBoxWaterDirection.Size = new System.Drawing.Size(195, 81);
            this.pictureBoxWaterDirection.TabIndex = 94;
            this.pictureBoxWaterDirection.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 470);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(179, 13);
            this.label13.TabIndex = 95;
            this.label13.Text = "Ispod premesteno sve sto sakrivamo";
            // 
            // endShaftView
            // 
            this.endShaftView.Description = "";
            this.endShaftView.Location = new System.Drawing.Point(614, 20);
            this.endShaftView.Name = "endShaftView";
            this.endShaftView.Size = new System.Drawing.Size(322, 199);
            this.endShaftView.TabIndex = 6;
            // 
            // startShaftView
            // 
            this.startShaftView.Description = "";
            this.startShaftView.Location = new System.Drawing.Point(12, 20);
            this.startShaftView.Name = "startShaftView";
            this.startShaftView.Size = new System.Drawing.Size(322, 209);
            this.startShaftView.TabIndex = 0;
            // 
            // totalsView
            // 
            this.totalsView.Location = new System.Drawing.Point(331, 347);
            this.totalsView.Name = "totalsView";
            this.totalsView.Size = new System.Drawing.Size(272, 112);
            this.totalsView.TabIndex = 0;
            // 
            // PipeShaftView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 347);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.pictureBoxWaterDirection);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.labelPipeSlope);
            this.Controls.Add(this.checkBoxAngleStepUsed);
            this.Controls.Add(this.labelAngleStep);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBoxAngleStep);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.labelPipeAngle);
            this.Controls.Add(this.labelPipeAngleText);
            this.Controls.Add(this.checkBoxSegmentsUsed);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelPipeLength2d);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBoxMinPipeSlope);
            this.Controls.Add(this.panelDrawRequest);
            this.Controls.Add(this.buttonSettings);
            this.Controls.Add(this.buttonSelectEndPoint);
            this.Controls.Add(this.endShaftView);
            this.Controls.Add(this.labelPipeSegments);
            this.Controls.Add(this.labelPipeHeightDelta);
            this.Controls.Add(this.labelPipeLength);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.labelHeightDelta);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.labelStartEndSegmentLength);
            this.Controls.Add(this.textBoxPipeStartEndSegment);
            this.Controls.Add(this.textBoxPipeDiameter);
            this.Controls.Add(this.labelDiameter);
            this.Controls.Add(this.buttonFinish);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.textBoxPipeSegmentLength);
            this.Controls.Add(this.labelSegmentLength);
            this.Controls.Add(this.textBoxMinPipeSlope);
            this.Controls.Add(this.labelSlope);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.startShaftView);
            this.Controls.Add(this.totalsView);
            this.KeyPreview = true;
            this.Name = "PipeShaftView";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manage pipe creation";
            this.panelDrawRequest.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWaterDirection)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label labelSlope;
		private System.Windows.Forms.TextBox textBoxMinPipeSlope;
		private System.Windows.Forms.TextBox textBoxPipeSegmentLength;
		private System.Windows.Forms.Label labelSegmentLength;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.Button buttonDraw;
		private System.Windows.Forms.Button buttonFinish;
		private System.Windows.Forms.TextBox textBoxPipeDiameter;
		private System.Windows.Forms.Label labelDiameter;
		private System.Windows.Forms.TextBox textBoxPipeStartEndSegment;
		private System.Windows.Forms.Label labelStartEndSegmentLength;
		private System.Windows.Forms.Label label42;
		private System.Windows.Forms.Label label43;
		private System.Windows.Forms.Label label44;
		private System.Windows.Forms.Label label45;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label labelHeightDelta;
		private System.Windows.Forms.Label label50;
		private System.Windows.Forms.Label labelPipeLength;
		private System.Windows.Forms.Label labelPipeHeightDelta;
		private System.Windows.Forms.Label labelPipeSegments;
		private ShaftControls.ShaftControl startShaftView;
		private ShaftControls.ShaftControl endShaftView;
		private Totals.Totals totalsView;
		private System.Windows.Forms.Button buttonSelectEndPoint;
		private System.Windows.Forms.Button buttonSettings;
		private System.Windows.Forms.Panel panelDrawRequest;
		private System.Windows.Forms.CheckBox checkBoxMinPipeSlope;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label labelPipeLength2d;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.CheckBox checkBoxSegmentsUsed;
		private System.Windows.Forms.Label labelPipeAngleText;
		private System.Windows.Forms.Label labelPipeAngle;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox textBoxAngleStep;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label labelAngleStep;
		private System.Windows.Forms.CheckBox checkBoxAngleStepUsed;
		private System.Windows.Forms.Label labelPipeSlope;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.PictureBox pictureBoxWaterDirection;
		private System.Windows.Forms.Label label13;
	}
}