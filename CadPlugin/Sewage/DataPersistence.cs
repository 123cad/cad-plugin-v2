﻿using CadPlugin.Sewage.Create.Settings;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using CadPlugin.Sewage.Plannings;
using System.Configuration;
using CadPlugin.Sewage.Plannings.Texts;

namespace CadPlugin.Sewage.Persistence
{

    [Serializable]
	public class LabelPersistence
	{
		public string LabelName { get; set; }
		public string DescriptionText { get; set; }
		public string SuffixText { get; set; }
		public ColorPersistence Color { get; set; }
		public double TextSize { get; set; }
		public bool DrawJig { get; set; }
		public bool DrawFinal { get; set; }
		public int Digits { get; set; }
		public int Scale { get; set; }
		public LabelPersistence()
		{
			LabelName = "name";
			DescriptionText = "";
			Color = new ColorPersistence(System.Drawing.Color.Black);
			DrawJig = true;
			DrawFinal = true;
			TextSize = 0.3;
			Digits = 2;
			Scale = (int)DisplayScale.Percent;
		}
	}
	class LabelCollection:ArrayList
	{
	}
	/// <summary>
	/// Saves/Loads data.
	/// </summary>
	class DataPersistence
	{
		public static void LoadLastPipe(PipeData pipe)
		{
			Properties.SewageCreate file = Properties.SewageCreate.Default;
			double s;
			s = file.PipeDiameter;
			//if (!string.IsNullOrEmpty(s))
				pipe.Diameter = s;
			s = file.PipeSegment;
			//if (!string.IsNullOrEmpty(s))
			pipe.SegmentLength = s;
			s = file.PipeSESegment;
			//if (!string.IsNullOrEmpty(s))
			pipe.StartEndSegmentLength = s;

			pipe.SegmentsUsed = file.PipeSegmentsUsed;
			pipe.AngleSteppingUsed = file.ShowAnglePipe;

			s = file.PipeAngle;
			pipe.AngleStepDeg = s;
			
		}
		public static void LoadLastStartShaft(ShaftData shaft)
		{
			Properties.SewageCreate file = Properties.SewageCreate.Default;
			double s;

			s = file.ShaftStartDiameter;
			//if (!string.IsNullOrEmpty(s))
			shaft.Diameter = s;// DoubleHelper.ParseInvariantDouble(s);
			s = file.ShaftStartInnerSlope;
			//if (!string.IsNullOrEmpty(s))
			shaft.InnerSlope = s;// DoubleHelper.ParseInvariantDouble(s);
			s = file.ShaftStartCoverDelta;
			//if (!string.IsNullOrEmpty(s))
			shaft.CoverHeight = shaft.TerrainHeight + s;// DoubleHelper.ParseInvariantDouble(s);
			//if (!string.IsNullOrEmpty(s))
			s = file.ShaftStartPipeOutDelta;
			shaft.PipeOutHeight = shaft.TerrainHeight - s;
			s = file.ShaftStartBottomDelta;
			shaft.BottomHeight = shaft.PipeOutHeight - s;// DoubleHelper.ParseInvariantDouble(s);
			s = file.ShaftStartMinDelta;
			shaft.MinPipeOutDelta = s;
		}
		public static void LoadLastEndShaft(ShaftData shaft)
		{
			Properties.SewageCreate file = Properties.SewageCreate.Default;
			double s;
			s = file.ShaftEndDiameter;
			//if (!string.IsNullOrEmpty(s))
			shaft.Diameter = s;// DoubleHelper.ParseInvariantDouble(s);
			s = file.ShaftEndInnerSlope;
			//if (!string.IsNullOrEmpty(s))
			shaft.InnerSlope = s;// DoubleHelper.ParseInvariantDouble(s);
			s = file.ShaftEndCoverDelta;
			//if (!string.IsNullOrEmpty(s))
			shaft.CoverHeight = shaft.TerrainHeight + s;// DoubleHelper.ParseInvariantDouble(s);
			s = file.ShaftEndBottomDelta;
			//if (!string.IsNullOrEmpty(s))
			shaft.BottomHeight = shaft.PipeOutHeight - s;// DoubleHelper.ParseInvariantDouble(s);
			s = file.ShaftEndMinDelta;
			shaft.MinPipeOutDelta = s;
			shaft.MinPipeOutDeltaUsed = file.ShaftEndMinDeltaUsed;
		}
		public static void LoadLastData(PipeShaftData data)
		{
			LoadLastPipe(data.PipeData);
			if (data.StartShaftData != null)
				LoadLastStartShaft(data.StartShaftData);
			if (data.EndShaftData != null)
				LoadLastEndShaft(data.EndShaftData);
			LoadLastMinValues(data);

		}
		public static void LoadLastMinValues (PipeShaftData data)
		{
			Properties.SewageCreate file = Properties.SewageCreate.Default;
			double s;

			s = file.PipeMinSlope;
			//if (!string.IsNullOrEmpty(s))
			if (s != 0)
			{
				data.ApplyMinPipeSlope = true;
				data.MinPipeSlope = s;// DoubleHelper.ParseInvariantDouble(s);
			}
			else
				data.ApplyMinPipeSlope = false;

			s = file.ShaftEndMinDelta;
			//if (!string.IsNullOrEmpty(s))
			if (data.EndShaft != null)
			{
				if (s != 0)
				{
					((ShaftData)data.EndShaft).MinPipeOutDeltaUsed = true;
					((ShaftData)data.EndShaft).MinPipeOutDelta = s;// DoubleHelper.ParseInvariantDouble(s);
				}
				else
					((ShaftData)data.EndShaft).MinPipeOutDeltaUsed = false;
			}
		}
		/// <summary>
		/// Saves provided data to config file (PipeShaftData object is used since all data
		/// inside is validated. Loading operation is done directly in constructor of 
		/// Pipe/Shaft Data).
		/// </summary>
		public static void SaveData(PipeShaftData data, bool startShaft, bool pipe, bool endShaft)
		{
			Properties.SewageCreate file = Properties.SewageCreate.Default;
			Func<double, double> convert = (d) =>
			{
				//return DoubleHelper.ToStringInvariant(d, 5);
				return d;
			};

			double s;
			file.PipeMinSlope = data.MinPipeSlope;
			if (pipe)
			{
				s = data.ApplyMinPipeSlope ? convert(data.MinPipeSlope) : 0;
				file.PipeMinSlope = s;
				s = convert(data.Pipe.Diameter);
				file.PipeDiameter = s;
				// Segment data is saved in any way. But is it used is defined with SegmentsUsed.
				s = convert(data.Pipe.SegmentLength);// data.Pipe.SegmentsUsed ? convert(data.Pipe.SegmentLength) : 0;
				file.PipeSegment= s;
				s = convert(data.Pipe.StartEndSegmentLength);// data.Pipe.SegmentsUsed ? convert(data.Pipe.StartEndSegmentLength) : 0;
				file.PipeSESegment = s;
				s = convert(data.Pipe.AngleStepDeg);// data.Pipe.AngleSteppingUsed ? convert(data.Pipe.AngleStepDeg) : 0;
				file.PipeAngle = s;
				file.PipeSegmentsUsed = data.Pipe.SegmentsUsed;
				file.ShowAnglePipe = data.Pipe.AngleSteppingUsed;

			}
			if (startShaft && data.StartShaftData != null)
			{
				s = convert(data.StartShaft.Diameter);
				file.ShaftStartDiameter = s;
				s = convert(data.StartShaft.InnerSlope);
				file.ShaftStartInnerSlope = s;
				s = convert(data.StartShaft.CoverHeight - data.StartShaft.TerrainHeight);
				file.ShaftStartCoverDelta = s;
				s = convert(data.StartShaft.TerrainHeight - data.StartShaft.PipeOutHeight);
				file.ShaftStartPipeOutDelta = s;
				s = convert(data.StartShaft.PipeOutHeight - data.StartShaft.BottomHeight);
				file.ShaftStartBottomDelta = s;
				s = data.StartShaft.MinPipeOutDelta;
				file.ShaftStartMinDelta = s;
				// MinDelta is used always for start shaft.
			}
			if (endShaft && data.EndShaftData != null)
			{
				s = convert(data.EndShaft.Diameter);
				file.ShaftEndDiameter = s;
				s = convert(data.EndShaft.InnerSlope);
				file.ShaftEndInnerSlope = s;
				s = convert(data.EndShaft.CoverHeight - data.EndShaft.TerrainHeight);
				file.ShaftEndCoverDelta = s;
				//s = convert(data.EndShaft.TerrainHeight - data.EndShaft.PipeOutHeight);
				//file.shaft = s;
				s = convert(data.EndShaft.PipeOutHeight - data.EndShaft.BottomHeight);
				file.ShaftEndBottomDelta= s;
				s = data.ApplyEndShaftPipeOutDelta ? convert(data.EndShaft.MinPipeOutDelta) : 0;
				file.ShaftEndMinDelta = s;
				s = convert(data.EndShaft.MinPipeOutDelta);
				file.ShaftEndMinDelta = s;
				file.ShaftEndMinDeltaUsed = data.EndShaft.MinPipeOutDeltaUsed;
			}
			file.Save();
		}
		public static void SaveLabels(TextDisplayOrdered collection)
		{
			Properties.SewageCreate file = Properties.SewageCreate.Default;
			List<LabelPersistence> pipeLabels = new List<LabelPersistence>();
			List<LabelPersistence> shaftLabels = new List<LabelPersistence>();
			foreach (TextDisplaySingle t in collection.PipeDisplayOrder)
			{
				LabelPersistence l = new LabelPersistence()
				{
					Color = new ColorPersistence(t.TextColor),
					LabelName = t.Name,
					DescriptionText = t.Description,
					TextSize = t.TextSize,
					DrawJig = t.DrawInJig,
					DrawFinal = t.DrawAsFinal,
					SuffixText = t.Suffix
				};
				
				if (t is ITextDecimals)
					l.Digits = ((ITextDecimals)t).Digits;
				if (t is IDisplayScaled)
					l.Scale = (int)(((IDisplayScaled)t).Scale);
				pipeLabels.Add(l);
			}
			file.PipeLabels = pipeLabels;
			foreach (TextDisplaySingle t in collection.ShaftDisplayOrder)
			{
				LabelPersistence l = new LabelPersistence()
				{
					Color = new ColorPersistence(t.TextColor),
					LabelName = t.Name,
					DescriptionText = t.Description,
					TextSize = t.TextSize,
					DrawJig = t.DrawInJig,
					DrawFinal = t.DrawAsFinal,
					SuffixText = t.Suffix
				};
				if (t is ITextDecimals)
					l.Digits = ((ITextDecimals)t).Digits;
				if (t is IDisplayScaled)
					l.Scale = (int)(((IDisplayScaled)t).Scale);
				shaftLabels.Add(l);
			}
			file.ShaftLabels = shaftLabels;

			file.PipeArrowColor = new ColorPersistence(Sewage.Create.AdditionalSettings.AdditionalSettings.Instance.ArrowColor);
			file.DrawPipeDirectionArrow = Sewage.Create.AdditionalSettings.AdditionalSettings.Instance.DrawArrowOnPipe;
			file.DrawPipeDirectionArrowText = Sewage.Create.AdditionalSettings.AdditionalSettings.Instance.DrawArrowInText;
			file.ShowAnglePipe = Sewage.Create.AdditionalSettings.AdditionalSettings.Instance.ShowAngle;


			file.Save();
		}
		

		public static void LoadLabels(TextDisplayOrdered collection)
		{
			collection.PipeDisplayOrder.Clear();
			Properties.SewageCreate file = Properties.SewageCreate.Default;
			if (file.PipeLabels != null)
				foreach (LabelPersistence l in file.PipeLabels)
				{
					TextDisplaySingle display = TextFactory.Instance.GetPipe(l.LabelName);
					if (display == null)
						continue;
					display.Description = l.DescriptionText;
					display.Suffix = l.SuffixText;
					display.TextColor = l.Color.ToColor();
					display.TextSize = l.TextSize;
					display.DrawInJig = l.DrawJig;
					display.DrawAsFinal = l.DrawFinal;

					if (display is ITextDecimals)
						((ITextDecimals)display).Digits = l.Digits;
					if (display is IDisplayScaled)
						((IDisplayScaled)display).Scale = (DisplayScale)l.Scale;

					collection.PipeDisplayOrder.Add(display);
				}
			if (file.ShaftLabels != null)
				foreach (LabelPersistence l in file.ShaftLabels)
				{
					TextDisplaySingle display = TextFactory.Instance.GetShaft(l.LabelName);
					if (display == null)
						continue;
					display.Description = l.DescriptionText;
					display.Suffix = l.SuffixText;
					display.TextColor = l.Color.ToColor();
					display.TextSize = l.TextSize;
					display.DrawInJig = l.DrawJig;
					display.DrawAsFinal = l.DrawFinal;
					if (display is ITextDecimals)
						((ITextDecimals)display).Digits = l.Digits;
					if (display is IDisplayScaled)
						((IDisplayScaled)display).Scale = (DisplayScale)l.Scale;
					collection.ShaftDisplayOrder.Add(display);
				}
			if (file.PipeArrowColor != null)
				Sewage.Create.AdditionalSettings.AdditionalSettings.Instance.ArrowColor = file.PipeArrowColor.ToColor();
			Sewage.Create.AdditionalSettings.AdditionalSettings.Instance.ShowAngle = file.ShowAnglePipe;

			Sewage.Create.AdditionalSettings.AdditionalSettings.Instance.DrawArrowOnPipe = file.DrawPipeDirectionArrow;
			Sewage.Create.AdditionalSettings.AdditionalSettings.Instance.DrawArrowInText = file.DrawPipeDirectionArrowText;
		}
	}
}
