﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyUtilities.Geometry;
using CadPlugin.Sewage.Persistence;

namespace CadPlugin.Sewage.Create.Settings
{
	//NOTE Used to limit access to setting the data - that can be done only by manager.

	public interface IShaftData
	{
		/// <summary>
		/// Diameter of the shaft, in meters.
		/// </summary>
		double Diameter { get; }
		/// <summary>
		/// Radius of the shaft, in meters.
		/// </summary>
		double Radius { get; }

		/// <summary>
		/// Inner slope as tangens.
		/// </summary>
		double InnerSlope { get; }
		/// <summary>
		/// Minimum delta h from pipe in to pipe out (meters).
		/// </summary>
		double InnerSlopeDeltaH { get; }

		double TerrainHeight { get; }

		double CoverHeight { get; }
		
		double PipeOutHeight { get; }
		double BottomHeight { get; }

		/// <summary>
		/// Minimum distance from terrain height to pipe out height.
		/// </summary>
		double MinPipeOutDelta { get; }
		bool MinPipeOutDeltaUsed { get; }
		
		Point2d Center { get; }
		Point3d CenterTerrainPoint { get; }
		Point3d CenterPipeOutPoint { get; }
		Point3d CenterPipeInPoint { get; }
	}
	public class ShaftData:IShaftData
	{
		private double __Diameter = 0;
		private double __Radius = 0;
		private double __InnerSlope = 0;
		private double __TerrainHeight = 0;
		private double __CoverHeight = 0;
		private double __PipeOutHeight;
		private double __BottomHeight;
		private double __MinPipeOutDelta = 0;
		private Point2d __Center;
		private Point3d? __CenterTerrainPoint;
		private Point3d? __CenterPipeOutPoint;
		private Point3d? __CenterPipeInPoint;

		/// <summary>
		/// Diameter of the shaft, in meters.
		/// </summary>
		public double Diameter
		{
			get { return __Diameter; }
			set { __Diameter = value; __Radius = value / 2; UpdateInnerSlopeDeltaH(); }
		}
		/// <summary>
		/// Radius of the shaft, in meters.
		/// </summary>
		public double Radius
		{
			get { return __Radius; }
			set { __Radius = value; __Diameter = value * 2; UpdateInnerSlopeDeltaH(); }
		}

		/// <summary>
		/// Inner slope as tangens.
		/// </summary>
		public double InnerSlope
		{
			get { return __InnerSlope; }
			set { __InnerSlope = value; UpdateInnerSlopeDeltaH(); }
		}
		/// <summary>
		/// Minimum delta h from pipe in to pipe out (meters).
		/// </summary>
		public double InnerSlopeDeltaH
		{
			get;private set;
		}

		public double TerrainHeight
		{
			get { return __TerrainHeight; }
			set { __TerrainHeight = value;  CancelPoints(); }
		}

		public double CoverHeight
		{
			get { return __CoverHeight; }
			set { __CoverHeight = value;  CancelPoints(); }
		}
		/// <summary>
		/// Pipe out height is updated (parameter is new pipe out height).
		/// </summary>
		internal event Action<double> PipeOutHeightUpdate;

		public double PipeOutHeight
		{
			get { return __PipeOutHeight; }
			set
			{
				__PipeOutHeight = value;
				CancelPoints();
				if (PipeOutHeightUpdate != null)
					PipeOutHeightUpdate.Invoke(value);
			}
		}
		public double BottomHeight
		{
			get { return __BottomHeight; }
			set { __BottomHeight = value; CancelPoints(); }
		}
		public double MinPipeOutDelta
		{
			get
			{
				return __MinPipeOutDelta;
			}
			set
			{
				__MinPipeOutDelta = value;
			}
		}
		public bool MinPipeOutDeltaUsed { get; set; }


		public Point2d Center
		{
			get { return __Center; }
			set { __Center = value; CancelPoints(); }
		}
		public Point3d CenterTerrainPoint
		{
			get
			{
				if (!__CenterTerrainPoint.HasValue)
					__CenterTerrainPoint = new Point3d(Center.X, Center.Y, TerrainHeight);
				return __CenterTerrainPoint.Value;
			}
		}
		public Point3d CenterPipeOutPoint
		{
			get
			{
				if (!__CenterPipeOutPoint.HasValue)
					__CenterPipeOutPoint = new Point3d(Center.X, Center.Y, PipeOutHeight);
				return __CenterPipeOutPoint.Value;
			}
		}
		public Point3d CenterPipeInPoint
		{
			get
			{
				if (!__CenterPipeInPoint.HasValue)
					__CenterPipeInPoint = new Point3d(Center.X, Center.Y, PipeOutHeight + InnerSlopeDeltaH);
				return __CenterPipeInPoint.Value;
			}
		}

		public ShaftData()
		{
			Diameter = 1;
			InnerSlope = 0.025;
			MinPipeOutDeltaUsed = false;
			MinPipeOutDelta = 0;
		}
		
		private void UpdateInnerSlopeDeltaH()
		{
			InnerSlopeDeltaH = Diameter * InnerSlope;

		}
		/// <summary>
		/// When data is updated, points have to be recalculated.
		/// </summary>
		private void CancelPoints()
		{
			// Introduced because of performance reasons - point will be calculated only when is needed.
			__CenterPipeOutPoint = null;
			__CenterTerrainPoint = null;
			__CenterPipeInPoint = null;
		}
		public ShaftData Clone()
		{
			ShaftData d = new ShaftData();
			d.Diameter = Diameter;
			d.InnerSlope = InnerSlope;
			d.TerrainHeight = TerrainHeight;
			d.CoverHeight = CoverHeight;
			d.BottomHeight = BottomHeight;
			d.__PipeOutHeight = __PipeOutHeight;
			d.Center = Center;
			d.MinPipeOutDelta = MinPipeOutDelta;
			d.MinPipeOutDeltaUsed = MinPipeOutDeltaUsed;

			return d;
		}
	}
}

