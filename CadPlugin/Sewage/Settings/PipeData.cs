﻿using CadPlugin.Inventories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyUtilities.Geometry;
using CadPlugin.Sewage.Persistence;
using CadPlugin.KHNeu.Models;

namespace CadPlugin.Sewage.Create.Settings
{
    public interface IPipeData
	{

		double Diameter { get; }

		double SegmentLength { get; }
		double StartEndSegmentLength { get; }

		double Slope { get; }

		int SegmentCount { get; }

		bool SegmentsUsed { get; }

		/// <summary>
		/// Pipe angle (to previous pipe direction) must be step of this value.
		/// </summary>
		double AngleStepDeg { get; }

		/// <summary>
		/// Indicates if angle stepping is used (angle from previous pipe direction.
		/// </summary>
		bool AngleSteppingUsed { get; }

		Point3d StartPoint { get; }

		Point3d EndPoint { get; }

		Vector3d Direction { get; }
	}
	public class PipeData:IPipeData
	{ 
		public virtual double Diameter
		{
			get;
			set;
		}

		public virtual double SegmentLength
		{
			get;
			set;
		}
		public double StartEndSegmentLength
		{
			get;set;
		}

		public virtual double Slope
		{
			get;
			set;
		}

		/// <summary>
		/// Number of segments used, NOT including start and end segment.
		/// </summary>
		public virtual int SegmentCount
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets if segments are used at all.
		/// </summary>
		public virtual bool SegmentsUsed
		{
			get;
			set;
		}

		/// <summary>
		/// Pipe angle (to previous pipe direction) must be step of this value.
		/// </summary>
		public double AngleStepDeg { get; set; }

		/// <summary>
		/// Indicates if angle stepping is used (angle from previous pipe direction.
		/// </summary>
		public bool AngleSteppingUsed { get; set; }

		public virtual Point3d StartPoint
		{
			get;
			set;
		}

		public virtual Point3d EndPoint
		{
			get;
			set;
		}

		public virtual Vector3d Direction
		{
			get;
			set;
		}
		public PipeData()
		{
			Diameter = 0.4;
			Slope = 0.05;
			SegmentLength = 1;
			SegmentsUsed = true;
			AngleSteppingUsed = false;
			AngleStepDeg = 5;
			DataPersistence.LoadLastPipe(this);
		}

		public PipeData Clone()
		{
			PipeData p = new PipeData();
			p.Diameter = Diameter;
			p.Slope = Slope;
			p.SegmentLength = SegmentLength;
			p.SegmentCount = SegmentCount;
			p.SegmentsUsed = SegmentsUsed;
			p.AngleStepDeg = AngleStepDeg;
			p.AngleSteppingUsed = AngleSteppingUsed;
			p.StartEndSegmentLength = StartEndSegmentLength;
			p.StartPoint = StartPoint;
			p.EndPoint = EndPoint;
			p.Direction = Direction;

			return p;
		}
				
	}
}

