﻿using CadPlugin.Inventories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CadPlugin.Sewage.Create.Settings
{
	public class PipeTextDisplay
	{ 
		public virtual bool Slope
		{
			get;
			set;
		}

		public virtual bool SegmentCount
		{
			get;
			set;
		}

		public virtual bool Length
		{
			get;
			set;
		}

		public virtual bool OutHeight
		{
			get;
			set;
		}

		public virtual bool InHeight
		{
			get;
			set;
		}

		public virtual bool Diameter
		{
			get;
			set;
		}

		
	}
}

