﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CadPlugin.Sewage.Create.Settings
{
	public class ShaftTextDisplay
	{ 
		public virtual bool TerrainHeight
		{
			get;
			set;
		}

		public virtual bool PipeOutHeight
		{
			get;
			set;
		}

		public virtual bool TerrainHeightToBottom
		{
			get;
			set;
		}

		public virtual bool TerrainHeightToPipeOut
		{
			get;
			set;
		}

		
	}
}

