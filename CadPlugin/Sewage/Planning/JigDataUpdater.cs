﻿using CadPlugin.PipeEntitiesJigs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Sewage.Plannings
{
	/// <remarks>Invokes recalculation of data, end when finished updates points on jig data.</remarks>
	public class JigDataUpdater
	{
		public virtual event Action DataUpdated;


		public virtual JigDataProvider JigDataProvider
		{
			get;
			private set;
		}

		public virtual PipeShaftDataManager PipeShaftDataManager
		{
			get;
			private set;
		}
		public JigDataUpdater(JigDataProvider data, PipeShaftDataManager manager)
		{
			PipeShaftDataManager = manager;
			JigDataProvider = data;
			data.UpdateData += Data_UpdateData;
		}

		private void Data_UpdateData(Point3d obj)
		{
			PipeShaftDataManager.SetVariablePoint(obj.FromCADPoint());
			PipeShaftDataManager.Recalculate();
			JigDataProvider.StartPoint = PipeShaftDataManager.Data.StartPoint.ToCADPoint();
			JigDataProvider.LineStartPoint = PipeShaftDataManager.Data.Pipe.StartPoint.ToCADPoint();
			JigDataProvider.EndPoint = PipeShaftDataManager.Data.EndPoint.ToCADPoint();
			JigDataProvider.LineEndPoint = PipeShaftDataManager.Data.Pipe.EndPoint.ToCADPoint();
			OnDataUpdated();
		}

		private void OnDataUpdated()
		{
			if (DataUpdated != null)
				DataUpdated.Invoke();
		}
		
	}
}

