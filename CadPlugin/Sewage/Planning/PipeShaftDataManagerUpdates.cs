﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CadPlugin.Sewage.Create.Settings;
using MyUtilities.Geometry;

namespace CadPlugin.Sewage.Plannings
{
	/// <summary>
	/// Supported fields for shaft data to be updated.
	/// </summary>
	public enum ShaftFields
	{
		Diameter,
		InnerSlope,
		Cover, 
		Terrain, 
		PipeOut, 
		Bottom,
		CoverTerrainDelta,
		TerrainPipeOutDelta,
		PipeOutBottomDelta,
	}
	/// <summary>
	/// Supported pipe fields to be updated.
	/// </summary>
	public enum PipeFields
	{
		Diameter,
		MinSlope,
		SegmentLength, 
		StartEndSegmentLength, 
		SegmentsUsed,
		StepAngle,
		StepAngleUsed,
		Slope,
		MinDelta
	}
	public partial class PipeShaftDataManager
	{
		/// <remarks>
		/// Updates shaft value, with adjusting other values:
		/// - new cover - sets new cover height
		/// - new coverTerrainDelta - updates cover height to be delta from terrain
		/// - new terrain - updates all values, preserving relative distance (delta)
		/// - new terrainPipeOutDelta - same as pipeOut
		/// - new pipeOut - updates pipe out, with moving bottom to preserve delta
		/// - new pipeOutBottomDelta - same as bottom
		/// - new bottom - updates bottom height.
		/// </remarks>
		/// <summary>Updates shaft value, with adjusting other values</summary>
		/// <param name="value">If value is delta, it should always be positive.</param>
		/// <param name="type"></param>
		/// <see cref="PipeFields"/>
		public void UpdateStartShaft(double value, ShaftFields type)
		{
			ShaftData data = __Data.StartShaftData;
			UpdateShaft(data, value, type);
		}
		public void UpdateEndShaft(double value, ShaftFields type)
		{
			ShaftData data = __Data.EndShaftData;
			UpdateShaft(data, value, type);
		}
		private void UpdateShaft(ShaftData data, double value, ShaftFields type)
		{
			double terrainHeight = data.TerrainHeight;
			double delta = 0;
			switch (type)
			{
				case ShaftFields.Diameter:
					data.Diameter = value;
					break;
				case ShaftFields.InnerSlope:
					data.InnerSlope = value;
					break;
				case ShaftFields.Cover:
					data.CoverHeight = value;
					break;
				case ShaftFields.CoverTerrainDelta:
					data.CoverHeight = terrainHeight + value;
					break;
				case ShaftFields.Terrain:
					// All values are moved same amount as terrain is moved.
					delta = value - terrainHeight;
					data.CoverHeight += delta;
					data.TerrainHeight += delta;
					data.PipeOutHeight += delta;
					data.BottomHeight += delta;
					break;
				case ShaftFields.TerrainPipeOutDelta:
					delta = data.PipeOutHeight - data.BottomHeight;
					data.PipeOutHeight = terrainHeight - value;
					data.BottomHeight = data.PipeOutHeight - delta;
					break;
				case ShaftFields.PipeOut:
					delta = value - data.PipeOutHeight;
					data.PipeOutHeight = value;
					data.BottomHeight += delta;
					break;
				case ShaftFields.PipeOutBottomDelta:
					data.BottomHeight = data.PipeOutHeight - value;
					break;
				case ShaftFields.Bottom:
					data.BottomHeight = value;
					break;
			}
		}
		public void UpdatePipe(double value, PipeFields type)
		{
			PipeData data = __Data.PipeData;
			switch (type)
			{
				case PipeFields.Diameter:
					data.Diameter = value;
					break;
				case PipeFields.SegmentLength:
					data.SegmentLength = value;
					break;
				case PipeFields.MinSlope:
					__Data.MinPipeSlope = value;
					data.Slope = value;
					break;
				case PipeFields.StartEndSegmentLength:
					data.StartEndSegmentLength = value;
					break;
				case PipeFields.StepAngle:
					data.AngleStepDeg = value;
					break;
			}
		}
	}
}

