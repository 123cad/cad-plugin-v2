﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Sewage.Create.Settings;
using CadPlugin.Sewage.Plannings;
using CadPlugin.Sewage.Create.AdditionalSettings;
using MyGeometry = MyUtilities.Geometry;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Teigha.Colors;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Sewage.Planning
{
	class DrawTextManager
	{

		/// <summary>
		/// Draws final text (for pipe).
		/// </summary>
		internal static List<Entity> DrawPipeText(Database db, Transaction tr, BlockTableRecord modelSpace, PipeShaftDataManager mgr, TextDisplayOrdered display)
		{

			Common.CADLayerHelper.AddLayerIfDoesntExist(db, Create.CreatePipe.ShaftLabelLayer);
			Common.CADLayerHelper.AddLayerIfDoesntExist(db, Create.CreatePipe.PipeLabelLayer);
			List<Entity> entities = new List<Entity>();

			DisplaySelectedData selectedData = DisplaySelectedData.CreateWithEndShaft(mgr);

			//CreateShaftLabels(tr, modelSpace, selectedData, mgr.Data.EndPoint.ToCADPoint(), display, entities);
			
			CreatePipeLabels(tr, modelSpace, selectedData, display, entities);
			return entities;
		}
		/// <summary>
		/// Draws final text (for end shaft).
		/// </summary>
		/// <param name="db"></param>
		/// <param name="tr"></param>
		/// <param name="modelSpace"></param>
		/// <param name="mgr"></param>
		/// <param name="display"></param>
		/// <returns></returns>
		internal static List<Entity> DrawEndShaftText(Database db, Transaction tr, BlockTableRecord modelSpace, PipeShaftDataManager mgr, TextDisplayOrdered display)
		{
			Common.CADLayerHelper.AddLayerIfDoesntExist(db, Create.CreatePipe.ShaftLabelLayer);
			List<Entity> entities = new List<Entity>();

			DisplaySelectedData selectedData = new DisplaySelectedData()
			{
				ShaftData = mgr.Data.EndShaft
			};
			Point3d p = mgr.Data.EndPoint.ToCADPoint();
			if (mgr.Data.EndShaft != null)
				p = p.FromCADPoint().GetAs2d().GetWithHeight(mgr.Data.EndShaft.TerrainHeight).ToCADPoint();
			CreateShaftLabels(tr, modelSpace, selectedData, p, display, entities);

			return entities;
		}
		/// <summary>
		/// Draws data for start shaft.
		/// </summary>
		internal static List<Entity> DrawStartShaftText(Database db, Transaction tr, BlockTableRecord modelSpace, PipeShaftDataManager mgr, TextDisplayOrdered display)
		{
			Common.CADLayerHelper.AddLayerIfDoesntExist(db, Create.CreatePipe.ShaftLabelLayer);
			List<Entity> entities = new List<Entity>();
			DisplaySelectedData selectedData = new DisplaySelectedData()
			{
				ShaftData = mgr.Data.StartShaft
			};

			Point3d p = mgr.Data.StartPoint.ToCADPoint();
			if (mgr.Data.StartShaft != null)
				p = p.FromCADPoint().GetAs2d().GetWithHeight(mgr.Data.StartShaft.TerrainHeight).ToCADPoint();
			CreateShaftLabels(tr, modelSpace, selectedData, p, display, entities);

			return entities;
		}
		private static void CreateShaftLabels(Transaction tr, BlockTableRecord modelSpace, DisplaySelectedData displayData, Point3d center, TextDisplayOrdered display, List<Entity> entities)
		{
			Point3d start = center;
			double currentDistance = (displayData.ShaftData != null ? -displayData.ShaftData.Radius : -1.0);
			foreach (TextDisplaySingle single in display.ShaftDisplayOrder)
			{
				if (!single.TextExists(displayData))
					continue;
				if (!single.DrawAsFinal)
					continue;
				string text = single.GetText(displayData);
				MText mText = new MText();
				modelSpace.AppendEntityCurrentLayer(mText);
				tr.AddNewlyCreatedDBObject(mText, true);
				mText.Layer = Create.CreatePipe.ShaftLabelLayer;

				mText.Color = Color.FromColor(single.TextColor);
				mText.TextHeight = single.TextSize;
				mText.Location = start.Add(new Vector3d(0, currentDistance, 0));
				mText.Contents = text;
				currentDistance -= single.TextSize * 1.5;

				entities.Add(mText);
			}
		}

		private static void CreatePipeLabels(Transaction tr, BlockTableRecord modelSpace, DisplaySelectedData displayData, TextDisplayOrdered display, List<Entity> entities)
		{
			// Draw pipes labels and pipe arrow direction.

			// First calculate single line. If it doesn't fit, split to multiple lines.
			// Every single line has its own max text height, which determines height for that line.
			// MText has reference point on the upper left side. Everything is calculated from (0,0) point, 
			// moves first line of text above pipe, and then aligned with the pipe (one line is above the pipe).



			IPipeData data = displayData.PipeData;

			// Text object, text drawing object, position.
			Tuple<TextDisplaySingle, MText>[] texts = new Tuple<TextDisplaySingle, MText>[display.PipeDisplayOrder.Count];
			MyUtilities.Geometry.Vector3d pipeVector = data.StartPoint.GetVectorTo(data.EndPoint);
			double length2d = new Vector2d(pipeVector.X, pipeVector.Y).Length;
			// Allowed length of the pipe to contain text.
			double allowedLength2d = 0.75 * length2d;
			double currentXdistance = 0;
			double currentLineHeight = 0;
			double firstLineHeight = 0;
			double firstLineWidth = 0;

			TextDisplaySingle pipeOutDisplay = null;
			TextDisplaySingle pipeInDisplay = null;

			int createdTextCount = 0;
			// Calculate single line
			for (int i = 0; i < display.PipeDisplayOrder.Count; i++)
			{
				TextDisplaySingle single = display.PipeDisplayOrder[i];
				if (!single.TextExists(displayData))
					continue;
				if (single is Plannings.Texts.Pipes.PipeOut)
				{
					pipeOutDisplay = single;
					continue;
				}
				if (single is Plannings.Texts.Pipes.PipeIn)
				{
					pipeInDisplay = single;
					continue;
				}
				if (!single.DrawAsFinal)
					continue;
				MText text = new MText();
				modelSpace.AppendEntityCurrentLayer(text);
				tr.AddNewlyCreatedDBObject(text, true);
				text.Layer = Create.CreatePipe.PipeLabelLayer;
				text.Color = Color.FromColor(single.TextColor);
				text.TextHeight = single.TextSize;
				text.Contents = single.GetText(displayData);
				text.Location = new Point3d(currentXdistance, 0, 0);
				entities.Add(text);
				createdTextCount++;
				texts[i] = new Tuple<TextDisplaySingle, MText>(single, text);

				if (single.TextSize > currentLineHeight)
					currentLineHeight = single.TextSize;
				currentXdistance += text.GeometricExtents.MaxPoint.X - text.GeometricExtents.MinPoint.X + single.TextSize;
			}
			if (createdTextCount == 0)
				return;
			firstLineHeight = currentLineHeight;
			firstLineWidth = currentXdistance;
			List<List<MText>> lines = new List<List<MText>>();
			// If single line does not fit, divide it to multiple lines.
			if (currentXdistance > allowedLength2d)
			{
				// Split into multiple lines.
				currentXdistance = 0;
				currentLineHeight = 0;
				double currentLineY = 0;
				// Lines of texts.
				List<MText> currentLineTexts = new List<MText>();
				for (int i = 0; i < texts.Length; i++)
				{
					var t = texts[i];
					//NOTE Test this! Same reason like lines.Add(texts.Where(x => x != null).Select(x => (MText)x.Item2).ToList());
					if (t == null)
						continue;
					MText text = t.Item2;
					TextDisplaySingle single = t.Item1;
					double currentTextWidth = text.GeometricExtents.MaxPoint.X - text.GeometricExtents.MinPoint.X;
					if (currentLineTexts.Count > 0 && currentXdistance + currentTextWidth > allowedLength2d)
					{
						if (lines.Count == 0)
						{
							firstLineHeight = currentLineHeight;
							firstLineWidth = currentXdistance;
						}
						// Move to the new line.
						currentLineY -= currentLineHeight * 1.3;// Add some space between lines.
						currentXdistance = 0;
						currentLineHeight = 0;
						lines.Add(currentLineTexts);
						currentLineTexts = new List<MText>();
					}
					currentLineTexts.Add(text);
					text.Location = new Point3d(currentXdistance, currentLineY, 0);
					currentXdistance += currentTextWidth + single.TextSize;
					if (single.TextSize > currentLineHeight)
						currentLineHeight = single.TextSize;
				}
				if (currentLineTexts.Count > 0)
					lines.Add(currentLineTexts);
			}
			else
			// Needed where because we have PipeIn and PipeOut which are not put into regular array.
			{
				lines.Add(texts.Where(x => x != null).Select(x => (MText)x.Item2).ToList());
			}

			{
				// First line can have different text heights. Since text reference point is upper left,
				// all texts will be aligned to the top line.
				// So here align them to the bottom line.
				if (lines.Count > 0 && lines[0].Count > 0)
				{
					foreach(MText text in lines[0])
					{

						Matrix3d adjust = Matrix3d.Displacement(new Vector3d(0, -(firstLineHeight - text.TextHeight), 0));
						text.TransformBy(adjust);
					}
				}
			}

			// All texts are positioned now (relative to (0,0,0) point) with angle 0.
			// Move first text line above pipe, rotate accordingly, and set new point (start point of pipe).
			double angleDeg = new MyUtilities.Geometry.Vector3d(1, 0, 0).GetAngleDegXY(data.StartPoint.GetVectorTo(data.EndPoint));
			// Text shouldn't be inverted.
			bool invertText = angleDeg < -90.0 || 90.0 < angleDeg;
			double textPositionX = length2d / 2 - firstLineWidth / 2;
			if (invertText)
			{
				// Keep angle between -90 and 90.
				angleDeg = angleDeg > 0 ? angleDeg - 180 : angleDeg + 180;
				textPositionX = -length2d / 2 - firstLineWidth / 2;
			}

			// Distance of text from pipe (dependent on diameter). 
			double distance = data.Diameter / 2 + 0.1 * firstLineHeight;
			CADMTextHelper.MTextParameters pars = new CADMTextHelper.MTextParameters()
			{
				TextDelta = distance,
				RotateToKeepHorizont = true,
				RotationType = CADMTextHelper.HorizontRotation.TextMiddlePointOnLine,
				TextAlignment= CADMTextHelper.TextAlignment.BottomMiddle
			};

			Matrix3d firstLinePosition = Matrix3d.Displacement(new Vector3d(textPositionX, firstLineHeight + distance, 0));
			firstLinePosition = firstLinePosition.PreMultiplyBy(Matrix3d.Rotation(MyUtilities.Helpers.Calculations.DegreesToRadians(angleDeg), Vector3d.ZAxis, new Point3d()));
			firstLinePosition = firstLinePosition.PreMultiplyBy(Matrix3d.Displacement(data.StartPoint.ToCADPoint().GetAsVector()));

			Matrix3d nonFirstLinePosition = Matrix3d.Displacement(new Vector3d(textPositionX, firstLineHeight * 1.3 - distance, 0));
			nonFirstLinePosition = nonFirstLinePosition.PreMultiplyBy(Matrix3d.Rotation(MyUtilities.Helpers.Calculations.DegreesToRadians(angleDeg), Vector3d.ZAxis, new Point3d()));
			nonFirstLinePosition = nonFirstLinePosition.PreMultiplyBy(Matrix3d.Displacement(data.StartPoint.ToCADPoint().GetAsVector()));

			// First line goes above the pipe, all others below.
			bool firstline = true;
			pars.TextAlignment = CADMTextHelper.TextAlignment.BottomMiddle;
			foreach (var l in lines)
			{
				MyUtilities.Geometry.Vector3d angle = data.StartPoint.GetVectorTo(data.EndPoint);
				MyUtilities.Geometry.Point3d middle = data.StartPoint.Add(angle.GetUnitVector().Multiply(angle.Length / 2.0));
				double distanceY = pars.TextDelta;

				l.AlignWithVector(middle.GetAs2d(), angle.GetAs2d(), pars);

				pars.TextAlignment = CADMTextHelper.TextAlignment.TopMiddle;
				double height = l.Max(x =>
				{
					double h = x.TextHeight;// x.GeometricExtents.MaxPoint.Y - x.GeometricExtents.MinPoint.Y;
					return h;
				});
				if (firstline)
					pars.TextDelta = distance;
				else
					pars.TextDelta += height + distance;

				/*foreach (MText t in l)
				{

					if (firstline)
					{
						pars.TextAlignment = CADMTextHelper.TextAlignment.BottomMidle;

						CADMTextHelper.AlignWithVector(t, middle.GetAs2d(), angle.GetAs2d(), pars);
						//t.TransformBy(firstLinePosition);
					}
					else
					{
						pars.TextAlignment = CADMTextHelper.TextAlignment.TopMiddle;
						CADMTextHelper.AlignWithVector(t, middle.GetAs2d(), angle.GetAs2d(), pars);
						pars.TextDelta += t.TextHeight + distance;
						//t.TransformBy(nonFirstLinePosition);
					}
				}*/
				firstline = false;
			}

			#region Draw arrow as prefix
				MyGeometry.Vector3d pipeInTextDirection = pipeVector;
				if (invertText)
					pipeInTextDirection = pipeInTextDirection.Negate();
			if (AdditionalSettings.Instance.DrawArrowInText)
			{

				CADArrowHelper.ArrowParameters arrowPars = new CADArrowHelper.ArrowParameters(firstLineHeight * 3, firstLineHeight * 0.9, 0.5, 0.5, 0.2, 1);
				CADArrowHelper.ArrowPositionText position = CADArrowHelper.ArrowPositionText.LeftMiddle;

				arrowPars.Direction = invertText ? CADArrowHelper.ArrowDirection.Against : CADArrowHelper.ArrowDirection.Along;
				Polyline arrowText = new Polyline();
				arrowText.Color = Color.FromColor(AdditionalSettings.Instance.ArrowColor);
				arrowText.Layer = Create.CreatePipe.PipeLabelLayer;
				modelSpace.AppendEntityCurrentLayer(arrowText);
				tr.AddNewlyCreatedDBObject(arrowText, true);
				entities.Add(arrowText);

				CADArrowHelper.CreateArrowAndAlign(arrowText, lines[0][0], pipeInTextDirection.GetAs2d(), arrowPars, position);


				/*if (invertText)
					pipe = pipe.Negate();
				MyGeometry.Vector3d t1 = pipe.CrossProduct(new MyGeometry.Vector3d(0, 0, 1));
				t1 = t1.GetUnitVector().Multiply(firstLineHeight / 2);
				Polyline pl = new Polyline();

				arrowText.Color = Color.FromColor(AdditionalSettings.Instance.ArrowColor);
				arrowText.Layer = Create.CreatePipe.PipeLabelLayer;
				modelSpace.AppendEntityCurrentLayer(arrowText);
				tr.AddNewlyCreatedDBObject(arrowText, true);
				entities.Add(arrowText);
				double tailHeight = firstLineHeight / 5;

				List<Point2d> points = new List<Point2d>();
				Point2d p1 = lines[0][0].Location.FromCADPoint().GetAs2d().Add(t1.GetAs2d()).ToCADPoint();
				p1 = p1.Add(pipe.GetAs2d().GetUnitVector().Multiply(firstLineHeight / 4).Negate().ToCADVector());
				points.Add(p1);
				p1 = p1.Add(pipe.GetAs2d().GetUnitVector().Multiply(firstLineHeight / 1.5).Negate().ToCADVector());
				points.Add(p1);
				p1 = p1.Add(pipe.GetAs2d().GetUnitVector().Multiply(firstLineHeight / 1.5).Negate().ToCADVector());
				points.Add(p1);
				if (invertText)
					points.Reverse();

				arrowText.AddVertexAt(0, points[0], 0, 0, firstLineHeight);
				arrowText.AddVertexAt(1, points[1], 0, tailHeight, tailHeight);
				arrowText.AddVertexAt(2, points[2], 0, 0, 0);*/
			}
			#endregion


			#region Draw arrow on the pipe
			if (AdditionalSettings.Instance.DrawArrowOnPipe)
			{
				double arrowHeight = data.Diameter * 0.8;
				double arrowTailWidth = arrowHeight * 0.2;
				double arrowLength = arrowHeight * 4;
				double arrowTailLength = arrowHeight * 2;
				double arrowHeadLength = arrowHeight * 2;

				Polyline arrow = new Polyline();
				modelSpace.AppendEntityCurrentLayer(arrow);
				tr.AddNewlyCreatedDBObject(arrow, true);
				entities.Add(arrow);
				arrow.Color = Color.FromColor(AdditionalSettings.Instance.ArrowColor);
				arrow.Layer = Create.CreatePipe.PipeLabelLayer;

				double pipeDiameter = data.Diameter;
				CADArrowHelper.ArrowParameters arrowPars = new CADArrowHelper.ArrowParameters(pipeDiameter * 3, pipeDiameter * 0.8, 0.5, 0.5, 0.2, 1);
				arrowPars.Direction = invertText ? CADArrowHelper.ArrowDirection.Against : CADArrowHelper.ArrowDirection.Along;

				MyUtilities.Geometry.Vector2d startArrowVector = pipeVector.GetAs2d().GetUnitVector().Multiply(pipeVector.Length / 2);
				CADArrowHelper.CreateArrow(arrow, data.StartPoint.GetAs2d().Add(startArrowVector), pipeInTextDirection.GetAs2d(), arrowPars, CADArrowHelper.ArrowPositionPoint.Middle);


				/*
				Geometry.Vector2d startArrowVector = pipeVector.GetUnitVector().GetAs2d().Multiply(pipeVector.Length / 2 - arrowLength / 2);
				Geometry.Vector2d arrowDirectionVector = startArrowVector.GetUnitVector();
				startArrowVector = startArrowVector.Add(data.StartPoint.GetAsVector().GetAs2d());
				Geometry.Vector2d startHeadArrowVector = startArrowVector.Add(arrowDirectionVector.Multiply(arrowTailLength));
				Geometry.Vector2d endArrowVector = startArrowVector.Add(arrowDirectionVector.Multiply(arrowLength));

				arrow.AddVertexAt(0, startArrowVector.GetAsPoint().ToCADPoint(), 0, arrowTailWidth, arrowTailWidth);
				arrow.AddVertexAt(1, startHeadArrowVector.GetAsPoint().ToCADPoint(), 0, arrowHeight, 0);
				arrow.AddVertexAt(2, endArrowVector.GetAsPoint().ToCADPoint(), 0, 0, 0);*/
			}
			#endregion

			angleDeg = new MyUtilities.Geometry.Vector3d(1, 0, 0).GetAngleDegXY(data.StartPoint.GetVectorTo(data.EndPoint));
			// Special drawn at the start and end of the pipe (not in the middle with other texts).
			#region Draw pipe out and pipe in
			if (pipeOutDisplay != null && pipeOutDisplay.DrawAsFinal)
			{
				MText pipeOut = new MText();
				modelSpace.AppendEntityCurrentLayer(pipeOut);
				tr.AddNewlyCreatedDBObject(pipeOut, true);
				entities.Add(pipeOut);
				pipeOut.TextHeight = pipeOutDisplay.TextSize;
				pipeOut.Color =Color.FromColor(pipeOutDisplay.TextColor);
				pipeOut.Layer = Create.CreatePipe.PipeLabelLayer;
				pipeOut.Contents = pipeOutDisplay.GetText(displayData);// "Pipe out: " + DoubleHelper.ToStringInvariant(data.StartPoint.Z, 2);

				CADMTextHelper.MTextParameters rotationParameters = new CADMTextHelper.MTextParameters();
				rotationParameters.RotateToKeepHorizont = true;
				rotationParameters.RotationType = CADMTextHelper.HorizontRotation.TextCenterPoint;
				rotationParameters.TextAlignment = CADMTextHelper.TextAlignment.TopLeft;
				rotationParameters.TextDelta = pipeOut.TextHeight / 2;
				pipeOut.AlignWithVector(data.StartPoint.GetAs2d(), data.StartPoint.GetVectorTo(data.EndPoint).GetAs2d(), rotationParameters);
				pipeOut.Location = new Point3d(pipeOut.Location.X, pipeOut.Location.Y, data.StartPoint.Z);

				//pipeOut.Location = data.StartPoint.ToCADPoint();
				//pipeOut.TransformBy(Matrix3d.Rotation(Common.Calculations.DegreesToRadians(angleDeg), Vector3d.ZAxis, new Point3d()));
				//pipeOut.TransformBy(Matrix3d.Displacement(data.StartPoint.GetAsVector().ToCADVector()));
			}

			if (pipeInDisplay != null && pipeInDisplay.DrawAsFinal)
			{
				MText pipeIn = new MText();
				modelSpace.AppendEntityCurrentLayer(pipeIn);
				tr.AddNewlyCreatedDBObject(pipeIn, true);
				entities.Add(pipeIn);
				pipeIn.TextHeight = pipeInDisplay.TextSize;
				pipeIn.Color = Color.FromColor(pipeInDisplay.TextColor);
				pipeIn.Layer = Create.CreatePipe.PipeLabelLayer;
				pipeIn.Contents = pipeInDisplay.GetText(displayData);// "Pipe in: " + DoubleHelper.ToStringInvariant(data.EndPoint.Z, 2);
																	 //pipeOut.Location = data.StartPoint.ToCADPoint();


				CADMTextHelper.MTextParameters rotationParameters = new CADMTextHelper.MTextParameters();
				rotationParameters.RotateToKeepHorizont = true;
				rotationParameters.RotationType = CADMTextHelper.HorizontRotation.TextMiddlePointOnLine;
				rotationParameters.TextAlignment = CADMTextHelper.TextAlignment.BottomRight;
				rotationParameters.TextDelta = pipeIn.TextHeight / 2;
				pipeIn.AlignWithVector(data.EndPoint.GetAs2d(), data.StartPoint.GetVectorTo(data.EndPoint).GetAs2d(), rotationParameters);
				pipeIn.Location = new Point3d(pipeIn.Location.X, pipeIn.Location.Y, data.EndPoint.Z);

				//Extents3d ext = pipeIn.GeometricExtents;
				//double stringSize = ext.MaxPoint.X - ext.MinPoint.X;
				//pipeIn.TransformBy(Matrix3d.Displacement(new Vector3d(-stringSize, 0, 0)));
				//pipeIn.TransformBy(Matrix3d.Rotation(Common.Calculations.DegreesToRadians(angleDeg), Vector3d.ZAxis, new Point3d()));
				//pipeIn.TransformBy(Matrix3d.Displacement(data.EndPoint.GetAsVector().ToCADVector()));
			}
			#endregion
		}
	}
}
