﻿using CadPlugin.Sewage.Plannings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtilities.Geometry;
using CadPlugin.Sewage.Create.Settings;

namespace CadPlugin.Sewage.Planning.PipeShaftDataManagers
{
	class DownstreamManager : PipeShaftDataManager
	{
		public override void MoveNextShaft()
		{
			bool firstPipe = PreviousDataExists;
			if (!Data.EndPointSet)
				throw new NotSupportedException("Unable to move to the next pipe. End point is not set!");
			dataHistory.Add(__Data.Clone());
			bool startDeltaUsed = false;
			bool endDeltaUsed = false;
			double startDelta = 0;
			double endDelta = 0;
			if (Data.StartShaft != null)
			{
				startDeltaUsed = Data.StartShaft.MinPipeOutDeltaUsed;
				startDelta = Data.StartShaft.MinPipeOutDelta;
			}
			if (Data.EndShaft != null)
			{
				endDeltaUsed = Data.EndShaft.MinPipeOutDeltaUsed;
				endDelta = Data.EndShaft.MinPipeOutDelta;
			}

			

			// Start shaft becomes end shaft.
			__Data.StartShaftData = __Data.EndShaftData;
			// New end shaft is cloned or created.
			if (Data.EndShaft != null)
				__Data.EndShaftData = __Data.EndShaftData.Clone();
			else
				__Data.EndShaftData = new ShaftData();
			Data.EndPointSet = false;


			// End shaft becomes start shaft.
			// Pipe out for new shaft is calculated (inner slope).
			if (Data.StartShaft != null)
			{
				__Data.StartShaftData.MinPipeOutDeltaUsed = startDeltaUsed;
				__Data.StartShaftData.MinPipeOutDelta = startDelta;

				if (startDeltaUsed)
					__Data.StartShaftData.PipeOutHeight = Data.StartShaft.TerrainHeight - startDelta;
				else
					__Data.StartShaftData.PipeOutHeight = Data.Pipe.EndPoint.Z - Data.StartShaft.InnerSlopeDeltaH;
			}
			if (Data.EndShaft != null)
			{
				__Data.EndShaftData.MinPipeOutDeltaUsed = endDeltaUsed;
				__Data.EndShaftData.MinPipeOutDelta = endDelta;
			}


			//NOTE Seems not so important [removed]
			// __Data.PipeData.StartPoint = Data.Pipe.EndPoint;

			// Notify Data that shaft has been changed.
			__Data.MoveNextShaft();
		}

		/// <summary>
		/// Updates end point.
		/// </summary>
		public override void UpdateVariablePoint(Point3d point, double? terrainHeight = null)
		{
			// Updates end point and performs recalculation.
			SetEndPoint(point);
			// Recalculate
			Recalculate();
		}

		protected override void StartPointUpdated()
		{
			// For downstream, updating start point cancels end point (actually true only for segments).
			Data.EndPointSet = false;
		}

		protected override void RecalculatePoint()
		{
			TerrainCalculator calc;
			double terrainH;

			calc = TerrainHeightCalculator;
			System.Diagnostics.Debug.Assert(calc != null, "Terrain calculator does not exist in downstream!");
			//NOTE If user sets height manually, additional property is needed.
			/*if (Data.EndShaft == null)
				calc = TerrainHeightCalculator;
			else
				// (i think this is used because user can change value manualy.
				// Otherwise, good value is set in calculator, so no need to fetch terrain).
				calc = new PointTerrainHeight(new Point3d(0, 0, Data.EndShaft.TerrainHeight));*/

			Calculation.UpdatePosition(this, calc);
			terrainH = calc.GetHeight(Calculation.EndPoint);

			if (Data.EndShaft != null)
			{
				UpdateEndShaft(terrainH, ShaftFields.Terrain);
				double t = 0;
				t = Calculation.EndPointOffset.Z - Data.EndShaft.InnerSlopeDeltaH;
				UpdateEndShaft(t, ShaftFields.PipeOut);
			}
		}

		public override void SetVariablePoint(Point3d position)
		{
			SetEndPoint(position);
		}
	}
}
