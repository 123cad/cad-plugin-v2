﻿using CadPlugin.Sewage.Plannings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtilities.Geometry;


namespace CadPlugin.Sewage.Planning
{
	internal class PointTerrainHeight : TerrainCalculator
	{
		public Point3d StartPoint { get; private set; }
		public PointTerrainHeight(Point3d p)
		{
			StartPoint = p;
		}
		public override double GetHeight(Point3d point)
		{
			return StartPoint.Z;
		}
		public override double GetHeight(Point2d point)
		{
			return StartPoint.Z;
		}
	}
}
