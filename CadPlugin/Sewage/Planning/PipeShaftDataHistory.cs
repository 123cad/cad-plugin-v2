﻿using CadPlugin.Sewage.Create.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Sewage.Planning
{
	class PipeShaftDataHistory
	{
		public IList<PipeShaftData> Data { get; private set; }

		public PipeShaftDataHistory()
		{
			Data = new List<PipeShaftData>();
		}
	}
}
