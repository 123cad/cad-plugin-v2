﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CadPlugin.Sewage.Create.Settings;
using CadPlugin.Sewage.Create.AdditionalSettings;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Sewage.Plannings
{
	/// <remarks>Manages text displayed when jigging</remarks>
	public class JigTextManager
	{ 
		private class TextPair
		{
			public MText Text;
			public TextDisplaySingle Data;
		}
		public virtual PipeShaftDataManager PipeShaftDataManager
		{
			get;
			private set;
		}
		private DisplaySelectedData selectedData;
		/// <summary>
		/// Texts which are to be jigged.
		/// </summary>
		internal IEnumerable<Entity> Texts { get
			{
				var res = textPairs.Select(x=>x.Text).Where(y=>y != null).ToList();
				if (angleDisplay != null)
					res.Add(angleDisplay);
				return res;
			}
		}
		private List<TextDisplaySingle> texts = new List<TextDisplaySingle>();

		private List<TextPair> textPairs = new List<TextPair>();
		private MText angleDisplay = null;
		public Transaction tr { get; private set; }
		public BlockTableRecord modelSpace { get; private set; }

		/// <summary>
		/// For triggering additional update operations.
		/// </summary>
		private event Action UpdateEvent;

		public JigTextManager(PipeShaftDataManager manager, Transaction tr, BlockTableRecord modelSpace, Create.WaterDirection waterDir = Create.WaterDirection.Downstream, JigDataUpdater updater = null)
		{
			this.tr = tr;
			this.modelSpace = modelSpace;
			PipeShaftDataManager = manager;
			switch (waterDir)
			{
				case Create.WaterDirection.Downstream:
					selectedData = DisplaySelectedData.CreateWithEndShaft(manager);
					break;
				case Create.WaterDirection.Upstream:
					selectedData = DisplaySelectedData.CreateWithStartShaft(manager);
					break;
			}

			if (updater != null)
				updater.DataUpdated += Update;
		}
		internal void Initialize(TextDisplayOrdered display, Point3d start)
		{
			texts.Clear();
			textPairs.Clear();
			double textHeight = 0.6;
			double textLineHeight = textHeight * 1.2;
			double currentDistance = 0;

			
			if (AdditionalSettings.Instance.ShowAngle && PipeShaftDataManager.PreviousDataExists)
			{
				// Angle text
				angleDisplay = new MText();
				modelSpace.AppendEntityCurrentLayer(angleDisplay);
				tr.AddNewlyCreatedDBObject(angleDisplay, true);
				angleDisplay.TextHeight = 0.3;
				angleDisplay.Location = PipeShaftDataManager.Data.StartPoint.Add(new MyUtilities.Geometry.Vector3d(0, angleDisplay.TextHeight * 1.3, 0)).ToCADPoint();
				MyUtilities.Geometry.Point3d centerPoint = PipeShaftDataManager.Data.StartPoint;
				MyUtilities.Geometry.Vector2d lastPipeVector = PipeShaftDataManager.DataHistory.Last().Pipe.StartPoint.GetVectorTo(centerPoint).GetAs2d();
				UpdateEvent += () =>
				{
					double angle = lastPipeVector.GetAngleDeg(PipeShaftDataManager.Data.Pipe.Direction.GetAs2d());
					angleDisplay.Contents = "Angle: " + DoubleHelper.ToStringInvariant(angle, 2) + "°";
				};
			}

			foreach (TextDisplaySingle single in display.ShaftDisplayOrder)
			{
				if (!single.DrawInJig)
					continue;
				MText text = new MText();
				modelSpace.AppendEntityCurrentLayer(text);
				tr.AddNewlyCreatedDBObject(text, true);

				text.TextHeight = textHeight;
				text.Location = start.Add(new Vector3d(0, currentDistance - textLineHeight, 0));
				currentDistance -= textLineHeight;

				textPairs.Add(new TextPair() { Data = single, Text = text });
				texts.Add(single);
			}
			foreach (TextDisplaySingle single in display.PipeDisplayOrder)
			{
				if (!single.DrawInJig)
					continue;
				MText text = new MText();
				modelSpace.AppendEntityCurrentLayer(text);
				tr.AddNewlyCreatedDBObject(text, true);

				text.TextHeight = textHeight;
				text.Location = start.Add(new Vector3d(0, currentDistance - textLineHeight, 0));
				currentDistance -= textLineHeight;

				textPairs.Add(new TextPair() { Data = single, Text = text });
				texts.Add(single);
			}
		}

		public virtual void Update()
		{
			foreach(TextPair p in textPairs)
			{
				string s = p.Data.GetText(selectedData);
				p.Text.Contents = s;
			}
			if (UpdateEvent != null)
				UpdateEvent.Invoke();
		}

		/// <summary>
		/// Jigging finished, clear all up.
		/// </summary>
		public void Finish()
		{
			foreach (TextPair p in textPairs)
			{
				if (p.Text == null)
					continue;
				p.Text.Erase();
				p.Text = null;
			}
			if (angleDisplay != null)
				angleDisplay.Erase();
		}
	}
}

