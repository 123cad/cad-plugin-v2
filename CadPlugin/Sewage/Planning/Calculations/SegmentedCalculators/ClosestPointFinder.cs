﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtilities.Geometry;

namespace CadPlugin.Sewage.Planning.Calculations
{
	
	public struct SearchPointParams<T>
	{
		public bool PointExists { get; private set; }
		public T Point { get; private set; }
		/// <summary>
		/// If needed, extra point can be set.
		/// </summary>
		public T ExtraPoint { get; set; }
		public double AssociatedLength { get; private set; }
		/// <summary>
		/// XY length of the calculated point.
		/// </summary>
		public double Length2D { get; set; }
		public SearchPointParams(T point, double length)
		{
			PointExists = true;
			Point = point;
			AssociatedLength = length;
			ExtraPoint = default(T);
			Length2D = AssociatedLength;
		}
		public static SearchPointParams<T> Null = new SearchPointParams<T>();
	}
	/// <summary>
	/// Finds closest point.
	/// </summary>
	public class ClosestPointFinder<T> //where T : Point2d
	{
		// Instead of calculating point position, we let user do it.
		// Point with length associated with it is provided by user.
		// In this way, this class is independent of other points and positions.

		private bool rightSideSet;
		private bool leftSideSet;
		private double closestRightLength;
		private double closestLeftLength;
		public SearchPointParams<T> LeftPointParams { get; private set; }
		public SearchPointParams<T> RightPointParams { get; private set; }

		/// <summary>
		/// Indicates if left point had been found.
		/// </summary>
		public bool LeftPointFound { get; private set; }
		/// <summary>
		/// Indicates if right point had been found.
		/// </summary>
		public bool RightPointFound { get; private set; }
		/// <summary>
		/// Indicates if both points have been found.
		/// </summary>
		public bool IsFinished { get { return LeftPointFound && RightPointFound; } }

		/// <summary>
		/// Returns closest point to the reference length.
		/// </summary>
		/// <param name="referenceLength"></param>
		/// <returns></returns>
		public SearchPointParams<T> GetFinalPoint(double referenceLength)
		{
			if (!leftSideSet && !rightSideSet)
				throw new NullReferenceException("Closest point does not exist!");
			SearchPointParams<T> t;
			if (leftSideSet && rightSideSet)
			{
				if (Math.Abs(closestLeftLength - referenceLength) < Math.Abs(closestRightLength - referenceLength))
					t = LeftPointParams;
				else
					t = RightPointParams;
			}
			else
			{
				if (leftSideSet)
					t = LeftPointParams;
				else
					t = RightPointParams;
			}
			return t;
		}


		/// <summary>
		/// Searches for closest point for the reference length.
		/// First time when point belonging to specific side is not closest one (set previously), method returns.
		/// Resulting value of true, means both left and right closest points have been found.
		/// </summary>
		public void Search(Func<double, SearchPointParams<T>> calculate, double referenceLength, SegmentedLengthIterator lengths)
		{
			while (true)
			{
				// Right side points.
				//Point2d? pt = LineCircleIntersection(start_Z, lengths.MaxLength, lineParams, desiredXYLength);
				double currentLength = lengths.MaxLength;
				SearchPointParams<T> result = calculate(currentLength);
				currentLength = result.Length2D;
				if (!result.PointExists)
				{
					lengths.IncreaseMaxSegments();
					continue;
				}

				// If we are on the left side (big delta/small segments/short pipe). 
				if (currentLength < referenceLength)
				{
					if (!leftSideSet || currentLength > closestLeftLength)//Math.Abs(closestLeftLength - referenceLength) > Math.Abs(currentLength - referenceLength))
					{
						closestLeftLength = currentLength;
						LeftPointParams = result;
						LeftPointFound = true;
					}
					leftSideSet = true;
				}
				else
				{
					if (!rightSideSet)
					{
						closestRightLength = currentLength;
						RightPointParams = result;
					}
					else
					{
						// As s oon as length starts increasing, new point has been found.
						if (currentLength > closestRightLength)//(Math.Abs(closestRightLength - referenceLength) < Math.Abs(currentLength - referenceLength))
						{
							RightPointFound = true;
							break;
						}
						else
						{
							closestRightLength = currentLength;
							RightPointParams = result;
						}
					}
					rightSideSet = true;
				}
				lengths.IncreaseMaxSegments();
			}
			// If right point appeared on the left side, left point can't be closer to reference length.
			if (!leftSideSet)
			{
				while (true)
				{
					// Left side points.
					//Point2d? pt = LineCircleIntersection(start_Z, lengths.MinLength, lineParams, desiredXYLength);
					double currentLength = lengths.MinLength;
					SearchPointParams<T> result = calculate(currentLength);
					currentLength = result.Length2D;
					if (!result.PointExists)
					{
						if (!lengths.DecreaseMinSegments())
							break;
						continue;
					}

					if (currentLength > referenceLength)
					{
						if (!rightSideSet || currentLength < closestRightLength)
						{
							closestRightLength = currentLength;
							RightPointParams = result;
							RightPointFound = true;
						}
						rightSideSet = true;
					}
					else
					{
						if (!leftSideSet)
						{
							closestLeftLength = currentLength;
							LeftPointParams = result;
						}
						else if (currentLength < closestLeftLength)//(Math.Abs(closestLeftLength - referenceLength) < Math.Abs(currentLength - referenceLength))
						{
							LeftPointFound = true;
							break;
						}
						leftSideSet = true;
					}
					if (!lengths.DecreaseMinSegments())
					{
						break;
					}
				}

				if (leftSideSet)
					LeftPointFound = true;
			}
		}

	}
}
