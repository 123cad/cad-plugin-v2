﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtilities.Geometry;

namespace CadPlugin.Sewage.Planning.Calculations.SegmentedCalculators
{
	/// <summary>
	/// Used to set custom coordinate center (set center to be new coordinate)
	/// </summary>
	struct CoordinateCenterMove
	{
		/// <summary>
		/// New center of coordinate system.
		/// </summary>
		public Point3d Center { get; private set; }
		/// <summary>
		/// Represents move to new center.
		/// </summary>
		private Vector3d toNewCenter { get; set; }
		/// <summary>
		/// Represents move from new center to (0,0,0).
		/// </summary>
		private Vector3d fromNewCenter { get; set; }

		public CoordinateCenterMove(Point3d center)
		{
			Center = center;
			toNewCenter = center.GetAsVector();
			fromNewCenter = toNewCenter.Negate();
		}
		public Point3d ConvertToNewSystem(Point3d p)
		{
			return p.Add(toNewCenter);
		}
		public Point3d ConvertToUCS(Point3d p)
		{
			return p.Add(fromNewCenter);
		}
	}
}
