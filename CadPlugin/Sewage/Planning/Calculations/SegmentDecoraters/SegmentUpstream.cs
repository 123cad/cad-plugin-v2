﻿using MyUtilities.Geometry;
using CadPlugin.Sewage.Create.Settings;
using CadPlugin.Sewage.Plannings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Sewage.Planning.Calculations.SegmentDecoraters
{
	class SegmentUpstream : SegmentedDecorater
	{
		public SegmentUpstream(PipeCalculation calc, double segmentLength) : base(calc, segmentLength)
		{
		}
		public SegmentUpstream(PipeCalculation calc, double segmentLength, double startEndSegmentLength) : base(calc, segmentLength, startEndSegmentLength)
		{
		}

		public override void UpdatePosition(PipeShaftDataManager mgr, TerrainCalculator calc)
		{
			// Shaft has only one height - center of the shaft height. This height is used for pipe in and for pipe out.
			// If no delta is included in calculation, then small difference exists.

			IPipeShaftData data = mgr.Data;
			Calculation.UpdatePosition(mgr, calc);
			Point3d endOffset = Calculation.EndPointOffset;// Needed because we have to update it via "ref"

			Vector3d pipeVector = Calculation.PipeVector;
			double length = pipeVector.Length;

			Vector3d segmentedPipeVector;// Start to end point.
			Point3d startPointOffset;
			int segmentCount = GetLowerSegmentCount(length, SegmentLength, StartEndSegmentLength);// GetNumberOfSegments(length, SegmentLength, StartEndSegmentLength);
			SegmentedLengthIterator lengths = new SegmentedLengthIterator(SegmentLength, StartEndSegmentLength, segmentCount, segmentCount + 1);
			if (data.ApplyMinPipeSlope && !data.ApplyEndShaftPipeOutDelta)// .SlopeSource == PipeSlopeSource.PipeSlope)
			{
				// Apply only min pipe slope.
				segmentedPipeVector = SegmentedCalculator.CalculatePointWithMinSlope(lengths, pipeVector);//, Calculation.StartPointOffset);
																										  //segmentedPipeVector = Calculation.StartPointOffset.GetVectorTo(pt);
																										  //double segmentedLength = segmentCount * SegmentLength + 2*StartEndSegmentLength;
																										  //segmentedPipeVector = pipeVector.GetUnitVector().Multiply(segmentedLength);
				segmentedPipeVector = segmentedPipeVector.Negate();
			}
			else
			{
				if (data.StartShaft == null)
					throw new NotSupportedException("Fixed pipe out delta is supported only when start shaft exitst!");
				IShaftData shaft = data.StartShaft;
				Point3d? calculatedPoint = null;
				double minDelta = shaft.MinPipeOutDelta;// shaft.TerrainHeight - shaft.PipeOutHeight;
				double startRadius = shaft != null ? shaft.Radius : 0;
				if (!data.ApplyMinPipeSlope)
					calculatedPoint = SegmentedCalculator.CalculatePointWithMinDelta(lengths, pipeVector.Negate(), Calculation.EndPointOffset, calc, minDelta, startRadius);
				else
				{
					double endPipeOutDelta = data.EndShaft != null && data.ApplyEndShaftPipeOutDelta ? data.EndShaft.MinPipeOutDelta : 0;
					calculatedPoint = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(lengths, pipeVector.Negate(), ref endOffset, data.EndShaft.TerrainHeight, calc, mgr.Data.MinPipeSlope, endPipeOutDelta, minDelta, startRadius);
				}
				//double delta = data.EndShaft.TerrainHeight - data.EndShaft.PipeOutHeight - data.EndShaft.InnerSlopeDeltaH;
				//Point3d? calculatedPoint = SegmentedCalculator.Calculate(Calculation.StartPointOffset, Calculation.PipeVector, segmentedLengths, calc, delta);

				segmentedPipeVector = endOffset.GetVectorTo(calculatedPoint.Value);
				
				//segmentedPipeVector = new Vector3d(pipe2d.X, pipe2d.Y, -delta);
			}
			// New end point of the pipe.
			startPointOffset = endOffset.Add(segmentedPipeVector);

			// Since pipe end point is moved, calculate how much to move end shaft.
			Vector3d deltaVector = Calculation.StartPointOffset.GetVectorTo(startPointOffset);

			// IMPORTANT: Set these to current instance, not decorated instance! 
			PipeVector = segmentedPipeVector.Negate();
			StartPoint = Calculation.StartPoint.Add(deltaVector);
			StartPointOffset = startPointOffset;
			EndPoint = Calculation.EndPoint;
			EndPointOffset = endOffset;
			System.Diagnostics.Debug.WriteLine("start: " + StartPoint);
		}
	}
}
