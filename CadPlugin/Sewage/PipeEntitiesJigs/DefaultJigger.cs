﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.PipeEntitiesJigs
{
	public class DefaultJigger : JiggerCalculation
	{ 		
		public DefaultJigger(Point3d center):base(center)
		{

		}

		public override SamplerStatus Update(Point3d mousePosition)
		{
			Data.LineEndPoint = mousePosition;
			Data.EndPoint = mousePosition;
			return SamplerStatus.OK;
		}
		

	}
}

