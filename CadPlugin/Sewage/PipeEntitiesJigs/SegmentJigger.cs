﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.PipeEntitiesJigs
{
	public abstract class SegmentJigger : JiggerCalculation
	{ 
		/// <summary>
		/// Gets slope (as tangens, not as percent).
		/// </summary>
		public virtual double Slope
		{
			get;
			protected set;
		}

		public virtual double SegmentLength
		{
			get;
			private set;
		}
		
		protected SegmentJigger(Point3d center, double slope, double segmentLength):base(center)
		{
			Slope = slope;
			SegmentLength = segmentLength;
		}
		public override Vector3d GetMoveVectorForEntities(Point3d currentPosition)
		{
			return currentPosition.GetVectorTo(Data.LineEndPoint);
		}

		public override abstract SamplerStatus Update(Point3d mousePosition);
		
	}
}

