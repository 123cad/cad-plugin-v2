﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.GraphicsInterface;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.GraphicsInterface;
#endif

namespace CadPlugin.PipeEntitiesJigs
{
	/// <summary>
	/// Jiggs line with entities (according to JiggerCalculation).
	/// </summary>
	public class LineWithEntitiesJigger : DrawJig
	{ 
		public virtual Point3d LastMousePosition { get; private set; }

		public virtual Point3d CurrentMousePosition { get; private set; }
		/// <summary>
		/// Entities can have different position than end of the line. 
		/// </summary>
		protected Point3d LastEntitiesPosition { get; set; }
		
		public JiggerCalculation JiggerCalculation { get; private set; }

		private IEnumerable<Entity> entities { get; set; }
		/// <summary>
		/// Entities that are being moved relative to end point.
		/// </summary>
		protected IEnumerable<Entity> entitiesCollection = null;
		/// <summary>
		/// Line drawn from start to end point.
		/// </summary>
		protected Line Line = null;

		public Matrix3d UCS { get; private set; }

		public event Action DataChanged;
		
		/// <summary>
		/// Message which is displayed to the user while jigging.
		/// </summary>
		public string JigMessage { get; private set; }
		/// <summary>
		/// Performance issue - this is set when JigMessage is set.
		/// </summary>
		private string JigMessageWithKeywords;
		/// <summary>
		/// Keywords used while jigging.
		/// </summary>
		public string JigKeywords { get; private set; }
		/// <summary>
		/// Flag which indicates that user typed a keyword (instead of selecting a point).
		/// </summary>
		public bool JigKeywordEntered { get; private set; }
		public string EnteredKeyword { get; private set; }


		/// <summary>
		/// 
		/// </summary>
		/// <param name="l"></param>
		/// <param name="entities"></param>
		/// <param name="calc"></param>
		/// <param name="ucs"></param>
		public LineWithEntitiesJigger(Line l, IEnumerable<Entity> entities, JiggerCalculation calc, Matrix3d ucs)
		{
			entitiesCollection = entities;
			Line = l;
			JiggerCalculation = calc;
			UCS = ucs;
			CurrentMousePosition = l.StartPoint;
			LastEntitiesPosition = l.StartPoint;
			JigMessage = "";
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="message">Just text, keywords are appended automatically.</param>
		/// <param name="keywords">Keywords - null if they don't exist (multiple keywords must be divided by empty space)</param>
		public void SetMessage(string message, string keywords = null)
		{
			JigMessage = message;
			JigKeywords = keywords;
			JigMessageWithKeywords = message;
			if (keywords != null)
				JigMessageWithKeywords += " [" + string.Join("/", keywords.Split(' ').Select(x=>x.Trim())) + "]:";
		}
		private void OnDataChanged()
		{
			if (DataChanged != null)
				DataChanged.Invoke();
		}
		public static int testCounterUpdates = 0;
		public static int testCounterRedraw = 0;
		protected override bool WorldDraw(WorldDraw draw)
		{
			WorldGeometry geo = draw.Geometry;
			if (geo != null)
			{
				//geo.PushModelTransform(UCS);
				//System.Diagnostics.Debug.WriteLine("redraw " + testCounterRedraw++);

				// Only when data is redrawn, LastMousePosition is updated.
				// Because there can be 10 updates of position, without redrawing, and we 
				// are related to redrawing.
				LastMousePosition = CurrentMousePosition;
				Vector3d moveVector = JiggerCalculation.GetMoveVectorForEntities(LastEntitiesPosition);


				this.Line.StartPoint = JiggerCalculation.GetLineStartPoint();
				this.Line.EndPoint = JiggerCalculation.GetLineEndPoint();
				geo.Draw(this.Line);

				LastEntitiesPosition = LastEntitiesPosition.Add(moveVector);
				Matrix3d moveMatrix = Matrix3d.Displacement(moveVector);
				foreach (Entity ent in entitiesCollection)
				{
					// For some reason, transforming AttributeReference downgrades 
					// BlockReference, and vice versa.
					if (!ent.IsWriteEnabled)
						ent.UpgradeOpen();
					ent.TransformBy(moveMatrix);

					geo.Draw(ent);
				}
				DrawEntities(moveVector, moveMatrix, geo);


				//geo.PopModelTransform();
			}

			return true;
		}
		static int i = 0;
		protected override SamplerStatus Sampler(JigPrompts prompts)
		{
			JigPromptPointOptions prOptions1 = new JigPromptPointOptions();
			prOptions1.UseBasePoint = false;
			
			if (JigKeywords == null)
				prOptions1.Message = JigMessage;
			else
			{
				try
				{
					/*string s1 = "Select point [Newface/Back]:";
					string s2 = "Newface Back";
					prOptions1.SetMessageAndKeywords(s1, s2);*/
					prOptions1.SetMessageAndKeywords(JigMessageWithKeywords, JigKeywords);
				}
				catch(System.Exception)
				{

				}
			}
			prOptions1.UserInputControls =
				UserInputControls.NullResponseAccepted | UserInputControls.Accept3dCoordinates |
				UserInputControls.GovernedByUCSDetect | UserInputControls.GovernedByOrthoMode/* |
				UserInputControls.AcceptMouseUpAsPoint*/;

			PromptPointResult prResult1 = prompts.AcquirePoint(prOptions1);
			if (prResult1.Status == PromptStatus.Cancel || prResult1.Status == PromptStatus.Error)
				return SamplerStatus.Cancel;
			if (prResult1.Status == PromptStatus.Keyword)
			{
				JigKeywordEntered = true;
				EnteredKeyword = prResult1.StringResult;
				return SamplerStatus.Cancel;
			}

			Point3d tempPt = prResult1.Value.TransformBy(UCS.Inverse());


			if (prResult1.Value.Equals(CurrentMousePosition))
			{
				return SamplerStatus.NoChange;
			}
			else
			{

				// LastMousePosition has to be updated when data is moved on the screen.
				// This is done in the WorldDraw method.
				CurrentMousePosition = prResult1.Value;
				SamplerStatus status = JiggerCalculation.Update(CurrentMousePosition);
				if (status == SamplerStatus.OK)
					OnDataChanged();
				//if (status == SamplerStatus.OK)
					//System.Diagnostics.Debug.WriteLine("update" + testCounterUpdates++);
				i++;
				if (i % 15 == 0)
					status = SamplerStatus.OK;
				else
					status = SamplerStatus.NoChange;
				return status;
			}

		}
		/// <summary>
		/// Intented for extending, to draw additional entities.
		/// </summary>
		/// <param name="moveFromLastPoint">Vector of movement between last user point and current user point.</param>
		/// <param name="transformFromLastPoint">Displacement matrix from last user point to current one</param>
		protected virtual void DrawEntities(Vector3d moveFromLastPoint, Matrix3d transformFromLastPoint, WorldGeometry draw)
		{

		}
	}
}

