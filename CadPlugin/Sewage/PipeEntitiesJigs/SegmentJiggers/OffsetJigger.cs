﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.PipeEntitiesJigs
{
	/// <summary>
	/// Offset (shaft radius) can exist at the start or/and at the end. Line is calculated
	/// to determine direction from start of the shaft to the end of the shaft, and pipe is calculated
	/// on this line (from edge of the shaft, to the edge of second one).
	/// Pipe is here in the focus - calculate length of it, and then just add shaft.
	/// </summary>
	public abstract class OffsetJigger : SegmentJigger
	{ 
		/// <summary>
		/// Gets offset at which start point is drawn.
		/// </summary>
		public double StartOffset { get; private set; }
		/// <summary>
		/// Gets offset at end point.
		/// </summary>
		public double EndOffset { get; private set; }
		public OffsetJigger(Point3d center, double startOffset, double endOffset, double slope, double segmentLength):base(center, slope, segmentLength)
		{
			StartOffset = startOffset;
			EndOffset = endOffset;
		}

		public override Point3d GetLineStartPoint()
		{
			return base.GetLineStartPoint();
		}

		/*public override Vector3d GetMoveVectorForEntities(Point3d currentPosition)
		{
			// Entites are relative to center of the end shaft.
			return currentPosition.GetVectorTo(Data.EndPoint);
		}*/

		public override SamplerStatus Update(Point3d mousePosition)
		{

			Data.CursorMove(mousePosition);
			

			SamplerStatus status = SamplerStatus.OK;
			return status;
		}
	}

	public class OffsetJiggerUpstream : OffsetJigger
	{
		public OffsetJiggerUpstream(Point3d center, double startOffset, double endOffset, double slope, double segmentLength) : base(center, startOffset, endOffset, slope, segmentLength)
		{
		}
		public override Vector3d GetMoveVectorForEntities(Point3d currentPosition)
		{
			return currentPosition.GetVectorTo(Data.StartPoint);
		}
	}
	public class OffsetJiggerDownstream : OffsetJigger
	{
		public OffsetJiggerDownstream(Point3d center, double startOffset, double endOffset, double slope, double segmentLength) : base(center, startOffset, endOffset, slope, segmentLength)
		{
		}
		public override Vector3d GetMoveVectorForEntities(Point3d currentPosition)
		{
			return currentPosition.GetVectorTo(Data.EndPoint);
		}
	}

}

