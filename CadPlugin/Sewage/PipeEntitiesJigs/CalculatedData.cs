﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.PipeEntitiesJigs
{
	public class CalculatedData
	{ 
		/// <summary>
		/// Start point of the line.
		/// </summary>
		public virtual Point3d LineStartPoint
		{
			get;
			set;
		}
		/// <summary>
		/// End point of the line.
		/// </summary>
		public virtual Point3d LineEndPoint
		{
			get;
			set;
		}
		/// <summary>
		/// New position of calculated data.
		/// </summary>
		public virtual Point3d EndPoint
		{
			get;set;
		}

		public virtual double Length
		{
			get;
			set;
		}

		public virtual int SegmentsCount
		{
			get;
			set;
		}

		
	}
}

