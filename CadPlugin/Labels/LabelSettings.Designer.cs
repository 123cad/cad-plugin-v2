﻿namespace CadPlugin
{
    partial class LabelSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LabelSettings));
            this.comboBoxAxis = new System.Windows.Forms.ComboBox();
            this.labelAxis = new System.Windows.Forms.Label();
            this.dataGridViewLevellinesSettings = new System.Windows.Forms.DataGridView();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.buttonOk = new System.Windows.Forms.Button();
            this.labelLevellines = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.groupBoxDecimalPlaces = new System.Windows.Forms.GroupBox();
            this.comboBoxSlope = new System.Windows.Forms.ComboBox();
            this.comboBoxRatio = new System.Windows.Forms.ComboBox();
            this.labelSlope = new System.Windows.Forms.Label();
            this.labelRatio = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLevellinesSettings)).BeginInit();
            this.groupBoxDecimalPlaces.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxAxis
            // 
            this.comboBoxAxis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAxis.FormattingEnabled = true;
            this.comboBoxAxis.Location = new System.Drawing.Point(12, 25);
            this.comboBoxAxis.Name = "comboBoxAxis";
            this.comboBoxAxis.Size = new System.Drawing.Size(191, 21);
            this.comboBoxAxis.TabIndex = 0;
            this.comboBoxAxis.SelectedIndexChanged += new System.EventHandler(this.comboBoxAxis_SelectedIndexChanged);
            // 
            // labelAxis
            // 
            this.labelAxis.AutoSize = true;
            this.labelAxis.Location = new System.Drawing.Point(9, 7);
            this.labelAxis.Name = "labelAxis";
            this.labelAxis.Size = new System.Drawing.Size(26, 13);
            this.labelAxis.TabIndex = 1;
            this.labelAxis.Text = "Axis";
            // 
            // dataGridViewLevellinesSettings
            // 
            this.dataGridViewLevellinesSettings.AllowUserToAddRows = false;
            this.dataGridViewLevellinesSettings.AllowUserToDeleteRows = false;
            this.dataGridViewLevellinesSettings.AllowUserToResizeColumns = false;
            this.dataGridViewLevellinesSettings.AllowUserToResizeRows = false;
            this.dataGridViewLevellinesSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewLevellinesSettings.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dataGridViewLevellinesSettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLevellinesSettings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.dataGridViewLevellinesSettings.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridViewLevellinesSettings.Location = new System.Drawing.Point(12, 86);
            this.dataGridViewLevellinesSettings.MultiSelect = false;
            this.dataGridViewLevellinesSettings.Name = "dataGridViewLevellinesSettings";
            this.dataGridViewLevellinesSettings.RowHeadersVisible = false;
            this.dataGridViewLevellinesSettings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewLevellinesSettings.Size = new System.Drawing.Size(436, 146);
            this.dataGridViewLevellinesSettings.TabIndex = 2;
            this.dataGridViewLevellinesSettings.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLevellinesSettings_CellMouseEnter);
            this.dataGridViewLevellinesSettings.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridViewLevellinesSettings_CurrentCellDirtyStateChanged);
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Level";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 50;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.HeaderText = "Name";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Label";
            this.Column4.Name = "Column4";
            this.Column4.Width = 40;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Type";
            this.Column5.Name = "Column5";
            this.Column5.Width = 80;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Text Size";
            this.Column6.Name = "Column6";
            this.Column6.Width = 70;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(292, 244);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 3;
            this.buttonOk.Text = "Ok";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // labelLevellines
            // 
            this.labelLevellines.AutoSize = true;
            this.labelLevellines.Location = new System.Drawing.Point(9, 65);
            this.labelLevellines.Name = "labelLevellines";
            this.labelLevellines.Size = new System.Drawing.Size(54, 13);
            this.labelLevellines.TabIndex = 5;
            this.labelLevellines.Text = "Levellines";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(373, 244);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // groupBoxDecimalPlaces
            // 
            this.groupBoxDecimalPlaces.Controls.Add(this.comboBoxSlope);
            this.groupBoxDecimalPlaces.Controls.Add(this.comboBoxRatio);
            this.groupBoxDecimalPlaces.Controls.Add(this.labelSlope);
            this.groupBoxDecimalPlaces.Controls.Add(this.labelRatio);
            this.groupBoxDecimalPlaces.Location = new System.Drawing.Point(315, 12);
            this.groupBoxDecimalPlaces.Name = "groupBoxDecimalPlaces";
            this.groupBoxDecimalPlaces.Size = new System.Drawing.Size(133, 68);
            this.groupBoxDecimalPlaces.TabIndex = 7;
            this.groupBoxDecimalPlaces.TabStop = false;
            this.groupBoxDecimalPlaces.Text = "Decimal places";
            this.groupBoxDecimalPlaces.Enter += new System.EventHandler(this.groupBoxDecimalPlaces_Enter);
            // 
            // comboBoxSlope
            // 
            this.comboBoxSlope.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSlope.FormattingEnabled = true;
            this.comboBoxSlope.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBoxSlope.Location = new System.Drawing.Point(81, 39);
            this.comboBoxSlope.Name = "comboBoxSlope";
            this.comboBoxSlope.Size = new System.Drawing.Size(40, 21);
            this.comboBoxSlope.TabIndex = 3;
            this.comboBoxSlope.SelectedIndexChanged += new System.EventHandler(this.comboBoxSlope_SelectedIndexChanged);
            // 
            // comboBoxRatio
            // 
            this.comboBoxRatio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRatio.FormattingEnabled = true;
            this.comboBoxRatio.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBoxRatio.Location = new System.Drawing.Point(81, 13);
            this.comboBoxRatio.Name = "comboBoxRatio";
            this.comboBoxRatio.Size = new System.Drawing.Size(40, 21);
            this.comboBoxRatio.TabIndex = 2;
            this.comboBoxRatio.SelectedIndexChanged += new System.EventHandler(this.comboBoxRatio_SelectedIndexChanged);
            // 
            // labelSlope
            // 
            this.labelSlope.AutoSize = true;
            this.labelSlope.Location = new System.Drawing.Point(12, 42);
            this.labelSlope.Name = "labelSlope";
            this.labelSlope.Size = new System.Drawing.Size(44, 13);
            this.labelSlope.TabIndex = 1;
            this.labelSlope.Text = "Percent";
            // 
            // labelRatio
            // 
            this.labelRatio.AutoSize = true;
            this.labelRatio.Location = new System.Drawing.Point(12, 18);
            this.labelRatio.Name = "labelRatio";
            this.labelRatio.Size = new System.Drawing.Size(32, 13);
            this.labelRatio.TabIndex = 0;
            this.labelRatio.Text = "Ratio";
            // 
            // LabelSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(460, 279);
            this.Controls.Add(this.groupBoxDecimalPlaces);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.labelLevellines);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.dataGridViewLevellinesSettings);
            this.Controls.Add(this.labelAxis);
            this.Controls.Add(this.comboBoxAxis);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LabelSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Label Settings";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLevellinesSettings)).EndInit();
            this.groupBoxDecimalPlaces.ResumeLayout(false);
            this.groupBoxDecimalPlaces.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxAxis;
        private System.Windows.Forms.Label labelAxis;
        private System.Windows.Forms.DataGridView dataGridViewLevellinesSettings;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Label labelLevellines;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column4;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column5;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column6;
        private System.Windows.Forms.GroupBox groupBoxDecimalPlaces;
        private System.Windows.Forms.ComboBox comboBoxSlope;
        private System.Windows.Forms.ComboBox comboBoxRatio;
        private System.Windows.Forms.Label labelSlope;
        private System.Windows.Forms.Label labelRatio;
    }
}