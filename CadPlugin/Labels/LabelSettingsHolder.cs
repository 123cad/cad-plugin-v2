﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CadPlugin
{
    public enum LabelType
        {
            RATIO, 
            PERCENT
        }
    public class LabelSettingsHolder
    {        
        /// <summary>
        /// (axisIndex, (levelline number, settings for levelline))
        /// </summary>
        private Dictionary<int, Dictionary<int, LevellineLabelSettings>> data;
        private Dictionary<int, string> axesNames;

        public int NumberOfDecimalPlacesForPercent = 2;
        public int NumberOfDecimalPlacesForRatio = 2;
        public LabelSettingsHolder()
        {
            data = new Dictionary<int, Dictionary<int, LevellineLabelSettings>>();
            axesNames = new Dictionary<int, string>();
        }
        public void AddAxis(int index)
        {
            AddAxis(index, "");
        }
        public void AddAxis(int index, string name)
        {
            if (data.ContainsKey(index))
                return;
            data.Add(index, new Dictionary<int, LevellineLabelSettings>());
            axesNames.Add(index, name);
        }
        public void AddLevelline(int axisIndex, int levelline)
        {
            AddAxis(axisIndex);
            if (!data[axisIndex].ContainsKey(levelline))
                data[axisIndex].Add(levelline, new LevellineLabelSettings());
        }
        
        public List<int> GetAllAxes()
        {
            return data.Keys.ToList();
        }
        /// <summary>
        /// Gets axis name.
        /// </summary>
        public string this[int axisIndex]
        {
            get
            {
                if (data.ContainsKey(axisIndex))
                    return axesNames[axisIndex];
                return "";
            }
        }
        public LevellineLabelSettings this[int axis, int levelline]
        {
            get
            {
                if (!data.ContainsKey(axis) || !data[axis].ContainsKey(levelline))
                    return null;
                return data[axis][levelline];
            }
        }
        public List<int> GetAllLevellines(int axis)
        {
            if (!data.ContainsKey(axis))
                return null;
            return data[axis].Keys.ToList();
        }
    }

    public class LevellineLabelSettings
    {
        public string Name;
        public bool Display;
        public LabelType Type;
        public double TextSize;
    }
}
