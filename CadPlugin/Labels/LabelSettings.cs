﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CadPlugin
{
    public partial class LabelSettings : Form
    {
        private static LabelSettingsHolder data;
        /// <summary>
        /// Divide this value with 100 to get actual height of text (it should be 0.05, 0.15, 0.1 ...). It is done like this because of easier comparing
        /// </summary>
        private static List<int> LabelTextHeight = new List<int>() { 5, 10, 15, 20 ,30, 40, 50 };

        private static bool Canceled;
        /// <summary>
        /// Must be at least one axis with at least one levelline. Returns false if canceled.
        /// </summary>
        public static bool SetLabels(LabelSettingsHolder data)
        {
            Canceled = true;//if user exists on X button, we consider it as close
            LabelSettings.data = data;
            LabelSettings form = new LabelSettings();
            form.ShowDialog();
            return !Canceled;
        }
        private List<int> axes;
        //private List<int> levellines;

        /// <summary>
        /// Bugfix - 29.1.2016. Axes ids don't have to start from 0 - they can be any number. So instead of using
        /// index of ListBox as axis id, we use that index to get axis id from axisIdsByIndex;
        /// </summary>
        private List<int> axesIdsByIndex;
        private LabelSettings()
        {
            InitializeComponent();
            SetText();
            axes = new List<int>();
            axes = data.GetAllAxes();
            axesIdsByIndex = new List<int>();
            comboBoxRatio.SelectedIndex = data.NumberOfDecimalPlacesForRatio;
            comboBoxSlope.SelectedIndex = data.NumberOfDecimalPlacesForPercent;
            if (axes.Count == 1)
            {
                comboBoxAxis.Items.Add(axes[0].ToString() + " - " + data[axes[0]]);
                axesIdsByIndex.Add(axes[0]);
                comboBoxAxis.SelectedIndex = 0;
                comboBoxAxis.Enabled = false;
            }
            else
            {
                int counter = 0;
                foreach (int key in axes)
                {
                    comboBoxAxis.Items.Add(key.ToString() + " - " + data[key]);
                    axesIdsByIndex.Add(axes[counter++]);
                }
                comboBoxAxis.SelectedIndex = 0;
            }
        }
        private string ratio;
        private string percent;
        private void SetText()
        {
            ratio = "Ratio";
            percent = "Percent";

            labelRatio.Text = "Verhältnis";
            labelSlope.Text = "Prozent";

            labelAxis.Text = "Achsen-Name";
            labelLevellines.Text = "Horizonte";
            groupBoxDecimalPlaces.Text = "Dezimal-Stellen";
            buttonCancel.Text = "Abbruch";
            buttonOk.Text = "OK";
            Column2.HeaderText = "Horizont";
            Column3.HeaderText = "Name";
            Column4.HeaderText = "Label";
            Column5.HeaderText = "Typ";
            Column6.HeaderText = "Text-Größe";

            Text = "Neigungs-Beschriftungen";

            //TODO here language should be read from registry and appropriate language set
        }
        private void PopulateCurrentAxis()
        {
            int axisIndex = comboBoxAxis.SelectedIndex;
            dataGridViewLevellinesSettings.Rows.Clear();
            if (axisIndex == -1)
                return;
            int axisId = axesIdsByIndex[axisIndex];
            foreach (int levelline in data.GetAllLevellines(axisId))
            {
                LevellineLabelSettings settings = data[axisId, levelline];
                object[] o = new object[5];
                o[0] = levelline;
                o[1] = settings.Name;
                o[2] = settings.Display;
                DataGridViewRow row = new DataGridViewRow();
                row.Tag = levelline;
                row.CreateCells(dataGridViewLevellinesSettings, o);
                dataGridViewLevellinesSettings.Rows.Add(row);
                var type = row.Cells[3] as DataGridViewComboBoxCell;//label type
                type.Items.Add(ratio);
                type.Items.Add(percent);
                if (settings.Type == LabelType.PERCENT)
                    type.Value = percent;
                else
                    type.Value = ratio;
                int size = (int)(100 * Math.Round(settings.TextSize, 2));
                int index = LabelTextHeight.IndexOf(size);
                type = row.Cells[4] as DataGridViewComboBoxCell;//text size
                if (index == -1)
                {
                    MessageBox.Show(/*"Label text size is not good"*/"Text-Größe nicht definiert");
                    index = 0;
                }
                for (int i = 0; i < LabelTextHeight.Count; i++ )
                {                    
                    string s = ((double)LabelTextHeight[i] / 100).ToString("0.00");
                        type.Items.Add(s);
                    if (index == i)
                        type.Value = s;
                }
            }

        }

        private void comboBoxAxis_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateCurrentAxis();
        }

        private bool updating = false;
        private void dataGridViewLevellinesSettings_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (updating)
                return;
            DataGridView view = dataGridViewLevellinesSettings;
            if (view.CurrentRow == null || (view.CurrentCell.ColumnIndex != 2 && view.CurrentCell.ColumnIndex != 3 && view.CurrentCell.ColumnIndex != 4))//if not checked draw and label return
                return;
            updating = true;
            if (view.IsCurrentCellDirty)
                view.EndEdit();
            updating = false;
            if (!view.IsCurrentCellDirty)
            {
                int axisIndex = comboBoxAxis.SelectedIndex;
                int axisId = axes[axisIndex];
                int levelline = (int)dataGridViewLevellinesSettings.CurrentRow.Tag;
                switch(view.CurrentCell.ColumnIndex)
                {
                    case 2:
                        data[axisId, levelline].Display = (bool)dataGridViewLevellinesSettings.CurrentCell.Value;
                        break;
                    case 3:
                        if ((string)dataGridViewLevellinesSettings.CurrentCell.Value == percent)
                            data[axisId, levelline].Type = LabelType.PERCENT;
                        else 
                            data[axisId, levelline].Type = LabelType.RATIO;                            
                        break;
                    case 4:
                        data[axisId, levelline].TextSize = double.Parse((string)dataGridViewLevellinesSettings.CurrentCell.Value, CultureInfo.InvariantCulture);
                        break;
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            Canceled = false;
            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Canceled = true;
            Close();
        }

        private void dataGridViewLevellinesSettings_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 3 && e.ColumnIndex != 4 || e.ColumnIndex == -1 || e.RowIndex == -1)
                return;
            dataGridViewLevellinesSettings.CurrentCell = dataGridViewLevellinesSettings.Rows[e.RowIndex].Cells[e.ColumnIndex];
        }

        private void comboBoxRatio_SelectedIndexChanged(object sender, EventArgs e)
        {
            data.NumberOfDecimalPlacesForRatio = comboBoxRatio.SelectedIndex;
        }

        private void comboBoxSlope_SelectedIndexChanged(object sender, EventArgs e)
        {
            data.NumberOfDecimalPlacesForPercent = comboBoxSlope.SelectedIndex;
        }

        private void groupBoxDecimalPlaces_Enter(object sender, EventArgs e)
        {

        }


        
    }
}
