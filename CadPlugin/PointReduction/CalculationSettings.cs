﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PointReduction
{
    class CalculationSettings
    {
        public double MinX { get; set; }
        public double MinY { get; set; }
        public double MaxX { get; set; }
        public double MaxY { get; set; }

        public double FieldWidth { get; private set; } = 1;
        public double FieldHeight { get; private set; } = 1;

        public int TotalRows { get; private set; }
        public int TotalColumns { get; private set; }


        /// <summary>
        /// All values less than this one are considered equal (and remove them).
        /// </summary>
        public double HeightDelta { get; set; } = 0.1;

        public Points AllPoints { get; private set; }

        public IAlgorithm Algorithm { get; set; }
        public CalculationSettings(IAlgorithm alg, double heightDelta = 0.1, double tableFieldWidth = 1, double tableFieldHeight=1)
        {
            HeightDelta = heightDelta;
            FieldWidth = tableFieldWidth;
            FieldHeight = tableFieldHeight;
            Algorithm = alg;
        }

        public void Initialize(Points pts)
        {
            AllPoints = pts;
            FindMinAndMax();
        }
        /// <summary>
        /// Width and Height are same.
        /// </summary>
        /// <param name="length"></param>
        /// <param name="offsetTable">Is table moved from original position by length/2.
        /// Used for 2nd iteration with same length, to process neighbour cells.</param>
        public void SetFieldLength(double length, bool offsetTable = false)
        {
            FieldWidth = FieldHeight = length;
            FindMinAndMax(offsetTable);
        }
        public int CalculateColumnIndex(double xCoordinate)
        {
            return (int)((xCoordinate - MinX) / FieldWidth);
        }
        public int CalculateRowIndex (double yCoordinate)
        {
            return (int)((yCoordinate - MinY) / FieldHeight);
        }
        /// <summary>
        /// When no offset, real min and max values are used.
        /// Offset is needed when we want to process neighbour cells 
        /// (after first reduction iteration)
        /// </summary>
        /// <param name="applyOffset"></param>
        private void FindMinAndMax(bool applyOffset = false)
        {
            var first = AllPoints.AllPoints.First();
            MinX = first.X;
            MinY = first.Y;
            MaxX = MinX;
            MaxY = MinY;
            foreach(var p in AllPoints.AllPoints)
            {
                if (p.X < MinX)
                    MinX = p.X;
                else if (p.X > MaxX)
                    MaxX = p.X;

                if (p.Y < MinY)
                    MinY = p.Y;
                else if (p.Y > MaxY)
                    MaxY = p.Y;
            }

            if (applyOffset)
            {
                double offsetX = FieldWidth / 2;
                double offsetY = FieldHeight / 2;
                MinX -= offsetX;
                MinY -= offsetY;
                MaxX += offsetX;
                MaxY += offsetY;
            }

            TotalColumns = CalculateColumnIndex(MaxX) + 1;
            TotalRows = CalculateRowIndex(MaxY) + 1;
        }      
        
    }
}
