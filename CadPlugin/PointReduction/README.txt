﻿Reduce 3d points depending on given delta h (smaller value, less points are removed).

Maybe combined with esri grid at the end? No, because esri implies that 1 point is in 1 field, and here we can have multiple points in 1 field.



using QuadTree algorithm (not exactly, but mapping in similar way).
QuadTree is using a tree, but in our case we will have too many levels needed to determine new point, and here we will have mapping function for determining index of field where point belongs.

Load points, find min/max values, split into grid and all points which belong to single cell are put in a list. When added, point height is compared to all points in same cell, and if difference is less than deltaH it is ignored.

Image should help understand better.

Some paper which could be usefule
https://www.researchgate.net/publication/279179060_AUTOMATIC_DATA_REDUCTION_FROM_A_LIDAR_POINT_CLOUD_DIRECTLY_TO_A_BARE_EARTH_REPRESENTATION
https://www.arcgis.com/home/item.html?id=03388990d3274160afe240ac54763e57
