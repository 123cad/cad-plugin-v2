﻿using MyUtilities.Geometry;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PointReduction
{
    class ReducedPoints
    {
        public List<Point3d>[,] Table { get; private set; }// new List<Point3d>[10, 2];
        public int TotalRows { get; private set; } = -1;
        public int TotalColumns { get; private set; } = -1;

        public int TotalPoints { get; private set; }
        public void SetTable(List<Point3d>[,] t)
        {
            Table = t;
            TotalRows = t?.GetLength(0) ?? -1;
            TotalColumns = t?.GetLength(1) ?? -1;
        }

        /// <summary>
        /// Returns points from all dimensions of the table.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Point3d> GetAllTablePoints()
        {
            if (Table == null)
                yield break;
            for (int i = 0; i < TotalRows; i++)
            {
                for (int j = 0; j < TotalColumns; j++)
                {
                    var list = Table[i, j];
                    if (list == null)
                        continue;
                    foreach (var p in list)
                        yield return p;
                }
            }
        }
        private int Count()
        {
            int counter = 0;
            if (Table != null)
            {

                for (int i = 0; i < TotalRows; i++)
                    for (int j = 0; j < TotalColumns; j++)
                        counter += Table[i, j]?.Count ?? 0;
            }
            return counter;

        }

        /// <summary>
        /// Adds points while applying reduction algorithm.
        /// </summary>
        /// <param name="pts"></param>
        public void AddPoints(IEnumerable<Point3d> pts, CalculationSettings data)
        {

            SetTable( new List<Point3d>[data.TotalRows, data.TotalColumns]);

            data.Algorithm.ProcessPoints(pts, this, data);

            TotalPoints = Count();                                    
        }
    }
}
