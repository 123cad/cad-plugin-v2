﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PointReduction
{
    public class Statistics
    {
        internal UserSettings Settings { get; private set; }
        public long PointsLoadedFromFileDuration { get; set; }
        public long PointsProcessedDuration { get; set; }
        public long DrawReducedPointsDuration { get; set; }
        public long DrawAllPointsDuration { get; set; }

        public int TotalPointsCount { get; set; }
        public int PointsRemainingAfterReduction { get; set; }
        /// <summary>
        /// When there are multiple reduction, keep information about each one.
        /// </summary>
        public List<SingleReductionStatistics> Reductions { get; } = new List<SingleReductionStatistics>();

        private Stopwatch sw = new Stopwatch();

        internal Statistics(UserSettings settings)
        {
            Settings = settings;
        }

        public void StartTimer()
        {
            sw.Restart();
        }
        public long StopTimer()
        {
            sw.Stop();
            return sw.ElapsedMilliseconds;
        }
    }
    /// <summary>
    /// Statistics of single reduction iteration.
    /// </summary>
    public class SingleReductionStatistics
    {
        public long Duration { get; set; }
        public int PointsBeforeReduction { get; set; }
        public int PointsAfterReduction { get; set; }
    }
}
