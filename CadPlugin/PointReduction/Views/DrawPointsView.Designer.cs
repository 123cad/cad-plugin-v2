﻿namespace CadPlugin.PointReduction.Views
{
    partial class DrawPointsView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonDraw = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.checkBoxReduced = new System.Windows.Forms.CheckBox();
            this.checkBoxAllLoaded = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelTotalPoints = new System.Windows.Forms.Label();
            this.labelRemainingPoints = new System.Windows.Forms.Label();
            this.labelProcessingTime = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(28, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Total loaded points";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonDraw
            // 
            this.buttonDraw.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDraw.Location = new System.Drawing.Point(138, 207);
            this.buttonDraw.Name = "buttonDraw";
            this.buttonDraw.Size = new System.Drawing.Size(75, 23);
            this.buttonDraw.TabIndex = 2;
            this.buttonDraw.Text = "Draw";
            this.buttonDraw.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(236, 207);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // checkBoxReduced
            // 
            this.checkBoxReduced.AutoSize = true;
            this.checkBoxReduced.Checked = true;
            this.checkBoxReduced.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxReduced.Location = new System.Drawing.Point(6, 19);
            this.checkBoxReduced.Name = "checkBoxReduced";
            this.checkBoxReduced.Size = new System.Drawing.Size(70, 17);
            this.checkBoxReduced.TabIndex = 4;
            this.checkBoxReduced.Text = "Reduced";
            this.checkBoxReduced.UseVisualStyleBackColor = true;
            // 
            // checkBoxAllLoaded
            // 
            this.checkBoxAllLoaded.AutoSize = true;
            this.checkBoxAllLoaded.Location = new System.Drawing.Point(6, 42);
            this.checkBoxAllLoaded.Name = "checkBoxAllLoaded";
            this.checkBoxAllLoaded.Size = new System.Drawing.Size(66, 17);
            this.checkBoxAllLoaded.TabIndex = 5;
            this.checkBoxAllLoaded.Text = "All loded";
            this.toolTip1.SetToolTip(this.checkBoxAllLoaded, "Draw all points loaded from the file");
            this.checkBoxAllLoaded.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxReduced);
            this.groupBox1.Controls.Add(this.checkBoxAllLoaded);
            this.groupBox1.Location = new System.Drawing.Point(51, 104);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(178, 65);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Draw points";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(31, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 19);
            this.label3.TabIndex = 7;
            this.label3.Text = "Remaining points";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(31, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 19);
            this.label2.TabIndex = 8;
            this.label2.Text = "Processing time";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTotalPoints
            // 
            this.labelTotalPoints.AutoSize = true;
            this.labelTotalPoints.Location = new System.Drawing.Point(144, 37);
            this.labelTotalPoints.Name = "labelTotalPoints";
            this.labelTotalPoints.Size = new System.Drawing.Size(27, 13);
            this.labelTotalPoints.TabIndex = 9;
            this.labelTotalPoints.Text = "total";
            // 
            // labelRemainingPoints
            // 
            this.labelRemainingPoints.AutoSize = true;
            this.labelRemainingPoints.Location = new System.Drawing.Point(144, 56);
            this.labelRemainingPoints.Name = "labelRemainingPoints";
            this.labelRemainingPoints.Size = new System.Drawing.Size(52, 13);
            this.labelRemainingPoints.TabIndex = 10;
            this.labelRemainingPoints.Text = "remaining";
            // 
            // labelProcessingTime
            // 
            this.labelProcessingTime.AutoSize = true;
            this.labelProcessingTime.Location = new System.Drawing.Point(144, 75);
            this.labelProcessingTime.Name = "labelProcessingTime";
            this.labelProcessingTime.Size = new System.Drawing.Size(26, 13);
            this.labelProcessingTime.TabIndex = 11;
            this.labelProcessingTime.Text = "time";
            // 
            // DrawPointsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(323, 242);
            this.Controls.Add(this.labelProcessingTime);
            this.Controls.Add(this.labelRemainingPoints);
            this.Controls.Add(this.labelTotalPoints);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonDraw);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DrawPointsView";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Draw points";
            this.TopMost = true;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonDraw;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.CheckBox checkBoxReduced;
        private System.Windows.Forms.CheckBox checkBoxAllLoaded;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelTotalPoints;
        private System.Windows.Forms.Label labelRemainingPoints;
        private System.Windows.Forms.Label labelProcessingTime;
    }
}