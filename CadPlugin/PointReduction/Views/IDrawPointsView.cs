﻿using CadPlugin.PointReduction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PointReduction.Views
{
    public class DrawResult
    {
        public bool DrawReducedPoints { get; set; }
        public bool DrawAllPoints { get; set; }
    }
    interface IDrawPointsView
    {
        DrawResult Result { get; set; }
    }
}
