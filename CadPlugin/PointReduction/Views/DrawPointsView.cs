﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PointReduction.Views
{
    public partial class DrawPointsView : Form, IDrawPointsView
    {
        public DrawPointsView(Statistics stats)
        {
            InitializeComponent();
            buttonDraw.Click += ButtonDraw_Click;
            buttonCancel.Click += (_, __) => Close();
            Result = null;
            SetData(stats);
        }

        private void ButtonDraw_Click(object sender, EventArgs e)
        {
            Result = new DrawResult()
            {
                DrawReducedPoints = checkBoxReduced.Checked,
                DrawAllPoints = checkBoxAllLoaded.Checked
            };
            Close();
        }

        public DrawResult Result { get; set; }
        private void SetData(Statistics stats)
        {
            NumberFormatInfo nfi = new NumberFormatInfo() { NumberGroupSeparator = " " };
            string totalPointsSeparated = stats.TotalPointsCount.ToString("n0", nfi);
            labelTotalPoints.Text = totalPointsSeparated + " (100%)";
            double percent = stats.TotalPointsCount > 0 ? 100.0 * stats.PointsRemainingAfterReduction / stats.TotalPointsCount : 0;
            string percentS = DoubleHelper.ToStringInvariant(percent, 2) + "%";
            string remainingPointsSeparated = stats.PointsRemainingAfterReduction.ToString("n0", nfi);
            labelRemainingPoints.Text = remainingPointsSeparated + $" ({percentS})";

            labelProcessingTime.Text = DoubleHelper.ToStringInvariant(stats.PointsLoadedFromFileDuration + stats.PointsProcessedDuration, 0) + "ms";

            checkBoxReduced.Text += $" ({remainingPointsSeparated.PadLeft(12)})";
            checkBoxAllLoaded.Text += $" ({totalPointsSeparated.PadLeft(12)})";
        }
    }
}
