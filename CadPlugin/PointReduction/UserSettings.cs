﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PointReduction
{
    class UserSettings
    {
        /// <summary>
        /// Size of table cell.
        /// </summary>
        public List<PointsTableCellSettings> FieldLength { get; private set; } = new List<PointsTableCellSettings>() { };

        public double HeightDelta { get; set; }

        //public bool DrawReducedPoints { get; set; } = false;
        //public bool DrawAllPoints { get; set; } = false;

        public string GetCommaDelimitedLengths()
        {
            return string.Join(",", FieldLength.Select(x => x.CellLength + (x.ApplyOffset ? "(o)" : "")));
        }

    }
}
