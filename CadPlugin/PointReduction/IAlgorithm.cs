﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PointReduction
{
    interface IAlgorithm
    {
        /// <summary>
        /// Add points and reduces them.
        /// </summary>
        /// <param name="pts"></param>
        void ProcessPoints(IEnumerable<Point3d> pts, ReducedPoints data, CalculationSettings settings);
    }
    class ReducePointWhenAdded : IAlgorithm
    {
        private void AddTableCellPoint(List<Point3d> pts, Point3d newPoint, CalculationSettings data)
        {
            foreach (var p in pts)
            {
                if (Math.Abs(p.Z - newPoint.Z) > data.HeightDelta)
                {
                    pts.Add(newPoint);
                    return;
                }
            }
        }
        public void ProcessPoints(IEnumerable<Point3d> pts, ReducedPoints data, CalculationSettings settings)
        {
            int index = 0;
            foreach (var p in pts)
            {
                index++;
                int row = settings.CalculateRowIndex(p.Y);
                int column = settings.CalculateColumnIndex(p.X);
                var l = data.Table[row, column];
                if (l == null)
                    data.Table[row, column] = new List<Point3d>() { p };
                else
                {
                    AddTableCellPoint(l, p, settings);
                }
            }
        }
    }
    class ReducePointAfterAdded : IAlgorithm
    {
        private void PerformReduction(ReducedPoints data)
        {
            for (int i = 0; i < data.TotalRows; i++)
                for (int j = 0; j < data.TotalColumns; j++)
                {
                    var v = data.Table[i, j];
                    if ((v?.Count ?? 0) <= 1)
                        continue;
                    //not sure what to put here
                }
        }
        public void ProcessPoints(IEnumerable<Point3d> pts, ReducedPoints data, CalculationSettings settings)
        {
            foreach (var p in pts)
            {
                int row = settings.CalculateRowIndex(p.Y);
                int column = settings.CalculateColumnIndex(p.X);
                var l = data.Table[row, column];
                if (l == null)
                    data.Table[row, column] = new List<Point3d>() { p };
                else
                {
                    l.Add(p);
                }
            }
            PerformReduction(data);
        }
    }
}
