﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PointReduction
{
    /// <summary>
    /// Table which is constructed over points. 
    /// </summary>
    class PointsTableCellSettings
    {
        public double CellLength { get; set; }
        /// <summary>
        /// Apply offset to table (move cell by CellLength/2), so
        /// neighbour cells can be processed.
        /// </summary>
        public bool ApplyOffset { get; set; }
        public PointsTableCellSettings(double cell, bool offset = false)
        {
            CellLength = cell;
            ApplyOffset = offset;
        }
    }
}
