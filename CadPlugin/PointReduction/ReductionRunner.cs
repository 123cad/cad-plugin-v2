﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PointReduction
{
    class ReductionRunner
    {
        /// <summary>
        /// Performs point reduction for given settings.
        /// </summary>
        /// <param name="allPoints"></param>
        /// <param name="settings"></param>
        /// <param name="stats"></param>
        /// <param name="reduced"></param>
        internal static void PerformCalculations(Points allPoints, UserSettings settings, Statistics stats, ReducedPoints reduced)
        {
            stats.StartTimer();
            double lengthX = settings.FieldLength[0].CellLength;
            double lengthY = lengthX;
            CalculationSettings data = InitializeData(allPoints, settings.HeightDelta, lengthX, lengthY);
            //stats.PointsLoadedFromFileDuration = stats.StopTimer();
            //stats.StartTimer();
            ProcessPoints(stats, reduced, data, settings.FieldLength);
            stats.PointsProcessedDuration = stats.StopTimer();
            stats.PointsRemainingAfterReduction = reduced.TotalPoints;
        }

        /// <summary>
        /// Performs point reduction for given parameters.
        /// </summary>
        /// <param name="stats"></param>
        /// <param name="reduced"></param>
        /// <param name="data"></param>
        /// <param name="parameters"></param>
        private static void ProcessPoints(Statistics stats, ReducedPoints reduced, CalculationSettings data, IEnumerable<PointsTableCellSettings> parameters)
        {
            IEnumerable<Point3d> tempPoints = data.AllPoints.AllPoints;
            int oldPointsCount = data.AllPoints.TotalPoints;
            int i = 0;
            Stopwatch sw = Stopwatch.StartNew();
            foreach (var p in parameters)
            {
                var v = p;
                data.SetFieldLength(p.CellLength, p.ApplyOffset);
                sw.Restart();
                reduced.AddPoints(tempPoints, data);
                sw.Stop();
                tempPoints = reduced.GetAllTablePoints().ToList();// .GetCopyOfAllPoints();
                int newPointsCount = reduced.TotalPoints;
                //ed.WriteMessage($"Processed iteration {i} with field length {v} \n");
                SingleReductionStatistics red = new SingleReductionStatistics()
                {
                    Duration = sw.ElapsedMilliseconds,
                    PointsBeforeReduction = oldPointsCount,
                    PointsAfterReduction = newPointsCount,
                };
                stats.Reductions.Add(red);
                //Debug.WriteLine($"Processed field length {v.CellLength.ToString("0.00")} . Removed points: {oldPointsCount - newPointsCount} ");
                oldPointsCount = newPointsCount;
                i++;
            }
        }

        private static CalculationSettings InitializeData(Points allPoints, double heightDelta, double tableWidth, double tableHeight)
        {
            CalculationSettings data = new CalculationSettings(new ReducePointWhenAdded(), heightDelta, tableWidth, tableHeight);
            data.Initialize(allPoints);
            return data;
        }
    }
}
