﻿using MyUtilities.Geometry;
using CadPlugin;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using forms = System.Windows.Forms;

namespace CadPlugin.PointReduction
{
    class PointsFileLoader
    {
        public const int IncludeAllPoints = -1;
        public static int MaximumNumberOfPoints = IncludeAllPoints;
        class CoordinateString
        {
            public int StartIndex { get; set; }
            public int Length { get; set; }
        }
        class Indexes
        {
            public CoordinateString X { get; set; }  = new CoordinateString();
            public CoordinateString Y { get; set; }  = new CoordinateString();
            public CoordinateString Z { get; set; } = new CoordinateString();
        }
        private static Indexes CheckIndexes(string s)
        {
            string[] r = s.Split(' ', '\t');
            bool xSet, ySet, zSet;
            xSet = ySet = zSet = false;
            Indexes indexes = new Indexes();
            int index = 0;
            for(int i = 0; i < r.Length; i++)
            {
                string p = r[i];
                if (p.Trim() == "")
                {
                    index += p.Length + 1;
                    continue;
                }
                double d = Parse(p);
                if (!xSet)
                {
                    indexes.X.StartIndex = index;
                    indexes.X.Length = p.Length;
                    xSet = true;
                }
                else if (!ySet)
                {
                    indexes.Y.StartIndex = index;
                    indexes.Y.Length = p.Length;
                    ySet = true;
                }
                else if (!zSet)
                {
                    indexes.Z.StartIndex = index;
                    indexes.Z.Length = p.Length;
                    zSet = true;
                    break;
                }
                index += p.Length + 1;
            }
            return indexes;

        }

       private static double Parse(string s) 
        {
            return double.Parse(s, CultureInfo.InvariantCulture);
        }
        private static string[] GetCoordinates(string s, Indexes ind)
        {
            string[] p = new string[3];
            p[0] = s.Substring(ind.X.StartIndex, ind.X.Length);
            p[1] = s.Substring(ind.Y.StartIndex, ind.Y.Length);
            p[2] = s.Substring(ind.Z.StartIndex, ind.Z.Length);
            return p;
        }
        public static Points LoadPoints(StreamReader fileStream)
        {
            int index = 0;
            Points pts = null;
            using (ProgressBar bar = new ProgressBar())
            {
                IEnumerable<string> lines = null;
                List<string> l = new List<string>(MaximumNumberOfPoints > 0 ? MaximumNumberOfPoints : 1);
                int loadedPoints = 0;
                while (!fileStream.EndOfStream &&
                        (
                            MaximumNumberOfPoints == IncludeAllPoints ||
                            loadedPoints++ < MaximumNumberOfPoints
                        )
                      )
                {
                    l.Add(fileStream.ReadLine());
                }
                lines = l;
                Indexes indexes = CheckIndexes(lines.First(x => '0' <= x[0] && x[0] <= '9'));

                int totalPoints = lines.Count();
                bool calculations = true;
                pts = new Points(calculations, totalPoints);
                

                bar.Start((uint)totalPoints, "Loading points");
                bar.PerformIteration();

                int currentPoints = 0;
                foreach (string s in lines)
                {
                    index++;
                    if (string.IsNullOrEmpty(s) || s[0] < '0' || '9' < s[0])
                        continue;
                    string[] sc = GetCoordinates(s, indexes);
                    double x = Parse(sc[0]);
                    double y = Parse(sc[1]);
                    double z = Parse(sc[2]);

                    pts.AddPoint(new Point3d(x, y, z));
                    currentPoints++;
                    bar.PerformIteration();
                }
                //bar.PerformStep();
                bar.Stop();
            }
            return pts;
        }
    }
}
