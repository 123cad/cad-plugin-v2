﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PointReduction.PerformanceTesting
{
    interface ITestOutput
    {
        /// <summary>
        /// Writes test to output.
        /// </summary>
        /// <param name="s"></param>
        void Write(string s);
    }
}
