﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PointReduction
{
    class PerformanceTestsData
    {
        private static PointsTableCellSettings CreateTableSettings(double lenght, bool offset = false) => new PointsTableCellSettings(lenght, offset);
        delegate PointsTableCellSettings PointTableSettingsDelegate(double lenght, bool offset = false);
        public static IEnumerable<UserSettings> CreateTestSettings(double deltaHeight)
        {
            PointTableSettingsDelegate ct = CreateTableSettings;
            List<UserSettings> settings = new List<UserSettings>();

            double[][] lengths = new double[][]
            {
                // height delta, params[] field length (0.5 means with offset)
                new double[]{ 1.0 },
                new double[]{ 2.0 },
                new double[]{ 2.5 },

                new double[]{ 3.0 },
                new double[]{ 3.5 },

                new double[]{ 4.0 },
                new double[]{ 4.5 },

                new double[]{ 5.0 },
                new double[]{ 5.5 },

                // For any length count > 2 variations are included:
                // (x1, x2) (x1, x1(o), x2, x2(o)), (x1, x2, x1(o), x2(o))
                // (x2, x1) (x2, x2(o), x1, x1(o)), (x2, x1, x2(o), x1(o))

                new double[]{ 2.0, 3.0 },

                new double[]{ 2.0, 5.0 },

                new double[]{ 2.0, 5.0, 2.0 },
                new double[]{ 5.0, 2.0, 5.0 },

                new double[]{ 2.0, 3.0, 2.0 },
                new double[]{ 3.0, 2.0, 3.0 },

                new double[]{ 2.0, 4.0, 2.0 },
                new double[]{ 4.0, 2.0, 4.0 },
            };
            foreach (double[] l in lengths)
            {
                UserSettings u;
                if (l.Length == 1)
                {
                    u = new UserSettings() { HeightDelta = deltaHeight };
                    u.FieldLength.AddRange(l.Select(x => new PointsTableCellSettings(x, false)));
                    settings.Add(u);

                    u = new UserSettings() { HeightDelta = deltaHeight };
                    u.FieldLength.AddRange(l.Select(x => new PointsTableCellSettings(x, true)));
                    settings.Add(u);
                }
                else
                {
                    u = new UserSettings() { HeightDelta = deltaHeight };
                    u.FieldLength.AddRange(l.Select(x => new PointsTableCellSettings(x, false)));
                    settings.Add(u);

                    u = new UserSettings() { HeightDelta = deltaHeight };
                    u.FieldLength.AddRange(l.Select(x => new PointsTableCellSettings(x, false)));
                    u.FieldLength.AddRange(l.Select(x => new PointsTableCellSettings(x, true)));
                    settings.Add(u);


                    u = new UserSettings() { HeightDelta = deltaHeight };
                    foreach (var l1 in l)
                    {
                        u.FieldLength.Add(new PointsTableCellSettings(l1, false));
                        u.FieldLength.Add(new PointsTableCellSettings(l1, true));
                    }
                    settings.Add(u);
                }
            }
            return settings;
        }

    }
}
