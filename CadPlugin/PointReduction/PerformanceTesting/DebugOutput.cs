﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PointReduction.PerformanceTesting
{
    class DebugOutput : ITestOutput
    {
        public void Write(string s)
        {
            Debug.Write(s);
        }
    }
}
