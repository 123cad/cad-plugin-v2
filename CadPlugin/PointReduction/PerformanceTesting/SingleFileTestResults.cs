﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PointReduction.PerformanceTesting
{
    class SingleFileTestResults
    {
        public string FileName { get; set; }
        /// <summary>
        /// Every UserSettings has its own statistics.
        /// </summary>
        public List<Statistics> Stats { get; set; } = new List<Statistics>();
        public Points PointsData { get; set; }
        public List<UserSettings> Settings { get; set; }
    }
}
