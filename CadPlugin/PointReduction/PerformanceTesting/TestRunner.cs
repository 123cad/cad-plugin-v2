﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PointReduction.PerformanceTesting
{
    class TestRunner
    {
        private static bool DetailedReport = true;
        public enum TestType
        {
            /// <summary>
            /// Multiple delta heights for single file
            /// </summary>
            SingleFile, 
            /// <summary>
            /// Single delta height for multiple files.
            /// </summary>
            MultipleFiles
        }
        private static List<string> GetFiles(TestType type)
        {
            List<string> files = null;
            string filter = "XYZ files (*.xyz;*.dat;*.data)|*.xyz;*.dat;*.data|All Files (*.*)|*.*"; 
            switch (type)
            {
                case TestType.SingleFile:
                    {
                        OpenFileDialog ofd = new OpenFileDialog();
                        ofd.CheckFileExists = true;
                        ofd.Filter = filter;
                        if (ofd.ShowDialog() == DialogResult.OK)
                            files = new List<string>() { ofd.FileName };
                    }
                    break;
                case TestType.MultipleFiles:
                    while (true)
                    {
                        OpenFileDialog ofd = new OpenFileDialog();
                        ofd.CheckFileExists = true;
                        ofd.Filter = filter;
                        if (ofd.ShowDialog() == DialogResult.OK)
                        {
                            if (files == null)
                                files = new List<string>();
                            files.Add(ofd.FileName);
                        }
                        else
                            break;
                    }
                    break;
            }
            return files;
        }
        public static void RunPerformanceTest(TestType test, double[] deltaHeights)
        {
            List<SingleFileTestResults> results = new List<SingleFileTestResults>();
            double[] heights = null;// new double[] { 0.3, 0.5, 0.7 };

            List<string> files = GetFiles(test);
            if (files == null)
                return;
            switch (test)
            {
                case TestType.SingleFile:
                    heights = deltaHeights;
                    results = heights.Select(x => new SingleFileTestResults() { FileName = files[0] }).ToList();
                    break;
                case TestType.MultipleFiles:
                    double height = deltaHeights[0];
                    heights = Enumerable.Repeat(height, files.Count).ToArray();
                    results = files.Select(x => new SingleFileTestResults() { FileName = x }).ToList();
                    break;
            }

            Stopwatch sw = Stopwatch.StartNew();
            Task[] tasks = new Task[results.Count];
            double[] filesProcessingTimeMs = new double[results.Count];
            for (int i = 0; i < results.Count; i++)
            {
                int index = i;
                var v = results[i];
                tasks[i] = Task.Run(() =>
                {
                    Stopwatch sw1 = Stopwatch.StartNew();
                    SingleFileTests(v, heights[index]);
                    sw1.Stop();
                    filesProcessingTimeMs[index] = sw1.ElapsedMilliseconds;
                });
            }
            Task allFinished = Task.Run(() => Task.WhenAll(tasks));
            while (true)
            {
                if (allFinished.Status == TaskStatus.RanToCompletion)
                    break;
                Task.Delay(200).Wait();
            }
            sw.Stop();
            if (test == TestType.MultipleFiles)
                VerifyMultipleFilesResults(results);

            string html = PrintMultipleFilesTestResultsHtml(results, filesProcessingTimeMs, sw.ElapsedMilliseconds, test);
            //string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            var path = GlobalConfig.DirectoryManager.Instance.DocumentsAsciiDir;
            string outputFileName = "reduce test results.html";
            string filePath = path + outputFileName;
            File.WriteAllText(filePath, html);
            //output.Write("\nFINISHED\n\n");
            Process.Start(filePath);
        }
        /// <summary> 
        /// When loading 2 same files (for multiple files test) compare results - to validate multithreading aspect of test (no race conditions, etc...)
        /// </summary>
        private static void VerifyMultipleFilesResults(List<SingleFileTestResults> results)
        {
            string[] fileNames = results.Select(x => x.FileName).ToArray();
            for (int i = 0; i < fileNames.Length - 1; i++)
            {
                for(int j = i + 1; j < fileNames.Length; j++)
                {
                    if (fileNames[i] == fileNames[j])
                    { 
                        string errors = "";
                        for (int k = 0; k < results[i].Stats.Count; k++)
                            if (!ValidateResults(results[i].Stats[k], results[j].Stats[k]))
                            {
                                errors +=  $"{results[i].Stats[k].Settings.GetCommaDelimitedLengths()} / ";
                            } 
                        if (errors != "")
                        {
                            string msg = $"Results for files \"({i}){Path.GetFileName(fileNames[i])}\" and \"({j}){Path.GetFileName(fileNames[j])}\" do not match!" +
                                $"Lengths: {errors}";
                            MessageBox.Show(msg, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }
        private static bool ValidateResults(Statistics left, Statistics right)
        {
            if (left.PointsRemainingAfterReduction != right.PointsRemainingAfterReduction)
                return false;
            return true;
        }
        /// <summary>
        /// Calculates data for multiple settings (without drawing, just performance testing).
        /// </summary>
        /// <param name="settings"></param>
        //internal static void RunPerformanceTests(ITestOutput output)
        //{
        //    output.Write("Select any number of files to test: ");
        //    var noOutput = new NoOutput();
        //    var debugOutput = new NoOutput();// DebugOutput();
        //    List<SingleFileTestResults> results = new List<SingleFileTestResults>();
        //    while (true)
        //    {
        //        OpenFileDialog ofd = new OpenFileDialog();
        //        ofd.CheckFileExists = true;
        //        ofd.Filter = "XYZ files (*.xyz;*.dat;*.data)|*.xyz;*.dat;*.data|All Files (*.*)|*.*";
        //        if (ofd.ShowDialog() == DialogResult.OK)
        //        {
        //            results.Add(new SingleFileTestResults() { FileName = ofd.FileName });
        //        }
        //        else
        //            break;
        //    }
        //    if (results.Count == 0)
        //    {
        //        output.Write("Canceled testing.");
        //        return;
        //    }
        //    Stopwatch sw = Stopwatch.StartNew();
        //    Task[] tasks = new Task[results.Count];
        //    double[] filesProcessingTimeMs = new double[results.Count];
        //    double deltaHeight = 0.3;
        //    for (int i = 0; i < results.Count; i++)
        //    {
        //        int index = i;
        //        var v = results[i];
        //        tasks[i] = Task.Run(() =>
        //        {
        //            Stopwatch sw1 = Stopwatch.StartNew();
        //            SingleFileTests(v, deltaHeight);
        //            sw1.Stop();
        //            filesProcessingTimeMs [index]= sw1.ElapsedMilliseconds;
        //        });
        //        debugOutput.Write($"Started task {i}");
        //    }
        //    Debug.WriteLine("Awaiting all tasks");
        //    Task allFinished = Task.Run(() => Task.WhenAll(tasks));
        //    //Task waiter = Task.Run(() =>
        //    while(true)
        //    {
        //        if (allFinished.Status == TaskStatus.RanToCompletion)
        //            break;
        //        Task.Delay(200).Wait();
        //    }
        //    sw.Stop();
        //    Debug.WriteLine("Await finished, continuing");
        //    string html = PrintMultipleFilesTestResultsHtml(results, filesProcessingTimeMs, sw.ElapsedMilliseconds);
        //    //string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        //    string outputFileName = "reduce test results.html";
        //    File.WriteAllText(outputFileName, html);
        //    output.Write("\nFINISHED\n\n");
        //    Process.Start(outputFileName);
        //
        //    foreach (var v in results)
        //        output.Write($"Total execution time for file: {Path.GetFileName(v.FileName)} is {v.Stats.Sum(x => x.PointsProcessedDuration)}ms\n");
        //    output.Write($"Total execution time of all files: {sw.ElapsedMilliseconds}ms\n");
        //}

        private static void SingleFileTests(SingleFileTestResults results, double deltaHeight)// string fileName)
        {
            Points allPoints;
            using (StreamReader fileReader = new StreamReader(results.FileName))
                allPoints = PointsFileLoader.LoadPoints(fileReader);
            if (allPoints.TotalPoints == 0)
                return;
            results.PointsData = allPoints;

            IEnumerable<UserSettings> settings = PerformanceTestsData.CreateTestSettings(deltaHeight);
            results.Settings = settings.ToList();

            Func<double, string> convert = (d) => DoubleHelper.ToStringInvariant(d, 2);
            int index = 0;
            Task[] tasks = new Task[settings.Count()];
            foreach (UserSettings sett in settings)
            {
                //if (index < tasks.Length)
                Statistics stat = new Statistics(sett);
                results.Stats.Add(stat);
                tasks[index] = Task.Run(() => CalculateReductionForSingleSettings(stat, allPoints, convert, index, sett));
                index++;
            }
            Task.WaitAll(tasks);
            //debugOutput.Write("Finished\n");
        }

        private static void CalculateReductionForSingleSettings(Statistics stats, Points allPoints, Func<double, string> convert, int index, UserSettings sett)
        {
            //string s = $"Start of iteration: {index}    ";
            //s += $" Height delta: {convert(sett.HeightDelta)} ";
            //if (!DetailedReport)
            //    s += " Lengths: " + string.Join(", ", sett.FieldLength.Select(x => x.CellLength + (x.ApplyOffset ? "(o)" : "")));
            //s += "\n----------------------\n";
            //editorOutput.Write(s);
            //debugOutput.Write(s);

            //Statistics stats = new Statistics();
            //results.Stats.Add(stats);
            ReducedPoints reduced = new ReducedPoints();
            ReductionRunner.PerformCalculations(allPoints, sett, stats, reduced);
            stats.TotalPointsCount = allPoints.TotalPoints;

            //s = "";
            //for (int i = 0; i < sett.FieldLength.Count; i++)
            //{
            //    var reduction = stats.Reductions[i];
            //    double percentReduced = 100.0 * reduction.PointsAfterReduction / allPoints.TotalPoints;
            //    string length = convert(sett.FieldLength[i].CellLength);
            //    string offset = sett.FieldLength[i].ApplyOffset ? "[o]" : "";
            //    long duration = reduction.Duration;
            //    //if (DetailedReport)
            //    //    s += $"Field length: {length} {offset}  Duration: {duration}ms  Percent remaining (of total): {convert(percentReduced)}%  \n";
            //}
            //if (DetailedReport)
            //    s += "***************************\n";
            //double percentFinal = 100.0 * stats.PointsRemainingAfterReduction / allPoints.TotalPoints;
            //s += $"Remaining points: {stats.PointsRemainingAfterReduction}  (Percent of starting points: {convert(percentFinal)}%)\n";
            //s += $"Total processing duration: {stats.PointsProcessedDuration}ms\n";
            //s += "---------------------\n\n";
            //debugOutput.Write(s);
            //editorOutput.Write(s);
        }

        private static string PrintMultipleFilesTestResultsHtml(List<SingleFileTestResults> results, double[] fileTimesMs, double totalTimeMs, TestType test)
        {
            StringBuilder b = new StringBuilder();
            b.Append("<html><head>");
            b.Append("<style> " +
                "table {border-collapse: collapse}" +
                "td{border-left:1px solid black; padding:3px 5px;} " +
                "td{border-bottom:1px solid black;} " +
                ".mycolor{background-color:lightgrey} " +
                "tr:hover { border: 2px solid black}" +
                ".best10Percent {background-color: #e8b9b9}" +
                ".minimumPoint { background-color: #aae6aa}" +
                "</style>");
            b.Append("</head><body>");
            {
                // Header row.
                b.Append("<table>");
                {
                    // Number of columns that each files data occupies.
                    int singleFileColumns = test == TestType.SingleFile ? 4 : 3;
                    string lTd = test == TestType.MultipleFiles ? "<td>Delta Z</td>":"";
                    string s = $"<tr><td stlye='padding-right:10px;'>Lengths</td>{lTd}<td colspan='{singleFileColumns}'>" + string.Join($"</td><td colspan='{singleFileColumns}'>", results.Select(x => Path.GetFileName(x.FileName))) + "</td></tr>";
                    b.Append(s);
                    int totalFiles = results.Count;
                    s = "<tr><td>&nbsp</td>";
                    for (int i = 0; i < totalFiles; i++)
                    {
                        if (test == TestType.SingleFile)
                            s += "<td>delta Z</td>";
                        else if (i == 0)
                            s += "<td></td>";
                        s += "<td>Remaining pts</td><td>Time</td><td>Percent</td>";
                    }
                    s += "</tr>";
                    b.Append(s);


                    // Smallest number of remaining points in all tests for a single file.
                    int[] minimumPointsInFile = new int[results.Count];
                    // Range of smallest numbers of remaining points in all tests for a single file.
                    int[] lowestPointsInFile = new int[results.Count];
                    for (int m = 0; m < results.Count; m++)
                    {
                        var current = results[m];
                        var sortedPoints = current.Stats.Select(x => x.PointsRemainingAfterReduction).OrderBy(x => x).ToList();
                        // Bound of lowest points index.
                        int index = (int)(sortedPoints.Count * .11);
                        lowestPointsInFile[m] = sortedPoints[index];
                        minimumPointsInFile[m] = sortedPoints[0];
                    }
                    
                    for (int i = 0; i < results[0].Settings.Count; i++)
                    {
                        s = "";
                        b.Append("<tr>");
                        {
                            string lengths = string.Join(",", results[0].Settings[i].FieldLength.Select(x => x.CellLength + (x.ApplyOffset ? "(o)" : "")));
                            s += $"<td>{lengths}</td>";
                            b.Append(s);
                            s = "";
                            //double height = results[0].Settings[i].HeightDelta;
                            //s = $"<td>{DoubleHelper.ToStringInvariant(height, 2)}</td>";
                            double minPercent = results.Min(x =>
                            {
                                var st = x.Stats[i];
                                return 100.0 * st.PointsRemainingAfterReduction / st.TotalPointsCount;
                            });
                            for (int j = 0; j < results.Count; j++)
                            {
                                //s = "<td></td>";
                                var r = results[j];
                                Statistics stats = r.Stats[i];
                                // Lowest number of remaining points in all tests for file.
                                bool isMinimum = stats.PointsRemainingAfterReduction <= minimumPointsInFile[j];
                                bool isMinimumRange = stats.PointsRemainingAfterReduction <= lowestPointsInFile[j];
                                string cssClass = "class='";
                                if (j % 2 == 0)
                                    cssClass += " mycolor ";
                                if (isMinimumRange)
                                    cssClass += " best10Percent ";
                                if (isMinimum)
                                    cssClass += "minimumPoint ";
                                cssClass += "'";
                                //string cssClass = j % 2 == 0 ? $"class='mycolor {(isMinimumRange ? "best10Percent " : "")} {(isMinimum?"minimumPoint ":"")}'" : "";
                                if (j == 0 || test == TestType.SingleFile)
                                {
                                    double height = r.Settings[i].HeightDelta;
                                    string s1 = test == TestType.SingleFile ? cssClass : "";
                                    s += $"<td {s1}>{DoubleHelper.ToStringInvariant(height, 2)}</td>";
                                }
                                s += $"<td {cssClass}>{stats.PointsRemainingAfterReduction}</td>";
                                s += $"<td {cssClass}>{stats.PointsProcessedDuration}ms</td>";
                                double percent = 100.0 * stats.PointsRemainingAfterReduction / stats.TotalPointsCount;
                                string style = "";
                                if (Math.Abs(percent - minPercent) < 0.0001)
                                    style = " style='border:2px solid black' ";
                                s += $"<td {cssClass} {style}>{DoubleHelper.ToStringInvariant(percent, 2)}%</td>";
                                //b.Append(s);
                            }
                            b.Append(s);
                        }
                        b.Append("</tr>");
                    }

                    b.Append("<tr><td colspan='1'>Real time</td>");
                    if (test == TestType.MultipleFiles)
                        b.Append("<td></td>");
                    foreach (var d in fileTimesMs)
                    {
                        b.Append(string.Join("",Enumerable.Repeat("<td>&nbsp</td>", singleFileColumns - 2)));
                        b.Append($"<td>{DoubleHelper.ToStringInvariant(d / 1000.0, 2)}s</td>");
                        b.Append("<td></td>");
                    }
                    b.Append("</tr>");
                    b.Append("</table>");
                    b.Append($"<p>Total program running time: {DoubleHelper.ToStringInvariant(totalTimeMs / 1000.0, 2)}s</p>");
                }
                b.Append("</body></html>");
            }
            return b.ToString();
        }
    }
}
