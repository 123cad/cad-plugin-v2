﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PointReduction
{
    class Points
    {
        public IEnumerable<Point3d> AllPoints { get { return points; } }
        private List<Point3d> points { get; set; } = new List<Point3d>();
        public int TotalPoints => points.Count;
        public double MinimumDistanceX { get; set; } = 1;
        public double MinimumDistanceY { get; set; } = 1;
        public double MaximumHeight { get; set; }
        public double MinimumHeight { get; set; }
        /// <summary>
        /// Last added point.
        /// </summary>
        private Point3d lastPoint { get; set; }
        /// <summary>
        /// Perform calculation of edge values (increases execution time).
        /// </summary>
        public bool CalculateEdgeValues { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="totalPoints">Improve performance little bit by setting total size.</param>
        public Points(bool edgeValues = true, int totalPoints = 0)
        {
            CalculateEdgeValues = edgeValues;
            points = new List<Point3d>(totalPoints > 0 ? totalPoints : 100);
        }
        public void AddPoint(Point3d pt)
        {
            points.Add(pt);
            if (!CalculateEdgeValues)
                return;
            if (points.Count == 1)
            {
                MaximumHeight = pt.Z;
                MinimumHeight = pt.Z;
            }
            else
            {
                double d = Math.Abs(pt.X - lastPoint.X);
                if (0.0001 < d && d < MinimumDistanceX)
                    MinimumDistanceX = d;
                d = Math.Abs(pt.Y - lastPoint.Y);
                if (0.0001 < d && d < MinimumDistanceY)
                    MinimumDistanceY = d;
                if (MaximumHeight < pt.Z)
                    MaximumHeight = pt.Z;
                if (MinimumHeight > pt.Z)
                    MinimumHeight = pt.Z;
            }
            lastPoint = pt;
        }
    }
}
