﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.PointReduction.CAD
{
    class UserSettingsHelper
    {

        private static double LastHeightDelta = 0.3;
        private static string LastTableCellWidth = "2";

        static UserSettingsHelper()
        {
            //var user = new UserSettings();
            //LastHeightDelta = user.HeightDelta;
            //LastTableCellWidth = string.Join(",", user.FieldLength.Select(x => x.CellLength.ToString("0.0", CultureInfo.InvariantCulture)).ToArray());
        }

        public static UserSettings GetUserSettings(Editor ed)
        {
            UserSettings user = new UserSettings();
            PromptDoubleOptions heightOpts = new PromptDoubleOptions("Height delta:");
            heightOpts.DefaultValue = LastHeightDelta;
            PromptDoubleResult heightRes = ed.GetDouble(heightOpts);
            if (heightRes.Status != PromptStatus.OK)
                return null;
            user.HeightDelta = heightRes.Value;
            LastHeightDelta = user.HeightDelta;

            PromptStringOptions lengthOpts = new PromptStringOptions("Field lengths (split by ';' , duplicate for offset(2;2) ): ");
            lengthOpts.DefaultValue = LastTableCellWidth;
            PromptResult res = ed.GetString(lengthOpts);
            if (res.Status != PromptStatus.OK)
                return null;
            string[] lengths = res.StringResult.Split(';');
            if (!lengths?.Any() ?? false)
                return null;
            user.FieldLength.Clear();
            double last = -1;
            foreach (var s in lengths)
            {
                double d;
                if (DoubleHelper.TryParseInvariant(s, out d))
                {
                    bool offset = last > 0 ? Math.Abs(d - last) < 0.0001 : false;
                    user.FieldLength.Add(new PointsTableCellSettings(d, offset));
                    last = d;
                }
            }
            LastTableCellWidth = res.StringResult;
            if (user.FieldLength.Count == 0)
            {
                user.FieldLength.Add(new PointsTableCellSettings(1));
                LastTableCellWidth = "1";
            }

            //string[] drawOptions = new string[]
            //{
            //    " 1 - None",
            //    " 2 - Only Reduced points",
            //    " 3 - All loaded points",
            //    " 4 - Both"
            //};
            //PromptIntegerOptions drawOpts = new PromptIntegerOptions("");
            //drawOpts.Keywords.Add(drawOptions[0]);
            //drawOpts.Keywords.Add(drawOptions[1]);
            //drawOpts.Keywords.Add(drawOptions[2]);
            //drawOpts.Keywords.Add(drawOptions[3]);
            //drawOpts.Keywords.Default = drawOptions[0];
            //drawOpts.AllowArbitraryInput = false;
            //drawOpts.AllowNone = false;
            //PromptIntegerResult drawRes = ed.GetInteger(drawOpts);
            //if (drawRes.Status != PromptStatus.OK)
            //    return null;
            //bool drawR = false, drawA = false;
            //switch (drawRes.Value)
            //{
            //    case 1: break;
            //    case 2: drawR = true; break;
            //    case 3: drawA = true; break;
            //    case 4: drawR = drawA = true; break;
            //}
            //user.DrawAllPoints = drawA;
            //user.DrawReducedPoints = drawR;

            return user;
        }
    }
}
