﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using my = MyUtilities.Geometry;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.PointReduction.CAD
{
    class PointCadDrawer
    {

        public static int DrawReducedPoints(Database db, ReducedPoints data)
        {
            int totalPoints = 0;
            //DBPoint[] points = data.GetAllTablePoints().Select(x => new DBPoint(new Teigha.Geometry.Point3d(x.X, x.Y, x.Z))).ToArray();
            //int currentPointsIndex = 0;
            //int range = 10000;
            //bool finished = false;
            //while (!finished)
            //{
            //    using (Transaction tr = db.TransactionManager.StartTransaction())
            //    {
            //        BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
            //        BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
            //        for (int i = 0; i < range; i++)
            //        {
            //            if (i >= range || points.Length <= currentPointsIndex)
            //                break;
            //            var p = points[currentPointsIndex];
            //            DBPoint pt = p;
            //            btr.AppendEntity(pt);
            //            tr.AddNewlyCreatedDBObject(pt, true);
            //            currentPointsIndex++;
            //        }
            //
            //
            //        tr.Commit();
            //    }
            //    if (currentPointsIndex >= points.Length)
            //        break;
            //}


            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

                using (ProgressBar bar = new ProgressBar())
                { 
                    var pts = data.GetAllTablePoints();
                    bar.Start((uint)pts.Count(), "Drawing points");
                    foreach (var p in pts)
                    {
                        totalPoints++;
                        DBPoint pt = new DBPoint(new Point3d(p.X, p.Y, p.Z));
                        btr.AppendEntity(pt);
                        tr.AddNewlyCreatedDBObject(pt, true);
                        bar.PerformIteration();
                    }
                    //foreach (var p in points)
                    //for (int i = 0; i < points.Length; i++)
                    //{
                    //    //totalPoints++;
                    //    //DBPoint pt = new DBPoint();
                    //    //pt.Position = new Teigha.Geometry.Point3d(p.X, p.Y, p.Z);
                    //    var pt = points[i];
                    //    btr.AppendEntity(pt);
                    //    tr.AddNewlyCreatedDBObject(pt, true);
                    //}
                    bar.SetToFull();
                    tr.Commit();
                }
            }
            return totalPoints;
        }
        public static void DrawAllPoints(Database db, IEnumerable<my.Point3d> pts)
        {
            string layerName = "AllPoints";
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                LayerTable lt = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForWrite);

                if (!lt.Has(layerName))
                {
                    LayerTableRecord rec = new LayerTableRecord();
                    rec.Name = layerName;
                    lt.Add(rec);
                    tr.AddNewlyCreatedDBObject(rec, true);
                }
                using (ProgressBar bar = new ProgressBar())
                {
                    var currentLayer = db.Clayer;
                    db.Clayer = lt[layerName];
                    bar.Start((uint)pts.Count(), "Drawing points");
                    foreach (var p in pts)
                    {
                        DBPoint pt = new DBPoint(new Point3d(p.X, p.Y, p.Z));
                        //pt.Layer = layerName;
                        //pt.Position = new Teigha.Geometry.Point3d(p.X, p.Y, p.Z);
                        btr.AppendEntity(pt);
                        tr.AddNewlyCreatedDBObject(pt, true);
                        bar.PerformIteration();
                    }
                    bar.Stop();
                db.Clayer = currentLayer;
                tr.Commit();
                }
            }

        }
    }
}
