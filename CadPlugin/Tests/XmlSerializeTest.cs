﻿using CadPlugin.HatchToolbars;
using CadPlugin.HatchToolbars.Data;
using CadPlugin.LinetypeToolbars.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace CadPlugin.Tests
{
	class XmlSerializeTest
	{
		public static void MyTestMethod()
		{
			HatchType ht = new Predefined();
			SingleHatchData h = new SingleHatchData();
			h.HatchType = ht;
			LoadSaveManager mgr = new HatchToolbarLoadSaveManager();
			string fileName = "test1.xml";
			mgr.SaveData(Enumerable.Repeat(h, 1), fileName);
			mgr.LoadData(fileName);



			XmlSerializer ser = new XmlSerializer(typeof(HatchType));
			HatchType t = new Predefined();
			string xml = "";

			using (StringWriter sw = new StringWriter())
			{
				using (XmlWriter writer = XmlWriter.Create(sw))
				{
					ser.Serialize(writer, t);
					xml = sw.ToString();
					//ser.Deserialize()
				}
			}

			using (StringReader sr = new StringReader(xml))
			{
				using (XmlReader reader = XmlReader.Create(sr))
				{
					var p = ser.Deserialize(reader);
				}
			}

		}
	}
	class HatchToolbarLoadSaveManager:LoadSaveManager
	{
		protected override string GlobalName
		{
			get
			{
				return "HatchToolbarSettings";
			}
		}
		private static List<Type> __TypesUsed = new List<Type>()
		{
			typeof(Predefined),
			typeof(Solid),
			typeof(UserDefined)
		};
		protected override IEnumerable<Type> TypesUsed
		{
			get
			{
				return __TypesUsed;
			}
		}
	}
	class LinetypeToolbarLoadSaveManager:LoadSaveManager
	{
		protected override string GlobalName
		{
			get
			{
				return "LinetypeToolbarSettings";
			}
		}
		private static List<Type> __TypesUsed = new List<Type>()
		{
			typeof(LinetypeType)
		};
		protected override IEnumerable<Type> TypesUsed
		{
			get
			{
				return __TypesUsed;
			}
		}
	}
	abstract class LoadSaveManager
	{
		protected abstract IEnumerable<Type> TypesUsed { get; }

		protected virtual string GlobalName { get; }
		public void SaveData(IEnumerable<object> objs, string fileName)
		{
			// Create new file
			// Write XML structure
			// Create
			XmlDocument doc = new XmlDocument();
			XmlConverter converter = new XmlConverter(TypesUsed);
			using (XmlTextWriter writer = new XmlTextWriter(fileName, null))
			{
				writer.Indentation = 1;
				writer.IndentChar = '\t';
				writer.Formatting = Formatting.Indented;

				writer.WriteStartDocument();
				writer.WriteStartElement(GlobalName);
				{
					writer.WriteStartElement("datalist");
					foreach (object o in objs)
					{
						writer.WriteStartElement("item");
						{
							string data = converter.ToXml(o, writer);
							// Save to file
							//writer.WriteRaw(data);
							//XmlConverter.ToObject(data, o.GetType());
						}
						writer.WriteEndElement();
					}
					writer.WriteEndElement();
				}
				writer.WriteEndElement();
				writer.WriteEndDocument();
				//doc.Save(writer);
				//writer.Flush();
			}
			//File.AppendAllText(fileName, str.ToString());
			//str.Flush();
		}
		public List<object> LoadData(string fileName)
		 {
			List<object> objs = new List<object>();
			XmlConverter converter = new XmlConverter(TypesUsed);
			XmlDocument doc = new XmlDocument();
			doc.Load(fileName);
			XmlElement element = doc.DocumentElement;
			if (element.LocalName != GlobalName)
				return null;
			XmlNode node = element.SelectSingleNode("datalist");
			XmlNodeList list = node.SelectNodes("item");
			foreach(XmlNode n in list)
			{
				object obj = converter.ToObject(n.InnerXml);
				objs.Add(obj);
			}

			return objs;
		}
	}
	class XmlConverter
	{
		private IEnumerable<Type> usedTypes;
		private List<XmlSerializer> serializers;
		public XmlConverter(IEnumerable<Type> usedTypes)
		{
			this.usedTypes = usedTypes;
			serializers = new List<XmlSerializer>();
			foreach (Type t in usedTypes)
				serializers.Add(new XmlSerializer(t));
		}
		public string ToXml(object obj, XmlWriter writer)
		{
			string xml = "";
			XmlSerializer serializer = new XmlSerializer(obj.GetType());
			//using (StringWriter sw = new StringWriter())
			{
				//using (XmlWriter xwr = XmlWriter.Create(sw))
				{
					serializer.Serialize(writer, obj);
					//xml = sw.ToString();
				}
				StringWriter sw = new StringWriter();
				XmlWriter wrt = XmlWriter.Create(sw);
				serializer.Serialize(wrt, obj);
				xml = sw.ToString();
			}
			return xml;
		}
		public object ToObject(string xml)
		{
			object obj = null;
			try
			{
				using (StringReader sReader = new StringReader(xml))
				{
					using (XmlReader xReader = XmlReader.Create(sReader))
					{
						foreach (XmlSerializer sr in serializers)
							if (sr.CanDeserialize(xReader))
							{
								obj = sr.Deserialize(xReader);
								break;
							}
					}
				}
			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.WriteLine(e);
			}
			return obj;
		}
	}

}
