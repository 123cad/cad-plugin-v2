﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Tests
{
    class DrawImageTest
    {
        public static void Start()
        {
            //Start1();
            //return;
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;
            Database db = doc.Database;
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                TransactionData data = new TransactionData(doc, tr);
                CADImageManager mgr = new CADImageManager(tr, doc.Database);
                string image1 = @"C:\Program Files\123CAD ingenieursoftware\123CAD 3.7\Support\Images\damage_crack.png";
                var info = mgr.LoadImage(image1);
                info.Properties.DirectionVector = new MyUtilities.Geometry.Vector3d(2, 1, 0).GetUnitVector();
                System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                List<Entity> entities = new List<Entity>();
                //for (int i = 0; i < 1000; i++)
                    mgr.DrawImage(entities, info);
                sw.Stop();
                ed.WriteMessage($"Time to draw {sw.ElapsedMilliseconds}");
                sw.Restart();
                tr.Commit();
                sw.Stop();
                ed.WriteMessage($"Time to commit {sw.ElapsedMilliseconds}");
            }
        }
        public static void Start1()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;
            Database db = doc.Database;
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                // Define the name and image to use
                string imageName = "pipe";
                string fileName = @"C:\Program Files\123CAD ingenieursoftware\123CAD 3.7\Support\Images\damage_crack.png";

                RasterImageDef rasterImageDef;
                bool rasterDefCreated = false;
                ObjectId imgDefId;

                // Get the image dictionary
                ObjectId imgDctID = RasterImageDef.GetImageDictionary(db);

                // Check to see if the dictionary does not exist, it not then create it
                if (imgDctID.IsNull)
                {
                    imgDctID = RasterImageDef.CreateImageDictionary(db);
                }

                // Open the image dictionary
                DBDictionary imageDictionary = tr.GetObject(imgDctID, OpenMode.ForRead) as DBDictionary;

                // Check to see if the image definition already exists
                if (imageDictionary.Contains(imageName))
                {
                    imgDefId = imageDictionary.GetAt(imageName);

                    rasterImageDef = tr.GetObject(imgDefId, OpenMode.ForWrite) as RasterImageDef;
                }
                else
                {
                    // Create a raster image definition
                    RasterImageDef rasterImageDefNew = new RasterImageDef();
                    // Add the image definition to the dictionary
                    imageDictionary.UpgradeOpen();
                    imgDefId = imageDictionary.SetAt(imageName, rasterImageDefNew);
                    tr.AddNewlyCreatedDBObject(rasterImageDefNew, true);

                    // Set the source for the image file
                    rasterImageDefNew.SourceFileName = fileName;
                    
                    // Load the image into memory
                    rasterImageDefNew.Load();



                    rasterImageDef = rasterImageDefNew;

                    rasterDefCreated = true;
                }

                // Open the Block table for read
                BlockTable blockTable;
                blockTable = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;

                // Open the Block table record Model space for write
                BlockTableRecord modelSpaceBTR;
                modelSpaceBTR = tr.GetObject(blockTable[BlockTableRecord.ModelSpace],
                                                OpenMode.ForWrite) as BlockTableRecord;

                // Create the new image and assign it the image definition
                using (RasterImage rasterImage = new RasterImage())
                {
                    // Add the new object to the block table record and the transaction
                    modelSpaceBTR.AppendEntity(rasterImage);
                    tr.AddNewlyCreatedDBObject(rasterImage, true);

                    rasterImage.ImageDefId = imgDefId;

                    // Use ImageWidth and ImageHeight to get the size of the image in pixels (1024 x 768).
                    // Use ResolutionMMPerPixel to determine the number of millimeters in a pixel so you 
                    // can convert the size of the drawing into other units or millimeters based on the 
                    // drawing units used in the current drawing.

                    // Define the width and height of the image
                    Vector3d width;
                    Vector3d height;

                    // Check to see if the measurement is set to English (Imperial) or Metric units
                    if (db.Measurement == MeasurementValue.English)
                    {
                        width = new Vector3d((rasterImageDef.ResolutionMMPerPixel.X * rasterImage.ImageWidth) / 25.4, 0, 0);
                        height = new Vector3d(0, (rasterImageDef.ResolutionMMPerPixel.Y * rasterImage.ImageHeight) / 25.4, 0);
                    }
                    else
                    {
                        width = new Vector3d(rasterImageDef.ResolutionMMPerPixel.X * rasterImage.ImageWidth, 0, 0);
                        height = new Vector3d(0, rasterImageDef.ResolutionMMPerPixel.Y * rasterImage.ImageHeight, 0);
                    }

                    // Define the position for the image 
                    Point3d insPt = new Point3d(1, -height.Length * 2, 0.0);

                    // Define and assign a coordinate system for the image's orientation
                    CoordinateSystem3d coordinateSystem = new CoordinateSystem3d(insPt, width, height * 2);
                    rasterImage.Orientation = coordinateSystem;

                    // Set the rotation angle for the image
                    rasterImage.Rotation = 0;


                    // Connect the raster definition and image together so the definition
                    // does not appear as "unreferenced" in the External References palette.
                    //RasterImage.EnableReactors(true);
                    rasterImage.AssociateRasterDef(rasterImageDef);

                    if (rasterDefCreated)
                    {
                        rasterImageDef.Dispose();
                    }
                }


                tr.Commit();
            }
        }
    }
}
