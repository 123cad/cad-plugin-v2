﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace MyUtilities.Tests.Common
{
	class CADMTextHelper_Test
	{
		private static MText Create(Transaction tr, Database db, BlockTableRecord modelSpace, string s)
		{
			MText text = new MText();
			modelSpace.AppendEntity(text);
			tr.AddNewlyCreatedDBObject(text, true);

			text.TextHeight = 1.2;
			text.Contents = s;
			return text;
		}
		private static void DrawLine(Transaction tr, Database db, BlockTableRecord modelSpace, MyUtilities.Geometry.Point2d p, MyUtilities.Geometry.Vector2d v)
		{
			Line l = new Line();
			modelSpace.AppendEntity(l);
			tr.AddNewlyCreatedDBObject(l, true);

			l.StartPoint = p.GetAsPoint3d().ToCADPoint();
			l.EndPoint = p.Add(v).GetAsPoint3d().ToCADPoint();

		}
		public static void Test()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			Database db = doc.Database;
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
				BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

				MText[] texts = new MText[]
				{
					Create(tr, db, btr, "Test 0"),
					Create(tr, db, btr, "Test 1"),
					Create(tr, db, btr, "Test 2"),
					Create(tr, db, btr, "Test 3"),
					Create(tr, db, btr, "Test 4"),
					Create(tr, db, btr, "Test 5"),
					Create(tr, db, btr, "Test 6"),
					Create(tr, db, btr, "Test 7"),
					Create(tr, db, btr, "Test 8"),
					Create(tr, db, btr, "Test 9")
				};

				CADMTextHelper.MTextParameters pars = new CADMTextHelper.MTextParameters();
				pars.TextAlignment = CADMTextHelper.TextAlignment.TopLeft;

				MText t = null;
                MyUtilities.Geometry.Point2d point = new Geometry.Point2d(-1, 1);
				MyUtilities.Geometry.Vector2d vector = new Geometry.Vector2d(1, 0);

				int index = 0;

				// 0
				t = texts[index++];
				point = new Geometry.Point2d(-10, 10);
				vector = new Geometry.Vector2d(1, 0);
				CADMTextHelper.AlignWithVector(t, point, vector, pars);

				// 1
				t = texts[index++];
				point = new Geometry.Point2d(-10, 10);
				vector = new Geometry.Vector2d(1, 1);
				CADMTextHelper.AlignWithVector(t, point, vector, pars);


				// 2
				t = texts[index++];
				point = new Geometry.Point2d(-10, 10);
				vector = new Geometry.Vector2d(-1, 1);
				CADMTextHelper.AlignWithVector(t, point, vector, pars);


				// 3
				t = texts[index++];
				point = new Geometry.Point2d(-10, 10);
				vector = new Geometry.Vector2d(-1, 0);
				CADMTextHelper.AlignWithVector(t, point, vector, pars);

				// 4
				pars.RotateToKeepHorizont = true;
				pars.RotationType = CADMTextHelper.HorizontRotation.TextCenterPoint;
				t = texts[index++];
				point = new Geometry.Point2d(10, 10);
				vector = new Geometry.Vector2d(1, 1);
				CADMTextHelper.AlignWithVector(t, point, vector, pars);

				// 5
				t = texts[index++];
				point = new Geometry.Point2d(10, 10);
				vector = new Geometry.Vector2d(-1, 1);
				CADMTextHelper.AlignWithVector(t, point, vector, pars);

				// 6
				pars.TextAlignment = CADMTextHelper.TextAlignment.BottomMiddle;
				t = texts[index++];
				point = new Geometry.Point2d(0, 5);
				vector = new Geometry.Vector2d(1, 0);
				CADMTextHelper.AlignWithVector(t, point, vector, pars);

				// 7
				pars.RotationType = CADMTextHelper.HorizontRotation.TextMiddlePointOnLine;
				t = texts[index++];
				point = new Geometry.Point2d(0, 5);
				vector = new Geometry.Vector2d(-1, 1);
				CADMTextHelper.AlignWithVector(t, point, vector, pars);

				// 8
				pars.TextDelta = 0.5;
				t = texts[index++];
				point = new Geometry.Point2d(0, 10);
				vector = new Geometry.Vector2d(1, 0);
				CADMTextHelper.AlignWithVector(t, point, vector, pars);

				// 8
				pars.TextDelta = 1;
				t = texts[index++];
				point = new Geometry.Point2d(0, 5);
				vector = new Geometry.Vector2d(-1, 1);
				CADMTextHelper.AlignWithVector(t, point, vector, pars);


				tr.Commit();
			}
		}
	}
}
