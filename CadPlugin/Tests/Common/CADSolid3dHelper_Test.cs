﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers.PipeProfileDrawers;
using Isybau2015.Isybau2015.ReferenceTableEnums;
using CadPlugin.Sewage.Pipe3dProfiles;
using CadPlugin.Sewage.Pipe3dProfiles.PipeProfileCreators;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Tests.Common
{
    class CADSolid3dHelper_Test
	{
		private static bool firstCall = true;
		public static void Start()
		{
			if (firstCall)
			{
				firstCall = false;
				return;
			}
			//All_104_Test();
			SweepWithExistingObjectsTest();
			return;
			//Func<ComplexSolidEntities> funcOuterTrapez = () => TrapezProfil.CreateBaseOuter(2);
			//Func<ComplexSolidEntities> funcOuterEgg= () => EggProfile.CreateBaseOuter(2);
			//Func<ComplexSolidEntities> funcInnerEgg = () => EggProfile.CreateBaseInner(2);
			//ProfilePipe_Test(funcOuterTrapez, true);
		}
		/// <summary>
		/// Test of algorithm - select profile entity (must be relative to (0,0,0) and apply it to path).
		/// </summary>
		public static void SweepWithExistingObjectsTest()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			Database db = doc.Database;
			Editor ed = doc.Editor;
			double diameter = 2;
			Polyline pl;
			using (TransactionWrapper tr = doc.StartTransaction())
			{
				tr.MS.UpgradeOpen();
				PromptEntityOptions opt = new PromptEntityOptions("Select base polyline");
				opt.SetRejectMessage("Not a polyline");
				opt.AddAllowedClass(typeof(Polyline3d), true);
				opt.AddAllowedClass(typeof(Polyline), true);
				PromptEntityResult ress = ed.GetEntity(opt);
				if (ress.Status != PromptStatus.OK)
				{
					var col1 = new ComplexSolidEntitiesCollection(); 
					PipeProfileCircle.Instance.CreateEntityBase(PipeParameters.CreateCircle(diameter), col1);
					var profile = col1.First().GetProfile();
					var t = profile.BaseProfileEntity;
					profile.Dispose();
					tr.MS.AppendEntity(t);
					tr.Tr.AddNewlyCreatedDBObject(t, true);
					tr.Commit();
					return;
				}
				pl = tr.GetObjectWrite<Polyline>(ress.ObjectId);



				Entity baseEntity = (Entity)pl.Clone();
				tr.MS.AppendEntity(baseEntity);
				tr.Tr.AddNewlyCreatedDBObject(baseEntity, true);

				opt.Message = "Select path polyline.";
				ress = ed.GetEntity(opt);
				if (ress.Status != PromptStatus.OK)
					return;
				pl = tr.GetObjectWrite<Polyline>(ress.ObjectId);
				Entity pathEntity = pl;
				IEnumerable<Point3d> points = null;
				if (baseEntity is Polyline)
				{
					Polyline pll = (Polyline)pathEntity;
					points = pll.GetPoints().Select(s => s.FromCADPoint().GetWithHeight(pll.Elevation).ToCADPoint());
				}
				else
					points = ((Polyline3d)pathEntity).GetPoints(tr);

				Dictionary<object, bool> rotationMask = new Dictionary<object, bool>();
				List<ComplexSolidEntitiesCollection> collections = new List<ComplexSolidEntitiesCollection>();
				var ab = new Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagen(null);
				ab.Objektbezeichnung = "123test";
				ab.Objektart = (int)G100_Objektart.Kante;
				var kante = new Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Kante(ab);
				ab.Auswahlelement = kante;
				kante.Profil = new Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Kantes.Profil(kante);
				kante.Profil.Profilbreite = (int)(diameter * 1000);
				CadPlugin.Inventories.Pipes.Pipe pipe = new CadPlugin.Inventories.Pipes.Pipe(new CadPlugin.Inventories.DataDrawers.SewageObjectsCollection()
					, ab, null, null);
				ComplexSolidEntitiesCollection col;
				//col = new ComplexSolidEntitiesCollection(); PipeBase_S104_00.Instance.CreateEntityBase(diameter, col); collections.Add(col);
				col = new ComplexSolidEntitiesCollection(); PipeProfileCircle.Instance.CreateEntityBase(PipeParameters.CreateCircle(diameter), col); collections.Add(col);
				//col = new ComplexSolidEntitiesCollection(); PipeBase_S104_02.Instance.CreateEntityBase(diameter, col); collections.Add(col);
				//col = new ComplexSolidEntitiesCollection(); PipeBase_S104_03.Instance.CreateEntityBase(diameter, col); collections.Add(col);
				//col = new ComplexSolidEntitiesCollection(); PipeBase_S104_04.Instance.CreateEntityBase(diameter, col); collections.Add(col);
				//col = new ComplexSolidEntitiesCollection(); PipeBase_S104_05.Instance.CreateEntityBase(diameter, col); collections.Add(col);
				//col = new ComplexSolidEntitiesCollection(); PipeBase_S104_06.Instance.CreateEntityBase(diameter, col); collections.Add(col);
				//col = new ComplexSolidEntitiesCollection(); PipeBase_S104_07.Instance.CreateEntityBase(diameter, col); collections.Add(col);
				//col = new ComplexSolidEntitiesCollection(); PipeBase_S104_08.Instance.CreateEntityBase(diameter, col); collections.Add(col);
				//col = new ComplexSolidEntitiesCollection(); PipeBase_S104_09.Instance.CreateEntityBase(diameter, col); collections.Add(col);
				//col = new ComplexSolidEntitiesCollection(); PipeBase_S104_10.Instance.CreateEntityBase(diameter, col); collections.Add(col);
				


				double currentOffset = 0;
				double offsetStep = diameter * 3;

				Vector3d directionOfOffset = points.ElementAt(0).GetVectorTo(points.ElementAt(1));
				directionOfOffset = directionOfOffset.RotateBy(-Math.PI / 2, Vector3d.ZAxis);
				directionOfOffset = directionOfOffset.GetNormal();

				//foreach (var cols in collections)
				{

					Point3d position = points.First();
					position = position.Add(directionOfOffset.MultiplyBy(currentOffset));

					var currentPts = points.Select(x => x.Add(directionOfOffset.MultiplyBy(currentOffset))).ToList();
					currentOffset += offsetStep;
					Matrix3d moveT = Matrix3d.Displacement(directionOfOffset.MultiplyBy(offsetStep));
					//pl.TransformBy(moveT);

					Polyline tempPl = tr.CreateEntity<Polyline>();
					tempPl.AddPoints(currentPts.Select(x => new Point2d(x.X, x.Y)).ToArray());
					tempPl.Elevation = currentPts.First().Z;

					//foreach (var ents in cols)
					{
						//bool rotate = rotationMask.ContainsKey(ents) ? rotationMask[ents] : false;
						//var baseEntity = ents.BaseProfileEntity;
						//data.Btr.AppendEntity(ents.BaseProfileEntity);
						//tr.AddNewlyCreatedDBObject(ents.BaseProfileEntity, true);

						//ents.InnerEntities.ForEach(x =>
						//{
							//data.Btr.AppendEntity(x);
							//data.Tr.AddNewlyCreatedDBObject(x, true);
						//});
						/*if (rotate)
						{

							Matrix3d rot = Matrix3d.Rotation(Math.PI, Vector3d.ZAxis, new Point3d());
							baseEntity.TransformBy(rot);
							foreach (var inner in ents.InnerEntities)
								inner.TransformBy(rot);
						}*/

						Entity solid = null;

						//if (useExistingPolyline)
						//var complex = new ComplexSolidEntities(baseEntity);
						solid = CadPlugin.Common.CADSolid3dHelper.CreateSolidAlongPath(tr, pl, new ComplexSolidProfileManager(new ComplexSolidProfile(baseEntity)));
						//else
						//solid = CadPlugin.Common.CADSolid3dHelper.CreateSolidAlongPath(data, currentPts.Select(x => x.FromCADPoint()), ents);
						tr.MS.AppendEntity(solid);
						tr.Tr.AddNewlyCreatedDBObject(solid, true);
					}
				}
				tr.Commit();
			}
		}
		public static void AllProfilesTest()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			Database db = doc.Database;
			Editor ed = doc.Editor;
			/*PromptEntityOptions opt = new PromptEntityOptions("");
			opt.SetRejectMessage("Not a polyline");
			opt.AddAllowedClass(typeof(Polyline3d), true);
			opt.AddAllowedClass(typeof(Polyline), true);
			PromptEntityResult res = ed.GetEntity(opt);
			if (res.Status != PromptStatus.OK)
				return;*/
			using (TransactionWrapper tr = doc.StartTransaction())
			{
				tr.MS.UpgradeOpen();
				Polyline pl = tr.CreateEntity<Polyline>();
				pl.AddPoints(new Point2d(1, 0), new Point2d(1, 3));
				pl.Elevation = 1.5;
				Entity ent = pl;
				//var pl = tr.GetObjectWrite<Entity>(res.ObjectId);

				//TrapezProfil.CreateBaseOuter(2);

				IEnumerable<Point3d> points = null;
				if (ent is Polyline)
				{
					Polyline pll = (Polyline)ent;
					points = pll.GetPoints().Select(s => s.FromCADPoint().GetWithHeight(pll.Elevation).ToCADPoint());
				}
				else
					points = ((Polyline3d)ent).GetPoints(tr);

				double diameter = 2;
				//List<Tuple<bool,ComplexSolidEntities, ComplexSolidEntities>> entities = new List<Tuple<bool,ComplexSolidEntities, ComplexSolidEntities>>();
				//List<Tuple<bool,ComplexSolidEntities, ComplexSolidEntities>> entities2 = new List<Tuple<bool,ComplexSolidEntities, ComplexSolidEntities>>();
				Dictionary<object, bool> rotationMask = new Dictionary<object, bool>();
				ComplexSolidEntitiesCollection collection = new ComplexSolidEntitiesCollection();
				ComplexSolidEntitiesCollection collection2 = new ComplexSolidEntitiesCollection();
				var ab = new Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagen(null);
				var kante = new Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Kante(ab);
				ab.Auswahlelement = kante;
				kante.Profil = new Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Kantes.Profil(kante);
				kante.Profil.Profilbreite = (int)(diameter * 1000);
				CadPlugin.Inventories.Pipes.Pipe pipe = new CadPlugin.Inventories.Pipes.Pipe(new CadPlugin.Inventories.DataDrawers.SewageObjectsCollection()
					, ab, null, null);
				PipeProfileCircle.Instance.CreateEntityBase(PipeParameters.CreateCircle(diameter), collection);
				ComplexSolidProfile complex;
				Func<ComplexSolidProfile, ComplexSolidProfileManager> getMgr = (cx) => new ComplexSolidProfileManager(cx);
				complex = TrapezProfil.CreateProfile(diameter); collection.Add(getMgr(complex));rotationMask.Add(complex, true);
				complex = TrapezProfil.CreateProfile(diameter); collection2.Add(getMgr(complex));rotationMask.Add(complex, true);
				complex = EggProfile.CreateProfile(diameter); collection.Add(getMgr(complex)); rotationMask.Add(complex, false);
				complex = EggProfile.CreateProfile(diameter); collection2.Add(getMgr(complex)); rotationMask.Add(complex, false);
				complex = RectangleProfileClosed.CreateProfile(diameter); collection.Add(getMgr(complex)); rotationMask.Add(complex, true);
				complex = RectangleProfileClosed.CreateProfile(diameter); collection2.Add(getMgr(complex)); rotationMask.Add(complex, true);
				complex = RectangleProfileOpen.CreateProfile(diameter); collection.Add(getMgr(complex)); rotationMask.Add(complex, true);
				complex = RectangleProfileOpen.CreateProfile(diameter); collection2.Add(getMgr(complex)); rotationMask.Add(complex, true);
				complex = RectangleProfilArc.CreateProfile(diameter); collection.Add(getMgr(complex)); rotationMask.Add(complex, true);
				complex = RectangleProfilArc.CreateProfile(diameter); collection2.Add(getMgr(complex)); rotationMask.Add(complex, true);
				complex = CircleProfile.CreateProfile(diameter); collection.Add(getMgr(complex)); rotationMask.Add(complex, true);
				complex = CircleProfile.CreateProfile(diameter); collection2.Add(getMgr(complex)); rotationMask.Add(complex, true);
				//entities.Add(new Tuple<bool, ComplexSolidEntities, ComplexSolidEntities>(true, TrapezProfil.CreateBaseOuter(diameter), null));
				//entities.Add(new Tuple<bool, ComplexSolidEntities, ComplexSolidEntities>(false, EggProfile.CreateBaseOuter(diameter), EggProfile.CreateBaseInner(diameter)));
				//entities.Add(new Tuple<bool, ComplexSolidEntities, ComplexSolidEntities>(true, RectangleProfileClosed.CreateBaseOuter(diameter), RectangleProfileClosed.CreateBaseInner(diameter)));
				//entities.Add(new Tuple<bool, ComplexSolidEntities, ComplexSolidEntities>(true, RectangleProfileOpen.CreateBaseOuter(diameter), null));
				//entities.Add(new Tuple<bool, ComplexSolidEntities, ComplexSolidEntities>(true, RectangleProfilArc.CreateBaseOuter(diameter), null));
				//entities.Add(new Tuple<bool, ComplexSolidEntities, ComplexSolidEntities>(true, CircleProfile.CreateBaseOuter(diameter), CircleProfile.CreateBaseInner(diameter)));
				//entities2.Add(new Tuple<bool, ComplexSolidEntities, ComplexSolidEntities>(true, TrapezProfil.CreateBaseOuter(diameter), null));
				//entities2.Add(new Tuple<bool, ComplexSolidEntities, ComplexSolidEntities>(false, EggProfile.CreateBaseOuter(diameter), EggProfile.CreateBaseInner(diameter)));
				//entities2.Add(new Tuple<bool, ComplexSolidEntities, ComplexSolidEntities>(true, RectangleProfileClosed.CreateBaseOuter(diameter), RectangleProfileClosed.CreateBaseInner(diameter)));
				//entities2.Add(new Tuple<bool, ComplexSolidEntities, ComplexSolidEntities>(true, RectangleProfileOpen.CreateBaseOuter(diameter), null));
				//entities2.Add(new Tuple<bool, ComplexSolidEntities, ComplexSolidEntities>(true, CircleProfile.CreateBaseOuter(diameter), CircleProfile.CreateBaseInner(diameter)));
				//entities2.Add(new Tuple<bool, ComplexSolidEntities, ComplexSolidEntities>(true, RectangleProfilArc.CreateBaseOuter(diameter), null));
				double currentOffset = 0;
				double offsetStep = diameter * 2;

				Vector3d directionOfOffset = points.ElementAt(0).GetVectorTo(points.ElementAt(1));
				directionOfOffset = directionOfOffset.RotateBy(-Math.PI / 2, Vector3d.ZAxis);
				directionOfOffset = directionOfOffset.GetNormal();

				foreach (var ents in collection2)
				{
					Point3d position = points.First();
					position = position.Add(directionOfOffset.MultiplyBy(currentOffset));
					position = position.Add(new Vector3d(0, 0, -3));

					bool rotate = rotationMask[ents];
					var profile = ents.GetProfile();
					var baseEntity = profile.BaseProfileEntity;//  EggProfile.CreateBaseInner(2);
					Matrix3d rot = Matrix3d.Rotation(Math.PI, Vector3d.ZAxis, new Point3d());
					Matrix3d move = Matrix3d.Displacement(position.GetAsVector());
					if (rotate)
					{
						baseEntity.TransformBy(rot);
					}
					baseEntity.TransformBy(move);
					foreach (var hollow in profile.InnerEntities)
					{
						var hollowEntity = hollow;// funcOuter();// EggProfile.CreateBaseOuter(2);

						if (rotate)
						{
							hollowEntity.TransformBy(rot);
						}
						hollowEntity.TransformBy(move);
						tr.MS.AppendEntity(hollowEntity);
						tr.Tr.AddNewlyCreatedDBObject(hollowEntity, true);
					}
					currentOffset += offsetStep;
					tr.MS.AppendEntity(baseEntity);
					tr.Tr.AddNewlyCreatedDBObject(baseEntity, true);
					profile.Dispose();
				}
				currentOffset = 0;
				offsetStep = diameter * 2;
				foreach (var ents in collection)
				{
					var profile = ents.GetProfile();

					Point3d position = points.First();
					position = position.Add(directionOfOffset.MultiplyBy(currentOffset));

					var currentPts = points.Select(x => x.Add(directionOfOffset.MultiplyBy(currentOffset))).ToList();
					currentOffset += offsetStep;

					Polyline tempPl = tr.CreateEntity<Polyline>();
					tempPl.AddPoints(currentPts.Select(x => new Point2d(x.X, x.Y)).ToArray());
					tempPl.Elevation = currentPts.First().Z;

					bool rotate = rotationMask[ents];
					var baseEntity = profile.BaseProfileEntity;//  EggProfile.CreateBaseInner(2);
					//if (drawSolid)
					{
						//Matrix3d move = Matrix3d.Displacement(points.First().GetAsVector());
						//baseEntity.Entity.TransformBy(move);
						//hollowEntity.Entity.TransformBy(move);
					}

					if (rotate)
					{

						Matrix3d rot = Matrix3d.Rotation(Math.PI, Vector3d.ZAxis, new Point3d());
						baseEntity.TransformBy(rot);
						foreach(var inner in profile.InnerEntities)
							inner.TransformBy(rot);
					}

					Entity solid = null;

					solid = CadPlugin.Common.CADSolid3dHelper.CreateSolidAlongPath(tr, currentPts.Select(x=>x.FromCADPoint()), ents);

					tr.MS.AppendEntity(solid);
					tr.Tr.AddNewlyCreatedDBObject(solid, true);

					profile.Dispose();
				}
				tr.Commit();
			}
		}

		public static void All_104_Test()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			Database db = doc.Database;
			Editor ed = doc.Editor;
			PromptKeywordOptions opts = new PromptKeywordOptions("Use custom polyline?");
			opts.Keywords.Add("Yes");
			opts.Keywords.Add("No");
			opts.Keywords.Default = "Yes";
			PromptResult res = ed.GetKeywords(opts);
			bool useExistingPolyline = true;
			Polyline pl;
			if (res.Status != PromptStatus.OK)
				return;
			if (res.StringResult != "Yes")
				useExistingPolyline = false;
			/*PromptEntityOptions opt = new PromptEntityOptions("");
			opt.SetRejectMessage("Not a polyline");
			opt.AddAllowedClass(typeof(Polyline3d), true);
			opt.AddAllowedClass(typeof(Polyline), true);
			PromptEntityResult res = ed.GetEntity(opt);
			if (res.Status != PromptStatus.OK)
				return;*/
			using (TransactionWrapper tr = doc.StartTransaction())
			{
				tr.MS.UpgradeOpen();
				if (useExistingPolyline)
				{
					PromptEntityOptions opt = new PromptEntityOptions("Select polyline");
					opt.SetRejectMessage("Not a polyline");
					opt.AddAllowedClass(typeof(Polyline3d), true);
					opt.AddAllowedClass(typeof(Polyline), true);
					PromptEntityResult ress = ed.GetEntity(opt);
					if (ress.Status != PromptStatus.OK)
						return;
					pl = tr.GetObjectWrite<Polyline>(ress.ObjectId);

				}
				else
				{
					pl = tr.CreateEntity<Polyline>();
					pl.AddPoints(new Point2d(1, 0), new Point2d(1, 3));
					pl.Elevation = 1.5;
				}
				Entity ent = pl;
				//var pl = tr.GetObjectWrite<Entity>(res.ObjectId);

				//TrapezProfil.CreateBaseOuter(2);

				IEnumerable<Point3d> points = null;
				if (ent is Polyline)
				{
					Polyline pll = (Polyline)ent;
					points = pll.GetPoints().Select(s => s.FromCADPoint().GetWithHeight(pll.Elevation).ToCADPoint());
				}
				else
					points = ((Polyline3d)ent).GetPoints(tr);

				double diameter = 2;
				//List<Tuple<bool,ComplexSolidEntities, ComplexSolidEntities>> entities = new List<Tuple<bool,ComplexSolidEntities, ComplexSolidEntities>>();
				//List<Tuple<bool,ComplexSolidEntities, ComplexSolidEntities>> entities2 = new List<Tuple<bool,ComplexSolidEntities, ComplexSolidEntities>>();
				Dictionary<object, bool> rotationMask = new Dictionary<object, bool>();
				List<ComplexSolidEntitiesCollection> collections = new List<ComplexSolidEntitiesCollection>();
				ComplexSolidEntitiesCollection collection = new ComplexSolidEntitiesCollection();
				ComplexSolidEntitiesCollection collection2 = new ComplexSolidEntitiesCollection();
				var ab = new Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagen(null);
				ab.Objektbezeichnung = "123test";
				ab.Objektart = (int)G100_Objektart.Kante;
				var kante = new Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Kante(ab);
				ab.Auswahlelement = kante;
				kante.Profil = new Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Kantes.Profil(kante);
				kante.Profil.Profilbreite = (int)(diameter * 1000);
				CadPlugin.Inventories.Pipes.Pipe pipe = new CadPlugin.Inventories.Pipes.Pipe(new CadPlugin.Inventories.DataDrawers.SewageObjectsCollection()
					, ab, null, null);
				ComplexSolidEntitiesCollection col;
				PipeParameters param = new PipeParameters(diameter);
				col = new ComplexSolidEntitiesCollection(); PipeProfileCircle.Instance.CreateEntityBase(param, col); collections.Add(col);
				col = new ComplexSolidEntitiesCollection(); PipeProfileEgg.Instance.CreateEntityBase(param, col); collections.Add(col);
				col = new ComplexSolidEntitiesCollection(); PipeProfileMouth.Instance.CreateEntityBase(param, col); collections.Add(col);
				col = new ComplexSolidEntitiesCollection(); PipeProfileRectangleClosed.Instance.CreateEntityBase(param, col); collections.Add(col);
				col = new ComplexSolidEntitiesCollection(); PipeProfileRing.Instance.CreateEntityBase(param, col); collections.Add(col);
				col = new ComplexSolidEntitiesCollection(); PipeProfileRectangleOpen.Instance.CreateEntityBase(param, col); collections.Add(col);
				col = new ComplexSolidEntitiesCollection(); PipeProfileEggUnequal.Instance.CreateEntityBase(param, col); collections.Add(col);
				col = new ComplexSolidEntitiesCollection(); PipeProfileMouthUnequal.Instance.CreateEntityBase(param, col); collections.Add(col);
				col = new ComplexSolidEntitiesCollection(); PipeProfileTrapez.Instance.CreateEntityBase(param, col); collections.Add(col);
				col = new ComplexSolidEntitiesCollection(); PipeProfileTrapezDouble.Instance.CreateEntityBase(param, col); collections.Add(col);
				col = new ComplexSolidEntitiesCollection(); PipeProfileRectangleArc.Instance.CreateEntityBase(param, col); collections.Add(col);
				/*ComplexSolidEntities complex;
				complex = TrapezProfil.CreateProfile(diameter); collection.Add(complex); rotationMask.Add(complex, true);
				complex = TrapezProfil.CreateProfile(diameter); collection2.Add(complex); rotationMask.Add(complex, true);
				complex = EggProfile.CreateProfile(diameter); collection.Add(complex); rotationMask.Add(complex, false);
				complex = EggProfile.CreateProfile(diameter); collection2.Add(complex); rotationMask.Add(complex, false);
				complex = RectangleProfileClosed.CreateProfile(diameter); collection.Add(complex); rotationMask.Add(complex, true);
				complex = RectangleProfileClosed.CreateProfile(diameter); collection2.Add(complex); rotationMask.Add(complex, true);
				complex = RectangleProfileOpen.CreateProfile(diameter); collection.Add(complex); rotationMask.Add(complex, true);
				complex = RectangleProfileOpen.CreateProfile(diameter); collection2.Add(complex); rotationMask.Add(complex, true);
				complex = RectangleProfilArc.CreateProfile(diameter); collection.Add(complex); rotationMask.Add(complex, true);
				complex = RectangleProfilArc.CreateProfile(diameter); collection2.Add(complex); rotationMask.Add(complex, true);
				complex = CircleProfile.CreateProfile(diameter); collection.Add(complex); rotationMask.Add(complex, true);
				complex = CircleProfile.CreateProfile(diameter); collection2.Add(complex); rotationMask.Add(complex, true);*/
				double currentOffset = 0;
				double offsetStep = diameter * 3;

				Vector3d directionOfOffset = points.ElementAt(0).GetVectorTo(points.ElementAt(1));
				directionOfOffset = directionOfOffset.RotateBy(-Math.PI / 2, Vector3d.ZAxis);
				directionOfOffset = directionOfOffset.GetNormal();

				/*foreach (var ents in collection2)
				{
					Point3d position = points.First();
					position = position.Add(directionOfOffset.MultiplyBy(currentOffset));
					position = position.Add(new Vector3d(0, 0, -3));

					bool rotate = rotationMask[ents];
					var baseEntity = ents.BaseProfileEntity;//  EggProfile.CreateBaseInner(2);
					Matrix3d rot = Matrix3d.Rotation(Math.PI, Vector3d.ZAxis, new Point3d());
					Matrix3d move = Matrix3d.Displacement(position.GetAsVector());
					if (rotate)
					{
						baseEntity.TransformBy(rot);
					}
					baseEntity.TransformBy(move);
					foreach (var hollow in ents.InnerEntities)
					{
						var hollowEntity = hollow;// funcOuter();// EggProfile.CreateBaseOuter(2);

						if (rotate)
						{
							hollowEntity.TransformBy(rot);
						}
						hollowEntity.TransformBy(move);
						data.Btr.AppendEntity(hollowEntity);
						tr.AddNewlyCreatedDBObject(hollowEntity, true);
					}
					currentOffset += offsetStep;
					data.Btr.AppendEntity(baseEntity);
					tr.AddNewlyCreatedDBObject(baseEntity, true);
				}*/
				currentOffset = 0;
				offsetStep = diameter * 2;
				foreach (var cols in collections)
				{

					Point3d position = points.First();
					position = position.Add(directionOfOffset.MultiplyBy(currentOffset));

					var currentPts = points.Select(x => x.Add(directionOfOffset.MultiplyBy(currentOffset))).ToList();
					currentOffset += offsetStep;
					if (useExistingPolyline)
					{
						Matrix3d moveT = Matrix3d.Displacement(directionOfOffset.MultiplyBy(offsetStep));
						pl.TransformBy(moveT);
					}

					Polyline tempPl = tr.CreateEntity<Polyline>();
					tempPl.AddPoints(currentPts.Select(x => new Point2d(x.X, x.Y)).ToArray());
					tempPl.Elevation = currentPts.First().Z;

					foreach (var ents in cols)
					{
						var profile = ents.GetProfile();

						bool rotate = rotationMask.ContainsKey(ents) ? rotationMask[ents] : false;
						var baseEntity = profile.BaseProfileEntity;//  EggProfile.CreateBaseInner(2);
																//if (drawSolid)
						{
							//Matrix3d move = Matrix3d.Displacement(points.First().GetAsVector());
							//baseEntity.Entity.TransformBy(move);
							//hollowEntity.Entity.TransformBy(move);
						}
						tr.MS.AppendEntity(profile.BaseProfileEntity);
						tr.Tr.AddNewlyCreatedDBObject(profile.BaseProfileEntity, true);

						if (rotate)
						{

							Matrix3d rot = Matrix3d.Rotation(Math.PI, Vector3d.ZAxis, new Point3d());
							baseEntity.TransformBy(rot);
							foreach (var inner in profile.InnerEntities)
								inner.TransformBy(rot);
						}

						Entity solid = null;

						if (useExistingPolyline)
							solid = CadPlugin.Common.CADSolid3dHelper.CreateSolidAlongPath(tr, pl, ents);
						else
							solid = CadPlugin.Common.CADSolid3dHelper.CreateSolidAlongPath(tr, currentPts.Select(x => x.FromCADPoint()), ents);
						tr.MS.AppendEntity(solid);
                        tr.Tr.AddNewlyCreatedDBObject(solid, true);
						profile.Dispose();
					}
				}
				tr.Commit();
			}
		}
		public static void ProfilePipe_Test(Func<ComplexSolidProfile> funcOuter, bool rotate = false, Func<ComplexSolidProfile> funcInner = null)
		{

			Document doc = Application.DocumentManager.MdiActiveDocument;
			Database db = doc.Database;
			Editor ed = doc.Editor;
			PromptEntityOptions opt = new PromptEntityOptions("");
			opt.SetRejectMessage("Not a polyline");
			opt.AddAllowedClass(typeof(Polyline3d), true);
			opt.AddAllowedClass(typeof(Polyline), true);
			PromptEntityResult res = ed.GetEntity(opt);
			if (res.Status != PromptStatus.OK)
				return;
			using (TransactionWrapper tr = doc.StartTransaction())
			{
				tr.MS.UpgradeOpen();
				var pl = tr.GetObjectWrite<Entity>(res.ObjectId);

				//TrapezProfil.CreateBaseOuter(2);

				IEnumerable<Point3d> points = null;
				if (pl is Polyline)
				{
					Polyline pll = (Polyline)pl;
					points = pll.GetPoints().Select(s => s.FromCADPoint().GetWithHeight(pll.Elevation).ToCADPoint());
				}
				else
					points = ((Polyline3d)pl).GetPoints(tr);
				var baseEntity = funcOuter();//  EggProfile.CreateBaseInner(2);
				var hollowEntity = (funcInner ?? (() => { return null; }))();// funcOuter();// EggProfile.CreateBaseOuter(2);
				bool drawSolid = true;
				if (drawSolid)
				{
					//Matrix3d move = Matrix3d.Displacement(points.First().GetAsVector());
					//baseEntity.Entity.TransformBy(move);
					//hollowEntity.Entity.TransformBy(move);
				}
				if (rotate)
				{
					
					Matrix3d rot = Matrix3d.Rotation(Math.PI, Vector3d.ZAxis, new Point3d());
					baseEntity.BaseProfileEntity.TransformBy(rot);
					hollowEntity?.BaseProfileEntity.TransformBy(rot);
				}

				Entity solid = null;

				if (drawSolid)
				{
					//if (hollowEntity != null)
					//solid = CadPlugin.Common.CADSolid3dHelper.CreateHollowSolidAlongPath(data, points.Select(x => x.FromCADPoint()), baseEntity, hollowEntity);
					//else
					var mgr = new ComplexSolidProfileManager(baseEntity);
						solid = CadPlugin.Common.CADSolid3dHelper.CreateSolidAlongPath(tr, points.Select(x => x.FromCADPoint()), mgr);
					mgr.Dispose();
				}
				if (solid != null)
				{
					tr.MS.AppendEntity(solid);
					tr.Tr.AddNewlyCreatedDBObject(solid, true);
					baseEntity.BaseProfileEntity.Dispose();
					hollowEntity?.BaseProfileEntity?.Dispose();
				}
				else
				{
					tr.MS.AppendEntity(baseEntity.BaseProfileEntity);
					tr.Tr.AddNewlyCreatedDBObject(baseEntity.BaseProfileEntity, true);
					tr.MS.AppendEntity(hollowEntity.BaseProfileEntity);
					tr.Tr.AddNewlyCreatedDBObject(hollowEntity.BaseProfileEntity, true);
				}
				tr.Commit();
			}

		}
		public static void EiprofileBase_Test()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			Database db = doc.Database;
			Editor ed = doc.Editor;
			var b = EggProfile.CreateBaseInner( 2);
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				TransactionData data = new TransactionData(doc, tr); 
				data.Btr.UpgradeOpen();
				data.Btr.AppendEntity(b);
				tr.AddNewlyCreatedDBObject(b, true);
				tr.Commit();
			}
		}
		public static void HollowPipe_Test()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			Database db = doc.Database;
			Editor ed = doc.Editor;
			using (TransactionWrapper tr = doc.StartTransaction())
			{
				List<MyUtilities.Geometry.Point3d> pts = new List<MyUtilities.Geometry.Point3d>()
				{
					new MyUtilities.Geometry.Point3d(1,0,0),
					new MyUtilities.Geometry.Point3d(2,2,0),
					new MyUtilities.Geometry.Point3d(3,2,0)
				};
				Circle c;
				double diameter = 1.5;
				Entity ent = null;
				Entity hollow = null;
				c = new Circle();
				c.Center = pts.First().ToCADPoint();
				c.Diameter = diameter;
				ent = c;
				c = new Circle();
				c.Center = pts.First().ToCADPoint(); 
				c.Diameter = diameter * 0.9;
				hollow = c;
				using (var mgr = new ComplexSolidProfileManager(new ComplexSolidProfile(ent)))
					CADSolid3dHelper.CreateSolidAlongPath(tr, pts, mgr );
				tr.Commit();
			}
		}
	}
}
