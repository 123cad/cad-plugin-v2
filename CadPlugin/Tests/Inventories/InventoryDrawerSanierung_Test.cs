﻿using   CadPlugin.Inventories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventory.CAD;
using   CadPlugin.Inventories.View.SelectDrawingOptionsTypes;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Tests.Inventories
{
	/// <summary>
	/// Draws objects in grid layout, with different drawing settings.
	/// </summary>
	class InventoryDrawerSanierung_Test
	{
		class SingleSpaceData
		{
			public string Space;
			public List<Tuple<string, SelectedDrawingOptions>> MainDefault;
			public List<Tuple<string, SelectedDrawingOptions>> MainEntwaess;
			public List<Tuple<string, SelectedDrawingOptions>> MainOwner;

		}
		public static void Test()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			Database db = doc.Database;
			Editor ed = doc.Editor;
			DisplayData d = null;
			CadPlugin.Commands.InventoryCommands.GetDrawingDataFromInventory(doc, ref d);
			if (d == null)
				throw new System.Exception("Testing InventoryManager failed!");
			// Draw test data.


			SelectedDrawingOptions optSource = new SelectedDrawingOptions()
			{
				DrawType = DrawView.View2d,
				DrawSingleLine = false,
				DrawAdditionalLines = AdditionalPipeLinetype.None,
				ColorOfObjects = ObjectColors.None,
				DrawDamageSource = DrawDamage.All,
				DrawObjectWithHighestDamageClass = false
			};

			SingleSpaceData[] spaces = new SingleSpaceData[3];

			var collection = new List<Tuple<string, SelectedDrawingOptions>>[3];
			 

			// 0 - space [2d/3d/v3d]
			// 1 - draw single line  [false/true]
			// 2 - additional lines [none/s_width/double/d_fill/d_full]
			// 3 - continuous [false/true]
			// 4 - main_color [default/entwaess/owner]
			// 5 - damage [all/h_pipe/h_station/selected]
			// 6 - high_pipe [false/true]

			string spaceT = "2d";
			bool single_line = false;
			string add_lines = "none";
			bool continuous = false;
			string main = "default";
			string damage = "all";
			bool high_pipe = false;
			SingleSpaceData space = new SingleSpaceData();

			#region 2D space


			Func<string, string, List<Tuple<string, SelectedDrawingOptions>>> addSpace2d3d = (spaceName, mainType) =>
			{
				List<Tuple<string, SelectedDrawingOptions>> opts = new List<Tuple<string, SelectedDrawingOptions>>();
				spaceT = spaceName;
				single_line = false;
				add_lines = "none";
				continuous = false;
				main = mainType;
				damage = "all";
				high_pipe = false;

				// 0
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 1
				single_line = true;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 2
				add_lines = "s_width";
				continuous = true;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 3
				damage = "h_station";
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 4
				high_pipe = true;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 5
				damage = "h_pipe";
				high_pipe = false;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 6
				high_pipe = true;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 7
				single_line = false;
				add_lines = "double";
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 8
				single_line = true;
				damage = "all";
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 9
				damage = "h_station";
				high_pipe = false;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 10
				high_pipe = true;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 11
				damage = "h_pipe";
				high_pipe = false;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 12
				high_pipe = true;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 13
				single_line = false;
				add_lines = "d_fill";
				damage = "all";
				high_pipe = false;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 14
				high_pipe = true;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 15
				damage = "h_station";
				high_pipe = false;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 16
				high_pipe = true;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 17
				single_line = true;
				damage = "h_pipe";
				high_pipe = false;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				high_pipe = true;
				// 18
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 19
				add_lines = "d_full";
				damage = "all";
				high_pipe = false;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 20
				high_pipe = true;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 21
				damage = "h_station";
				high_pipe = false;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 22
				high_pipe = true;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 23
				damage = "h_pipe";
				high_pipe = false;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
				// 24
				high_pipe = true;
				opts.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));

				return opts;
			};

			space = new SingleSpaceData() { Space = "2D" };
			spaces[0] = space;
			space.MainDefault = addSpace2d3d("2d", "default");
			space.MainEntwaess = addSpace2d3d("2d", "entwaess");
			space.MainOwner= addSpace2d3d("2d", "owner");
			#endregion

			#region 3D space
			space = new SingleSpaceData() { Space = "3D" };
			spaces[1] = space;
			space.MainDefault = addSpace2d3d("3d", "default");
			space.MainEntwaess = addSpace2d3d("3d", "entwaess");
			space.MainOwner = addSpace2d3d("3d", "owner");
			#endregion

			#region Visual 3D
			space = new SingleSpaceData() { Space = "Visual 3D" };
			spaces[2] = space;

			spaceT = "v3d";
			single_line = false;
			add_lines = "none";
			continuous = false;
			main = "default";
			damage = "all";
			high_pipe = false;

			List<Tuple<string, SelectedDrawingOptions>> t;
			#region default
			t = new List<Tuple<string, SelectedDrawingOptions>>();
			space.MainDefault = t;
			t.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
			t.Add(null);
			t.Add(null);
			damage = "h_station";
			t.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
			high_pipe = true;
			t.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
			damage = "h_pipe";
			high_pipe = false;
			t.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
			high_pipe = true;
			t.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
			#endregion

			#region entwaess
			main = "entwaess";
			damage = "all";
			high_pipe = false;
			t = new List<Tuple<string, SelectedDrawingOptions>>();
			space.MainEntwaess = t;
			t.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
			t.Add(null);
			t.Add(null);
			damage = "h_station";
			t.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
			high_pipe = true;
			t.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
			damage = "h_pipe";
			high_pipe = false;
			t.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
			high_pipe = true;
			t.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
			#endregion

			#region owner
			main = "owner";
			damage = "all";
			high_pipe = false;
			t = new List<Tuple<string, SelectedDrawingOptions>>();
			space.MainOwner = t;
			t.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
			t.Add(null);
			t.Add(null);
			damage = "h_station";
			t.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
			high_pipe = true;
			t.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
			damage = "h_pipe";
			high_pipe = false;
			t.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));
			high_pipe = true;
			t.Add(createOpts(spaceT, single_line, add_lines, continuous, main, damage, high_pipe));

			#endregion
			#endregion


			// Ensure only selected columns are drawn.
			bool[] drawColumnMask = new bool[spaces[0].MainDefault.Count];
			int[] drawColumnIndexes = new int[]
			{
				//4
			};
			bool drawAllColumns = drawColumnIndexes.Length == 0;
			if (drawAllColumns)
				drawColumnMask = Enumerable.Repeat(true, drawColumnMask.Length).ToArray();
			else
				for (int i = 0; i < drawColumnIndexes.Length; i++)
				{
					int index = drawColumnIndexes[i];
					drawColumnMask[index] = true;
				}
			bool[] drawSpaceMask = new bool[spaces.Length];
			drawSpaceMask[0] = true; // 2d
			drawSpaceMask[1] = true; // 3d
			drawSpaceMask[2] = true; // visual 3d

			bool drawDefault = true;
			//drawDefault = false;
			bool drawEntwaess = true;
			//drawEntwaess = false;
			bool drawOwner = true;
			drawOwner = false;

			Vector3d verticalTranslate = new Vector3d(0, 20, 0);
			Vector3d horizontalTranslate = new Vector3d(-15, 0, 0);
			Matrix3d translate = Matrix3d.Displacement(horizontalTranslate);
			Vector3d totalHorizontalMove = new Vector3d();

			// For each space draw 3 rows: default, entwaess and owner.
			// Before each row add text (space + (default/entwaess/owner))

			for (int i = 0; i < spaces.Length; i++)
			{
				if (!drawSpaceMask[i])
					continue;

				// Draw single space as 3 rows.
				SingleSpaceData currentSpace = spaces[i];
				Action<SingleSpaceData, List<Tuple<string, SelectedDrawingOptions>>, string> drawSingleRow = (cSpace, list, rowHeader) =>
				{
					using (Transaction tr = db.TransactionManager.StartTransaction())
					{
						TransactionData data = new TransactionData(doc, tr);
						MText text = tr.CreateEntity<MText>(data.Btr);
						text.Contents = cSpace.Space + MText.LineBreak.ToUpper() + rowHeader;
						text.TextHeight = 0.5;
						text.Location = new Point3d(-3, -3, 0).Add(horizontalTranslate).Add(verticalTranslate/3.0);

						for (int j = 0; j < list.Count; j++)
						{
							var val = list[j];
							if (val != null && drawColumnMask[j])
							{
								InventoryManager.DrawDataWithSelectedOptionsTest(doc, d, ed, db, val.Item2);
								text = tr.CreateEntity<MText>(data.Btr);
								text.Contents = val.Item1;
								text.TextHeight = 0.5;
								text.Location = new Point3d(-3, -3, 0);
							}
							foreach (ObjectId oid in data.Btr)
							{
								Entity e = tr.GetObject(oid, OpenMode.ForWrite) as Entity;
								if (e == null)
									continue;
								e.TransformBy(translate);
							}
							totalHorizontalMove = totalHorizontalMove.Add(horizontalTranslate);
						}
						tr.Commit();
					}
					// Move all objects 1 row up
					using (Transaction tr = db.TransactionManager.StartTransaction())
					{
						TransactionData data = new TransactionData(doc, tr);
						Matrix3d move = Matrix3d.Displacement(totalHorizontalMove.Negate().Add(verticalTranslate));
						foreach (ObjectId oid in data.Btr)
						{
							Entity e = tr.GetObject(oid, OpenMode.ForWrite) as Entity;
							if (e == null)
								continue;
							e.TransformBy(move);
						}
						totalHorizontalMove = new Vector3d();
						tr.Commit();
					}
				};
				if (drawDefault)
					drawSingleRow(currentSpace, currentSpace.MainDefault, "default");
				if (drawEntwaess)
					drawSingleRow(currentSpace, currentSpace.MainEntwaess, "Entwaess");
				if (drawOwner)
					drawSingleRow(currentSpace, currentSpace.MainOwner, "Owner");
			}


			/*for (int i = 0; i < collection.Length; i++)
			{
				var op = collection[i];
				foreach (var val in op)
				{

					InventoryManager.DrawDataWithSelectedOptionsTest(doc, d, ed, db, val.Item2);

					using (Transaction tr = db.TransactionManager.StartTransaction())
					{
						TransactionData data = new TransactionData(doc, tr);
						MText text = tr.CreateEntity(data.Btr, EntityToCreate.MText) as MText;
						text.Contents = val.Item1;
						text.TextHeight = 0.5;
						text.Location = new Point3d(-3, -3, 0);

						//var coll = tr.GetAllObjects();

						foreach (ObjectId oid in data.Btr)
						{
							Entity e = tr.GetObject(oid, OpenMode.ForWrite) as Entity;
							if (e == null)
								continue;
							e.TransformBy(translate);
						}
						totalHorizontalMove = totalHorizontalMove.Add(horizontalTranslate);
						tr.Commit();
					}
				}
				// Move all objects 1 row up
				using (Transaction tr = db.TransactionManager.StartTransaction())
				{
					TransactionData data = new TransactionData(doc, tr);
					Matrix3d move = Matrix3d.Displacement(totalHorizontalMove.Negate().Add(verticalTranslate));
					foreach (ObjectId oid in data.Btr)
					{
						Entity e = tr.GetObject(oid, OpenMode.ForWrite) as Entity;
						if (e == null)
							continue;
						e.TransformBy(move);
					}
					totalHorizontalMove = new Vector3d();
					tr.Commit();
				}
			}*/
		}

		private static Tuple<string, SelectedDrawingOptions> createOpts(string space = "2d", bool center_line_pipe = false, string additional_lines = "none", bool continuous = false, string main_color = "default", string dmg = "all", bool damage_line = false)
		{
			SelectedDrawingOptions o = new SelectedDrawingOptions()
			{
				DrawType = DrawView.View2d,
				DrawSingleLine = false,
				DrawAdditionalLines = AdditionalPipeLinetype.None,
				ColorOfObjects = ObjectColors.None,
				DrawDamageSource = DrawDamage.All,
				DrawObjectWithHighestDamageClass = false, 
				DrawOnlySanierung = true,
				DrawSanierungConcept = true,
				DrawSanierungProcedures = true
			};
			switch (space)
			{
				case "2d": o.DrawType = DrawView.View2d; break;
				case "3d": o.DrawType = DrawView.View3d; break;
				case "v3d": o.DrawType = DrawView.ViewVisual3d; break;
			}
			o.DrawSingleLine = center_line_pipe;
			switch (additional_lines)
			{
				case "none": break;
				case "s_width": o.DrawAdditionalLines = AdditionalPipeLinetype.SingleLineWidthWidth; break;
				case "double": o.DrawAdditionalLines = AdditionalPipeLinetype.Double; break;
				case "d_fill": o.DrawAdditionalLines = AdditionalPipeLinetype.DoubleFilledMiddle; break;
				case "d_full": o.DrawAdditionalLines = AdditionalPipeLinetype.DoubleFilledAll; break;
			}
			o.UseOnlyContinuousLinetype = continuous;
			o.ColorOfObjects = ObjectColors.None;
			if (main_color == "entwaess")
				o.ColorOfObjects = ObjectColors.Status;
			if (main_color == "owner")
				o.ColorOfObjects = ObjectColors.Owner;
			switch (dmg)
			{
				case "all": o.DrawDamageSource = DrawDamage.All; break;
				case "h_pipe": o.DrawDamageSource = DrawDamage.HighestPipeClassOnly; break;
				case "h_station": o.DrawDamageSource = DrawDamage.HighestStationClassOnly; break;
				case "selected": o.DrawDamageSource = DrawDamage.SelectedClasses; break;
			}
			o.DrawObjectWithHighestDamageClass = damage_line;
			string lnBrk = MText.LineBreak.ToUpper();
			string s = "";
			//s += "space: " + o.DrawType + lnBrk;
			s += "center_line: " + (o.DrawSingleLine ? "yes" : "no") + lnBrk;
			s += "add_lines: " + o.DrawAdditionalLines + lnBrk;
			s += "status: " + o.DrawDamageSource + lnBrk;
			s += "pipe_dmg_color: " + o.DrawObjectWithHighestDamageClass + lnBrk;

			return new Tuple<string, SelectedDrawingOptions>(s, o);
		}
	}
}
