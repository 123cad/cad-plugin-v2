﻿using Inventory.CAD.GeometryAssignment.Data;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using abw = Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Inventories.GeometryAssignment;
using CadPlugin.Inventories.GeometryAssignment.View;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Tests.Inventories
{
    class GeometryAssign_MainForm_Test
    {
        static MainForm form;
        internal static void Start()
        {
            // when error happens, form remains hidden. 
            if (form != null)
                form.Visible = true;

            List<Pipe> pipes = new List<Pipe>();
            List<Shaft> shafts = new List<Shaft>();
            int index = 0;
            string name = "Shaft";
            string name1 = name + index++;
            string name2 = name + index++;
            pipes.Add(CreatePipe(name1, name2));
            shafts.Add(Createshaft(name1));
            shafts.Add(Createshaft(name2));
            name1 = name2;
            name2 = name + index++;
            pipes.Add(CreatePipe(name1, name2));



            DataManagerEventArgs args = new DataManagerEventArgs();

            args.Pipes = pipes;
            args.Shafts = shafts;
            //((List<Pipe>)args.Pipes).Add(p);

            CadManager mgr = new CadManager(Application.DocumentManager.MdiActiveDocument);
            // Start GeometryAssignment form.
            form = new MainForm(mgr);
            form.Initialize(args);

            // Not modal, so we have breakpoints.
            form.Show((System.Windows.Forms.IWin32Window)Application.MainWindow);
            form.FormClosed += (_, __) => form = null;


            Document doc = Application.DocumentManager.MdiActiveDocument;
            using (Transaction tr = doc.Database.TransactionManager.StartTransaction())
            {
                TransactionData data = new TransactionData(doc, tr);
                Circle c1 = tr.CreateEntity<Circle>(data.Btr);
                Point3d p1 = new Point3d(10, 0, 0);
                c1.Center = p1;
                c1.Diameter = 3;

                Circle c2 = tr.CreateEntity<Circle>(data.Btr);
                Point3d p2 = new Point3d(0, 10, 0);
                c2.Center = p2; 
                c2.Diameter = 4;

                CADZoomHelper.ZoomToEntities(doc.Editor, new List<Entity>() { c1, c2 }, 100);


                Polyline3d pl = tr.CreateEntity<Polyline3d>(data.Btr);
                pl.AppendPoint(new Point3d(0, 0, 0));
                pl.AppendPoint(new Point3d(1, 1, 1));

                Point3d pt1 = pl.GetPointAtParameter(0);
                pl.ReverseCurve();
                Point3d pt2 = pl.GetPointAtParameter(0);

                tr.Commit();
            }

            // Without disposing mgr, it should show message about it.
            // mgr.Dispose();
        }
        private static Pipe CreatePipe(string name, string toShaft)
        {
            AbwassertechnischeAnlagen ab = new AbwassertechnischeAnlagen(null);
            // This initialization depends only on data needed for initializing the Pipe.
            ab.Objektbezeichnung = name;
            abw.Objektarts.Kante kante = new abw.Objektarts.Kante(ab);
            ab.Auswahlelement = kante;
            kante.Material = "";
            kante.KnotenZulauf = name;
            kante.KnotenAblauf = toShaft;
            abw.Objektarts.Kantes.Profil profil = new abw.Objektarts.Kantes.Profil(kante);
            kante.Profil = profil;
            profil.Profilbreite = 150;

            Pipe p = new Pipe(ab);
            return p;
        }
        private static Shaft Createshaft(string name)
        {
            AbwassertechnischeAnlagen ab = new AbwassertechnischeAnlagen(null);
            // This initialization depends only on data needed for initializing the Pipe.
            ab.Objektbezeichnung = name;
            abw.Objektarts.Knoten kante = new abw.Objektarts.Knoten(ab);
            ab.Auswahlelement = kante;
            Shaft s = new Shaft(ab);

            return s;
        }
    }
}
