﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Inventories.Pipes.Segments;
using CadPlugin.Inventories;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Tests.Inventories
{
	class InterpolatedArcTest
	{
		public void GetPointAtDistance1()
		{
			Point3d start = new Point3d(-1, 0, 0);
			Point3d middle = new Point3d(0, 1, 1);
			Point3d end = new Point3d(1, 0, 2);
			double startDistance = 25;
			InterpolatedArc a = new InterpolatedArc(null, start, end, middle, startDistance);
			PositionVector? pp = a.GetPointAtDistance(26);


		}

		
	}
}
