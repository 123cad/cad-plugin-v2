﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Inventories.Pipes.Segments;
using CadPlugin.Inventories;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

[assembly: CommandClass(typeof(CadPlugin.Tests.InventoriesTestManager))]
namespace CadPlugin.Tests
{
	class InventoriesTestManager
	{
		internal static void RunTests()
		{
			GetPointAtDistance1();
		}
	 [CommandMethod("123_TEST_INV_1")]
		public static void GetPointAtDistance1()
		{
			Document doc = Application.DocumentManager.MdiActiveDocument;
			Database db = doc.Database;
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
				BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
				Point3d start = new Point3d(-1, 0, 0);
				Point3d middle = new Point3d(0, 1, 1);
				Point3d end = new Point3d(1, 0, 2);
				double startDistance = 25;
				InterpolatedArc a = new InterpolatedArc(null, start, end, middle, startDistance);
				PositionVector? pp = a.GetPointAtDistance(26);

				Polyline3d pl = new Polyline3d();
				btr.AppendEntityCurrentLayer(pl);
				tr.AddNewlyCreatedDBObject(pl, true);
				Polyline3d pl1 = new Polyline3d();
				btr.AppendEntityCurrentLayer(pl1);
				tr.AddNewlyCreatedDBObject(pl1, true);

				a.Draw(pl);

				a.Draw(26, 27, pl1);

				tr.Commit();
			}

		}
	}
}
