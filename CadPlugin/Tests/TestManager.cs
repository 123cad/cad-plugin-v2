﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.Tests
{
	public class TestManager
	{
		private static bool firstRun = true;
		public static bool RunTestFlag = 
#if TEST
			true;
#else
			false;
#endif

		[Conditional("DEBUG")]
		public static void RunTests()
		{
			if (!RunTestFlag)
				return;
			if (firstRun)
			{
				firstRun = false;
				return;
			}
			//Common.CADSelectionFilter_Test.Run();
			//Tests.PositionMaker.PositionMaker_Tests.Run();
			//Tests.Common.CADSolid3dHelper_Test.Start();
			Tests.Inventories.InventoryDrawerSanierung_Test.Test();
			//Tests.Inventories.InventoryDrawerGeometry_Test.Test();
			//Tests.Inventories.SelectDrawingOptions_Test.Start();
            //Tests.Inventories.GeometryAssign_MainForm_Test.Start();
            //InventoriesTestManager.RunTests();
            //Commands.PipeEntitiesJigs.JiggerCalculation_Test.Test1();
            //XmlSerializeTest.MyTestMethod();

            //CadPlugin.Tests.Common.CADMTextHelper_Test.Test();
            //System.Diagnostics.Debug.WriteLine("TEST run");
            //Environment.FailFast("Test run");

            //CadToInvenaryToolbar.Start();
		}
	}
}
