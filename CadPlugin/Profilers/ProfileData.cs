﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mg = MyUtilities.Geometry;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Profilers
{
    public class ProfileData
    {
        public int Distance { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Unique index of profile (in all axes), like it is stored in drawing. 
        /// </summary>
        public int Index { get; set; }
        public double Height;
        public LevelLine GroundLevel;
        public LevelLine CrownOfTheRoad;
        public List<Point3d> Links;
        public List<Point3d> Rechts;
        /// <summary>
        /// Prvi element uvek levelline number!!!
        /// </summary>
        public List<LevelLine> OtherPoints;
        public Point3d Center;
        //private bool _isRelativized;
        //public Line3d CenterLine;//da li nam treba
        public double DX;//relativna leva koordinata, na krug se dodaje ova vrednost da bi se dobilo centralno x
        public ProfileData(double distance, string name, int index, double height, List<Point3d> gl, string glName, List<Point3d> cotr, string cotrName, Point3d center, double dx)
        {
            //_isRelativized = false;
            Distance = CustomSettings.DistanceToInt(distance);
            Index = index;
            Height = height;
            Center = center;
            Name = name;
            DX = dx;
            CrownOfTheRoad = new LevelLine(-1, cotrName, cotr);
            GroundLevel = new LevelLine(-1, glName, gl);
        }
        /// <summary>
        /// Absolute coordinates transfers to relative coordinates - coordinates from cad drawing to coordinates of profile
        /// </summary>
        public void Relativize()
        {
            double x = Center.X + Math.Abs(DX);
            double y = Center.Y;
            double h = Height;
            List<Point3d> tempGL;
            List<Point3d> tempCOTR;
            if (GroundLevel != null && GroundLevel.Points != null)
            {
                tempGL = new List<Point3d>();
                foreach (Point3d p in GroundLevel.Points)
                {
                    Point3d p1 = new Point3d(Math.Round(p.X - x, 3), Math.Round(p.Y - y + h, 3), Math.Round(p.Z, 3));
                    tempGL.Add(p1);
                }
                GroundLevel.Points = tempGL;
            }
            if (CrownOfTheRoad != null && CrownOfTheRoad.Points != null)
            {
                tempCOTR = new List<Point3d>();
                int i = 0;
                foreach (Point3d p in CrownOfTheRoad.Points)
                {
                    Point3d p1 = new Point3d(Math.Round(i == 1 ? 0 : p.X - x, 3), Math.Round(p.Y - y + h, 3), Math.Round(p.Z, 3));
                    tempCOTR.Add(p1);
                    i++;
                }
                CrownOfTheRoad.Points = tempCOTR;
            }
            //TODO insert humus line
            //DISABLED Distance = Math.Round(Distance, 3);
            //_isRelativized = true;
        }

        public void AddLinksAndRechts(List<mg.Point2d> links, List<mg.Point2d> rechts)
        {
            if (links != null && links.Count > 0)
            {
                Links = new List<Point3d>(links.Count);
                foreach (mg.Point2d p in links)
                    Links.Add(new Point3d(p.X, p.Y, 0));
            }
            if (rechts != null && rechts.Count > 0)
            {
                Rechts = new List<Point3d>(rechts.Count);
                foreach (mg.Point2d p in rechts)
                    Rechts.Add(new Point3d(p.X, p.Y, 0));
            }
        }
        public void GlobalizeLinksAndRechts()
        {
            double x = Center.X + Math.Abs(DX);
            double y = Center.Y;
            double h = Height;
            if (Links != null && Links.Count > 0)
                for (int i = 0; i < Links.Count; i++)// (Point3d p in Links)
                    Links[i] = Links[i].Add(new Vector3d(x, -Height + y, 0));
            if (Rechts != null && Rechts.Count > 0)
                for (int i = 0; i < Rechts.Count; i++)// (Point3d p in Rechts)
                    Rechts[i] = Rechts[i].Add(new Vector3d(x, -Height + y, 0));
        }
        /// <summary>
        /// Forwarded otherPoints are not copied, original is taken. 
        /// </summary>
        public void AddOtherPoints(List<LevelLine> otherPoints)
        {
            if (otherPoints == null || otherPoints.Count == 0)
                return;
            OtherPoints = otherPoints;
            /*List<Point3d> points = new List<Point3d>();
            foreach (List<MyPoint2d> list in otherPoints)
            {
                foreach (MyPoint2d p in list)
                    points.Add(new Point3d(p.X, p.Y, 0));
                OtherPoints.Add(points);
                points = new List<Point3d>();
            }*/
        }
        public void GlobalizeOtherPoints()
        {
            double x = Center.X + Math.Abs(DX);
            double y = Center.Y;
            double h = Height;
            if (OtherPoints == null)
                return;
            foreach (LevelLine pointsList in OtherPoints)
            {
                for (int i = 0; i < pointsList.Points.Count; i++)
                    pointsList.Points[i] = pointsList.Points[i].Add(new Vector3d(x, -Height + y, 0));
            }
        }

    }
}
