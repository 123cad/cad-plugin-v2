﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Profilers
{
    public static class CustomSettings
    {
        //NOTE profile = station
        public const int GroundLevelIndex = 10;
        public const int CrownOfTheRoadIndex = 20;
        public const int LinksIndex = 30;
        public const int RechtIndex = 40;
        /// <summary>
        /// //NOTE name for now is this + levelline level
        /// </summary>
        public const string ProfilerPolylinesName = "123_Qschnitt_KZ_";
        public static string CreateLevellineName(int index)
        {
            return ProfilerPolylinesName + index;
        }

        public static int DistanceToInt(double distance)
        {
            //NOTE kept 4 decimal places
            return (int)Math.Round(distance * 10 * 1000);
        }
        public static double DistanceFromInt(int distance)
        {
            return ((double)distance) / (10 * 1000);
        }
    }
}
