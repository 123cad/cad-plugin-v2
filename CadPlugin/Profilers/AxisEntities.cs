﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyExceptions;
using System.Windows;
using Isybau2015.Ownerships.LoadFiles;
using C123InteropWrapper;
using Interop.C123YardXRXLib;
using System.Diagnostics;
using Profiler.Construction;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Profilers
{
    /// <summary>
    /// Keeps collection of axes, which contains levellines (each levelline has 
    /// its polyline).
    /// </summary>
    class AxisEntities
    {
        class AxisWrapper
        {
            public int Id => Axis.Index;
            public SingleAxis Axis { get; set; }
            public Dictionary<int, ProfileWrapper> Profiles { get; } = new Dictionary<int, ProfileWrapper>();

            public AxisWrapper(SingleAxis axis)
            {
                Axis = axis;
            }
        }
        class ProfileWrapper
        {
            public int Id => Data.Distance;
            public ProfileData Data { get; set; }
            public Dictionary<int, LevellineWrapper> Levellines { get; } = new Dictionary<int, LevellineWrapper>();

            public ProfileWrapper(ProfileData data)
            {
                Data = data;
            }
        }
        class LevellineWrapper
        {
            public int Id => Level.Level;
            public LevelLine Level { get; set; }
            public ObjectId PolylineId { get; set; }

            public LevellineWrapper(LevelLine level, ObjectId polylineId)
            {
                Level = level;
                PolylineId = polylineId;
            }
        }
        private Dictionary<int, AxisWrapper> axes = new Dictionary<int, AxisWrapper>();
        public void Add(SingleAxis axis, ProfileData profile, LevelLine levelLine, ObjectId polylineId)
        {
            if (!axes.ContainsKey(axis.Index))
            {
                axes.Add(axis.Index, new AxisWrapper(axis));
            }
            var axisW = axes[axis.Index];
            if (!axisW.Profiles.ContainsKey(profile.Index))
            {
                axisW.Profiles.Add(profile.Index, new ProfileWrapper(profile));
            }
            var profileW = axisW.Profiles[profile.Index];

            var levellines = profileW.Levellines;
            if (!levellines.ContainsKey(levelLine.Level))
            {
                levellines.Add(levelLine.Level, new LevellineWrapper(levelLine, polylineId));
            }
            else
                throw new DuplicateObjectException($"Axis <{axis.Name}/{axis.Index}> already has levelline <{levelLine.Level}> set!");
        }
        
        /// <summary>
        /// Assigns data fetched from profiler to axes which already exist in dwg
        /// (automatically create terrains and assign levellines, from created polyline).
        /// </summary>
        public void ApplyDataToExistingAxes()
        {
            MyLog.MyTraceSource.Information("Started applying levellines from profiler to drawing");
            var ls = new COM123CADWrapper();
            foreach(var a in axes)
            {
                var result = MessageBox.Show($"Hinzu Achse <{a.Value.Axis.Name}> ?", "", MessageBoxButton.YesNoCancel);
                switch (result)
                {
                    case MessageBoxResult.Cancel:
                        return;
                    case MessageBoxResult.No:
                        continue;
                    case MessageBoxResult.Yes:
                        break;
                }
                int[] qsects = ls.Dwg.GetAxisQSects((short)a.Value.Axis.Index);
                // Distance * 1000 as int, QS index.
                Dictionary<int, int> stations = new Dictionary<int, int>();
                for (short i = 0; i < qsects.Length; i++)
                {
                    short qSectIndex = (short)qsects[i];
                    ls.Sect.SetActiveQSect(qSectIndex);
                    double station = ls.Sect.QSStation;
                    // Place for potential problems, because of rounding.
                    // Rounding and multiplying by 10000 made number non rounded (.999997)
                    int stationInt = (int)(Math.Round(station * 10000, 0));
                    stations.Add(stationInt, qSectIndex);
                }

                ls.Sect.BeginUpdate();

                bool deleteWithoutAsking = true;
                bool deleteIsAsked = false;

                // For each station try to fetch it from COM.
                foreach(var st in a.Value.Profiles)
                {
                    if (!stations.ContainsKey(st.Value.Data.Distance))
                    {
                        Debug.WriteLine($"Axis {a.Value.Id} station {st.Value.Id} not found!");
                        continue;
                    }
                    var qsect = stations[st.Value.Data.Distance];
                    ls.Sect.SetActiveQSect((short)qsect);
                    string[] levellineNames = ls.Sect.GetTerrains();
                    // <levelline number, levelline name>
                    Dictionary<string, string> levellines = new Dictionary<string, string>();
                    foreach(var ln in levellineNames)
                    {
                        ls.SectTerrain.SetTerrain(ln);
                        string lNumber = ls.SectTerrain.BorderlineNumber;
                        levellines.Add(lNumber, ln);
                    }

                    // For each levelline try to create terrain from it
                    foreach (var l in st.Value.Levellines)
                    {
                        try
                        {
                            string handle = l.Value.PolylineId.Handle.ConvertToString();
                            string levellineDescription = l.Value.Level.Name;
                            string levelName = l.Value.Level.Level.ToString();

                            // If terrain by level exists already, ask user to delete it (only first time).
                            bool levellineExists = levellines.ContainsKey(levelName);

                            if (levellineExists)
                            {
                                bool deleteExistingLevelLines = true;
                                if (!deleteIsAsked)
                                {
                                    //var status = MessageBox.Show("Delete all existing levellines without asking?", "", MessageBoxButton.YesNo);
                                    var status = MessageBox.Show("Aktualisiere Alle Horizonte oder <Nein>für Einzelne Profile?", "", MessageBoxButton.YesNo);
                                    deleteWithoutAsking = status == MessageBoxResult.Yes;
                                    deleteIsAsked = true;
                                }
                                if (!deleteWithoutAsking)
                                {
                                    ls.Sect.ZoomExtents();
                                    //var status = MessageBox.Show($"Delete existing levelline <{levelName}> on profile <{st.Value.Data.Name}>?", "", MessageBoxButton.YesNo);
                                    var status = MessageBox.Show($"Aktualisiere Horizont <{levelName}> in Profil <{st.Value.Data.Name}>?", "", MessageBoxButton.YesNo);
                                    deleteExistingLevelLines = status == MessageBoxResult.Yes;
                                }


                                if (deleteExistingLevelLines)
                                {
                                    ls.Sect.RemoveTerrain(levellines[levelName]);
                                    MyLog.MyTraceSource.Information($"Deleted existing levelline <{levelName}> on profile <{st.Value.Data.Name}>");
                                }
                                else
                                {
                                    MyLog.MyTraceSource.Information($"Skipped deleting <{levelName}> on profile <{st.Value.Data.Name}>. New levelline not applied.");
                                    continue;
                                }
                            }


                            string tName = ls.Sect.AddNewTerrainFromPolyline(handle, levellineDescription, levelName);
                            //ls.SectTerrain.SetTerrain(tName);
                            //ls.Sect.SetTerrainDescription(tName, l.Value.Level.Name);
                            //ls.SectTerrain.BorderlineNumber = l.Value.Level.Level.ToString();
                        }
                        catch(System.Exception e)
                        {
                            Debug.WriteLine("Error when creating terrain. " + e.Message);
                        }
                    }
                }
                ls.Sect.EndUpdate();
            }
            MyLog.MyTraceSource.Information("Finished profiler!");
        }
    }
}
