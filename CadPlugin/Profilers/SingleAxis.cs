﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Profilers
{
    public class SingleAxis
    {
        public string Name { get; private set; }
        public int Index { get; private set; }
        public double StartStation { get; private set; }
        public double EndStation { get; private set; }
        //ne treba! problems with always having sorted, so i will use existing dictionaries and sort them when needed
        private Dictionary<int, ProfileData> _index_profiles;
        public int NumberOfProfiles { get { return _index_profiles.Count; } }
        public SingleAxis(string name, int index)
        {
            Name = name;
            StartStation = EndStation = -1;
            Index = index;
            _index_profiles = new Dictionary<int, ProfileData>();
        }
        public bool AddProfile(ProfileData data)
        {
            data.Distance = data.Distance; //CustomSettings.DistanceToInt(data.Distance);
            _index_profiles.Add(data.Distance, data);
            if (StartStation == -1)
                StartStation = EndStation = data.Distance;
            else if (StartStation > data.Distance)
                StartStation = data.Distance;
            else if (EndStation < data.Distance)
                EndStation = data.Distance;
            return true;
        }
        public ProfileData GetProfile(int distance)
        {
            if (_index_profiles.ContainsKey(distance))
                return _index_profiles[distance];
            return null;
        }
        public ProfileData GetProfile(double distance)
        {
            int d = CustomSettings.DistanceToInt(distance);
            return GetProfile(d);
        }
        /// <summary>
        /// Returns station, null if it doesn't exist
        /// </summary>
        public ProfileData this[int distance]
        {
            get
            {
                return GetProfile(distance);
            }
        }
        /// <summary>
        /// Converts distance to int representation with precision, and returns profile if it exists.
        /// </summary>
        public ProfileData this[double distance]
        {
            get
            {
                return this[CustomSettings.DistanceToInt(distance)];
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool GetProfile(double station, out ProfileData data)//pocetak stanica za axis
        {
            int temp = CustomSettings.DistanceToInt(station);
            data = null;
            if (_index_profiles.ContainsKey(temp))
                data = _index_profiles[temp];
            else return false;
            return true;
        }

        public void Relativize()
        {
            foreach (ProfileData data in _index_profiles.Values)
                data.Relativize();
        }
        public List<ProfileData> GetAllProfiles()
        {
            return _index_profiles.Values.ToList();
        }

        /*public List<TStation> GetStations()
        {
            List<TStation> stations = new List<TStation>();
            foreach (ProfileData data in _index_profiles.Values)
            {
                TStation t = data.GetInformation();
                //DISABLED t.Distance += axisDistance;
                stations.Add(t);
            }
            return stations;
        }*/
    }
}
