﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Profilers
{

    public class LevelLine
    {
        public int Level;
        public string Name;
        public List<Point3d> Points;
        public LevelLine(int level, List<Point3d> points) : this(level, "", points)
        {
        }
        public LevelLine(int level, string name, List<Point3d> points)
        {
            Level = level;
            Points = points;
            Name = name;
        }
    }
}
