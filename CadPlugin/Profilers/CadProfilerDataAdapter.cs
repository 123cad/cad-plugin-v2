﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Format066Holder;
using CadPlugin.Profilers;

#if BRICSCAD
using Teigha.Geometry;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.GraphicsInterface;
using Autodesk.AutoCAD.Geometry;
#endif



namespace CadPlugin.Profilers
{
    /// <summary>
    /// Class responsible to transfer format of saved stations from cad to profiler, and vice versa
    /// </summary>
    public class CadProfilerDataAdapter
    {
        public static AxesHolder CADInterfaceToProfilerInterface(AxesGroup group)
        {
            AxesHolder axes = new AxesHolder();
            foreach(int index in group.GetAxesIndexes())
            {
                SingleAxis axis = group[index];
                Axis066 axis66 = new Axis066(axis.Name, axis.Index);
                foreach(ProfileData profile in axis.GetAllProfiles())
                {
                    double distance = CustomSettings.DistanceFromInt(profile.Distance);
                    Station066 station = new Station066(distance, profile.Name);
                    station.UniqueId = profile.Index;
                    //NOTE level of levellines is set in profiler (it can be fetched from drawing, but no need for it now)
                    if (profile.GroundLevel != null && profile.GroundLevel.Points != null)
                    { 
                        //NOTE set level of level066 - for now not used in profiler, it assigns its own level numbers                        
                        int levelLevel = Profiler.Settings.AppSettings.GroundlevelDefaultLevel;
                        string levelName = profile.GroundLevel.Name;
                        Level066 l = new Level066(levelName, levelLevel);
                        foreach (Point3d p in profile.GroundLevel.Points)
                            l.AddPoint(new Point2D(p.X, p.Y));
                        station.AddLevel(l);
                    }
                            
                    if (profile.CrownOfTheRoad != null && profile.CrownOfTheRoad.Points != null)
                    {
                        //NOTE set level of level066 - for now not used in profiler, it assigns its own level numbers
                        int levelLevel = Profiler.Settings.AppSettings.CrownOfTheRoadDefaultLevel;
                        string levelName = profile.CrownOfTheRoad.Name;
                        Level066 l = new Level066(levelName, levelLevel);
                        foreach (Point3d p in profile.CrownOfTheRoad.Points)
                            l.AddPoint(new Point2D(p.X, p.Y));
                        station.AddLevel(l);
                    }
                    //add humus line - no need for humus line, it is created in profiler

                    axis66.AddStation(station);
                }
                axes.AddAxis(axis66);
            }
            return axes;
        }
        /// <summary>
        /// Profiler only adds levellines, so we add it to axesgroup
        /// </summary>
        public static void UpdateAxesFromProfiler(AxesHolder axes, AxesGroup group)
        {
            //it seems i have to use existing axesgroup because it already contains all required data, so i only update it
            //AxesGroup group = new AxesGroup();
            foreach(Axis066 axis in axes.GetAllAxes())
            {
                SingleAxis ax = group[axis.Id];
                foreach(Station066 station in axis.GetStations())
                {
                    ProfileData data = ax.GetProfile(station.Distance);
                    List<LevelLine> levellines = new List<LevelLine>();
                    foreach(Level066 level in station.GetLevels())
                    {
                        List<Point3d> points = new List<Point3d>(level.Points.Count);
                        foreach (var p in level.Points)
                            points.Add(new Point3d(p.X, p.Y, 0));
                        levellines.Add(new LevelLine(level.LevelNumber, level.Name, points));
                    }
                    data.AddOtherPoints(levellines);
                    data.GlobalizeOtherPoints();
                }
            }
        }
    }
}
