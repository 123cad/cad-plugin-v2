﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Globalization;
using System.Diagnostics;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin
{
	partial class MyPlugin
	{
		private static string GetLibrariesLocation()
		{
			string currentDir = Assembly.GetExecutingAssembly().Location;
			currentDir = currentDir.Substring(0, currentDir.LastIndexOf('\\'));
			string dllDir = currentDir;

			//if (!File.Exists(currentDir + @"\MyLog.dll"))
			if (Directory.Exists(currentDir + @"\Libraries"))
				dllDir += @"\Libraries";
			return dllDir;
		}
		private static void LoadModules()
		{
			//ALL DLLS ARE BEING LOADED FROM HERE, MANUALLY!
			//Console.Write("test");

			string assemblyName;
#if DEBUG
			// dll are loaded in project file (dynamically, debug/release depending what is selected.
			// In this way intelisense is working, among some other things.
			Console.WriteLine("LOADING MODULES MANUALLY!!!");
			//return;
#endif
			// If found, libraries are loaded from current directory. 
			// Otherwise
			string dllDir = GetLibrariesLocation();

			//assemblyName = dllDir + @"\Profiler.exe";
			//Assembly.LoadFrom(assemblyName);
			//assemblyName = dllDir + @"\BlockData.dll";
			//Assembly.LoadFrom(assemblyName);
			//assemblyName = dllDir + @"\123ASCII.dll";
			//Assembly.LoadFrom(assemblyName);
			////assemblyName = programPath + @"\Libraries\BlockExportLib.dll";
			////Assembly.LoadFrom(assemblyName);
			//assemblyName = dllDir + @"\Format066Holder.dll";
			//Assembly.LoadFrom(assemblyName);
			//assemblyName = dllDir + @"\BlockExportView.dll";
			//Assembly.LoadFrom(assemblyName);
			//assemblyName = dllDir + @"\Manager.dll";
			//Assembly.LoadFrom(assemblyName);
			//assemblyName = dllDir + @"\GlobalConfig.dll";
			//Assembly.LoadFrom(assemblyName);
			//assemblyName = dllDir + @"\MyUtilities.dll";
			//Assembly.LoadFrom(assemblyName);

#if AUTOCAD
			assemblyName = dllDir + @"\SharedUtilities acad.dll";
			Assembly.LoadFrom(assemblyName);
			assemblyName = dllDir + @"\SewageUtilities acad.dll";
			Assembly.LoadFrom(assemblyName);
			assemblyName = dllDir + @"\Islands acad.dll";
			Assembly.LoadFrom(assemblyName);
			assemblyName = dllDir + @"\Axis acad.dll";
			Assembly.LoadFrom(assemblyName);
			assemblyName = dllDir + @"\DTM acad.dll";
			Assembly.LoadFrom(assemblyName);
			assemblyName = dllDir + @"\BlockAttributes_acad.dll";
			if (File.Exists(assemblyName))
				Assembly.LoadFrom(assemblyName);
			assemblyName = dllDir + @"\Sewage acad.dll";
			if (File.Exists(assemblyName))
				Assembly.LoadFrom(assemblyName);
#endif
#if BRICSCAD
			assemblyName = dllDir + @"\SharedUtilities bcad.dll";
			//Assembly.LoadFrom(assemblyName);
			assemblyName = dllDir + @"\SewageUtilities bcad.dll";
			//Assembly.LoadFrom(assemblyName);
			assemblyName = dllDir + @"\NetworkKS bcad.dll";
			Assembly.LoadFrom(assemblyName);
			assemblyName = dllDir + @"\Islands bcad.dll";
			Assembly.LoadFrom(assemblyName);
			assemblyName = dllDir + @"\Axis bcad.dll";
			Assembly.LoadFrom(assemblyName);
			assemblyName = dllDir + @"\Autotrack bcad.dll";
			Assembly.LoadFrom(assemblyName);
			assemblyName = dllDir + @"\DTM bcad.dll";
			if (File.Exists(assemblyName))
			{
				Assembly.LoadFrom(assemblyName);
				MyCommands.loadedDTM = true;
			}
			assemblyName = dllDir + @"\BlockAttributes_bcad.dll";
			if (File.Exists(assemblyName))
				Assembly.LoadFrom(assemblyName);
			assemblyName = dllDir + @"\Sewage bcad.dll";
			if (File.Exists(assemblyName))
				Assembly.LoadFrom(assemblyName);
#endif
			//assemblyName = dllDir + @"\MyLog.dll";
			//Assembly.LoadFrom(assemblyName);
			//
			//assemblyName = dllDir + @"\GraphicLibrary.dll";
			//Assembly.LoadFrom(assemblyName);
			//
			//assemblyName = dllDir + @"\Ninject.dll";
			//Assembly.LoadFrom(assemblyName);
			//
			//assemblyName = dllDir + @"\Isybau2015.dll";
			//Assembly.LoadFrom(assemblyName);
			//
			//assemblyName = dllDir + @"\C123InteropWrapper.dll";
			//if (File.Exists(assemblyName))
			//	Assembly.LoadFrom(assemblyName);

			assemblyName = dllDir + @"\Inventary.dll";
			if (File.Exists(assemblyName))
			{
				Assembly.LoadFrom(assemblyName);
				Action initInventory = () => { MyCommands.Command_123Inventory(); };
				LoadCompiledClass(inventoryClass, "CadPlugin_Compiled.Sewage", "init", initInventory, ref inventoryAssembly);
			}

			assemblyName = dllDir + @"\PresentationFramework.Aero.dll";
			if (File.Exists(assemblyName))
				Assembly.LoadFrom(assemblyName);

			assemblyName = dllDir + @"\PresentationFramework.Aero2.dll";
			if (File.Exists(assemblyName))
				Assembly.LoadFrom(assemblyName);

#if AUTOCAD
			assemblyName = dllDir + @"\DC_old\DYC acad.dll";
#endif
#if BRICSCAD
			assemblyName = dllDir + @"\DC_old\DYC bcad.dll";
#endif
			if (File.Exists(assemblyName))
			{
				Assembly.LoadFrom(assemblyName);
				Action initDynamicCurves = () => { MyCommands.Command_123DynamicalCurves(); };
				LoadCompiledClass(dynamicCurvesClass, "CadPlugin_Compiled.DynamicCurves", "init", initDynamicCurves, ref dynamicCurvesAssembly);

			}

		}
		private static string inventoryClass = string.Concat(
#if BRICSCAD
				"using Teigha.Runtime;",
#endif
#if AUTOCAD
				"using Autodesk.AutoCAD.Runtime;", 
#endif
				"using System;",
				"[assembly: CommandClass(typeof(CadPlugin_Compiled.Sewage))]",
				"namespace CadPlugin_Compiled",
				"{",
					"public class Sewage",
					"{",
						"private static Action call;",
						"public static void init(Action a)",
						"{",
							"call = a;",
						"}",
						"[CommandMethod(\"" + MyCommands._123Inventory + "\")]",
						"public static void Command_123Inventory()",
						"{",
							"call.Invoke();",// Not possible to call directly because of circular dependency (?)
						"}",
						"public static void Dispose()",
						"{",
							"call = null;",
						"}",
					"}",
				"}");
		private static string dynamicCurvesClass = string.Concat(
#if BRICSCAD
				"using Teigha.Runtime;",
#endif
#if AUTOCAD
				"using Autodesk.AutoCAD.Runtime;", 
#endif
				"using System;",
				"[assembly: CommandClass(typeof(CadPlugin_Compiled.DynamicCurves))]",
				"namespace CadPlugin_Compiled",
				"{",
					"public class DynamicCurves",
					"{",
						"private static Action call;",
						"public static void init(Action a)",
						"{",
							"call = a;",
						"}",
						"[CommandMethod(\"" + MyCommands._123Dc + "\")]",
						"public static void Command_123DynamicalCurves()",
						"{",
							"call.Invoke();",// Not possible to call directly because of circular dependency (?)
						"}",
						"public static void Dispose()",
						"{",
							"call = null;",
						"}",
					"}",
				"}");

		private static void LoadCompiledClass(string classCode, string className, string methodName, Action commandCallback, ref Assembly assembly)
		{
			CSharpCodeProvider provider = new CSharpCodeProvider();
			CompilerParameters parameters = new CompilerParameters();
			parameters.GenerateInMemory = true;
			parameters.GenerateExecutable = false;
			// Bug! If this is set, then it is searched for on computer (this didn't work on 
			// claus computer). With leaving it null (default) program works.
			//parameters.OutputAssembly = "DynamicCommands";
			//parameters.OutputAssembly = "SewageCommands.prc";
			Assembly[] ass = AppDomain.CurrentDomain.GetAssemblies();
			string[] assemblies;
			int loadedCount = 0;// How many needed assemblies has been loaded. 
#if BRICSCAD
			assemblies = new string[] { "td_mgd.dll", "brxmgd.dll" };
#endif
#if AUTOCAD
			assemblies = new string[]{"AcCoreMgd.dll", "AcDbMgd.dll", "AcMgd.dll"};
#endif
			foreach (Assembly a in ass)
			{
				for (int i = 0; i < assemblies.Length; i++)
				{
					if (assemblies[i] == null)
						continue;
					if (a.Location.EndsWith(assemblies[i], true, CultureInfo.InvariantCulture))
					{
						parameters.ReferencedAssemblies.Add(a.Location);
						loadedCount++;
						assemblies[i] = null;
						break;
					}
				}
				if (assemblies.Length == loadedCount)
					break;
			}
			parameters.ReferencedAssemblies.Add("mscorlib.dll");
			parameters.ReferencedAssemblies.Add("system.dll");
			//parameters.ReferencedAssemblies.Add(Assembly.GetExecutingAssembly().Location);
			parameters.IncludeDebugInformation = false;
			CompilerResults results = provider.CompileAssemblyFromSource(parameters, classCode);
			if (results.Errors.Count > 0)
				Debug.WriteLine(results.Errors);
			assembly = results.CompiledAssembly;

			MethodInfo mInfo = assembly.GetType(className).GetMethod(methodName, BindingFlags.Public | BindingFlags.Static);
			try
			{
				mInfo.Invoke(null, new object[] { commandCallback });
			}
			catch (System.Exception e)
			{
				Debug.WriteLine(e);
			}

		}
	}
}
