﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CadPlugin.dtm_protection
{
    public partial class ErrorDialog : Form
    {
        public ErrorDialog(int errorCode)
        {
            InitializeComponent();
            label1.Text = "Error has occured, please contact support! " + errorCode; 
            errorProvider1.SetError(label1, "");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
