﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.dtm_protection
{
    public class ActivationCheck
    {
        public event Action ResultsReadyEvent;
        private void onResultsReadyEvent()
        {
            if (ResultsReadyEvent != null)
                ResultsReadyEvent.Invoke();
        }

        public bool IsActivated { get; private set; }
        public string ErrorMessage { get; private set; }
        public int ErrorCode { get; private set; }
        

        public ActivationCheck()
        {
            IsActivated = false;
            code = new Random(DateTime.Now.Millisecond).Next(0, 10);
        }
        private int code = 0;

        public void StartCheck()
        {
            Task.Factory.StartNew(() => 
                {
                    // Create 
                    string path = Assembly.GetExecutingAssembly().Location;
                    path = path.Substring(0, path.LastIndexOf('\\'));
                    string[] files = Directory.GetFiles(path);
                    string id = "";
                    foreach(string s in files)
                    {
                        string t = s.Substring(s.LastIndexOf('\\')+1);
                        if (t[0] == '4' && t[1] == '7')
                        {
                            id = t;
                            break;
                        }
                    }
                    if (id == "")
                    {
                        IsActivated = false;
                        ErrorMessage = "Id not found!";
                        ErrorCode = 1;
                    }
                    else
                    {
                        try
                        {
                            using (var wb = new WebClient())
                            {
                                wb.Proxy = null;

                                // Connect to the internet
                                var data = new NameValueCollection();
                                data["id"] = code + id;
                                // Send request
                                string url = "http://aplikacija.tk/123temp_activation.php";
                                var response = wb.UploadValues(url, "POST", data);

                                //wait for response
                                var responseString = Encoding.Default.GetString(response);

                                // Set activation result
                                IsActivated = decodeResponse(responseString, id);

                                ErrorCode = IsActivated ? 0 : 3;
                            }
                        }
                        catch(Exception)
                        {
                            IsActivated = false;
                            ErrorMessage = "Error contacting web server.";
                            ErrorCode = 2;
                        }
                    }

                    // Raise ResultsReady event
                    onResultsReadyEvent();
                }
            );
        }
        private bool decodeResponse(string str, string id)
        {
            str = str.ToLower();
            var md5 = MD5.Create();
            var input = System.Text.Encoding.ASCII.GetBytes(code + "1" + id);
            byte[] hash = md5.ComputeHash(input);
            
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            string h = sb.ToString();

            return string.Compare(sb.ToString().ToLower(), str) == 0;
        }
    }
}
