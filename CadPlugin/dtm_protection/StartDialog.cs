﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CadPlugin.dtm_protection
{
    public partial class StartDialog : Form
    {
        private static bool returnValue;
        public static bool Start()
        {
            returnValue = false;
            new StartDialog().ShowDialog();
            return returnValue;
        }
        public StartDialog()
        {
            InitializeComponent();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            returnValue = true;
            Close();
        }
    }
}
