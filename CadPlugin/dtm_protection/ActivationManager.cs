﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CadPlugin.dtm_protection
{
    public class ActivationManager
    {
        private static bool Activated = false;
        public static bool CheckActivation()
        {
            if (Activated)
                return true;

            // Start first dialog
            if (!StartDialog.Start())
                return false;

            // Start waiting dialog
            ActivationCheck a = new ActivationCheck();
            WaitingDialog waiting = new WaitingDialog();
            waiting.Shown += (e,e1) =>
            {
                a.ResultsReadyEvent += () =>
                {
                    waiting.Invoke(new Action(delegate ()
                    {
                        waiting.Close();
                    })
                    );
                };
                a.StartCheck();
            };
            waiting.ShowDialog();

            // Start error dialog if error occured
            if (!a.IsActivated)
                new ErrorDialog(a.ErrorCode).ShowDialog();

            //Console.Write(a.IsActivated ? "Activated" : a.ErrorMessage);

            Activated = a.IsActivated;

            return Activated;

        }
    }
}
