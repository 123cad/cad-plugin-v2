﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CadPlugin.KHNeu.Views.UserControls
{
    /// <summary>
    /// Interaction logic for ShaftControl.xaml
    /// </summary>
    public partial class ShaftControl : UserControl
    {
        public ShaftControl()
        {
            InitializeComponent();
        }


        private void globalLostFocus(object sender, RoutedEventArgs e)
        {
            // When TextBox loses focus, if value type is invalid it is replaced
            // with last valid value.
            // * Note that value can still be logically invalid (type is good).
            var t = e.OriginalSource as TextBox;
            t?.GetBindingExpression(TextBox.TextProperty)?.UpdateSource();
            t?.GetBindingExpression(TextBox.TextProperty)?.UpdateTarget();
        }
    }
}
