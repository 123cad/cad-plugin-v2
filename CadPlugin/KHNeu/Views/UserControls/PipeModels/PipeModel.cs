﻿using CadPlugin.KHNeu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF;

namespace CadPlugin.KHNeu.Views.UserControls.PipeModels
{
    public enum PipeSlopeState
    {
        /// <summary>
        /// Downstream mode, pipe going downstream.
        /// </summary>
        DownstreamDown,
        DownstreamUp,
        UpstreamDown,
        UpstreamUp

    }
    public class PipeModel : ValidationBase
    {
        public WaterDirection WaterDirection{ get; }
        /// <summary>
        /// Downstream mode - pipe goes up.
        /// Upstream mode - pipe goes down.
        /// </summary>
        public bool DirectionInversed { get; private set; }
        public PipeSlopeState SlopeState { get; private set; }
        /// <summary>
        /// Current pipe slope in %.
        /// </summary>
        /// <remarks>
        /// Slope is always calculated for pipe going from Shaft1 to Shaft2.
        /// This means if slope is negative, pipe is going upstream (depends
        /// on mode if this is what is needed/valid).
        /// </remarks>
        public double CurrentSlope
        {
            get => _currentSlope; set => OnPropertyChanged(() =>
            {
                _currentSlope = value;
                checkSlopeValid();
                OnPropertyChanged(nameof(CurrentSlope));
            });
        }
        public bool UseMinSlope
        {
            get => _useMinSlope; set => OnPropertyChanged(() =>
            {
                _useMinSlope = value;
                checkSlopeValid();
            });
        }
        /// <summary>
        /// Minimum slope in %, always must be positive.
        /// </summary>
        public double MinSlope
        {
            get => _minSlope; set
            {
                if (value < 0)
                    value = 0;
                OnPropertyChanged(() => _minSlope = value);
                checkSlopeValid();
            }
        }
        /// <summary>
        /// In Downstream mode we can always go down to make slope bigger.
        /// In Upstream mode making slope bigger can go above terrain - this is invalid case.
        /// </summary>
        public bool IsSlopeValid{ get; private set; }
        public double Diameter
        {
            get => _diameter;
            set => OnPropertyChanged(() => _diameter = value > 0.001 ? value : 0.001);
        }
        public double SegmentLength
        {
            get => _segmentLength; set
            {
                string msg = "Segment must be greater than 0.";
                msg = "Segmente müssen länger als 0 sein!";
                ValidateChange(m => m.AddIf(UseSegments && value < 0.001, msg), () => _segmentLength = value);
            }
        }
        public double StartEndSegmentLength
        {
            get => _startEndSegmentLength; set => OnPropertyChanged(() => _startEndSegmentLength = value);
        }
        public bool UseSegments
        {
            get => _useSegments;set
            {
                OnPropertyChanged(() => _useSegments = value);
                // Invoke validation (when use segments validate value, when not using clear error).
                SegmentLength = SegmentLength;
            }
        }
        /// <summary>
        /// Step of the angle of output pipe, from input pipe's direction.
        /// </summary>
        public double AngleStep
        {
            get => _angleStep; set => OnPropertyChanged(() => _angleStep = value);
        }
        public bool UseAngleStep
        {
            get => _useAngleStep; set => OnPropertyChanged(() => _useAngleStep = value);
        }
        /// <summary>
        /// Delta between start and end of pipe height (in cm).
        /// </summary>
        public double HeightDelta
        {
            get => _heightDelta; set => OnPropertyChanged(() => _heightDelta = value);
        }
        public double StartHeight
        {
            get => _startHeight; set => OnPropertyChanged(() => _startHeight = value);
        }
        public double EndHeight
        {
            get => _endHeight; set => OnPropertyChanged(() => _endHeight = value);
        }
        public double PipeLength2d
        {
            get => _pipeLength2d;set => OnPropertyChanged(() => _pipeLength2d = value);
        }
        public double SMPLength
        {
            get => _smpLength;set => OnPropertyChanged(() => _smpLength = value);
        }

        /// <summary>
        /// Angle of current pipe, related to previous pipe.
        /// </summary>
        public double CurrentAngleDeg
        {
            get => _currentAngleDeg; set => OnPropertyChanged(() => _currentAngleDeg = value);
        }



        private double _currentSlope = 0;
        private double _minSlope = 0;
        private bool _useMinSlope = false;
        private double _diameter = 0.3;
        private double _segmentLength = 1;
        private double _startEndSegmentLength = 0;
        private bool _useSegments = false;
        private double _angleStep = 15;
        private bool _useAngleStep = false;
        private double _heightDelta = 0;
        private double _startHeight= 0;
        private double _endHeight = 0;
        private double _currentAngleDeg = 0;
        private double _pipeLength2d = 0;
        private double _smpLength = 0;

        public PipeModel(WaterDirection waterDirection)
        {
            WaterDirection = waterDirection;
        }

        private void checkSlopeValid()
        {
            if (UseMinSlope)
            {
                var slope = CurrentSlope;
                // This is little bit tricky.
                // Pipe slope is negative if going upstream, from shaft1 to shaft2.
                // 
                if (WaterDirection == WaterDirection.Downstream)
                {
                    IsSlopeValid = slope >= MinSlope - 1e-5;
                }

                if (WaterDirection == WaterDirection.Upstream)
                {
                    // Slope must be greater than MinSlope
                    // (which is always positive).
                    IsSlopeValid = slope <= -MinSlope + 1e-5;
                }
            }
            else
            {
                IsSlopeValid = true;
            }

            refreshSlopeState();
            OnPropertyChanged(nameof(IsSlopeValid));
        }
        private void refreshSlopeState()
        {
            switch (WaterDirection)
            {
                case WaterDirection.Downstream:
                    if (CurrentSlope < 0)
                        SlopeState = PipeSlopeState.DownstreamUp;
                    else
                        SlopeState = PipeSlopeState.DownstreamDown;
                    break;
                case WaterDirection.Upstream:
                    if (CurrentSlope < 0)
                        SlopeState = PipeSlopeState.UpstreamUp;
                    else
                        SlopeState = PipeSlopeState.UpstreamDown;
                    break;
            }

            switch (SlopeState)
            {
                case PipeSlopeState.DownstreamUp:
                case PipeSlopeState.UpstreamDown:
                    DirectionInversed = true;
                    break;
                case PipeSlopeState.DownstreamDown:
                case PipeSlopeState.UpstreamUp:
                    DirectionInversed = false;
                    break;
            }

            OnPropertyChanged(nameof(SlopeState));
            OnPropertyChanged(nameof(DirectionInversed));
        }
    }
}
