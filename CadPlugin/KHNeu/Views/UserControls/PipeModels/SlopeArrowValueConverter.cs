﻿using CadPlugin.KHNeu.Views.KHNeuModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using CadPlugin.KHNeu.Models;
using System.Reflection;
using System.Windows.Media;

namespace CadPlugin.KHNeu.Views.UserControls.PipeModels
{
    
    public class SlopeArrowValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var model = (KHNeuModel) value;
            string fileName = "";
            switch (model.WaterDirection)
            {
                case WaterDirection.Downstream:
                    if (model.Pipe.CurrentSlope < 0)
                        fileName = "stream_down-up";
                    else
                        fileName = "stream_down-down";
                    break;
                case WaterDirection.Upstream:
                    if (model.Pipe.CurrentSlope < 0)
                        fileName = "stream_up-up";
                    else
                        fileName = "stream_up-down";
                    break;
            }

            fileName += ".png";
            string assembly = "";
#if AUTOCAD
            assembly = "Autocad";
#endif
#if BRICSCAD
            assembly = "Bricscad";
#endif
            string path = $"pack://application:,,,/{assembly};component/Content/KHNEU/{fileName}";
            return new Uri(path);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
