﻿using CadPlugin.KHNeu.Views.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF;

namespace CadPlugin.KHNeu.Views.UserControls.ShaftModels
{
    public class ShaftModel : ValidationBase
    {
        public string Header
        {
            get => _header;set => OnPropertyChanged(() => _header = value);
        }
        public double Diameter
        {
            get => _diameter; set => ValidateChange((m) => m.AddIf(value < 0.001, /*"Error"*/"Fehler"), () => _diameter = value); 
        }
        /// <summary>
        /// In percent.
        /// </summary>
        public double InnerSlope
        {
            get => _innerSlope; set => ValidateChange((m) => m.AddIf(value < 0, /*"Error"*/"Fehler"), () => _innerSlope = value);
        }
        public double HeightDMP
        {
            get => _heightDMP; set => ValidateChange(m=>m.AddIf(HeightSMP > value, /*"DMP above SMP"*/"DMP-Höhe über SMP-Höhe!"), () =>
            {
                _heightDMP = value;
                _heightSMP = _heightDMP - Delta;
                OnPropertyChanged(nameof(HeightSMP));
            });
        }
        public double Delta
        {
            get => _delta; set => OnPropertyChanged(() =>
            {
                _delta = value < 0 ? 0 : value;
                if (!UseMinDelta)
                {
                    HeightSMP = _heightDMP - _delta;
                    //OnPropertyChanged(nameof(HeightSMP));
                    //ValidateChange(null, null, nameof(HeightSMP));
                }
            });
        }
        public bool UseMinDelta
        {
            get => _useMinDelta;
            set
            {
                OnPropertyChanged(() => _useMinDelta = value);
                if (value)
                {
                    if (HeightDMP - HeightSMP < MinDelta)
                       HeightSMP = HeightDMP - MinDelta;
                }
            }
        }

        public double MinDelta
        {
            get => _minDelta; 
            set 
            {
                if (value < 0)
                    value = 0;
                ValidateChange(m => m.AddIf(value < 0, /*"Minimum delta"*/"Min.-Delta"), () => _minDelta = value);
                if (UseMinDelta && MinDelta > Delta)
                    HeightSMP = HeightDMP - value;
            }
        }
        public double HeightSMP
        {
            get => _heightSMP; 
            set => ValidateChange(m=>m.AddIf(value > HeightDMP, /*"SMP above DMP"*/"SMP-Höhe über DMP-Höhe"), () =>
            {
                _heightSMP = value;
                _delta = _heightDMP - _heightSMP;
                OnPropertyChanged(nameof(Delta));
                ValidateChange(m=>m.AddIf(HeightSMP > HeightDMP, "SMP-Höhe über DMP-Höhe"/*"SMP above DMP"*/), () => { }, nameof(HeightDMP));
            }
            );
        }
        public bool IsDrawn
        {
            get => _isDrawn;set => OnPropertyChanged(() => _isDrawn = value);
        }
        public bool IsFirst
        {
            get => _isFirst; set => OnPropertyChanged(() =>
            {
                _isFirst = value;
            });
        }

        public bool CanUserChangeSMP
        {
            get => _canUserChangeSmp;
            set => OnPropertyChanged(()=> _canUserChangeSmp = value);
        }

        /// <summary>
        /// Shaft1 (on the left side), has diameter which can be changed
        /// only if shaft is first (no pipes before).
        /// </summary>
        public bool IsShaft1{ get; }

        private string _header = "Shaft";
        private double _diameter = 1;
        private double _innerSlope = 2.5;
        private double _heightDMP;
        private double _delta;
        private bool _useMinDelta;
        private double _minDelta = 1;
        private double _heightSMP;
        private bool _isDrawn = false;
        private bool _isFirst = true;
        private bool _canUserChangeSmp = true;

        public ShaftModel(bool isShaft1)
        {
            IsShaft1 = isShaft1;
        }

        public void AssignFrom(ShaftModel m)
        {
            Diameter = m.Diameter;
            InnerSlope = m.InnerSlope;
            HeightDMP = m.HeightDMP;
            Delta = m.Delta;
            HeightSMP = m.HeightSMP;
            UseMinDelta = m.UseMinDelta;
            MinDelta = m.MinDelta;
            IsDrawn = m.IsDrawn;
            IsFirst = m.IsFirst;
            CanUserChangeSMP = false;
        }

        /// <summary>
        /// Returns delta between pipe in and pipe out.
        /// Depends on diameter and inner slope.
        /// </summary>
        /// <returns></returns>
        public double GetInnerSlopeDelta()
        {
	        double d = Diameter;
	        double s = InnerSlope / 100;
            double h = d * s;
            return h;
        }
    }
}
