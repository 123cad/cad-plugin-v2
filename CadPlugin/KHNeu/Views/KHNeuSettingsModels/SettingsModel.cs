﻿using CadPlugin.KHNeu.Models;
using CadPlugin.KHNeu.Models.TextDisplay;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Views.KHNeuSettingsModels
{

	public class SettingsModel : Notify
	{
		public static List<string> PipeNames { get; } = new List<string>();
		public static List<string> ShaftNames { get; } = new List<string>();
		public static List<string> Scale { get; } = new List<string>() { "%", "‰" };
		public static List<string> Digits { get; } = new List<string>() { "-", "0.0", "0.00", "0.000", "0.0000", "0.00000" };


		public Color ArrowColor { get => settings.ArrowColor;
			set => OnPropertyChanged(() => settings.ArrowColor= value); }
		public bool DrawArrowOnPipe { get => settings.DrawArrowOnPipe; 
			set => OnPropertyChanged(() => settings.DrawArrowOnPipe = value); }
		public bool DrawArrowInText { get => settings.DrawArrowInText; 
			set => OnPropertyChanged(() => settings.DrawArrowInText = value); }
		public bool ShowAngle { get => settings.ShowAngle; 
			set => OnPropertyChanged(() => settings.ShowAngle = value); }
		public ObservableCollection<SingleDisplayedText> PipeTexts { get => settings.PipeTexts; }
		public ObservableCollection<SingleDisplayedText> ShaftTexts { get => settings.ShaftTexts; }
		public SingleDisplayedText SelectedPipe { get => _selectedPipe; set => OnPropertyChanged(() => _selectedPipe = value); }
		public SingleDisplayedText SelectedShaft { get => _selectedShaft; set => OnPropertyChanged(() => _selectedShaft = value); }
		
		private SingleDisplayedText _selectedPipe;
		private SingleDisplayedText _selectedShaft;
		private Settings settings;

		static SettingsModel()
        {
			var tf = TextOptionsFactory.Instance;
			foreach(var p in tf.GetAllPipeDescriptions())
            {
				var pipe = tf.CreatePipeOptions(p);
				PipeNames.Add(pipe.Description);// pipe.Description);
            }
			foreach(var s in tf.GetAllShaftDescriptions())
            {
				var shaft = tf.CreateShaftOptions(s);
				ShaftNames.Add(shaft.Description);// shaft.Description);
            }
        }

		public SettingsModel(Settings settings)
        {
			this.settings = settings;
        }



		internal void AddPipe()
		{
			string name = TextOptionsFactory.Instance.GetAllPipeDescriptions().First();
			var opt = TextOptionsFactory.Instance.CreatePipeOptions(name);
			var pipe = new SingleDisplayedText(TextDisplayType.Pipe, opt);
			var last = PipeTexts.LastOrDefault();
			//if (last != null)
				//opt.AssignFrom(last.Options);
			PipeTexts.Add(pipe);
		}

		internal void AddShaft()
		{
			string name = TextOptionsFactory.Instance.GetAllShaftDescriptions().First();
			var opt = TextOptionsFactory.Instance.CreateShaftOptions(name);
			var shaft = new SingleDisplayedText(TextDisplayType.Shaft, opt);
			var last = ShaftTexts.LastOrDefault();
			//if (last != null)
				//opt.AssignFrom(last.Options);
			ShaftTexts.Add(shaft);
		}

		internal void RemovePipe()
		{
			if (SelectedPipe == null)
				return;
			int index = PipeTexts.IndexOf(SelectedPipe);
			PipeTexts.Remove(SelectedPipe);
			if (PipeTexts.Count == index)
				index--;
			if (index > 0)
				SelectedPipe = PipeTexts[index];
			else
				SelectedPipe = null;
		}
		internal void RemoveShaft()
		{
			if (SelectedShaft == null)
				return;
			int index = ShaftTexts.IndexOf(SelectedShaft);
			ShaftTexts.Remove(SelectedShaft);
			if (ShaftTexts.Count == index)
				index--;
			if (index > 0)
				SelectedShaft = ShaftTexts[index];
			else
				SelectedShaft = null;
		}

		private static void moveDown(IList<SingleDisplayedText> texts, SingleDisplayedText item)
		{
			if (item == null)
				return;
			int index = texts.IndexOf(item);
			if (index == -1 || index >= texts.Count - 1)
				return;
			texts.RemoveAt(index);
			texts.Insert(index + 1, item);
		}
		private static void moveUp(IList<SingleDisplayedText> texts, SingleDisplayedText item)
		{
			if (item == null)
				return;
			int index = texts.IndexOf(item);
			if (index < 1)
				return;
            texts.RemoveAt(index);
			texts.Insert(index - 1, item);
		}

        public void MoveShaftDown()
        {
            var selected = SelectedShaft;
            moveDown(ShaftTexts, SelectedShaft);
            SelectedShaft = selected;
        }

        public void MoveShaftUp()
        {
            var selected = SelectedShaft;
            moveUp(ShaftTexts, SelectedShaft);
            SelectedShaft = selected;
        }

        public void MovePipeDown()
        {
            var selected = SelectedPipe;
            moveDown(PipeTexts, SelectedPipe);
            SelectedPipe = selected;
        }

        public void MovePipeUp()
        {
            var selected = SelectedPipe;
            moveUp(PipeTexts, SelectedPipe);
            SelectedPipe = selected;
        }


	}
}
