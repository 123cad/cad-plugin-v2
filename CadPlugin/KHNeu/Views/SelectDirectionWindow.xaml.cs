﻿using CadPlugin.KHNeu.Models;
using CadPlugin.KHNeu.Views.Shared;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CadPlugin.KHNeu.Views
{
    /// <summary>
    /// Interaction logic for SelectDirectionWindow.xaml
    /// </summary>
    public partial class SelectDirectionWindow : Window
    {
        public static WaterDirection Direction => model.Direction;
        public static ShaftPointType ShaftPoint => model.ShaftPoint;

        private class Model : Notify
        {
            public WaterDirection Direction
            {
                get => _direction;
                set => OnPropertyChanged(()=> _direction = value);
            }

            public ShaftPointType ShaftPoint
            {
                get => _shaftPoint;
                set => OnPropertyChanged(() => _shaftPoint = value);
            }

            private WaterDirection _direction;
            private ShaftPointType _shaftPoint;
        }

        private static Model model;

        public SelectDirectionWindow()
        {
            InitializeComponent();

            if (model == null)
            {
                model = new Model
                {
                    Direction = WaterDirection.Downstream,
                    ShaftPoint = ShaftPointType.DMP
                };
            }

            if (Properties.KHNEU.Default != null)
            {
                model.Direction = Properties.KHNEU.Default.WaterDirection;
                model.ShaftPoint = Properties.KHNEU.Default.StartShaftPointType;
            }

            down.IsChecked = model.Direction == WaterDirection.Downstream;
            up.IsChecked = !down.IsChecked;

            dmp.IsChecked = model.ShaftPoint == ShaftPointType.DMP;
            smp.IsChecked = !dmp.IsChecked;

            Closed += (_, __) =>
              {
                  if (DialogResult == true)
                  {
                      Properties.KHNEU.Default.WaterDirection = Direction;
                      Properties.KHNEU.Default.StartShaftPointType = ShaftPoint;
                      Properties.KHNEU.Default.Save();
                  }
              };
        }

        private void ok_Click(object sender, RoutedEventArgs e)
        {
            if (down.IsChecked == true)
                model.Direction = WaterDirection.Downstream;
            if (up.IsChecked == true)
                model.Direction = WaterDirection.Upstream;

            if (dmp.IsChecked == true)
                model.ShaftPoint = ShaftPointType.DMP;
            if (smp.IsChecked == true)
                model.ShaftPoint = ShaftPointType.SMP;

            DialogResult = true;
            Close();
        }

        private void window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                DialogResult = false;
                Close();
            }    
        }
    }
}
