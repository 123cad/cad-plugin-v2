﻿using CadPlugin.KHNeu.Models;
using CadPlugin.KHNeu.Models.TextDisplay;
using CadPlugin.KHNeu.Views.KHNeuModels;
using CadPlugin.KHNeu.Views.KHNeuSettingsModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using wf = System.Windows.Forms;
using dwg = System.Drawing;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CadPlugin.KHNeu.Models.Persistence;

namespace CadPlugin.KHNeu.Views
{
    /// <summary>
    /// Interaction logic for KHNeuSettings.xaml
    /// </summary>
    public partial class KHNeuSettings : Window
    {
        private SettingsModel model;
        public KHNeuSettings()
        {
            InitializeComponent();
            model = new SettingsModel(Settings.Instance);
            DataContext = model;
        }


        private void shaftDown_Click(object sender, RoutedEventArgs e)
            => model.MoveShaftDown();

        private void shaftUp_Click(object sender, RoutedEventArgs e)
            => model.MoveShaftUp();

        private void pipeDown_Click(object sender, RoutedEventArgs e)
            => model.MovePipeDown();
        private void pipeUp_Click(object sender, RoutedEventArgs e)
            => model.MovePipeUp();

        private void shaftAdd_Click(object sender, RoutedEventArgs e)
            => model.AddShaft();
        private void pipeAdd_Click(object sender, RoutedEventArgs e)
            => model.AddPipe();

        private void shaftRemove_Click(object sender, RoutedEventArgs e)
            => model.RemoveShaft();
        private void pipeRemove_Click(object sender, RoutedEventArgs e)
            => model.RemovePipe();

        private void selectItemColor(SingleDisplayedText item)
        {
            if (item == null)
                return;
            var c = selectColor(item.TextColor);
            if (c.HasValue)
                item.TextColor = c.Value;
        }

        private void pipeColor_Click(object sender, MouseButtonEventArgs e)
        {
            var item = model.SelectedPipe;
            selectItemColor(item);
        }
        private void shaftColor_Click(object sender, MouseButtonEventArgs e)
        {
            var item = model.SelectedShaft;
            selectItemColor(item);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void arrowColor_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var c = selectColor(model.ArrowColor);
            if (c.HasValue)
                model.ArrowColor = c.Value;

        }
        private dwg.Color? selectColor(dwg.Color c)
        {
            var dlg = new wf.ColorDialog();
            dlg.Color = c;
            if (dlg.ShowDialog() == wf.DialogResult.OK)
                return dlg.Color;
            return null;
        }

        private void cell_LostFocus(object sender, RoutedEventArgs e)
        {
            var tb = e.Source as TextBox;
            if (tb == null)
                return;
            tb.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            tb.GetBindingExpression(TextBox.TextProperty).UpdateTarget();
        }

    }
}
