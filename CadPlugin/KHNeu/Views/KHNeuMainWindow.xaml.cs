﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CadPlugin.KHNeu.Views
{
    public class WindowEventArgs
    {
        public bool CancelClose { get; set; }
    }
    /// <summary>
    /// Interaction logic for KHNeuMainWindow.xaml
    /// </summary>
    public partial class KHNeuMainWindow : Window
    {
        public event Action SelectDTM;
        public event Action SelectNextPoint;
        /// <summary>
        /// 
        /// </summary>
        public event Action Draw;
        /// <summary>
        /// Invoked when closing - keep all data.
        /// </summary>
        public event Action<WindowEventArgs> Finish;
        /// <summary>
        /// Invoked when closing - user canceled.
        /// </summary>
        public event Action Cancel;

        public KHNeuMainWindow()
        {
            InitializeComponent();
            Closing += KHNeuMainWindow_Closing;
        }

        private void KHNeuMainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (DialogResult == true)
                return;
            string msg;// = "All created data will be deleted. \nContinue?";
            msg = "Alle Daten werden gelöscht. \nWeiter?";
            if (MessageBox.Show(msg, /*"Confirm"*/"Bestätigen", MessageBoxButton.YesNo, MessageBoxImage.Warning)
                != MessageBoxResult.Yes)
                e.Cancel = true;
        }

        private void selectDTM_Click(object sender, RoutedEventArgs e)
        {
            SelectDTM?.Invoke();
        }

        private void nextPoint_Click(object sender, RoutedEventArgs e)
        {
            SelectNextPoint?.Invoke();
        }

        private void draw_Click(object sender, RoutedEventArgs e)
        {
            Draw?.Invoke();
        }

        private void finish_Click(object sender, RoutedEventArgs e)
        {
            var args = new WindowEventArgs();
            Finish?.Invoke(args);
            if (args.CancelClose)
                return;
            DialogResult = true;
            Close();
        }

        private void globalKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var t = e.OriginalSource as TextBox;
                t?.GetBindingExpression(TextBox.TextProperty)?.UpdateSource();
            }
            
        }

        private void globalLostFocus(object sender, RoutedEventArgs e)
        {
            // When TextBox loses focus, if value type is invalid it is replaced
            // with last valid value.
            // * Note that value can still be logically invalid (type is good).
            var t = e.OriginalSource as TextBox;
            t?.GetBindingExpression(TextBox.TextProperty)?.UpdateSource();
            t?.GetBindingExpression(TextBox.TextProperty)?.UpdateTarget();
        }

        private void settings_Click(object sender, RoutedEventArgs e)
        {
            new KHNeuSettings().ShowDialog();
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            Cancel?.Invoke();
            Close();
        }
    }
}
