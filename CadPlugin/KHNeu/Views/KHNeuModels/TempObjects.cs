﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.KHNeu.Models.Datas;
using System.Threading;
using System.Windows.Markup.Localizer;
using CadPlugin.KHNeu.Models;

#if BRICSCAD
using Teigha.Colors;
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.KHNeu.Views.KHNeuModels
{
    /// <summary>
    /// Draws temporary objects for current shaft1-pipe-shaft2.
    /// </summary>
    class TempObjects : IDisposable
    {
        public static string DashedLayer = "123sewage_dashed_temp";
        public static double LinetypeScale = 0.05;

        private static int runningInstances = 0;
        private static object syncObject = new object();

        private Document doc;
        private List<ObjectId> createdObjects { get; } = new List<ObjectId>();
        public TempObjects(Document doc)
        {
            this.doc = doc;
        }

        public void DrawEntities(DataHistory mgr)
        {
            var shaft1Data = mgr.Shaft1;
            var shaft2Data = mgr.Shaft2;
            var pipeData = mgr.Pipe;
            lock (syncObject)
            {
                runningInstances++;
                Database db = doc.Database;
                Common.CADLayerHelper.AddLayerIfDoesntExist(db, DashedLayer);
                ObjectId linetypeId = Common.CADLineTypeHelper.GetLineTypeId(db, "DASHED2");
                Common.CADLayerHelper.SetLayerLineType(db, DashedLayer, linetypeId);

                using (TransactionWrapper tw = doc.StartTransaction(true))
                {
                    var shaft1 = tw.CreateEntity<Circle>();
                    shaft1.Color = Color.FromColor(System.Drawing.Color.LightGray);
                    shaft1.Diameter = shaft1Data.Diameter;
                    shaft1.Center = mgr.Shaft1.Center.ToCADPoint().To3d(shaft1Data.HeightDMP);
                    shaft1.Layer = DashedLayer;
                    shaft1.LinetypeScale = LinetypeScale;
                    createdObjects.Add(shaft1.Id);


                    var shaft2 = tw.CreateEntity<Circle>();
                    shaft2.Color = Color.FromColor(System.Drawing.Color.LightGray);
                    shaft2.Diameter = shaft2Data.Diameter;
                    shaft2.Center = mgr.Shaft2.Center.ToCADPoint().To3d(shaft2Data.HeightDMP);
                    shaft2.Layer = DashedLayer;
                    shaft2.LinetypeScale = LinetypeScale;
                    createdObjects.Add(shaft2.Id);

                    Vector3d direction = shaft1Data.Center.GetVectorTo(mgr.Shaft2.Center)
                        .ToCADVector().To3d().GetNormal();
                    var pipe = tw.CreateEntity<Line>();
                    pipe.Color = Color.FromColor(System.Drawing.Color.LightGray);
                    //pipe.StartPoint = shaft1Data.Center.ToCADPoint().GetAs3d()
                    //    .Add(direction * shaft1Data.Diameter / 2);
                    //pipe.EndPoint = shaft2Center.Add(direction.Negate() * shaft2Data.Diameter / 2);
                    pipe.StartPoint = pipeData.Point1.ToCADPoint();
                    pipe.EndPoint = pipeData.Point2.ToCADPoint();
                    pipe.Layer = DashedLayer;
                    pipe.LinetypeScale = LinetypeScale;
                    createdObjects.Add(pipe.Id);
                }

                doc.Editor.Regen();
            }
        }

        public void Dispose()
        {
            lock (syncObject)
            {
                runningInstances--;
                foreach (var id in createdObjects)
                    id.Erase();
                createdObjects.Clear();
                if (runningInstances == 0)
                    Common.CADLayerHelper.DeleteLayers(doc.Database, DashedLayer);
            }
        }
    }
}
