﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Views.KHNeuModels
{
    /// <summary>
    /// Invokes event if changed data requires 
    /// next point to be selected again.
    /// </summary>
    interface INextPointValidCheck
    {
        event Action ReselectNeeded;
        event Action RecalculateNeeded;
    }
}
