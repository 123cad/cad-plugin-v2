﻿using CadPlugin.KHNeu.Views.UserControls.PipeModels;
using CadPlugin.KHNeu.Views.UserControls.ShaftModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Views.KHNeuModels
{
    /// <summary>
    /// When some data is changed, checks if 
    /// Next Point needs to be reselected.
    /// </summary>
    class NextPointValidCheckDownstream : INextPointValidCheck
    {
        public event Action ReselectNeeded;
        public event Action RecalculateNeeded;

        private KHNeuModel model;
        private bool lastReselect = false;
        public NextPointValidCheckDownstream(KHNeuModel model)
        {
            this.model = model;

            model.Pipe.PropertyChanged += Pipe_PropertyChanged;
            model.Shaft1.PropertyChanged += Shaft1_PropertyChanged;
            model.Shaft2.PropertyChanged += Shaft2_PropertyChanged;
        }

        private void Shaft2_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (!model.NextPointValid)
                return;
            var shaft2 = model.Shaft1;
            var pipe = model.Pipe;
            switch (e.PropertyName)
            {
                case nameof(ShaftModel.IsDrawn):
                    break;
                case nameof(ShaftModel.Diameter):
                    raiseReselectIf(
                        pipe.UseMinSlope && pipe.CurrentSlope < pipe.MinSlope ||
                        pipe.UseSegments, true
                        );
                    raiseRecalculateIf(!lastReselect && !pipe.UseSegments);
                    break;
                case nameof(ShaftModel.HeightDMP):
                    raiseReselectIf(
                        pipe.UseMinSlope && pipe.CurrentSlope < pipe.MinSlope ||
                        pipe.UseSegments, true
                        );
                    raiseRecalculateIf(!lastReselect && !pipe.UseSegments);
                    break;
                case nameof(ShaftModel.HeightSMP):
                    raiseReselectIf(
                        pipe.UseMinSlope && pipe.CurrentSlope < pipe.MinSlope ||
                        pipe.UseSegments, true
                        );
                    raiseRecalculateIf(!lastReselect && !pipe.UseSegments);
                    break;
                case nameof(ShaftModel.InnerSlope):
                    raiseRecalculateIf();
                    break;
            }
        }

        private void Shaft1_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (!model.NextPointValid)
                return;
            var shaft1 = model.Shaft1;
            var shaft2 = model.Shaft2;
            var pipe = model.Pipe;
            switch (e.PropertyName)
            {
                case nameof(ShaftModel.IsDrawn):
                    break;
                case nameof(ShaftModel.Diameter):
                    raiseReselectIf(
                        pipe.UseMinSlope && pipe.CurrentSlope < pipe.MinSlope ||
                        pipe.UseSegments, true
                        );
                    break;
                case nameof(ShaftModel.HeightDMP):
                    raiseReselectIf(
                        pipe.UseMinSlope && pipe.CurrentSlope < pipe.MinSlope ||
                        pipe.UseSegments ||
                        shaft2.UseMinDelta, true
                        );
                    break;
                case nameof(ShaftModel.HeightSMP):
                    raiseReselectIf(
                        pipe.UseMinSlope && pipe.CurrentSlope < pipe.MinSlope ||
                        pipe.UseSegments ||
                        shaft2.UseMinDelta, true
                        );
                    break;
                case nameof(ShaftModel.InnerSlope):
                    raiseReselectIf(
                        pipe.UseMinSlope && pipe.CurrentSlope < pipe.MinSlope ||
                        pipe.UseSegments ||
                        shaft2.UseMinDelta, true
                        );
                    break;
            }
        }

        private void Pipe_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (!model.NextPointValid)
                return;
            var pipe = model.Pipe;
            switch(e.PropertyName)
            {
                case nameof(PipeModel.MinSlope):
                    raiseReselectIf(pipe.UseSegments, true);
                    raiseRecalculateIf(!lastReselect && pipe.UseMinSlope);
                    break;
                case nameof(PipeModel.UseSegments):
                    raiseReselectIf(pipe.UseSegments, false);
                    break;
                case nameof(PipeModel.UseMinSlope):
                    raiseReselectIf(!pipe.UseSegments);
                    raiseRecalculateIf(!lastReselect);
                    break;
                case nameof(PipeModel.UseAngleStep):
                    raiseReselectIf(pipe.UseAngleStep, true);
                    break;
                case nameof(PipeModel.AngleStep):
                    raiseReselectIf(pipe.UseAngleStep, true);
                    break;
                case nameof(PipeModel.StartEndSegmentLength):
                    raiseReselectIf(pipe.UseSegments, true);
                    break;
                case nameof(PipeModel.SegmentLength):
                    raiseReselectIf(pipe.UseSegments, true);
                    break;
            }
        }
        private void raiseReselectIf(bool expression, bool result = true)
        {
            if (expression == result)
            {
                ReselectNeeded?.Invoke();
                lastReselect = true;
            }
            lastReselect = false;
        }

        private void raiseRecalculateIf(bool expression = true, bool result = true)
        {
            RecalculateNeeded?.Invoke();
        }

    }
}
