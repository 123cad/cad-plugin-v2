﻿using CadPlugin.KHNeu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Views.KHNeuModels
{
    /// <summary>
    /// Manages DataHistory and keeps it in Sync with Model.
    /// </summary>
    public class DataHistoryManager
    {
        private bool updateData = true;
        private KHNeuModel model { get; }
        private DataHistory data { get; }

        public DataHistoryManager(KHNeuModel model, DataHistory data)
        {
            this.model = model;
            this.data = data;


            model.Shaft1.PropertyChanged += Shaft1_PropertyChanged;
            model.Pipe.PropertyChanged += Pipe_PropertyChanged;
            model.Shaft2.PropertyChanged += Shaft2_PropertyChanged;
        }


        private void Pipe_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (!updateData)
                return;
            data.RefreshPipe(model.Pipe);
        }

        private void Shaft1_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (!updateData)
                return;
            data.RefreshShaft1(model.Shaft1);
        }
        private void Shaft2_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (!updateData)
                return;
            data.RefreshShaft2(model.Shaft2);
        }

        /// <summary>
        /// Saves data to history, and moves Shaft2 to Shaft1.
        /// </summary>
        public void MoveToNext()
        {
            // Save current values, and prepare next ones.
            data.CreateAndMove();

            updateData = false;
            // Assigns values from Shaft2 to Shaft1.
            // This is not to trigger DataManager update multiple times.
            model.Shaft1.AssignFrom(model.Shaft2);
            updateData = true;

            data.RefreshAll(model.Shaft1, model.Pipe, model.Shaft2);
        }
    }
}
