﻿using CadPlugin.KHNeu.Models;
using CadPlugin.KHNeu.Models.TerrainHeights.TerrainHeightCalculators;
using CadPlugin.KHNeu.Views.Models;
using CadPlugin.KHNeu.Views.UserControls.PipeModels;
using CadPlugin.KHNeu.Views.UserControls.ShaftModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Views.KHNeuModels
{
    public class KHNeuModel : Notify    
    {
        public WaterDirection WaterDirection
        {
            get => _waterDirection;set => OnPropertyChanged(() => _waterDirection = value);
        }
        public HeightSourceType HeightType
        {
            get => _heightType;set => OnPropertyChanged(() => _heightType = value);
        }
        /// <summary>
        /// Height that user entered for static.
        /// </summary>
        public double StaticHeight
        {
            get => _staticHeight;set => OnPropertyChanged(() =>
            {
                _staticHeight = value;
                FixedHeight.Instance.Height = value;
            });
        }
        /// <summary>
        /// Valid when user selects it. 
        /// Can be invalidated depending on changed value in 
        /// the window.
        /// </summary>
        public bool NextPointValid
        {
            get => _nextPointValid;
            set => OnPropertyChanged(() => _nextPointValid = value);
        }
        


        public PipeModel Pipe { get; }
        public ShaftModel Shaft1 { get; } = new ShaftModel(true) { IsFirst = true };
        public ShaftModel Shaft2 { get; } = new ShaftModel(false) { IsFirst = false };

        private WaterDirection _waterDirection = WaterDirection.Downstream;
        private double _staticHeight = 100;
        private HeightSourceType _heightType = HeightSourceType.Static;
        private bool _nextPointValid = false;

        public KHNeuModel(WaterDirection water)
        {
            // Refresh height.
            StaticHeight = StaticHeight;
            WaterDirection = water;
            Pipe = new PipeModel(water);

            Shaft1.PropertyChanged += Shaft1_PropertyChanged;
            Shaft2.PropertyChanged += Shaft2_PropertyChanged;
            Pipe.PropertyChanged += Pipe_PropertyChanged;
            // Trigger property changed to update shaft2.
            Pipe.UseMinSlope = Pipe.UseMinSlope;
        }

        private void Shaft1_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ShaftModel.UseMinDelta))
                refreshShaft1();
        }

        private void Pipe_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(PipeModel.UseMinSlope))
                refreshShaft2();
        }

        private void refreshShaft1()
        {
            Shaft1.CanUserChangeSMP = !(/*Pipe.UseMinSlope || */Shaft1.UseMinDelta);
        }
        private void refreshShaft2()
        {
            Shaft2.CanUserChangeSMP = !(Pipe.UseMinSlope || Shaft2.UseMinDelta);
        }
        
        private void Shaft2_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ShaftModel.UseMinDelta))
                refreshShaft2();
        }

        public bool HasErrors()
        {
            if (Pipe.HasErrors)
                return true;
            if (Shaft1.HasErrors)
                return true;
            if (Shaft2.HasErrors)
                return true;
            return false;
        }
    }
}
