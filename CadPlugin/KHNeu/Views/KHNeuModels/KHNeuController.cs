﻿using CadPlugin.KHNeu.Models;
using CadPlugin.KHNeu.Models.DTMs;
using CadPlugin.KHNeu.Models.SewageCalculators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using win = System.Windows;
using CadPlugin.Common;
using CadPlugin.KHNeu.Models.TerrainHeights;
using CadPlugin.KHNeu.Views.Models;
using CadPlugin.KHNeu.Models.TextDisplay;
using CadPlugin.Common.CAD;
using CadPlugin.KHNeu.Models.EntityDrawers;
using CadPlugin.KHNeu.Models.Persistence;
using CadPlugin.KHNeu.Models.Datas;
using my = MyUtilities.Geometry;
using CadPlugin.KHNeu.Models.TerrainHeights.TerrainHeightCalculators;
using CadPlugin.KHNeu.Views.Shared;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.KHNeu.Views.KHNeuModels
{
    public class KHNeuController
    {
        private KHNeuModel mainModel;
        private DataHistory history;

        /// <summary>
        /// Keeps model in sync with current values in history.
        /// </summary>
        private DataHistoryManager historyManager { get; }
        private SewageCalculator calculator { get; }
        private INextPointValidCheck nextPointValidCheck { get; }
        private NextShaftJigManager jigManager { get; set; }
        /// <summary>
        /// Temporary drawn objects, for current selected next point.
        /// </summary>
        private TempObjects tempObjects { get; set; }
        private TextDrawerManager textManager { get; }
        private EntitiesDrawerManager entitiesManager { get; }
        private Document doc { get; }
        private ITerrainHeight lastHeightCalc { get; set; }
        private bool recalculating = false;
        public static void StartWindow(Document doc)
        {
            adjustCADPoint();


            Properties.KHNEU.Default.Upgrade();
            WaterDirection direction;
            ShaftPointType startPointType;
            if (new SelectDirectionWindow().ShowDialog() != true)
                return;
            direction = SelectDirectionWindow.Direction;
            startPointType = SelectDirectionWindow.ShaftPoint;

            string s = startPointType == ShaftPointType.DMP ? "DMP" : "SMP";
            var opts = new PromptPointOptions($"Startpunkt ({s}) wählen:");//"Select fist point: ");
            var res = doc.Editor.GetPoint(opts);
            if (res.Status != PromptStatus.OK)
                return;

            SettingsPersistence.LoadSettings(Settings.Instance);
            var window = new KHNeuMainWindow();

            var c = new KHNeuController(doc, window, direction, res.Value);
            window.Closed += (_, __) => window_closed(c);
            window.DataContext = c.mainModel;
            PersistenceModel.LoadModel(c.mainModel);
            setStartHeight();
            Application.ShowModalWindow(window);
            //window.ShowDialog();
            if (window.DialogResult == true)
            {
                if (new FinishDialog().ShowDialog() == true)
                    c.drawFinishEntities();
            }
            else
                c.cancel();

            SettingsPersistence.SaveSettings(Settings.Instance);
            PersistenceModel.SaveModel(c.mainModel);

            void setStartHeight()
            {
                var startPoint = res.Value;
                switch (startPointType)
                {
                    case ShaftPointType.DMP:
                        c.mainModel.Shaft1.HeightDMP = startPoint.Z;
                        break;
                    case ShaftPointType.SMP:
                        c.mainModel.Shaft1.HeightDMP = startPoint.Z + c.mainModel.Shaft1.Delta;
                        break;
                }
            }
        }

        private static void window_closed(KHNeuController c)
        {
            c.tempObjects?.Dispose();
            c.jigManager.Dispose();
        }

        /// <summary>
        /// Claus asked to set this when starting command.
        /// </summary>
        private static void adjustCADPoint()
        {
            SystemVariables.SetSystemVariable("PDMODE", 3);
            SystemVariables.SetSystemVariable("PDSIZE", 0.75);
        }

        private KHNeuController(Document doc, KHNeuMainWindow window, WaterDirection waterDirection, Point3d startPoint)
        {
            this.doc = doc;
            history = new DataHistory(waterDirection);
            mainModel = new KHNeuModel(waterDirection);
            historyManager = new DataHistoryManager(mainModel, history);
            textManager = new TextDrawerManager(doc, mainModel, history);
            entitiesManager = new EntitiesDrawerManager(history, doc);
            history.Shaft1.Center = startPoint.GetAs2d().FromCADPoint();
            mainModel.Shaft2.MinDelta = 2;
            mainModel.Shaft1.HeightSMP = startPoint.Z - mainModel.Shaft2.MinDelta;
            switch (waterDirection)
            {
                case WaterDirection.Downstream:
                    mainModel.Shaft1.Header = "Start-Schacht";// "Start shaft"; 
                    mainModel.Shaft2.Header = "End-Schacht";// "End shaft";
                    calculator = new DownstreamCalculator();
                    nextPointValidCheck = new NextPointValidCheckDownstream(mainModel);
                    break;
                case WaterDirection.Upstream:
                    mainModel.Shaft1.Header = "End-Schacht";//"End shaft";
                    mainModel.Shaft2.Header = "Start-Schacht";//"Start shaft";
                    calculator = new UpstreamCalculator();
                    nextPointValidCheck = new NextPointValidCheckDownstream(mainModel);
                    //throw new NotImplementedException("Upstream not implemented yet!");
                    break;
            }

            window.SelectDTM += () => DTMManager.Instance.SelectDTM(doc);
            window.SelectNextPoint += Window_SelectNextPoint;
            window.Draw += Window_Draw;
            window.Finish += Window_Finish;
            jigManager = new NextShaftJigManager(doc, history, mainModel, calculator);
            nextPointValidCheck.ReselectNeeded += () => mainModel.NextPointValid = false;
            nextPointValidCheck.RecalculateNeeded += NextPointValidCheck_RecalculateNeeded;
        }

        private void NextPointValidCheck_RecalculateNeeded()
        {
            if (!mainModel.NextPointValid)
                return;
            if (recalculating)
                return;
            recalculating = true;
            calculator.CalculateNextShaft(lastHeightCalc, history,
                mainModel.Shaft1, mainModel.Pipe, mainModel.Shaft2,
                history.Shaft2.GetDMP());
            recalculating = false;
        }

        /// <summary>
        /// Delete all created entities (undo)
        /// </summary>
        private void cancel()
        {
            textManager.Cancel();
            textManager.Dispose();
            entitiesManager.DeleteAll();
        }

        private void Window_Finish(WindowEventArgs args)
        {
            if (mainModel.NextPointValid)
            {
                string msg;// = $"Selected point hasn't been applied.\n Continue (changes will be lost)?";
                msg = "Gewählter Punkt kann nicht verarbeitet werden. Weiter (Daten gehen verloren)?";
                if (win.MessageBox.Show(msg, "", win.MessageBoxButton.YesNo, win.MessageBoxImage.Question) == win.MessageBoxResult.No)
                {
                    args.CancelClose = true;
                    return;
                }
            }

        }

        private bool checkErrors()
        {
            if (mainModel.HasErrors())
            {
                string msg;// = "Please fix errors first.";
                msg = "Bitte zunächst Fehler beheben!";
                win.MessageBox.Show(msg, /*"Unable to proceed"*/"Berechnung nicht möglich!", win.MessageBoxButton.OK, win.MessageBoxImage.Warning);
                return true;
            }
            return false;
        }

        private void drawFinishEntities()
        {
            if (!history.Elements.Any())
                return;
            CADLayerHelper.AddLayerIfDoesntExist(doc.Database, Layers.Pipe2dHelpLayer);
            CADLayerHelper.AddLayerIfDoesntExist(doc.Database, Layers.Pipe3dHelpLayer);
            CADLayerHelper.AddLayerIfDoesntExist(doc.Database, Layers.Pipe3dPolyLayer);


            var smpPoints = new List<my.Point3d>();
            var dmpPoints = new List<my.Point3d>();
            var smpRapPoints = new List<my.Point3d>();
            PipeData previousPipe = null;
            foreach (var d in history.Elements)
            {
                var shaft = d as ShaftData;
                var pipe = d as PipeData;
                if (shaft != null)
                {
                    smpPoints.Add(shaft.GetSMP());
                    dmpPoints.Add(shaft.GetDMP());
                    if (previousPipe != null)
                    {
                        smpRapPoints.Add(shaft.Center.GetWithHeight(previousPipe.Point2.Z));
                    }
                    smpRapPoints.Add(shaft.GetSMP());
                }

                if (pipe != null)
                {
                    smpRapPoints.Add(pipe.Point1);
                    smpRapPoints.Add(pipe.Point2);
                    previousPipe = pipe;
                }
            }

            smpRapPoints.RemoveAt(smpRapPoints.Count - 1);

            var settings = Settings.Instance;
            using (TransactionWrapper tw = doc.StartTransaction(true))
            {
                if (settings.DrawDMPLine)
                {
                    var pl = tw.CreateEntity<Polyline3d>(Layers.Pipe3dHelpLayer);
                    pl.AddPoints(dmpPoints.Select(x => x.ToCADPoint()).ToArray());
                }

                if (settings.DrawSMPLine)
                {
                    var pl = tw.CreateEntity<Polyline3d>(Layers.Pipe3dPolyLayer);
                    pl.AddPoints(smpPoints.Select(x => x.ToCADPoint()).ToArray());
                }

                if (settings.DrawSMPLine2d)
                {
                    var pl = tw.CreateEntity<Polyline>(Layers.Pipe2dHelpLayer);
                    pl.AddPoints(smpPoints.Select(x=>x.ToCADPoint().To2d()).ToArray());
                }

                if (settings.DrawRAPSMPRAPLine)
                {
                    var pl = tw.CreateEntity<Polyline3d>(Layers.Pipe3dPolyLayer);
                    pl.AddPoints(smpRapPoints.Select(x => x.ToCADPoint()).ToArray());
                }
            }

            BlockDrawerManager.DrawFinalBlocks(doc, history);
        }

        private void Window_Draw()
        {
            if (checkErrors())
                return;
            // Draw what is needed.
            textManager.DrawCurrent();
            textManager.MoveNext();
            entitiesManager.DrawCurrent();

            MoveToNext();

            // Just to allow user to see what is drawn.
            var opts = new PromptStringOptions(/*"Press Enter to continue..."*/"ENTER für Weiter…");
            doc.Editor.GetString(opts);
        }

        private void Window_SelectNextPoint()
        {
            if (checkErrors())
                return;

            if (mainModel.HeightType == Models.HeightSourceType.SelectDTM)
            {
                if (DTMManager.Instance.Current == null)
                    DTMManager.Instance.SelectDTM(doc);
                if (DTMManager.Instance.Current == null)
                {
                    win.MessageBox.Show(/*"Please select DTM."*/"Wähle Geländemodell:", "", win.MessageBoxButton.OK, win.MessageBoxImage.Warning);
                    return;
                }
            }
            TerrainHeightFactory.Instance.UpdateDocument(doc);
            using (var highlight = DTMTriangleHighlight.Instance.StartHighlight(doc))
            {
                ITerrainHeight heightCalc = null;
                if (mainModel.HeightType == HeightSourceType.SelectPoint)
                {
                    // For selected point, ask user only if segments are used.
                    // Otherwise, point is dynamically updated during jigging 
                    // (this allows to select height and point with user action).
                    // For segments it's more complicated, so preselected point is needed.
                    if (!mainModel.Pipe.UseSegments)
                        heightCalc = new SelectedPoint(new my.Point3d());
                }
                if (heightCalc == null)
                    heightCalc = TerrainHeightFactory.Instance.GetHeightCalculator(mainModel.HeightType);
                if (heightCalc == null)
                    // User cancel.
                    return;
                lastHeightCalc = heightCalc;
                (heightCalc as ITerrainHeightHighlight)?.Highlight();

                doc.Editor.Regen();

                // Create history.
                // Create Downstream/Upstream calculator.
                // Create some wrapper (for jigging). It is passed only current point, and invokes 
                //  others as needed (with required parameters)


                jigManager.SelectNextPoint(heightCalc);

                tempObjects?.Dispose();
                tempObjects = new TempObjects(doc);
                tempObjects.DrawEntities(history);

                mainModel.NextPointValid = true;
            }
        }

        /// <summary>
        /// Moving to next pipe.
        /// Shaft2 becomes Shaft1.
        /// </summary>
        public void MoveToNext()
        {
            tempObjects?.Dispose();
            tempObjects = null;
            jigManager.ClearLastPoint();
            historyManager.MoveToNext();
            mainModel.NextPointValid = false;
        }
    }
}
