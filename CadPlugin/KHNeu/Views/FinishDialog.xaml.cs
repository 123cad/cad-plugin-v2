﻿using CadPlugin.KHNeu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CadPlugin.KHNeu.Views
{
    /// <summary>
    /// Interaction logic for FinishDialog.xaml
    /// </summary>
    public partial class FinishDialog : Window
    {
        public FinishDialog()
        {
            InitializeComponent();
            DataContext = Settings.Instance;
        }

        private void buttonOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
