﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CadPlugin.KHNeu.Views.Shared.ValueConverters
{
    public class DiameterValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double d = (double)value;
            int p = (int)(Math.Round(d * 1000));
            return p;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                // Need to do like this (convert to double, not to int, and then parse invariant).
                // Backing field is double, so binding does not fail - need to handle this here.
                // Just in case to ignore , and . (they make problems during parsing).
                string s = value as string;
                s = s.Replace(',', '.');
                double d = DoubleHelper.ParseInvariantDouble(s);
                d /= 1000.0;
                return d;
            }
            catch
            {
                return value;
            }
        }
    }
}
