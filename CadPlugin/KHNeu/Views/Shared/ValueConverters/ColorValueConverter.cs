﻿using System;
using System.Collections.Generic;
using dw = System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace CadPlugin.KHNeu.Views.Shared.ValueConverters
{
    class ColorValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var color = (dw.Color)value;
            var c = color;
            var bg = new SolidColorBrush(Color.FromArgb(c.A, c.R, c.G, c.B));
            return bg;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
