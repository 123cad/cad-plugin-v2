﻿using CadPlugin.KHNeu.Views.KHNeuSettingsModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CadPlugin.KHNeu.Views.Shared.ValueConverters
{
    class DecimalsValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int i = (int)value;
            string s = "0";
            if (0 <= i && i < 6)
                s = SettingsModel.Digits[i];
            return s;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string s = value as string;
            s = s.Trim();
            int i = SettingsModel.Digits.IndexOf(s);
            if (i == -1)
                i = 0;
            return i;
        }
    }
}
