﻿using CadPlugin.KHNeu.Views.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CadPlugin.KHNeu.Views.Shared.ValueConverters
{
    public class HeightTypeValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var t = (HeightSourceType)value;
            string val = parameter as string;
            switch(val)
            {
                case "static":
                    return t == HeightSourceType.Static;
                case "point":
                    return t == HeightSourceType.SelectPoint;
                case "face":
                    return t == HeightSourceType.SelectFace3d;
                case "dtm":
                    return t == HeightSourceType.SelectDTM;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            HeightSourceType t = HeightSourceType.Static;
            string val = parameter as string;
            switch (val)
            {
                case "static":
                    t = HeightSourceType.Static;
                    break;
                case "point":
                    t = HeightSourceType.SelectPoint;
                    break;
                case "face":
                    t = HeightSourceType.SelectFace3d;
                    break;
                case "dtm":
                    return t = HeightSourceType.SelectDTM;
                    break;
            }
            return t;
        }
    }
}
