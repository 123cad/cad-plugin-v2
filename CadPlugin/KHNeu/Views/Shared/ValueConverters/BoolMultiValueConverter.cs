﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CadPlugin.KHNeu.Views.Shared.ValueConverters
{
    class BoolMultiValueConverter : IMultiValueConverter
    {
        /// <summary>
        /// If true, AND between values is used.
        /// If false, OR is used.
        /// </summary>
        public bool IsAND { get; set; }
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            // Initial value is neutral (true for AND, false for OR).
            bool result = IsAND;  
            foreach (bool b in values)
                if (IsAND)
                    result &= b;
                else
                    result |= b;
            return result;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
