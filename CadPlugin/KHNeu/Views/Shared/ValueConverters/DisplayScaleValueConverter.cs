﻿using CadPlugin.KHNeu.Models.TextDisplay;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CadPlugin.KHNeu.Views.Shared.ValueConverters
{
    class DisplayScaleValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var scale = (DisplayScale)value;
            string s = "";
            switch (scale)
            {
                case DisplayScale.Percent:
                    s = "%";
                    break;
                case DisplayScale.Permille:
                    s = "‰";
                    break;
            }
            return s;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var s = value as string;
            var scale = DisplayScale.Percent;
            switch (s)
            {
                case "%":scale = DisplayScale.Percent;
                    break;
                case "‰":scale = DisplayScale.Permille;
                    break;
            }
            return scale;
        }
    }
}
