﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Views.Shared
{
    public enum ShaftPointType
    {
        /// <summary>
        /// Cover height.
        /// </summary>
        DMP,
        /// <summary>
        /// Bottom height.
        /// </summary>
        SMP
    }
}
