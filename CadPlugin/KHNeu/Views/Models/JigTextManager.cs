﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using my = MyUtilities.Geometry;
using CadPlugin.KHNeu.Views.Models;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.KHNeu.Models.TextDisplay
{
	/// <remarks>Manages text displayed when jigging</remarks>
	public class JigTextManager
	{
		private class TextPair
		{
			public MText Text;
			public SingleDisplayedText Data;
		}
		private TextDisplaySourceData selectedData;
		/// <summary>
		/// Texts which are to be jigged.
		/// </summary>
		internal IEnumerable<Entity> Texts
		{
			get
			{
				var res = textPairs.Select(x => x.Text).Where(y => y != null).ToList();
				if (angleDisplay != null)
					res.Add(angleDisplay);
				return res;
			}
		}
		private List<SingleDisplayedText> texts = new List<SingleDisplayedText>();

		private List<TextPair> textPairs = new List<TextPair>();
		private MText angleDisplay = null;
		public TransactionWrapper tw { get; private set; }

		/// <summary>
		/// For triggering additional update operations.
		/// </summary>
		private event Action UpdateEvent;
		private Point3d lastPosition = new Point3d();
        string hiddenLayer = "123_jig_hidden";
		public JigTextManager(TransactionWrapper tw, TextDisplaySourceData sourceData)
		{
			this.tw = tw;
			selectedData = sourceData;
		}
		internal void Initialize(Point3d start)
		{
			texts.Clear();
			textPairs.Clear();
			double textHeight = 0.6;
			double textLineHeight = textHeight * 1.2;
			double currentDistance = 0;

            CADLayerHelper.AddLayerIfDoesntExist(tw.Db, hiddenLayer);

			if (Settings.Instance.ShowAngle && selectedData.PreviousPipe != null)
			{
				// Angle text
				angleDisplay = tw.CreateEntity<MText>();
				angleDisplay.TextHeight = 0.3;
				angleDisplay.Location = start;// selectedData.PipeData.EndPoint.ToCADPoint();// PipeShaftDataManager.Data.StartPoint.Add(new MyUtilities.Geometry.Vector3d(0, angleDisplay.TextHeight * 1.3, 0)).ToCADPoint();
				currentDistance -= angleDisplay.TextHeight * 2;
				//var centerPoint = PipeShaftDataManager.Data.StartPoint;
				//var lastPipeVector = PipeShaftDataManager.DataHistory.Last().Pipe.StartPoint.GetVectorTo(centerPoint).GetAs2d();
				UpdateEvent += () =>
				{
					double angle = selectedData.CurrentPipe.CurrentAngleDeg;// lastPipeVector.GetAngleDeg(PipeShaftDataManager.Data.Pipe.Direction.GetAs2d());
					angleDisplay.Contents = "Angle: " + DoubleHelper.ToStringInvariant(angle, 2) + "°";
				};
			}

			foreach (SingleDisplayedText single in Settings.Instance.ShaftTexts.Where(x=>x.DrawInJig))
			{
				MText text = tw.CreateEntity<MText>();
				text.Color = Color.FromColor(single.TextColor);
				text.TextHeight = textHeight;
				text.Location = start.Add(new Vector3d(0, currentDistance - textLineHeight, 0));
				currentDistance -= textLineHeight;

				textPairs.Add(new TextPair() { Data = single, Text = text });
				texts.Add(single);
			}
			foreach (SingleDisplayedText single in Settings.Instance.PipeTexts.Where(x=>x.DrawInJig))
			{
				MText text = tw.CreateEntity<MText>();

				text.Color = Color.FromColor(single.TextColor);
				text.TextHeight = textHeight;
				text.Location = start.Add(new Vector3d(0, currentDistance - textLineHeight, 0));
				currentDistance -= textLineHeight;

				textPairs.Add(new TextPair() { Data = single, Text = text });
				texts.Add(single);
			}

            foreach (var t in textPairs)
                t.Text.Layer = hiddenLayer;
            //CADLayerHelper.SetLayersFrozenState(tw.Doc, true, hiddenLayer);
				
			lastPosition = start;
		}


        public virtual void Update()
		{
			foreach (TextPair p in textPairs)
			{
				string s = p.Data.GetText(selectedData);
				p.Text.Contents = s;
			}
			if (UpdateEvent != null)
				UpdateEvent.Invoke();
		}

		/// <summary>
		/// Jigging finished, clear all up.
		/// </summary>
		public void Finish()
		{
			foreach (TextPair p in textPairs)
			{
				if (p.Text == null)
					continue;
				p.Text.Erase();
				p.Text = null;
			}
			if (angleDisplay != null)
				angleDisplay.Erase();
            CADLayerHelper.DeleteLayers(tw.Db, hiddenLayer);
        }
	}
}
