﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Views.Models
{
    public enum HeightSourceType
    {
        /// <summary>
        /// User inputs desired height manually.
        /// </summary>
        Static,
        /// <summary>
        /// User selects point on the drawing.
        /// </summary>
        SelectPoint,
        /// <summary>
        /// User selects Face3d, which is used.
        /// </summary>
        SelectFace3d,
        /// <summary>
        /// User selects DTM (PolyfaceMesh).
        /// Dynamically determine current Face, 
        /// and use it for calculation.
        /// </summary>
        SelectDTM
    }
}
