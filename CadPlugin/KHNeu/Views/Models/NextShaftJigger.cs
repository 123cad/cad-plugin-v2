﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Teigha.GraphicsInterface;
using CadPlugin.KHNeu.Models.TerrainHeights;
using CadPlugin.KHNeu.Models.DTMs;
using System.Windows.Media.Media3D;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.KHNeu.Views.Models
{
    /// <summary>
    /// Mouse position represents (potential) position for next shaft.
    /// Do recalculations, and draw some basic pipe-shaft layout.
    /// </summary>
    public class NextShaftJigger : DrawJig, IDisposable
    {

		/// <summary>
		/// Needed because i couldn't make Jig return Z coordinate.
		/// UserInputControls.Accept3dCoordinates doesn't work (V19 and V20).
		/// </summary>
		private PointMonitor ptMonitor;

		/// <summary>
		/// Refresh data with this point.
		/// </summary>
		public event Action<Point3d> PointUpdated;
        public Point3d CurrentMousePosition { get; private set; }
		/// <summary>
		/// Last CurrentMousePosition that was used for drawing entities.
		/// </summary>
		public Point3d LastUsedPosition { get; private set; }

		private Matrix3d _ed;
		/// <summary>
		/// Entities which are moved with selected point.
		/// </summary>
		private IEnumerable<Entity> movableEntities;
		/// <summary>
		/// All entities used in jig (draw them all).
		/// </summary>
		private IEnumerable<Entity> allEntities;
		private Line line;
        public NextShaftJigger(Editor ed, Line l, IEnumerable<Entity> movable, 
			IEnumerable<Entity> allEntities)
        {
			LastUsedPosition = l.EndPoint;
			this._ed = ed.CurrentUserCoordinateSystem;
			movableEntities = movable;
			this.allEntities = allEntities;
			foreach (var e in allEntities)
				if (!e.IsWriteEnabled)
					e.UpgradeOpen();
			line = l;
            ptMonitor = PointMonitor.Start(ed);
        }

		protected override SamplerStatus Sampler(JigPrompts prompts)
		{
			JigPromptPointOptions prOptions1 = new JigPromptPointOptions();
			prOptions1.UseBasePoint = true;
            prOptions1.BasePoint = new Point3d();
            prOptions1.UserInputControls = UserInputControls.Accept3dCoordinates
										   //|
                                           //UserInputControls.NullResponseAccepted
				//UserInputControls.NullResponseAccepted | 

				//| UserInputControls.GovernedByUCSDetect//
                                           | UserInputControls.GovernedByOrthoMode/* |
				//UserInputControls.AcceptMouseUpAsPoint*/
				;
			PromptPointResult prResult1 = prompts.AcquirePoint(prOptions1);
			if (prResult1.Status == PromptStatus.Cancel || prResult1.Status == PromptStatus.Error)
				return SamplerStatus.Cancel;
			

			if (prResult1.Value.Equals(CurrentMousePosition))
			{
				return SamplerStatus.NoChange;
			}

            Point3d tempPt = ptMonitor.CurrentPoint;// prResult1.Value.TransformBy(_ed.Inverse());
			CurrentMousePosition = tempPt;
			return SamplerStatus.OK;
		}

        protected override bool WorldDraw(WorldDraw draw)
        {
			WorldGeometry geo = draw.Geometry;
			if (geo != null)
			{
				//geo.PushModelTransform(UCS);

				//geo.Draw(text);

				Point3d currentPosition = CurrentMousePosition;
				Vector3d moveVector = LastUsedPosition.GetVectorTo(currentPosition);
				LastUsedPosition = currentPosition;
				PointUpdated?.Invoke(currentPosition);

				line.EndPoint = line.EndPoint.Add(moveVector);
				Matrix3d translation = Matrix3d.Displacement(moveVector);
				foreach (var e in movableEntities)
				{
					e.TransformBy(translation);
				}

				// Use WorldDraw instead of geo.Draw to avoid overlapping
				// entities problem.
                foreach (var e in allEntities)
                    e.WorldDraw(draw);
                    //geo.Draw(e);


                //geo.PopModelTransform();
            }

			return true;
		}

        public void Dispose()
        {
            ptMonitor?.Dispose();
        }
    }
}
