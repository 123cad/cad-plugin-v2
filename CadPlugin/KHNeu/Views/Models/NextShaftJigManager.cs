﻿using CadPlugin.KHNeu.Models;
using CadPlugin.KHNeu.Models.TerrainHeights;
using CadPlugin.KHNeu.Views.KHNeuModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.KHNeu.Models.Datas;
using CadPlugin.KHNeu.Models.TextDisplay;
using CadPlugin.Common;
using CadPlugin.KHNeu.Models.TerrainHeights.TerrainHeightCalculators;
using System.Diagnostics;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.KHNeu.Views.Models
{
    /// <summary>
    /// Gets point of next shaft, using jigging, while 
    /// keeping all created entities during lifetime of object.
    /// </summary>
    public class NextShaftJigManager : IDisposable
    {
        private Document doc;
        private DataHistory mgr;
        private KHNeuModel model;
        private SewageCalculator calc;
        /// <summary>
        /// Last selected point, if exists.
        /// </summary>
        private Point3d? lastPoint;
        public NextShaftJigManager(Document doc, DataHistory mgr, KHNeuModel model, SewageCalculator calc)
        {
            this.doc = doc;
            this.mgr = mgr;
            this.model = model;
            this.calc = calc;
        }

        /// <summary>
        /// When moved to next shaft, reset previous point.
        /// </summary>
        public void ClearLastPoint()
        {
            lastPoint = null;
        }

        public void SelectNextPoint(ITerrainHeight terrainCalc)
        {
            // Create entities for jig

            TempObjects temp = null;
            if (lastPoint.HasValue)
                temp = new TempObjects(doc);
            temp?.DrawEntities(mgr);

            using (TransactionWrapper tw = doc.StartTransaction(true))
            {
                Point3d startSMP = mgr.Shaft1.Center.ToCADPoint().GetAs3d();
                Point3d endSMP = mgr.Shaft2.Center.ToCADPoint().GetAs3d();
                Vector3d direction = startSMP.GetVectorTo(endSMP).GetNormal();

                var allJigEntities = new List<Entity>();
                Line l = new Line();// tw.CreateEntity<Line>();
                l.StartPoint = startSMP.Add(direction * model.Shaft1.Diameter / 2);
                l.EndPoint = endSMP.Add(direction.Negate() * model.Shaft2.Diameter / 2);
                allJigEntities.Add(l);

                var movableEntities = new List<Entity>();

                // Create shaft 1 circle and text
                Circle c1 = null;
                if (mgr.IsFirstPipe)
                {
                    c1 = new Circle();// tw.CreateEntity<Circle>();
                    c1.Radius = mgr.Shaft1.Diameter / 2;
                    c1.Center = startSMP;
                    allJigEntities.Add(c1);
                }

                // Create shaft 2 circle and text
                Circle c2 = new Circle();// tw.CreateEntity<Circle>();
                c2.Radius = mgr.Shaft2.Diameter / 2;
                c2.Center = endSMP;
                movableEntities.Add(c2);
                allJigEntities.Add(c2);

                Polyline arrow = new Polyline();
                arrow.Color = Color.FromColor(System.Drawing.Color.CornflowerBlue);
                double[] arrowWidths = new double[4];
                Action updateArrow = () =>
                {
                    var dir = mgr.Pipe.Point2.GetVectorTo(mgr.Pipe.Point1)
                        .GetAs2d().GetUnitVector().ToCADVector();
                    var startPoint = mgr.Pipe.Point2.GetAs2d().ToCADPoint();
					//Debug.WriteLine($"Arrow")
                    startPoint = startPoint.Add(dir * 0.1);// Add small offset from shaft.
                    double arrowBody = 0.05;
                    double arrowHead = 0.18;
                    double splitDistance;
                    double arrowLength = 0.8;
                    if (mgr.Pipe.Point1.Z < mgr.Pipe.Point2.Z)
                    {
                        // upstream
                        arrowWidths[0] = arrowBody;
                        arrowWidths[1] = arrowBody;
                        arrowWidths[2] = arrowHead;
                        arrowWidths[3] = 0;
                        splitDistance = 0.3;
                    }
                    else
                    {
                        // downstream
                        // upstream
                        arrowWidths[0] = 0;
                        arrowWidths[1] = arrowHead;
                        arrowWidths[2] = arrowBody;
                        arrowWidths[3] = arrowBody;
                        splitDistance = 0.5;
                    }
                    arrow.ReplacePoints(
                        new PolylineVertex<Point2d>(startPoint, arrowWidths[0], arrowWidths[1]),
                        new PolylineVertex<Point2d>(startPoint.Add(dir * splitDistance), arrowWidths[2], arrowWidths[3]),
                        new PolylineVertex<Point2d>(startPoint.Add(dir * arrowLength))
                    );
                };
                updateArrow();
                allJigEntities.Add(arrow);

                TextDisplaySourceData sourceData = new TextDisplaySourceData()
                {
                    CurrentPipe = model.Pipe,
                    PipeData = mgr.Pipe,
                    PreviousPipe = mgr.GetLastCreatedPipe(),
                    Shaft1Data = mgr.Shaft1,
                    Shaft2Data = mgr.Shaft2,
                    Shaft2Model = model.Shaft2
                };
                var textManager = new JigTextManager(tw, sourceData);
                textManager.Initialize(endSMP);
                movableEntities.AddRange(textManager.Texts);
                allJigEntities.AddRange(textManager.Texts);


                // Create and do jig (with updates)
                using (var jig = new NextShaftJigger(doc.Editor, l, movableEntities, allJigEntities))
                {


                    Action<Point3d> recalc = p =>
                    {
                        if (mgr.Shaft1.Center.GetDistanceTo(p.FromCADPoint().GetAs2d()) < 0.5)
                            return;
                        if (!model.Pipe.UseSegments)
                            // When no segments are used, selected point is dynamically updated.
                            (terrainCalc as SelectedPoint)?.SetPoint(p.FromCADPoint());
                        //doc.Editor.WriteMessage($"Depth1: {model.Shaft2.Delta:0.000} SMP: {model.Shaft2.HeightSMP:0.00}");
                        calc.CalculateNextShaft(terrainCalc, mgr,
                            model.Shaft1, model.Pipe, model.Shaft2, p.FromCADPoint());
                        //doc.Editor.WriteMessage($"Depth2: {model.Shaft2.Delta:0.000} SMP: {model.Shaft2.HeightSMP:0.00}");
                        l.StartPoint = mgr.Pipe.Point1.ToCADPoint();
                        l.EndPoint = mgr.Pipe.Point2.ToCADPoint();
                        c2.Center = mgr.Shaft2.Center.ToCADPoint().To3d();
						//Debug.WriteLine($"Center: {c2.Center.Y:0.0000000000000}");
						//Debug.WriteLine($"Start <{l.StartPoint}>, End <{l.EndPoint}>, Center <{c2.Center}>");
						updateArrow();
                        // Refresh displayed data.
                        textManager.Update();
                    };
                    jig.PointUpdated += recalc;

                    var jigResult = doc.Editor.Drag(jig);
                    textManager.Finish();
                    if (jigResult.Status != PromptStatus.OK)
                    {
                        // Restore old data.
                    }
                    else
                    {
                        // Last data is ok.
                        lastPoint = jig.LastUsedPosition;
                    }

                }

                // Apply last selected point
                if (true)
                {
                    //l.Erase();
                    //c1?.Erase();
                    //c2.Erase();
                    l.Dispose();
                    c1?.Dispose();
                    c2?.Dispose();
                }

            }
            temp?.Dispose();

            // Update values

            // Check result (enable cancel if possible).
        }

        public void Dispose()
        {
        }
    }
}
