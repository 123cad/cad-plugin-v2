﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.KHNeu.Models.DTMs;
using CadPlugin.Common.CAD;
using CadPlugin.Common;
using my = MyUtilities.Geometry;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.KHNeu._Diagnostics
{
    public class DiagnosticsDTM
    {
        private static Document doc => Application.DocumentManager.MdiActiveDocument;

        private static List<ObjectId> currentSectionData { get; } = new List<ObjectId>();
        public static void LoadDTM()
        {
            DTMManager.Instance.SelectDTM(doc);
            if (DTMManager.Instance.Current == null)
                return;
            drawDTMSections();
            deletePreviousSectionData();
        }
        public static void MarkTrianglesForSection()
        {
            var opts = new PromptPointOptions("Enter (row,column): ");
            opts.AllowArbitraryInput = true;
            var res = doc.Editor.GetPoint(opts);
            if (!(res.Status == PromptStatus.OK || res.Status == PromptStatus.Keyword))
                return;
            DTMSection section = null;
            if (res.Status == PromptStatus.OK)
            {
                // Point is selected. Find Section.
                section = DTMManager.Instance.Current.FindSection(res.Value.FromCADPoint());
            }
            else if (res.Status == PromptStatus.Keyword)
            {

                string str = res.StringResult.Replace(',', '.');
                var pars = str.Split('.');
                int row = int.Parse(pars[0]);
                int column = int.Parse(pars[1]);

                section = DTMManager.Instance.Current.Sections[row, column];
            }

            deletePreviousSectionData();
            using (TransactionWrapper tw = doc.StartTransaction(true))
            {
                var s = section;
                int counter = 0;
                foreach (var t in s.Triangles)
                {
                    Polyline pl = drawTriangle(tw, t);
                    currentSectionData.Add(pl.Id);

                    var text = tw.CreateEntity<MText>();
                    text.TextHeight = 0.4;
                    text.Contents = $"{counter}({t.Id})";// counter.ToString() + t.Id;
                    text.Location = t.GetCenterOfGravity().ToCADPoint();
                    currentSectionData.Add(text.Id);

                    pl = drawBoundary(tw, section);
                    currentSectionData.Add(pl.Id);

                    counter++;
                }
            }
        }
        private static Polyline drawBoundary(TransactionWrapper tw, DTMSection section)
        {
            var pl = tw.CreateEntity<Polyline>();
            pl.Closed = true;
            pl.Color = Color.FromColor(System.Drawing.Color.Gray);
            var bd = section.Boundary;
            var pts = new my.Point2d[]
            {
                my.RectangleExtensions.GetTopLeft(bd),
                my.RectangleExtensions.GetTopRight(bd),
                my.RectangleExtensions.GetBottomRight(bd),
                my.RectangleExtensions.GetBottomLeft(bd)
            };
            double width = 0.17;
            pl.AddPoints(
                new PolylineVertex<Point2d>(pts[0].ToCADPoint(), width, width),
                new PolylineVertex<Point2d>(pts[1].ToCADPoint(), width, width),
                new PolylineVertex<Point2d>(pts[2].ToCADPoint(), width, width),
                new PolylineVertex<Point2d>(pts[3].ToCADPoint(), width, width)
                );

            return pl;
        }
        private static Polyline drawTriangle(TransactionWrapper tw, Triangle t, double width = 0.1)
        {
            var pl = tw.CreateEntity<Polyline>();
            pl.Closed = true;
            pl.Color = Color.FromColor(System.Drawing.Color.Green);
            pl.AddPoints(t.GetPoints()
                .Select(x =>
                    new PolylineVertex<Point2d>(x.ToCADPoint().To2d(), width, width)
                ));
            return pl;
        }

        public static void TrackPoint()
        {
            ObjectId lastTriangle = ObjectId.Null;
            while (true)
            {
                var opts = new PromptPointOptions("Select point: ");
                var res = doc.Editor.GetPoint(opts);
                if (res.Status != PromptStatus.OK)
                    return;
                var triangle = DTMManager.Instance.Current.GetTriangle(res.Value.FromCADPoint());
                if (triangle == null)
                    continue;

                using (TransactionWrapper tw = doc.StartTransaction(true))
                {
                    if (!lastTriangle.IsNull)
                        tw.GetObjectWrite<DBObject>(lastTriangle).Erase();
                    var pl = drawTriangle(tw, triangle, 0.15);
                    pl.Color = Color.FromColor(System.Drawing.Color.Purple);
                    lastTriangle = pl.Id;
                }
            }
        }
        private static void deletePreviousSectionData()
        {
            if (currentSectionData == null || !currentSectionData.Any())
                return;

            using (TransactionWrapper tw = doc.StartTransaction(true))
            {
                foreach (var id in currentSectionData)
                    try
                    {
                        tw.GetObjectWrite<DBObject>(id).Erase();
                    }
                    catch { }
            }
            currentSectionData.Clear();
        }
        private static void drawDTMSections()
        {

            using (TransactionWrapper tw = doc.StartTransaction(true))
            {
                var sections = DTMManager.Instance.Current.Sections;
                int rows = sections.GetLength(0);
                int columns = sections.GetLength(1);
                //foreach(var s in DTMManager.Instance.Current.Sections)
                for (int i = 0; i < rows; i++)
                {
                    for (int j = 0; j < columns; j++)
                    {
                        var s = sections[i, j];
                        var pl = tw.CreateEntity<Polyline>();
                        pl.Closed = true;
                        pl.Color = Color.FromColor(System.Drawing.Color.Red);
                        double width = 0.1;
                        var pts = new my.Point2d[]
                        {
                            my.RectangleExtensions.GetTopLeft(s.Boundary),
                            my.RectangleExtensions.GetTopRight(s.Boundary),
                            my.RectangleExtensions.GetBottomRight(s.Boundary),
                            my.RectangleExtensions.GetBottomLeft(s.Boundary)
                        };
                        pl.AddPoints(
                            pts.Select(x=>new PolylineVertex<Point2d>(x.ToCADPoint(), width, width))
                            );

                        var text = $"{i},{j}";
                        var mText = tw.CreateEntity<MText>();
                        mText.Contents = text;
                        mText.TextHeight = 1.5;
                        mText.Location = (pts[0].Add(pts[0].GetVectorTo(pts[2]).Multiply(0.4))).ToCADPoint().GetAs3d();


                    }
                }
            }
        }

    }
}
