﻿using CadPlugin.KHNeu.Models.TerrainHeights;
using CadPlugin.KHNeu.Views.UserControls.PipeModels;
using CadPlugin.KHNeu.Views.UserControls.ShaftModels;
using MyUtilities.Geometry;
using System;
using CadPlugin.KHNeu.Models.PipeCalculators;
using MyUtilities.Helpers;
using SewageUtilities.Helpers;
using System.Text;

namespace CadPlugin.KHNeu.Models.SewageCalculators
{
    public class UpstreamCalculator : SewageCalculator
    {
        private PipeCalculatorSegmentUpstream segmentCalculator
            = new PipeCalculatorSegmentUpstream();
        public UpstreamCalculator() : base()
        {
        }

        void adjustMinSlope(PipeModel model, ref double slope)
        {
            if (model.UseMinSlope)
            {
                double minSlope = model.MinSlope / 100;
                if (minSlope > -slope)
                    slope = -minSlope;
            }
        }
        public override void CalculateNextShaft(
            ITerrainHeight terrainCalculator,
            DataHistory mgr,
            ShaftModel shaft1, PipeModel pipeModel, ShaftModel shaft2,
            Point3d nextPoint)
        {
            double terrainHeight2 = terrainCalculator.GetHeight(nextPoint);
            nextPoint = nextPoint.GetOnZ(terrainHeight2);
            Point2d nextPoint2d = nextPoint.GetAs2d();

            // Terrain height of shaft 2.
            bool nextPointChanged = false;

            var pipe = new PipeCalculatorUpstream();
            pipe.Initialize(mgr.Shaft1.Center, mgr.Shaft1.Radius, mgr.Shaft2.Radius, nextPoint2d);
            if (pipe.CheckLength(mgr.Shaft1.Center, ref nextPoint2d))
                nextPointChanged = true;
            if (pipe.AdjustPipeDirectionAngle(mgr, pipeModel, ref nextPoint2d))
            {
                nextPointChanged = true;
            }

            // Update terrain height on nextPoint, if needed.
            if (nextPointChanged)
            {
                terrainHeight2 = terrainCalculator.GetHeight(nextPoint);
                nextPoint = nextPoint2d.GetWithHeight(terrainHeight2);
            }
            shaft2.HeightDMP = terrainHeight2;
            

            double shaft1InnerSlopeDelta = PipeHelper.GetHeightForSlope(mgr.Shaft1.Diameter, shaft1.InnerSlope / 100);
            double shaft2InnerSlopeDelta = PipeHelper.GetHeightForSlope(mgr.Shaft2.Diameter, shaft2.InnerSlope / 100);
            
            double outHeight = 0;
            // Terrain height is maximum value for outHeight.
            if (pipeModel.UseMinSlope && shaft2.UseMinDelta)
            {
                // Calculate with min slope, and check if min delta is good.
                // If not, slope will be less than min slope (outHeight can only go down).
                double deltaHeight = -PipeHelper.GetEndZ(pipe.Start.GetAsPoint3d(), pipe.PipeEnd.GetAsPoint3d(), pipeModel.MinSlope / 100);
                outHeight = shaft1.HeightSMP + shaft1InnerSlopeDelta + deltaHeight;

                if (shaft2.HeightDMP - shaft2.MinDelta < outHeight)
                    outHeight = shaft2.HeightDMP - shaft2.MinDelta;

            }
            else if (pipeModel.UseMinSlope)
            {
                // Calculate by minSlope, but outHeight can't go above terrain height.
                double height = -PipeHelper.GetEndZ(pipe.Start.GetAsPoint3d(), pipe.PipeEnd.GetAsPoint3d(), pipeModel.MinSlope / 100);
                outHeight = shaft1.HeightSMP + shaft1InnerSlopeDelta + height;

                if (outHeight > shaft2.HeightDMP - shaft2InnerSlopeDelta)
                    outHeight = shaft2.HeightDMP - shaft2InnerSlopeDelta;
            }
            else if (shaft2.UseMinDelta)
            {
                // Will be less than terrain height.
                outHeight = shaft2.HeightDMP - shaft2.MinDelta - shaft2InnerSlopeDelta;
            }
            else
            {
                // Will be less than terrain height because SMP is <= .
                outHeight = shaft2.HeightSMP;
            }
            
            // Calculate slope.
            double slope;
            //outHeight = terrainHeight2 - (shaft2.UseMinDelta ? shaft2.MinDelta : 0) - shaft1InnerSlopeDelta;
            double pipeDeltaZ = mgr.Shaft1.HeightSMP + shaft1InnerSlopeDelta - outHeight;
            Vector3d pipeVector3d = pipe.Vector.GetAs3d(-pipeDeltaZ);
            slope = Calculations.GetSlope(pipeVector3d);

            /*double adjustedSlope = slope;
            adjustMinSlope(pipeModel, ref adjustedSlope);
            // If new slope would go over terrain height, keep old one.
            var tempPipeVector3d = GetVectorWithSlope(pipe.Vector, adjustedSlope);
            if (mgr.Shaft1.HeightSMP + shaft1InnerSlopeDelta + tempPipeVector3d.Z <=
                outHeight)
            {
                slope = adjustedSlope;
                pipeVector3d = tempPipeVector3d;
            }*/


            // Update calculate positions to data.
            double previousPipeOut = /*mgr.GetLastCreatedPipe()?.Point2.Z ??*/ mgr.Shaft1.HeightSMP;
            mgr.Pipe.Point1 = pipe.Start.GetWithHeight(previousPipeOut + shaft1InnerSlopeDelta);
            pipeModel.StartHeight = mgr.Pipe.Point1.Z;

            Action<Vector3d, Point3d> updateValues = (pipeVector, endShaftPoint) =>
            {
                double pipeSlope = Calculations.GetSlope(pipeVector);
                pipeModel.CurrentSlope = pipeSlope * 100;
                mgr.Pipe.Point2 = mgr.Pipe.Point1.Add(pipeVector);
                pipeModel.EndHeight = mgr.Pipe.Point2.Z;
                pipeModel.PipeLength2d = pipeVector.GetAs2d().Length;
                pipeModel.HeightDelta = pipeVector.Z * 100;
                mgr.Shaft2.Center = endShaftPoint.GetAs2d();
                double shaft2SMP = mgr.Pipe.Point2.Z;// - shaft2InnerSlopeDeltaZ;
                //if (shaft2.HeightSMP > shaft2SMP)
                shaft2.HeightSMP = shaft2SMP;

                pipeModel.SMPLength = mgr.Shaft1.Center.GetVectorTo(mgr.Shaft2.Center).Length;
            };
            updateValues(pipeVector3d, nextPoint);
            //mgr.Pipe.EndPoint = mgr.Pipe.StartPoint.Add(pipeVector3d);
            //pipeModel.CurrentSlope = slope;
            //pipeModel.PipeLength2d = pipeVector3d.GetAs2d().Length;
            //pipeModel.SMPLength = mgr.Shaft1.Center.GetVectorTo(nextPoint2d).Length;
            //pipeModel.HeightDelta = (pipeModel.StartHeight - pipeModel.EndHeight) * 100;

            // Set DMP.
            // If depth is smaller than minimum, set SMP with min delta (if used).
            // Update SMP, if needed, with pipeIn and innerSlope.
            //mgr.Shaft2.Center = nextPoint.GetAs2d();
            //if (shaft2.UseMinDelta)
            //if (mgr.Shaft2.HeightDMP - mgr.Shaft2.HeightSMP < shaft2.MinDelta)
            //mgr.Shaft2.HeightSMP = mgr.Shaft2.HeightDMP - shaft2.MinDelta;
            //double shaft2SMP = mgr.Pipe.EndPoint.Z - shaft2InnerSlopeDeltaZ;
            //// If user set SMP height, don't change it if it is valid.
            //if (mgr.Shaft2.HeightSMP > shaft2SMP)
            //    mgr.Shaft2.HeightSMP = shaft2SMP;

            // Slope should never be above min slope.
            //if (slope > pipeModel.MinSlope)
                //throw new InvalidOperationException();
            if (pipeModel.UseSegments)
            {
                // Adjust pipe.
                pipeVector3d = segmentCalculator.AdjustForSegment(mgr.Pipe.Point1, pipeVector3d, shaft1, pipeModel, shaft2, terrainCalculator);
                //pipeModel.CurrentSlope = Calculations.GetSlope(pipeVector3d);
                //mgr.Pipe.EndPoint = mgr.Pipe.StartPoint.Add(pipeVector3d);
                //pipeModel.EndHeight = mgr.Pipe.EndPoint.Z;
                //pipeModel.HeightDelta = (pipeModel.StartHeight - pipeModel.EndHeight) * 100;
                //shaft2SMP = mgr.Pipe.EndPoint.Z - shaft2InnerSlopeDeltaZ;
                //if (mgr.Shaft2.HeightSMP > shaft2SMP)
                //    mgr.Shaft2.HeightSMP = shaft2SMP;
                nextPoint = mgr.Pipe.Point1.Add(pipeVector3d).Add(pipeVector3d.GetUnitVector() * mgr.Shaft2.Radius);
                terrainHeight2 = terrainCalculator.GetHeight(nextPoint);
                nextPoint = nextPoint.GetOnZ(terrainHeight2);
                updateValues(pipeVector3d, nextPoint);

                mgr.Pipe.SetSegments(pipeModel.StartEndSegmentLength, pipeModel.SegmentLength);

                // Update end shaft center.
                //pipeModel.PipeLength2d = pipeVector3d.GetAs2d().Length;

                //pipeModel.SMPLength = mgr.Shaft1.Center.GetVectorTo(nextPoint2d).Length;
            }
        }
    }
}
