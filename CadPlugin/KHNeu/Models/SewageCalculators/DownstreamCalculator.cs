﻿using CadPlugin.KHNeu.Models.PipeCalculators;
using CadPlugin.KHNeu.Models.TerrainHeights;
using CadPlugin.KHNeu.Views.UserControls.PipeModels;
using CadPlugin.KHNeu.Views.UserControls.ShaftModels;
using MyUtilities.Geometry;
using MyUtilities.Helpers;
using SewageUtilities.Helpers;
using System;

namespace CadPlugin.KHNeu.Models.SewageCalculators
{
    
    public class DownstreamCalculator : SewageCalculator
    {
        private PipeCalculatorSegmentDownstream segmentCalculator 
            = new PipeCalculatorSegmentDownstream();
        public DownstreamCalculator() : base()
        {
        }

        void adjustMinSlope(PipeModel model, ref double slope)
        {
            if (model.UseMinSlope)
            {
                double minSlope = model.MinSlope / 100;
                if (minSlope > slope)
                    slope = minSlope;
            }
        }
        public override void CalculateNextShaft(
            ITerrainHeight terrainCalculator,
            DataHistory mgr,
            ShaftModel shaft1, PipeModel pipeModel, ShaftModel shaft2,
            Point3d nextPoint)
        {
            double terrainHeight2 = terrainCalculator.GetHeight(nextPoint);
            nextPoint = nextPoint.GetOnZ(terrainHeight2);
            Point2d nextPoint2d = nextPoint.GetAs2d();

            var mShaft1 = mgr.Shaft1;
            var mShaft2 = mgr.Shaft2;
            var mPipe = mgr.Pipe;
            double shaft1Radius = mShaft1.Radius;
            double shaft2Radius = mShaft2.Radius;
            // Terrain height of shaft 2.
            bool nextPointChanged = false;

            var pipe = new PipeCalculatorDownstream();
            pipe.Initialize(mShaft1.Center, shaft1Radius, shaft2Radius, nextPoint2d);
            if (pipe.CheckLength(mShaft1.Center, ref nextPoint2d))
                nextPointChanged = true;
            if (pipe.AdjustPipeDirectionAngle(mgr, pipeModel, ref nextPoint2d))
            {
                nextPointChanged = true;
            }

            // Update terrain height on nextPoint, if needed.
            if (nextPointChanged)
            {
                terrainHeight2 = terrainCalculator.GetHeight(nextPoint);
                nextPoint = nextPoint2d.GetWithHeight(terrainHeight2);
            }
            shaft2.HeightDMP = terrainHeight2;

            double shaft2InnerSlopeDeltaZ = PipeHelper.GetHeightForSlope(shaft2Radius * 2, shaft2.InnerSlope / 100);

            // Calculate slope.
            // Terrain height top height, pipe in must be below it.
            double slope;
            double inHeight;
            if (pipeModel.UseMinSlope && shaft2.UseMinDelta)
            {
                double deltaHeight = -PipeHelper.GetEndZ(pipe.PipeStart.GetAsPoint3d(), pipe.PipeEnd.GetAsPoint3d(), pipeModel.MinSlope / 100);
                inHeight = shaft1.HeightSMP - deltaHeight;

                if (inHeight > shaft2.HeightDMP - shaft2.MinDelta)
                    inHeight = shaft2.HeightDMP - shaft2.MinDelta;
            }
            else if (pipeModel.UseMinSlope)
            {
                double height = -PipeHelper.GetEndZ(pipe.PipeStart.GetAsPoint3d(), pipe.PipeEnd.GetAsPoint3d(), pipeModel.MinSlope / 100);
                inHeight = shaft1.HeightSMP - height;

                if (inHeight > shaft2.HeightDMP)
                    inHeight = shaft2.HeightDMP;
            }
            else if (shaft2.UseMinDelta)
            {
                inHeight = shaft2.HeightDMP - shaft2.MinDelta;
            }
            else
            {
                inHeight = shaft2.HeightSMP + shaft2InnerSlopeDeltaZ;
            }
                //inHeight = shaft2.HeightDMP - (shaft2.UseMinDelta ? shaft2.MinDelta : 0);// - pipeModel.Diameter;
            double pipeDeltaZ = mShaft1.HeightSMP - inHeight;
            Vector3d pipeVector3d = pipe.Vector.GetAs3d(-pipeDeltaZ);
            slope = Calculations.GetSlope(pipeVector3d);

            //adjustMinSlope(pipeModel, ref slope);

            pipeVector3d = GetVectorWithSlope(pipe.Vector, slope);


            double shaft1InnerSlopeDelta = PipeHelper.GetHeightForSlope(shaft1.Diameter, shaft1.InnerSlope / 100);
            double pipeOutHeight = mShaft1.HeightSMP;
            if (mgr.GetLastCreatedPipe() != null)
            {
                pipeOutHeight = mgr.GetLastCreatedPipe().Point2.Z - shaft1InnerSlopeDelta;
                shaft1.HeightSMP = pipeOutHeight;
            }
            // Update calculate positions to data.
            mPipe.Point1 = pipe.PipeStart.GetWithHeight(pipeOutHeight);
            pipeModel.StartHeight = mPipe.Point1.Z;

            Action<Vector3d, Point3d> updateValues = (pipeVector, endShaftPoint) =>
            {
                double pipeSlope = Calculations.GetSlope(pipeVector);
                pipeModel.CurrentSlope = pipeSlope * 100;
                mPipe.Point2 = mPipe.Point1.Add(pipeVector);
                pipeModel.EndHeight = mPipe.Point2.Z;
                pipeModel.PipeLength2d = pipeVector.GetAs2d().Length;
                pipeModel.HeightDelta = -pipeVector.Z * 100;
                mShaft2.Center = endShaftPoint.GetAs2d();
                double shaft2SMP = mPipe.Point2.Z - shaft2InnerSlopeDeltaZ;
                //if (shaft2.HeightSMP > shaft2SMP)
                    shaft2.HeightSMP = shaft2SMP;

                pipeModel.SMPLength = mShaft1.Center.GetVectorTo(mShaft2.Center).Length;
            };
            updateValues(pipeVector3d, nextPoint);
            //mgr.Pipe.EndPoint = mgr.Pipe.StartPoint.Add(pipeVector3d);
            //pipeModel.CurrentSlope = slope;
            //pipeModel.PipeLength2d = pipeVector3d.GetAs2d().Length;
            //pipeModel.SMPLength = mgr.Shaft1.Center.GetVectorTo(nextPoint2d).Length;
            //pipeModel.HeightDelta = (pipeModel.StartHeight - pipeModel.EndHeight) * 100;
            
            // Set DMP.
            // If depth is smaller than minimum, set SMP with min delta (if used).
            // Update SMP, if needed, with pipeIn and innerSlope.
            //mgr.Shaft2.Center = nextPoint.GetAs2d();
            //if (shaft2.UseMinDelta)
                //if (mgr.Shaft2.HeightDMP - mgr.Shaft2.HeightSMP < shaft2.MinDelta)
                    //mgr.Shaft2.HeightSMP = mgr.Shaft2.HeightDMP - shaft2.MinDelta;
            //double shaft2SMP = mgr.Pipe.EndPoint.Z - shaft2InnerSlopeDeltaZ;
            //// If user set SMP height, don't change it if it is valid.
            //if (mgr.Shaft2.HeightSMP > shaft2SMP)
            //    mgr.Shaft2.HeightSMP = shaft2SMP;

            if (pipeModel.UseSegments)
            {
                // Adjust pipe.
                pipeVector3d = segmentCalculator.AdjustForSegment(mPipe.Point1, pipeVector3d, shaft1, pipeModel, shaft2, terrainCalculator);
                //pipeModel.CurrentSlope = Calculations.GetSlope(pipeVector3d);
                //mgr.Pipe.EndPoint = mgr.Pipe.StartPoint.Add(pipeVector3d);
                //pipeModel.EndHeight = mgr.Pipe.EndPoint.Z;
                //pipeModel.HeightDelta = (pipeModel.StartHeight - pipeModel.EndHeight) * 100;
                //shaft2SMP = mgr.Pipe.EndPoint.Z - shaft2InnerSlopeDeltaZ;
                //if (mgr.Shaft2.HeightSMP > shaft2SMP)
                //    mgr.Shaft2.HeightSMP = shaft2SMP;
                nextPoint = mPipe.Point1.Add(pipeVector3d).Add(pipeVector3d.GetUnitVector() * shaft2Radius);
                terrainHeight2 = terrainCalculator.GetHeight(nextPoint);
                nextPoint = nextPoint.GetOnZ(terrainHeight2);
                updateValues(pipeVector3d, nextPoint);

                mPipe.SetSegments(pipeModel.StartEndSegmentLength, pipeModel.SegmentLength);

                // Update end shaft center.
                //pipeModel.PipeLength2d = pipeVector3d.GetAs2d().Length;

                //pipeModel.SMPLength = mgr.Shaft1.Center.GetVectorTo(nextPoint2d).Length;
            }
        }
    }
    
    
}
