﻿using CadPlugin.KHNeu.Models.TerrainHeights;
using MyUtilities.Geometry;
using MyUtilities.Helpers;
using SewageUtilities.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.PipeCalculators.Old_Refactor
{
    class SegmentedCalculatorUpstream
	{
		/// <summary>
		/// Calculates point2 of pipe when delta is fixed (point doesn't include delta!).
		/// </summary>
		public static Point3d CalculatePointWithMinDelta(SegmentedLengthIterator lengths, Vector3d nonSegmentedPipe, Point3d point1,
															ITerrainHeight terrainCalc, double minDelta, double shaft2Radius = 0)
		{
			double desiredXYLength = nonSegmentedPipe.GetAs2d().Length;

			Point2d startPointXY = point1.GetAs2d();
			Vector3d nonSegmentedUnit = nonSegmentedPipe.GetUnitVector();
			// XY terrain reference points (along XY direction of pipe), used to determine terrain line equation.
			Point2d terrainPoint2d1 = startPointXY.Add(nonSegmentedUnit.GetAs2d().GetUnitVector());
			Point2d terrainPoint2d2 = terrainPoint2d1.Add(nonSegmentedUnit.GetAs2d().GetUnitVector());
			double terrainHeight1 = terrainCalc.GetHeight(terrainPoint2d1.GetAsPoint3d());
			double terrainHeight2 = terrainCalc.GetHeight(terrainPoint2d2.GetAsPoint3d());


			// Transform all point to system with startPointXY as coordinates start.
			Vector2d offsetCenterVector = startPointXY.GetAsVector();
			Point2d terrainP1_XY = terrainPoint2d1.Add(offsetCenterVector.Negate());
			Point2d terrainP2_XY = terrainPoint2d2.Add(offsetCenterVector.Negate());
			Point2d? pointResult_Z = null;

			{
				// Transform to XY-Z plane. 
				// Suffix _Z for this plane.
				Point2d start_Z = new Point2d(0, point1.Z + minDelta);
				Point2d terrainP1_Z = new Point2d(terrainP1_XY.GetAsVector().Length, terrainHeight1);
				Point2d terrainP2_Z = new Point2d(terrainP2_XY.GetAsVector().Length, terrainHeight2);

				Line2dParameters lineParams = new Line2dParameters(terrainP1_Z, terrainP2_Z);
				{
					// Adjust terrain height to center of shaft.
					// If terrain line is going up, then need to move up (terrain height in shaft center
					// becomes terrain height at pipe in xy).
					// If going down, then need to move down.
					// This is achieved acording to K sign.
					double delta = lineParams.K * shaft2Radius;
					lineParams.MoveY(delta);
				}

				Func<double, SearchPointParams<Point2d>> calculation = (d) =>
				{
					Point2d? pt = LineCircleIntersection(start_Z, d, lineParams, desiredXYLength);
					if (pt == null)
						return SearchPointParams<Point2d>.Null;
					SearchPointParams<Point2d> pParams = new SearchPointParams<Point2d>(pt.Value, pt.Value.X);
					return pParams;
				};
				ClosestPointFinder<Point2d> finder = new ClosestPointFinder<Point2d>();
				finder.Search(calculation, desiredXYLength, lengths);
				Point2d? res_Z = finder.GetFinalPoint(desiredXYLength).Point;

				Point2d closestPoint_Z = res_Z.Value;
				closestPoint_Z = closestPoint_Z.Add(new Vector2d(0, -minDelta));
				pointResult_Z = closestPoint_Z;
			}
			Point2d resultXY = nonSegmentedUnit.GetAs2d().GetUnitVector().Multiply(pointResult_Z.Value.X).GetAsPoint();
			Point3d result = resultXY.GetWithHeight(pointResult_Z.Value.Y);
			result = result.Add(offsetCenterVector.GetAs3d());
			return result;
		}

        class PipeConversion
        {
			/// <summary>
			/// Point on Shaft2 (water flows downstream, to Point1).
			/// </summary>
			public Point3d Point2 { get; }
			/// <summary>
			/// From point2 to point1.
			/// </summary>
			public Vector3d NonSegmented { get; }
			/// <summary>
			/// Point on Shaft1 (water flows downstream, from Point1).
			/// </summary>
			public Point3d Point1 { get; }
			public Vector3d NonSegmentedUnit { get; }
			/// <summary>
			/// Pipe vector in 2d world.
			/// </summary>
			public Vector2d PipeXY { get; }
			/// <summary>
			/// Pipe unit vector in 2d world.
            /// </summary>
			public Vector2d PipeXYUnit { get; }
            public PipeConversion(Vector3d nonSegmentedPipe, Point3d point2)
            {
                NonSegmented = nonSegmentedPipe;
                NonSegmentedUnit = NonSegmented.GetUnitVector();
                PipeXY = new Vector2d(NonSegmented.GetAs2d().Length, NonSegmented.Z);
                PipeXYUnit = PipeXY.GetUnitVector();
                Point2 = point2;
                Point1 = Point2.Add(nonSegmentedPipe);
            }

            public static PipeConversion CreateWithMinSlope(Vector3d nonSegmented, Point3d point2, double minSlope)
            {
			    Vector2d t = nonSegmented.GetAs2d();
				double newZ = Math.Sign(nonSegmented.Z) * PipeHelper.GetHeightForSlope(t.Length, minSlope);
                Vector3d minSlopeNonSegmentedPipe = new Vector3d(t.X, t.Y, newZ);
                PipeConversion pc = new PipeConversion(minSlopeNonSegmentedPipe, point2);
				return pc;
            }
        }

        class TerrainHelper
        {
            private ITerrainHeight calculator { get; }
			private PipeConversion pipe { get; }

            public TerrainHelper(ITerrainHeight calculator, PipeConversion pipe)
            {
                this.calculator = calculator;
                this.pipe = pipe;
            }

            public Point3d GetPointBefore(Point3d p)
            {
                var pt = getPoint(p, -1);
                return pt;
            }

            public Point3d GetPointAfter(Point3d p)
			{
				var pt = getPoint(p, +1);
                return pt;
			}

            private Point3d getPoint(Point3d p, int sign)
            {
                var pt = p.Add(pipe.NonSegmentedUnit * sign * 0.5);
                var height = calculator.GetHeight(pt);
                return pt.GetOnZ(height);
			}
        }


		/// <summary>
		/// Calculates end point of pipe with both min slope and <c>System.</c>min delta are used (end point doesn't include delta).
		/// </summary>
		/// <param name="nonSegmentedPipe">From 2 to 1 point. Considered is that slope of this parameter is min slope.</param>
		/// <param name="point1Terrain">Terrain height for end point (which is fixed)</param>
		/// <param name="terrainCalc">Calculator for point 2.</param>
		/// <param name="point2">Pipe point on Shaft2.</param>
		public static Point3d? CalculatePointWithMinSlopeAndDelta(
            SegmentedLengthIterator lengths, Vector3d nonSegmentedPipe, Point3d point2,
		    double point1Terrain, ITerrainHeight terrainCalc, double minSlope, double minDelta,
			double point1Delta, double shaft2Radius = 0)
		{
			/*
			    Calculate with min slope, and check min delta value. 
			    If not good, move pipe out down, to min delta.

		        Min delta takes precedence (slope won't be in range).
			*/


			// Recalculate with minSlope (it can happen that nonSegmentedPipe is with minDelta and not slope).
			// Calculate first with minSlope, and then check minDelta.
            PipeConversion pipe = PipeConversion.CreateWithMinSlope(nonSegmentedPipe, point2, minSlope);
            TerrainHelper terrain = new TerrainHelper(terrainCalc, pipe);


			// Switch to XY-Z 2d system.
            double referenceLengthXY = pipe.PipeXY.Length;// minSlopeNonSegmentedPipe.GetAs2d().Length;
            //Vector2d directionXY = pipe.PipeXYUnit;// minSlopeNonSegmentedPipe.GetAs2d().GetUnitVector();
            //Vector3d direction = pipe.NonSegmentedUnit;// minSlopeNonSegmentedPipe.GetUnitVector();

			// Terrain points around point2.
            Point3d terrain1 = terrain.GetPointBefore(point2);// point2.Add(pipe.NonSegmentedUnit.Multiply(-0.5));
            Point3d terrain2 = terrain.GetPointAfter(point2);// point2.Add(pipe.NonSegmentedUnit.Multiply(+0.5));
			//double height1 = terrainCalc.GetHeight(terrain1);
			//double height2 = terrainCalc.GetHeight(terrain2);
			//terrain1 = terrain1.GetOnZ(height1);
			//terrain2 = terrain2.GetOnZ(height2);

			// Set start point as coordinate center (world for calculation).
			// OffsetVector defines position of point1, which is referent
			// point for calculations (point1 receives water).
            Vector3d offsetVector = pipe.Point1.GetOnZ().GetAsVector();// point2.GetAsVector().GetWithZ();
            Point3d center = pipe.Point1.Add(offsetVector.Negate());// new Point3d(0, 0, 0);
			// Move terrain points to be relative to center.
            Point3d relative1 = terrain1.Add(offsetVector.Negate());
			Point3d relative2 = terrain2.Add(offsetVector.Negate());

			// Transform to xy-z plane
			Point2d center_z;// = new Point2d(0, 0);
			Point2d terrain1_z = new Point2d(relative1.GetAsVector().Length, relative1.Z);
			Point2d terrain2_z = new Point2d(relative2.GetAsVector().Length, relative2.Z);

			Line2dParameters lineParams = new Line2dParameters(terrain1_z, terrain2_z);
			{
				// Adjust terrain height to center of shaft.
				// If terrain line is going up, then need to move up (terrain height in shaft center
				// becomes terrain height at pipe in xy).
				// If going down, then need to move down.
				// This is achieved according to K sign.
				double delta = lineParams.K * shaft2Radius;
				lineParams.MoveY(delta);
			}

			// Needed to use endPoint inside lambda.
			Point3d endPoint1 = point2;

			Func<double, SearchPointParams<Point3d>> calculation = (d) =>
			{
				double segmentedLength = d;

				// For 1 length:
				// - calculate point with length (with min slope)
				// - get terrain height for that point
				// - if terrain to point delta is > minDelta => point is good
				// - else calculate point with minDelta
				// -	calculate slope for that point
				// -	if slope is > minSlope => point is good
				// -	else
				//-			only possibility is that both slope and delta are != min values.
				//-			(not solved yet)

                Vector3d pipeSegmented = pipe.NonSegmentedUnit.Multiply(-segmentedLength);
                Point3d tempPoint2 = pipe.Point1.Add(pipeSegmented);
                double tempTerrainHeight = terrainCalc.GetHeight(tempPoint2);
                double delta = tempTerrainHeight - tempPoint2.Z;
                if (delta < minDelta)
                {
					// Not possible to go up.
					// Check if going down will solve the problem.
					// Point can exist, but with invalid slope.
                    center_z = new Point2d(0, pipe.Point1.Z + minDelta);
                    Point2d? p = LineCircleIntersection(center_z, segmentedLength, lineParams, referenceLengthXY);
					if (!p.HasValue)
                        return SearchPointParams<Point3d>.Null;

                    Vector2d tempPipe = center_z.GetVectorTo(p.Value);
                    double slope = Calculations.GetSlope(new Vector3d(tempPipe.X, 0, tempPipe.Y));
					if (slope < minSlope)
                        return SearchPointParams<Point3d>.Null;

                    tempPoint2 = pipe.PipeXYUnit.Multiply(p.Value.X).GetAs3d(p.Value.Y - minDelta).GetAsPoint();
                }

                return new SearchPointParams<Point3d>(tempPoint2, pipe.Point1.GetDistanceTo(tempPoint2));

				/*
				Vector3d pipeSegmented = pipe.NonSegmentedUnit.Multiply(segmentedLength);
				Point3d endPointTemp = new Point3d(endPoint1.X, endPoint1.Y, endPoint1.Z);
				Point3d startPoint = endPointTemp.Add(pipeSegmented);
				double startHeight = terrainCalc.GetHeight(startPoint);
				// Adjust start point delta.
				Vector3d offsetZ = new Vector3d(0, 0, -startPoint.Z + (startHeight - point1Delta));
				startPoint = startPoint.Add(offsetZ);
				endPointTemp = endPointTemp.Add(offsetZ);

				// Start point height is fixed (as delta from terrain).
				// Heights of start and end point are adjusted equally, 
				// so slope of pipe is not changed.
				double deltaZ = (startHeight - point1Delta) - startPoint.Z;
				Vector3d deltaZVector = new Vector3d(0, 0, deltaZ);
				startPoint = startPoint.Add(deltaZVector);
				Point3d tempEndPoint = endPointTemp.Add(deltaZVector);

				double delta = point1Terrain - tempEndPoint.Z;

				Point3d? resultStartPoint = null;
				if (delta < minDelta)
				{
					double endHeight = point1Terrain;// terrainCalc.GetHeight(tempEndPoint.GetAs2d());
					center_z = new Point2d(0, endHeight - minDelta + point1Delta);
					// Find circle intersection
					Point2d? p = LineCircleIntersection(center_z, segmentedLength, lineParams, referenceLengthXY);
					if (!p.HasValue)
						return SearchPointParams<Point3d>.Null;
					tempEndPoint = new Point3d(tempEndPoint.X, tempEndPoint.Y, endHeight - minDelta);

					Point2d result_z = p.Value.Add(new Vector2d(0, -point1Delta));
					if ((result_z.Y - tempEndPoint.Z) / result_z.X + 0.0001 < minSlope)
					{
						// Try to find a point (where both delta and slope are not equal to their minimum values).
						//Debug.Assert(false, "Point doesn't have min slope or min delta");
						return SearchPointParams<Point3d>.Null;
					}

					// Now we have good point, translate it to original coordinate system.
					Point2d pt = pipe.PipeXYUnit.Multiply(result_z.X).GetAsPoint();
					Point3d pt3d = pt.GetWithHeight(result_z.Y);
					pt3d = pt3d.Add(offsetVector);

					resultStartPoint = pt3d;
				}
				else
					resultStartPoint = startPoint;
				if (!resultStartPoint.HasValue)
					return SearchPointParams<Point3d>.Null;
				return new SearchPointParams<Point3d>(resultStartPoint.Value, tempEndPoint.GetVectorTo(resultStartPoint.Value).GetAs2d().Length)
				{
					ExtraPoint = tempEndPoint,
					Length2D = resultStartPoint.Value.GetVectorTo(endPointTemp).GetAs2d().Length
				};*/
			};
			ClosestPointFinder<Point3d> finder = new ClosestPointFinder<Point3d>();
			finder.Search(calculation, referenceLengthXY, lengths);
			if (!finder.LeftPointFound && !finder.RightPointFound)
				return null;
			SearchPointParams<Point3d> res = finder.GetFinalPoint(referenceLengthXY);
			// Only height should be changed.
			//Debug.Assert(DoubleHelper.AreEqual(endPoint.X, res.ExtraPoint.X, 0.0001), "upstream endPoint.X is changed!");
			//Debug.Assert(DoubleHelper.AreEqual(endPoint.Y, res.ExtraPoint.Y, 0.0001), "upstream endPoint.Y is changed!");
			point2 = res.ExtraPoint;
			return res.Point;
		}


		/// <summary>
		/// Calculates end point of pipe with both min slope and <c>System.</c>min delta are used (end point doesn't include delta).
		/// </summary>
		/// <param name="nonSegmentedPipe">From end to start point. Considered is that slope of this paramater is min slope.</param>
		/// <param name="endPointTerrain">Terrain height for end point (which is fixed)</param>
		/// <param name="terrainCalc">Calculator for start point.</param>
		public static Point3d? CalculatePointWithMinSlopeAndDeltaUpstream(SegmentedLengthIterator lengths, Vector3d nonSegmentedPipe, ref Point3d endPoint,
																			double endPointTerrain, ITerrainHeight terrainCalc, double minSlope, double minDelta,
																			double startPointDelta, double endShaftRadius = 0)
		{
			// Recalculate with minSlope (it can happen that nonSegmentedPipe is with minDelta and not slope).
			// Calculate first with minSlope, and then check minDelta.
			Vector2d t = nonSegmentedPipe.GetAs2d();
			double newZ = Math.Sign(nonSegmentedPipe.Z) * t.Length * minSlope;
			Vector3d minSlopeNonSegmentedPipe = new Vector3d(t.X, t.Y, newZ);

			double referenceLengthXY = minSlopeNonSegmentedPipe.GetAs2d().Length;
			Vector2d directionXY = minSlopeNonSegmentedPipe.GetAs2d().GetUnitVector();
			Vector3d direction = minSlopeNonSegmentedPipe.GetUnitVector();

			Point3d terrain1 = endPoint.Add(direction);
			Point3d terrain2 = endPoint.Add(direction.Multiply(2));
			double height1 = terrainCalc.GetHeight(terrain1);
			double height2 = terrainCalc.GetHeight(terrain2);
			terrain1 = terrain1.GetAs2d().GetWithHeight(height1);
			terrain2 = terrain2.GetAs2d().GetWithHeight(height2);

			// Set start point as coordinate center.
			Vector3d offsetVector = endPoint.GetAs2d().GetAsVector().GetAs3d();
			Point3d center = new Point3d(0, 0, endPoint.Z);
			Point2d terrainXY1 = terrain1.GetAs2d().Add(offsetVector.GetAs2d().Negate());
			Point2d terrainXY2 = terrain2.GetAs2d().Add(offsetVector.GetAs2d().Negate());

			// Transform to xy-z plane
			Point2d center_z = new Point2d(0, center.Z + minDelta);
			Point2d terrain1_z = new Point2d(terrainXY1.GetAsVector().Length, height1);
			Point2d terrain2_z = new Point2d(terrainXY2.GetAsVector().Length, height2);

			Line2dParameters lineParams = new Line2dParameters(terrain1_z, terrain2_z);
			{
				// Adjust terrain height to center of shaft.
				// If terrain line is going up, then need to move up (terrain height in shaft center
				// becomes terrain height at pipe in xy).
				// If going down, then need to move down.
				// This is achieved acording to K sign.
				double delta = lineParams.K * endShaftRadius;
				lineParams.MoveY(delta);
			}

			// Needed to use endPoint inside lambda.
			Point3d endPoint1 = endPoint;

			Func<double, SearchPointParams<Point3d>> calculation = (d) =>
			{
				double segmentedLength = d;

				// For 1 length:
				// - calculate point with length (with min slope)
				// - get terrain height for that point
				// - if terrain to point delta is > minDelta => point is good
				// - else calculate point with minDelta
				// -	calculate slope for that point
				// -	if slope is > minSlope => point is good
				// -	else
				//-			only possibility is that both slope and delta are != min values.
				//-			(not solved yet)

				Vector3d pipeSegmented = direction.Multiply(segmentedLength);
				Point3d endPointTemp = new Point3d(endPoint1.X, endPoint1.Y, endPoint1.Z);
				Point3d startPoint = endPointTemp.Add(pipeSegmented);
				double startHeight = terrainCalc.GetHeight(startPoint);
				// Adjust start point delta.
				Vector3d offsetZ = new Vector3d(0, 0, -startPoint.Z + (startHeight - startPointDelta));
				startPoint = startPoint.Add(offsetZ);
				endPointTemp = endPointTemp.Add(offsetZ);

				// Start point height is fixed (as delta from terrain).
				// Heights of start and end point are adjusted equally, 
				// so slope of pipe is not changed.
				double deltaZ = (startHeight - startPointDelta) - startPoint.Z;
				Vector3d deltaZVector = new Vector3d(0, 0, deltaZ);
				startPoint = startPoint.Add(deltaZVector);
				Point3d tempEndPoint = endPointTemp.Add(deltaZVector);

				double delta = endPointTerrain - tempEndPoint.Z;

				Point3d? resultStartPoint = null;
				if (delta < minDelta)
				{
					double endHeight = endPointTerrain;// terrainCalc.GetHeight(tempEndPoint.GetAs2d());
					center_z = new Point2d(0, endHeight - minDelta + startPointDelta);
					// Find circle intersecestion
					Point2d? p = LineCircleIntersection(center_z, segmentedLength, lineParams, referenceLengthXY);
					if (!p.HasValue)
						return SearchPointParams<Point3d>.Null;
					tempEndPoint = new Point3d(tempEndPoint.X, tempEndPoint.Y, endHeight - minDelta);

					Point2d result_z = p.Value.Add(new Vector2d(0, -startPointDelta));
					if ((result_z.Y - tempEndPoint.Z) / result_z.X + 0.0001 < minSlope)
					{
						// Try to find a point (where both delta and slope are not equal to their minimum values).
						//Debug.Assert(false, "Point doesn't have min slope or min delta");
						return SearchPointParams<Point3d>.Null;
					}

					// Now we have good point, translate it to original coordinate system.
					Point2d pt = directionXY.Multiply(result_z.X).GetAsPoint();
					Point3d pt3d = pt.GetWithHeight(result_z.Y);
					pt3d = pt3d.Add(offsetVector);

					resultStartPoint = pt3d;
				}
				else
					resultStartPoint = startPoint;
				if (!resultStartPoint.HasValue)
					return SearchPointParams<Point3d>.Null;
				return new SearchPointParams<Point3d>(resultStartPoint.Value, tempEndPoint.GetVectorTo(resultStartPoint.Value).GetAs2d().Length)
				{
					ExtraPoint = tempEndPoint,
					Length2D = resultStartPoint.Value.GetVectorTo(endPointTemp).GetAs2d().Length
				};
			};
			ClosestPointFinder<Point3d> finder = new ClosestPointFinder<Point3d>();
			finder.Search(calculation, referenceLengthXY, lengths);
			if (!finder.LeftPointFound && !finder.RightPointFound)
				return null;
			SearchPointParams<Point3d> res = finder.GetFinalPoint(referenceLengthXY);
			// Only height should be changed.
			endPoint = res.ExtraPoint;
			return res.Point;
		}

		public static Point2d LinesIntersection(Point2d p1_1, Point2d p1_2, Point2d p2_1, Point2d p2_2)
		{
			// y = kx + n
			// y - y1 = (y2 - y1) / (x2 - x1)  * (x - x1)
			// k = (y2 - y1) / (x2 - x1);
			// n = y1 - x1 * k;
			double k1 = (p1_2.Y - p1_1.Y) / (p1_2.X - p1_1.X);
			double k2 = (p2_2.Y - p2_1.Y) / (p2_2.X - p2_1.X);
			double n1 = p1_1.Y - p1_1.X * k1;
			double n2 = p2_2.Y - p2_2.X * k2;

			// y1 = k1 * x1 + n1
			// y1 = k2 * x2 + n2
			// Intersection point (x1 = x2 = x, y1 = y2 = y)
			// k1 * x + n1 = k2 * x + n2
			// x = (n1 - n2)/(k2 - k1)
			// y = k1 * x + n1
			double x0 = (n1 - n2) / (k2 - k1);
			double y0 = x0 * k1 + n1;

			return new Point2d(x0, y0);
		}
		/// <summary>
		/// Calculates intersection point between line and circle (from 2 results uses the one which is closer to referenceLength.
		/// </summary>
		public static Point2d? LineCircleIntersection(Point2d centerCircle, double radius, Line2dParameters lineParams, double referenceXYLength)
		{
			// (x - p) ^ 2 + (y - q) ^ 2 = r ^ 2
			double p = centerCircle.X;
			double q = centerCircle.Y;
			double r = radius;

			// y = k * x + n
			double k = lineParams.K; // (p2.Y - p1.Y) / (p2.X - p1.X);
			double n = lineParams.N; // p1.Y - p1.X * k;

			// Replacing y and solving for x:
			// a * x ^ 2 + b * x + c
			double a = 1 + k * k;
			double b = -2 * (p - k * (n - q));
			double c = (n - q) * (n - q) - r * r + p * p;

			double D = b * b - 4 * a * c;
			if (D < 0)
				return null;
			double x1 = (-b + Math.Sqrt(D)) / (2 * a);
			double x2 = (-b - Math.Sqrt(D)) / (2 * a);

			double length1 = x1 - centerCircle.X;
			double length2 = x2 - centerCircle.X;

			double x;
			double y;
			if (Math.Abs(length1 - referenceXYLength) < Math.Abs(length2 - referenceXYLength))
			{
				x = x1;
			}
			else
				x = x2;
			y = k * x + n;
			return new Point2d(x, y);

		}
	}
}
