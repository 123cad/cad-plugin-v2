﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.PipeCalculators.Old_Refactor
{
	/// <summary>
	/// Holds current min and max segment count. User can choose to 
	/// decrease min or increase max.
	/// </summary>
	public class SegmentedLengthIterator
	{
		private double SegmentLength;
		private double StartEndSegmentLength;
		/// <summary>
		/// Current number of segments in lower range.
		/// </summary>
		private double MinSegmentCount;
		/// <summary>
		/// Current number of segments on the right side.
		/// </summary>
		private double MaxSegmentCount;
		/// <summary>
		/// Length including min segments.
		/// </summary>
		public double MinLength { get; private set; }
		/// <summary>
		/// Indicates if min length value is good.
		/// </summary>
		public bool MinLengthValid { get { return MinSegmentCount > 0; } }
		/// <summary>
		/// Length including max segments.
		/// </summary>
		public double MaxLength { get; private set; }
		public SegmentedLengthIterator(double segmentLength, double startEndSegmentLength, int minSegments, int maxSegments)
		{
			SegmentLength = segmentLength;
			StartEndSegmentLength = startEndSegmentLength;
			MinSegmentCount = minSegments;
			MaxSegmentCount = maxSegments;
			MinSegmentCount++;
			DecreaseMinSegments();
			MaxSegmentCount--;
			IncreaseMaxSegments();
		}
		/// <summary>
		/// Decreases min segments by 1. When segment count is less than 0, returns false.
		/// </summary>
		/// <returns></returns>
		public bool DecreaseMinSegments()
		{
			MinSegmentCount--;
			MinLength = 0;
			if (MinSegmentCount < 1)
				return false;
			MinLength = SegmentLength * MinSegmentCount + 2 * StartEndSegmentLength;
			return true;
		}
		public void IncreaseMaxSegments()
		{
			MaxSegmentCount++;
			MaxLength = SegmentLength * MaxSegmentCount + 2 * StartEndSegmentLength;
		}

	}
}
