﻿using CadPlugin.KHNeu.Models.TerrainHeights;
using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.PipeCalculators.Old_Refactor
{
    class SegmentedCalculatorDownstream
    {
		/// <summary>
		/// Calculates end point of pipe when delta is fixed (point doesn't include delta!).
		/// </summary>
		public static Point3d CalculatePointWithMinDelta(SegmentedLengthIterator lengths, Vector3d nonSegmentedPipe, Point3d startPoint,
															ITerrainHeight terrainCalc, double minDelta, double endShaftRadius = 0)
		{
			double desiredXYLength = nonSegmentedPipe.GetAs2d().Length;

			Point2d startPointXY = startPoint.GetAs2d();
			Vector3d nonSegmentedUnit = nonSegmentedPipe.GetUnitVector();
			// XY terrain reference points (along XY direction of pipe), used to determine terrain line equation.
			Point2d terrainPoint2d1 = startPointXY.Add(nonSegmentedUnit.GetAs2d().GetUnitVector());
			Point2d terrainPoint2d2 = terrainPoint2d1.Add(nonSegmentedUnit.GetAs2d().GetUnitVector());
			double terrainHeight1 = terrainCalc.GetHeight(terrainPoint2d1.GetAsPoint3d());
			double terrainHeight2 = terrainCalc.GetHeight(terrainPoint2d2.GetAsPoint3d());


			// Transform all point to system with startPointXY as coordinates start.
			Vector2d offsetCenterVector = startPointXY.GetAsVector();
			Point2d terrainP1_XY = terrainPoint2d1.Add(offsetCenterVector.Negate());
			Point2d terrainP2_XY = terrainPoint2d2.Add(offsetCenterVector.Negate());
			Point2d? pointResult_Z = null;

			{
				// Transform to XY-Z plane. 
				// Suffix _Z for this plane.
				Point2d start_Z = new Point2d(0, startPoint.Z + minDelta);
				Point2d terrainP1_Z = new Point2d(terrainP1_XY.GetAsVector().Length, terrainHeight1);
				Point2d terrainP2_Z = new Point2d(terrainP2_XY.GetAsVector().Length, terrainHeight2);

				Line2dParameters lineParams = new Line2dParameters(terrainP1_Z, terrainP2_Z);
				{
					// Adjust terrain height to center of shaft.
					// If terrain line is going up, then need to move up (terrain height in shaft center
					// becomes terrain height at pipe in xy).
					// If going down, then need to move down.
					// This is achieved acording to K sign.
					double delta = lineParams.K * endShaftRadius;
					lineParams.MoveY(delta);
				}

				Func<double, SearchPointParams<Point2d>> calculation = (d) =>
				{
					Point2d? pt = LineCircleIntersection(start_Z, d, lineParams, desiredXYLength);
					if (pt == null)
						return SearchPointParams<Point2d>.Null;
					SearchPointParams<Point2d> pParams = new SearchPointParams<Point2d>(pt.Value, pt.Value.X);
					return pParams;
				};
				ClosestPointFinder<Point2d> finder = new ClosestPointFinder<Point2d>();
				finder.Search(calculation, desiredXYLength, lengths);
				Point2d? res_Z = finder.GetFinalPoint(desiredXYLength).Point;

				Point2d closestPoint_Z = res_Z.Value;
				closestPoint_Z = closestPoint_Z.Add(new Vector2d(0, -minDelta));
				pointResult_Z = closestPoint_Z;
			}
			Point2d resultXY = nonSegmentedUnit.GetAs2d().GetUnitVector().Multiply(pointResult_Z.Value.X).GetAsPoint();
			Point3d result = resultXY.GetWithHeight(pointResult_Z.Value.Y);
			result = result.Add(offsetCenterVector.GetAs3d());
			return result;
		}
		/// <summary>
		/// Calculates end point of pipe with both min slope and <c>System.</c>min delta are used (end point doesn't include delta).
		/// </summary>
		/// <param name="nonSegmentedPipe">From start to end point. Considered is that slope of this paramater is min slope.</param>
		public static Point3d? CalculatePointWithMinSlopeAndDeltaDownstream(SegmentedLengthIterator lengths, 
            Vector3d nonSegmentedPipe, Point3d startPoint, ITerrainHeight terrainCalc, 
            double minSlope, double minDelta, double endShaftRadius = 0)
		{
			// Recalculate with minSlope (it can happen that nonSegmentedPipe is with minDelta and not slope).
			// Calculate first with minSlope, and then check minDelta.
			Vector2d t = nonSegmentedPipe.GetAs2d();
			double newZ = Math.Sign(nonSegmentedPipe.Z) * t.Length * minSlope;
			Vector3d minSlopeNonSegmentedPipe = new Vector3d(t.X, t.Y, newZ);

			double referenceLengthXY = minSlopeNonSegmentedPipe.GetAs2d().Length;
			Vector2d directionXY = minSlopeNonSegmentedPipe.GetAs2d().GetUnitVector();
			Vector3d direction = minSlopeNonSegmentedPipe.GetUnitVector();

			Point3d terrain1 = startPoint.Add(direction);
			Point3d terrain2 = startPoint.Add(direction.Multiply(2));
			double height1 = terrainCalc.GetHeight(terrain1);
			double height2 = terrainCalc.GetHeight(terrain2);
			terrain1 = terrain1.GetAs2d().GetWithHeight(height1);
			terrain2 = terrain2.GetAs2d().GetWithHeight(height2);

			// Set start point as coordinate center.
			Vector3d offsetVector = startPoint.GetAs2d().GetAsVector().GetAs3d();
			Point3d center = new Point3d(0, 0, startPoint.Z);
			Point2d terrainXY1 = terrain1.GetAs2d().Add(offsetVector.GetAs2d().Negate());
			Point2d terrainXY2 = terrain2.GetAs2d().Add(offsetVector.GetAs2d().Negate());

			// Transform to xy-z plane
			Point2d center_z = new Point2d(0, center.Z + minDelta);
			Point2d terrain1_z = new Point2d(terrainXY1.GetAsVector().Length, height1);
			Point2d terrain2_z = new Point2d(terrainXY2.GetAsVector().Length, height2);

			Line2dParameters lineParams = new Line2dParameters(terrain1_z, terrain2_z);
			{
				// Adjust terrain height to center of shaft.
				// If terrain line is going up, then need to move up (terrain height in shaft center
				// becomes terrain height at pipe in xy).
				// If going down, then need to move down.
				// This is achieved acording to K sign.
				double delta = lineParams.K * endShaftRadius;
				lineParams.MoveY(delta);
			}

			Func<double, SearchPointParams<Point3d>> calculation = (d) =>
			{
				double segmentedLength = d;

				// For 1 length:
				// - calculate point with length (with min slope)
				// - get terrain height for that point
				// - if terrain to point delta is > minDelta => point is good
				// - else calculate point with minDelta
				// -	calculate slope for that point
				// -	if slope is > minSlope => point is good
				// -	else
				//-			only possibility is that both slope and delta are != min values.
				//-			(not solved yet)

				Vector3d pipeSegmented = direction.Multiply(segmentedLength);

				Point3d endPoint = startPoint.Add(pipeSegmented);
				double delta = terrainCalc.GetHeight(endPoint) - endPoint.Z;

				Point3d? resultPoint = null;
				if (delta < minDelta)
				{

					// Find circle intersecestion
					Point2d? p = LineCircleIntersection(center_z, segmentedLength, lineParams, referenceLengthXY);
					if (!p.HasValue)
						return SearchPointParams<Point3d>.Null;

					Point2d result_z = p.Value.Add(new Vector2d(0, -minDelta));
					if (-(result_z.Y - startPoint.Z) / result_z.X + 0.0001 < minSlope)
					{
						// Try to find a point (where both delta and slope are not equal to their minimum values).
						//Debug.Assert(false, "Point doesn't have min slope or min delta");
						return SearchPointParams<Point3d>.Null;
					}

					// Now we have good point, translate it to original coordinate system.
					Point2d pt = directionXY.Multiply(result_z.X).GetAsPoint();
					Point3d pt3d = pt.GetWithHeight(result_z.Y);
					pt3d = pt3d.Add(offsetVector);

					resultPoint = pt3d;
				}
				else
					//TEST Slope is not checked here?
					resultPoint = endPoint;
				if (!resultPoint.HasValue)
					return SearchPointParams<Point3d>.Null;
				return new SearchPointParams<Point3d>(resultPoint.Value, startPoint.GetVectorTo(resultPoint.Value).GetAs2d().Length)
				{
					Length2D = startPoint.GetVectorTo(resultPoint.Value).GetAs2d().Length
				};
			};
			ClosestPointFinder<Point3d> finder = new ClosestPointFinder<Point3d>();
			finder.Search(calculation, referenceLengthXY, lengths);
			if (!finder.LeftPointFound && !finder.RightPointFound)
				return null;
			Point3d res = finder.GetFinalPoint(referenceLengthXY).Point;
			return res;
		}

		public static Point2d LinesIntersection(Point2d p1_1, Point2d p1_2, Point2d p2_1, Point2d p2_2)
		{
			// y = kx + n
			// y - y1 = (y2 - y1) / (x2 - x1)  * (x - x1)
			// k = (y2 - y1) / (x2 - x1);
			// n = y1 - x1 * k;
			double k1 = (p1_2.Y - p1_1.Y) / (p1_2.X - p1_1.X);
			double k2 = (p2_2.Y - p2_1.Y) / (p2_2.X - p2_1.X);
			double n1 = p1_1.Y - p1_1.X * k1;
			double n2 = p2_2.Y - p2_2.X * k2;

			// y1 = k1 * x1 + n1
			// y1 = k2 * x2 + n2
			// Intersection point (x1 = x2 = x, y1 = y2 = y)
			// k1 * x + n1 = k2 * x + n2
			// x = (n1 - n2)/(k2 - k1)
			// y = k1 * x + n1
			double x0 = (n1 - n2) / (k2 - k1);
			double y0 = x0 * k1 + n1;

			return new Point2d(x0, y0);
		}
		/// <summary>
		/// Calculates intersection point between line and circle (from 2 results uses the one which is closer to referenceLength.
		/// </summary>
		public static Point2d? LineCircleIntersection(Point2d centerCircle, double radius, Line2dParameters lineParams, double referenceXYLength)
		{
			// (x - p) ^ 2 + (y - q) ^ 2 = r ^ 2
			double p = centerCircle.X;
			double q = centerCircle.Y;
			double r = radius;

			// y = k * x + n
			double k = lineParams.K; // (p2.Y - p1.Y) / (p2.X - p1.X);
			double n = lineParams.N; // p1.Y - p1.X * k;

			// Replacing y and solving for x:
			// a * x ^ 2 + b * x + c
			double a = 1 + k * k;
			double b = -2 * (p - k * (n - q));
			double c = (n - q) * (n - q) - r * r + p * p;

			double D = b * b - 4 * a * c;
			if (D < 0)
				return null;
			double x1 = (-b + Math.Sqrt(D)) / (2 * a);
			double x2 = (-b - Math.Sqrt(D)) / (2 * a);

			double length1 = x1 - centerCircle.X;
			double length2 = x2 - centerCircle.X;

			double x;
			double y;
			if (Math.Abs(length1 - referenceXYLength) < Math.Abs(length2 - referenceXYLength))
			{
				x = x1;
			}
			else
				x = x2;
			y = k * x + n;
			return new Point2d(x, y);

		}
	}
}
