﻿using CadPlugin.KHNeu.Models.PipeCalculators.Old_Refactor;
using CadPlugin.KHNeu.Models.SewageCalculators;
using CadPlugin.KHNeu.Models.TerrainHeights;
using CadPlugin.KHNeu.Views.UserControls.PipeModels;
using CadPlugin.KHNeu.Views.UserControls.ShaftModels;
using MyUtilities.Geometry;
using MyUtilities.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.PipeCalculators
{
    public class PipeCalculatorSegmentDownstream : PipeCalculatorSegment
    {
        /// <summary>
        /// Returns pipe vector, adjusted for segments.
        /// </summary>
        /// <param name="mgr"></param>
        /// <param name="shaft1"></param>
        /// <param name="pipeModel"></param>
        /// <param name="shaft2"></param>
        /// <param name="terrainCalc"></param>
        /// <returns></returns>
        public Vector3d AdjustForSegment(
            Point3d pipeStart,
            Vector3d nonSegmentedPipeVector,
            ShaftModel shaft1,
            PipeModel pipeModel,
            ShaftModel shaft2,
            ITerrainHeight terrainCalc)
        {
            if (pipeModel.SegmentLength < 1e-3)
                throw new NotSupportedException("Pipe segment can't be 0!");
            Vector3d pipeVector = nonSegmentedPipeVector;// mgr.Pipe.StartPoint.GetVectorTo(mgr.Pipe.EndPoint);
            int segmentCount = GetSegmentCount(pipeVector, pipeModel);

            if (pipeModel.UseMinSlope && !shaft2.UseMinDelta)
            {
                // Keep slope, make pipe shorter or longer and that's it.
                pipeVector = ApplySegmentAndKeepSlope(pipeVector, pipeModel, ref segmentCount);
            }
            else
            {
                // Use MinDelta (with or without MinSlope).
                Point3d calculatedPoint;
                if (!pipeModel.UseMinSlope)
                    calculatedPoint = calculatePointWithMinDelta_old(pipeVector, pipeStart, pipeModel, shaft2, segmentCount, terrainCalc);
                else
                    calculatedPoint = calculatePointWithMinSlopeDelta_old(pipeVector, pipeStart, pipeModel, shaft2, segmentCount, terrainCalc);
                pipeVector = pipeStart.GetVectorTo(calculatedPoint);// mgr.Pipe.StartPoint.GetVectorTo(calculatedPoint);
            }

            return pipeVector;
        }


        /// <summary>
        /// Adjusts pipe length, to match segments.
        /// Slope is not changed.
        /// </summary>
        private Vector3d ApplySegmentAndKeepSlope(Vector3d pipeVector, PipeModel model, ref int segmentCount)
        {
            double length2d = pipeVector.GetAs2d().Length;
            Vector3d direction = pipeVector.GetUnitVector();
            double basicLength = model.StartEndSegmentLength * 2;
            double segmentedLengthMin = basicLength + segmentCount * model.SegmentLength;
            double segmentedLengthMax = segmentedLengthMin + model.SegmentLength;

            double segmentedLength;
            if (segmentCount == 0)
            {
                segmentCount++;
                segmentedLength = segmentedLengthMax;
            }
            else
            {
                Vector3d temp1 = direction * segmentedLengthMin;
                Vector3d temp2 = direction * segmentedLengthMax;

                // Use closer one.
                double length2d1 = temp1.GetAs2d().Length;
                double length2d2 = temp2.GetAs2d().Length;
                double delta1 = length2d - length2d1;
                double delta2 = length2d2 - length2d;
                if (delta1 < delta2)
                    segmentedLength = segmentedLengthMin;
                else
                {
                    segmentCount++;
                    segmentedLength = segmentedLengthMax;
                }
            }

            return direction * segmentedLength;
        }

        #region From old code, could be refactored at some point

        //NOTE This is algorithm from previous version, not alternated at all.
        //NOTE For upstream calculations, maybe will need to refactor all this.
        //NOTE Need better solution for terrain height calculator (currently, for min delta)
        //NOTE it takes previous and next point, and creates line.

        private Point3d calculatePointWithMinDelta_old(Vector3d nonSegmentedPipe, Point3d pipeStart, PipeModel pipe, 
	        ShaftModel shaft2, int segmentCount, ITerrainHeight terrain)
        {
            var lengths = new SegmentedLengthIterator(pipe.SegmentLength, pipe.StartEndSegmentLength, segmentCount, segmentCount + 1);
            var pt = SegmentedCalculatorDownstream.CalculatePointWithMinDelta(lengths, nonSegmentedPipe, 
                pipeStart, terrain, shaft2.Delta, shaft2.Diameter / 2);
            return pt;
        }
        private Point3d calculatePointWithMinSlopeDelta_old(Vector3d nonSegmentedPipe, Point3d pipeStart, PipeModel pipe, 
	        ShaftModel shaft2, int segmentCount, ITerrainHeight terrain)
        {
            var lengths = new SegmentedLengthIterator(pipe.SegmentLength, pipe.StartEndSegmentLength, segmentCount, segmentCount + 1);
            var pt = SegmentedCalculatorDownstream.CalculatePointWithMinSlopeAndDeltaDownstream(lengths, nonSegmentedPipe,
                pipeStart, terrain, pipe.MinSlope, shaft2.MinDelta, shaft2.Diameter / 2);
            return pt.Value;
        }


        #endregion
    }
}
