﻿using CadPlugin.KHNeu.Views.UserControls.PipeModels;
using CadPlugin.KHNeu.Views.UserControls.ShaftModels;
using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.PipeCalculators
{
    public abstract class PipeCalculatorSegment
    {
        /// <summary>
        /// Returns number of segments for given pipe (lower bound, shorter than legth).
        /// (not including start and end).
        /// </summary>
        /// <param name="pipeVector"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        protected int GetSegmentCount(Vector3d pipeVector, PipeModel model)
        {
            double length3d = pipeVector.Length;
            double segmentCountD = (length3d - model.StartEndSegmentLength * 2) / model.SegmentLength;
            return (int)segmentCountD;
        }
    }
}
