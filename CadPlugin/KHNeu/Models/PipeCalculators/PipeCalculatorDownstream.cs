﻿using CadPlugin.KHNeu.Views.UserControls.PipeModels;
using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.PipeCalculators
{
    public class PipeCalculatorDownstream
    {
        /// <summary>
        /// Includes start point of pipe, and it's 2d length.
        /// </summary>
        public Vector2d Vector { get; set; }
        public Vector2d PipeDirection { get; set; }
        public double PipeLength2d { get; set; }
		public Point2d StartSMP { get; set; }
        public Point2d PipeStart { get; set; }
        public Point2d PipeEnd { get; set; }
        /// <summary>
        /// This is start shaft Radius, or 0 if no shaft exists.
        /// </summary>
        public double StartOffset { get; set; }
        /// <summary>
        /// End shaft radius, or 0 if no shaft exists.
        /// </summary>
        public double EndOffset { get; set; }

        public void Initialize(Point2d startSMP, double startShaftRadius, double nextShaftRadius, Point2d nextPoint)
        {
            StartOffset = startShaftRadius;
            EndOffset = nextShaftRadius;

            Vector2d smpV = startSMP.GetVectorTo(nextPoint);
            PipeDirection = smpV.GetUnitVector();

			StartSMP = startSMP;
            PipeStart = startSMP.Add(PipeDirection * startShaftRadius);
            PipeEnd = nextPoint.Add(PipeDirection * (-nextShaftRadius));
            Vector = PipeStart.GetVectorTo(PipeEnd);
            PipeLength2d = Vector.Length;
        }

        /// <summary>
        /// Adjusts length if too small, with moving nextPoint (only by XY).
        /// </summary>
        /// <returns>True if nextPoint was changed.</returns>
        public bool CheckLength(Point2d startSMP, ref Point2d nextPoint)
        {
            // Just to have some distance between points (0.1 should be neutral).
            double minimumPipeLength = 0.1;
            double minimumSMP = StartOffset + minimumPipeLength + EndOffset;

            double smpLength = startSMP.GetVectorTo(nextPoint).Length;

            if (smpLength < minimumSMP)
            {
                nextPoint = startSMP.Add(PipeDirection * minimumSMP);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Adjusts direction of pipe, depending on previous pipe using 
        /// AngleStep.
        /// Not applied to first pipe.
        /// </summary>
        public bool AdjustPipeDirectionAngle(DataHistory mgr, PipeModel model, ref Point2d nextPoint)
        {

            if (mgr.IsFirstPipe || !model.UseAngleStep)
                return false;

            var lastPipe = mgr.GetLastCreatedPipe();
            var lastDirection = lastPipe.Point1.GetVectorTo(lastPipe.Point2).GetAs2d();
            double angleDeg = lastDirection.GetAngleDeg(PipeDirection);
            int steps = (int)Math.Round(angleDeg / model.AngleStep);
            double newAngleDeg = steps * model.AngleStep;
            model.CurrentAngleDeg = newAngleDeg;
            var rotation = Matrix2d.CreateRotation(newAngleDeg);// - angleDeg);
            PipeDirection = rotation.Transform(lastDirection.GetUnitVector());// PipeDirection);
            PipeStart = StartSMP.Add(PipeDirection.Multiply(StartOffset));
            Vector = PipeDirection * PipeLength2d;
            PipeEnd = PipeStart.Add(Vector);
            nextPoint = PipeEnd.Add(PipeDirection * EndOffset);
            //Debug.WriteLine($"Y = {PipeEnd.Y:0.00000000} Direction: Y = {PipeDirection.Y:0.00000000000000} Length = {PipeDirection.Length:0.00000000000000}");
            //Debug.WriteLine($"Next point: <{nextPoint}>, PipeStart <{PipeStart}>, PipDir <{PipeDirection}>, PipeEnd <{PipeEnd}>, Length <{PipeLength2d:0.00}>");
            return true;
        }
    }
}
