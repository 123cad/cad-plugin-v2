﻿using CadPlugin.KHNeu.Models.Datas;
using CadPlugin.KHNeu.Views.KHNeuModels;
using CadPlugin.KHNeu.Views.UserControls.PipeModels;
using CadPlugin.KHNeu.Views.UserControls.ShaftModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models
{
    /// <summary>
    /// Keeps history of created elements, and keeps data 
    /// for current pipe and shaft.
    /// </summary>
    public class DataHistory
    {
        public WaterDirection WaterDirection{ get; }
        public virtual ShaftData Shaft1 { get; private set; }
        public virtual PipeData Pipe { get; private set; }
        public virtual ShaftData Shaft2 { get; private set; }
        /// <summary>
        /// Elements are arranged in following order: 
        /// ShaftData,PipeData,ShaftData[,PipeData,ShaftData[...]]
        /// </summary>
        public IEnumerable<Data> Elements => _elements;
        /// <summary>
        /// Index of current pipe's data (not yet created).
        /// </summary>
        public int CurrentPipeIndex => CreatedPipes;
        /// <summary>
        /// Number of pipes that are created (exist in history).
        /// (pipe is created only after calling MoveNext)
        /// </summary>
        public int CreatedPipes { get; private set; }
        public virtual bool IsFirstPipe => CurrentPipeIndex == 0;
		
        private List<Data> _elements = new List<Data>();
        public DataHistory(WaterDirection direction)
        {
            WaterDirection = direction;
            Shaft1 = new ShaftData();
            Pipe = new PipeData();
            Shaft2 = new ShaftData();
        }
        /// <summary>
        /// Returns pipe that was last created (current pipe 
        /// is not considered).
        /// </summary>
        /// <returns></returns>
        public virtual PipeData GetLastCreatedPipe()
        {
            if (!_elements.Any())
                return null;
            return (PipeData)_elements[_elements.Count - 2];
        }
        public PipeData GetPipe(int index)
        {
            int i = 0;
            foreach (var e in _elements)
            {
                var p = e as PipeData;
                if (p != null)
                {
                    i++;
                    if (i == index)
                        return p;
                }
            }
            return null;
        }

        public void RefreshShaft1(ShaftModel s)
        {
            refreshShaft(Shaft1, s);
        }
        public void RefreshShaft2(ShaftModel s)
        {
            refreshShaft(Shaft2, s);
        }
        private void refreshShaft(ShaftData sd, ShaftModel sm)
        {
            sd.Diameter = sm.Diameter;
            sd.HeightDMP = sm.HeightDMP;
            sd.HeightSMP = sm.HeightSMP;
            sd.IsDrawn = sm.IsDrawn;
        }
        public void RefreshPipe(PipeModel p)
        {
            Pipe.Diameter = p.Diameter;
        }
        public void RefreshAll(ShaftModel shaft1, PipeModel pipe, ShaftModel shaft2)
        {
            RefreshShaft1(shaft1);
            RefreshPipe(pipe);
            RefreshShaft2(shaft2);
        }

        /// <summary>
        /// Save current pipe (create it in history), and prepare data for next one.
        /// </summary>
        public void CreateAndMove()
        {
            if (!_elements.Any())
                _elements.Add(Shaft1);
            _elements.Add(Pipe);
            _elements.Add(Shaft2);

            Shaft1 = Shaft2;
            Pipe = new PipeData();
            Shaft2 = new ShaftData(Shaft2);

            CreatedPipes++;
        }
    }
}
