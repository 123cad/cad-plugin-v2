﻿using CadPlugin.KHNeu.Models.TerrainHeights;
using CadPlugin.KHNeu.Views.UserControls.PipeModels;
using CadPlugin.KHNeu.Views.UserControls.ShaftModels;
using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models
{
    /// <summary>
    /// Calculates pipe and shaft upstream and downstream.
    /// </summary>
    /// <remarks>
    /// Shaft1 and Shaft2:
    ///     Downstream: Shaft1 is Start shaft, Shaft2 is End shaft
    ///     Upstream: Shaft1 is End shaft, Shaft2 is Start shaft
    /// </remarks>
    public abstract class SewageCalculator
    {


        protected SewageCalculator()
        {
        }



        /// <summary>
        /// Calculates data for next shaft. Model parameters are used, for calculation, and results are populated
        /// to DataManager.
        /// </summary>
        /// <param name="nextPoint">Point that user selected for desired next shaft (with DMP height).</param>
        public abstract void CalculateNextShaft(ITerrainHeight terrainCalculator, DataHistory mgr, ShaftModel shaft1, PipeModel pipe, ShaftModel shaft2, Point3d nextPoint);

        /// <summary>
		/// Passes 2d vector and slope - returns 3d vector which has provided slope.
		/// (2d projection of returned 3d vector is same as passed 2d vector.)
		/// </summary>
		protected Vector3d GetVectorWithSlope(Vector2d xyVector, double slope)
        {

            double length2d = xyVector.Length;
            double depth = length2d * slope;

            Vector3d pipe = new Vector3d(xyVector.X, xyVector.Y, -depth);

            return pipe;
        }

        /// <summary>
        /// Ensures next point's distance is not too close.
        /// Returns flag to indicate if next point has been changed.
        /// </summary>
        protected bool SetPipeDirectionAndCheckDistance(
            Point2d smp1,
            double shaft1Radius,
            double shaft2Radius, 
            out Vector2d pipeVectorUnit,
            ref Point3d nextPoint
            )
        {
            // Just to have some distance between points (0.1 should be neutral).
            double minimumPipe2dLength = 0.1;
            double minimum2dSMPDistance = shaft1Radius + minimumPipe2dLength + shaft2Radius;
            Vector2d smpVector = smp1.GetVectorTo(nextPoint.GetAs2d());
            pipeVectorUnit = smpVector.GetUnitVector();

            if (smpVector.Length < minimum2dSMPDistance)
            {
                // Direction doesn't matter, on this small length.
                if (smpVector.Length < 1e-5)
                    smpVector = new Vector2d(1, 0);
                pipeVectorUnit = smpVector.GetUnitVector();
                smpVector = pipeVectorUnit * minimum2dSMPDistance;
                nextPoint = smp1.Add(smpVector).GetWithHeight(nextPoint.Z);
                return true;
            }
            return false;
        }
        protected void SetPipePoints(Point2d smp1, Point2d smp2, 
            double shaft1Radius, double shaft2Radius,
            Vector2d pipeDirectionUnit,
            out Point2d pipe1, out Point2d pipe2)
        {
            pipe1 = smp1.Add(pipeDirectionUnit * shaft1Radius);
            pipe2 = smp2.Add(pipeDirectionUnit * -shaft2Radius);
        }
    }
}
