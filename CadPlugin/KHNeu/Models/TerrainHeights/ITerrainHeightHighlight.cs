﻿using CadPlugin.KHNeu.Models.DTMs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.TerrainHeights
{
    /// <summary>
    /// ITerrainHeight can be highlighted.
    /// </summary>
    public interface ITerrainHeightHighlight
    {
        void Highlight();
    }
}
