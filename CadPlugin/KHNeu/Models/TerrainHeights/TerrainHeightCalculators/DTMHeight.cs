﻿using CadPlugin.KHNeu.Models.DTMs;
using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.TerrainHeights.TerrainHeightCalculators
{
    public class DTMHeight : ITerrainHeight, ITerrainHeightHighlight
    {

        public double GetHeight(Point3d p)
        {
            var t = DTMManager.Instance.GetTriangle(p);
            if (t == null)
                t = DTMManager.Instance.Last;
            double h = t?.GetHeight(p)??0;
            return h;
        }

        public void Highlight()
        {
            DTMTriangleHighlight.Instance.TrackCurrentTriangle(DTMManager.Instance);
        }
    }
}
