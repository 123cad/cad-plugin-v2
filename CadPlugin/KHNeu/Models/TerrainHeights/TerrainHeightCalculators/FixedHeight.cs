﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.TerrainHeights.TerrainHeightCalculators
{
    public class FixedHeight : ITerrainHeight
    {
        public static FixedHeight Instance { get; } = new FixedHeight();

        public double Height { get; set; }

        public double GetHeight(Point3d p)
            => Height;

        private FixedHeight() { }
    }
}
