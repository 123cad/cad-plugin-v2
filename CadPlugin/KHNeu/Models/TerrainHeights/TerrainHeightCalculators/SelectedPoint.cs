﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.TerrainHeights.TerrainHeightCalculators
{
    public class SelectedPoint : ITerrainHeight
    {
        public Point3d Point { get; private set; }

        public SelectedPoint(Point3d p)
        {
            Point = p;
        }

        public void SetPoint(Point3d p)
            => Point = p;

        public double GetHeight(Point3d p)
            => Point.Z;
    }
}
