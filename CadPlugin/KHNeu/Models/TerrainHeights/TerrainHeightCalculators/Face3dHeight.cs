﻿using CadPlugin.KHNeu.Models.DTMs;
using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.TerrainHeights.TerrainHeightCalculators
{
    public class Face3dHeight : ITerrainHeight, ITerrainHeightHighlight
    {
        public Triangle Triangle { get; }

        public Face3dHeight(Triangle triangle)
        {
            Triangle = triangle;
        }

        public double GetHeight(Point3d p)
            => Triangle.GetHeight(p);

        public void Highlight()
        {
            DTMTriangleHighlight.Instance.Draw(Triangle);
        }
    }
}
