﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.TerrainHeights
{
    /// <summary>
    /// Calculates terrain height at given point.
    /// </summary>
    public interface ITerrainHeight
    {
        double GetHeight(Point3d p);
    }
}
