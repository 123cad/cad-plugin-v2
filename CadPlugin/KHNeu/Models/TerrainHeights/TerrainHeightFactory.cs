﻿using CadPlugin.KHNeu.Models.TerrainHeights.TerrainHeightCalculators;
using CadPlugin.KHNeu.Views.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.KHNeu.Models.DTMs;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.KHNeu.Models.TerrainHeights
{
    public class TerrainHeightFactory
    {
        public static TerrainHeightFactory Instance { get; } = new TerrainHeightFactory();
        public Document doc { get; private set; }
        private Editor ed => doc.Editor;
        private TerrainHeightFactory() { }

        public void UpdateDocument(Document doc)
        {
            this.doc = doc;
        }
        public ITerrainHeight GetHeightCalculator(HeightSourceType heightType)
        {
            ITerrainHeight th = null;
            switch (heightType)
            {
                case HeightSourceType.Static:
                    th = FixedHeight.Instance;
                    break;
                case HeightSourceType.SelectPoint:
                    // Select point.
                    Point3d p = new Point3d();
                    if (!GetPointHeight(ref p))
                        th = null;
                    else
                        th = new SelectedPoint(p.FromCADPoint());
                    break;
                case HeightSourceType.SelectFace3d:
                    // Select Face
                    var triangle = GetFace();
                    if (triangle == null)
                        th = null;
                    else
                        th = new Face3dHeight(triangle);
                    break;
                case HeightSourceType.SelectDTM:
                    // Controller doesn't allow to get here without DTM.
                    th = new DTMHeight();
                    break;
            }
            return th;
        }
        private bool GetPointHeight(ref Point3d p)
        {
            string s;// = "\nSelect height point (Esc to cancel): ";
            s = "\nPunkt (DMP) wählen (ESC für Abbruch): ";
            PromptPointOptions opt = new PromptPointOptions(s);
            opt.AllowNone = false;
            opt.AllowArbitraryInput = false;
            PromptPointResult res = doc.Editor.GetPoint(opt);
            if (res.Status != PromptStatus.OK)
                return false;

            p = res.Value;
            return true;
        }
        private Triangle GetFace()
        {
            string s;// = "\nSelect 3d Face";
            s = "\n3D-Fläche wählen: ";
            PromptEntityOptions opt = new PromptEntityOptions(s);
            //s = "Not a 3d Face!";
            s = "Dies ist keine 3D-Fläche!";
            opt.SetRejectMessage(s);
            opt.AddAllowedClass(typeof(Face), true);
            PromptEntityResult res = ed.GetEntity(opt);
            if (res.Status != PromptStatus.OK)
                return null;


            using (TransactionWrapper tw = doc.StartTransaction(true))
            {
                var face = tw.GetObjectRead<Face>(res.ObjectId);
                var pts = CADFaceHelper.GetPoints(face);
                Triangle t = new Triangle(
                    pts.P1.FromCADPoint(),
                    pts.P2.FromCADPoint(),
                    pts.P3.FromCADPoint()
                    );
                return t;
            }

        }
    }
}
