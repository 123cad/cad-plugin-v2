﻿using my = MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedUtilities.CAD;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.KHNeu.Models.DTMs
{
    public class DTMManager
    {
        public event Action<Triangle> CurrentTriangleChanged;
        public static DTMManager Instance { get; } = new DTMManager();

        public DTM Current { get; private set; }


        /// <summary>
        /// Last returned triangle.
        /// </summary>
        public Triangle Last { get; private set; }


        private DTMManager() { }

        //TODO Select DTM, load it as DTM (load triangles and load into sections).

        /// <summary>
        /// Returns triangle for given point.
        /// </summary>
        public Triangle GetTriangle(my.Point3d p) 
        {
            var t = Current.GetTriangle(p);
            if (t != null)
            {
                Last = t;
                CurrentTriangleChanged?.Invoke(t);
            }
            return t;
        }

        public void SelectDTM(Document doc)
        {
            string s;// = "Select DTM (Polyface Mesh):";
            s = "Wähle Geländemodell (Vielflächennetz):";
            var opt = new PromptEntityOptions(s);
            opt.SetRejectMessage("\n" + s);
            opt.AddAllowedClass(typeof(PolyFaceMesh), true);
            var res = doc.Editor.GetEntity(opt);
            if (res.Status == PromptStatus.OK)
            {
                var oldCursor = System.Windows.Forms.Cursor.Current;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                // Load triangles.
                List<Triangle> triangles;
                using (TransactionWrapper tw = doc.StartTransaction())
                {
                    var pfm = tw.GetObjectWrite<PolyFaceMesh>(res.ObjectId);
                    var faces = pfm.ExtractFaces(tw);
                    triangles = new List<Triangle>(faces.Count());
                    foreach(var f in faces)
                    {
                        var pts = f.GetPoints();
                        var t = new Triangle();
                        t.SetPoints
                            (
                                pts.P1.FromCADPoint(),
                                pts.P2.FromCADPoint(),
                                pts.P3.FromCADPoint()
                            );
                        triangles.Add(t);

                        // Must be done, because objects are created but are not added to DB.
                        f.Dispose();
                    }

                    // Do commit even for readonly operations.
                    tw.Commit();
                }

                Current = new DTM();
                Current.Initialize(doc, triangles);

                System.Windows.Forms.Cursor.Current = oldCursor;

                // Create DTM (which splits by sections).
            }
        }
    }
}
