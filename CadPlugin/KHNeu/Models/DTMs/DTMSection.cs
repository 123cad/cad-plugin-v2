﻿using my = MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.KHNeu.Models.DTMs
{
    /// <summary>
    /// Represents section (rectangle) in DTM, which
    /// contains all triangles inside of it.
    /// </summary>
    public class DTMSection
    {
        public my.Rectangle<double> Boundary { get; set; }
        /// <summary>
        /// All triangles that belongs to this section.
        /// Triangle can belong to multiple sections.
        /// </summary>
        public List<Triangle> Triangles { get; } = new List<Triangle>();

        /// <summary>
        /// Does provided points belong to section.
        /// If true, it still doesn't confirm that it is contained
        /// in a triangle.
        /// </summary>
        public bool IsInside(my.Point3d p)
            => IsInsideX(p) && IsInsideY(p);

        /// <summary>
        /// Is inside section limited to X axis (y axis is not important, anywhere is good).
        /// </summary>
        public bool IsInsideX(my.Point3d p)
            => Boundary.X <= p.X && p.X <= (Boundary.X + Boundary.Width);

        public bool IsInsideY(my.Point3d p)
            => (Boundary.Y - Boundary.Width) <= p.Y && p.Y <= Boundary.Y;

        /// <summary>
        /// Tries to find triangle to which provided point belongs to.
        /// Returns null if not found.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Triangle FindTriangle(my.Point3d p)
        {
            if (!IsInside(p))
                return null;
            foreach(var t in Triangles)
            {
                if (t.IsPointInside(p))
                    return t;
            }

            return null;
        }
    }
}
