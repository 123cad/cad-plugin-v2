﻿using my = MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.KHNeu.Models.DTMs
{
    public class DTM
    {

        /// <summary>
        /// All triangles in DTM.
        /// </summary>
        private readonly List<Triangle> triangles = new List<Triangle>();
        internal DTMSection[,] Sections { get; private set; }

        public DTM()
        {
            Triangle.ResetId();
        }

        internal DTMSection FindSection(my.Point3d p)
        {
            foreach (var s in Sections)
            {
                if (s.IsInside(p))
                {
                    return s;
                }
            }
            return null;
        }

        /// <summary>
        /// Returns triangle which belongs to provided point.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Triangle GetTriangle(my.Point3d p)
        {
            DTMSection section = FindSection(p);
            if (section == null)
            {
                return null;
                throw new NotImplementedException("Not decided what to do when point doesn't have triangle.");
            }

            var triangle = section.FindTriangle(p);
            return triangle;
        }



        /// <summary>
        /// Creates DTM from triangles.
        /// </summary>
        public void Initialize(Document doc, IEnumerable<Triangle> triangles)
        {
            Sections = DTMSectionsSplit.GetSections(doc, triangles);
        }
    }
}
