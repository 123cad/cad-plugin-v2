﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.KHNeu.Models.DTMs
{

    /// <summary>
    /// Can draw traingle manually, or can track 
    /// and draw currently selected triangle in DTM.
    /// </summary>
    public class DTMTriangleHighlight
    {
        #region Highlight session
        /// <summary>
        /// Allow to start/end highlighting in using block.
        /// </summary>
        public interface IHighlightSession : IDisposable
        {
        }
        private class MyHighlightSession : IHighlightSession
        {
            public MyHighlightSession()
            {                
            }
            public void Dispose()
            {
                Instance.StopHighlight();
            }
        }
        #endregion

        public static DTMTriangleHighlight Instance { get; } = new DTMTriangleHighlight();

        private ObjectId id;
        private Document doc;
        private DTMTriangleHighlight() { }

        /// <summary>
        /// Invoke when selection of next point is started.
        /// </summary>
        public IHighlightSession StartHighlight(Document doc)
        {
            StopHighlight();
            this.doc = doc;
            return new MyHighlightSession();
        }

        /// <summary>
        /// Selection is finished.
        /// </summary>
        public void StopHighlight()
        {
            if (!id.IsNull)
            {
                // Check with Autocad.
                id.Erase();
                id = ObjectId.Null;
            }
            DTMManager.Instance.CurrentTriangleChanged -= Instance_CurrentTriangleChanged;
        }

        /// <summary>
        /// Draws provided Face (deletes old one).
        /// </summary>
        public void Draw(ObjectId faceId)
        {
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                var face = tw.GetObjectRead<Face>(faceId);
                var facePts = face.GetPoints().GetPoints();
                var triangle = new Triangle();
                triangle.SetPoints(facePts.Select(x=>x.FromCADPoint()));

                draw(tw, triangle);

                tw.Commit();
            }
        }
        public void Draw(Triangle t)
        {
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                draw(tw, t);
                tw.Commit();
            }
        }
        private void draw(TransactionWrapper tw, Triangle t)
        {
            if (!id.IsNull)
                // Check with Autocad.
                id.Erase();
            var pl = tw.CreateEntity<Polyline>();
            id = pl.Id;
            pl.Closed = true;
            pl.Color = Color.FromColor(System.Drawing.Color.Blue);
            pl.AddPoints(t.GetPoints().Select(x => new PolylineVertex<Point2d>(x.GetAs2d().ToCADPoint(), 0.1, 0.1)));
            
        }

        /// <summary>
        /// When current triangle in DTM is changed, it is updated.
        /// </summary>
        public void TrackCurrentTriangle(DTMManager mgr)
        {
            mgr.CurrentTriangleChanged += Instance_CurrentTriangleChanged;
        }

        private void Instance_CurrentTriangleChanged(Triangle obj)
        {
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                draw(tw, obj);
                tw.Commit();
            }
            doc.Editor.Regen();
        }
    }
}
