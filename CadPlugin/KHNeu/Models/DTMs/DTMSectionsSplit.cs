﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using my = MyUtilities.Geometry;
using NetworkKS.Isybau.NetworkTrees;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.KHNeu.Models.DTMs
{
    /// <summary>
    /// Splits DTM into sections (matrix like) with triangles.
    /// Each section contains all triangles which have any part 
    /// inside of section.
    /// </summary>
    public class DTMSectionsSplit
    {
        public static double SectionWidth { get; set; } = 10;
        public static double SectionHeight { get; set; } = 10;
        private class SectionTemp
        {
            public int RowIndex { get; }
            public int ColumnIndex { get; }
            public DTMSection Section { get; }
            /// <summary>
            /// Potential Triangles - could be inside Section, test them.
            /// </summary>
            public List<Triangle> Triangles { get; set; } = new List<Triangle>();

            public SectionTemp(int rowIndex, int columnIndex, DTMSection section)
            {
                RowIndex = rowIndex;
                ColumnIndex = columnIndex;
                Section = section;
            }
            /// <summary>
            /// Thread-safe add.
            /// </summary>
            public void AddTriangle(Triangle t)
            {
                lock (Section)
                {
                    Triangles.Add(t);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static DTMSection[,] GetSections(Document doc, IEnumerable<Triangle> triangles)
        {
            DTMSection[,] sections;
            // Calculate min and max points.
            my.Point2d min, max;
            getMinMax(triangles, out min, out max);

            // Split into sections
            sections = createMatrix(min, max);
            int rows = sections.GetLength(0);
            int columns = sections.GetLength(1);
            var tempSections = new SectionTemp[rows, columns];
            for (int i = 0; i < rows; i++)
                for (int j = 0; j < columns; j++)
                    tempSections[i, j] = new SectionTemp(i, j, sections[i, j]);

            // Split traingles to sections, with simple check if triangle belongs to 
            // the section.
            filterStage1(tempSections, triangles);
            //filterStage2(tempSections);
            filterStage3_Final(doc, tempSections);

            return sections;
        }

        /// <summary>
        /// Creates sections matrix in a rectangle limited by min and max points.
        /// </summary>
        private static DTMSection[,] createMatrix(my.Point2d min, my.Point2d max)
        {
            var sectionsX = getSectionsAlongX(max, min);
            var sectionsY = getSectionsAlongY(max, min);
            var tempSections = new DTMSection[sectionsY.Count, sectionsX.Count];
            double currentY = min.Y;
            for (int i = 0; i < sectionsY.Count; i++)
            {
                var sY = sectionsY[i];
                double currentX = min.X;
                currentY += sY;
                for (int j = 0; j < sectionsX.Count; j++)
                {
                    var sX = sectionsX[j];
                    var temp = new DTMSection();
                    temp.Boundary = new my.Rectangle<double>(currentX, currentY, sY, sX);

                    tempSections[i, j] = temp;
                    currentX += sX;
                }
            }
            return tempSections;
        }
        /// <summary>
        /// Returns min and max values in triangles collection.
        /// </summary>
        private static void getMinMax(IEnumerable<Triangle> triangles, out my.Point2d min, out my.Point2d max)
        {
            double minX, maxX, minY, maxY;
            minX = maxX = minY = maxY = 0;
            var first = triangles.FirstOrDefault();
            if (first != null)
            {

                minX = maxX = first.P1.X;
                minY = maxY = first.P1.Y;

                Action<my.Point3d> checkPoint = (p) =>
                {
                    if (p.X < minX)
                        minX = p.X;
                    if (p.X > maxX)
                        maxX = p.X;
                    if (p.Y < minY)
                        minY = p.Y;
                    if (p.Y > maxY)
                        maxY = p.Y;
                };

                foreach (var t in triangles)
                {
                    checkPoint(t.P1);
                    checkPoint(t.P2);
                    checkPoint(t.P3);
                }
            }
            min = new my.Point2d(minX, minY);
            max = new my.Point2d(maxX, maxY);
        }
        /// <summary>
        /// Returns collection of widths - each item is 1 section (column in matrix).
        /// </summary>
        private static List<double> getSectionsAlongX(my.Point2d max, my.Point2d min)
        {
            double sectionWidth = SectionWidth;
            double totalLength = max.X - min.X;
            List<double> sectionsLengths = createSections(sectionWidth, totalLength);
            return sectionsLengths;
        }
        /// <summary>
        /// Returns collection of heights - each item is 1 section (row in matrix).
        /// </summary>
        private static List<double> getSectionsAlongY(my.Point2d max, my.Point2d min)
        {
            double sectionWidth = SectionHeight;
            double totalLength = max.Y - min.Y;
            List<double> sectionsLengths = createSections(sectionWidth, totalLength);
            return sectionsLengths;
        }

        private static List<double> createSections(double sectionWidth, double totalLength)
        {
            var sectionsLengths = new List<double>();
            double current = 0;
            // Make all sections equal. If last section is too small,
            // merge it to previous one.
            while (current + sectionWidth < totalLength)
            {
                double d = sectionWidth;
                sectionsLengths.Add(d);
                current += d;
            }
            double delta = totalLength - sectionsLengths.Sum();
            if (delta / sectionWidth < 0.5)
            {
                int i = sectionsLengths.Count - 1;
                sectionsLengths[i] += delta;
            }
            else
                sectionsLengths.Add(delta);


            return sectionsLengths;
        }

        /// <summary>
        /// Assigns triangles to their possible sections.
        /// </summary>
        private static void filterStage1(SectionTemp[,] sections, IEnumerable<Triangle> triangles)
        {
            // Assign triangles to sections.
            int rows = sections.GetLength(0);
            int columns = sections.GetLength(1);
            var tasks = new Task[rows];
            for (int i = 0; i < rows; i++)
            {
                var task = new Task((k) =>
                {
                    int index = (int)k;
                    for (int j = 0; j < columns; j++)
                    {
                        var temp = sections[index, j];
                        var section = temp.Section;
                        //var pSect = new SectionTemp(index, j, section);
                        var rect = section.Boundary;
                        double sMinX = rect.X;
                        double sMaxX = rect.X + rect.Width;
                        double sMinY = rect.Y - rect.Height;
                        double sMaxY = rect.Y;
                        foreach (var t in triangles)
                        {
                            bool include = true;
                            if (t.MaxX < sMinX)
                                include = false;
                            if (sMaxX < t.MinX)
                                include = false;

                            if (t.MaxY < sMinY)
                                include = false;
                            if (sMaxY < t.MinY)
                                include = false;
                            // If triangle has min/max value in X range or in Y range.
                            // Or, if any point of triangle is in range.
                            if (sMinX < t.MinX && t.MinX < sMaxX)
                                include = true;
                            if (sMinX < t.MaxX && t.MaxX < sMaxX)
                                include = true;

                            if (sMinY < t.MinY && t.MinY < sMaxY)
                                include = true;
                            if (sMinY < t.MaxY && t.MaxY < sMaxY)
                                include = true;

                            if (!include)
                            {
                                // Special case when triangle is much bigger than section
                                // (it doesn't have any point inside, but section takes some
                                // part of triangle).
                                if (t.MinX < sMinX && sMaxX < t.MaxX)
                                    include = true;
                                if (t.MinY < sMinY && sMaxY < t.MaxY)
                                    include = true;
                            }

                            if (include)
                                temp.AddTriangle(t);
                        }
                    }
                }, i);
                tasks[i] = task;
                task.Start();
            }
            Task.WaitAll(tasks);
        }

        /// <summary>
        /// Performs little bit better test if triangle is inside section
        /// (checks if any vertex of triangle is inside section - NOT GOOD CONDITION).
        /// After method is finished, triangles are either inside section or cutting it.
        /// </summary>
        [Obsolete("Bad condition used in this stage")]
        private static void filterStage2(SectionTemp[,] sections)
        {
            int rows = sections.GetLength(0);
            int columns = sections.GetLength(1);
            var tasks = new Task[rows];
            for (int i = 0; i < rows; i++)
            {
                var task = new Task((index) =>
                {
                    int ii = (int)index;
                    for (int j = 0; j < columns; j++)
                    {
                        var temp = sections[ii, j];
                        var s = temp.Section;
                        for (int iT = 0; iT < temp.Triangles.Count; iT++)
                        {
                            var t = temp.Triangles[iT];
                            throw new InvalidOperationException("Not good because triangle point doens't need to be inside of section");
                            if (s.IsInside(t.P1) ||
                                s.IsInside(t.P2) ||
                                s.IsInside(t.P3))
                            {
                                s.Triangles.Add(t);
                                temp.Triangles[iT] = null;
                            }
                        }
                        temp.Triangles = temp.Triangles.Where(x => x != null).ToList();
                    }
                }, i);
                tasks[i] = task;
                task.Start();
            }
            Task.WaitAll(tasks);
        }

        /// <summary>
        /// Final stage - check if triangle is part of section
        /// (check if there is intersection between section and triangle
        /// - if yes, then triangle has a part inside of the section).
        /// </summary>
        private static void filterStage3_Final(Document doc, SectionTemp[,] tempSections)
        {
            using (TransactionWrapper tw = doc.StartTransaction(true))
            {
                // Extract here for reusing.
                var pl = tw.CreateEntity<Polyline>();
                pl.Closed = true;
                var tPl = tw.CreateEntity<Polyline>();
                tPl.Closed = true; 
                var intersectionPoints = new Point3dCollection();

                int rows = tempSections.GetLength(0);
                int columns = tempSections.GetLength(1);
                for (int i = 0; i < rows; i++)
                {
                    for (int j = 0; j < columns; j++)
                    {
                        var temp = tempSections[i, j];
                        var s = temp.Section;
                        if (!temp.Triangles.Any())
                            continue;

                        var bd = s.Boundary;
                        pl.ReplacePoints(
                                new Point2d(bd.X, bd.Y),
                                new Point2d(bd.X + bd.Width, bd.Y),
                                new Point2d(bd.X + bd.Width, bd.Y - bd.Height),
                                new Point2d(bd.X, bd.Y - bd.Height)
                            );

                        foreach (var t in temp.Triangles)
                        {
                            bool isInside = false;
                            // Enough to check 1 point. If it is inside, triangle is completely
                            // inside (which could be checked with remaining 2 points) or partially
                            // inside - both cases are good for us.
                            if (s.IsInside(t.P1))
                                isInside = true;
                            else
                            {
                                // Triangle could be completely outside or cutting section line.
                                intersectionPoints.Clear();
                                tPl.ReplacePoints(t.GetPoints().Select(x => x.ToCADPoint().To2d()));

                                pl.IntersectWith(tPl, Intersect.OnBothOperands, intersectionPoints, IntPtr.Zero, IntPtr.Zero);

                                if (intersectionPoints.Count > 0)
                                    isInside = true;
                                else
                                {
                                    // Check if section is completely inside of triangle.
                                    // This is done by checking any point of section.
                                    bool b = t.IsPointInside(my.RectangleExtensions.GetBottomLeft(s.Boundary).GetAsPoint3d());
                                    if (b)
                                        isInside = true;
                                }
                            }
                            if (isInside)
                                s.Triangles.Add(t);
                        }
                    }
                }
                pl.Erase();
                tPl.Erase();
            }

        }
    }
}
