﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SharedUtilities.Geometry;


namespace CadPlugin.KHNeu.Models.DTMs
{
    public class Triangle
    {
        private static int id = 0;
        public int Id { get; } = Interlocked.Increment(ref id);

        public Point3d P1 { get; private set; }
        public Point3d P2 { get; private set; }
        public Point3d P3 { get; private set; }

        public double MaxX { get; private set; }
        public double MinX { get; private set; }
        public double MaxY { get; private set; }
        public double MinY { get; private set; }

        public Triangle() 
            :this(new Point3d(), new Point3d(), new Point3d())
        { }
        public Triangle(Point3d p1, Point3d p2, Point3d p3)
        {
            P1 = p1;
            P2 = p2;
            P3 = p3;
        }

        public static void ResetId() => id = 0;

        public IEnumerable<Point3d> GetPoints()
            => new Point3d[] { P1, P2, P3 };

        public void SetPoints(Point3d p1, Point3d p2, Point3d p3)
        {
            P1 = p1;
            P2 = p2;
            P3 = p3;
            MaxX = MinX = p1.X;
            MaxY = MinY = p1.Y;
            if (p2.X > MaxX)
                MaxX = p2.X;
            if (p2.X < MinX)
                MinX = p2.X;
            if (p2.Y > MaxY)
                MaxY = p2.Y;
            if (p2.Y < MinY)
                MinY = p2.Y;

            if (p3.X > MaxX)
                MaxX = p3.X;
            if (p3.X < MinX)
                MinX = p3.X;
            if (p3.Y > MaxY)
                MaxY = p3.Y;
            if (p3.Y < MinY)
                MinY = p3.Y;
        }

        public void SetPoints(IEnumerable<Point3d> pts)
        {
            P1 = pts.ElementAt(0);
            P2 = pts.ElementAt(1);
            P3 = pts.ElementAt(2);
        }

        /// <summary>
        /// Returns height of point, on triangle plane.
        /// </summary>
        public double GetHeight(Point3d p)
        {
            return MyUtilities.Helpers.Calculations.PointHeightInPlane(P1, P2, P3, new Point2d(p.X, p.Y));
        }

        public Point3d GetCenterOfGravity()
        {
            var pt = TriangleCalculations.GetCenterOfGravity(
                P1.ToCADPoint(), P2.ToCADPoint(), P3.ToCADPoint()
                );
            return pt.FromCADPoint();
        }

        /// <summary>
        /// Checks if point is inside triangle (in 2d).
        /// </summary>
        public bool IsPointInside(Point3d p)
        {
            bool b = TriangleCalculations.IsPointInside(
                P1.ToCADPoint(), P2.ToCADPoint(), P3.ToCADPoint(),
                p.ToCADPoint());
            return b;
        }
        
    }
}
