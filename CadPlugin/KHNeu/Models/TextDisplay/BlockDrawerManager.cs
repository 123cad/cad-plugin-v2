﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SewageCommands.KBlock;
using CadPlugin.KHNeu.Models.Datas;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.KHNeu.Models.TextDisplay
{
    class BlockDrawerManager
    {
        public static void DrawFinalBlocks(Document doc, DataHistory mgr)
        {
            if (!Settings.Instance.EndDrawBlocks)
                return;

            var st = Settings.Instance;
            var data = new BlockCreationData();
            data.Name = new BlockNameData()
            {
                StartIndex = 1,// st.NameStartIndex,
                Prefix = "SH"// st.NamePrefix
            };
            var list = mgr.Elements.ToList();
            for (int i = 0; i < list.Count; i+=2)
            {
                var shaft = (ShaftData)list[i];
                var points = new ShaftPoints()
                {
                    SMP = shaft.GetSMP().ToCADPoint(),
                    DMP = shaft.GetDMP().ToCADPoint(),
                    ShaftDiameter = shaft.Diameter
                };
                points.PipeIns.Add(new ShaftPipeIn());
                if (shaft.IsDrawn)
                {
                    points.CreateBlock_SMP = true;
                    points.CreateBlock_SMP2 = true;
                    points.CreateBlock_DMP = true;
                    points.PipeIns[0].CreateBlock = true;
                    //points.CreateBlock_RAPin = true;
                    points.CreateBlock_RAPout = true;
                }
                else
                {
                    points.CreateBlock_SMP = false;
                    points.CreateBlock_SMP2 = false;
                    points.CreateBlock_DMP = false;
                    points.PipeIns[0].CreateBlock = true;
                    //points.CreateBlock_RAPin = true;
                    points.CreateBlock_RAPout = true;
                }
                // Downstream - point 1 out, point 2 in
                // Upstream - point 1 in, point 2 out
                if (i > 0)
                {   
                    var pipe = (PipeData)list[i - 1];
                    if (mgr.WaterDirection == WaterDirection.Downstream)
                    {

                        points.PipeIns[0].Point = pipe.Point2.ToCADPoint();
                        points.PipeIns[0].Diameter = pipe.Diameter;
                        //points.RAPin = pipe.Point2.ToCADPoint();
                        //points.RAPInDiameter = pipe.Diameter;
                    }
                    else // Upstream
                    { 
                        points.RAPout = pipe.Point2.ToCADPoint();
                        points.RAPOutDiameter = pipe.Diameter;
                    }
                }

                if (i + 1 < list.Count)
                {
                    var pipe = (PipeData) list[i + 1];
                    if (mgr.WaterDirection == WaterDirection.Downstream)
                    {
                        points.RAPout = pipe.Point1.ToCADPoint();
                        points.RAPOutDiameter = pipe.Diameter;
                    }
                    else // Upstream
                    {
                        points.PipeIns[0].Point = pipe.Point1.ToCADPoint();
                        points.PipeIns[0].Diameter = pipe.Diameter;
                        //points.RAPin = pipe.Point1.ToCADPoint();
                        //points.RAPInDiameter = pipe.Diameter;
                    }
                }

                data.Shafts.Add(points);
            }

            // Naming always goes from lower indexes to higher ones.
            // If it is upstream mode, we want that start shaft to have first index.
            if (mgr.WaterDirection == WaterDirection.Upstream)
                data.Shafts.Reverse();

            KBlockLayerNames layers = new KBlockLayerNames()
            {
	            SMP = Layers.ShaftSMPLayer,
	            SMP2 = Layers.ShaftSMPLayer,
				DMP = Layers.ShaftDMPLayer,
				RAP = Layers.ShaftPipeInLayer
            };
            CommandKBlock.DrawBlocks(doc, data, layers);
        }


    }
}
