﻿using CadPlugin.KHNeu.Views.KHNeuModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.KHNeu.Models.TextDisplay
{
    class TextDrawerManager : IDisposable
    {
        private Document doc;
        private KHNeuModel model;
        private DataHistory mgr;

        private List<List<ObjectId>> itemTexts { get; } = new List<List<ObjectId>>();
        private List<ObjectId> shaft1Entities;
        private List<ObjectId> shaft2Entities;
        private List<ObjectId> pipeEntities;

        public TextDrawerManager(Document doc, KHNeuModel model, DataHistory data)
        {
            this.doc = doc;
            this.model = model;
            this.mgr = data;
        }

        public void DrawCurrent()
        {
            // Delete shaft1 if exists.
            using (TransactionWrapper tw = doc.StartTransaction(true))
            {
                deleteCurrent();

                // Draw shaft1.
                var sourceData = new TextDisplaySourceData()
                {
                    Shaft2Data = mgr.Shaft1,
                    Shaft2Model = model.Shaft1
                };
                shaft1Entities = TextDrawer.DrawShaftText(tw, sourceData).Select(x => x.Id).ToList();

                // Draw pipe.
                sourceData = new TextDisplaySourceData()
                {
                    CurrentPipe = model.Pipe,
                    PipeData = mgr.Pipe,
                    PreviousPipe = mgr.GetLastCreatedPipe(),
                    Shaft1Data = mgr.Shaft1,
                    Shaft2Data = mgr.Shaft2,
                    Shaft2Model = model.Shaft2
                };
                pipeEntities = TextDrawer.DrawPipeText(tw, sourceData).Select(x => x.Id).ToList();

                // Draw shaft2.
                shaft2Entities = TextDrawer.DrawShaftText(tw, sourceData).Select(x => x.Id).ToList();
            }
        }

        public void MoveNext()
        {
            // Delete current shaft1
            itemTexts.Add(shaft1Entities);
            itemTexts.Add(pipeEntities);
            shaft1Entities = null;
            pipeEntities = null;
            shaft1Entities = shaft2Entities;
            shaft2Entities = null;

            // Draw current shaft2 as shaft1
        }

        /// <summary>
        /// Deletes all non applied texts.
        /// </summary>
        private void deleteCurrent()
        {
            using (TransactionWrapper tw = doc.StartTransaction(true))
            {

                Action<IEnumerable<ObjectId>> delete =
                    list =>
                    {
                        if (list == null)
                            return;
                        foreach (var oid in list)
                            tw.GetObjectWrite<Entity>(oid).Erase();
                    };
                delete(shaft1Entities);
                delete(shaft2Entities);
                delete(pipeEntities);
                shaft1Entities = null;
                shaft2Entities = null;
                pipeEntities = null;

            }
        }

        /// <summary>
        /// Deletes all created entities.
        /// </summary>
        public void Cancel()
        {
            deleteCurrent();

            using (TransactionWrapper tw = doc.StartTransaction(true))
            {
                foreach (var v in itemTexts)
                    foreach (var oid in v)
                        tw.GetObjectWrite<Entity>(oid).Erase();
            }
        }

        public void Dispose()
        {

        }
    }
}
