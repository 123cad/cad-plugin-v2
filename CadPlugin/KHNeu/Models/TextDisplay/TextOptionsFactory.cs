﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using pipes = CadPlugin.KHNeu.Models.TextDisplay.PipeTexts;
using shafts = CadPlugin.KHNeu.Models.TextDisplay.ShaftsTexts;

namespace CadPlugin.KHNeu.Models.TextDisplay
{
    class TextOptionsFactory
    {
		private Dictionary<TextOptions, Type> allPipes = new Dictionary<TextOptions, Type>();
		private Dictionary<TextOptions, Type> allShafts = new Dictionary<TextOptions, Type>();
		private static TextOptionsFactory __Instance = null;

        private static List<Type> pipeClasses = new List<Type>();
        private static List<Type> shaftClasses = new List<Type>();
		public static TextOptionsFactory Instance
		{
			get
			{
				if (__Instance == null)
					__Instance = new TextOptionsFactory();
				return __Instance;
			}
		}

        static TextOptionsFactory()
        {
			// Needed this, because Assembly.GetTypes throws exception
			// about loading some assemblies.
			// Best guess is that GetTypes is loading ALL types in assembly, 
			// and it crashes somewhere.
            pipeClasses.Add(typeof(pipes.CustomText));
            pipeClasses.Add(typeof(pipes.DMPLength));
            pipeClasses.Add(typeof(pipes.PipeIn));
            pipeClasses.Add(typeof(pipes.PipeLength2d));
            pipeClasses.Add(typeof(pipes.PipeLength3d));
            pipeClasses.Add(typeof(pipes.PipeOut));
            pipeClasses.Add(typeof(pipes.PipeSlope));
            pipeClasses.Add(typeof(pipes.SegmentsCount));

            shaftClasses.Add(typeof(shafts.CustomText));
            shaftClasses.Add(typeof(shafts.ShaftCover));
            shaftClasses.Add(typeof(shafts.ShaftDepth));
            shaftClasses.Add(typeof(shafts.ShaftPipeAngle));
            shaftClasses.Add(typeof(shafts.ShaftPipeOut));
        }
		private TextOptionsFactory()
        {
			initialize();
        }
		private void initialize()
		{
			foreach (Type t in pipeClasses)
			{
				var p = (TextOptions)Activator.CreateInstance(t);
				allPipes.Add(p, t);
			}
			foreach (Type t in shaftClasses)
			{
				var s = (TextOptions)Activator.CreateInstance(t);
				allShafts.Add(s, t);
			}
		}
		public IEnumerable<string> GetAllPipeDescriptions()
		{
			return allPipes.Select(x=>x.Key.Description).ToList();
		}
		public IEnumerable<string> GetAllShaftDescriptions()
		{
			return allShafts.Select(x => x.Key.Description).ToList();
		}
		public TextOptions CreatePipeOptions(string uniqueName)
        {
			if (!allPipes.Any(x => x.Key.Description == uniqueName))
				return null;
            var t = allPipes.First(x => x.Key.Description == uniqueName).Value;
			return (TextOptions)Activator.CreateInstance(t);
		}
		public TextOptions CreateShaftOptions(string uniqueName)
		{
            if (!allShafts.Any(x => x.Key.Description == uniqueName))
                return null;
            var t = allShafts.First(x => x.Key.Description == uniqueName).Value;
            return (TextOptions)Activator.CreateInstance(t);
		}
	}
}
