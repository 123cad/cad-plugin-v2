﻿using CadPlugin.KHNeu.Models.Datas;
using CadPlugin.KHNeu.Views.UserControls.PipeModels;
using CadPlugin.KHNeu.Views.UserControls.ShaftModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.TextDisplay
{
    /// <summary>
    /// Data needed for text classes (to generate displayed text).
    /// </summary>
    public class TextDisplaySourceData
    {
        // Some data is stored in model class, some in data.
        // Have it both, just in case.

        public ShaftModel Shaft2Model { get; set; }
        public PipeModel CurrentPipe { get; set; }

        public ShaftData Shaft1Data { get; set; }
        public ShaftData Shaft2Data { get; set; }
        public PipeData PipeData { get; set; }
        public PipeData PreviousPipe { get; set; }

    }
}
