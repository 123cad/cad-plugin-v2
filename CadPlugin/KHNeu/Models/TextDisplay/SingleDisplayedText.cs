﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.TextDisplay
{
	public enum TextDisplayType
	{
		Pipe,
		Shaft
	}
	public enum DisplayScale
	{
		Percent = 0,
		Permille = 1
	}

	public abstract class TextOptions
    {
		public string TypeName { get { return GetType().Name; } }

		public string Description { get; set; }
		public virtual bool HasDigits => false;
		public int Digits { get; set; } = 2;
        public string DefaultPrefix { get; set; } = "";

		public virtual bool HasScale => false;
		public DisplayScale Scale { get; set; } = DisplayScale.Percent;

		public virtual bool TextExists(TextDisplaySourceData data) => true;
		public abstract string GetValue(TextDisplaySourceData data);

		public void AssignFrom(TextOptions t)
        {
			Digits = t.Digits;
			Scale = t.Scale;
        }
    }
	public class SingleDisplayedText : Notify
	{
		/// <summary>
		/// Unique name of text object.
		/// </summary>
		public string Name
		{
			get => Options.TypeName;
			set
			{
				Update(value);
				OnPropertyChanged();
			}
		}

        /// <summary>
        /// Prefix text which appears before value.
        /// </summary>
        public string Description
        {
            get => Options.Description;
            set
            {
                Update(value);
                OnPropertyChanged();
            }
        }

        public string Prefix{ get => _prefix; set => OnPropertyChanged(() => _prefix = value); }
		/// <summary>
        /// Text drawn at the end.
		/// </summary>
		public string Suffix { get => _suffix; set => OnPropertyChanged(() => _suffix = value); }
		public double TextSize { get => _textSize; set => OnPropertyChanged(() => _textSize = value); }
		public Color TextColor { get => _textColor; set => OnPropertyChanged(() => _textColor = value); }
		/// <summary>
		/// Is text is displayed while jigging.
		/// </summary>
		public bool DrawInJig { get => _drawJig; set => OnPropertyChanged(() => _drawJig = value); }
		/// <summary>
		/// Is drawn as final text (when users moves to next shaft, this data is drawn).
		/// </summary>
		public bool DrawAsFinal { get => _drawFinal; set => OnPropertyChanged(() => _drawFinal = value); }

		public TextDisplayType TextType { get; }
		public TextOptions Options { get => _options; set => OnPropertyChanged(() => _options = value); }

		private string _suffix;
		private double _textSize;
		private Color _textColor;
		private bool _drawJig;
		private bool _drawFinal;
		private TextOptions _options;
        private string _prefix;

        public SingleDisplayedText(TextDisplayType type, TextOptions options)
		{
			TextType = type;
			TextSize = 0.3;
			TextColor = Settings.DefaultTextColor;
			DrawInJig = true;
			DrawAsFinal = true;
			Options = options;
            Prefix = options.DefaultPrefix;
			if (options == null)
				throw new ArgumentNullException();
		}
		public void AssignFrom(SingleDisplayedText d)
        {
            Prefix = d.Prefix;
			Suffix = d.Suffix;
			TextSize = d.TextSize;
			DrawInJig = d.DrawInJig;
			DrawAsFinal = d.DrawAsFinal;
			Options = d.Options;
        }

		public void Update(string typeName)
        {
			TextOptions o = null;
            switch (TextType)
            {
                case TextDisplayType.Pipe:
					o = TextOptionsFactory.Instance.CreatePipeOptions(typeName);
                    break;
                case TextDisplayType.Shaft:
					o = TextOptionsFactory.Instance.CreateShaftOptions(typeName);
                    break;
            }
			if (o == null)
				throw new ArgumentException($"TypeName ({typeName}) is not TextOption!");
			o.AssignFrom(Options);
			Options = o;
            Prefix = o.DefaultPrefix;
			OnPropertyChanged(nameof(Name));
			OnPropertyChanged(nameof(Description));
            OnPropertyChanged(nameof(Prefix));
        }

		/// <summary>
		/// Indicates if text exists for current state.
		/// </summary>
		public bool TextExists(TextDisplaySourceData d)
			=> Options.TextExists(d);
		public string GetValue(TextDisplaySourceData d)
			=> Options.GetValue(d);

		/// <summary>
		/// Returns displayed text with Prefix and Suffix.
		/// </summary>
		public string GetText(TextDisplaySourceData data)
		{
			if (!TextExists(data))
				return "";
			string s = GetValue(data);
			return Prefix + " " + s + Suffix;
		}
	}

}
