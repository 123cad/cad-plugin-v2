﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.KHNeu.Models.Datas;
using CadPlugin.KHNeu.Views.UserControls.ShaftModels;
using my = MyUtilities.Geometry;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.KHNeu.Models.TextDisplay
{
	class TextDrawer
	{

		/// <summary>
		/// Draws final text (for pipe).
		/// </summary>
		internal static List<Entity> DrawPipeText(TransactionWrapper tw, TextDisplaySourceData data)
		{
			var db = tw.Database;
			Common.CADLayerHelper.AddLayerIfDoesntExist(db, Layers.ShaftLabelLayer);
			Common.CADLayerHelper.AddLayerIfDoesntExist(db, Layers.PipeLabelLayer);
			List<Entity> entities = new List<Entity>();

			//DisplaySelectedData selectedData = DisplaySelectedData.CreateWithEndShaft(mgr);

			//CreateShaftLabels(tr, modelSpace, selectedData, mgr.Data.EndPoint.ToCADPoint(), display, entities);

			CreatePipeLabels(tw, data, entities);
			return entities;
		}
		/// <summary>
		/// Draws data for shaft.
		/// </summary>
		internal static List<Entity> DrawShaftText(TransactionWrapper tw, TextDisplaySourceData data)
		{
			var db = tw.Database;
			Common.CADLayerHelper.AddLayerIfDoesntExist(db, Layers.ShaftLabelLayer);
			List<Entity> entities = new List<Entity>();

			if (!data.Shaft2Data.IsDrawn)
				return entities;

			Point3d p = data.Shaft2Data.Center.GetWithHeight(data.Shaft2Data.HeightDMP).ToCADPoint();
			p = p.Add(new Vector3d(data.Shaft2Data.Radius * 1.5, 0, 0));
			CreateShaftLabels(tw, data, p, entities);

			return entities;
		}
		private static void CreateShaftLabels(TransactionWrapper tw, TextDisplaySourceData data, Point3d center, List<Entity> entities)
		{
			var settings = Settings.Instance;
			Point3d start = center;
			double currentDistance = data.Shaft2Data.Radius;// (data.Shaft2Data.IsDrawn ? -displayData.ShaftData.Radius : -1.0);
			foreach (SingleDisplayedText single in settings.ShaftTexts.Where(x=>x.DrawAsFinal))
			{
				if (!single.TextExists(data))
					continue;
				if (!single.DrawAsFinal)
					continue;
				string text = single.GetText(data);
				MText mText = tw.CreateEntity<MText>(Layers.ShaftLabelLayer);
				mText.Layer = Layers.ShaftLabelLayer;

				mText.Color = Color.FromColor(single.TextColor);
				mText.TextHeight = single.TextSize;
				mText.Location = start.Add(new Vector3d(0, currentDistance, 0));
				mText.Contents = text;
				currentDistance -= single.TextSize * 1.5;

				entities.Add(mText);
			}
		}

		private static void CreatePipeLabels(TransactionWrapper tw, TextDisplaySourceData data, List<Entity> entities)
		{
			// Draw pipes labels and pipe arrow direction.

			// First calculate single line. If it doesn't fit, split to multiple lines.
			// Every single line has its own max text height, which determines height for that line.
			// MText has reference point on the upper left side. Everything is calculated from (0,0) point, 
			// moves first line of text above pipe, and then aligned with the pipe (one line is above the pipe).

			var settings = Settings.Instance;

			var pipeTexts = settings.PipeTexts.Where(x => x.DrawAsFinal).ToList();
			var pipeData = data.PipeData;

			// Text object, text drawing object, position.
			Tuple<SingleDisplayedText, MText>[] texts = new Tuple<SingleDisplayedText, MText>[pipeTexts.Count];
			my.Vector3d pipeVector = pipeData.Point1.GetVectorTo(pipeData.Point2);
            double slope = MyUtilities.Helpers.Calculations.GetSlope(pipeVector);
			// Calculations are done against downstream. This is to adjust arrow.
            bool isUpstream = slope < 0;
			double length2d = new Vector2d(pipeVector.X, pipeVector.Y).Length;
			// Allowed length of the pipe to contain text.
			double allowedLength2d = 0.75 * length2d;
			double currentXdistance = 0;
			double currentLineHeight = 0;
			double firstLineHeight = 0;
			double firstLineWidth = 0;

			SingleDisplayedText pipeOutDisplay = null;
			SingleDisplayedText pipeInDisplay = null;

			int createdTextCount = 0;
			// Calculate single line
			for (int i = 0; i < pipeTexts.Count; i++)
			{
				SingleDisplayedText single = pipeTexts[i];
				if (!single.TextExists(data))
					continue;
				if (single.Options is PipeTexts.PipeOut)
				{
					pipeOutDisplay = single;
					continue;
				}
				if (single.Options is PipeTexts.PipeIn)
				{
					pipeInDisplay = single;
					continue;
				}
				if (!single.DrawAsFinal)
					continue;
				var text = tw.CreateEntity<MText>(Layers.PipeLabelLayer);
				text.Layer = Layers.PipeLabelLayer;
				text.Color = Color.FromColor(single.TextColor);
				text.TextHeight = single.TextSize;
				text.Contents = single.GetText(data);
				text.Location = new Point3d(currentXdistance, 0, 0);
				entities.Add(text);
				createdTextCount++;
				texts[i] = new Tuple<SingleDisplayedText, MText>(single, text);

				if (single.TextSize > currentLineHeight)
					currentLineHeight = single.TextSize;
				currentXdistance += text.GeometricExtents.MaxPoint.X - text.GeometricExtents.MinPoint.X + single.TextSize;
			}
			if (createdTextCount == 0)
				return;
			firstLineHeight = currentLineHeight;
			firstLineWidth = currentXdistance;
			List<List<MText>> lines = new List<List<MText>>();
			// If single line does not fit, divide it to multiple lines.
			if (currentXdistance > allowedLength2d)
			{
				// Split into multiple lines.
				currentXdistance = 0;
				currentLineHeight = 0;
				double currentLineY = 0;
				// Lines of texts.
				List<MText> currentLineTexts = new List<MText>();
				for (int i = 0; i < texts.Length; i++)
				{
					var t = texts[i];
					//NOTE Test this! Same reason like lines.Add(texts.Where(x => x != null).Select(x => (MText)x.Item2).ToList());
					if (t == null)
						continue;
					MText text = t.Item2;
					SingleDisplayedText single = t.Item1;
					double currentTextWidth = text.GeometricExtents.MaxPoint.X - text.GeometricExtents.MinPoint.X;
					if (currentLineTexts.Count > 0 && currentXdistance + currentTextWidth > allowedLength2d)
					{
						if (lines.Count == 0)
						{
							firstLineHeight = currentLineHeight;
							firstLineWidth = currentXdistance;
						}
						// Move to the new line.
						currentLineY -= currentLineHeight * 1.3;// Add some space between lines.
						currentXdistance = 0;
						currentLineHeight = 0;
						lines.Add(currentLineTexts);
						currentLineTexts = new List<MText>();
					}
					currentLineTexts.Add(text);
					text.Location = new Point3d(currentXdistance, currentLineY, 0);
					currentXdistance += currentTextWidth + single.TextSize;
					if (single.TextSize > currentLineHeight)
						currentLineHeight = single.TextSize;
				}
				if (currentLineTexts.Count > 0)
					lines.Add(currentLineTexts);
			}
			else
			// Needed where because we have PipeIn and PipeOut which are not put into regular array.
			{
				lines.Add(texts.Where(x => x != null).Select(x => (MText)x.Item2).ToList());
			} 

			{
				// First line can have different text heights. Since text reference point is upper left,
				// all texts will be aligned to the top line.
				// So here align them to the bottom line.
				if (lines.Count > 0 && lines[0].Count > 0)
				{
					foreach (MText text in lines[0])
					{

						Matrix3d adjust = Matrix3d.Displacement(new Vector3d(0, -(firstLineHeight - text.TextHeight), 0));
						text.TransformBy(adjust);
					}
				}
			}

			// All texts are positioned now (relative to (0,0,0) point) with angle 0.
			// Move first text line above pipe, rotate accordingly, and set new point (start point of pipe).
			double angleDeg = new my.Vector3d(1, 0, 0).GetAngleDegXY(pipeVector);
			// Text shouldn't be inverted.
			bool invertText = angleDeg < -90.0 || 90.0 < angleDeg;
			double textPositionX = length2d / 2 - firstLineWidth / 2;
			if (invertText)
			{
				// Keep angle between -90 and 90.
				angleDeg = angleDeg > 0 ? angleDeg - 180 : angleDeg + 180;
				textPositionX = -length2d / 2 - firstLineWidth / 2;
			}

			// Distance of text from pipe (dependent on diameter). 
			double distance = pipeData.Radius + 0.1 * firstLineHeight;
			CADMTextHelper.MTextParameters pars = new CADMTextHelper.MTextParameters()
			{
				TextDelta = distance,
				RotateToKeepHorizont = true,
				RotationType = CADMTextHelper.HorizontRotation.TextMiddlePointOnLine,
				TextAlignment = CADMTextHelper.TextAlignment.BottomMiddle
			};

			Matrix3d firstLinePosition = Matrix3d.Displacement(new Vector3d(textPositionX, firstLineHeight + distance, 0));
			firstLinePosition = firstLinePosition.PreMultiplyBy(Matrix3d.Rotation(MyUtilities.Helpers.Calculations.DegreesToRadians(angleDeg), Vector3d.ZAxis, new Point3d()));
			firstLinePosition = firstLinePosition.PreMultiplyBy(Matrix3d.Displacement(pipeVector.ToCADVector()));

			Matrix3d nonFirstLinePosition = Matrix3d.Displacement(new Vector3d(textPositionX, firstLineHeight * 1.3 - distance, 0));
			nonFirstLinePosition = nonFirstLinePosition.PreMultiplyBy(Matrix3d.Rotation(MyUtilities.Helpers.Calculations.DegreesToRadians(angleDeg), Vector3d.ZAxis, new Point3d()));
			nonFirstLinePosition = nonFirstLinePosition.PreMultiplyBy(Matrix3d.Displacement(pipeVector.ToCADVector()));

			// First line goes above the pipe, all others below.
			bool firstline = true;
			pars.TextAlignment = CADMTextHelper.TextAlignment.BottomMiddle;
			foreach (var l in lines)
			{
				my.Vector3d angle = pipeVector;
				my.Point3d middle = pipeData.Point1.Add(angle.GetUnitVector().Multiply(angle.Length / 2.0));
				double distanceY = pars.TextDelta;

				l.AlignWithVector(middle.GetAs2d(), angle.GetAs2d(), pars);

				pars.TextAlignment = CADMTextHelper.TextAlignment.TopMiddle;
				double height = l.Max(x =>
				{
					double h = x.TextHeight;// x.GeometricExtents.MaxPoint.Y - x.GeometricExtents.MinPoint.Y;
					return h;
				});
				if (firstline)
					pars.TextDelta = distance;
				else
					pars.TextDelta += height + distance;

				/*foreach (MText t in l)
				{

					if (firstline)
					{
						pars.TextAlignment = CADMTextHelper.TextAlignment.BottomMidle;

						CADMTextHelper.AlignWithVector(t, middle.GetAs2d(), angle.GetAs2d(), pars);
						//t.TransformBy(firstLinePosition);
					}
					else
					{
						pars.TextAlignment = CADMTextHelper.TextAlignment.TopMiddle;
						CADMTextHelper.AlignWithVector(t, middle.GetAs2d(), angle.GetAs2d(), pars);
						pars.TextDelta += t.TextHeight + distance;
						//t.TransformBy(nonFirstLinePosition);
					}
				}*/
				firstline = false;
			}

			#region Draw arrow as prefix
			my.Vector3d pipeInTextDirection = pipeVector;
			if (invertText)
				pipeInTextDirection = pipeInTextDirection.Negate();
			if (settings.DrawArrowInText)
			{

				CADArrowHelper.ArrowParameters arrowPars = new CADArrowHelper.ArrowParameters(firstLineHeight * 3, firstLineHeight * 0.9, 0.5, 0.5, 0.2, 1);
				CADArrowHelper.ArrowPositionText position = CADArrowHelper.ArrowPositionText.LeftMiddle;

				arrowPars.Direction = invertText ? CADArrowHelper.ArrowDirection.Against : CADArrowHelper.ArrowDirection.Along;
				//  If upstream make opposite.
                if (isUpstream)
                    arrowPars.Direction = arrowPars.Direction == CADArrowHelper.ArrowDirection.Against
                        ? CADArrowHelper.ArrowDirection.Along
                        : CADArrowHelper.ArrowDirection.Against;
				Polyline arrowText = tw.CreateEntity<Polyline>(Layers.PipeLabelLayer);
				arrowText.Color = Color.FromColor(settings.ArrowColor);
				entities.Add(arrowText);

                var arrowDirection = pipeInTextDirection.GetAs2d();
				CADArrowHelper.CreateArrowAndAlign(arrowText, lines[0][0], arrowDirection, arrowPars, position);


				/*if (invertText)
					pipe = pipe.Negate();
				MyGeometry.Vector3d t1 = pipe.CrossProduct(new MyGeometry.Vector3d(0, 0, 1));
				t1 = t1.GetUnitVector().Multiply(firstLineHeight / 2);
				Polyline pl = new Polyline();

				arrowText.Color = Color.FromColor(AdditionalSettings.Instance.ArrowColor);
				arrowText.Layer = Create.CreatePipe.PipeLabelLayer;
				modelSpace.AppendEntityCurrentLayer(arrowText);
				tr.AddNewlyCreatedDBObject(arrowText, true);
				entities.Add(arrowText);
				double tailHeight = firstLineHeight / 5;

				List<Point2d> points = new List<Point2d>();
				Point2d p1 = lines[0][0].Location.FromCADPoint().GetAs2d().Add(t1.GetAs2d()).ToCADPoint();
				p1 = p1.Add(pipe.GetAs2d().GetUnitVector().Multiply(firstLineHeight / 4).Negate().ToCADVector());
				points.Add(p1);
				p1 = p1.Add(pipe.GetAs2d().GetUnitVector().Multiply(firstLineHeight / 1.5).Negate().ToCADVector());
				points.Add(p1);
				p1 = p1.Add(pipe.GetAs2d().GetUnitVector().Multiply(firstLineHeight / 1.5).Negate().ToCADVector());
				points.Add(p1);
				if (invertText)
					points.Reverse();

				arrowText.AddVertexAt(0, points[0], 0, 0, firstLineHeight);
				arrowText.AddVertexAt(1, points[1], 0, tailHeight, tailHeight);
				arrowText.AddVertexAt(2, points[2], 0, 0, 0);*/
			}
			#endregion


			#region Draw arrow on the pipe
			if (settings.DrawArrowOnPipe)
			{
				double arrowHeight = pipeData.Diameter * 0.8;
				double arrowTailWidth = arrowHeight * 0.2;
				double arrowLength = arrowHeight * 4;
				double arrowTailLength = arrowHeight * 2;
				double arrowHeadLength = arrowHeight * 2;

				Polyline arrow = tw.CreateEntity<Polyline>(Layers.PipeLabelLayer);
				entities.Add(arrow);
				arrow.Color = Color.FromColor(settings.ArrowColor);

				double pipeDiameter = pipeData.Diameter;
				CADArrowHelper.ArrowParameters arrowPars = new CADArrowHelper.ArrowParameters(pipeDiameter * 3, pipeDiameter * 0.8, 0.5, 0.5, 0.2, 1);
				arrowPars.Direction = invertText ? CADArrowHelper.ArrowDirection.Against : CADArrowHelper.ArrowDirection.Along;
                //  If upstream make opposite.
                if (isUpstream)
                    arrowPars.Direction = arrowPars.Direction == CADArrowHelper.ArrowDirection.Against
                        ? CADArrowHelper.ArrowDirection.Along
                        : CADArrowHelper.ArrowDirection.Against;

				my.Vector2d startArrowVector = pipeVector.GetAs2d().GetUnitVector().Multiply(pipeVector.Length / 2);
				CADArrowHelper.CreateArrow(arrow, pipeData.Point1.GetAs2d().Add(startArrowVector), pipeInTextDirection.GetAs2d(), arrowPars, CADArrowHelper.ArrowPositionPoint.Middle);


				/*
				Geometry.Vector2d startArrowVector = pipeVector.GetUnitVector().GetAs2d().Multiply(pipeVector.Length / 2 - arrowLength / 2);
				Geometry.Vector2d arrowDirectionVector = startArrowVector.GetUnitVector();
				startArrowVector = startArrowVector.Add(data.StartPoint.GetAsVector().GetAs2d());
				Geometry.Vector2d startHeadArrowVector = startArrowVector.Add(arrowDirectionVector.Multiply(arrowTailLength));
				Geometry.Vector2d endArrowVector = startArrowVector.Add(arrowDirectionVector.Multiply(arrowLength));

				arrow.AddVertexAt(0, startArrowVector.GetAsPoint().ToCADPoint(), 0, arrowTailWidth, arrowTailWidth);
				arrow.AddVertexAt(1, startHeadArrowVector.GetAsPoint().ToCADPoint(), 0, arrowHeight, 0);
				arrow.AddVertexAt(2, endArrowVector.GetAsPoint().ToCADPoint(), 0, 0, 0);*/
			}
			#endregion

			angleDeg = new my.Vector3d(1, 0, 0).GetAngleDegXY(pipeVector);
			// Special drawn at the start and end of the pipe (not in the middle with other texts).
			#region Draw pipe out and pipe in
			if (pipeOutDisplay != null && pipeOutDisplay.DrawAsFinal)
			{
				MText pipeOut = tw.CreateEntity<MText>(Layers.PipeLabelLayer);
				entities.Add(pipeOut);
				pipeOut.TextHeight = pipeOutDisplay.TextSize;
				pipeOut.Color = Color.FromColor(pipeOutDisplay.TextColor);
				pipeOut.Contents = pipeOutDisplay.GetText(data);// "Pipe out: " + DoubleHelper.ToStringInvariant(data.StartPoint.Z, 2);

				CADMTextHelper.MTextParameters rotationParameters = new CADMTextHelper.MTextParameters();
				rotationParameters.RotateToKeepHorizont = true;
				rotationParameters.RotationType = CADMTextHelper.HorizontRotation.TextCenterPoint;
				rotationParameters.TextAlignment = CADMTextHelper.TextAlignment.TopLeft;
				rotationParameters.TextDelta = pipeOut.TextHeight / 2;
				pipeOut.AlignWithVector(pipeData.Point1.GetAs2d(), pipeVector.GetAs2d(), rotationParameters);
				pipeOut.Location = new Point3d(pipeOut.Location.X, pipeOut.Location.Y, pipeData.Point1.Z);

				//pipeOut.Location = data.StartPoint.ToCADPoint();
				//pipeOut.TransformBy(Matrix3d.Rotation(Common.Calculations.DegreesToRadians(angleDeg), Vector3d.ZAxis, new Point3d()));
				//pipeOut.TransformBy(Matrix3d.Displacement(data.StartPoint.GetAsVector().ToCADVector()));
			}

			if (pipeInDisplay != null && pipeInDisplay.DrawAsFinal)
			{
				MText pipeIn = tw.CreateEntity<MText>(Layers.PipeLabelLayer);
				entities.Add(pipeIn);
				pipeIn.TextHeight = pipeInDisplay.TextSize;
				pipeIn.Color = Color.FromColor(pipeInDisplay.TextColor);
				pipeIn.Contents = pipeInDisplay.GetText(data);// "Pipe in: " + DoubleHelper.ToStringInvariant(data.EndPoint.Z, 2);
																	 //pipeOut.Location = data.StartPoint.ToCADPoint();


				CADMTextHelper.MTextParameters rotationParameters = new CADMTextHelper.MTextParameters();
				rotationParameters.RotateToKeepHorizont = true;
				rotationParameters.RotationType = CADMTextHelper.HorizontRotation.TextMiddlePointOnLine;
				rotationParameters.TextAlignment = CADMTextHelper.TextAlignment.BottomRight;
				rotationParameters.TextDelta = pipeIn.TextHeight / 2;
				pipeIn.AlignWithVector(pipeData.Point2.GetAs2d(), pipeVector.GetAs2d(), rotationParameters);
				pipeIn.Location = new Point3d(pipeIn.Location.X, pipeIn.Location.Y, pipeData.Point2.Z);

				//Extents3d ext = pipeIn.GeometricExtents;
				//double stringSize = ext.MaxPoint.X - ext.MinPoint.X;
				//pipeIn.TransformBy(Matrix3d.Displacement(new Vector3d(-stringSize, 0, 0)));
				//pipeIn.TransformBy(Matrix3d.Rotation(Common.Calculations.DegreesToRadians(angleDeg), Vector3d.ZAxis, new Point3d()));
				//pipeIn.TransformBy(Matrix3d.Displacement(data.EndPoint.GetAsVector().ToCADVector()));
			}
			#endregion
		}
	}
}
