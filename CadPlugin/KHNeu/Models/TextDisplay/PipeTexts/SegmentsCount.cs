﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.TextDisplay.PipeTexts
{
    class SegmentsCount : TextOptions
	{
		public override bool HasDigits => true;
        public SegmentsCount()
        {
            Description = "Segmente"; //"Segments";
        }
		public override string GetValue(TextDisplaySourceData data)
		{
			if (!data.CurrentPipe.UseSegments)
				return "-";
			int segments = data.PipeData.Segments.Count();
			string s = segments.ToString();
			return s;
		}
	}
	class PipeLength2d : TextOptions
	{
		public override bool HasDigits => true;
		public PipeLength2d()
		{
			Description = "Länge 2D"; //Length 2d";
            DefaultPrefix = "L=";
			Digits = 2;
		}
		public override string GetValue(TextDisplaySourceData data)
		{
			var p1 = data.PipeData.Point1;
			var p2 = data.PipeData.Point2;
			double length = p1.GetDistanceTo(p2);// new MyUtilities.Geometry.Point2d(p1.X, p1.Y).GetVectorTo(new MyUtilities.Geometry.Point2d(p2.X, p2.Y)).Length;
			string s = DoubleHelper.ToStringInvariant(length, Digits);//*DisplayFormat(length)*/ + " m";
			return s;
		}
	}
	class DMPLength : TextOptions
	{
		public override bool HasDigits => true;
		public DMPLength()
		{
			Description = "DMP Length";
            DefaultPrefix = "D=";
			Digits = 2;
		}
		public override string GetValue(TextDisplaySourceData data)
		{
			var p1 = data.Shaft1Data.Center;
            var p2 = data.Shaft2Data.Center;
			double length = p1.GetDistanceTo(p2);// new MyUtilities.Geometry.Point2d(p1.X, p1.Y).GetVectorTo(new MyUtilities.Geometry.Point2d(p2.X, p2.Y)).Length;
			string s = DoubleHelper.ToStringInvariant(length, Digits);//*DisplayFormat(length)*/ + " m";
			return s;
		}
	}
	class PipeLength3d : TextOptions
	{
		public override bool HasDigits => true;
		public PipeLength3d()
		{
			Description = "Länge 3D"; //Length 3d";
			Digits = 2;
		}

		public override string GetValue(TextDisplaySourceData data)
		{
			double length = data.PipeData.Point1.GetVectorTo(data.PipeData.Point2).Length;
			string s = DoubleHelper.ToStringInvariant(length, Digits);//* DisplayFormat(length)*/ + " m";
			return s;
		}
	}
	class PipeSlope : TextOptions
	{
		public override bool HasDigits => true;
        public override bool HasScale => true;
        public PipeSlope()
		{
			Description = "Neigung"; //Slope";
            DefaultPrefix = "N=";
			Digits = 2;
			Scale = DisplayScale.Percent;
		}

		public override string GetValue(TextDisplaySourceData data)
		{
			int scale = 0;
			string sign = "";
			switch (Scale)
			{
				case DisplayScale.Percent:
					scale = 1;
					sign = "%";
					break;
				case DisplayScale.Permille:
					scale = 10;
					sign = "‰";
					break;
			}
			double d = data.CurrentPipe.CurrentSlope * scale;
            d = Math.Abs(d);
			return DoubleHelper.ToStringInvariant(d, Digits)/*DisplayFormat(d)*/ + " " + sign;
		}
	}
	/// <summary>
	/// Drawn at the beggining of the pipe (next to shaft).
	/// </summary>
	class PipeOut : TextOptions
	{
		public override bool HasDigits => true;
		public PipeOut()
		{
			Description = "E-Höhe"; //Pipe out";
            DefaultPrefix = "S=";
			Digits = 2;
		}

		public override string GetValue(TextDisplaySourceData data)
		{
			double d = data.PipeData.Point1.Z;
			string s = DoubleHelper.ToStringInvariant(d, Digits); //DisplayFormat(d);// + "m";
			return s;
		}
	}
	/// <summary>
	/// Drawn at the end of the pipe (next to shaft).
	/// </summary>
	class PipeIn : TextOptions
	{
		public override bool HasDigits => true;
		public PipeIn()
		{
			Description = "A-Höhe"; //Pipe in";
			Digits = 2;
		}

		public override string GetValue(TextDisplaySourceData data)
		{
			double d = data.PipeData.Point2.Z;
			string s = DoubleHelper.ToStringInvariant(d, Digits); // DisplayFormat(d);// + "m";
			return s;
		}
	}
	/// <summary>
	/// Draws custom text from user.
	/// </summary>
	class CustomText : TextOptions
	{
		public CustomText()
		{
			Description = "Benutzer"; //Custom text";
		}
		public override string GetValue(TextDisplaySourceData data)
		{
			return "";
		}
	}
}
