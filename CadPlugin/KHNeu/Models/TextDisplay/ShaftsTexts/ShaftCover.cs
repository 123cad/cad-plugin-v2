﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.TextDisplay.ShaftsTexts
{
    class ShaftCover : TextOptions
    {
        public override bool HasDigits => true;

        public ShaftCover()
        {
            Description = "DMP"; //Cover";
            DefaultPrefix = "D=";
            Digits = 2;
        }

        public override bool TextExists(TextDisplaySourceData data)
        {
            return data.Shaft2Model.IsDrawn;
        }

        public override string GetValue(TextDisplaySourceData data)
        {
            double d = data.Shaft2Data.HeightDMP;
            string s = DoubleHelper.ToStringInvariant(d, Digits); //*DisplayFormat(d)*/ + " m";
            return s;
        }
    }


    class ShaftPipeOut : TextOptions
    {
        public override bool HasDigits => true;

        public ShaftPipeOut()
        {
            Description = "SMP"; //Pipe out";
            Digits = 2;
        }

        public override bool TextExists(TextDisplaySourceData data)
        {
            return data.Shaft2Data != null;
        }

        public override string GetValue(TextDisplaySourceData data)
        {
            double d = data.Shaft2Data.HeightSMP;
            string s = DoubleHelper.ToStringInvariant(d, Digits); //*DisplayFormat(d)*/ + " m";
            return s;
        }
    }
    
    class ShaftDepth : TextOptions
    {
        //public override string Name { get { return "Depth"; } }
        public override bool HasDigits => true;

        public ShaftDepth()
        {
            Description = "Tiefe"; //Depth";
            DefaultPrefix = "T=";
            Digits = 2;
        }

        public override bool TextExists(TextDisplaySourceData data)
        {
            return data.Shaft2Data != null;
        }

        public override string GetValue(TextDisplaySourceData data)
        {
            double d = data.Shaft2Data.HeightDMP - data.Shaft2Data.HeightSMP;
            string s = DoubleHelper.ToStringInvariant(d, Digits); //*DisplayFormat(d)*/ + " m";
            return s;
        }
    }

    class ShaftPipeAngle : TextOptions
    {
        //public override string Name { get { return "Angle"; } }
        public override bool HasDigits => true;

        public ShaftPipeAngle()
        {
            Description = "Winkel"; //Pipe angle";
            Digits = 2;
        }

        public override bool TextExists(TextDisplaySourceData data)
        {
            return data.PreviousPipe != null;
        }

        public override string GetValue(TextDisplaySourceData data)
        {
            string s = "";
            double angleDeg = data.CurrentPipe.CurrentAngleDeg;
            s = DoubleHelper.ToStringInvariant(angleDeg, Digits); ///*DisplayFormat(angleDeg)*/ + " °";

            return s;
        }
    }

    /// <summary>
    /// Draws custom text from user.
    /// </summary>
    class CustomText : TextOptions
    {
        public CustomText()
        {
            Description = "Benutzer"; //Custom text";
        }

        public override string GetValue(TextDisplaySourceData data)
        {
            return "";
        }
    }
}
