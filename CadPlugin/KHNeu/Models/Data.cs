﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models
{
    public abstract class Data
    {
        public double Diameter { get; set; }
        public double Radius => Diameter / 2;
    }
}
