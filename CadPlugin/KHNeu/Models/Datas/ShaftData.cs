﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.Datas
{
    public class ShaftData : Data
    {
        public Point2d Center { get; set; }
        public double HeightDMP { get; set; }
        public double HeightSMP { get; set; }
        /// <summary>
        /// Is shaft data drawn (text and others...).
        /// (in any case, shaft is always included in calculation)
        /// </summary>
        public bool IsDrawn { get; set; } = true;


        public ShaftData()
        {

        }
        public ShaftData(ShaftData d)
        {
            Center = d.Center;
            HeightDMP = d.HeightDMP;
            HeightSMP = d.HeightSMP;
            IsDrawn = d.IsDrawn;
        }

        public Point3d GetSMP()
            => Center.GetWithHeight(HeightSMP);

        public Point3d GetDMP()
            => Center.GetWithHeight(HeightDMP);
    }
}
