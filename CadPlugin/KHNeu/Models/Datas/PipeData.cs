﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.Datas
{
    public class PipeData : Data
    {
        /// <summary>
        /// Depends on water direction. For downstream,
        /// this is Start, for upstream this is End.
        /// </summary>
        public Point3d Point1 { get; set; }
        /// <summary>
        /// Depends on water direction. For downstream,
        /// this is End, for upstream this is Up.
        /// </summary>
        public Point3d Point2 { get; set; }

        public IEnumerable<SegmentData> Segments => _segments;

        private List<SegmentData> _segments = new List<SegmentData>();


        public void SetSegments(double startEndSegmentLength, double segmentLength)
        {
            double pipeLength3d = Point1.GetVectorTo(Point2).Length;
            double totalSegmentsD = (pipeLength3d - 2 * startEndSegmentLength) / segmentLength;
            double remaining = Math.Round(totalSegmentsD) - totalSegmentsD;
            //if (remaining > 0.99)
                //remaining = 0;
            if (remaining > 1e-3)
                throw new ArgumentException("Segment length is invalid (it must be integer number of segments). " +
                    $"Remaining is {remaining:0.00}");

            int totalSegments = (int)Math.Round(totalSegmentsD);
            _segments.Clear();
            _segments.Add(new SegmentData() { Length = startEndSegmentLength });
            _segments.AddRange(new int[totalSegments].Select(x => new SegmentData() { Length = segmentLength }));
            _segments.Add(new SegmentData() { Length = startEndSegmentLength });
        }
    }
}
