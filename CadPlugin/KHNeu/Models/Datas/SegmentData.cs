﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models.Datas
{
    /// <summary>
    /// Data for single segment of the pipe.
    /// </summary>
    public class SegmentData
    {
        public double Length { get; set; }
    }
}
