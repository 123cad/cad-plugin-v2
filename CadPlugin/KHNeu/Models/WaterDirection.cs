﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models
{
    public enum WaterDirection
    {
        /// <summary>
        /// Calculations are performed for Shaft1 
        /// being Start, and pipe going downstream
        /// to Shaft2.
        /// </summary>
        Downstream,
        /// <summary>
        /// Calculations are performed for Shaft2 
        /// being Start, and pipe going upstream
        /// to Shaft1.
        /// </summary>
        Upstream
    }
}
