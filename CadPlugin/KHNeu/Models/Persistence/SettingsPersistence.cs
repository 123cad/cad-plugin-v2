﻿using System;
using System.Collections.Generic;
using CadPlugin.KHNeu.Models.TextDisplay;
using CadPlugin.KHNeu.Models.TextDisplay.PipeTexts;
using Newtonsoft.Json;

namespace CadPlugin.KHNeu.Models.Persistence
{

    [Serializable]
    public class LabelPersistence
    {
        public string LabelName { get; set; }
        public string DescriptionText { get; set; }
        public string PrefixText { get; set; }
        public string SuffixText { get; set; }
        public ColorPersistence Color { get; set; }
        public double TextSize { get; set; }
        public bool DrawJig { get; set; }
        public bool DrawFinal { get; set; }
        public int Digits { get; set; }
        public int Scale { get; set; }
        public LabelPersistence()
        {
            LabelName = "name";
            DescriptionText = "";
            Color = new ColorPersistence(System.Drawing.Color.Black);
            DrawJig = true;
            DrawFinal = true;
            TextSize = 0.3;
            Digits = 2;
            Scale = (int)DisplayScale.Percent;
        }
    }
    /// <summary>
    /// Saves/Loads data.
    /// </summary>
    class SettingsPersistence
    {
        private static Properties.KHNEU props => Properties.KHNEU.Default;

        public static void LoadSettings(Settings s)
        {
            var p = Properties.KHNEU.Default;
            s.ArrowColor = p.PipeArrowColor?.ToColor()?? System.Drawing.Color.Black;
            s.DrawArrowOnPipe = p.DrawArrowOnPipe;
            s.DrawArrowInText = p.DrawArrowDirectionText;
            s.ShowAngle = p.PipeAngleShow;
            s.DrawDMPLine = p.DrawDMPLine;
            s.DrawSMPLine = p.DrawSMPLine;
            s.DrawSMPLine2d = p.DrawSMPLine2d;
            s.DrawRAPSMPRAPLine = p.DrawRAPSMPRAPLine;

            // Load text
            LoadLabels(s);
        }
        public static void SaveSettings(Settings s)
        {

            var p = Properties.KHNEU.Default;
            p.PipeArrowColor = new ColorPersistence(s.ArrowColor);
            p.DrawArrowOnPipe = s.DrawArrowOnPipe;
            p.DrawArrowDirectionText = s.DrawArrowInText;
            p.PipeAngleShow = s.ShowAngle;
            p.DrawDMPLine = s.DrawDMPLine;
            p.DrawSMPLine = s.DrawSMPLine;
            p.DrawSMPLine2d = s.DrawSMPLine2d;
            p.DrawRAPSMPRAPLine = s.DrawRAPSMPRAPLine;

            // Save text
            SaveLabels(s);

            p.Save();
        }

        private static void SaveLabels(Settings settings)
        {
            var props = Properties.KHNEU.Default;

            List<LabelPersistence> pipeLabels = new List<LabelPersistence>();
            List<LabelPersistence> shaftLabels = new List<LabelPersistence>();
            foreach (var pipe in settings.PipeTexts)
            { 
                LabelPersistence l = new LabelPersistence()
                {
                    Color = new ColorPersistence(pipe.TextColor),
                    LabelName = pipe.Name,
                    DescriptionText = pipe.Description,
                    TextSize = pipe.TextSize,
                    DrawJig = pipe.DrawInJig,
                    DrawFinal = pipe.DrawAsFinal,
                    PrefixText = pipe.Prefix,
                    SuffixText = pipe.Suffix
                };
                if (pipe.Options.HasDigits)
                    l.Digits = pipe.Options.Digits;
                if (pipe.Options.HasScale)
                    l.Scale = (int)pipe.Options.Scale;
                pipeLabels.Add(l);
            }

            var pipeLabelsJson = JsonConvert.SerializeObject(pipeLabels);
            props.PipeLabels = pipeLabelsJson;
            foreach (var shaft in settings.ShaftTexts)
            {
                LabelPersistence l = new LabelPersistence()
                {
                    Color = new ColorPersistence(shaft.TextColor),
                    LabelName = shaft.Name,
                    DescriptionText = shaft.Description,
                    TextSize = shaft.TextSize,
                    DrawJig = shaft.DrawInJig,
                    DrawFinal = shaft.DrawAsFinal,
                    PrefixText = shaft.Prefix,
                    SuffixText = shaft.Suffix
                };
                if (shaft.Options.HasDigits)
                    l.Digits = shaft.Options.Digits;
                if (shaft.Options.HasScale)
                    l.Scale = (int)(shaft.Options.Scale);
                shaftLabels.Add(l);
            }

            var shaftLabelsJson = JsonConvert.SerializeObject(shaftLabels);
            props.ShaftLabels = shaftLabelsJson;
        }


        private static void LoadLabels(Settings settings)
        {
            var props = Properties.KHNEU.Default;
            settings.PipeTexts.Clear();
            settings.ShaftTexts.Clear();
            if (!string.IsNullOrEmpty(props.PipeLabels))
            {

                try
                {
                    var pipes = JsonConvert.DeserializeObject<List<LabelPersistence>>(props.PipeLabels);
                    foreach (LabelPersistence l in pipes)
                    {
                        var opt = TextOptionsFactory.Instance.CreatePipeOptions(l.DescriptionText);
                        if (opt == null)
                            continue;
                        var text = new SingleDisplayedText(TextDisplayType.Pipe, opt)
                        {
                            Prefix = l.PrefixText,
                            Suffix = l.SuffixText,
                            TextColor = l.Color.ToColor(),
                            TextSize = l.TextSize,
                            DrawInJig = l.DrawJig,
                            DrawAsFinal = l.DrawFinal
                        };
                        if (opt.HasDigits)
                            opt.Digits = l.Digits;
                        if (opt.HasScale)
                            opt.Scale = (DisplayScale)l.Scale;

                        settings.PipeTexts.Add(text);
                    }
                }
                catch
                {
                }
            }
            if (props.ShaftLabels != null)
            {
                try
                {
                    var shafts = JsonConvert.DeserializeObject<List<LabelPersistence>>(props.ShaftLabels);
                    foreach (LabelPersistence l in shafts)
                    {
                        var opt = TextOptionsFactory.Instance.CreateShaftOptions(l.DescriptionText);
                        if (opt == null)
                            continue;
                        var text = new SingleDisplayedText(TextDisplayType.Shaft, opt)
                        {
                            Prefix = l.PrefixText,
                            Suffix = l.SuffixText,
                            TextColor = l.Color.ToColor(),
                            TextSize = l.TextSize,
                            DrawInJig = l.DrawJig,
                            DrawAsFinal = l.DrawFinal
                        };
                        settings.ShaftTexts.Add(text);
                    }
                }
                catch
                {
                    
                }
            }
        }
    }
}
