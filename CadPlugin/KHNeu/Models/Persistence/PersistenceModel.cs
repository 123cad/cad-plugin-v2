﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.KHNeu.Views.KHNeuModels;
using CadPlugin.KHNeu.Views.UserControls.PipeModels;
using CadPlugin.KHNeu.Views.UserControls.ShaftModels;

namespace CadPlugin.KHNeu.Models.Persistence
{
    class PersistenceModel
    {
        private static Properties.KHNEU props => Properties.KHNEU.Default;
        private static void LoadLastPipe(PipeModel pipe)
        {
            pipe.Diameter = props.PipeDiameter;
            pipe.SegmentLength = props.PipeSegment;
            pipe.StartEndSegmentLength = props.PipeSESegment;

            pipe.UseSegments = props.PipeUseSegment;
            pipe.UseAngleStep = props.PipeUseAngle;
            pipe.AngleStep = props.PipeAngle;
            pipe.MinSlope = props.PipeMinSlope;
            pipe.UseMinSlope = props.PipeUseMinSlope;

        }
        private static void LoadLastShaft1(ShaftModel shaft)
        {
            shaft.IsDrawn = props.Shaft1IsDrawn;
            shaft.Diameter = props.Shaft1Diameter;
            shaft.InnerSlope = props.Shaft1InnerSlope;
            shaft.Delta = props.Shaft1Delta;
            shaft.MinDelta = props.Shaft1MinDelta;
            shaft.UseMinDelta = props.Shaft1UseMinDelta;
        }
        private static void LoadLastShaft2(ShaftModel shaft)
        {
            shaft.IsDrawn = props.Shaft2IsDrawn;
            shaft.Diameter = props.Shaft2Diameter;
            shaft.InnerSlope = props.Shaft2InnerSlope;
            shaft.Delta = props.Shaft2Delta;
            shaft.MinDelta = props.Shaft2MinDelta;
            shaft.UseMinDelta = props.Shaft2UseMinDelta;
        }

        private static void SaveLastPipe(PipeModel pipe)
        {
            props.PipeDiameter = pipe.Diameter;
            props.PipeSegment = pipe.SegmentLength;
            props.PipeSESegment = pipe.StartEndSegmentLength;

            props.PipeUseSegment = pipe.UseSegments;
            props.PipeUseAngle = pipe.UseAngleStep;
            props.PipeAngle = pipe.AngleStep;
            props.PipeMinSlope = pipe.MinSlope;
            props.PipeUseMinSlope = pipe.UseMinSlope;

        }

        private static void SaveLastShaft1(ShaftModel shaft)
        {
            props.Shaft1IsDrawn = shaft.IsDrawn;
            props.Shaft1Diameter = shaft.Diameter;
            props.Shaft1InnerSlope = shaft.InnerSlope;
            props.Shaft1Delta = shaft.Delta;
            props.Shaft1MinDelta = shaft.MinDelta;
            props.Shaft1UseMinDelta = shaft.UseMinDelta;
        }

        private static void SaveLastShaft2(ShaftModel shaft)
        {
            props.Shaft2IsDrawn = shaft.IsDrawn;
            props.Shaft2Diameter = shaft.Diameter;
            props.Shaft2InnerSlope = shaft.InnerSlope;
            props.Shaft2Delta = shaft.Delta;
            props.Shaft2MinDelta = shaft.MinDelta;
            props.Shaft2UseMinDelta = shaft.UseMinDelta;
        }

        public static void LoadModel(KHNeuModel model)
        {
            model.StaticHeight = Properties.KHNEU.Default.StaticHeight;
            model.HeightType = Properties.KHNEU.Default.HeightSource;
            
            LoadLastShaft1(model.Shaft1);
            LoadLastPipe(model.Pipe);
            LoadLastShaft2(model.Shaft2);
        }

        public static void SaveModel(KHNeuModel model)
        {
            props.StaticHeight = model.StaticHeight;
            props.HeightSource = model.HeightType;
            SaveLastShaft1(model.Shaft1);
            SaveLastPipe(model.Pipe);
            SaveLastShaft2(model.Shaft2);

            props.Save();
        }
    }
}
