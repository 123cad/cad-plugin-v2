﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.KHNeu.Models.EntityDrawers
{
    class EntitiesDrawerManager
    {
        private List<ObjectId> allObjects { get; } = new List<ObjectId>();
        private DataHistory manager { get; }
        private Document doc { get; }
        public EntitiesDrawerManager(DataHistory mgr, Document doc)
        {
            manager = mgr;
            this.doc = doc;
        }

        public void DrawCurrent()
        {
            using (TransactionWrapper tw = doc.StartTransaction(true))
            {
                var entities = EntitiesDrawer.DrawCurrentEntities(tw, manager);
                allObjects.AddRange(entities.Select(x => x.Id));
            }
        }

        public void DeleteAll()
        {
            foreach (var oid in allObjects)
                oid.Erase();
        }
        
    }
}
