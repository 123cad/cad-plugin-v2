﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.KHNeu.Models.TextDisplay;
using CadPlugin.KHNeu.Models.Datas;
using my = MyUtilities.Geometry;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.KHNeu.Models.EntityDrawers
{
    /// <summary>
    /// Draw entities after user confirmed current selection.
    /// (not used in jigging).
    /// </summary>
    class EntitiesDrawer
    {
        /// <summary>
        /// Draws all data needed after jigging (not same entities like for jigging).
        /// </summary> 
        internal static List<Entity> DrawCurrentEntities(TransactionWrapper tw, DataHistory data)
        {
            var db = tw.Database;
            Common.CADLayerHelper.AddLayerIfDoesntExist(db, Layers.ShaftDMPLayer);
            //Common.CADLayerHelper.AddLayerIfDoesntExist(db, Layers.ShaftTerrainLayer);
            Common.CADLayerHelper.AddLayerIfDoesntExist(db, Layers.ShaftPipeInLayer);
            Common.CADLayerHelper.AddLayerIfDoesntExist(db, Layers.ShaftPipeOutLayer);
            Common.CADLayerHelper.AddLayerIfDoesntExist(db, Layers.ShaftSMPLayer);
            //Common.CADLayerHelper.AddLayerIfDoesntExist(db, Layers.ShaftBottomLayer);
            Common.CADLayerHelper.AddLayerIfDoesntExist(db, Layers.ShaftLabelLayer);

            Common.CADLayerHelper.AddLayerIfDoesntExist(db, Layers.Pipe3dPolyLayer);
            Common.CADLayerHelper.AddLayerIfDoesntExist(db, Layers.Pipe3dDoubleLineLayer);

            List<Entity> entities = new List<Entity>();

            var pipe = data.Pipe;
            var shaft1 = data.Shaft1;
            var shaft2 = data.Shaft2;

            // Draw start shaft only for first pipe.
            Point3d pipeStart = data.Pipe.Point1.ToCADPoint();
            Point3d pipeEnd = data.Pipe.Point2.ToCADPoint();
            if (data.WaterDirection == WaterDirection.Upstream)
            {
                var p = pipeStart;
                pipeStart = pipeEnd;
                pipeEnd = p;

            }

            Point3d startLinePoint = pipeStart;
            Point3d endLinePoint = pipeEnd;
            if (data.IsFirstPipe && data.Shaft1.IsDrawn)
            {
                //Circle pipeOutCircle = CADCircleHelper.CreateCircle(tw, 
                //	shaft1.Center.GetWithHeight(shaft1.HeightSMP).ToCADPoint(), 
                //	shaft1.Radius);
                //pipeOutCircle.Layer = Layers.ShaftSMPLayer;
                //entities.Add(pipeOutCircle);

                Point3d terrain = new Point3d(shaft1.Center.X, shaft1.Center.Y, shaft1.HeightDMP);
                DBPoint terrainPoint = tw.CreateEntity<DBPoint>(Layers.ShaftDMPLayer);
                terrainPoint.Position = terrain;
                entities.Add(terrainPoint);

                Point3d bottom = terrain.GetOnZ(shaft1.HeightSMP);
                Circle bottomCircle = CADCircleHelper.CreateCircle(tw, bottom, shaft1.Radius);
                bottomCircle.Layer = Layers.ShaftSMPLayer;
                entities.Add(bottomCircle);


            }

            if (data.Shaft1.IsDrawn)
            {
                Point3d pipeOut = startLinePoint;
                Circle pipeOutSmallCircle = CADCircleHelper.CreateCircle(tw, pipeOut, pipe.Radius * 0.95);
                pipeOutSmallCircle.Layer = Layers.ShaftPipeOutLayer;
                entities.Add(pipeOutSmallCircle);
            }


            if (shaft2.IsDrawn)
            {
                //Circle pipeInCircle = CADCircleHelper.CreateCircle(tw, 
                //	shaft2.Center.GetWithHeight(pipe.StartPoint.Z).ToCADPoint(), 
                //	shaft2.Radius);
                //pipeInCircle.Layer = Layers.ShaftPipeInLayer;
                //entities.Add(pipeInCircle);

                Point3d terrain = new Point3d(shaft2.Center.X, shaft2.Center.Y, shaft2.HeightDMP);
                DBPoint terrainPoint = tw.CreateEntity<DBPoint>(Layers.ShaftDMPLayer);
                terrainPoint.Position = terrain;
                entities.Add(terrainPoint);

                Point3d bottom = new Point3d(shaft2.Center.X, shaft2.Center.Y, shaft2.HeightSMP);
                Circle bottomCircle = CADCircleHelper.CreateCircle(tw, bottom, shaft2.Radius);
                bottomCircle.Layer = Layers.ShaftSMPLayer;
                entities.Add(bottomCircle);

                Point3d pipeIn = endLinePoint;
                Circle pipeInSmallCircle = CADCircleHelper.CreateCircle(tw, pipeIn, pipe.Radius * 0.95);
                pipeInSmallCircle.Layer = Layers.ShaftPipeInLayer;
                entities.Add(pipeInSmallCircle);

                //Point2d shaftCenter = shaft2.Center.ToCADPoint();
                //Point2d pipeIn2d = new Point2d(pipeIn.X, pipeIn.Y);
                //Vector2d coverVector = shaftCenter.GetVectorTo(pipeIn2d);
                //coverVector = coverVector.MultiplyBy(0.5);
                //Point3d coverPoint = new Point3d(shaftCenter.X, shaftCenter.Y, shaft2.HeightDMP)
                //    .Add(new Vector3d(coverVector.X, coverVector.Y, 0));
                //Circle coverCircle = CADCircleHelper.CreateCircle(tw, coverPoint, shaft2.Radius * 0.3);
                //coverCircle.Layer = Layers.ShaftDMPLayer;
                //entities.Add(coverCircle);
            }

            my.Point3d startCenter = startLinePoint.FromCADPoint();
            my.Point3d endCenter = endLinePoint.FromCADPoint();
            my.Vector3d pipeV = startCenter.GetVectorTo(endCenter);
            my.Vector3d ort = pipeV.CrossProduct(Vector3d.ZAxis.FromCADVector());
            ort = ort.GetUnitVector();
            ort = ort.Multiply(data.Pipe.Diameter / 2);
            my.Point3d rightPoint = startCenter.Add(ort);
            my.Point3d leftPoint = startCenter.Add(ort.Negate());
            Line leftPipe = tw.CreateEntity<Line>(Layers.Pipe3dDoubleLineLayer);
            leftPipe.StartPoint = leftPoint.ToCADPoint();
            leftPipe.EndPoint = leftPoint.Add(pipeV).ToCADPoint();
            Line rightPipe = tw.CreateEntity<Line>(Layers.Pipe3dDoubleLineLayer);
            rightPipe.StartPoint = rightPoint.ToCADPoint();
            rightPipe.EndPoint = rightPoint.Add(pipeV).ToCADPoint();
            entities.Add(leftPipe);
            entities.Add(rightPipe);


            return entities;
        }
    }
}
