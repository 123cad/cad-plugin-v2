﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models
{
	/// <summary>
	/// Layers used in KHNEU.
	/// </summary>
    static class Layers
    {

		public const string ShaftLabelLayer = "123_KHNeu_Schacht_Texte";
		public const string ShaftDMPLayer = "123_KHNeu_Schacht_DMP";
		//public const string ShaftTerrainLayer = "123_Schacht_GelaendeHoehe";
		public const string ShaftPipeInLayer = "123_KHNeu_Schacht_RAP";
		public const string ShaftPipeOutLayer = "123_KHNeu_Schacht_RAP";
		public const string ShaftSMPLayer = "123_KHNeu_Schacht_SMP";
		//public const string ShaftBottomLayer = "123_Schacht_BodenHoehe";

		public const string PipeArrowLayer = "123_KHNeu_Haltung_Pfeil";
		public const string PipeLabelLayer = "123_KHNeu_Haltung_Texte";
		public const string Pipe3dDoubleLineLayer = "123_KHNeu_Haltung_DN";
		public const string Pipe3dPolyLayer = "123_KHNeu_SohlLinie";
		public const string Pipe2dHelpLayer = "123_KHNeu_SohlLinie";
		public const string Pipe3dHelpLayer = "123_KHNeu_GelaendeLinie";
	}
}
