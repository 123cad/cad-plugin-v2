﻿using CadPlugin.KHNeu.Models.TextDisplay;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.KHNeu.Models
{
    public class Settings : Notify
	{
		public static Settings Instance
		{
			get
			{
				if (__Instance == null)
				{
					__Instance = new Settings();
					//Persistence.DataPersistence.LoadLabels(__Instance.TextOrder);
				}
				return __Instance;
			}
		}
		private static Settings __Instance;

		/// <summary>
		/// Depends on background color of autocad/bricsad.
		/// </summary>
		public static Color DefaultTextColor = Color.Black;
		/// <summary>
		/// Color of the arrow displaying downstream direction.
		/// </summary>
        public Color ArrowColor { get; set; } = Color.Black;
		/// <summary>
		/// Draws water direction arrow inside pipe - scaled to pipe diameter.
		/// </summary>
		public bool DrawArrowOnPipe { get; set; }
		/// <summary>
		/// Draws water direction arrow before text - scaled to text size.
		/// </summary>
        public bool DrawArrowInText { get; set; }
		/// <summary>
		/// Indicates if angle between old pipe and new one should be displayed.
		/// </summary>
        public bool ShowAngle { get; set; }
		/// <summary>
		/// Draw DMP line when finished.
		/// </summary>
        public bool DrawDMPLine{ get; set; }
		/// <summary>
		/// Draw SMP line when finished.
		/// </summary>
        public bool DrawSMPLine{ get; set; }
		/// <summary>
		/// Draw SMP line when finished, but as 2d.
		/// </summary>
        public bool DrawSMPLine2d{ get; set; }
		/// <summary>
		/// Draw RAP-SMP-RAP line when finished.
		/// </summary>
        public bool DrawRAPSMPRAPLine { get; set; }

        /// <summary>
        /// Draw finish data as MText.
        /// </summary>
        public bool EndDrawText { get; set; } = true;
		/// <summary>
		/// Draw finish data as Blocks.
		/// </summary>
		public bool EndDrawBlocks { get; set; } = true;
		
		internal ObservableCollection<SingleDisplayedText> PipeTexts { get; } = new ObservableCollection<SingleDisplayedText>();
		internal ObservableCollection<SingleDisplayedText> ShaftTexts { get; } = new ObservableCollection<SingleDisplayedText>();

		private Settings()
		{
			var tf = TextOptionsFactory.Instance;
			/*
			foreach (var v in tf.GetAllPipeNames())
				PipeTexts.Add(new SingleDisplayedText(TextDisplayType.Pipe, tf.CreatePipeOptions(v)));
			foreach (var v in tf.GetAllShaftNames())
				ShaftTexts.Add(new SingleDisplayedText(TextDisplayType.Shaft, tf.CreateShaftOptions(v)));
			*/
			// Load values from data store.
		}


	}
}
