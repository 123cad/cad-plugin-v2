﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CadPlugin
{
    public partial class LevellineChoser : Form
    {
        private static List<int> _levellines;
        private static List<int> _levelline;
        public static List<int> ChoseLevelline(List<int> levellines)
        {
            _levellines = levellines;
            LevellineChoser lc = new LevellineChoser();
            lc.ShowDialog();
            return _levelline;
        }    

        public LevellineChoser()
        {
            InitializeComponent();
            foreach (int level in _levellines)
                listBoxLevellines.Items.Add(level);
            if (listBoxLevellines.Items.Count == 1)
                listBoxLevellines.SelectedIndex = 0;

            Text = "Horizont-Kennzahl wählen";
            listBoxLevellines.SelectionMode = SelectionMode.MultiExtended;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBoxLevellines.SelectedIndex >= 0)
            {
                _levelline = new List<int>();
                foreach( var v in listBoxLevellines.SelectedItems)
                {
                    _levelline.Add((int)v);
                }
                Close();
                return;
            }
            MessageBox.Show(/*"Levelline must be selected!"*/"Horizont-Kennzahl wählen!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            _levelline = null;
            Close();
        }

      




    }
}
