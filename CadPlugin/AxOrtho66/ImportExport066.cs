﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CadPlugin.Profilers;
using MyUtilities.Geometry;

namespace CadPlugin
{
    /*public static class ImportExport066
    {
        public static string SaveFile(List<TStation> stations, ref int groundlevel, ref int crownOfTheRoad)
        {
            bool gl = false;
            bool cotr = false;
            //gde se cuva jos nije odredjeno
            StringBuilder text = new StringBuilder();
            foreach (TStation station in stations)
            {
                if (station.NumberOfGLPoints <= 0)
                    break;
                //ako se barem jednom udje u petlju, znaci postoji gl i treba ga izabrati u profiler
                gl = true;
                int n = station.NumberOfGLPoints / 4 + (station.NumberOfGLPoints % 4 == 0 ? 0 : 1);
                int stationsNum = station.NumberOfGLPoints;
                for (int i = 0; i < n; i++)
                {
                    text.Append("66     " + String.Format("{0,2}", station.GL)
                       + String.Format("{0,9}", (int)(station.Distance * 1000)) +
                       string.Format("{0,2}", i + 1) + " " +
                       string.Format("{0,7}", (int)(station.GetGLPoint(i * 4).X * 1000)) + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4).Y * 1000)));
                    if (i * 4 + 1 < stationsNum)
                        text.Append(" " + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 1).X * 1000)) + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 1).Y * 1000)));
                    if (i * 4 + 2 < stationsNum)
                        text.Append(" " + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 2).X * 1000)) + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 2).Y * 1000)));
                    if (i * 4 + 3 < stationsNum)
                        text.Append(" " + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 3).X * 1000)) + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 3).Y * 1000)));
                    text.Append("\r\n");
                }
                if (station.NumberOfCOTRPoints > 0)
                {
                    cotr = true;
                    text.Append("66     " + String.Format("{0,2}", station.COTR)
                      + String.Format("{0,9}", (int)(station.Distance * 1000)) + " 1 " +
                      string.Format("{0,7}", (int)(station.GetCOTRPoint(0).X * 1000)) + string.Format("{0,7}", (int)(station.GetCOTRPoint(0).Y * 1000)));
                    text.Append(" " + string.Format("{0,7}", (int)(station.GetCOTRPoint(1).X * 1000)) + string.Format("{0,7}", (int)(station.GetCOTRPoint(1).Y * 1000)));
                    text.Append(" " + string.Format("{0,7}", (int)(station.GetCOTRPoint(2).X * 1000)) + string.Format("{0,7}", (int)(station.GetCOTRPoint(2).Y * 1000)));
                }

                text.Append("\r\n");
            }
            if (!cotr)
                crownOfTheRoad = -1;
            if (!gl)
                groundlevel = -1;
            return text.ToString();
        }
        public static List<TStation> LoadFile(string[] content)
        {
            //dohvatiti sve i smestiti u listu stanica
            List<TStation> stations = new List<TStation>(); 
            int levelline;
            double station;
            int lineIndex;//broj linije - ako polilinija ima vise od 4 tacke, onda je vrednost >1
            double[] x = new double[4];
            double[] y = new double[4];
            List<Point2d> gl = new List<Point2d>();
            List<Point2d> cotr = new List<Point2d>();
            List<Point2d> links = new List<Point2d>();
            List<Point2d> rechts = new List<Point2d>();
            List<Point2d> other = new List<Point2d>();
            List<List<Point2d>> otherPoints = new List<List<Point2d>>();
            int lastLevelline = 0;
            double lastStation = -1;
            for (int i = 0; i < content.Length; i++)
            {
                string line = content[i];
                levelline = int.Parse(line.Substring(7, 2));
                station = Math.Round((int.Parse(line.Substring(9, 9)) / 1000.0),3);
                lineIndex = int.Parse(line.Substring(18, 2));
                x[0] = int.Parse(line.Substring(21, 7)) / 1000.0;
                y[0] = int.Parse(line.Substring(28, 7)) / 1000.0;
                int numberOfPoints = 1;
                if (line.Length >= 50)
                {
                    x[1] = int.Parse(line.Substring(36, 7)) / 1000.0;
                    y[1] = int.Parse(line.Substring(43, 7)) / 1000.0;
                    numberOfPoints++;
                    if (line.Length >= 65)
                    {
                        x[2] = int.Parse(line.Substring(51, 7)) / 1000.0;
                        y[2] = int.Parse(line.Substring(58, 7)) / 1000.0;
                        numberOfPoints++;
                        if (line.Length >= 80)
                        {
                            x[3] = int.Parse(line.Substring(66, 7)) / 1000.0;
                            y[3] = int.Parse(line.Substring(73, 7)) / 1000.0;
                            numberOfPoints++;
                        }
                    }
                }
                if (i > 0)
                {
                    if (lastStation != station)
                    {
                        if (other.Count > 0)
                            otherPoints.Add(other);
                        TStation st = new TStation(lastStation, CustomSettings.GroundLevelIndex, CustomSettings.CrownOfTheRoadIndex, gl, cotr, CustomSettings.LinksIndex,
                            CustomSettings.RechtIndex, links, rechts, otherPoints);
                        stations.Add(st);
                        gl = new List<Point2d>();
                        cotr = new List<Point2d>();
                        links = new List<Point2d>();
                        rechts = new List<Point2d>();
                        other = new List<Point2d>();
                        otherPoints = new List<List<Point2d>>();
                    }
                }
                else
                    lastLevelline = levelline;
                if (levelline == CustomSettings.GroundLevelIndex)
                    for (int j = 0; j < numberOfPoints; j++)
                        gl.Add(new Point2d(x[j], y[j]));
                else if (levelline == CustomSettings.CrownOfTheRoadIndex)
                    for (int j = 0; j < numberOfPoints; j++)
                        cotr.Add(new Point2d(x[j], y[j]));
                else if (levelline == CustomSettings.LinksIndex)
                    for (int j = 0; j < numberOfPoints; j++)
                        links.Add(new Point2d(x[j], y[j]));
                else if (levelline == CustomSettings.RechtIndex)
                    for (int j = 0; j < numberOfPoints; j++)
                        rechts.Add(new Point2d(x[j], y[j]));
                else
                {
                    if (lastLevelline != levelline)
                    {
                        otherPoints.Add(other);
                        other = new List<Point2d>();
                    }
                    if (other.Count == 0)
                        other.Insert(0, new Point2d(levelline, 0));
                    for (int j = 0; j < numberOfPoints; j++)
                        other.Add(new Point2d(x[j], y[j]));

                }
                lastStation = station;
                lastLevelline = levelline;
                if (i == content.Length - 1)
                {//ako smo u poslednjoj liniji onda treba da dodamo stanicu
                    if (other.Count > 0)
                        otherPoints.Add(other);
                    TStation st = new TStation(lastStation, CustomSettings.GroundLevelIndex, CustomSettings.CrownOfTheRoadIndex, gl, cotr, CustomSettings.LinksIndex,
                            CustomSettings.RechtIndex, links, rechts, otherPoints);
                    stations.Add(st);
                }
            }

            return stations;
        }
    }*/
    public static class ImportExport066
    {
        public static string SaveFile(List<TStation> stations, ref int groundlevel, ref int crownOfTheRoad)
        {
            bool gl = false;
            bool cotr = false;
            //gde se cuva jos nije odredjeno
            StringBuilder text = new StringBuilder();
            foreach (TStation station in stations)
            {
                if (station.NumberOfGLPoints <= 0)
                    break;
                //ako se barem jednom udje u petlju, znaci postoji gl i treba ga izabrati u profiler
                gl = true;
                int n = station.NumberOfGLPoints / 4 + (station.NumberOfGLPoints % 4 == 0 ? 0 : 1);
                int stationsNum = station.NumberOfGLPoints;
                for (int i = 0; i < n; i++)
                {
                    text.Append("66     " + String.Format("{0,2}", station.GL)
                       + String.Format("{0,9}", (int)(station.Distance * 1000)) +
                       string.Format("{0,2}", i + 1) + " " +
                       string.Format("{0,7}", (int)(station.GetGLPoint(i * 4).X * 1000)) + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4).Y * 1000)));
                    if (i * 4 + 1 < stationsNum)
                        text.Append(" " + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 1).X * 1000)) + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 1).Y * 1000)));
                    if (i * 4 + 2 < stationsNum)
                        text.Append(" " + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 2).X * 1000)) + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 2).Y * 1000)));
                    if (i * 4 + 3 < stationsNum)
                        text.Append(" " + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 3).X * 1000)) + string.Format("{0,7}", (int)(station.GetGLPoint(i * 4 + 3).Y * 1000)));
                    text.Append("\r\n");
                }
                if (station.NumberOfCOTRPoints > 0)
                {
                    cotr = true;
                    text.Append("66     " + String.Format("{0,2}", station.COTR)
                      + String.Format("{0,9}", (int)(station.Distance * 1000)) + " 1 " +
                      string.Format("{0,7}", (int)(station.GetCOTRPoint(0).X * 1000)) + string.Format("{0,7}", (int)(station.GetCOTRPoint(0).Y * 1000)));
                    text.Append(" " + string.Format("{0,7}", (int)(station.GetCOTRPoint(1).X * 1000)) + string.Format("{0,7}", (int)(station.GetCOTRPoint(1).Y * 1000)));
                    text.Append(" " + string.Format("{0,7}", (int)(station.GetCOTRPoint(2).X * 1000)) + string.Format("{0,7}", (int)(station.GetCOTRPoint(2).Y * 1000)));
                }

                text.Append("\r\n");
            }
            if (!cotr)
                crownOfTheRoad = -1;
            if (!gl)
                groundlevel = -1;
            return text.ToString();
        }
        /// <summary>
        /// Ucitava fajl uz popunjavanje gl, cotr, links i rechts
        /// </summary>
        public static List<TStation> LoadFileSpecific(string[] content)
        {
            List<TStation> loadedStations = LoadFileSimple(content);
            foreach (TStation station in loadedStations)
            {
                List<Point2d> gl = null;
                List<Point2d> cotr = null;
                List<Point2d> links = null;
                List<Point2d> rechts = null;
                foreach (List<Point2d> line in station.GetOtherPoints())
                {
                    if (line.Count == 0)
                        continue;
                    if (line[0].X == CustomSettings.GroundLevelIndex)
                        gl = line;
                    else if (line[0].X == CustomSettings.CrownOfTheRoadIndex)
                        cotr = line;
                    else if (line[0].X == CustomSettings.LinksIndex)
                        links = line;
                    else if (line[0].X == CustomSettings.RechtIndex)
                        rechts = line;
                }
                List<Point2d> temp = gl;
                //Vadimo prvi element jer je on indeks linije
                if (temp != null)
                {
                    station.RemoveOtherPointsLine(temp);
                    temp.Remove(temp[0]);
                    station.SetGLPoints(temp);
                }
                temp = cotr;
                if (temp != null)
                {
                    station.RemoveOtherPointsLine(temp);
                    temp.Remove(temp[0]);
                    station.SetGLPoints(temp);
                }
                temp = links;
                if (temp != null)
                {
                    station.RemoveOtherPointsLine(temp);
                    temp.Remove(temp[0]);
                    station.SetGLPoints(temp);
                }
                temp = rechts;
                if (temp != null)
                {
                    station.RemoveOtherPointsLine(temp);
                    temp.Remove(temp[0]);
                    station.SetGLPoints(temp);
                }

            }
            return loadedStations;
            #region Old func back up
            /*
            //dohvatiti sve i smestiti u listu stanica
            List<TStation> stations = new List<TStation>(); 
            int levelline;
            double station;
            int lineIndex;//broj linije - ako polilinija ima vise od 4 tacke, onda je vrednost >1
            double[] x = new double[4];
            double[] y = new double[4];
            List<Point2d> gl = new List<Point2d>();
            List<Point2d> cotr = new List<Point2d>();
            List<Point2d> links = new List<Point2d>();
            List<Point2d> rechts = new List<Point2d>();
            List<Point2d> other = new List<Point2d>();
            List<List<Point2d>> otherPoints = new List<List<Point2d>>();
            int lastLevelline = 0;
            double lastStation = -1;
            for (int i = 0; i < content.Length; i++)
            {
                string line = content[i];
                levelline = int.Parse(line.Substring(7, 2));
                station = Math.Round((int.Parse(line.Substring(9, 9)) / 1000.0),3);
                lineIndex = int.Parse(line.Substring(18, 2));
                x[0] = int.Parse(line.Substring(21, 7)) / 1000.0;
                y[0] = int.Parse(line.Substring(28, 7)) / 1000.0;
                int numberOfPoints = 1;
                if (line.Length >= 50)
                {
                    x[1] = int.Parse(line.Substring(36, 7)) / 1000.0;
                    y[1] = int.Parse(line.Substring(43, 7)) / 1000.0;
                    numberOfPoints++;
                    if (line.Length >= 65)
                    {
                        x[2] = int.Parse(line.Substring(51, 7)) / 1000.0;
                        y[2] = int.Parse(line.Substring(58, 7)) / 1000.0;
                        numberOfPoints++;
                        if (line.Length >= 80)
                        {
                            x[3] = int.Parse(line.Substring(66, 7)) / 1000.0;
                            y[3] = int.Parse(line.Substring(73, 7)) / 1000.0;
                            numberOfPoints++;
                        }
                    }
                }
                if (i > 0)
                {
                    if (lastStation != station)
                    {
                        if (other.Count > 0)
                            otherPoints.Add(other);
                        TStation st = new TStation(lastStation, CustomSettings.GroundLevelIndex, CustomSettings.CrownOfTheRoadIndex, gl, cotr, CustomSettings.LinksIndex,
                            CustomSettings.RechtIndex, links, rechts, otherPoints);
                        stations.Add(st);
                        gl = new List<Point2d>();
                        cotr = new List<Point2d>();
                        links = new List<Point2d>();
                        rechts = new List<Point2d>();
                        other = new List<Point2d>();
                        otherPoints = new List<List<Point2d>>();
                    }
                }
                else
                    lastLevelline = levelline;
                if (levelline == CustomSettings.GroundLevelIndex)
                    for (int j = 0; j < numberOfPoints; j++)
                        gl.Add(new Point2d(x[j], y[j]));
                else if (levelline == CustomSettings.CrownOfTheRoadIndex)
                    for (int j = 0; j < numberOfPoints; j++)
                        cotr.Add(new Point2d(x[j], y[j]));
                else if (levelline == CustomSettings.LinksIndex)
                    for (int j = 0; j < numberOfPoints; j++)
                        links.Add(new Point2d(x[j], y[j]));
                else if (levelline == CustomSettings.RechtIndex)
                    for (int j = 0; j < numberOfPoints; j++)
                        rechts.Add(new Point2d(x[j], y[j]));
                else
                {
                    if (lastLevelline != levelline)
                    {
                        otherPoints.Add(other);
                        other = new List<Point2d>();
                    }
                    if (other.Count == 0)
                        other.Insert(0, new Point2d(levelline, 0));
                    for (int j = 0; j < numberOfPoints; j++)
                        other.Add(new Point2d(x[j], y[j]));

                }
                lastStation = station;
                lastLevelline = levelline;
                if (i == content.Length - 1)
                {//ako smo u poslednjoj liniji onda treba da dodamo stanicu
                    if (other.Count > 0)
                        otherPoints.Add(other);
                    TStation st = new TStation(lastStation, CustomSettings.GroundLevelIndex, CustomSettings.CrownOfTheRoadIndex, gl, cotr, CustomSettings.LinksIndex,
                            CustomSettings.RechtIndex, links, rechts, otherPoints);
                    stations.Add(st);
                }
            }

            return stations;
             */
            #endregion
        }
        /// <summary>
        /// Ucitava 066 fajl u niz stanica i smesta sve u otherPoints - 
        /// gde je prvi element.X indeks linije. Kasnije se odatle ekstraktuju gl, cotr, links i rechts
        /// </summary>
        public static List<TStation> LoadFileSimple(string[] content)
        {
            //dohvatiti sve i smestiti u listu stanica
            List<TStation> stations = new List<TStation>();
            int levelline;
            double station;
            int lineIndex;//broj linije - ako polilinija ima vise od 4 tacke, onda je vrednost >1
            double[] x = new double[4];
            double[] y = new double[4];
            /*List<Point2d> gl = new List<Point2d>();
            List<Point2d> cotr = new List<Point2d>();
            List<Point2d> links = new List<Point2d>();
            List<Point2d> rechts = new List<Point2d>();*/
            List<Point2d> other = new List<Point2d>();
            List<List<Point2d>> otherPoints = new List<List<Point2d>>();
            int lastLevelline = 0;
            double lastStation = -1;
            for (int i = 0; i < content.Length; i++)
            {
                content[i] = content[i].Trim();
                string line = content[i];
                if (line.Length > 20)
                {
                    levelline = int.Parse(line.Substring(7, 2));
                    station = Math.Round((int.Parse(line.Substring(9, 9)) / 1000.0), 3);
                    lineIndex = int.Parse(line.Substring(18, 2));
                    x[0] = int.Parse(line.Substring(21, 7)) / 1000.0;
                    y[0] = int.Parse(line.Substring(28, 7)) / 1000.0;
                    int numberOfPoints = 1;
                    if (line.Length >= 50)
                    {
                        x[1] = int.Parse(line.Substring(36, 7)) / 1000.0;
                        y[1] = int.Parse(line.Substring(43, 7)) / 1000.0;
                        numberOfPoints++;
                        if (line.Length >= 65)
                        {
                            x[2] = int.Parse(line.Substring(51, 7)) / 1000.0;
                            y[2] = int.Parse(line.Substring(58, 7)) / 1000.0;
                            numberOfPoints++;
                            if (line.Length >= 80)
                            {
                                x[3] = int.Parse(line.Substring(66, 7)) / 1000.0;
                                y[3] = int.Parse(line.Substring(73, 7)) / 1000.0;
                                numberOfPoints++;
                            }
                        }
                    }
                    if (i > 0)
                    {
                        if (lastStation != station)
                        {
                            if (other.Count > 0)
                                otherPoints.Add(other);
                            /*TStation st = new TStation(lastStation, CustomSettings.GroundLevelIndex, CustomSettings.CrownOfTheRoadIndex, gl, cotr, CustomSettings.LinksIndex,
                                CustomSettings.RechtIndex, links, rechts, otherPoints);*/
                            TStation st = new TStation(lastStation, -1, -1, null, null, -1, -1, null, null, otherPoints);
                            stations.Add(st);
                            /*gl = new List<Point2d>();
                            cotr = new List<Point2d>();
                            links = new List<Point2d>();
                            rechts = new List<Point2d>();*/
                            other = new List<Point2d>();
                            otherPoints = new List<List<Point2d>>();
                        }
                    }
                    else
                        lastLevelline = levelline;
                    /*if (levelline == CustomSettings.GroundLevelIndex)
                        for (int j = 0; j < numberOfPoints; j++)
                            gl.Add(new Point2d(x[j], y[j]));
                    else    if (levelline == CustomSettings.CrownOfTheRoadIndex)
                        for (int j = 0; j < numberOfPoints; j++)
                            cotr.Add(new Point2d(x[j], y[j]));
                    else if (levelline == CustomSettings.LinksIndex)
                        for (int j = 0; j < numberOfPoints; j++)
                            links.Add(new Point2d(x[j], y[j]));
                    else if (levelline == CustomSettings.RechtIndex)
                        for (int j = 0; j < numberOfPoints; j++)
                            rechts.Add(new Point2d(x[j], y[j]));
                    else*/
                    {
                        if (lastLevelline != levelline)
                        {
                            if (other.Count > 0)
                                otherPoints.Add(other);
                            other = new List<Point2d>();
                        }
                        if (other.Count == 0)
                            other.Insert(0, new Point2d(levelline, 0));
                        for (int j = 0; j < numberOfPoints; j++)
                            other.Add(new Point2d(x[j], y[j]));
                    }
                    lastStation = station;
                    lastLevelline = levelline;
                }
                if (i == content.Length - 1)
                {//ako smo u poslednjoj liniji onda treba da dodamo stanicu
                    if (other.Count > 0)
                        otherPoints.Add(other);
                    TStation st = new TStation(lastStation, -1, -1, null, null, -1, -1, null, null, otherPoints);
                    stations.Add(st);
                }
            }

            return stations;
        }
    }
}
