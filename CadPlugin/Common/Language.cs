﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Common
{
	static class Language
	{
		/// <summary>
		/// Language independent, because it can make problems in saving data for drawing (in nod)
		/// </summary>
		public static string Ratio = "Ratio";
		/// <summary>
		/// Language independent, because it can make problems in saving data for drawing (in nod)
		/// </summary>
		public static string Percent = "Percent";
		

	}
}
