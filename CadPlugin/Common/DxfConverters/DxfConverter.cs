﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common.DxfConverters.DxfConverterEntities;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common.DxfConverters
{
    static class DxfConverter
    {
        private static Dictionary<string, IDxfConverterEntity> converters = new Dictionary<string, IDxfConverterEntity>();
        static DxfConverter()
        {
            // Initialize here all supported converters.
            converters.Add(nameof(DBPoint), new DxfConverterDBPoint());
        }

        /// <summary>
        /// Creates values from entity.
        /// </summary>
        /// <param name="tw"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static TypedValue[] ToDxf(TransactionWrapper tw, Entity e)
        {
            string name = e.GetType().Name;
            TypedValue[] v = null;
            if (converters.ContainsKey(name))
                v = converters[name].SaveEntity(tw, e);
            return v;
        }
        /// <summary>
        /// Creates new entity with data from values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tw"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public static T FromDxf<T>(TransactionWrapper tw, TypedValue[] v) where T:Entity
        {
            string name = typeof(T).Name;
            T res = null;
            if (converters.ContainsKey(name))
                res = (T)converters[name].LoadEntity(tw, v);
            return res;
        }
        /// <summary>
        /// Creates new entity with data from t (entity type is defined in t).
        /// </summary>
        /// <param name="tw"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static Entity FromDxf(TransactionWrapper tw, TypedValue[] t)
        {
            string name = DxfConverterBase.GetTypeName(t);
            Entity e = null;
            if (converters.ContainsKey(name))
                e = converters[name].LoadEntity(tw, t);
            return e;
        }
        /// <summary>
        /// Updates existing entity with data from v.
        /// </summary>
        /// <param name="tw"></param>
        /// <param name="v"></param>
        /// <param name="e"></param>
        public static void FromDxfToEntity(TransactionWrapper tw, TypedValue[] v, Entity e)
        {
            string name = DxfConverterBase.GetTypeName(v);
            if (converters.ContainsKey(name))
                converters[name].LoadToEntity(tw, v, e);
        }

    }
}
