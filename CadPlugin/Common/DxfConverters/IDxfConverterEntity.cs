﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common.DxfConverters
{
    /// <summary>
    /// Defines operations of Loading/Saving entity to dxf.
    /// </summary>
    interface IDxfConverterEntity
    {
        //NOTE Some entities require to be added to the database, in order to
        //NOTE perform some other operations (for example i think adding points to Polyline3d).
        //NOTE This is main reason to have TransactionWrapper passed as parameter).

        /// <summary>
        /// Ensures that provided entity and provided values match (load data only if necessary).
        /// When property is different, it is updated to Entity.
        /// </summary>
        /// <param name="v"></param>
        /// <param name="tw"></param>
        /// <returns></returns>
        void LoadToEntity(TransactionWrapper tw, TypedValue[] v, Entity e);
        /// <summary>
        /// Creates new entity and loads data to it.
        /// </summary>
        /// <param name="v"></param>
        /// <param name="tw"></param>
        /// <returns></returns>
        Entity LoadEntity(TransactionWrapper tw, TypedValue[] v);
        TypedValue[] SaveEntity(TransactionWrapper tw, Entity e);
    }
}
