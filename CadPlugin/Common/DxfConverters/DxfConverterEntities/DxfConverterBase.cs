﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common.DxfConverters.DxfConverterEntities
{
    abstract class DxfConverterBase
    {
        protected static TypedValue SaveTypeName(Entity e)
        {
            return new TypedValue((int)DxfCode.Start, e.GetType().Name);
        }
        public static string GetTypeName(TypedValue[] tv)
        {
            string res = null;
            TypedValue t = tv.FirstOrDefault(x => x.TypeCode == (int)DxfCode.Start);
            if (t != null)
                res = t.Value as string;
            return res;
        }
        public static bool IsOfType<T>(TypedValue[] v)
        {
            string tName = typeof(T).Name;
            string name = GetTypeName(v);
               bool res = name == tName;
            return res;
        }
    }
}
