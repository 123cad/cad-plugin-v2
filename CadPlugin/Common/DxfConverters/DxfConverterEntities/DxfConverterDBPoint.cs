﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
//using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
//using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common.DxfConverters.DxfConverterEntities
{

    class DxfConverterDBPoint:DxfConverterBase, IDxfConverterEntity
    {

        public TypedValue[] SaveEntity(TransactionWrapper tw, Entity e)
        {
            DBPoint pt = e as DBPoint;
            if (pt == null)
                throw new MyExceptions.NotSupportedException($"{e?.GetType().Name} is not a DBPoint!");
            List<TypedValue> v = new List<TypedValue>()
            {
                SaveTypeName(pt),
                new TypedValue((int)DxfCode.XCoordinate, pt.Position),
                new TypedValue((int)DxfCode.Handle, pt.ObjectId.Handle),
                new TypedValue((int)DxfCode.Thickness, pt.Thickness)
            };
            if (pt.Color.ColorMethod == ColorMethod.ByColor)
                v.Add(new TypedValue((int)DxfCode.Color, pt.Color));


            // Save Extension dictionary.
            return v.ToArray();
        }

        public void LoadToEntity(TransactionWrapper tw, TypedValue[] v, Entity e)
        {
            DBPoint p = e as DBPoint;
            if (p == null)
                throw new MyExceptions.NotSupportedException($"{e.GetType().Name} is not a DBPoint!");
            Dictionary<DxfCode, object> dic = v.ToDictionary(x => (DxfCode)x.TypeCode, y => y.Value);

            p.Position = (Point3d)dic[DxfCode.XCoordinate];
            p.Thickness = (double)dic[DxfCode.Thickness];
            if (dic.ContainsKey(DxfCode.Color))
                p.Color = (Color)dic[DxfCode.Color];
        }

        public Entity LoadEntity(TransactionWrapper tw, TypedValue[] v)
        {
            if (!IsOfType<DBPoint>(v))
                throw new MyExceptions.NotSupportedException($"{GetTypeName(v).GetType().Name} is not a DBPoint!");
            DBPoint p = tw.CreateEntity<DBPoint>();
            LoadToEntity(tw, v, p);
            return p;
        }
    }
}
