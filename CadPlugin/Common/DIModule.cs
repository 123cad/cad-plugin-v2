﻿using CadPlugin.SewageHelpers;
using GlobalDependencyInjection;
using SewageUtilities.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Common
{
    class DIModule : BaseDIModule
    {
        protected override void LoadModule()
        {
            Bind<PipeHelper>().To<PipeHelper>().InSingletonScope();
        }
    }
}
