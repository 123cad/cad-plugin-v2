﻿using System;
using System.Collections.Generic;
using System.Linq;
using Teigha.Geometry;
#if BRICSCAD

#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.DTMBalance.VolumeCalculators
{
    /// <summary>
    /// Triangle with volume information.
    /// </summary>
    public class Triangle
    {
        public Point3d P0 { get => _p0; private set { _p0 = value; refresh(); } }
        public Point3d P1 { get => _p1; private set { _p1 = value; refresh(); } }
        public Point3d P2 { get => _p2; private set { _p2 = value; refresh(); } }

        public double MinimumHeight { get; private set; }
        public double MaximumHeight { get; private set; }

        public Point2d MinimumPoint { get; private set; }
        public Point2d MaximumPoint { get; private set; }

        /// <summary>
        /// Volume of triangle from min height to max height, with 2d (xy) boundary.
        /// </summary>
        public double Volume { get; private set; }
        /// <summary>
        /// Area of triangle base.
        /// (all points are set to Z=0)
        /// </summary>
        public double Area2d { get; private set; }

        private Point3d _p0;
        private Point3d _p1;
        private Point3d _p2;

        public Triangle(Point3d p0, Point3d p1, Point3d p2)
        {
            _p0 = p0;
            _p1 = p1;
            _p2 = p2;
            refresh();
        }
        private void refreshPoints()
        {
            double minY, minX, maxY, maxX;
            List<Point3d> pts = new List<Point3d>() { P0, P1, P2 };
            minY = pts.Min(x => x.Y);
            minX = pts.Min(x => x.X);
            maxY = pts.Max(x => x.Y);
            maxX = pts.Max(x => x.X);
            MinimumPoint = new Point2d(minX, minY);
            MaximumPoint = new Point2d(maxX, maxY);
        }
        private void refresh()
        {
            refreshPoints();
            int minIndex = 0;
            double min = P0.Z;
            if (P1.Z < min)
            {
                min = P1.Z;
                minIndex = 1;
            }
            if (P2.Z < min)
            {
                min = P2.Z;
                minIndex = 2;
            }
            MinimumHeight = min;

            double max = P0.Z;
            int maxIndex = 0;
            if (P1.Z > max)
            {
                max = P1.Z;
                maxIndex = 1;
            }
            if (P2.Z > max)
            {
                max = P2.Z;
                maxIndex = 2;
            }
            MaximumHeight = max;


            double a1, a2, a3;
            a1 = P0.X * (P1.Y - P2.Y);
            a2 = P1.X * (P2.Y - P0.Y);
            a3 = P2.X * (P0.Y - P1.Y);
            double P = 1.0 / 2 * (a1 + a2 + a3);
            Area2d = P;

            if (minIndex == maxIndex)
            {
                Volume = 0;
                return;
            }

            Point3d variablePoint;
            bool[] used = new bool[3];
            used[minIndex] = true;
            used[maxIndex] = true;
            if (!used[0])
                variablePoint = P0;
            else if (!used[1])
                variablePoint = P1;
            else if (!used[2])
                variablePoint = P2;
            else
                throw new NotSupportedException("Variable point not found!");
            /*
                Calculating volume of sliced prism:
                    - base is triangle in 2d (z=0)
                    - height is max-min height
                    - sliced by 3d triangle
                    - 2 points of prism and triangle must mach, 3rd point
                        is on line which connects 3rd xy with Z min and Z max
                Volume depends on position of this point. 
                However, rule is found (by me - could be wrong), that volume can be scaled depending on position
                of 3rd point.
                Formula for prism volume is B*H .
                Formula for pyramid volume is 1/3*B*H .
                Formula for sliced prism:
                    - if 3rd point's Z is at max, then formula is B*H * 2 / 3
                    - if 3rd point's Z is at min, then formula is B*H * 1 / 3
                    - if 3rd point's Z is between min and max, formula is B*H * 1 / 3 * m
                        - m is (it goes from 1/3 to 2/3): 1 + (Z3 - min) / H

             */


            // Area of triangle from 3 points (using matrix).
            

            // Calculate volume of 3 sided prism (from bottom point to top point),
            // and divide by 2 to get wanted volume.
            double height = MaximumHeight - MinimumHeight;
            double totalV = P * height;

            double c = (variablePoint.Z - MinimumHeight) / height;
            if (c < 0 || c > 1)
                throw new NotSupportedException("Height must be between min and max!");

            Volume = totalV * (1 + c) / 3;
        }

        public double GetVolumeFromZ(double z)
        {
            if (z > MinimumHeight)
                throw new NotSupportedException("height must be under minimum height!");

            double height = MinimumHeight - z;

            double V = Area2d * height;
            return Volume + V;
        }
    }
}
