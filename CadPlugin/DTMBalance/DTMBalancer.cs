﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.DTMBalance.VolumeCalculators;
using wf = System.Windows.Forms;
using wpf = System.Windows;

#if BRICSCAD
using Bricscad.Runtime;
using Bricscad.ApplicationServices;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Runtime;
using Teigha.Colors;
#endif
#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.DTMBalance
{
    class DTMBalancer
    {
        public void Command_DTMBalance(Document doc)
        {

            doc.Editor.WriteMessage("Select entities: ");
            PromptSelectionOptions optsS = new PromptSelectionOptions();
            var resS = doc.Editor.GetSelection(optsS);
            if (resS.Status != PromptStatus.OK)
                return;

            List<Triangle> triangles = new List<Triangle>();
            Action<Face> addFace = (face) =>
            {
                if (face == null)
                    return;
                var pts = PointsExtractor.GetPoints(face);
                var triangle = new Triangle(pts[0], pts[1], pts[2]);
                if (pts.Count > 3)
                    throw new NotSupportedException("Face can't have 4 points!");
                triangles.Add(triangle);
            };
            using (Transaction tr = doc.TransactionManager.StartTransaction())
            {
                var t = new PointsExtractor();
                t.Initialize(tr);
                foreach (ObjectId pid in resS.Value.GetObjectIds())
                {
                    var ent = tr.GetObject(pid, OpenMode.ForRead) as Entity;
                    if (ent is Face)
                        addFace(ent as Face);
                    else if (ent is PolyFaceMesh)
                    {
                        var faces = PointsExtractor.GetFaces(tr, ent as PolyFaceMesh);
                        foreach (var f in faces)
                            addFace(f);
                    }
                }
                // Do commit even for readonly operations.
                tr.Commit();
            }

            if (triangles.Count == 0)
            {
                string msg = "No triangles found! \nPlease select Face or DTM";
                wpf.MessageBox.Show(msg, "", wpf.MessageBoxButton.OK, wpf.MessageBoxImage.Warning);
                return;
            }

            double minHeight = triangles.Min(x => x.MinimumHeight);
            double baseArea = triangles.Sum(x => x.Area2d);
            double volume = triangles.Sum(x => x.GetVolumeFromZ(minHeight));

            double alignedHeightDelta = volume / baseArea;
            double alignedHeight = minHeight + alignedHeightDelta;

            List<Point3d> allPoints = triangles.SelectMany(x => new[] { x.P0, x.P1, x.P2 }).ToList();//.Where(x => x.Z < alignedHeight).ToList();
            List<Point3d> underPoints = new List<Point3d>();
            List<Point3d> abovePoints = new List<Point3d>();
            for (int i = 0; i < allPoints.Count; i++)
            {
                var p = allPoints[i];
                if (p.Z < alignedHeight)
                    underPoints.Add(p);
                else
                    abovePoints.Add(p);
            }

            double minY = triangles.Min(x => x.MinimumPoint.Y);
            double minX = triangles.Min(x => x.MinimumPoint.X);
            double maxY = triangles.Max(x => x.MaximumPoint.Y);
            double maxX = triangles.Max(x => x.MaximumPoint.X);

            var db = doc.Database;
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

                /*
                foreach(var p in underPoints)
                {
                    var pt = new DBPoint(p);
                    btr.AppendEntity(pt);
                    tr.AddNewlyCreatedDBObject(pt, true);
                }*/

                var pl = new Polyline();
                pl.Closed = true;
                pl.Elevation = alignedHeight;
                btr.AppendEntity(pl);
                tr.AddNewlyCreatedDBObject(pl, true);

                var points = new List<Point2d>()
                {
                    new Point2d(minX, minY),
                    new Point2d(maxX, minY),
                    new Point2d(maxX, maxY),
                    new Point2d(minX, maxY)
                };
                for (int i = 0; i < points.Count; i++)
                    pl.AddVertexAt(i, points[i], 0, 0, 0);
                /*

                Hatch h = new Hatch();
                btr.AppendEntity(h);
                tr.AddNewlyCreatedDBObject(h, true);
                h.Color = Color.FromColor(System.Drawing.Color.Red);
                h.AppendLoop(HatchLoopTypes.Outermost, new ObjectIdCollection(new[] { pl.Id }));

                foreach (var p in abovePoints)
                {
                    var pt = new DBPoint(p);
                    btr.AppendEntity(pt);
                    tr.AddNewlyCreatedDBObject(pt, true);
                }*/

                // Do commit even for readonly operations.
                tr.Commit();
            }


        }
    }
}
