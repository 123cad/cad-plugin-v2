﻿using System.Collections.Generic;
using Teigha.DatabaseServices;
using Teigha.Geometry;
#if BRICSCAD

#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.DTMBalance
{
    public class PointsExtractor
    {
        public Region Region { get; } = new Region();
        private Transaction tr;
        public void Initialize(Transaction tr)
        {
            this.tr = tr;
        }
        public void ExtractPoint(params Entity[] ents)
        {
            foreach(var e in ents)
            {
                if (GetPoints(e as Polyline)) continue;
                if (GetPoints(e as Polyline2d)) continue;
                if (GetPoints(e as Polyline3d)) continue;
                if (GetPoints(e as Line)) continue;
                if (GetPoints(e as DBPoint)) continue;
                if (GetPoints(e as Circle)) continue;
                if (GetPoints(e as PolyFaceMesh)) continue;
                if (getPoints(e as Face)) continue;
                
            }
        }

        public bool GetPoints(Polyline pl)
        {
            if (pl == null)
                return false;
            for (int i = 0; i < pl.NumberOfVertices; i++)
            {
                var p = pl.GetPoint2dAt(i);
                Region.AddPoint(p);
            }
            return true;
        }
        public bool GetPoints(Polyline2d pl)
        {
            if (pl == null)
                return false;
            for (double i = 0; i - 0.05 < pl.EndParam; i+=1)
            {
                if (i + 1 > pl.EndParam)
                    Region.AddPoint(pl.EndPoint);
                else
                {
                    var pt = pl.GetPointAtParameter(i);
                    Region.AddPoint(pt);
                }
            }
            return true;
        }
        public bool GetPoints(Polyline3d pl)
        {
            if (pl == null)
                return false;
            foreach (ObjectId oid in pl)
            {
                var p = tr.GetObject(oid, OpenMode.ForRead) as PolylineVertex3d;
                Region.AddPoint(p.Position);
            }
            return true;
        }
        public bool GetPoints(Line l)
        {
            if (l == null)
                return false;
            Region.AddPoint(l.StartPoint);
            Region.AddPoint(l.EndPoint);
            return true;
        }
        public bool GetPoints(DBPoint p)
        {
            if (p == null)
                return false;
            Region.AddPoint(p.Position);
            return true;
        }

        public bool GetPoints(Circle c)
        {
            if (c == null)
                return false;
            Region.AddPoint(c.Center);
            return true;
        }
        public bool GetPoints(PolyFaceMesh m)
        {
            if (m == null)
                return false;
            foreach(ObjectId pid in m)
            {
                var ent = tr.GetObject(pid, OpenMode.ForWrite) as Entity;
                var pmv = ent as PolyFaceMeshVertex;
                DBObjectCollection collection = new DBObjectCollection();
                (m as PolyFaceMesh).ExplodeGeometry(collection);
                foreach(DBObject eO in collection)
                {
                    //var eO = tr.GetObject(eId, OpenMode.ForRead);
                    if (!(eO is Face))
                    {

                    }
                    else
                        getPoints(eO as Face);
                }
                if (pmv != null)
                    Region.AddPoint(pmv.Position);
                else
                {
                    //var fc = ent as FaceRecord;
                    //if (fc != null)
                    //{
                    //    Region.AddPoint(fc.get);
                    //}
                }
            }
            

            return true;
        }
        public bool getPoints(Face f)
        {
            if (f == null)
                return false;
            Point3d last = new Point3d();
            for (int i = 0; i < 4; i++)
            {
                var p = f.GetVertexAt((short)i);
                if (i == 3)
                {
                    if (p.GetVectorTo(last).Length < 1e-5)
                        break;
                }
                Region.AddPoint(p);
                last = p;
            }
            

            return true;
        }

        public static List<Point3d> GetPoints(Face f)
        {
            List<Point3d> pts = new List<Point3d>();
            Point3d last = new Point3d();
            for (int i = 0; i < 4; i++)
            {
                var p = f.GetVertexAt((short)i);
                if (i == 3)
                {
                    if (p.GetVectorTo(last).Length < 1e-5)
                        break;
                }
                pts.Add(p);
                last = p;
            }
            return pts;
        }

        public static List<Face> GetFaces(Transaction tr, PolyFaceMesh mesh)
        {
            var list = new List<Face>();
            //foreach (ObjectId pid in mesh)
            {
                DBObjectCollection collection = new DBObjectCollection();
                mesh.ExplodeGeometry(collection);
                foreach (DBObject eO in collection)
                {
                    var f = eO as Face;
                    if (f != null)
                        list.Add(f);
                }
            }

            return list;
        }
        
    }
}
