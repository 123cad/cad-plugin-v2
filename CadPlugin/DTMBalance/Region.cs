﻿using System.Collections.Generic;
using Teigha.Geometry;
#if BRICSCAD

#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.DTMBalance
{
    /// <summary>
    /// 
    /// </summary>
    public class Region
    {
        public List<Point3d> Points = new List<Point3d>();

        public double MaximumZ { get; private set; }
        public double MinimumZ { get; private set; }
        public double MinimumY { get; private set; }
        public double MaximumY { get; private set; }
        public double MinimumX { get; private set; }
        public double MaximumX { get; private set; }
        public System.Drawing.Rectangle Region2d { get; private set; } = new System.Drawing.Rectangle();

        public void AddPoint(double x, double y, double z = 0) => Points.Add(new Point3d(x, y, z));
        public void AddPoint(Point2d p) => AddPoint(p.X, p.Y);
        public void AddPoint(Point3d p) => Points.Add(p);

        public void ProcessPoints()
        {
            MinimumZ = MaximumZ = MinimumY = MaximumY = MinimumX = MaximumX = 0;
            if (Points.Count == 0)
                return;
            var pt = Points[0];
            MinimumX = pt.X;
            MaximumX = pt.X;
            MinimumY = pt.Y;
            MaximumY = pt.Y;
            MinimumZ = pt.Z;
            MaximumZ = pt.Z;

            foreach (var p in Points)
            {
                if (p.X < MinimumX)
                    MinimumX = p.X;
                else if (p.X > MaximumX)
                    MaximumX = p.X;

                if (p.Y < MinimumY)
                    MinimumY = p.Y;
                else if (p.Y > MaximumY)
                    MaximumY = p.Y;

                if (p.Z < MinimumZ)
                    MinimumZ = p.Z;
                else if (p.Z > MaximumZ)
                    MaximumZ = p.Z;
            }
        }
    }
}
