﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.BlockAttributeManagers
{
	class BlockAttributesManagerController
	{
		public static void Start(Document doc)
		{
			BlockAttributeManagerView view = new BlockAttributeManagerView(doc);
			//view.Show();
			var res = view.ShowDialog();//  Application.ShowModalDialog(view);
			if (res == System.Windows.Forms.DialogResult.OK)
				Apply(doc, view.data);

		}
		/// <summary>
		/// Applies changes to BlockReferences.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="applyToAll">True - apply to all BlockReference in the drawing. False - apply only to selected ones.</param>
		private static void Apply(Document doc, BlockAttributeManagerView.Data data)
		{
			List<ObjectId> selected = data.selectedBlocks;
			using (Transaction tr = doc.Database.TransactionManager.StartTransaction())
			{
				if (data.ApplyToAll)
				{
					BlockTable bt = tr.GetObject(doc.Database.BlockTableId, OpenMode.ForRead) as BlockTable;
					BlockTableRecord btr = tr.GetObject(bt[data.BlockName], OpenMode.ForRead) as BlockTableRecord;
					ObjectIdCollection blocks = btr.GetBlockReferenceIds(true, false);

					selected.Clear();
					foreach (ObjectId id in blocks)
						selected.Add(id);
				}
				// Maybe something is changed in current block, which is not changed in others
				// so we want to apply changes from current one to new ones.
				//if (data.ApplyToAll || data.HasDataChanged || data.HasOrderChanged)
				{
					foreach (ObjectId oid in selected)
					{
						BlockReference br = tr.GetObject(oid, OpenMode.ForWrite) as BlockReference;
						foreach (ObjectId id in br.AttributeCollection)
						{
							AttributeReference ar = tr.GetObject(id, OpenMode.ForWrite) as AttributeReference;
							string attName = ar.Tag;
							if (data.HasDataChanged)
							{
								if (data.AttributeData.ContainsKey(attName))
								{
									var attData = data.AttributeData[attName];
									ar.Invisible = !attData.Visible;
									//TODO Here update text.
									ar.TextString = attData.AdjustAttributeContent(ar.TextString);
								}
							}
						}
						// Update attributes order.
						UpdateAttributesOrder(tr, br, data.AttributeData.ToDictionary(x=>x.Key, y=>y.Value.Index));
					}
				}
				tr.Commit();
			}
		}
		private class AttributeData
		{
			public double TopPadding;
			public AttributeReference ar;

		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="newIndexes">index of element is current attribute index, value is new attribute index.</param>
		/// <param name="br"></param>
		private static void UpdateAttributesOrder(Transaction tr, BlockReference br, Dictionary<string, int> attributesIndexes)
		{
			// Update for rotated blocks (unrotate, apply, rotate back to angle).
			// Get attributes which exist in block (if user deletes something manually, we don't want to crash).
			// If there are attributes which don't have entry in attributesIndexes, handle them differently.
			// Get paddings and order by indexes.

			double angleRad = br.Rotation;
			Matrix3d rotation3d = Matrix3d.Rotation(-angleRad, Vector3d.ZAxis, br.Position);
			br.TransformBy(rotation3d);

			int length = 0;
			List<AttributeReference> refs = new List<AttributeReference>();
				foreach (ObjectId aId in br.AttributeCollection)
				{
					AttributeReference ar = tr.GetObject(aId, OpenMode.ForWrite) as AttributeReference;
					if (attributesIndexes.ContainsKey(ar.Tag))
					{
						length++;
						refs.Add(ar);
						//break;
					}
				}

			 
			AttributeData[] atts = refs.OrderByDescending(x => x.Position.Y).Select(y=>new AttributeData() { ar = y }).ToArray();

			//NOTE This doesn't work for rotated blocks!
			// Explore how to get direction (is it only blockreference rotation?)

			int index = 0;
			double lastY = 0;
			double topY = 0;
			foreach(AttributeData d in atts)
			//foreach (ObjectId aId in br.AttributeCollection)
			{
				//AttributeReference ar = tr.GetObject(aId, OpenMode.ForWrite) as AttributeReference;
				//atts[index].ar = ar;
				double currentY = d.ar.Position.Y;
				if (index > 0)
				{
					double h = lastY - currentY;
					atts[index].TopPadding= h - d.ar.Height;
				}
				else
				{
					atts[0].TopPadding = d.ar.Height * 0.3;
					topY = d.ar.Position.Y;
				}
				if (topY < d.ar.Position.Y)
					topY = d.ar.Position.Y;
				index++;
				lastY = currentY;
			}
			
			AttributeData[] old = atts.ToArray();
			foreach(AttributeData ad in old)
			{
				int j = attributesIndexes[ad.ar.Tag];
				atts[j] = ad;
			}


			/*for (int i = 0; i < atts.Length; i++)
			{
				AttributeReference ar = oldOrder[i];
				int j = attributesIndexes[ar.Tag];
				atts[i].ar = oldOrder[j];
			}*/

			double offset = 0;
			AttributeData last = null;
			bool first = true;
			foreach (AttributeData ad in atts)
			{
				AttributeReference ar = ad.ar;
				double d = ar.Height + ad.TopPadding;
				if (first)
				{
					first = false;
					d = 0;
				}
				if (ar.Invisible)
					d = 0;
				offset += d;
				if (last == null)
					ar.Position = new Point3d(ar.Position.X, topY - offset, ar.Position.Z);
			}


			rotation3d = Matrix3d.Rotation(angleRad, Vector3d.ZAxis, br.Position);
			br.TransformBy(rotation3d);
		}
	}
}
