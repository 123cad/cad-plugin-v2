﻿namespace CadPlugin.BlockAttributeManagers
{
	partial class BlockAttributeManagerView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlockAttributeManagerView));
            this.buttonSelectBlocks = new System.Windows.Forms.Button();
            this.textBoxBlockName = new System.Windows.Forms.TextBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.buttonMoveUp = new System.Windows.Forms.Button();
            this.buttonMoveDown = new System.Windows.Forms.Button();
            this.buttonApplyToSelection = new System.Windows.Forms.Button();
            this.buttonToAll = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelSelection = new System.Windows.Forms.Label();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnVisible = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColumnDecimals = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonSelectBlocks
            // 
            this.buttonSelectBlocks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSelectBlocks.Location = new System.Drawing.Point(369, 20);
            this.buttonSelectBlocks.Name = "buttonSelectBlocks";
            this.buttonSelectBlocks.Size = new System.Drawing.Size(94, 23);
            this.buttonSelectBlocks.TabIndex = 0;
            this.buttonSelectBlocks.Text = "Select blocks";
            this.buttonSelectBlocks.UseVisualStyleBackColor = true;
            this.buttonSelectBlocks.Click += new System.EventHandler(this.buttonSelectBlocks_Click);
            // 
            // textBoxBlockName
            // 
            this.textBoxBlockName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxBlockName.Location = new System.Drawing.Point(28, 23);
            this.textBoxBlockName.Name = "textBoxBlockName";
            this.textBoxBlockName.ReadOnly = true;
            this.textBoxBlockName.Size = new System.Drawing.Size(318, 20);
            this.textBoxBlockName.TabIndex = 1;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnName,
            this.ColumnVisible,
            this.ColumnDecimals});
            this.dataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView.Location = new System.Drawing.Point(28, 95);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.Size = new System.Drawing.Size(435, 178);
            this.dataGridView.TabIndex = 2;
            // 
            // buttonMoveUp
            // 
            this.buttonMoveUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMoveUp.BackgroundImage = global::CadPlugin.Properties.Resources.up_arrow1;
            this.buttonMoveUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonMoveUp.Location = new System.Drawing.Point(391, 65);
            this.buttonMoveUp.Name = "buttonMoveUp";
            this.buttonMoveUp.Size = new System.Drawing.Size(30, 24);
            this.buttonMoveUp.TabIndex = 3;
            this.buttonMoveUp.UseVisualStyleBackColor = true;
            this.buttonMoveUp.Click += new System.EventHandler(this.buttonMoveUp_Click);
            // 
            // buttonMoveDown
            // 
            this.buttonMoveDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMoveDown.BackgroundImage = global::CadPlugin.Properties.Resources.down_arrow1;
            this.buttonMoveDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonMoveDown.Location = new System.Drawing.Point(430, 65);
            this.buttonMoveDown.Name = "buttonMoveDown";
            this.buttonMoveDown.Size = new System.Drawing.Size(33, 24);
            this.buttonMoveDown.TabIndex = 4;
            this.buttonMoveDown.UseVisualStyleBackColor = true;
            this.buttonMoveDown.Click += new System.EventHandler(this.buttonMoveDown_Click);
            // 
            // buttonApplyToSelection
            // 
            this.buttonApplyToSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonApplyToSelection.Location = new System.Drawing.Point(280, 322);
            this.buttonApplyToSelection.Name = "buttonApplyToSelection";
            this.buttonApplyToSelection.Size = new System.Drawing.Size(100, 23);
            this.buttonApplyToSelection.TabIndex = 5;
            this.buttonApplyToSelection.Text = "Apply selection";
            this.buttonApplyToSelection.UseVisualStyleBackColor = true;
            this.buttonApplyToSelection.Click += new System.EventHandler(this.buttonApplyToSelection_Click);
            // 
            // buttonToAll
            // 
            this.buttonToAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonToAll.Location = new System.Drawing.Point(280, 293);
            this.buttonToAll.Name = "buttonToAll";
            this.buttonToAll.Size = new System.Drawing.Size(100, 23);
            this.buttonToAll.TabIndex = 6;
            this.buttonToAll.Text = "Apply to all";
            this.buttonToAll.UseVisualStyleBackColor = true;
            this.buttonToAll.Click += new System.EventHandler(this.buttonToAll_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(388, 322);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 7;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // labelSelection
            // 
            this.labelSelection.AutoSize = true;
            this.labelSelection.Location = new System.Drawing.Point(41, 54);
            this.labelSelection.Name = "labelSelection";
            this.labelSelection.Size = new System.Drawing.Size(35, 13);
            this.labelSelection.TabIndex = 8;
            this.labelSelection.Text = "label1";
            // 
            // ColumnName
            // 
            this.ColumnName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnName.HeaderText = "Name";
            this.ColumnName.Name = "ColumnName";
            this.ColumnName.ReadOnly = true;
            // 
            // ColumnVisible
            // 
            this.ColumnVisible.HeaderText = "Visible";
            this.ColumnVisible.Name = "ColumnVisible";
            this.ColumnVisible.Width = 60;
            // 
            // ColumnDecimals
            // 
            this.ColumnDecimals.HeaderText = "Decimals";
            this.ColumnDecimals.Name = "ColumnDecimals";
            this.ColumnDecimals.Width = 60;
            // 
            // BlockAttributeManagerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 364);
            this.Controls.Add(this.labelSelection);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonToAll);
            this.Controls.Add(this.buttonApplyToSelection);
            this.Controls.Add(this.buttonMoveDown);
            this.Controls.Add(this.buttonMoveUp);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.textBoxBlockName);
            this.Controls.Add(this.buttonSelectBlocks);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BlockAttributeManagerView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "123CAD Block Attribute Manager";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button buttonSelectBlocks;
		private System.Windows.Forms.TextBox textBoxBlockName;
		private System.Windows.Forms.DataGridView dataGridView;
		private System.Windows.Forms.Button buttonMoveUp;
		private System.Windows.Forms.Button buttonMoveDown;
		private System.Windows.Forms.Button buttonApplyToSelection;
		private System.Windows.Forms.Button buttonToAll;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.Label labelSelection;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnVisible;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnDecimals;
    }
}