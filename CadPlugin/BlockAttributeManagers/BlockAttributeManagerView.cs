﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CadPlugin.Common;
using System.Text.RegularExpressions;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.BlockAttributeManagers
{
	public partial class BlockAttributeManagerView : Form
	{
		public class SingleAttributeData
		{
			public string Name { get; private set; }
			public bool Visible { get; set; }
			public int Index { get; set; }
			/// <summary>
			/// Indicates if in attribute there is decimal number which should be adjusted.
			/// String can contain anything, only number will be checked.
			/// </summary>
			public bool ContainsDecimalNumber { get; private set; }
			/// <summary>
			/// Number of decimal places in attribute in initial block
			/// </summary>
			public int InitialDecimalPlaces { get; private set; }
			/// <summary>
			/// Decimal places that user has selected.
			/// </summary>
			public int DecimalPlaces { get; private set; }

			public SingleAttributeData(string name)
			{
				Name = name;
			}
			public SingleAttributeData(string name, bool visible)
			{
				Name = name;
				Visible = visible;
			}
			public SingleAttributeData(string name, bool visible, int index)
			{
				Name = name;
				Visible = visible;
				Index = index;
			}
			/// <summary>
			/// Initial content which determines type of attribute.
			/// </summary>
			/// <param name="content"></param>
			public void InitializeWithContent(string content)
			{
                if (Name == "BEZEICHNUNG")
                {
                    ContainsDecimalNumber = false;
                    return;
                }
				ContainsDecimalNumber = false;
				string number = GetNumberString(content);

				double d;
				if (System.DoubleHelper.TryParseDoubleInvariant(number, out d))
				{
					ContainsDecimalNumber = true;
					InitialDecimalPlaces = number.Substring(number.IndexOf('.') + 1).Length;
					DecimalPlaces = InitialDecimalPlaces;
				}

			}
			public void SetDecimals(int val)
			{
				if (!ContainsDecimalNumber)
					return;
				int d;
				DecimalPlaces = val;
			}
			public string AdjustAttributeContent(string content)
			{
				if (!ContainsDecimalNumber)
					return content;
				string adjusted = content;
				string number = GetNumberString(content);

				double d;
				if (System.DoubleHelper.TryParseDoubleInvariant(number, out d))
				{
					adjusted = ReplaceNumberString(content, d);
				}
				return adjusted;
			}
			/// <summary>
			/// Returns number string if it is contains inside content.
			/// </summary>
			/// <param name="content"></param>
			/// <returns></returns>
			private string GetNumberString(string content)
			{
				string pattern = "([0-9]+.?[0-9]+)";
				Regex r = new Regex(pattern);
				var match = r.Match(content);
				string number = match.Value;
				return number;
			}
			/// <summary>
			/// Replaces number in content string with provided value.
			/// </summary>
			/// <param name="content"></param>
			/// <param name="value"></param>
			/// <returns></returns>
			private string ReplaceNumberString(string content, double value)
			{
				string pattern = "([0-9]+.?[0-9]+)";
				Regex r = new Regex(pattern);
				string s = System.DoubleHelper.ToStringInvariant(value, DecimalPlaces);
				if (DecimalPlaces == 0)
					s = ((int)value).ToString();
				string adjusted = r.Replace(content, s);
				return adjusted;
			}
		}
		public class Data
		{
			/// <summary>
			/// Name of the selected block.
			/// </summary>
			public string BlockName { get; set; }
			/// <summary>
			/// (name, visible) - defines if attribute with given name is visible.
			/// </summary>
			//public Dictionary<string, bool> AttributeVisibility = new Dictionary<string, bool>();
			/// <summary>
			/// (name, index) - defines index of 
			/// </summary>
			//public Dictionary<string, int> AttributeOrder = new Dictionary<string, int>();
			public Dictionary<string, SingleAttributeData> AttributeData = new Dictionary<string, SingleAttributeData>();

			/// <summary>
			/// Attribute name, decimal places (-1 means not supported).
			/// </summary>
			public Dictionary<string, int> AttributeDecimalPlaces = new Dictionary<string, int>();
			/// <summary>
			/// Blocks which user has selected.
			/// </summary>
			public List<ObjectId> selectedBlocks;

			/// <summary>
			/// Tells if some data has been changed (except attributes order).
			/// </summary>
			public bool HasDataChanged = false;
			/// <summary>
			/// Tells if order of attributes has been changed.
			/// </summary>
			public bool HasOrderChanged = false;

			public bool ApplyToAll = false;
		}
		
		class DecimalsSource
		{
			private string[] decimals = new string[]
			{
				"-",
				"0.0", 
				"0.00",
				"0.000",
				"0.0000",
			};
			public string[] GetAllDecimals()
			{
				return decimals;
			}
			public string GetFormat(int decimalCount)
			{
				int i = decimalCount;
				if (i >= decimals.Length)
					i = decimals.Length - 1;
				else if (i < 0)
					i = 0;
				return decimals[i];
			}
			public int GetDecimalsPlaces(string format)
			{
				string s = null;
				int count = 0;
				if (format == decimals[0])
					count = 0;
				else
					foreach (var dec in decimals)
						if (dec == format)
						{
							s = dec;
							count = dec.Length - 2;
							break;
						}
				return count;
			}
		}

		public Data data { get; }

		private DecimalsSource decimalMgr = new DecimalsSource();
		private Document doc;
		public BlockAttributeManagerView(Document doc)
		{
			InitializeComponent();
			labelSelection.Text = "";
			this.doc = doc;
			data = new Data();
			dataGridView.CellValueChanged += (_, __) => { data.HasDataChanged = true; };

			buttonSelectBlocks_Click(null, null);

			dataGridView.DataError += DataGridView_DataError;

			ColumnDecimals.Items.AddRange(decimalMgr.GetAllDecimals());
		}

		private void DataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
		{

		}

		/// <summary>
		/// BlockReference which serves as prototype for decimal places.
		/// </summary>
		/// <param name="br"></param>
		private void InitializeWith(BlockReference br, IEnumerable<AttributeReference> atts)
		{
			data.AttributeData.Clear();
			int index = 0;
			foreach(var att in atts)
			{
				var attData = new SingleAttributeData(att.Tag, !att.Invisible, index++);
				data.AttributeData.Add(att.Tag, attData);
				attData.InitializeWithContent(att.TextString);
            }
		}
		private void buttonSelectBlocks_Click(object sender, EventArgs e)
		{
			var v = Common.CADSelectionHelper.SelectBlockReferences();
			if (v != null && v.Count > 0)
			{
				dataGridView.Rows.Clear();
				data.selectedBlocks = new List<ObjectId>();
				string blockName = null;
				using (Transaction tr = doc.Database.TransactionManager.StartTransaction())
				{
					foreach(ObjectId bId in v)
					{
						BlockReference br = tr.GetObject(bId, OpenMode.ForRead) as BlockReference;
						if (br == null)
							continue;
						if (blockName == null)
						{
							blockName = br.Name;
							textBoxBlockName.Text = blockName;
							data.BlockName = blockName;
						}
						if (blockName == br.Name)
							data.selectedBlocks.Add(bId);
					}

					BlockReference first = tr.GetObject(data.selectedBlocks[0], OpenMode.ForWrite) as BlockReference;
					double angleRad = first.Rotation;
					Matrix3d rotation3d = Matrix3d.Rotation(-angleRad, Vector3d.ZAxis, first.Position);
					first.TransformBy(rotation3d);

					List<AttributeReference> refs = new List<AttributeReference>();
					foreach (ObjectId aId in first.AttributeCollection)
					{
						AttributeReference ar = tr.GetObject(aId, OpenMode.ForRead) as AttributeReference;
						refs.Add(ar);
					}

					refs = refs.OrderByDescending(x => x.Position.Y).ToList();
					InitializeWith(first, refs);

					foreach (AttributeReference ar in refs)
					{
						var attData = data.AttributeData[ar.Tag];
						string decimals = decimalMgr.GetFormat(attData.DecimalPlaces);
						int rowIndex = dataGridView.Rows.Add(attData.Name, attData.Visible, decimals);
						dataGridView.Rows[rowIndex].Tag = ar.ObjectId;
					}

					rotation3d = Matrix3d.Rotation(angleRad, Vector3d.ZAxis, first.Position);
					first.TransformBy(rotation3d);
					first.DowngradeOpen();
					tr.Commit();
				}
			}
		}

		private void buttonMoveUp_Click(object sender, EventArgs e)
		{
			int rowIndex = dataGridView.CurrentCell.RowIndex;
			if (rowIndex < 1)
				return;
			data.HasOrderChanged = true;
			DataGridViewRow row = dataGridView.Rows[rowIndex - 1];
			dataGridView.Rows.RemoveAt(rowIndex - 1);
			dataGridView.Rows.Insert(rowIndex, row);
			dataGridView.CurrentCell = dataGridView[dataGridView.CurrentCell.ColumnIndex, rowIndex - 1];
		}

		private void buttonMoveDown_Click(object sender, EventArgs e)
		{
			int rowIndex = dataGridView.CurrentCell.RowIndex;
			if (rowIndex > dataGridView.Rows.Count - 2)
				return;
			data.HasOrderChanged = true;
			DataGridViewRow row = dataGridView.Rows[rowIndex];
			dataGridView.Rows.RemoveAt(rowIndex);
			dataGridView.Rows.Insert(rowIndex + 1, row);
			dataGridView.CurrentCell = dataGridView[dataGridView.CurrentCell.ColumnIndex, rowIndex + 1];
		}
		public void ApplyAttributeVisibility(Transaction tr, BlockReference br, Dictionary<string, bool> visibleMask)
		{
			foreach (ObjectId aId in br.AttributeCollection)
			{
				AttributeReference ar = tr.GetObject(aId, OpenMode.ForWrite) as AttributeReference;
				if (visibleMask.ContainsKey(ar.Tag))
					ar.Invisible = !visibleMask[ar.Tag];
			}
		}
		private void buttonToAll_Click(object sender, EventArgs e)
		{
			SaveData();
			data.ApplyToAll = true;
			DialogResult = DialogResult.OK;
			Close();
		}
		
		private void SaveData()
		{
			foreach (DataGridViewRow row in dataGridView.Rows)
			{
				if (row.Tag == null)
					continue;

				string attTag = (string)row.Cells[ColumnName.Index].Value;
				bool visible = (bool)row.Cells[ColumnVisible.Index].Value;
				var decimals = (string)row.Cells[ColumnDecimals.Index].Value;

				var attData = data.AttributeData[attTag];
				attData.Visible = visible;
				attData.Index = row.Index;
				attData.SetDecimals(decimalMgr.GetDecimalsPlaces(decimals));
			}
		}
		private void buttonApplyToSelection_Click(object sender, EventArgs e)
		{
			SaveData();
			data.ApplyToAll = false;
			DialogResult = DialogResult.OK;
			Close();
		}
		

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
			Close();
		}
	}
}
