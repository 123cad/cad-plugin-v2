﻿
using System;
using System.IO;
using System.Reflection;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Globalization;
using System.Diagnostics;
using MyLog;
using System.Collections.Generic;
using Microsoft.Win32;
using System.Threading;
using System.Threading.Tasks;
using wf = System.Windows.Forms;
using SharedUtilities.GlobalDependencyInjection;
using CadPlugin.Common;

#if BRICSCAD
using Teigha.Runtime;
using Bricscad.ApplicationServices;
using Teigha.DatabaseServices;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using At = BlocksAttributes;
using Autodesk.AutoCAD.DatabaseServices;
#endif


// This line is not mandatory, but improves loading performances
[assembly: ExtensionApplication(typeof(CadPlugin.MyPlugin))]

namespace CadPlugin
{

	// This class is instantiated by AutoCAD once and kept alive for the  
	// duration of the session. If you don't do any one time initialization 
	// then you should remove this class.
	public partial class MyPlugin : IExtensionApplication
	{
		private static Assembly inventoryAssembly = null;
		private static Assembly dynamicCurvesAssembly = null;

		/// <summary>
		/// Some unclear deadlocks occured while doing LoadFrom (and attempt to run init task in MyLog), so MyLog
		/// will be loaded later, and messages written after loading everything.
		/// </summary>
		private static List<string> loadedAssemblies;
		static MyPlugin()
		{




			loadedAssemblies = new List<string>();
            // Log all fatal exceptions.
            // Not possible in autocad (some exception rises).
            /*
            System.Windows.Forms.Application.SetUnhandledExceptionMode(System.Windows.Forms.UnhandledExceptionMode.CatchException);
            System.Windows.Forms.Application.ThreadException += (_, e) => { MyLog.MyTraceSource.Error("Unhandled UI thread exception. " + e.Exception); };
            AppDomain.CurrentDomain.UnhandledException += (_, e) => { MyTraceSource.Current.Error("Unhandled non UI thread exception" + e.ExceptionObject); };
            //*/

			AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;

#if DEBUG
			//IMPORTANT!!! If needed, use this only when debugging, not for production!!!
			// It causes, if exception is thrown even inside try/catch block, program to hang.
			// In specific case, initializing MyLog, when probing for files (IOException was thrown, but not caught by
			// catch/finally block, even if event handler for this was empty!!!)
			//AppDomain.CurrentDomain.FirstChanceException += CurrentDomain_FirstChanceException;
			//AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
#endif
			AppDomain.CurrentDomain.AssemblyLoad += CurrentDomain_AssemblyLoad;

			System.Windows.Application.ResourceAssembly = Assembly.GetExecutingAssembly();

			LoadModules();
			AppDomain.CurrentDomain.AssemblyLoad -= CurrentDomain_AssemblyLoad;
			AppDomain.CurrentDomain.AssemblyResolve -= CurrentDomain_AssemblyResolve;

			FlushMessages();


			// Load DependencyInjection.
			GlobalDI.LoadModule<DIModule>();
			GlobalDI.LoadModuleCad<DIModuleCad>();
		}


        private static void FlushMessages()
		{
			MyLog.MyTraceSource.Initialize();
			string[] ls = loadedAssemblies.ToArray();
			foreach (string s in ls)
				MyLog.MyTraceSource.Information(s);
			//loadedAssemblies.Clear();
		}

		private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
		{
			if (args.Name.Contains("MyLog"))
			{
				string dir = GetLibrariesLocation();
				string dll = dir + @"\MyLog.dll";
				return Assembly.LoadFrom(dll);
			}
			return null;
		}

		private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			//Debug.WriteLine("Unhandled exception: " + e.ExceptionObject);
			wf.MessageBox.Show("EXCEPTION " + e);
			//MyTraceSource.Current.Critical("Unhandled exception: " + e.ExceptionObject);
		}

		private static void CurrentDomain_FirstChanceException(object sender, System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs e)
		{
			Debug.WriteLine("First change exception" + e.Exception);
		}
		private static void CurrentDomain_AssemblyLoad(object sender, AssemblyLoadEventArgs args)
		{
			loadedAssemblies.Add("Time: " + DateTime.Now + "Asembly loaded: " + args.LoadedAssembly.FullName);
			//MyLog.MyTraceSourceTraceInformation("Asembly loaded: " + args.LoadedAssembly.FullName);
		}

		void IExtensionApplication.Initialize()
		{
			//DI.Kernel.Bind<TaskScheduler>().ToConstant(TaskScheduler.FromCurrentSynchronizationContext());

#if RELEASE
            /*var culture = new CultureInfo("de-DE");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;*/
#endif

            Application.DocumentManager.DocumentCreated += DocumentManager_DocumentCreated;
			foreach (Document doc in Application.DocumentManager)
			{
				InitiDocument(doc);
			}

			var v = new BlocksAttributes.AttributePlugin();
			v.Initialize();

			Commands.HatchToolbar.ApplicationInitialized();
			Commands.LinetypeToolbar.ApplicationInitialized();

			// Remove tests from this assembly, so they doesn't ship together.
			Tests.TestManager.RunTests();

			LoadCui();



		}

		/// <summary>
		/// Automatically load cui file on start.
		/// </summary>
		public static void LoadCui()
		{
			try
			{
				// Cui is loaded automatically only first time 123CAD dll is loaded on computer.
				using (Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\123cad", true))
				{
					object val = key.GetValue("CuiLoaded");
					if (val != null)
						return;

					key.SetValue("CuiLoaded", 0);
					string support = key.GetValue("LocationDir") as string;
					string file = support + "Support\\";
					// If menugroup already exists, skip this step
#if AUTOCAD
				file += "123cad " + Version.Major + "." + Version.Minor + "-black.cuix";
				Application.LoadPartialMenu(file);
#endif
#if BRICSCAD
					file += "123cad " + Version.Major + "." + Version.Minor + "-white.cui";
					file = file.Replace(@"\", "\\\\");
					Application.DocumentManager.MdiActiveDocument.SendStringToExecute("(command \"_.cuiload\" \"" + file + "\")\n", false, false, false);
#endif
				}
			}
			catch (System.Exception e)
			{
				MyLog.MyTraceSource.Warning("Unable to load cui file. " + e.Message);
			}
		}


		private void Database_ObjectErased(object sender, ObjectErasedEventArgs e)
		{
			HatchToolbars.HatchPlugin.ObjectErased(e.DBObject);
			LinetypeToolbars.LinetypePlugin.ObjectErased(e.DBObject);
		}

		private void MdiActiveDocument_CommandEnded(object sender, CommandEventArgs e)
		{
			// This is used, because in acad it is impossible to start transaction in ObjectModified event.
			/*
			 * If exception occurs here, CAD will crash!
			 * (if object is not opened for write, when write is done, for example)
			 */
			HatchToolbars.HatchPlugin.ApplyUpdates();
			LinetypeToolbars.LinetypePlugin.ApplyUpdates();
		}

		private void Database_ObjectModified(object sender, ObjectEventArgs e)
		{
			HatchToolbars.HatchPlugin.ObjectUpdated(e.DBObject);
			LinetypeToolbars.LinetypePlugin.ObjectUpdated(e.DBObject);
		}
		private void InitiDocument(Document doc)
		{
			doc.Database.ObjectModified += Database_ObjectModified;
			doc.Database.ObjectErased += Database_ObjectErased;
			doc.CommandEnded += MyCommands.MdiActiveDocument_CommandEnded;
			doc.CommandEnded += MdiActiveDocument_CommandEnded;
			//doc.CommandEnded += (e, o) => HatchToolbars.HatchPlugin.CommandEnded(o.GlobalCommandName);
			// doc.Database.BeginDeepClone += Database_BeginDeepClone;
			// doc.Database.BeginDeepCloneTranslation += (_, __) => Debug.WriteLine("deep translation");
			// doc.Database.DeepCloneEnded += (_, __) => Debug.WriteLine("deep ended");
			// doc.Database.BeginInsert += (_, __) => Debug.WriteLine("insert started");
			// doc.Database.BeginWblockSelectedObjects += (_, __) => Debug.WriteLine("wblock");
		}
		void DocumentManager_DocumentCreated(object sender, DocumentCollectionEventArgs e)
		{
			InitiDocument(e.Document);
		}
		void IExtensionApplication.Terminate()
		{
			try
			{
				if (inventoryAssembly != null)
				{
					//Application.DocumentManager.MdiActiveDocument.CommandEnded -= MyCommands.MdiActiveDocument_CommandEnded;
					Type t = inventoryAssembly.GetType("CadPlugin_Compiled.Sewage");
					MethodInfo dispose = t.GetMethod("Dispose", BindingFlags.Public | BindingFlags.Static);
					dispose.Invoke(null, null);
					inventoryAssembly = null;
				}
			}
			catch (System.Exception)
			{

			}
			try
			{
				if (dynamicCurvesAssembly != null)
				{
					Type t = inventoryAssembly.GetType("CadPlugin_Compiled.DynamicCurves");
					MethodInfo dispose = t.GetMethod("Dispose", BindingFlags.Public | BindingFlags.Static);
					dispose.Invoke(null, null);
					inventoryAssembly = null;
				}
			}
			catch (System.Exception)
			{

			}
		}

	}

}