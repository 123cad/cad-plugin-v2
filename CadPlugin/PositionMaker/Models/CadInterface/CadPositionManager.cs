﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.PositionMaker.Views.MainView;
using CadPlugin.Common;
using CadPlugin.PositionMaker.Models.CadInterface.SelectionGroups;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.PositionMaker.Models.CadInterface
{
	class CadPositionManager
	{

		public static void Start(Document doc)
		{
			MainForm form = new MainForm();
			MainController ctrl = new MainController(form);
			ctrl.SelectObjects += (e)=> Ctrl_SelectObjects(e, doc);
			form.ShowDialog();
		}
		

		private static void Ctrl_SelectObjects(SelectObjectsEventArgs obj, Document doc)
		{
			Database db = doc.Database;
			Editor ed = doc.Editor;

			ISelectionGroup group = null;
			string message = "";
			switch (obj.Group)
			{
				case CadObjectGroup.Count:
					group = new CountingGroup();
					break;
				case CadObjectGroup.Length:
					group = new LengthGroup();
					break;
				case CadObjectGroup.Area:
					group = new AreaGroup();
					break;
			}
			if (group == null)
				return;

			ed.WriteMessage(message + "\n");
			PromptSelectionResult res = ed.GetSelection(group.GetFilter());
			if (res.Status == PromptStatus.OK)
			{
				using (Transaction tr = db.TransactionManager.StartTransaction())
				{
					TransactionData data = new TransactionData(doc, tr);
					var objs = res.Value.GetObjectIds();
					double d = group.PerformCalculation(data, objs);
					obj.TotalObjectsSelected = objs.Length;
					obj.TotalSum = d;
				}
			}
		}
	}
}
