﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PositionMaker.Models.CadInterface
{
	enum CadObjectGroup
	{
		/// <summary>
		/// Counts objects.
		/// </summary>
		Count,
		/// <summary>
		/// Calculates legnth of objects.
		/// </summary>
		Length,
		/// <summary>
		/// Calculates area of objects.
		/// </summary>
		Area
	}
	class SelectObjectsEventArgs
	{
		public CadObjectGroup Group { get; }
		/// <summary>
		/// Total sum of desired Group values.
		/// </summary>
		public double TotalSum { get; set; }
		/// <summary>
		/// Total number of objects which sum has been used.
		/// </summary>
		public int TotalObjectsSelected { get; set; }
		public SelectObjectsEventArgs(CadObjectGroup g)
		{
			Group = g;
		}
	}
}
