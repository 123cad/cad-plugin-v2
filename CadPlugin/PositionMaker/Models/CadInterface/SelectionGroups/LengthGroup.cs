﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.PositionMaker.Models.CadInterface.SelectionGroups
{
	class LengthGroup : ISelectionGroup
	{
		public string DisplayMessage { get; } = "Select Line and Polyline objects for length calculation:";
		public SelectionFilter GetFilter()
		{
			int polylineState = (int)PolylineState.Opened;
			TypedValue[] vals = new TypedValue[]
			{
				new TypedValue((int)DxfCode.Operator, "<or"),

				new TypedValue((int)DxfCode.Start, "LINE"),

				// This catches closed Polyline object.
				new TypedValue((int)DxfCode.Operator, "<and"),
				new TypedValue((int)DxfCode.Start, "LWPOLYLINE"),
				new TypedValue(70, polylineState),
				new TypedValue((int)DxfCode.Operator, "and>"),

				// This catches closed polyline 2d or 3d object.
				new TypedValue((int)DxfCode.Operator, "<and"),
				new TypedValue((int)DxfCode.Start, "POLYLINE"),
				new TypedValue((int)DxfCode.Operator, "<or"),
				// Closed Polyline2d object.
				new TypedValue(70, polylineState),
				// Closed Polyline3d object.
				new TypedValue(70, 8 | polylineState),
				new TypedValue((int)DxfCode.Operator, "or>"),
				new TypedValue((int)DxfCode.Operator, "and>"),

				new TypedValue((int)DxfCode.Operator, "or>"),
			};
			SelectionFilter f = new SelectionFilter(vals);
			return f;
		}

		public double PerformCalculation(TransactionData data, IEnumerable<ObjectId> objects)
		{
			double lenght = 0;
			foreach(var oid in objects)
			{
				DBObject o = data.Tr.GetObjectRead<DBObject>(oid);
				Curve c = o as Curve;
				lenght += c.GetDistanceAtParameter(c.EndParam);
			}

			return lenght;
		}
	}
}
