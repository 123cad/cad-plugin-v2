﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif


namespace CadPlugin.PositionMaker.Models.CadInterface.SelectionGroups
{
	class CountingGroup : ISelectionGroup
	{
		public string DisplayMessage { get; } = "Select Point and Circle objects for counting:";
		public SelectionFilter GetFilter()
		{
			TypedValue[] vals = new TypedValue[]
			{
				new TypedValue((int)DxfCode.Operator, "<or"),
				new TypedValue((int)DxfCode.Start, "POINT"),
				new TypedValue((int)DxfCode.Start, "CIRCLE"),
				new TypedValue((int)DxfCode.Operator, "or>"),
			};
			SelectionFilter f = new SelectionFilter(vals);
			return f;
		}

		public double PerformCalculation(TransactionData data, IEnumerable<ObjectId> objects)
		{
			int count = objects.Count();
			
			return count;
		}
	}
}
