﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.PositionMaker.Models.CadInterface.SelectionGroups
{
	/// <summary>
	/// Interface for CadObjectGroup enums - each one has its own class.
	/// Used to encapsulate filter and calculation data (which depends on supported objects).
	/// </summary>
	interface ISelectionGroup
	{
		/// <summary>
		/// Message which is displayed before selection.
		/// </summary>
		string DisplayMessage { get; }
		/// <summary>
		/// Gets filter of objects which will be selected.
		/// </summary>
		/// <returns></returns>
		SelectionFilter GetFilter();
		/// <summary>
		/// Performs desired calculation on provided objects.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="objects"></param>
		/// <returns></returns>
		double PerformCalculation(TransactionData data, IEnumerable<ObjectId> objects);
	}

	enum PolylineState
	{
		Opened = 0,
		Closed = 1
	}	
}
