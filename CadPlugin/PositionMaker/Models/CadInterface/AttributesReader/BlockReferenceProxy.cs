﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.PositionMaker.Models.CadInterface.AttributesReader
{
    /// <summary>
    /// Improves performance of multiple Attribute reading from BlockReference.
    /// (read all attributes first time, and return existing results)
    /// Note: changing attribute value directly on BlockReference breaks usability of this proxy (unless
    /// event handler is attached to database - for now no need for this).
    /// </summary>
    class BlockReferenceProxy
    {
        public BlockReference Br { get; private set; }
        public Transaction Tr { get; private set; }
        private Dictionary<string, string> attributes { get; } = new Dictionary<string, string>();
        private bool attributesAreFetched = false;

        public BlockReferenceProxy(BlockReference br, Transaction tr)
        {
            Br = br;
            Tr = tr;
        }

        public string GetAttributeValue(string attribute)
        {
            if (!attributesAreFetched)
            {
                var dir = Br.ReadAllBlockAttributes(Tr);
                foreach (var p in dir)
                    attributes[p.Key] = p.Value;
            }
            attributesAreFetched = true;

            if (attributes.ContainsKey(attribute))
                return attributes[attribute];

            return null;
        }

    }
}
