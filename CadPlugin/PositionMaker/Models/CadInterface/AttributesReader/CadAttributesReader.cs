﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.PositionMaker.Models.CadInterface.AttributesReader
{
    class CadAttributesReader
    {
        public static readonly string AttributePosition = "POS-NR";
        public static readonly string AttributeQuantity = "QUANTITY";
        /// <summary>
        /// Returns collection of BlockReference which contain position attribute.
        /// </summary>
        /// <param name="tr"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private static IEnumerable<BlockReference> GetPositionBlocks(Transaction tr, Database db)
        {
            List<BlockReference> references = new List<BlockReference>();
            IEnumerable<BlockTableRecord> blocks = CADBlockHelper.GetAllBlockDefinitions(tr, db);
            // Check if attributes contain "POS-NR"
            foreach(var btr in blocks)
            {
                if (!CADBlockHelper.DoesAttributeExist(tr, btr, AttributePosition))
                    continue;
                foreach(ObjectId oid in btr.GetBlockReferenceIds(true, false))
                {
                    BlockReference br = tr.GetObjectRead<BlockReference>(oid);
                    references.Add(br);
                }
            }
            return references;
        }
        public static void Process(Document doc, IEnumerable<CadPositionData> positions)
        {
            using (Transaction tr = doc.Database.TransactionManager.StartTransaction())
            {
                TransactionData data = new TransactionData(doc, tr);
                //string[] blockNames = new string[]
                //{
                //"3D_Body_50cm",
                //"3D_Body_100cm",
                //"3D_Ring_05cm"
                //};
                //List<ObjectId> blockIds = new List<ObjectId>();
                //for (int i = 0; i < blockNames.Length; i++)
                //    blockIds.AddRange(Common.CADBlockHelper.GetAllBlocksForName(data, blockNames[i]));
                //List<BlockReferenceProxy> brs = new List<BlockReferenceProxy>();
                //
                //foreach (ObjectId oid in blockIds)
                //{
                //    BlockReference br = tr.GetObjectRead<BlockReference>(oid);
                //    brs.Add(new BlockReferenceProxy(br, tr));
                //}
                List<BlockReferenceProxy> brs = GetPositionBlocks(tr, doc.Database).Select(x => new BlockReferenceProxy(x, tr)).ToList();
                Dictionary<string, List<BlockReferenceProxy>> positionBlocks = new Dictionary<string, List<BlockReferenceProxy>>();
                foreach (BlockReferenceProxy brp in brs)
                {
                    string position = brp.GetAttributeValue(AttributePosition);
                    if (string.IsNullOrEmpty(position))
                        continue;
                    if (!positionBlocks.ContainsKey(position))
                        positionBlocks.Add(position, new List<BlockReferenceProxy>());
                    positionBlocks[position].Add(brp);
                }

                foreach (CadPositionData d in positions)
                {
                    if (!positionBlocks.ContainsKey(d.Position))
                        continue;
                    foreach (var p in positionBlocks[d.Position])
                    {
                        double m = 0;
                        if (!d.AttributeReader.GetQuantity(p, ref m))
                            continue;
                        d.Quantity += m;
                        d.TotalObjects++;
                    }
                }
            }
        }
    }
}
