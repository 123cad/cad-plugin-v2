﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.PositionMaker.Models.CadInterface.AttributesReader
{
    class AttributeReader
    {
        public bool GetQuantity(BlockReferenceProxy br, ref double quantity)
        {
            string q = br.GetAttributeValue(CadAttributesReader.AttributeQuantity);
            if (string.IsNullOrEmpty(q))
                return false;
            double d;
            if (!DoubleHelper.TryParseInvariant(q, out d))
                return false;
            quantity = d;
            return true;
        }
    }    
}
