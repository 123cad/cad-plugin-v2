﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PositionMaker.Models.CadInterface.AttributesReader
{
    class CadPositionData
    {
        public string Position { get; }
        public double Quantity { get; set; }
        public int TotalObjects { get; set; }

        public AttributeReader AttributeReader { get; set; }

        public CadPositionData(string name)
        {
            Position = name;
        }
    }
}
