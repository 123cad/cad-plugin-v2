﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventory.Positions;
using CadPlugin.PositionMaker.Models.CadInterface.AttributesReader;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.PositionMaker.Models.CadInterface
{
    class PositionReader
    {
        public static void ReadPositions(IEnumerable<SegmentData> data)
        {
            int total = data.Count();
            CadPositionData[] dataPos = new CadPositionData[total];
            var srcArray = data.ToArray();
            for (int i = 0; i < total; i++)
            {
                SegmentData d = srcArray[i];
                AttributeReader attRd = new AttributeReader();
                CadPositionData pd = new CadPositionData(d.GetPositionId())
                {
                    AttributeReader = attRd
                };
                dataPos[i] = pd;
            }
            // Invoke calculator
            CadAttributesReader.Process(Application.DocumentManager.MdiActiveDocument, dataPos);

            // Apply results.
            for (int i = 0; i < total; i++)
            {
                if (dataPos[i] == null)
                    continue;
                CadPositionData pd = dataPos[i];
                srcArray[i].Quantity = pd.Quantity;
            }
        }
    }
}
