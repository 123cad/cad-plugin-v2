﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PositionMaker.Models
{
	public enum Unit
	{
		NotAUnit,
		/// <summary>
		/// m^2
		/// </summary>
		Area,
		/// <summary>
		/// /
		/// </summary>
		Count,
		/// <summary>
		/// m
		/// </summary>
		Length, 

	}
}
