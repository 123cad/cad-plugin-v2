﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Inventory.Positions;

namespace CadPlugin.PositionMaker.Models.Importing
{
	class D8xFormatException : MyExceptions.Exception
	{
		public D8xFormatException(string msg) : base(msg) { }
	}
	/// <summary>
	/// Imports DA83 file.
	/// </summary>
	class D8X
	{
		/* Every row starts with 2 digits code. It defines data in the row.
		 * T0-T9 - start and end commend section. T1 is comment.
		 *	00 - start of position data.
		 *	01 - service description 2-42; date:43-50
		 *	02 - project name
		 *	03 - client name
		 *	04 - ?????
		 *	08 - currency used in the file.
		 *	11 - start of the group (11223333 - digits organized by segments) When
		 *		only 1111, this means start of main group, 111122 means start of subgroup... 
		 *		Not all groups are required to exist, but at least 2 are used (in the GAEB viewer, 
		 *		3 groups are supported, and can contain any character).
		 *	12 - name of the group
		 *	21 - position number, quantity (24-34, 3 decimals) and unit of measurement(35...) .
		 *	22,23 - position number, unit price (13-23, 2 decimals), total price - quantity * unit price - (25-36, 1 decimal).
		 *			If value is pauschal, unit price is not displayed.
		 *	
		 *	25 - Title
		 *	26 - Description
		 * 
		 */ 
		public static PositionsManager LoadFromFile(string fileName)
		{
			using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))

			using (StreamReader rd = new StreamReader(fs, Encoding.GetEncoding("iso-8859-1")))
			{
				bool dataStarted = false;
				PositionsManager manager = new PositionsManager();
				int lineIndex = 0;
				SegmentNode currentNode = null;
				SegmentData currentData = null;
				Dictionary<string, SegmentNode> nodes = new Dictionary<string, SegmentNode>();
				while (!rd.EndOfStream)
				{
					string line = rd.ReadLine();
					line = FixEncodingIssues(line);
					lineIndex++;
					if (line.Length < 2)
						continue;
					string code = line.Substring(0, 2);
					//if (!dataStarted && code != "00")
					//continue;
					//else
					//dataStarted = true;
					switch (code)
					{
						default: continue;
						case "T1":
							manager.ProjectDescription += line.Substring(2, 72) + "\r\n";
							break;
						case "T9":
							manager.ProjectDescription += "\r\n";
							break;
						case "01":
							{
								string s = line.Substring(2, 40).Trim();
								s = s.Trim();
								manager.ServiceInfo = s;
								DateTime dt;
								string dateString = line.Substring(42, 8);

								if (DateTime.TryParseExact(dateString, "dd.MM.yy", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None, out dt))
									manager.ProjectDate = dt;
							}
							break;
						case "02":
							{
								string s = line.Substring(2, 72).TrimEnd();
								s = s.Trim();
								manager.ProjectName = s;
							}
							break;
						case "03":
							{
								string s = line.Substring(2, 72).TrimEnd();
								s = s.Trim();
								manager.PurchaserName = s;
							}
							break;
						case "04":
							{
								string s = line.Substring(2, 72).TrimEnd();
								s = s.Trim();
								manager.PurchaserName = s;
							}
							break;
						case "08":
							{
								string s = line.Substring(2, 6).Trim();
								s = s.Trim();
								manager.Currency = s;
							}
							break;
						case "11":// Group id.
							{
								string t = line.Length > 6 ? line : line + "    ";
								string s = t.Substring(2, 4);
								string s1 = s.Substring(0, 2).Trim();
								string s2 = s.Substring(2, 2).Trim();
								if (string.IsNullOrEmpty(s1))
									throw new D8xFormatException($"Group id (code 11) can not be empty! Line index: {lineIndex}");
								SegmentNode seg1 = null;
								if (!nodes.ContainsKey(s1))
								{
									seg1 = new SegmentNode(s1, null);
									nodes.Add(s1, seg1);
									manager.AddSegment(seg1);
								}
								seg1 = nodes[s1];
								currentNode = seg1;
								if (!string.IsNullOrEmpty(s2))
								{
									string key = s1 + s2;
									SegmentNode seg2 = null;
									if (!nodes.ContainsKey(key))
									{
										seg2 = new SegmentNode(s2, seg1);
										seg1.AddChild(seg2);
										nodes.Add(key, seg2);
									}
									seg2 = nodes[key];
									currentNode = seg2;
								}
							}
							currentData = null;
							break;
						case "12":// Group name.
							{
								if (currentNode == null)
									throw new MyExceptions.NullReferenceException();
								string s = line.Substring(2, 72).TrimEnd();
								currentNode.Title += (currentNode.Title == "" ? "" : " ") + s;
							}
							break;
						case "21":
							{
								string t = line.Length > 10 ? line : line + "          "; // Just in case.
								string s = t.Substring(2, 8);
								string s1 = s.Substring(0, 2).Trim();
								string s2 = s.Substring(2, 2).Trim();
								string s3 = s.Substring(4, 4).Trim();
								if (string.IsNullOrEmpty(s1))
									throw new D8xFormatException($"Group id (code 11) can not be empty! Line index: {lineIndex}");
								int numberOfSegments = string.IsNullOrEmpty(s3) ? 2 : 3;
								SegmentNode seg1 = null;
								if (!nodes.ContainsKey(s1))
									continue;
								seg1 = nodes[s1];
								currentNode = seg1;
								SegmentNode parent = seg1;
								if (numberOfSegments > 2)
								{
									string key = s1 + s2;
									SegmentNode seg2 = null;
									if (!nodes.ContainsKey(key))
										continue;
									seg2 = nodes[key];
									currentNode = seg2;
									parent = seg2;
								}
								SegmentData data = new SegmentData(numberOfSegments > 2 ? s3 : s2, parent);
								parent.AddChild(data);
								string quantity = line.Substring(23, 20);
								string unit = quantity.Substring(11).Trim();
								string val = quantity.Substring(0, 11).Trim();
								int valQ;// = int.Parse(val);
								if (int.TryParse(val, out valQ))
									data.Quantity = valQ / 1000.0;
								data.UnitOfMeasurement = unit;
								currentData = data;
							}
							break;
						case "22":
						case "23":
							{
								if (currentData == null)
									throw new D8xFormatException($"Prices row belong to no positions! At line {lineIndex}");
								// Check if position is the same
								string unitPrice = line.Substring(12, 11).Trim();
								string totalPrice = line.Substring(24, 12).Trim();
								int i;
								double unit = 0;
								if (int.TryParse(unitPrice, out i))
									unit = i / 100.0;
								else if (int.TryParse(totalPrice, out i))
									unit = i / 10.0;
								currentData.UnitPrice = unit;
							}
							break;
						case "25":
							{
								if (currentData == null)
									throw new D8xFormatException($"No current segment data found! At line {lineIndex}");
								string s = line.Substring(2, 72).TrimEnd();
								currentData.Title = s;
							}
							break;
						case "26":
							{
								//if (currentData == null)
								//throw new MyExceptions.ArgumentNullException("No current segment data found!");
								string s = line.Substring(2, 72).TrimEnd();
								if (currentData != null)
									currentData.Description += s + "\r\n";
								else if (currentNode != null)
									// When data is null, current segment is group.
									currentNode.Description += s + "\r\n";
							}
							break;
					}
				}
				manager.CheckAndFixStructure();
				return manager;
			}
		}
		private static string FixEncodingIssues(string s)
		{
			// For some reason files d8x have umlaut character codes changed to 
			// some values (can be found in dictionary below).
			// iso-8859-1 has 1 byte encoding with special western europe characters.
			Dictionary<int, char> chars = new Dictionary<int, char>()
			{
				{ 0x84, 'ä' },
				{ 0x94, 'ö' },
				{ 0x81, 'ü' },
				{ 0xE1, 'ß' },
				{ 0x8E, 'Ä' },
				{ 0x99, 'Ö' },
				{ 0x9A, 'Ü' },
			};
			char[] cs = s.ToArray();
			for(int i = 0; i < cs.Length; i++)
			{
				char c = cs[i];
				if (chars.ContainsKey(c))
					cs[i] = chars[c];
			}
			return string.Concat(cs);

		}
	}
}
