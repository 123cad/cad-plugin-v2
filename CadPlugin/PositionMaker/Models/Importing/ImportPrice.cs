﻿using Inventory.Positions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PositionMaker.Models.Importing
{
	class CSVFormatException : MyExceptions.Exception
	{
		public CSVFormatException(string msg) : base(msg) { }
	}
	/// <summary>
	/// Loads prices for positions, from csv file.
	/// </summary>
	class ImportPrice
	{
		public static bool ImportPriceFromCsvFile(PositionsManager mgr, string fileName)
		{
			int positionColumn = 0;
			int priceColumn = 7;
			int numberOfSegments = mgr.FirstSegmentsCollection.First().GetDepth();
			using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
			using (StreamReader rd = new StreamReader(fs, Encoding.GetEncoding("iso-8859-1")))
			{
				int totalUpdatedPrices = 0;
				while (!rd.EndOfStream)
				{
					string line = rd.ReadLine();
					if (string.IsNullOrEmpty(line) || line[0] < '0' || '9' < line[0])
						continue;
					string[] columns = line.Split(';');
					if (columns.Length < priceColumn + 1)
						continue;
					string[] id = DecodePositionId(columns[positionColumn]);
					if (id.Length != numberOfSegments)
					{
						//System.Windows.Forms.MessageBox.Show($"position invalid {columns[positionColumn]}");
						continue;
					}
					Segment seg = mgr.GetSegment(id);
					if (seg == null)
					{
						//System.Windows.Forms.MessageBox.Show($"segment is null for position {columns[positionColumn]}");
						continue;
					}
					SegmentData sd = seg as SegmentData;
					if (sd == null)
					{
						//System.Windows.Forms.MessageBox.Show($"segment is null for position {columns[positionColumn]}");
						continue;
					}
					double price;
					string priceS = columns[priceColumn].Replace(',', '.');
					if (!DoubleHelper.TryParseInvariant(priceS, out price))
					{
						//System.Windows.Forms.MessageBox.Show($"Double is not valid: {priceS}");
						continue;
					}

					sd.UnitPrice = price;
					totalUpdatedPrices++;
				}

				System.Windows.Forms.MessageBox.Show($"Total updated prices: {totalUpdatedPrices} \n Segment count: {numberOfSegments}");
				return true;
			}
		}

		private static string[] DecodePositionId(string p)
		{
			string[] s = p.Trim().Split('.');
			s = s.Where(x => !string.IsNullOrEmpty(x)).ToArray();
			return s;
		}
	}
}
