﻿using CadPlugin.PositionMaker.Models;
using Inventory.Positions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CadPlugin.PositionMaker.Models
{
	class ExportXml
	{
		/// <summary>
		/// Saves positions to provided file in xml format.
		/// </summary>
		/// <param name="mgr"></param>
		/// <param name="fileName"></param>
		public static void SaveFile(PositionsManager mgr, string fileName)
		{
			XmlWriterSettings settings = new XmlWriterSettings
			{
				Encoding = Encoding.GetEncoding("iso-8859-1"),
				Indent = true,
				IndentChars = "\t"
				//,DoNotEscapeUriAttributes = true // Test to see if empty node is not merged to  <node />
			
			};
			using (FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None))
			using (XmlWriter writer = XmlWriter.Create(fs, settings))
			{
				Save(mgr, writer);
			}
		}
		/// <summary>
		/// Saves to string in xml format.
		/// </summary>
		/// <param name="mgr"></param>
		/// <returns></returns>
		public static string SaveToString(PositionsManager mgr)
		{
			XmlWriterSettings settings = new XmlWriterSettings
			{
				Encoding = Encoding.GetEncoding("iso-8859-1"),
				Indent = true,
				IndentChars = "\t"
			};
			StringBuilder builder = new StringBuilder();
			using (XmlWriter writer = XmlWriter.Create(builder, settings))
			{
				Save(mgr, writer);
			}
			return builder.ToString();
		}
		private static void Save(PositionsManager mgr, XmlWriter writer)
		{
			writer.WriteStartDocument();
			{
				writer.WriteStartElement("c123cad");
				{
					writer.WriteStartElement("Positions");
					{
						writer.WriteElementString("ServiceInfo", mgr.ServiceInfo);
						writer.WriteElementString("ProjectDate", mgr.ProjectDate.ToString("dd.MM.yyyy"));
						writer.WriteElementString("ProjectName", mgr.ProjectName);
						writer.WriteElementString("PurchaserName", mgr.PurchaserName);
						writer.WriteElementString("CompanyName", mgr.CompanyName);
						writer.WriteElementString("Currency", mgr.Currency);
						writer.WriteElementString("ProjectDescription", mgr.ProjectDescription);
						writer.WriteStartElement("Elements");
						{
							foreach (var v in mgr.FirstSegmentsCollection)
								WritePositionsRecursive(writer, v);
						}
						writer.WriteEndElement();
					}
					

					if (mgr.Groups.Any())
					{
						writer.WriteStartElement("PositionGroups");
						{
							foreach(var gr in mgr.Groups)
							{
								writer.WriteStartElement("Group");
								{
									writer.WriteElementString("Name", gr.Name);
									writer.WriteStartElement("Positions");
									{
										foreach (var v in gr.Positions.OrderBy(x=>x))
										{
											writer.WriteElementString("id", v);
										}
									}
									writer.WriteEndElement();
								}
								writer.WriteEndElement();
							}
						}
						writer.WriteEndElement();
					}
					writer.WriteEndElement();
				}
				writer.WriteEndElement();
			}
			writer.WriteEndDocument();
		}
		private static void WritePositionsRecursive(XmlWriter writer, Segment seg)
		{
			writer.WriteStartElement("Segment");
			{
				writer.WriteElementString("Id", seg.Id);
				writer.WriteElementString("Title", seg.Title);
				writer.WriteElementString("Description", seg.Description);
				var t = seg as SegmentNode;
				var p = seg as SegmentData;
				if (t != null)
				{
					writer.WriteStartElement("Children");
					{
						foreach (var v in t.Children)
							WritePositionsRecursive(writer, v);
					}
					writer.WriteEndElement();
				}
				else if (p != null)
				{
					writer.WriteStartElement("PositionData");
					{
						string attributeName = p.GetPositionId();
						writer.WriteAttributeString("id", attributeName);

						writer.WriteElementString("Description", p.Description);
						int decimals = 5;
						writer.WriteElementString("Quantity", DoubleHelper.ToStringInvariant(p.Quantity, decimals));
						writer.WriteElementString("Unit", p.UnitOfMeasurement);
						writer.WriteElementString("UnitPrice", DoubleHelper.ToStringInvariant(p.UnitPrice, decimals));
						writer.WriteStartElement("TotalTax");
						{
							writer.WriteAttributeString("percent", DoubleHelper.ToStringInvariant(p.VatPercent, 1));
							writer.WriteString(DoubleHelper.ToStringInvariant(p.TotalTax, decimals));
						}
						writer.WriteEndElement();
						writer.WriteElementString("TotalPrice", DoubleHelper.ToStringInvariant(p.TotalPrice, decimals));
					}
					writer.WriteEndElement();
				}
			}
			writer.WriteEndElement();
		}
	}
}
