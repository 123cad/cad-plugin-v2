﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PositionMaker.Models
{
	/// <summary>
	/// This class is used for keeping unit associations
	/// (all values which are connected to predefined unit, 
	/// so user can use its own symbols).
	/// </summary>
	class UnitAssociations
	{
		private Dictionary<string, Unit> units = new Dictionary<string, Unit>();

		public void SetUnit(Unit unit, string[] strings)
		{
			var remove = units.Where(x => x.Value == unit).ToList();
			foreach (var s in remove)
				units.Remove(s.Key);
			foreach (string s in strings)
				units[s.ToLower()] = unit;
		}

		/// <summary>
		/// Sets unit associated with p, and returns if operation was successfull.
		/// </summary>
		/// <param name="u"></param>
		/// <returns></returns>
		public Unit GetUnit(string p)
		{
			string s = p.ToLower();
			Unit u = Unit.NotAUnit;
			if (units.ContainsKey(s))
				u = units[s];
			return u;
		}
	}
}
