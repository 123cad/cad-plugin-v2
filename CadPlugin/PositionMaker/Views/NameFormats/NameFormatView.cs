﻿using CadPlugin.PositionMaker.Controllers;
using CadPlugin.PositionMaker.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views.NameFormats
{
	public partial class NameFormatView : Form
	{
		private PositionPointDefinition positionDef;
		private PositionPreviewController previewController = new PositionPreviewController();
		/// <summary>
		/// If control has an error, it exists in the collection. Value not used for now(could be error message).
		/// </summary>
		private Dictionary<Control, string> controlWithErrors = new Dictionary<Control, string>();
		public NameFormatView(PositionPointDefinition definition)
		{
			InitializeComponent();
			positionDef = definition;

			Initialize();
		}
		private void Initialize()
		{
			Func<string, object, string, Binding> createBinding = (property, dataObject, dataProperty) =>
			{
				Binding bd = new Binding(property, dataObject, dataProperty);
				bd.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
				bd.FormattingEnabled = true;
				return bd;
			};
			Binding b;
			b = createBinding(nameof(CheckBox.Checked), positionDef, nameof(PositionPointDefinition.Segment1Used));
			b.BindingComplete += (_, e) =>
			{
				if (e.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate)
					UpdatePreview();
			};
			checkBoxIncluded1.DataBindings.Add(b);

			b = createBinding(nameof(CheckBox.Checked), positionDef, nameof(PositionPointDefinition.Segment2Used));
			b.BindingComplete += (_, e) =>
			{
				if (e.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate)
					UpdatePreview();
			};
			checkBoxIncluded2.DataBindings.Add(b);

			b = createBinding(nameof(CheckBox.Checked), positionDef, nameof(PositionPointDefinition.Segment3Used));
			b.BindingComplete += (_, e) =>
			{
				if (e.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate)
					UpdatePreview();
			};
			checkBoxIncluded3.DataBindings.Add(b);

			b = createBinding(nameof(CheckBox.Checked), positionDef, nameof(PositionPointDefinition.Segment4Used));
			b.BindingComplete += (_, e) =>
			{
				if (e.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate)
					UpdatePreview();
			};
			checkBoxIncluded4.DataBindings.Add(b);

			b = createBinding(nameof(CheckBox.Checked), positionDef.Segment1, nameof(PositionNumberSegment.NumberOfLeading0));
			b.Format += (_, e) =>
			{
				e.Value = positionDef.Segment1.NumberOfLeading0 == 1;
			};
			b.Parse += (_, e2) =>
			{
				//positionDef.Segment1.NumberOfLeading0 
				e2.Value = checkBoxZeroUsed1.Checked ? 1 : 0;
			};
			b.BindingComplete += (_, e) =>
			{
				if (e.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate)
					UpdatePreview();
			};
			checkBoxZeroUsed1.DataBindings.Add(b);

			b = createBinding(nameof(CheckBox.Checked), positionDef.Segment2, nameof(PositionNumberSegment.NumberOfLeading0));
			b.Format += (_, e) =>
			{
				e.Value = positionDef.Segment2.NumberOfLeading0 == 1;
			};
			b.Parse += (_, e2) =>
			{
				e2.Value = checkBoxZeroUsed2.Checked ? 1 : 0;
			};
			b.BindingComplete += (_, e) =>
			{
				if (e.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate)
					UpdatePreview();
			};
			checkBoxZeroUsed2.DataBindings.Add(b);

			b = createBinding(nameof(ComboBox.SelectedItem), positionDef.Segment3, nameof(PositionNumberSegment.NumberOfLeading0));
			if (positionDef.Segment3.NumberOfLeading0 == 0)
				comboBoxZeroUsed3.SelectedIndex = 0;
			else
				comboBoxZeroUsed3.SelectedValue = positionDef.Segment3.NumberOfLeading0.ToString();
			b.Parse += (_, e2) =>
			{
				int i;
				if (!int.TryParse(e2.Value as string, out i))
					e2.Value = 0;
				else
					e2.Value = i;
			};
			b.BindingComplete += (_, e) =>
			{
				if (e.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate)
					UpdatePreview();
			};
			// BindingComplete not fired (as it should), so this is workaround.
			comboBoxZeroUsed3.SelectedIndexChanged += (_, e) =>
			{
				comboBoxZeroUsed3.DataBindings[nameof(ComboBox.SelectedItem)].WriteValue();
			};
			comboBoxZeroUsed3.DataBindings.Add(b);

			// Skip non digit characters.
			KeyPressEventHandler allowOnlyNumbers = (_, e)=>
			{
				if (!char.IsDigit(e.KeyChar))
					e.Handled = true;
			};
			textBoxIncrement1.KeyPress += allowOnlyNumbers;
			textBoxIncrement2.KeyPress += allowOnlyNumbers;
			textBoxIncrement3.KeyPress += allowOnlyNumbers;


			Action<ConvertEventArgs, Control> parseIncrement = (e, c) =>
			{
				controlWithErrors.Remove(c);
				errorProvider1.SetError(c, null);

				string val = (e.Value as string) ?? "1";
				int increment = int.Parse(val);
				if (positionDef.Segment1.IsIncrementAllowed(increment))
					e.Value = increment;
				else
				{
					controlWithErrors.Add(c, "");
					errorProvider1.SetError(c, "Desired increment is too big!");
				}
			};
			b = createBinding(nameof(TextBox.Text), positionDef.Segment1, nameof(PositionNumberSegment.Increment));
			b.Parse += (_, e) =>
			{
				parseIncrement(e, textBoxIncrement1);
			};
			b.BindingComplete += (_, e) =>
			{
				if (e.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate)
					UpdatePreview();
			};
			textBoxIncrement1.DataBindings.Add(b);
			b = createBinding(nameof(TextBox.Text), positionDef.Segment2,  nameof(PositionNumberSegment.Increment));
			b.Parse += (_, e) =>
			{
				parseIncrement(e, textBoxIncrement2);
			};
			b.BindingComplete += (_, e) =>
			{
				if (e.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate)
					UpdatePreview();
			};
			textBoxIncrement2.DataBindings.Add(b);

			b = createBinding(nameof(TextBox.Text), positionDef.Segment3, nameof(PositionNumberSegment.Increment));
			b.Parse += (_, e) =>
			{
				parseIncrement(e, textBoxIncrement3);
			};
			b.BindingComplete += (_, e) =>
			{
				if (e.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate)
					UpdatePreview();
			};
			textBoxIncrement3.DataBindings.Add(b);



			/*checkBoxIncluded1.CheckedChanged += (_, e) => UpdatePreview();
			checkBoxIncluded2.CheckedChanged += (_, e) => UpdatePreview();
			checkBoxIncluded3.CheckedChanged += (_, e) => UpdatePreview();
			checkBoxIncluded4.CheckedChanged += (_, e) => UpdatePreview();

			checkBoxZeroUsed1.CheckedChanged += (_, e) => UpdatePreview();
			checkBoxZeroUsed2.CheckedChanged += (_, e) => UpdatePreview();
			comboBoxZeroUsed3.SelectedIndexChanged += (_, e) => UpdatePreview();

			textBoxIncrement1.TextChanged += (_, e) => UpdatePreview();
			textBoxIncrement2.TextChanged += (_, e) => UpdatePreview();
			textBoxIncrement3.TextChanged += (_, e) => UpdatePreview();*/

			

			UpdatePreview();
		}
		private void UpdatePreview()
		{
			listBoxPreview.Items.Clear();
			//var vals = previewController.CreatePreviewNames(positionDef);
			//listBoxPreview.Items.AddRange(vals.ToArray());
		}
	}
}
