﻿namespace CadPlugin.PositionMaker.Views.NameFormats
{
	partial class NameFormatView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.listBoxPreview = new System.Windows.Forms.ListBox();
			this.label3 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.textBoxIncrement1 = new System.Windows.Forms.TextBox();
			this.checkBoxZeroUsed1 = new System.Windows.Forms.CheckBox();
			this.label5 = new System.Windows.Forms.Label();
			this.checkBoxIncluded1 = new System.Windows.Forms.CheckBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.textBoxIncrement2 = new System.Windows.Forms.TextBox();
			this.checkBoxZeroUsed2 = new System.Windows.Forms.CheckBox();
			this.label6 = new System.Windows.Forms.Label();
			this.checkBoxIncluded2 = new System.Windows.Forms.CheckBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.label8 = new System.Windows.Forms.Label();
			this.comboBoxZeroUsed3 = new System.Windows.Forms.ComboBox();
			this.textBoxIncrement3 = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.checkBoxIncluded3 = new System.Windows.Forms.CheckBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.checkBoxIncluded4 = new System.Windows.Forms.CheckBox();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.buttonOk = new System.Windows.Forms.Button();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
			this.SuspendLayout();
			// 
			// listBoxPreview
			// 
			this.listBoxPreview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.listBoxPreview.FormattingEnabled = true;
			this.listBoxPreview.Location = new System.Drawing.Point(120, 253);
			this.listBoxPreview.Name = "listBoxPreview";
			this.listBoxPreview.Size = new System.Drawing.Size(110, 108);
			this.listBoxPreview.TabIndex = 13;
			this.listBoxPreview.TabStop = false;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(69, 253);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(45, 13);
			this.label3.TabIndex = 14;
			this.label3.Text = "Preview";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.textBoxIncrement1);
			this.groupBox1.Controls.Add(this.checkBoxZeroUsed1);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.checkBoxIncluded1);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(434, 52);
			this.groupBox1.TabIndex = 19;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Segment 1";
			// 
			// textBoxIncrement1
			// 
			this.textBoxIncrement1.Location = new System.Drawing.Point(370, 16);
			this.textBoxIncrement1.Name = "textBoxIncrement1";
			this.textBoxIncrement1.Size = new System.Drawing.Size(41, 20);
			this.textBoxIncrement1.TabIndex = 21;
			this.textBoxIncrement1.Text = "1";
			// 
			// checkBoxZeroUsed1
			// 
			this.checkBoxZeroUsed1.AutoSize = true;
			this.checkBoxZeroUsed1.Location = new System.Drawing.Point(136, 19);
			this.checkBoxZeroUsed1.Name = "checkBoxZeroUsed1";
			this.checkBoxZeroUsed1.Size = new System.Drawing.Size(113, 17);
			this.checkBoxZeroUsed1.TabIndex = 20;
			this.checkBoxZeroUsed1.Text = "Leading zero used";
			this.checkBoxZeroUsed1.UseVisualStyleBackColor = true;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(310, 19);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(54, 13);
			this.label5.TabIndex = 20;
			this.label5.Text = "Increment";
			// 
			// checkBoxIncluded1
			// 
			this.checkBoxIncluded1.AutoSize = true;
			this.checkBoxIncluded1.Checked = true;
			this.checkBoxIncluded1.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxIncluded1.Location = new System.Drawing.Point(6, 19);
			this.checkBoxIncluded1.Name = "checkBoxIncluded1";
			this.checkBoxIncluded1.Size = new System.Drawing.Size(67, 17);
			this.checkBoxIncluded1.TabIndex = 0;
			this.checkBoxIncluded1.Text = "Included";
			this.checkBoxIncluded1.UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.textBoxIncrement2);
			this.groupBox2.Controls.Add(this.checkBoxZeroUsed2);
			this.groupBox2.Controls.Add(this.label6);
			this.groupBox2.Controls.Add(this.checkBoxIncluded2);
			this.groupBox2.Location = new System.Drawing.Point(12, 71);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(434, 52);
			this.groupBox2.TabIndex = 22;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Segment 2";
			// 
			// textBoxIncrement2
			// 
			this.textBoxIncrement2.Location = new System.Drawing.Point(370, 16);
			this.textBoxIncrement2.Name = "textBoxIncrement2";
			this.textBoxIncrement2.Size = new System.Drawing.Size(41, 20);
			this.textBoxIncrement2.TabIndex = 21;
			this.textBoxIncrement2.Text = "1";
			// 
			// checkBoxZeroUsed2
			// 
			this.checkBoxZeroUsed2.AutoSize = true;
			this.checkBoxZeroUsed2.Location = new System.Drawing.Point(136, 19);
			this.checkBoxZeroUsed2.Name = "checkBoxZeroUsed2";
			this.checkBoxZeroUsed2.Size = new System.Drawing.Size(113, 17);
			this.checkBoxZeroUsed2.TabIndex = 20;
			this.checkBoxZeroUsed2.Text = "Leading zero used";
			this.checkBoxZeroUsed2.UseVisualStyleBackColor = true;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(310, 19);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(54, 13);
			this.label6.TabIndex = 20;
			this.label6.Text = "Increment";
			// 
			// checkBoxIncluded2
			// 
			this.checkBoxIncluded2.AutoSize = true;
			this.checkBoxIncluded2.Checked = true;
			this.checkBoxIncluded2.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxIncluded2.Location = new System.Drawing.Point(6, 19);
			this.checkBoxIncluded2.Name = "checkBoxIncluded2";
			this.checkBoxIncluded2.Size = new System.Drawing.Size(67, 17);
			this.checkBoxIncluded2.TabIndex = 0;
			this.checkBoxIncluded2.Text = "Included";
			this.checkBoxIncluded2.UseVisualStyleBackColor = true;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.label8);
			this.groupBox3.Controls.Add(this.comboBoxZeroUsed3);
			this.groupBox3.Controls.Add(this.textBoxIncrement3);
			this.groupBox3.Controls.Add(this.label7);
			this.groupBox3.Controls.Add(this.checkBoxIncluded3);
			this.groupBox3.Location = new System.Drawing.Point(12, 129);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(434, 52);
			this.groupBox3.TabIndex = 22;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Segment 3";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(131, 21);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(54, 13);
			this.label8.TabIndex = 23;
			this.label8.Text = "Leading 0";
			// 
			// comboBoxZeroUsed3
			// 
			this.comboBoxZeroUsed3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxZeroUsed3.FormattingEnabled = true;
			this.comboBoxZeroUsed3.Items.AddRange(new object[] {
            "None",
            "1",
            "2",
            "3"});
			this.comboBoxZeroUsed3.Location = new System.Drawing.Point(191, 17);
			this.comboBoxZeroUsed3.Name = "comboBoxZeroUsed3";
			this.comboBoxZeroUsed3.Size = new System.Drawing.Size(58, 21);
			this.comboBoxZeroUsed3.TabIndex = 22;
			// 
			// textBoxIncrement3
			// 
			this.textBoxIncrement3.Location = new System.Drawing.Point(370, 16);
			this.textBoxIncrement3.Name = "textBoxIncrement3";
			this.textBoxIncrement3.Size = new System.Drawing.Size(41, 20);
			this.textBoxIncrement3.TabIndex = 21;
			this.textBoxIncrement3.Text = "1";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(310, 19);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(54, 13);
			this.label7.TabIndex = 20;
			this.label7.Text = "Increment";
			// 
			// checkBoxIncluded3
			// 
			this.checkBoxIncluded3.AutoSize = true;
			this.checkBoxIncluded3.Checked = true;
			this.checkBoxIncluded3.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxIncluded3.Location = new System.Drawing.Point(6, 19);
			this.checkBoxIncluded3.Name = "checkBoxIncluded3";
			this.checkBoxIncluded3.Size = new System.Drawing.Size(67, 17);
			this.checkBoxIncluded3.TabIndex = 0;
			this.checkBoxIncluded3.Text = "Included";
			this.checkBoxIncluded3.UseVisualStyleBackColor = true;
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.checkBoxIncluded4);
			this.groupBox4.Location = new System.Drawing.Point(12, 187);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(434, 52);
			this.groupBox4.TabIndex = 22;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Segment 4";
			// 
			// checkBoxIncluded4
			// 
			this.checkBoxIncluded4.AutoSize = true;
			this.checkBoxIncluded4.Checked = true;
			this.checkBoxIncluded4.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxIncluded4.Location = new System.Drawing.Point(6, 19);
			this.checkBoxIncluded4.Name = "checkBoxIncluded4";
			this.checkBoxIncluded4.Size = new System.Drawing.Size(67, 17);
			this.checkBoxIncluded4.TabIndex = 0;
			this.checkBoxIncluded4.Text = "Included";
			this.checkBoxIncluded4.UseVisualStyleBackColor = true;
			// 
			// buttonCancel
			// 
			this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonCancel.Location = new System.Drawing.Point(450, 368);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(75, 23);
			this.buttonCancel.TabIndex = 23;
			this.buttonCancel.Text = "Cancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			// 
			// buttonOk
			// 
			this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonOk.Location = new System.Drawing.Point(450, 339);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(75, 23);
			this.buttonOk.TabIndex = 24;
			this.buttonOk.Text = "Ok";
			this.buttonOk.UseVisualStyleBackColor = true;
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			// 
			// NameFormatView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(559, 670);
			this.Controls.Add(this.buttonOk);
			this.Controls.Add(this.buttonCancel);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.listBoxPreview);
			this.Name = "NameFormatView";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Poition point name";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.ListBox listBoxPreview;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox textBoxIncrement1;
		private System.Windows.Forms.CheckBox checkBoxZeroUsed1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.CheckBox checkBoxIncluded1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox textBoxIncrement2;
		private System.Windows.Forms.CheckBox checkBoxZeroUsed2;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.CheckBox checkBoxIncluded2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.ComboBox comboBoxZeroUsed3;
		private System.Windows.Forms.TextBox textBoxIncrement3;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.CheckBox checkBoxIncluded3;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.CheckBox checkBoxIncluded4;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.Button buttonOk;
		private System.Windows.Forms.ErrorProvider errorProvider1;
	}
}