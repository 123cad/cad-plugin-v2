﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace CadPlugin.PositionMaker.Views.TreeSegmentDisplay
{
	public partial class TreeSegmentDisplayView : UserControl, ITreeSegmentDisplayView
	{
		public TreeSegmentDisplayView()
		{
			InitializeComponent();
			typeof(DataGridView).InvokeMember(
			  "DoubleBuffered",
			  BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty,
			  null,
			  Dgv,
			  new object[] { true });
		}

		public DataGridViewColumn ColumnUnitOfMeasurement => ColumnUnitMeasurement;
		public DataGridView Dgv => dataGridViewMain;

		DataGridViewColumn ITreeSegmentDisplayView.ColumnQuantity => ColumnQuantity;

		DataGridViewColumn ITreeSegmentDisplayView.ColumnTitle => ColumnTitle;

		DataGridViewColumn ITreeSegmentDisplayView.ColumnTotalPrice => ColumnTotalPrice;

		DataGridViewColumn ITreeSegmentDisplayView.ColumnUnitPrice => ColumnUnitPrice;

		DataGridViewColumn ITreeSegmentDisplayView.ColumnVat => ColumnVat;
	}
}
