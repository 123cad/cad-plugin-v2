﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views.TreeSegmentDisplay
{
	interface ITreeSegmentDisplayView
	{
		DataGridView Dgv { get; }

		DataGridViewColumn ColumnQuantity { get; }
		DataGridViewColumn ColumnUnitOfMeasurement { get; }
		DataGridViewColumn ColumnUnitPrice { get; }
		DataGridViewColumn ColumnVat { get; }
		DataGridViewColumn ColumnTotalPrice { get; }
		DataGridViewColumn ColumnTitle { get; }

	}
}
