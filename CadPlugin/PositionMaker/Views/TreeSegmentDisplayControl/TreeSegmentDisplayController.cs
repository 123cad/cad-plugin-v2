﻿using CadPlugin.PositionMaker.Models;
using CadPlugin.PositionMaker.Models.CadInterface;
using Inventory.Positions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views.TreeSegmentDisplay
{
	/// <summary>
	/// Displaying segment as a tree view inside of DGV.
	/// </summary>
	class TreeSegmentDisplayController
	{
		/// <summary>
		/// Used to merge together required data.
		/// </summary>
		class SegmentWrapper
		{
			public Segment Segment { get; set; }
			/// <summary>
			/// For perfomance reasons - only 1 casting.
			/// </summary>
			public SegmentData SegmentAsData { get; set; }
			/// <summary>
			/// Added for performance.
			/// </summary>
			public bool IsSegmentData { get; private set; }
			public bool IsButtonDisplayed { get; set; }
			public SegmentWrapper(Segment s)
			{
				Segment = s;
				SegmentAsData = Segment as SegmentData;
				IsSegmentData= SegmentAsData != null;
				// By default SegmentData has a button.
				IsButtonDisplayed = IsSegmentData;
			}
		}


		private ITreeSegmentDisplayView view;
		private PositionsManager data;
		private int numberOfSegments = 0;

		/// <summary>
		/// Is column used for selecting CAD objects used.
		/// </summary>
		public bool IsSelectObjectsColumnUsed { get; private set; }
		/// <summary>
		/// Column whcih displays button for selecting object.
		/// </summary>
		private DataGridViewColumn SelectObjectColumn { get; set; }
		


		/// <summary>
		/// Segments ordered by index.
		/// </summary>
		private SegmentWrapper[] displayedSegments = new SegmentWrapper[0];
		private Segment _SelectedSegment = null;
		public Segment SelectedSegment
		{
			get { return _SelectedSegment; }
			set
			{
				if (displayedSegments != null)
				{
					for (int i = 0; i < displayedSegments.Length; i++)
					{
						var v = displayedSegments[i];
						if (v.Segment == value)
						{
							int column = view.ColumnTitle.Index;
							if (numberOfSegments != 0)
								column = numberOfSegments - 1;
							view.Dgv.CurrentCell = view.Dgv[column, i];
						}
					}
				}

			}
		}

		/// <summary>
		/// Occurs when any field is changed in DGV.
		/// </summary>
		public event Action<Segment> DataChanged;
		public event Action SelectedSegmentChanged;
		public event Action<SegmentData> SegmentUnitChanged;
		private bool _Enabled = false;
		/// <summary>
		/// When changing color to disabled, keeps previous active color.
		/// </summary>
		private System.Drawing.Color normalColor;
		private bool skipValueNeeded = false;
		public bool Enabled
		{
			get
			{
				return _Enabled;
			}
			set
			{
				_Enabled = value;
				((Control)view).Enabled = value;
				view.Dgv.Enabled = value;
				view.Dgv.ReadOnly = value;
				if (!value)
				{
					normalColor = view.Dgv.ForeColor;
					view.Dgv.ForeColor = System.Drawing.Color.LightGray;
				}
				else
					view.Dgv.ForeColor = normalColor;
			}
		}

		/// <summary>
		/// Select objects and set Quantity property.
		/// </summary>
		public event Action<SegmentData> SelectPositionObjects;

		/// <summary>
		/// All columns used in DGV (segments are counted as 1).
		/// </summary>
		private enum Columns
		{
			Segments, 
			Title, 
			UnitMeasurement, 
			UnitPrice, 
			Quantity, 
			Vat,
			TotalPrice, 
			SelectCadObjects
		}

		private Dictionary<Columns, bool> columnEditMask = new Dictionary<Columns, bool>
		{
			{ Columns.Segments			, false},
			{ Columns.Title				, false},
			{ Columns.UnitMeasurement	, false},
			{ Columns.UnitPrice			, false},
			{ Columns.Quantity			, false},
			{ Columns.Vat				, false},
			{ Columns.TotalPrice        , false},
			{ Columns.SelectCadObjects	, false},
		};

		public TreeSegmentDisplayController(ITreeSegmentDisplayView v, PositionsManager mgr, bool addSelectButtonColumn = false)
		{
			view = v;
			data = mgr;
			IsSelectObjectsColumnUsed = addSelectButtonColumn;

			Initialize(true);
		}
		public void SetNewDataSource(PositionsManager d)
		{
			data = d;
			DataStructureUpdated();
		}

		/// <summary>
		/// Sets which fields are editable.
		/// </summary>
		/// <param name="segments"></param>
		/// <param name="title"></param>
		/// <param name="quantity"></param>
		/// <param name="measurementUnit"></param>
		/// <param name="unitPrice"></param>
		/// <param name="vat"></param>
		/// <param name="totalPrice"></param>
		public void SetEditMask(bool segments = false, bool title = false, bool quantity = false, 
								bool measurementUnit = false, bool unitPrice = false,
								bool vat = false, bool totalPrice = false, bool selectObjects = false)
		{
			columnEditMask[Columns.Segments] = segments;
			columnEditMask[Columns.Title] = title;
			columnEditMask[Columns.Quantity] = quantity;
			columnEditMask[Columns.UnitMeasurement] = measurementUnit;
			columnEditMask[Columns.UnitPrice] = unitPrice;
			columnEditMask[Columns.Vat] = vat;
			columnEditMask[Columns.TotalPrice] = totalPrice;
			columnEditMask[Columns.SelectCadObjects] = selectObjects;
		}

		private void Initialize(bool firstInvoke)
		{
			try
			{
				skipValueNeeded = true;
				view.Dgv.SuspendLayout();
				var displayed = data.GetAllSegments().ToArray();
				displayedSegments = displayed.Select(x => new SegmentWrapper(x)).ToArray();
				view.Dgv.CurrentCell = null;
				view.Dgv.RowCount = 0;
				view.Dgv.RowCount = displayedSegments.Length;
				if (firstInvoke)
				{
					view.Dgv.VirtualMode = true;
					view.Dgv.CellValueNeeded += MainDgv_CellValueNeeded;
					view.Dgv.CellValuePushed += Dgv_CellValuePushed;
					view.Dgv.CurrentCellChanged += Dgv_CurrentCellChanged;
				}
				if (displayedSegments.Length == 0)
				{
					view.Dgv.RowCount = 0;
					//return;
				}

				// Remove existing segment columns.
				while (numberOfSegments-- > 0)
				{
					view.Dgv.Columns.RemoveAt(0);
				}

				numberOfSegments = data.FirstSegmentsCollection.FirstOrDefault()?.GetDepth()??0;
				// Create Columns
				for (int i = 0; i < numberOfSegments; i++)
				{
					AddSegmentColumn(i);
				}
				if (IsSelectObjectsColumnUsed && firstInvoke)
				{
					int index = view.ColumnTitle.Index + 1;
					SelectObjectColumn = new DataGridViewButtonColumn();
					SelectObjectColumn.HeaderText = "";
					SelectObjectColumn.ToolTipText = "Select objects.";
					SelectObjectColumn.Width = 40;
					view.Dgv.Columns.Insert(index, SelectObjectColumn);
					// Row in which mouse is pressed. Reset on MouseUp or MouseLeave.
					int pressedMouseRow = -2;
					//int cursorRow = -2;
					view.Dgv.CellPainting += (_, e) =>
					{
						// Draw a button for SegmentData columns.
						if (e.RowIndex == -1 || e.ColumnIndex != SelectObjectColumn.Index)
							return;
						var segment = displayedSegments[e.RowIndex];
						if (!segment.IsSegmentData || !segment.IsButtonDisplayed)
						{
							e.PaintBackground(e.ClipBounds, false);
							e.Handled = true;
						}
						else
						{
							e.PaintBackground(e.ClipBounds, false);
							bool currentCellPressed = pressedMouseRow == e.RowIndex;
							var buttonState = currentCellPressed ? System.Windows.Forms.VisualStyles.PushButtonState.Pressed : System.Windows.Forms.VisualStyles.PushButtonState.Normal;
							ButtonRenderer.DrawButton(e.Graphics, e.CellBounds, "+", view.Dgv.Font,false, buttonState);
							e.Handled = true;
						}
					};
					view.Dgv.CellMouseEnter += (_, e) =>
					{
						//cursorRow = e.RowIndex;
						//Cursor.Current = Cursors.Hand;
					};
					view.Dgv.CellMouseDown += (_, e) =>
					{
						// Draw a button for SegmentData columns.
						if (e.RowIndex == -1 || e.ColumnIndex != SelectObjectColumn.Index)
							return;
						var segment = displayedSegments[e.RowIndex];
						if (segment.IsSegmentData && segment.IsButtonDisplayed)
						{
							pressedMouseRow = e.RowIndex;
							view.Dgv.InvalidateCell(e.ColumnIndex, e.RowIndex);
						}
					};
					view.Dgv.CellMouseUp += (_, e) =>
					{
						// Draw a button for SegmentData columns.
						if (e.RowIndex == -1 || e.ColumnIndex != SelectObjectColumn.Index)
							return;
						var segment = displayedSegments[e.RowIndex];
						if (segment.IsSegmentData && segment.IsButtonDisplayed)
						{
							pressedMouseRow = -2;
							view.Dgv.InvalidateCell(e.ColumnIndex, e.RowIndex);
						}
					};
					view.Dgv.CellMouseLeave += (_, e) =>
					{
						/*if (cursorRow == e.RowIndex)
						{
							Cursor.Current = Cursors.Arrow;
							cursorRow = -2;
						}*/
						// Draw a button for SegmentData columns.
						if (e.RowIndex == -1 || e.ColumnIndex != SelectObjectColumn.Index)
							return;
						var segment = displayedSegments[e.RowIndex];
						//if (pressedMouseRow == e.RowIndex)
						pressedMouseRow = -2;
						if (segment.IsSegmentData && segment.IsButtonDisplayed)
						{
							view.Dgv.InvalidateCell(e.ColumnIndex, e.RowIndex);
						}
					};
					view.Dgv.CellClick += (_, e) =>
					{
						if (e.RowIndex == -1 || e.ColumnIndex != SelectObjectColumn.Index)
							return;
						var segment = displayedSegments[e.RowIndex];
						if (segment.IsSegmentData && segment.IsButtonDisplayed)
						{
							var currentCell = view.Dgv.CurrentCell;
							view.Dgv.CurrentCell = null;
							var dataSeg = segment.SegmentAsData;
							double oldQuantity = dataSeg.Quantity;
							SelectPositionObjects?.Invoke(segment.SegmentAsData);
							// Update data in the row.
							view.Dgv.InvalidateRow(e.RowIndex);
							view.Dgv.CurrentCell = view.Dgv[view.ColumnQuantity.Index, currentCell.RowIndex];

							string oldQ = DoubleHelper.ToStringInvariant(oldQuantity, 3);
							string newQ = DoubleHelper.ToStringInvariant(dataSeg.Quantity, 3);
							if (oldQ != newQ)

								MessageBox.Show($"Quantity updated from {oldQ} to {newQ}", $"Position: {dataSeg.GetPositionId()}");
						}
					};
				}
			}
			finally
			{
				view.Dgv.ResumeLayout();
				skipValueNeeded = false;
			}
			
		}

		/// <summary>
		/// Creates new segment column, on provided index.
		/// </summary>
		/// <param name="index"></param>
		private void AddSegmentColumn(int index)
		{
			DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
			column.HeaderText = $"S{index.ToString()}";
			column.Width = 40;
			if (index + 1 == numberOfSegments)
				column.Width = 25 * numberOfSegments + 10;
			view.Dgv.Columns.Insert(index, column);
			column.HeaderCell.InheritedStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			// AllCells are the best, but then we have performance problem - on changing
			// RowCount all AllCells column are checked.
			//column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
			column.ReadOnly = true;
		}

		/// <summary>
		/// When there are no positions, segment columns are not created.
		/// With this method we show them (used in PositionsCreator for better UX)
		/// </summary>
		/// <param name="n"></param>
		public void SetNumberOfSegmentColumns(int n)
		{
			// If there is data, there are columns.
			if (view.Dgv.RowCount > 0)
				return;
			while (numberOfSegments-- > 0)
				view.Dgv.Columns.RemoveAt(0);

			numberOfSegments = n;
			for (int i = 0; i < n; i++)
				AddSegmentColumn(i);
		}
		/// <summary>
		/// Provides function which determines if Position has a button.
		/// If Segment is null, all segments are checked. Otherwise, only selected one.
		/// </summary>
		/// <param name="f"></param>
		public void SetPositionsWhichHaveButton(Func<SegmentData, bool> f, Segment segment = null)
		{
			foreach(var seg in displayedSegments)
			{
				if (!seg.IsSegmentData)
					continue;
				if (segment != null)
				{
					if (segment == seg.Segment)
					{
						seg.IsButtonDisplayed = f(seg.SegmentAsData);
						break;
					}
				}
				else
					seg.IsButtonDisplayed = f(seg.SegmentAsData);

			}
			if (IsSelectObjectsColumnUsed)
				view.Dgv.InvalidateColumn(this.SelectObjectColumn.Index);
		}

		private void Dgv_CurrentCellChanged(object sender, EventArgs e)
		{
			_SelectedSegment = null;
			if (view.Dgv.CurrentCell != null)
			{
				_SelectedSegment = displayedSegments[view.Dgv.CurrentCell.RowIndex].Segment;
			}

			SelectedSegmentChanged?.Invoke();
		}

		/// <summary>
		/// Invoke when anything in PositionsManager changes (add or remove segment).
		/// </summary>
		public void DataStructureUpdated()
		{
			int row = view.Dgv.CurrentCell?.RowIndex ?? -1;
			int column = view.Dgv.CurrentCell?.ColumnIndex ?? -1;
			view.Dgv.CurrentCell = null;
			Initialize(false);
			if (view.Dgv.RowCount <= row)
				row = view.Dgv.RowCount - 1;
			if (view.Dgv.ColumnCount <= column)
				column = view.Dgv.ColumnCount - 1;
			if (row >= 0 && column >= 0)
				view.Dgv.CurrentCell = view.Dgv[column, row];
		}


		private void Dgv_CellValuePushed(object sender, DataGridViewCellValueEventArgs e)
		{
			if (!data.FirstSegmentsCollection.Any())
				return;
			int row = view.Dgv.CurrentCell.RowIndex;
			int column = view.Dgv.CurrentCell.ColumnIndex;
			if (column < data.FirstSegmentsCollection.First().GetDepth())//  definitions.NumberOfSegments)

				return;
			var seg = displayedSegments[row];
			if (column == view.ColumnTitle.Index)
				seg.Segment.Title = e.Value as string;
			else
			{
				if (seg.IsSegmentData)
				{
					SegmentData sd = seg.SegmentAsData;
					double d;
					// Prices has ',' instead of '.' for decimal separator.
					string priceString = e.Value as string;
					if (priceString != null)
					{
						priceString = priceString.Replace('.', ';');
						priceString = priceString.Replace(',', '.');
						priceString = priceString.Replace(';', ',');
					}
					if (column == view.ColumnQuantity.Index && DoubleHelper.TryParseInvariant(e.Value as string, out d)) sd.Quantity = d;
					if (column == view.ColumnUnitPrice.Index && DoubleHelper.TryParseInvariant(priceString, out d)) sd.UnitPrice = d;
					if (column == view.ColumnUnitOfMeasurement.Index)
					{
						sd.UnitOfMeasurement = e.Value as string;
						//if (IsSelectObjectsColumnUsed)
						//view.Dgv.InvalidateColumn(this.SelectObjectColumn.Index);
						SegmentUnitChanged?.Invoke(sd);
					};
					if (column == view.ColumnVat.Index && DoubleHelper.TryParseInvariant(priceString, out d)) sd.VatPercent = d;
					//view.Dgv.InvalidateRow(e.RowIndex);

					// Needed because price in parents is also changed, and we need to display it.
					// (optimization could be to refresh N upstream rows, but need to detect where to stop.
					view.Dgv.InvalidateColumn(view.ColumnTotalPrice.Index);
				}
			}
			DataChanged?.Invoke(seg.Segment);
		}
		private void MainDgv_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
		{
			//if (skipValueNeeded)
				//return;
			// Length = 0 is needed because at when RowCount is set to 0, this method is invoked with e.RowIndex of current cell.
			if (e.RowIndex == -1 || displayedSegments.Length == 0)
				return;
			if (e.RowIndex >= displayedSegments.Length)
				return;
			var segment = displayedSegments[e.RowIndex];
			bool isSegmentColumn = e.ColumnIndex < numberOfSegments;
			if (isSegmentColumn)
			{
				if (e.ColumnIndex == segment.Segment.LevelIndex)
				{
					// Column in which segment data is set.
					if (e.ColumnIndex + 1 == numberOfSegments)
					{
						// Position data.
						if (segment.IsSegmentData)
							e.Value = segment.SegmentAsData.GetPositionId();
					}
					else
					{
						e.Value = segment.Segment.Id;
					}
				}
			}
			else
			{
				string s = "";
				// Populate data only if segment is position segment (for all others, only title is used).
				if (segment.IsSegmentData)
				{
					SegmentData data = segment.SegmentAsData;
					if (e.ColumnIndex == view.ColumnQuantity.Index) s = DoubleHelper.ToStringInvariant(data.Quantity, 3);
					if (e.ColumnIndex == view.ColumnUnitOfMeasurement.Index) s = data.UnitOfMeasurement;
					if (e.ColumnIndex == view.ColumnUnitPrice.Index) { s = DoubleHelper.ToStringInvariant(data.UnitPrice, 2, true); s = FixPricesInvariantString(s); }
					if (e.ColumnIndex == view.ColumnVat.Index) { s = DoubleHelper.ToStringInvariant(data.VatPercent, 0); s = FixPricesInvariantString(s); }
					if (e.ColumnIndex == view.ColumnTotalPrice.Index) { s = DoubleHelper.ToStringInvariant(data.TotalPrice, 2, true); s = FixPricesInvariantString(s); }
					if (IsSelectObjectsColumnUsed && e.ColumnIndex == (SelectObjectColumn.Index)) s = "+";
				}
				if (e.ColumnIndex == view.ColumnTitle.Index)
					s = segment.Segment.Title;
				if (e.ColumnIndex == view.ColumnTotalPrice.Index)
				{
					s = DoubleHelper.ToStringInvariant(segment.Segment.GetTotalPrice(), 2, true);
					s = FixPricesInvariantString(s);
				}
			e.Value = s;
			}

		}
        /// <summary>
        /// For any selected position number find sum of all quantity attributes with same position number attribute.
        /// </summary>
        public void ReadSelectedPositionsQuantity()
        {
            int[] rowIndex = view.Dgv.SelectedCells.Cast<DataGridViewCell>().Select(x => x.RowIndex).Distinct().ToArray();
            if (rowIndex.Length == 0)
            {
                MessageBox.Show("No positions are selected!", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            List<SegmentData> data = new List<SegmentData>();
            for (int i  = 0; i < rowIndex.Length; i++)
            {
                int index = rowIndex[i];
                var segment = displayedSegments[index];
                if (!segment.IsSegmentData)
                    continue;
                data.Add(segment.SegmentAsData);
            }
            PositionReader.ReadPositions(data);
            for (int i = 0; i < rowIndex.Length; i++)
                view.Dgv.InvalidateRow(rowIndex[i]);
        }
		private string FixPricesInvariantString(string d)
		{
			d = d.Replace(',', ';');
			d = d.Replace('.', ',');
			d = d.Replace(';', '.');
			return d;
		}
	}
}
