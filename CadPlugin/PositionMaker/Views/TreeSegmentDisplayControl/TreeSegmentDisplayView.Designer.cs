﻿namespace CadPlugin.PositionMaker.Views.TreeSegmentDisplay
{
	partial class TreeSegmentDisplayView
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			this.dataGridViewMain = new System.Windows.Forms.DataGridView();
			this.ColumnTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnUnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnUnitMeasurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnVat = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnTotalPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridViewMain
			// 
			this.dataGridViewMain.AllowUserToAddRows = false;
			this.dataGridViewMain.AllowUserToDeleteRows = false;
			this.dataGridViewMain.AllowUserToResizeColumns = false;
			this.dataGridViewMain.AllowUserToResizeRows = false;
			this.dataGridViewMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewMain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnTitle,
            this.ColumnQuantity,
            this.ColumnUnitPrice,
            this.ColumnUnitMeasurement,
            this.ColumnVat,
            this.ColumnTotalPrice});
			this.dataGridViewMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridViewMain.Location = new System.Drawing.Point(0, 0);
			this.dataGridViewMain.Name = "dataGridViewMain";
			this.dataGridViewMain.RowHeadersVisible = false;
			this.dataGridViewMain.Size = new System.Drawing.Size(801, 288);
			this.dataGridViewMain.TabIndex = 1;
			// 
			// ColumnTitle
			// 
			this.ColumnTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.ColumnTitle.HeaderText = "Title";
			this.ColumnTitle.Name = "ColumnTitle";
			// 
			// ColumnQuantity
			// 
			this.ColumnQuantity.HeaderText = "Quantity";
			this.ColumnQuantity.Name = "ColumnQuantity";
			this.ColumnQuantity.Width = 70;
			// 
			// ColumnUnitPrice
			// 
			this.ColumnUnitPrice.HeaderText = "Unit Price";
			this.ColumnUnitPrice.Name = "ColumnUnitPrice";
			this.ColumnUnitPrice.Width = 80;
			// 
			// ColumnUnitMeasurement
			// 
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			this.ColumnUnitMeasurement.DefaultCellStyle = dataGridViewCellStyle1;
			this.ColumnUnitMeasurement.HeaderText = "Unit";
			this.ColumnUnitMeasurement.Name = "ColumnUnitMeasurement";
			this.ColumnUnitMeasurement.Width = 40;
			// 
			// ColumnVat
			// 
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			this.ColumnVat.DefaultCellStyle = dataGridViewCellStyle2;
			this.ColumnVat.HeaderText = "Vat";
			this.ColumnVat.Name = "ColumnVat";
			this.ColumnVat.Width = 40;
			// 
			// ColumnTotalPrice
			// 
			this.ColumnTotalPrice.HeaderText = "Total Price";
			this.ColumnTotalPrice.Name = "ColumnTotalPrice";
			this.ColumnTotalPrice.ReadOnly = true;
			// 
			// TreeSegmentDisplayView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.dataGridViewMain);
			this.Name = "TreeSegmentDisplayView";
			this.Size = new System.Drawing.Size(801, 288);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridViewMain;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTitle;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnQuantity;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnUnitPrice;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnUnitMeasurement;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnVat;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTotalPrice;
	}
}
