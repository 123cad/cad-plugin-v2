﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PositionMaker.Views.Main.DisplayTypes
{
	enum ExpandState
	{
		NotSupported,
		Expanded,
		Collapsed
	}
}
