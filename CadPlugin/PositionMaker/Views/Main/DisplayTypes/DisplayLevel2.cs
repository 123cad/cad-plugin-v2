﻿using CadPlugin.PositionMaker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PositionMaker.Views.Main.DisplayTypes
{
	class DisplayLevel2
	{
		public bool IsCollapsed { get; set; }

		public PositionLevel2 SourceLevel { get; private set; }

		public DisplayLevel1 ParentLevel { get; private set; }
		public List<DisplayLevel3> SubLevels { get; } = new List<DisplayLevel3>();
		public DisplayLevel2(PositionLevel2 l, DisplayLevel1 parent)
		{
			SourceLevel = l;
			ParentLevel = parent;
		}
		public void SetExpandState(ExpandState state)
		{
			IsCollapsed = state == ExpandState.Collapsed;
		}
	}
}
