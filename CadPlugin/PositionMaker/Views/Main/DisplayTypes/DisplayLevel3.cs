﻿using CadPlugin.PositionMaker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PositionMaker.Views.Main.DisplayTypes
{
	class DisplayLevel3
	{
		public PositionLevel3 SourceLevel { get; private set; }
		public DisplayLevel2 ParentLevel { get; private set; }
		public DisplayLevel3(PositionLevel3 l, DisplayLevel2 parent)
		{
			SourceLevel = l;
			ParentLevel = parent;
		}
	}
}
