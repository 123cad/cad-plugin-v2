﻿using CadPlugin.PositionMaker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PositionMaker.Views.Main.DisplayTypes
{
	class PositionDisplayCollection
	{
		/// <summary>
		/// Single
		/// </summary>
		private class PositionRow
		{
			public DisplayLevel1 Level1 { get; }
			public DisplayLevel2 Level2 { get; }
			public DisplayLevel3 Level3 { get; }
			public PositionRow(DisplayLevel1 l1) { Level1 = l1; }
			public PositionRow(DisplayLevel2 l2) { Level2 = l2; }
			public PositionRow(DisplayLevel3 l3) { Level3 = l3; }
		}
		public enum LevelIndex
		{
			Level1,
			Level2,
			Level3
		}

		/// <summary>
		/// All position organized by levels.
		/// </summary>
		private List<DisplayLevel1> levels = new List<DisplayLevel1>();
		public IEnumerable<DisplayLevel1> Levels => levels;
		private PositionCollection source { get; set; }
		private PositionPointDefinition definition { get; set; }
		/// <summary>
		/// Data which is displayed in DGV - it is mapped to dgv row index.
		/// When level collapsed/expanded, this has to be repopulated.
		/// </summary>
		private PositionRow[] dgvRows { get; set; } = null;
		/// <summary>
		/// All all positions represented as collection (same data is in levels).
		/// </summary>
		//private List<PositionRow> allRows { get; set; }

		/// <summary>
		/// Data has been updated - refresh dependencies.
		/// </summary>
		public event Action DataUpdated;

		public PositionDisplayCollection(PositionPointDefinition def)
		{
			definition = def;
		}
		public void CreateFrom(PositionCollection source)
		{
			this.source = source;
			SourceUpdated();
		}
		/// <summary>
		/// When any operation happens which can change number of rows in DGV, run this.
		/// </summary>
		private void SourceUpdated()
		{
			levels.Clear();

			List<PositionRow> t = new List<PositionRow>();
			foreach (var l1 in source)
			{
				DisplayLevel1 dl1 = new DisplayLevel1(l1);
				levels.Add(dl1);
				t.Add(new PositionRow(dl1));
				foreach (var l2 in l1)
				{
					DisplayLevel2 dl2 = new DisplayLevel2(l2, dl1);
					dl1.Sublevels.Add(dl2);
					t.Add(new PositionRow(dl2));
					foreach (var l3 in l2)
					{
						DisplayLevel3 dl3 = new DisplayLevel3(l3, dl2);
						dl2.SubLevels.Add(dl3);
						t.Add(new PositionRow(dl3));
					}
				}
			}
			dgvRows = t.ToArray();
			DataUpdated?.Invoke();
		}

		/// <summary>
		/// Number of rows which are visible to the user (sub rows of collapsed row is not included).
		/// </summary>
		/// <returns></returns>
		public int CountVisibleRows()
		{
			return dgvRows.Length;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="row"></param>
		/// <param name="columnLevel">1 to 3 - represents segment number.</param>
		/// <returns></returns>
		public string GetRowName(int row, LevelIndex columnLevel)
		{
			if (row < 0 || dgvRows.Length <= row)
				return "";
			string name = "";
			PositionRow r = dgvRows[row];
			if (r.Level1 != null && definition.Segment1Used)
			{
				if (columnLevel == LevelIndex.Level1)
					name = definition.GetPositionName(r.Level1.SourceLevel.Id);
			}
			else if (r.Level2 != null && definition.Segment2Used)
			{
				if (columnLevel == LevelIndex.Level2)
					name = definition.GetPositionName(r.Level2.ParentLevel.SourceLevel.Id, r.Level2.SourceLevel.Id);
			}
			else if (r.Level3 != null)
			{
				if (columnLevel == LevelIndex.Level3)
				{
					name = definition.GetPositionName(
						r.Level3.ParentLevel.ParentLevel.SourceLevel.Id,
						r.Level3.ParentLevel.SourceLevel.Id,
						r.Level3.SourceLevel.Id,
						' ');
				}
			}
			return name;
		}
		public string GetRowTitle(int row)
		{
			string title = "";
			if (row < 0 || dgvRows.Length <= row)
				return "";
			PositionRow r = dgvRows[row];
			if (r.Level3 != null)
				title = r.Level3.SourceLevel.Title;
			return title;
		}
		public double GetQuantity(int row)
		{
			double total = -1;
			if (row < 0 || dgvRows.Length <= row)
				return total;
			PositionRow r = dgvRows[row];
			if (r.Level3 != null)
				total = 0;// r.Level3.SourceLevel.Quantity;
			return total;
		}

		public int GetRowLevel(int row)
		{
			int level = -1;
			if (row < 0 || dgvRows.Length <= row)
				return level;
			PositionRow r = dgvRows[row];
			if (r.Level1 != null)
				level = 1;
			else if (r.Level2 != null)
				level = 2;
			else if (r.Level3 != null)
				level = 3;
			return level;
		}
		/// <summary>
		/// Is there a "+" to add object of same kind.
		/// </summary>
		/// <param name="row"></param>
		/// <returns></returns>
		public bool CanAddNew(int row)
		{
			bool val = false;
			if (row < 0 || dgvRows.Length <= row)
				return val;
			PositionRow r = dgvRows[row];
			// Level 1 and 2 are always available for adding new element.
			if (r.Level1 != null && definition.Segment1Used 
				|| r.Level2 != null && definition.Segment2Used)
				val = true;
			if (r.Level3 != null)
			{
				// Only if it last in the sublevel of the parent (or last one at all).
				if (row == dgvRows.Length - 1
					|| dgvRows[row + 1].Level3 == null)
					val = true;
			}
			return val;
		}
		public ExpandState GetRowExpandState(int row)
		{
			ExpandState state = ExpandState.NotSupported;
			if (row < 0 || dgvRows.Length <= row)
				return state;
			PositionRow r = dgvRows[row];
			if (r.Level1 != null && definition.Segment1Used)
				state = r.Level1.IsCollapsed ? ExpandState.Collapsed : ExpandState.Expanded;
			if (r.Level2 != null && definition.Segment2Used)
				state = r.Level2.IsCollapsed ? ExpandState.Collapsed : ExpandState.Expanded;
			return state;
		}
		/// <summary>
		/// Determines what object to add, and adds it according to rules.
		/// </summary>
		/// <param name="row"></param>
		public void AddNew(int row)
		{
			if (!CanAddNew(row))
				return;
			PositionRow r = dgvRows[row];
			if (r.Level1 != null)
			{
				source.AddNewPosition(definition);
			}
			else if (r.Level2 != null)
				r.Level2.ParentLevel.SourceLevel.AddNewChild(definition);
			else if (r.Level3 != null)
				r.Level3.ParentLevel.SourceLevel.AddNewChild(definition);

			SourceUpdated();
		}

		public void ToggleExpandState(int row)
		{
			ExpandState currentState = GetRowExpandState(row);
			if (currentState == ExpandState.NotSupported)
				return;
			ExpandState newState = currentState == ExpandState.Collapsed ? ExpandState.Expanded : ExpandState.Collapsed;
			PositionRow r = dgvRows[row];
			if (r.Level1 != null)
				r.Level1.IsCollapsed = newState == ExpandState.Collapsed;
			if (r.Level2 != null)
				r.Level2.IsCollapsed = newState == ExpandState.Collapsed;
			SourceUpdated();
		}
	}
}
