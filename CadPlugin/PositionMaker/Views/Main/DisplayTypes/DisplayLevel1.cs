﻿using CadPlugin.PositionMaker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PositionMaker.Views.Main.DisplayTypes
{
	class DisplayLevel1
	{
		public bool IsCollapsed { get; set; }

		public PositionLevel1 SourceLevel { get; private set; }

		public List<DisplayLevel2> Sublevels { get; } = new List<DisplayLevel2>(); 

		public DisplayLevel1(PositionLevel1 l)
		{
			SourceLevel = l;
		}
		public void SetExpandedState(ExpandState state)
		{
			IsCollapsed = state == ExpandState.Collapsed;
		}
	}
}
