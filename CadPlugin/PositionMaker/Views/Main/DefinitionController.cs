﻿using CadPlugin.PositionMaker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views.Main
{
	/// <summary>
	/// Handles segment definition part.
	/// </summary>
	public class DefinitionController
	{
		private IMainView view { get; }
		private PositionPointDefinition positionDefinition { get; }
		public DefinitionController(IMainView v, PositionPointDefinition definition)
		{
			view = v;
			positionDefinition = definition;
			Initialize();
			UpdatePreview();
		}
		private void Initialize()
		{
			Func<string, object, string, Binding> createBinding = (property, dataObject, dataProperty) =>
			{
				Binding bd = new Binding(property, dataObject, dataProperty);
				bd.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
				bd.FormattingEnabled = true;
				return bd;
			};
			var pd = positionDefinition;
			Binding b;
			Action<PositionNumberSegment, ConvertEventArgs> formatStart = (seg, e) => e.Value = seg.FormatNumber(seg.StartValue);
			Action<PositionNumberSegment, ConvertEventArgs> parseStart = (seg, e) =>
			{
				int t;
				string s = e.Value as string;
				s = s ?? "";
				if (!string.IsNullOrEmpty(s) && int.TryParse((e.Value as string) ?? "", out t))
				{
					e.Value = t;
					seg.NumberOfLeading0 = s.Trim().Length - t.ToString().Length;
					seg.IsUsed = true;
				}
				else
					seg.IsUsed = false;
			};
			BindingCompleteEventHandler bindingComplete = (_, bind) => 
			{
				if (bind.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate)
					UpdatePreview();
			};
			// Prepare Start fields.
			/*var segment1 = positionDefinition.Segment1;
			b = createBinding(nameof(TextBox.Text), segment1, nameof(PositionNumberSegment.StartValue));
			b.Format += (_, e) => formatStart(segment1, e);
			b.Parse += (_, e) => parseStart(segment1, e);
			b.BindingComplete += bindingComplete;
			view.StartSegment1.DataBindings.Add(b);

			var segment2 = positionDefinition.Segment2;
			b = createBinding(nameof(TextBox.Text), segment2, nameof(PositionNumberSegment.StartValue));
			b.Format += (_, e) => formatStart(segment2, e);
			b.Parse += (_, e) => parseStart(segment2, e);
			b.BindingComplete += bindingComplete;
			view.StartSegment2.DataBindings.Add(b);

			var segment3 = positionDefinition.Segment3;
			b = createBinding(nameof(TextBox.Text), segment3, nameof(PositionNumberSegment.StartValue));
			b.Format += (_, e) => formatStart(segment3, e);
			b.Parse += (_, e) => parseStart(segment3, e);
			b.BindingComplete += bindingComplete;
			view.StartSegment3.DataBindings.Add(b);

			var segmentChar = positionDefinition.Segment4;
			b = createBinding(nameof(TextBox.Text), segmentChar, nameof(PositionCharSegment.StartValue));
			b.Format += (_, e) => {  };
			b.Parse += (_, e) =>
			{
				string s = e.Value as string;
				if (string.IsNullOrEmpty(s))
					segmentChar.IsUsed = false;
				else
					segmentChar.IsUsed = true;
			};
			b.BindingComplete += bindingComplete;
			view.StartSegment4.DataBindings.Add(b);

			// Prepare Increment fields.
			Action<PositionNumberSegment, ConvertEventArgs> formatIncrement = (seg, e) =>
			{

			};
			Action<PositionNumberSegment, ConvertEventArgs> parseIncrement = (seg, e) =>
			{
				int i;
				string s = (e.Value as string) ?? "";
				if (s != "" && int.TryParse(s, out i))
				{
					e.Value = i;
					seg.IsUsed = true;
				}
				else
					seg.IsUsed = false;
			};
			segment1 = positionDefinition.Segment1;
			b = createBinding(nameof(TextBox.Text), segment1, nameof(PositionNumberSegment.Increment));
			b.Format += (_, e) => formatIncrement(segment1, e);
			b.Parse += (_, e) => parseIncrement(segment1, e);
			b.BindingComplete += bindingComplete;
			view.IncrementSegment1.DataBindings.Add(b);

			segment1 = positionDefinition.Segment2;
			b = createBinding(nameof(TextBox.Text), segment1, nameof(PositionNumberSegment.Increment));
			b.Format += (_, e) => formatIncrement(segment1, e);
			b.Parse += (_, e) => parseIncrement(segment1, e);
			b.BindingComplete += bindingComplete;
			view.IncrementSegment2.DataBindings.Add(b);

			segment1 = positionDefinition.Segment3;
			b = createBinding(nameof(TextBox.Text), segment1, nameof(PositionNumberSegment.Increment));
			b.Format += (_, e) => formatIncrement(segment1, e);
			b.Parse += (_, e) => parseIncrement(segment1, e);
			b.BindingComplete += bindingComplete;
			view.IncrementSegment3.DataBindings.Add(b);
			*/
			view.StartSegment1.Text = pd.Segment1.FormatNumber(pd.Segment1.StartValue);
			view.StartSegment2.Text = pd.Segment2.FormatNumber(pd.Segment2.StartValue);
			view.StartSegment3.Text = pd.Segment3.FormatNumber(pd.Segment3.StartValue);
			view.StartSegment4.Text = pd.Segment4.StartValue.ToString();

			view.IncrementSegment1.Text = pd.Segment1.Increment.ToString();
			view.IncrementSegment2.Text = pd.Segment2.Increment.ToString();
			view.IncrementSegment3.Text = pd.Segment3.Increment.ToString();
			//view.IncrementSegment4.Text = pd.Segment4.Increment.ToString();

			Action<TextBox, PositionNumberSegment> segmentTextChanged = (tbox, seg) =>
			{
				string val = tbox.Text;
				if (string.IsNullOrEmpty(val))
					seg.IsUsed = false;
				else
				{
					seg.IsUsed = true;
					seg.SetFromFormattedValue(val);
				}
				UpdatePreview();
			};
			Action<TextBox, PositionNumberSegment> lostFocus = (tbox, seg) =>
			{
				string val = tbox.Text;
				if (string.IsNullOrEmpty(val))
					tbox.Text = pd.Segment1.StartValue.ToString();
			};

			view.StartSegment1.TextChanged += (_, e) => segmentTextChanged((TextBox)_, pd.Segment1);
			view.StartSegment1.LostFocus += (_, e) => lostFocus((TextBox)_, pd.Segment1);

			view.StartSegment2.TextChanged += (_, e) => segmentTextChanged((TextBox)_, pd.Segment2);
			view.StartSegment2.LostFocus += (_, e) => lostFocus((TextBox)_, pd.Segment2);

			view.StartSegment3.TextChanged += (_, e) => segmentTextChanged((TextBox)_, pd.Segment3);
			view.StartSegment3.LostFocus += (_, e) => lostFocus((TextBox)_, pd.Segment3);
			
			view.StartSegment4.TextChanged += (_, e) =>
			{
				TextBox t = (TextBox)_;
				string val = t.Text;
				pd.Segment4.StartValue = string.IsNullOrEmpty(val)?PositionCharSegment.DefaultValue:val.First();
				pd.Segment4.IsUsed = (val ?? "").Length > 0;
				UpdatePreview();
			};
			view.StartSegment4.LostFocus += (_, e) =>
			{
				TextBox t = (TextBox)_;
				string val = t.Text;
				if (string.IsNullOrEmpty(val))
					t.Text = pd.Segment4.StartValue.ToString();
			};

			view.IncrementSegment1.TextChanged += (_, e) =>
			{
				TextBox t = (TextBox)_;
				view.ErrorProvider.SetError(t, null);
				var s = view.IncrementSegment1.Text;
				s = s ?? "";
				int i;
				if (int.TryParse(s, out i) && pd.Segment1.IsIncrementAllowed(i))
					pd.Segment1.Increment = i;
				else
					view.ErrorProvider.SetError(t, "Value not valid or increment not allowed!");
				UpdatePreview();
			};
			view.IncrementSegment2.TextChanged += (_, e) =>
			{
				TextBox t = (TextBox)_;
				view.ErrorProvider.SetError(t, null);
				var s = view.IncrementSegment2.Text;
				s = s ?? "";
				int i;
				if (int.TryParse(s, out i) && pd.Segment2.IsIncrementAllowed(i))
					pd.Segment2.Increment = i;
				else
					view.ErrorProvider.SetError(t, "Value not valid or increment not allowed!");
				UpdatePreview();
			};
			view.IncrementSegment3.TextChanged += (_, e) =>
			{
				TextBox t = (TextBox)_;
				view.ErrorProvider.SetError(t, null);
				var s = view.IncrementSegment3.Text;
				s = s ?? "";
				int i;
				if (int.TryParse(s, out i) && pd.Segment3.IsIncrementAllowed(i))
					pd.Segment3.Increment = i;
				else
					view.ErrorProvider.SetError(t, "Value not valid or increment not allowed!");
				UpdatePreview();
			};

			Binding checkBind = createBinding(nameof(CheckBox.Checked), pd, nameof(PositionPointDefinition.Increment4));
			checkBind.Parse += (_, e) =>
			 {
				 Console.WriteLine(e.Value);
			 };
			view.CheckIncrementSegment4.DataBindings.Add(checkBind);

			

			// Skip non digit characters.
			KeyPressEventHandler allowOnlyNumbers = (_, e) =>
			{
				if (!char.IsNumber(e.KeyChar) && ! char.IsControl(e.KeyChar))
					e.Handled = true;
			};
			view.StartSegment1.KeyPress += allowOnlyNumbers;
			view.StartSegment2.KeyPress += allowOnlyNumbers;
			view.StartSegment3.KeyPress += allowOnlyNumbers;
			view.IncrementSegment1.KeyPress += allowOnlyNumbers;
			view.IncrementSegment2.KeyPress += allowOnlyNumbers;
			view.IncrementSegment3.KeyPress += allowOnlyNumbers;
			//view.IncrementSegment4.KeyPress += allowOnlyNumbers;
			
		}
		private void UpdatePreview()
		{
			string previewStart = null;
			int previewCount = 3;
			PositionCollection coll = new PositionCollection();
			coll.AddNewPosition(positionDefinition);
			if (positionDefinition.Segment1Used)
			{
				view.PreviewSegment1 = coll.PreviewNextAvailableLevel1Id(positionDefinition, previewCount).Select(x=>x.ToString()).ToList();
				previewStart += (previewStart != null ? "." : "") + positionDefinition.Segment1.FormatNumber(positionDefinition.Segment1.StartValue);
			}
			if (positionDefinition.Segment2Used)
			{
				view.PreviewSegment2 = coll.First().PreviewNextAvailableLevel2Id(positionDefinition, previewCount).Select(x => x.ToString()).ToList();
				previewStart += (previewStart != null ? "." : "") + positionDefinition.Segment2.FormatNumber(positionDefinition.Segment2.StartValue);
			}
			if (positionDefinition.Segment3Used)
			{
				view.PreviewSegment3 = coll.First().First().PreviewNextAvailableLevel3Id(positionDefinition, previewCount).Select(x => x.ToString()).ToList();
				previewStart += (previewStart != null ? "." : "") + positionDefinition.Segment3.FormatNumber(positionDefinition.Segment3.StartValue);
			}
			if (positionDefinition.Segment4Used)
			{
				view.PreviewSegment4 = coll.First().First().First().PreviewNextAvailableLevel4Id(positionDefinition, previewCount).Select(x => x.ToString()).ToList();
				previewStart += (previewStart != null ? "." : "") + view.PreviewSegment4.First();
			}
			view.StartPreview = previewStart;
		}
	}
}
