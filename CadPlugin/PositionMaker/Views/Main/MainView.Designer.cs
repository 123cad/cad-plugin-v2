﻿namespace CadPlugin.PositionMaker.Views.Main
{
	partial class MainView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.textBoxIncrement4 = new System.Windows.Forms.TextBox();
			this.textBoxIncrement3 = new System.Windows.Forms.TextBox();
			this.textBoxIncrement2 = new System.Windows.Forms.TextBox();
			this.textBoxIncrement1 = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.textBoxStart4 = new System.Windows.Forms.TextBox();
			this.textBoxStart3 = new System.Windows.Forms.TextBox();
			this.textBoxStart2 = new System.Windows.Forms.TextBox();
			this.textBoxStart1 = new System.Windows.Forms.TextBox();
			this.label21 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.checkBoxIncrement4 = new System.Windows.Forms.CheckBox();
			this.label18 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.listBoxPreview4 = new System.Windows.Forms.ListBox();
			this.listBoxPreview3 = new System.Windows.Forms.ListBox();
			this.listBoxPreview2 = new System.Windows.Forms.ListBox();
			this.listBoxPreview1 = new System.Windows.Forms.ListBox();
			this.label8 = new System.Windows.Forms.Label();
			this.textBoxStartPreview = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
			this.dataGridView = new System.Windows.Forms.DataGridView();
			this.ColumnS1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnS2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnS3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnS4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.buttonAddSegment1 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.buttonAddSegment2 = new System.Windows.Forms.Button();
			this.buttonAddSegment3 = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.labelNextSegment = new System.Windows.Forms.Label();
			this.textBoxSegment1 = new System.Windows.Forms.TextBox();
			this.textBoxSegment2 = new System.Windows.Forms.TextBox();
			this.toolTip = new System.Windows.Forms.ToolTip(this.components);
			this.buttonDeletePosition = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.textBoxSegment3 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.buttonAddSegment4 = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// textBoxIncrement4
			// 
			this.textBoxIncrement4.Enabled = false;
			this.textBoxIncrement4.Location = new System.Drawing.Point(277, 74);
			this.textBoxIncrement4.Name = "textBoxIncrement4";
			this.textBoxIncrement4.Size = new System.Drawing.Size(22, 20);
			this.textBoxIncrement4.TabIndex = 79;
			this.textBoxIncrement4.Text = "1";
			this.textBoxIncrement4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.textBoxIncrement4.Visible = false;
			// 
			// textBoxIncrement3
			// 
			this.textBoxIncrement3.Location = new System.Drawing.Point(213, 74);
			this.textBoxIncrement3.Name = "textBoxIncrement3";
			this.textBoxIncrement3.Size = new System.Drawing.Size(41, 20);
			this.textBoxIncrement3.TabIndex = 78;
			this.textBoxIncrement3.Text = "10";
			this.textBoxIncrement3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxIncrement2
			// 
			this.textBoxIncrement2.Location = new System.Drawing.Point(155, 74);
			this.textBoxIncrement2.Name = "textBoxIncrement2";
			this.textBoxIncrement2.Size = new System.Drawing.Size(31, 20);
			this.textBoxIncrement2.TabIndex = 77;
			this.textBoxIncrement2.Text = "1";
			this.textBoxIncrement2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxIncrement1
			// 
			this.textBoxIncrement1.Location = new System.Drawing.Point(97, 74);
			this.textBoxIncrement1.Name = "textBoxIncrement1";
			this.textBoxIncrement1.Size = new System.Drawing.Size(31, 20);
			this.textBoxIncrement1.TabIndex = 76;
			this.textBoxIncrement1.Text = "1";
			this.textBoxIncrement1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(27, 74);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(54, 13);
			this.label16.TabIndex = 72;
			this.label16.Text = "Increment";
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(52, 46);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(29, 13);
			this.label20.TabIndex = 68;
			this.label20.Text = "Start";
			// 
			// textBoxStart4
			// 
			this.textBoxStart4.Location = new System.Drawing.Point(277, 43);
			this.textBoxStart4.MaxLength = 1;
			this.textBoxStart4.Name = "textBoxStart4";
			this.textBoxStart4.Size = new System.Drawing.Size(22, 20);
			this.textBoxStart4.TabIndex = 67;
			this.textBoxStart4.Text = "a";
			this.textBoxStart4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxStart3
			// 
			this.textBoxStart3.Location = new System.Drawing.Point(213, 43);
			this.textBoxStart3.MaxLength = 4;
			this.textBoxStart3.Name = "textBoxStart3";
			this.textBoxStart3.Size = new System.Drawing.Size(41, 20);
			this.textBoxStart3.TabIndex = 66;
			this.textBoxStart3.Text = "0000";
			this.textBoxStart3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxStart2
			// 
			this.textBoxStart2.Location = new System.Drawing.Point(155, 43);
			this.textBoxStart2.MaxLength = 2;
			this.textBoxStart2.Name = "textBoxStart2";
			this.textBoxStart2.Size = new System.Drawing.Size(31, 20);
			this.textBoxStart2.TabIndex = 65;
			this.textBoxStart2.Text = "00";
			this.textBoxStart2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// textBoxStart1
			// 
			this.textBoxStart1.Location = new System.Drawing.Point(97, 43);
			this.textBoxStart1.MaxLength = 2;
			this.textBoxStart1.Name = "textBoxStart1";
			this.textBoxStart1.Size = new System.Drawing.Size(31, 20);
			this.textBoxStart1.TabIndex = 64;
			this.textBoxStart1.Text = "00";
			this.textBoxStart1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(281, 24);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(13, 13);
			this.label21.TabIndex = 63;
			this.label21.Text = "4";
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(227, 24);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(13, 13);
			this.label22.TabIndex = 62;
			this.label22.Text = "3";
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Location = new System.Drawing.Point(164, 24);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(13, 13);
			this.label23.TabIndex = 61;
			this.label23.Text = "2";
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Location = new System.Drawing.Point(103, 24);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(13, 13);
			this.label24.TabIndex = 60;
			this.label24.Text = "1";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.checkBoxIncrement4);
			this.groupBox1.Controls.Add(this.label18);
			this.groupBox1.Controls.Add(this.label17);
			this.groupBox1.Controls.Add(this.label15);
			this.groupBox1.Controls.Add(this.listBoxPreview4);
			this.groupBox1.Controls.Add(this.listBoxPreview3);
			this.groupBox1.Controls.Add(this.listBoxPreview2);
			this.groupBox1.Controls.Add(this.listBoxPreview1);
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.textBoxStartPreview);
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.label23);
			this.groupBox1.Controls.Add(this.textBoxIncrement4);
			this.groupBox1.Controls.Add(this.label24);
			this.groupBox1.Controls.Add(this.textBoxIncrement3);
			this.groupBox1.Controls.Add(this.label22);
			this.groupBox1.Controls.Add(this.textBoxIncrement2);
			this.groupBox1.Controls.Add(this.label21);
			this.groupBox1.Controls.Add(this.textBoxIncrement1);
			this.groupBox1.Controls.Add(this.textBoxStart1);
			this.groupBox1.Controls.Add(this.label16);
			this.groupBox1.Controls.Add(this.textBoxStart2);
			this.groupBox1.Controls.Add(this.label20);
			this.groupBox1.Controls.Add(this.textBoxStart3);
			this.groupBox1.Controls.Add(this.textBoxStart4);
			this.groupBox1.Location = new System.Drawing.Point(44, 36);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(327, 208);
			this.groupBox1.TabIndex = 80;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Segment definition";
			// 
			// checkBoxIncrement4
			// 
			this.checkBoxIncrement4.AutoSize = true;
			this.checkBoxIncrement4.Location = new System.Drawing.Point(277, 77);
			this.checkBoxIncrement4.Name = "checkBoxIncrement4";
			this.checkBoxIncrement4.Size = new System.Drawing.Size(38, 17);
			this.checkBoxIncrement4.TabIndex = 91;
			this.checkBoxIncrement4.Text = "+1";
			this.toolTip.SetToolTip(this.checkBoxIncrement4, "Automatically increment for new position.");
			this.checkBoxIncrement4.UseVisualStyleBackColor = true;
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label18.Location = new System.Drawing.Point(258, 114);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(17, 25);
			this.label18.TabIndex = 90;
			this.label18.Text = ".";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label17.Location = new System.Drawing.Point(191, 115);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(17, 25);
			this.label17.TabIndex = 89;
			this.label17.Text = ".";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label15.Location = new System.Drawing.Point(133, 115);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(17, 25);
			this.label15.TabIndex = 88;
			this.label15.Text = ".";
			// 
			// listBoxPreview4
			// 
			this.listBoxPreview4.BackColor = System.Drawing.SystemColors.MenuBar;
			this.listBoxPreview4.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.listBoxPreview4.Enabled = false;
			this.listBoxPreview4.FormattingEnabled = true;
			this.listBoxPreview4.Items.AddRange(new object[] {
            "a",
            "a",
            "a"});
			this.listBoxPreview4.Location = new System.Drawing.Point(284, 112);
			this.listBoxPreview4.Name = "listBoxPreview4";
			this.listBoxPreview4.Size = new System.Drawing.Size(14, 39);
			this.listBoxPreview4.TabIndex = 87;
			// 
			// listBoxPreview3
			// 
			this.listBoxPreview3.BackColor = System.Drawing.SystemColors.MenuBar;
			this.listBoxPreview3.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.listBoxPreview3.Enabled = false;
			this.listBoxPreview3.FormattingEnabled = true;
			this.listBoxPreview3.Items.AddRange(new object[] {
            "0000",
            "0010",
            "0020"});
			this.listBoxPreview3.Location = new System.Drawing.Point(221, 113);
			this.listBoxPreview3.Name = "listBoxPreview3";
			this.listBoxPreview3.Size = new System.Drawing.Size(33, 39);
			this.listBoxPreview3.TabIndex = 86;
			// 
			// listBoxPreview2
			// 
			this.listBoxPreview2.BackColor = System.Drawing.SystemColors.MenuBar;
			this.listBoxPreview2.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.listBoxPreview2.Enabled = false;
			this.listBoxPreview2.FormattingEnabled = true;
			this.listBoxPreview2.Items.AddRange(new object[] {
            "00",
            "01",
            "02"});
			this.listBoxPreview2.Location = new System.Drawing.Point(163, 113);
			this.listBoxPreview2.Name = "listBoxPreview2";
			this.listBoxPreview2.Size = new System.Drawing.Size(23, 39);
			this.listBoxPreview2.TabIndex = 85;
			// 
			// listBoxPreview1
			// 
			this.listBoxPreview1.BackColor = System.Drawing.SystemColors.MenuBar;
			this.listBoxPreview1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.listBoxPreview1.Enabled = false;
			this.listBoxPreview1.FormattingEnabled = true;
			this.listBoxPreview1.Items.AddRange(new object[] {
            "00",
            "01",
            "02"});
			this.listBoxPreview1.Location = new System.Drawing.Point(104, 113);
			this.listBoxPreview1.Name = "listBoxPreview1";
			this.listBoxPreview1.Size = new System.Drawing.Size(26, 39);
			this.listBoxPreview1.TabIndex = 84;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(36, 127);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(45, 13);
			this.label8.TabIndex = 83;
			this.label8.Text = "Preview";
			// 
			// textBoxStartPreview
			// 
			this.textBoxStartPreview.Location = new System.Drawing.Point(97, 171);
			this.textBoxStartPreview.Name = "textBoxStartPreview";
			this.textBoxStartPreview.ReadOnly = true;
			this.textBoxStartPreview.Size = new System.Drawing.Size(100, 20);
			this.textBoxStartPreview.TabIndex = 81;
			this.textBoxStartPreview.TabStop = false;
			this.textBoxStartPreview.Text = "01.01.0001.a";
			this.textBoxStartPreview.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(12, 174);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(68, 13);
			this.label7.TabIndex = 80;
			this.label7.Text = "Start position";
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			// 
			// dataGridView
			// 
			this.dataGridView.AllowUserToDeleteRows = false;
			this.dataGridView.AllowUserToResizeColumns = false;
			this.dataGridView.AllowUserToResizeRows = false;
			this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnS1,
            this.ColumnS2,
            this.ColumnS3,
            this.ColumnS4,
            this.ColumnTitle});
			this.dataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
			this.dataGridView.Location = new System.Drawing.Point(44, 283);
			this.dataGridView.MultiSelect = false;
			this.dataGridView.Name = "dataGridView";
			this.dataGridView.RowHeadersVisible = false;
			this.dataGridView.Size = new System.Drawing.Size(395, 181);
			this.dataGridView.TabIndex = 81;
			// 
			// ColumnS1
			// 
			this.ColumnS1.HeaderText = "S1";
			this.ColumnS1.Name = "ColumnS1";
			this.ColumnS1.ReadOnly = true;
			this.ColumnS1.Width = 40;
			// 
			// ColumnS2
			// 
			this.ColumnS2.HeaderText = "S2";
			this.ColumnS2.Name = "ColumnS2";
			this.ColumnS2.ReadOnly = true;
			this.ColumnS2.Width = 35;
			// 
			// ColumnS3
			// 
			this.ColumnS3.HeaderText = "S3";
			this.ColumnS3.Name = "ColumnS3";
			this.ColumnS3.Width = 50;
			// 
			// ColumnS4
			// 
			this.ColumnS4.HeaderText = "S4";
			this.ColumnS4.Name = "ColumnS4";
			this.ColumnS4.Width = 80;
			// 
			// ColumnTitle
			// 
			this.ColumnTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.ColumnTitle.HeaderText = "Title";
			this.ColumnTitle.MinimumWidth = 60;
			this.ColumnTitle.Name = "ColumnTitle";
			// 
			// buttonAddSegment1
			// 
			this.buttonAddSegment1.Location = new System.Drawing.Point(47, 71);
			this.buttonAddSegment1.Name = "buttonAddSegment1";
			this.buttonAddSegment1.Size = new System.Drawing.Size(55, 23);
			this.buttonAddSegment1.TabIndex = 82;
			this.buttonAddSegment1.Text = "Add S1";
			this.buttonAddSegment1.UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(111, 17);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(58, 13);
			this.label1.TabIndex = 84;
			this.label1.Text = "Segment 1";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(182, 17);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(58, 13);
			this.label2.TabIndex = 87;
			this.label2.Text = "Segment 2";
			// 
			// buttonAddSegment2
			// 
			this.buttonAddSegment2.Location = new System.Drawing.Point(114, 71);
			this.buttonAddSegment2.Name = "buttonAddSegment2";
			this.buttonAddSegment2.Size = new System.Drawing.Size(55, 23);
			this.buttonAddSegment2.TabIndex = 85;
			this.buttonAddSegment2.Text = "Add S2";
			this.buttonAddSegment2.UseVisualStyleBackColor = true;
			// 
			// buttonAddSegment3
			// 
			this.buttonAddSegment3.Location = new System.Drawing.Point(185, 71);
			this.buttonAddSegment3.Name = "buttonAddSegment3";
			this.buttonAddSegment3.Size = new System.Drawing.Size(55, 23);
			this.buttonAddSegment3.TabIndex = 88;
			this.buttonAddSegment3.Text = "Add S3";
			this.buttonAddSegment3.UseVisualStyleBackColor = true;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(21, 112);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(115, 13);
			this.label4.TabIndex = 91;
			this.label4.Text = "Next segment preview:";
			// 
			// labelNextSegment
			// 
			this.labelNextSegment.AutoSize = true;
			this.labelNextSegment.Location = new System.Drawing.Point(149, 112);
			this.labelNextSegment.Name = "labelNextSegment";
			this.labelNextSegment.Size = new System.Drawing.Size(70, 13);
			this.labelNextSegment.TabIndex = 92;
			this.labelNextSegment.Text = "01.01.0001.a";
			// 
			// textBoxSegment1
			// 
			this.textBoxSegment1.Location = new System.Drawing.Point(114, 37);
			this.textBoxSegment1.Name = "textBoxSegment1";
			this.textBoxSegment1.ReadOnly = true;
			this.textBoxSegment1.Size = new System.Drawing.Size(55, 20);
			this.textBoxSegment1.TabIndex = 93;
			// 
			// textBoxSegment2
			// 
			this.textBoxSegment2.Location = new System.Drawing.Point(185, 37);
			this.textBoxSegment2.Name = "textBoxSegment2";
			this.textBoxSegment2.ReadOnly = true;
			this.textBoxSegment2.Size = new System.Drawing.Size(55, 20);
			this.textBoxSegment2.TabIndex = 94;
			// 
			// buttonDeletePosition
			// 
			this.buttonDeletePosition.Location = new System.Drawing.Point(479, 429);
			this.buttonDeletePosition.Name = "buttonDeletePosition";
			this.buttonDeletePosition.Size = new System.Drawing.Size(105, 23);
			this.buttonDeletePosition.TabIndex = 95;
			this.buttonDeletePosition.Text = "Delete current";
			this.buttonDeletePosition.UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.textBoxSegment3);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.buttonAddSegment4);
			this.groupBox2.Controls.Add(this.buttonAddSegment2);
			this.groupBox2.Controls.Add(this.buttonAddSegment1);
			this.groupBox2.Controls.Add(this.textBoxSegment2);
			this.groupBox2.Controls.Add(this.label1);
			this.groupBox2.Controls.Add(this.textBoxSegment1);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Controls.Add(this.labelNextSegment);
			this.groupBox2.Controls.Add(this.buttonAddSegment3);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Location = new System.Drawing.Point(479, 277);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(354, 133);
			this.groupBox2.TabIndex = 96;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Add Position";
			// 
			// textBoxSegment3
			// 
			this.textBoxSegment3.Location = new System.Drawing.Point(257, 37);
			this.textBoxSegment3.Name = "textBoxSegment3";
			this.textBoxSegment3.ReadOnly = true;
			this.textBoxSegment3.Size = new System.Drawing.Size(55, 20);
			this.textBoxSegment3.TabIndex = 97;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(254, 17);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(58, 13);
			this.label3.TabIndex = 95;
			this.label3.Text = "Segment 3";
			// 
			// buttonAddSegment4
			// 
			this.buttonAddSegment4.Location = new System.Drawing.Point(257, 71);
			this.buttonAddSegment4.Name = "buttonAddSegment4";
			this.buttonAddSegment4.Size = new System.Drawing.Size(55, 23);
			this.buttonAddSegment4.TabIndex = 96;
			this.buttonAddSegment4.Text = "Add S4";
			this.buttonAddSegment4.UseVisualStyleBackColor = true;
			// 
			// MainView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(886, 476);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.buttonDeletePosition);
			this.Controls.Add(this.dataGridView);
			this.Controls.Add(this.groupBox1);
			this.Name = "MainView";
			this.Text = "MainView";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.TextBox textBoxIncrement4;
		private System.Windows.Forms.TextBox textBoxIncrement3;
		private System.Windows.Forms.TextBox textBoxIncrement2;
		private System.Windows.Forms.TextBox textBoxIncrement1;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.TextBox textBoxStart4;
		private System.Windows.Forms.TextBox textBoxStart3;
		private System.Windows.Forms.TextBox textBoxStart2;
		private System.Windows.Forms.TextBox textBoxStart1;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox textBoxStartPreview;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.ListBox listBoxPreview4;
		private System.Windows.Forms.ListBox listBoxPreview3;
		private System.Windows.Forms.ListBox listBoxPreview2;
		private System.Windows.Forms.ListBox listBoxPreview1;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.ErrorProvider errorProvider1;
		private System.Windows.Forms.DataGridView dataGridView;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
		private System.Windows.Forms.Button buttonAddSegment3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button buttonAddSegment2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button buttonAddSegment1;
		private System.Windows.Forms.Label labelNextSegment;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBoxSegment2;
		private System.Windows.Forms.TextBox textBoxSegment1;
		private System.Windows.Forms.ToolTip toolTip;
		private System.Windows.Forms.Button buttonDeletePosition;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox textBoxSegment3;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button buttonAddSegment4;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnS1;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnS2;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnS3;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnS4;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTitle;
		private System.Windows.Forms.CheckBox checkBoxIncrement4;
	}
}