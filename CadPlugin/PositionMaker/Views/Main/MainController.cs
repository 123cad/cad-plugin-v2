﻿using CadPlugin.PositionMaker.Models;
using CadPlugin.PositionMaker.Views.Main.DisplayTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views.Main
{
	public class MainController
	{
		private class PositionRow
		{
			public PositionLevel1 Level1 { get; }
			public PositionLevel2 Level2 { get; }
			public PositionLevel3 Level3 { get; }
			public PositionLevel4 Level4 { get; }
			public PositionRow(PositionLevel1 l1) { Level1 = l1; }
			public PositionRow(PositionLevel2 l2) { Level2 = l2; }
			public PositionRow(PositionLevel3 l3) { Level3 = l3; }
			public PositionRow(PositionLevel4 l4) { Level4 = l4; }
		}

		private IMainView view { get; set; }
		private DataGridView dgv { get; set; }
		private PositionCollection positions { get; set; }
		private PositionPointDefinition definition { get; set; }
		//private PositionDisplayCollection displayCollection { get; set; }

		private List<PositionRow> rowData { get; } = new List<PositionRow>();

		private PositionLevel1 SelectedLevel1 { get; set; }
		private PositionLevel2 SelectedLevel2 { get; set; }
		private PositionLevel3 SelectedLevel3 { get; set; }

		public MainController(IMainView v, PositionCollection pos, PositionPointDefinition definition)
		{
			view = v;
			dgv = view.Data;
			positions = pos;
			this.definition = definition;
			//displayCollection = new PositionDisplayCollection(definition);
			//displayCollection.CreateFrom(pos);
			//displayCollection.DataUpdated += () =>
			//{
			//	dgv.RowCount = displayCollection.CountVisibleRows();
			//	dgv.Refresh();
			//};

			InitializeData();

			dgv.CurrentCellChanged += (_, e) =>
			{
				if (dgv.CurrentCell == null || dgv.CurrentCell.RowIndex == -1)
				{
					SelectedLevel1 = null;
					SelectedLevel2 = null;
					view.TextBoxSegment1.Text = "";
					view.TextBoxSegment2.Text = "";
					return;
				}
				int row = dgv.CurrentCell.RowIndex;
				var data = rowData[row];
				if (data.Level1!= null)
				{
					SelectedLevel1 = data.Level1;
					SelectedLevel2 = null;
				}
				else if (data.Level2 != null)
				{
					SelectedLevel1 = data.Level2.Parent;
					SelectedLevel2 = data.Level2;
				}
				else if (data.Level3 != null)
				{
					SelectedLevel1 = data.Level3.Parent.Parent;
					SelectedLevel2 = data.Level3.Parent;
				}
				else if (data.Level4 != null)
				{
					SelectedLevel1 = data.Level4.Parent.Parent.Parent;
					SelectedLevel2 = data.Level4.Parent.Parent;
					SelectedLevel3 = data.Level4.Parent;
				}
				else
				{
					SelectedLevel1 = null;
					SelectedLevel2 = null;
					SelectedLevel3 = null;
				}
				if (SelectedLevel1 != null)
					view.TextBoxSegment1.Text = definition.GetPositionName(SelectedLevel1.Id);
				else
					view.TextBoxSegment1.Text = "";
				if (SelectedLevel2 != null)
					view.TextBoxSegment2.Text = definition.GetPositionName(null, SelectedLevel2.Id);
				else
					view.TextBoxSegment2.Text = "";
				if (SelectedLevel3 != null)
					view.TextBoxSegment3.Text = definition.GetPositionName(null, SelectedLevel3.Id);
				else
					view.TextBoxSegment3.Text = "";
			};

			view.AddSegment1.Click += AddSegment1_Click;
			view.AddSegment2.Click += AddSegment2_Click;
			view.AddSegment3.Click += AddSegment3_Click;
			view.AddSegment4.Click += AddSegment4_Click;

			view.AddSegment1.MouseEnter += AddSegment_MouseEnter;
			view.AddSegment1.MouseLeave += AddSegment_MouseLeave;
			view.AddSegment2.MouseEnter += AddSegment_MouseEnter;
			view.AddSegment2.MouseLeave += AddSegment_MouseLeave;
			view.AddSegment3.MouseEnter += AddSegment_MouseEnter;
			view.AddSegment3.MouseLeave += AddSegment_MouseLeave;
			view.AddSegment4.MouseEnter += AddSegment_MouseEnter;
			view.AddSegment4.MouseLeave += AddSegment_MouseLeave;

			view.DeletePosition.Click += DeletePosition_Click;
		}


		private void DeletePosition_Click(object sender, EventArgs e)
		{
			if (view.Data.CurrentCell == null)
				return;
			int row = view.Data.CurrentCell.RowIndex;
			if (row == -1)
				return;

			var data = rowData[row];
			if (data.Level1 != null)
				positions.DeleteChild(data.Level1);
			else if (data.Level2 != null)
				data.Level2.Parent.DeleteChild(data.Level2);
			else if (data.Level3 != null)
				data.Level3.Parent.DeleteChild(data.Level3);
			else if (data.Level4 != null)
				data.Level4.Parent.DeleteChild(data.Level4);
			RefreshSource();
		}

		private void AddSegment_MouseLeave(object sender, EventArgs e)
		{
			view.NextSegmentPreview = "";
			view.ToolTip.RemoveAll();
		}

		private void RefreshPositionNamePreview(Button bt)
		{
			string s = "";
			bool valid = true;
			string errorMessage = "";
			int s1 = 0, s2 = 0, s3 = 0;
			char s4 = ' ';
			if (bt == view.AddSegment1)
			{
				s1 = positions.PreviewNextAvailableLevel1Id(definition);
				s2 = definition.Segment2.StartValue;
				s3 = definition.Segment3.StartValue;
				s4 = definition.Segment4.StartValue;
			}
			else if (bt == view.AddSegment2)
			{
				if (SelectedLevel1 == null)
				{
					valid = false;
					errorMessage = "Segment 1 not selected!";
				}
				else
				{
					s1 = SelectedLevel1.Id;
					s2 = SelectedLevel1.PreviewNextAvailableLevel2Id(definition);
					s3 = definition.Segment3.StartValue;
					s4 = definition.Segment4.StartValue;
				}
			}
			else if (bt == view.AddSegment3)
			{
				if (SelectedLevel1 == null || SelectedLevel2 == null)
				{
					valid = false;
					errorMessage = "Segment 1 or 2 not selected!";
				}
				else
				{
					s1 = SelectedLevel1.Id;
					s2 = SelectedLevel2.Id;
					s3 = SelectedLevel2.PreviewNextAvailableLevel3Id(definition);
					s4 = definition.Segment4.StartValue;
				}
			}
			else if (bt == view.AddSegment4)
			{
				if (SelectedLevel1 == null || SelectedLevel2 == null || SelectedLevel3 == null)
				{
					valid = false;
					errorMessage = "Segment 1, 2 or 3 not selected!";
				}
				else
				{
					s1 = SelectedLevel1.Id;
					s2 = SelectedLevel2.Id;
					s3 = SelectedLevel3.Id;//.PreviewNextAvailableLevel3Id(definition);
					s4 = SelectedLevel3.PreviewNextAvailableLevel4Id(definition);
				}
			}
			if (valid)
				s = definition.GetPositionName(s1, s2, s3, s4);
			else
				s = errorMessage;
			view.NextSegmentPreview = s;
			view.ToolTip.ShowAlways = false;
			view.ToolTip.AutomaticDelay = 0;
			view.ToolTip.InitialDelay = 0;
			view.ToolTip.SetToolTip(bt, s);
		}

		private void AddSegment_MouseEnter(object sender, EventArgs e)
		{
			var bt = (Button)sender;
			RefreshPositionNamePreview(bt);
		}

		private void AddSegment4_Click(object sender, EventArgs e)
		{
			if (SelectedLevel3 != null)
			{
				SelectedLevel3.AddNewChild(definition);
				RefreshSource();
				RefreshPositionNamePreview(view.AddSegment4);
			}
		}
		private void AddSegment3_Click(object sender, EventArgs e)
		{
			if (SelectedLevel2 != null)
			{
				SelectedLevel2.AddNewChild(definition);
				RefreshSource();
				RefreshPositionNamePreview(view.AddSegment3);
			}
		}

		private void AddSegment2_Click(object sender, EventArgs e)
		{
			if (SelectedLevel1 != null)
			{
				SelectedLevel1.AddNewChild(definition);
				RefreshSource();
				RefreshPositionNamePreview(view.AddSegment2);
			}
		}

		private void AddSegment1_Click(object sender, EventArgs e)
		{
			positions.AddNewPosition(definition);
			RefreshSource();
			RefreshPositionNamePreview(view.AddSegment1);
		}


		private void InitializeData()
		{
			dgv.VirtualMode = true;
			dgv.AllowUserToAddRows = false;

			dgv.CellValueNeeded += Dgv_CellValueNeeded;
			dgv.CellValuePushed += Dgv_CellValuePushed;
			//dgv.CellPainting += Dgv_CellPainting;

			//dgv.RowCount = displayCollection.CountVisibleRows();
			//dgv.CellContentClick += Dgv_CellContentClick;

			RefreshSource();
		}
		private void RefreshSource()
		{
			rowData.Clear();
			foreach (var l1 in positions)
			{
				rowData.Add(new PositionRow(l1));
				foreach (var l2 in l1)
				{
					rowData.Add(new PositionRow(l2));
					foreach (var l3 in l2)
					{
						rowData.Add(new PositionRow(l3));
						foreach (var l4 in l3)
							rowData.Add(new PositionRow(l4));
					}
				}
			}
			dgv.RowCount = rowData.Count;
		}

		

		private void Dgv_CellValuePushed(object sender, DataGridViewCellValueEventArgs e)
		{

		}

		private void Dgv_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
		{
			if (e.RowIndex == -1)
				return;
			// Needed because in RefreshSource when RowCount is set to 0, this is invoked for cell (0,0).
			if (rowData.Count == 0)
				return;
			var data = rowData[e.RowIndex];
			string s = "";
			bool s1 = e.ColumnIndex == view.DgvColumnSegment1.Index;
			bool s2 = e.ColumnIndex == view.DgvColumnSegment2.Index;
			bool s3 = e.ColumnIndex == view.DgvColumnSegment3.Index;
			bool s4 = e.ColumnIndex == view.DgvColumnSegment4.Index;
			if (s1 || s2 || s3 || s4)
			{
				if (s1 && data.Level1 != null)
				{
					s = definition.GetPositionName(data.Level1.Id);
				}
				else if (s2 && data.Level2 != null)
				{
					s = definition.GetPositionName(
							null,//data.Level2.Parent.Id,
							data.Level2.Id);
				}
				else if (s3 && data.Level3 != null)
				{
					s = definition.GetPositionName(
							null,
							null,
							data.Level3.Id,
							null);
				}
				else if (s4 && data.Level4 != null)
				{
					s = definition.GetPositionName(
							data.Level4.Parent.Parent.Parent.Id,
							data.Level4.Parent.Parent.Id,
							data.Level4.Parent.Id,
							data.Level4.Id);
				}
				
			}
			if (e.ColumnIndex == view.DgvColumnTitle.Index && data.Level3 != null)
				s = data.Level3.Title;
			e.Value = s;			
		}
	}
}
