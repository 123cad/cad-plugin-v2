﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views.Main
{
	public interface IMainView
	{
		TextBox StartSegment1 { get; }
		TextBox StartSegment2 { get; }
		TextBox StartSegment3 { get; }
		TextBox StartSegment4 { get; }

		TextBox IncrementSegment1 { get; }
		TextBox IncrementSegment2 { get; }
		TextBox IncrementSegment3 { get; }
		TextBox IncrementSegment4 { get; }
		CheckBox CheckIncrementSegment4 { get; }

		IEnumerable<string> PreviewSegment1 { get; set; }
		IEnumerable<string> PreviewSegment2 { get; set; }
		IEnumerable<string> PreviewSegment3 { get; set; }
		IEnumerable<string> PreviewSegment4 { get; set; }

		string StartPreview { set; }

		ErrorProvider ErrorProvider { get; }

		DataGridView Data { get; }

		//DataGridViewColumn DgvColumnAddSegment { get; }
		//DataGridViewColumn DgvColumnExpand { get; }
		DataGridViewColumn DgvColumnSegment1 { get; }
		DataGridViewColumn DgvColumnSegment2 { get; }
		DataGridViewColumn DgvColumnSegment3 { get; }
		DataGridViewColumn DgvColumnSegment4 { get; }
		DataGridViewColumn DgvColumnTitle { get; }
		
		Button AddSegment1 { get; }
		Button AddSegment2 { get; }
		Button AddSegment3 { get; }
		Button AddSegment4 { get; }

		TextBox TextBoxSegment1 { get; }
		TextBox TextBoxSegment2 { get; }
		TextBox TextBoxSegment3 { get; }

		string NextSegmentPreview { set; }

		ToolTip ToolTip { get; }

		Button DeletePosition { get; }

	}
}
