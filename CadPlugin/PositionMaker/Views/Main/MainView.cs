﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views.Main
{
	public partial class MainView : Form, IMainView
	{
		public MainView()
		{
			InitializeComponent();
			
		}
		

		public TextBox IncrementSegment1 => textBoxIncrement1;
		public TextBox IncrementSegment2 => textBoxIncrement2;
		public TextBox IncrementSegment3 => textBoxIncrement3;
		public TextBox IncrementSegment4 => textBoxIncrement4;
		public CheckBox CheckIncrementSegment4 => checkBoxIncrement4;


		public IEnumerable<string> PreviewSegment1
		{
			get
			{
				foreach (var v in listBoxPreview1.Items)
					yield return (string)v;
			}

			set
			{
				listBoxPreview1.Items.Clear();
				foreach (var v in value)
					listBoxPreview1.Items.Add(v);
			}
		}

		public IEnumerable<string> PreviewSegment2
		{
			get
			{
				foreach (var v in listBoxPreview2.Items)
					yield return (string)v;
			}

			set
			{
				listBoxPreview2.Items.Clear();
				foreach (var v in value)
					listBoxPreview2.Items.Add(v);
			}
		}

		public IEnumerable<string> PreviewSegment3
		{
			get
			{
				foreach (var v in listBoxPreview3.Items)
					yield return (string)v;
			}

			set
			{
				listBoxPreview3.Items.Clear();
				foreach (var v in value)
					listBoxPreview3.Items.Add(v);
			}
		}

		public IEnumerable<string> PreviewSegment4
		{
			get
			{
				foreach (var v in listBoxPreview4.Items)
					yield return (string)v;
			}

			set
			{
				listBoxPreview4.Items.Clear();
				foreach (var v in value)
					listBoxPreview4.Items.Add(v);
			}
		}

		public TextBox StartSegment1 => textBoxStart1;
		public TextBox StartSegment2 => textBoxStart2;
		public TextBox StartSegment3 => textBoxStart3;
		public TextBox StartSegment4 => textBoxStart4;
		public ErrorProvider ErrorProvider => errorProvider1;

		public string StartPreview
		{
			set
			{
				textBoxStartPreview.Text = value;
			}
		}

		public DataGridView Data => dataGridView;



		//public DataGridViewColumn DgvColumnAddSegment => ColumnAdd;
		//public DataGridViewColumn DgvColumnExpand => ColumnExpand;
		public DataGridViewColumn DgvColumnSegment1 => ColumnS1;
		public DataGridViewColumn DgvColumnSegment2 => ColumnS2;
		public DataGridViewColumn DgvColumnSegment3 => ColumnS3;
		public DataGridViewColumn DgvColumnSegment4 => ColumnS4;
		public DataGridViewColumn DgvColumnTitle => ColumnTitle;


		public Button AddSegment1 => buttonAddSegment1;
		public Button AddSegment2 => buttonAddSegment2;
		public Button AddSegment3 => buttonAddSegment3;
		public Button AddSegment4 => buttonAddSegment4;

		public TextBox TextBoxSegment1 => textBoxSegment1;
		public TextBox TextBoxSegment2 => textBoxSegment2;
		public TextBox TextBoxSegment3 => textBoxSegment3;

		public string NextSegmentPreview { set { labelNextSegment.Text = value; } }

		public ToolTip ToolTip => toolTip;
		public Button DeletePosition => buttonDeletePosition;


	}
}
