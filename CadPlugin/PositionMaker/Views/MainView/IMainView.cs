﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views.MainView
{
	interface IMainView
	{
		Panel PanelTreeSegment { get; }
		Panel PanelUnits { get; }

		event Action CreatePositionClick;
		event Action ManageGroups;
		event Action ExportXML;
		event Action ImportXML;
		event Action ImportD8X;
        /// <summary>
        /// Read quantity for selected positions in .dwg .
        /// </summary>
        event Action ReadPositionFromDwg;
		/// <summary>
		/// Import prices for existing structure.
		/// </summary>
		event Action ImportPrices;

		TextBox Description { get; }

		TextBox ServiceInfo { get; }
		TextBox ProjektName { get; }
		TextBox Purchaser { get; }
		TextBox Company { get; }
		TextBox Currency { get; }
		DateTimePicker Date { get; }

		CheckBox ShowProjectDescription { get; }

		string TotalSum { set; }
	}
}
