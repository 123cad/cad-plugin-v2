﻿namespace CadPlugin.PositionMaker.Views.MainView
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.textBoxTotalSum = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dateTimePickerProjectDate = new System.Windows.Forms.DateTimePicker();
            this.textBoxCurrency = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxCompany = new System.Windows.Forms.TextBox();
            this.textBoxPurchaser = new System.Windows.Forms.TextBox();
            this.textBoxProject = new System.Windows.Forms.TextBox();
            this.textBoxServiceInfo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panelDgv = new System.Windows.Forms.Panel();
            this.panelUnits = new System.Windows.Forms.Panel();
            this.checkBoxProjectDescription = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.importD82ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createPositionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importPricesFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageGroupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonReadPositionsFromDwg = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDescription.Location = new System.Drawing.Point(3, 168);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxDescription.Size = new System.Drawing.Size(452, 270);
            this.textBoxDescription.TabIndex = 1;
            this.textBoxDescription.Text = "012345678901234567890123456789012345678901234567890123456789";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Controls.Add(this.buttonReadPositionsFromDwg);
            this.splitContainer1.Panel1.Controls.Add(this.textBoxTotalSum);
            this.splitContainer1.Panel1.Controls.Add(this.label8);
            this.splitContainer1.Panel1.Controls.Add(this.label7);
            this.splitContainer1.Panel1.Controls.Add(this.dateTimePickerProjectDate);
            this.splitContainer1.Panel1.Controls.Add(this.textBoxCurrency);
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.textBoxCompany);
            this.splitContainer1.Panel1.Controls.Add(this.textBoxPurchaser);
            this.splitContainer1.Panel1.Controls.Add(this.textBoxProject);
            this.splitContainer1.Panel1.Controls.Add(this.textBoxServiceInfo);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.panelDgv);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panelUnits);
            this.splitContainer1.Panel2.Controls.Add(this.checkBoxProjectDescription);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.textBoxDescription);
            this.splitContainer1.Size = new System.Drawing.Size(1199, 511);
            this.splitContainer1.SplitterDistance = 732;
            this.splitContainer1.TabIndex = 2;
            // 
            // textBoxTotalSum
            // 
            this.textBoxTotalSum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTotalSum.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTotalSum.Location = new System.Drawing.Point(578, 447);
            this.textBoxTotalSum.Name = "textBoxTotalSum";
            this.textBoxTotalSum.ReadOnly = true;
            this.textBoxTotalSum.Size = new System.Drawing.Size(135, 21);
            this.textBoxTotalSum.TabIndex = 13;
            this.textBoxTotalSum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(507, 452);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Total sum:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(474, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Date";
            // 
            // dateTimePickerProjectDate
            // 
            this.dateTimePickerProjectDate.CustomFormat = "dd.MM.yy";
            this.dateTimePickerProjectDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerProjectDate.Location = new System.Drawing.Point(510, 13);
            this.dateTimePickerProjectDate.MaxDate = new System.DateTime(2046, 12, 31, 0, 0, 0, 0);
            this.dateTimePickerProjectDate.MinDate = new System.DateTime(1903, 1, 1, 0, 0, 0, 0);
            this.dateTimePickerProjectDate.Name = "dateTimePickerProjectDate";
            this.dateTimePickerProjectDate.Size = new System.Drawing.Size(86, 20);
            this.dateTimePickerProjectDate.TabIndex = 10;
            this.dateTimePickerProjectDate.Value = new System.DateTime(1952, 12, 31, 0, 0, 0, 0);
            // 
            // textBoxCurrency
            // 
            this.textBoxCurrency.Location = new System.Drawing.Point(101, 122);
            this.textBoxCurrency.Name = "textBoxCurrency";
            this.textBoxCurrency.Size = new System.Drawing.Size(61, 20);
            this.textBoxCurrency.TabIndex = 9;
            this.textBoxCurrency.Text = "EUR";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 125);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Currency";
            // 
            // textBoxCompany
            // 
            this.textBoxCompany.Location = new System.Drawing.Point(101, 96);
            this.textBoxCompany.Name = "textBoxCompany";
            this.textBoxCompany.Size = new System.Drawing.Size(355, 20);
            this.textBoxCompany.TabIndex = 6;
            // 
            // textBoxPurchaser
            // 
            this.textBoxPurchaser.Location = new System.Drawing.Point(101, 70);
            this.textBoxPurchaser.Name = "textBoxPurchaser";
            this.textBoxPurchaser.Size = new System.Drawing.Size(355, 20);
            this.textBoxPurchaser.TabIndex = 7;
            // 
            // textBoxProject
            // 
            this.textBoxProject.Location = new System.Drawing.Point(101, 42);
            this.textBoxProject.Name = "textBoxProject";
            this.textBoxProject.Size = new System.Drawing.Size(355, 20);
            this.textBoxProject.TabIndex = 6;
            // 
            // textBoxServiceInfo
            // 
            this.textBoxServiceInfo.Location = new System.Drawing.Point(101, 13);
            this.textBoxServiceInfo.Name = "textBoxServiceInfo";
            this.textBoxServiceInfo.Size = new System.Drawing.Size(355, 20);
            this.textBoxServiceInfo.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Bieter";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Auftraggeber";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Projekt";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "LV";
            // 
            // panelDgv
            // 
            this.panelDgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelDgv.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelDgv.Location = new System.Drawing.Point(12, 168);
            this.panelDgv.Name = "panelDgv";
            this.panelDgv.Size = new System.Drawing.Size(717, 270);
            this.panelDgv.TabIndex = 0;
            // 
            // panelUnits
            // 
            this.panelUnits.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelUnits.Location = new System.Drawing.Point(24, 7);
            this.panelUnits.Name = "panelUnits";
            this.panelUnits.Size = new System.Drawing.Size(269, 122);
            this.panelUnits.TabIndex = 4;
            // 
            // checkBoxProjectDescription
            // 
            this.checkBoxProjectDescription.AutoSize = true;
            this.checkBoxProjectDescription.Location = new System.Drawing.Point(278, 145);
            this.checkBoxProjectDescription.Name = "checkBoxProjectDescription";
            this.checkBoxProjectDescription.Size = new System.Drawing.Size(144, 17);
            this.checkBoxProjectDescription.TabIndex = 3;
            this.checkBoxProjectDescription.Text = "Show project Description";
            this.checkBoxProjectDescription.UseVisualStyleBackColor = true;
            this.checkBoxProjectDescription.CheckedChanged += new System.EventHandler(this.checkBoxProjectDescription_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 142);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Description";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1199, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportXMLToolStripMenuItem,
            this.importXMLToolStripMenuItem,
            this.toolStripSeparator1,
            this.importD82ToolStripMenuItem,
            this.toolStripSeparator3,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exportXMLToolStripMenuItem
            // 
            this.exportXMLToolStripMenuItem.Name = "exportXMLToolStripMenuItem";
            this.exportXMLToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.exportXMLToolStripMenuItem.Text = "Export XML";
            // 
            // importXMLToolStripMenuItem
            // 
            this.importXMLToolStripMenuItem.AutoToolTip = true;
            this.importXMLToolStripMenuItem.Name = "importXMLToolStripMenuItem";
            this.importXMLToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.importXMLToolStripMenuItem.Text = "Import XML";
            this.importXMLToolStripMenuItem.ToolTipText = "Import from 123CAD XML file";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(159, 6);
            // 
            // importD82ToolStripMenuItem
            // 
            this.importD82ToolStripMenuItem.Name = "importD82ToolStripMenuItem";
            this.importD82ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.importD82ToolStripMenuItem.Text = "Import d80 - d83";
            this.importD82ToolStripMenuItem.Click += new System.EventHandler(this.importD8XToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(159, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createPositionsToolStripMenuItem,
            this.importPricesFileToolStripMenuItem,
            this.manageGroupsToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // createPositionsToolStripMenuItem
            // 
            this.createPositionsToolStripMenuItem.Name = "createPositionsToolStripMenuItem";
            this.createPositionsToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.createPositionsToolStripMenuItem.Text = "Create positions";
            // 
            // importPricesFileToolStripMenuItem
            // 
            this.importPricesFileToolStripMenuItem.Name = "importPricesFileToolStripMenuItem";
            this.importPricesFileToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.importPricesFileToolStripMenuItem.Text = "Import prices file";
            // 
            // manageGroupsToolStripMenuItem
            // 
            this.manageGroupsToolStripMenuItem.Name = "manageGroupsToolStripMenuItem";
            this.manageGroupsToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.manageGroupsToolStripMenuItem.Text = "Manage Groups";
            this.manageGroupsToolStripMenuItem.Click += new System.EventHandler(this.manageGroupsToolStripMenuItem_Click);
            // 
            // buttonReadPositionsFromDwg
            // 
            this.buttonReadPositionsFromDwg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonReadPositionsFromDwg.Location = new System.Drawing.Point(12, 446);
            this.buttonReadPositionsFromDwg.Name = "buttonReadPositionsFromDwg";
            this.buttonReadPositionsFromDwg.Size = new System.Drawing.Size(100, 23);
            this.buttonReadPositionsFromDwg.TabIndex = 14;
            this.buttonReadPositionsFromDwg.Text = "Read from DWG";
            this.buttonReadPositionsFromDwg.UseVisualStyleBackColor = true;
            this.buttonReadPositionsFromDwg.Click += new System.EventHandler(this.buttonReadPositionsFromDwg_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1199, 535);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "LV - Manager";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.TextBox textBoxDescription;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exportXMLToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem createPositionsToolStripMenuItem;
		private System.Windows.Forms.Panel panelDgv;
		private System.Windows.Forms.ToolStripMenuItem importXMLToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem importD82ToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.TextBox textBoxCurrency;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox textBoxCompany;
		private System.Windows.Forms.TextBox textBoxPurchaser;
		private System.Windows.Forms.TextBox textBoxProject;
		private System.Windows.Forms.TextBox textBoxServiceInfo;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.DateTimePicker dateTimePickerProjectDate;
		private System.Windows.Forms.CheckBox checkBoxProjectDescription;
		private System.Windows.Forms.Panel panelUnits;
		private System.Windows.Forms.TextBox textBoxTotalSum;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.ToolStripMenuItem importPricesFileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem manageGroupsToolStripMenuItem;
        private System.Windows.Forms.Button buttonReadPositionsFromDwg;
    }
}