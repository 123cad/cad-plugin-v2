﻿using CadPlugin.PositionMaker.Models;
using CadPlugin.PositionMaker.Models.CadInterface;
using CadPlugin.PositionMaker.Models.Importing;
using CadPlugin.PositionMaker.Views.GroupManagerForm;
using CadPlugin.PositionMaker.Views.TreeSegmentDisplay;
using Inventory.Positions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views.MainView
{
	class MainController
	{
		private IMainView view;
		private PositionsManager data;

		private TreeSegmentDisplayController treeController;
		private UnitAssociationControl.UnitAssociationController unitsController;

		public event Action<SelectObjectsEventArgs> SelectObjects;
		/// <summary>
		/// Project description is shown inside Description TextBox.
		/// </summary>
		private bool showProjectDescription = false;

		/// <summary>
		/// When selected object changing, do not update description,
		/// because it is the reason for changing.
		/// </summary>
		private bool selectedObjectChanging = false;
		/// <summary>
		/// Used by project description, to restore previous value.
		/// </summary>
		private string lastDescription = "";
				
		public MainController(IMainView view)
		{
			this.view = view;
			var f = (Form)view;
			f.Height = 600;
			//f.Icon = Properties.Resources._123cad;
			data = new PositionsManager();

			// Add DGV control.
			TreeSegmentDisplayView tv = new TreeSegmentDisplayView();
			tv.Dock = DockStyle.Fill;
			view.PanelTreeSegment.Controls.Add(tv);
			treeController = new TreeSegmentDisplayController(tv, data, true);
			treeController.SelectedSegmentChanged += () =>
			{
				selectedObjectChanging = true;
				view.Description.Text = "";
				view.Description.Enabled = false;
				if (treeController.SelectedSegment != null )
				{
					Segment sg = treeController.SelectedSegment as Segment;
					if (sg != null)
					{
						view.Description.Enabled = true;
						view.Description.Text = sg.Description;
					}
				}
				selectedObjectChanging = false;
			};
			treeController.DataChanged += (e) => UpdateTotalSum();

			treeController.SegmentUnitChanged += (e) =>
			{
				treeController.SetPositionsWhichHaveButton(PositionCanGetDataFromCad, e);
			};

			Func<SegmentData, bool> isPositionWithAButton = (dt) =>
			{
				bool b = unitsController.UnitAssociations.GetUnit(dt.UnitOfMeasurement) != Unit.NotAUnit;
				return b;
			};

			// Add units control
			var unitsView = new UnitAssociationControl.UnitAssociationView();
			unitsView.Dock = DockStyle.Fill;
			view.PanelUnits.Controls.Add(unitsView);
			unitsController = new UnitAssociationControl.UnitAssociationController(unitsView);
			unitsController.AssociationChanged += () =>
			{
				treeController.SetPositionsWhichHaveButton(PositionCanGetDataFromCad);
			};
			treeController.SetPositionsWhichHaveButton(PositionCanGetDataFromCad);

            view.ReadPositionFromDwg += () => treeController.ReadSelectedPositionsQuantity();


			treeController.SelectPositionObjects += position =>
			{
				CadObjectGroup? group = null;
				Unit u = unitsController.UnitAssociations.GetUnit(position.UnitOfMeasurement);
				switch (u)
				{
					case Unit.Count: group = CadObjectGroup.Count;	break;// = "count"; unit = ""; break;
					case Unit.Length:group = CadObjectGroup.Length; break;// = "length"; unit = "m"; break;
					case Unit.Area:group = CadObjectGroup.Area;		break;//t = "area"; unit = "m²"; break;
					default:break;
				}
				if (!group.HasValue)
					return;
				SelectObjectsEventArgs args = new SelectObjectsEventArgs(group.Value);
				SelectObjects?.Invoke(args);

				//MessageBox.Show($"Selected {args.TotalObjectsSelected} objects with total {t} of {DoubleHelper.ToStringInvariant(args.TotalSum, 2)} {unit}");

				position.Quantity = args.TotalSum;
			};

			view.CreatePositionClick += () =>
			{
				PositionDefinitionView v = new PositionDefinitionView();
				if (data != null && data.FirstSegmentsCollection.Any())
					v.InitializeWithExistingData(data);
				if (v.ShowDialog() == DialogResult.OK)
				//if (v.ShowDialog() == DialogResult.OK)
				{
					data = v.manager;
					FileLoaded();
				}
			};

			view.ExportXML += () =>
			{
				SaveFileDialog dlg = new SaveFileDialog();
				dlg.FileName = "test.xml";
				dlg.Filter = "XML file (*.xml)|*.xml";
				if (dlg.ShowDialog() == DialogResult.OK)
				{
					ExportXml.SaveFile(data, dlg.FileName);
				}
			};
			view.ImportXML += () =>
			{
				OpenFileDialog dlg = new OpenFileDialog();
				dlg.CheckFileExists = true;
				dlg.Filter = "123CAD position xml (*.xml)|*xml";
				if (dlg.ShowDialog() == DialogResult.OK)
				{
					view.ShowProjectDescription.Checked = false;
					var mgr = ImportXml.LoadFromFile(dlg.FileName);
					data = mgr;
					FileLoaded();
				}
			};
			view.ImportD8X += () =>
			{
				OpenFileDialog dlg = new OpenFileDialog();
				dlg.CheckFileExists = true;
				dlg.Filter = "d8X file (*.d80;*.d81;*.d82;*.d83)|*.d80;*.d81;*.d82;*.d83";
				if (dlg.ShowDialog()== DialogResult.OK)
				{
					try
					{
						view.ShowProjectDescription.Checked = false;
						var mgr = D8X.LoadFromFile(dlg.FileName);
						data = mgr;
						FileLoaded();
					}
					catch (D8xFormatException e)
					{
						MyLog.MyTraceSource.Error("Import D8X error: " + e.Message);
					}
				}
			};
			view.ImportPrices += () =>
			{
				if (!data.FirstSegmentsCollection.Any())
				{
					System.Windows.Forms.MessageBox.Show($"Please first load/create positions, before loading pricing file.", "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
					return;
				}
				OpenFileDialog dlg = new OpenFileDialog();
				dlg.CheckFileExists = true;
				dlg.Filter = "Comma separated file (*.csv)|*.csv";
				if (dlg.ShowDialog() == DialogResult.OK)
				{
					try
					{
						view.ShowProjectDescription.Checked = false;
						ImportPrice.ImportPriceFromCsvFile(data, dlg.FileName);
						FileLoaded();
					}
					catch (CSVFormatException e)
					{
						MyLog.MyTraceSource.Error("Import CSV file error: " + e.Message);
					}
				}
			};
			view.ShowProjectDescription.CheckedChanged += (_, e) =>
			{
				showProjectDescription = view.ShowProjectDescription.Checked;
				if (showProjectDescription)
				{
					lastDescription = view.Description.Text;
					view.Description.Text = data.ProjectDescription;
				}
				else
					view.Description.Text = lastDescription;
				treeController.Enabled = !showProjectDescription;

			};
			view.ManageGroups += () =>
			{

				GroupManagerController.StartGroupManager(data);
			};


			InitializeBindings();

			view.Description.TextChanged += Description_TextChanged;
			view.Description.Enabled = false;
			Initialize();
			UpdateTotalSum();

			view.ShowProjectDescription.Checked = false;
		}
		
		/// <summary>
		/// Determines if position can get information from CAD (depends on measurement unit).
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		private bool PositionCanGetDataFromCad(SegmentData data)
		{
			bool b = unitsController.UnitAssociations.GetUnit(data.UnitOfMeasurement) != Unit.NotAUnit;
			return b;
		}
		private void FileLoaded()
		{
			treeController.SetNewDataSource(data);
			InitializeBindings();
			treeController.SetPositionsWhichHaveButton(PositionCanGetDataFromCad);
			UpdateTotalSum();
		}
		/// <summary>
		/// When any data has changed in PositionManager.
		/// </summary>
		/// <param name="s"></param>
		private void UpdateTotalSum()
		{
			double sum = data.FirstSegmentsCollection.Sum(x => x.GetTotalPrice());

			string st = DoubleHelper.ToStringInvariant(sum, 2, true);// sum.ToString("0,0.00", CultureInfo.InvariantCulture);
			// Invariant culture sets: 1,234.54 . We need 1.234,54
			st = st.Replace('.', ';');
			st = st.Replace(',', '.');
			st = st.Replace(';', ',');
			if (data.Currency?.ToLower() == "eur"
				|| data.Currency?.ToLower() == "euro")
				st += " €";
			view.TotalSum = st;
		}

		private void InitializeBindings()
		{
			lastDescription = "";
			view.ServiceInfo.DataBindings.Clear();
			view.ProjektName.DataBindings.Clear();
			view.Purchaser.DataBindings.Clear();
			view.Company.DataBindings.Clear();
			view.Currency.DataBindings.Clear();
			view.Date.DataBindings.Clear();
			view.ServiceInfo.DataBindings.Add(nameof(TextBox.Text), data, nameof(PositionsManager.ServiceInfo));
			view.ProjektName.DataBindings.Add(nameof(TextBox.Text), data, nameof(PositionsManager.ProjectName));
			view.Purchaser.DataBindings.Add(nameof(TextBox.Text), data, nameof(PositionsManager.PurchaserName));
			view.Company.DataBindings.Add(nameof(TextBox.Text), data, nameof(PositionsManager.CompanyName));
			Binding bind = new Binding(nameof(TextBox.Text), data, nameof(PositionsManager.Currency));
			bind.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
			bind.FormattingEnabled = true;
			bind.BindingComplete += (_, e) =>
			{
				UpdateTotalSum();
			};
			view.Currency.DataBindings.Add(bind);
			//view.Currency.DataBindings.Add(nameof(TextBox.Text), data, nameof(PositionsManager.Currency));
			//Binding bind = new Binding();
			view.Date.DataBindings.Add(nameof(DateTimePicker.Value), data, nameof(PositionsManager.ProjectDate));
		}

		private void Description_TextChanged(object sender, EventArgs e)
		{
			// When text is changing because object is being changed, ignore it.
			if (selectedObjectChanging)
				return;
			if (showProjectDescription)
			{
				data.ProjectDescription = view.Description.Text;
			}	
			else
			{
				var seg = treeController.SelectedSegment as Segment;
				if (seg == null)
					return;
				seg.Description = view.Description.Text;
			}
		}

		private void Initialize()
		{
			
		}
		
	}
}
