﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views.MainView
{
	public partial class MainForm : Form, IMainView
	{
		public MainForm()
		{
			InitializeComponent();
			createPositionsToolStripMenuItem.Click += (_, e) => CreatePositionClick?.Invoke();
			exportXMLToolStripMenuItem.Click += (_, e) => ExportXML?.Invoke();
			importXMLToolStripMenuItem.Click += (_, e) => ImportXML?.Invoke();
			importPricesFileToolStripMenuItem.Click += (_, e) => ImportPrices?.Invoke();
		}

		public event Action CreatePositionClick;
		public event Action ManageGroups;
		public event Action ExportXML;
		public event Action ImportXML;

		public event Action ImportD8X;
		public event Action ImportPrices;
        public event Action ReadPositionFromDwg;

        public CheckBox ShowProjectDescription => checkBoxProjectDescription;
		public TextBox Description => textBoxDescription;

		public Panel PanelTreeSegment => panelDgv;

		public TextBox ServiceInfo => textBoxServiceInfo;

		public TextBox ProjektName => textBoxProject;

		public TextBox Purchaser => textBoxPurchaser;
		public TextBox Company => textBoxCompany;
		public TextBox Currency => textBoxCurrency;

		public DateTimePicker Date => dateTimePickerProjectDate;

		public Panel PanelUnits => panelUnits;

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void importD80ToolStripMenuItem_Click(object sender, EventArgs e)
		{
		}

		private void importD8XToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ImportD8X?.Invoke();
		}

		private void checkBoxProjectDescription_CheckedChanged(object sender, EventArgs e)
		{

		}
		public string TotalSum { set { textBoxTotalSum.Text = value; } }

		private void manageGroupsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ManageGroups?.Invoke();
		}

        private void buttonReadPositionsFromDwg_Click(object sender, EventArgs e)
        {
            ReadPositionFromDwg?.Invoke();
        }
    }
}
