﻿namespace CadPlugin.PositionMaker.Views
{
	partial class PositionDefinitionView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PositionDefinitionView));
            this.comboBoxNumberOfSegments = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonCreatePosition = new System.Windows.Forms.Button();
            this.labelNextPositionPreview = new System.Windows.Forms.Label();
            this.checkBoxLastCharSegment = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelSegment10 = new System.Windows.Forms.Label();
            this.labelSegment9 = new System.Windows.Forms.Label();
            this.labelSegment8 = new System.Windows.Forms.Label();
            this.labelSegment6 = new System.Windows.Forms.Label();
            this.labelSegment7 = new System.Windows.Forms.Label();
            this.labelSegment4 = new System.Windows.Forms.Label();
            this.labelSegment5 = new System.Windows.Forms.Label();
            this.labelSegment2 = new System.Windows.Forms.Label();
            this.labelSegment3 = new System.Windows.Forms.Label();
            this.labelSegment0 = new System.Windows.Forms.Label();
            this.textBoxInc9 = new System.Windows.Forms.TextBox();
            this.textBoxInc8 = new System.Windows.Forms.TextBox();
            this.textBoxInc7 = new System.Windows.Forms.TextBox();
            this.textBoxInc6 = new System.Windows.Forms.TextBox();
            this.textBoxInc5 = new System.Windows.Forms.TextBox();
            this.textBoxInc4 = new System.Windows.Forms.TextBox();
            this.textBoxInc3 = new System.Windows.Forms.TextBox();
            this.textBoxInc2 = new System.Windows.Forms.TextBox();
            this.textBoxInc1 = new System.Windows.Forms.TextBox();
            this.textBoxStart8 = new System.Windows.Forms.TextBox();
            this.textBoxStart7 = new System.Windows.Forms.TextBox();
            this.textBoxStart6 = new System.Windows.Forms.TextBox();
            this.textBoxStart5 = new System.Windows.Forms.TextBox();
            this.textBoxStart4 = new System.Windows.Forms.TextBox();
            this.textBoxStart3 = new System.Windows.Forms.TextBox();
            this.textBoxInc0 = new System.Windows.Forms.TextBox();
            this.textBoxStart2 = new System.Windows.Forms.TextBox();
            this.textBoxStart1 = new System.Windows.Forms.TextBox();
            this.textBoxStart0 = new System.Windows.Forms.TextBox();
            this.checkBoxIncrementChar = new System.Windows.Forms.CheckBox();
            this.textBoxStartPreview = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.labelSegment1 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxStart9 = new System.Windows.Forms.TextBox();
            this.textBoxStart10 = new System.Windows.Forms.TextBox();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxNumberOfSegments
            // 
            this.comboBoxNumberOfSegments.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxNumberOfSegments.FormattingEnabled = true;
            this.comboBoxNumberOfSegments.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.comboBoxNumberOfSegments.Location = new System.Drawing.Point(30, 37);
            this.comboBoxNumberOfSegments.Name = "comboBoxNumberOfSegments";
            this.comboBoxNumberOfSegments.Size = new System.Drawing.Size(121, 21);
            this.comboBoxNumberOfSegments.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Number of segments";
            // 
            // buttonCreatePosition
            // 
            this.buttonCreatePosition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCreatePosition.Location = new System.Drawing.Point(30, 355);
            this.buttonCreatePosition.Name = "buttonCreatePosition";
            this.buttonCreatePosition.Size = new System.Drawing.Size(75, 23);
            this.buttonCreatePosition.TabIndex = 4;
            this.buttonCreatePosition.Text = "Create";
            this.buttonCreatePosition.UseVisualStyleBackColor = true;
            // 
            // labelNextPositionPreview
            // 
            this.labelNextPositionPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelNextPositionPreview.AutoSize = true;
            this.labelNextPositionPreview.Location = new System.Drawing.Point(27, 334);
            this.labelNextPositionPreview.Name = "labelNextPositionPreview";
            this.labelNextPositionPreview.Size = new System.Drawing.Size(83, 13);
            this.labelNextPositionPreview.TabIndex = 5;
            this.labelNextPositionPreview.Text = "Create: 0.0.0.01";
            // 
            // checkBoxLastCharSegment
            // 
            this.checkBoxLastCharSegment.AutoSize = true;
            this.checkBoxLastCharSegment.Location = new System.Drawing.Point(30, 74);
            this.checkBoxLastCharSegment.Name = "checkBoxLastCharSegment";
            this.checkBoxLastCharSegment.Size = new System.Drawing.Size(156, 17);
            this.checkBoxLastCharSegment.TabIndex = 6;
            this.checkBoxLastCharSegment.Text = "Last segment char segment";
            this.checkBoxLastCharSegment.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelSegment10);
            this.groupBox1.Controls.Add(this.labelSegment9);
            this.groupBox1.Controls.Add(this.labelSegment8);
            this.groupBox1.Controls.Add(this.labelSegment6);
            this.groupBox1.Controls.Add(this.labelSegment7);
            this.groupBox1.Controls.Add(this.labelSegment4);
            this.groupBox1.Controls.Add(this.labelSegment5);
            this.groupBox1.Controls.Add(this.labelSegment2);
            this.groupBox1.Controls.Add(this.labelSegment3);
            this.groupBox1.Controls.Add(this.labelSegment0);
            this.groupBox1.Controls.Add(this.textBoxInc9);
            this.groupBox1.Controls.Add(this.textBoxInc8);
            this.groupBox1.Controls.Add(this.textBoxInc7);
            this.groupBox1.Controls.Add(this.textBoxInc6);
            this.groupBox1.Controls.Add(this.textBoxInc5);
            this.groupBox1.Controls.Add(this.textBoxInc4);
            this.groupBox1.Controls.Add(this.textBoxInc3);
            this.groupBox1.Controls.Add(this.textBoxInc2);
            this.groupBox1.Controls.Add(this.textBoxInc1);
            this.groupBox1.Controls.Add(this.textBoxStart8);
            this.groupBox1.Controls.Add(this.textBoxStart7);
            this.groupBox1.Controls.Add(this.textBoxStart6);
            this.groupBox1.Controls.Add(this.textBoxStart5);
            this.groupBox1.Controls.Add(this.textBoxStart4);
            this.groupBox1.Controls.Add(this.textBoxStart3);
            this.groupBox1.Controls.Add(this.textBoxInc0);
            this.groupBox1.Controls.Add(this.textBoxStart2);
            this.groupBox1.Controls.Add(this.textBoxStart1);
            this.groupBox1.Controls.Add(this.textBoxStart0);
            this.groupBox1.Controls.Add(this.checkBoxIncrementChar);
            this.groupBox1.Controls.Add(this.textBoxStartPreview);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.labelSegment1);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.textBoxStart9);
            this.groupBox1.Controls.Add(this.textBoxStart10);
            this.groupBox1.Location = new System.Drawing.Point(217, 18);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(519, 135);
            this.groupBox1.TabIndex = 81;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Segment definition";
            // 
            // labelSegment10
            // 
            this.labelSegment10.AutoSize = true;
            this.labelSegment10.Location = new System.Drawing.Point(475, 25);
            this.labelSegment10.Name = "labelSegment10";
            this.labelSegment10.Size = new System.Drawing.Size(13, 13);
            this.labelSegment10.TabIndex = 129;
            this.labelSegment10.Text = "1";
            // 
            // labelSegment9
            // 
            this.labelSegment9.AutoSize = true;
            this.labelSegment9.Location = new System.Drawing.Point(425, 25);
            this.labelSegment9.Name = "labelSegment9";
            this.labelSegment9.Size = new System.Drawing.Size(13, 13);
            this.labelSegment9.TabIndex = 128;
            this.labelSegment9.Text = "1";
            // 
            // labelSegment8
            // 
            this.labelSegment8.AutoSize = true;
            this.labelSegment8.Location = new System.Drawing.Point(370, 25);
            this.labelSegment8.Name = "labelSegment8";
            this.labelSegment8.Size = new System.Drawing.Size(13, 13);
            this.labelSegment8.TabIndex = 127;
            this.labelSegment8.Text = "1";
            // 
            // labelSegment6
            // 
            this.labelSegment6.AutoSize = true;
            this.labelSegment6.Location = new System.Drawing.Point(272, 25);
            this.labelSegment6.Name = "labelSegment6";
            this.labelSegment6.Size = new System.Drawing.Size(13, 13);
            this.labelSegment6.TabIndex = 126;
            this.labelSegment6.Text = "1";
            // 
            // labelSegment7
            // 
            this.labelSegment7.AutoSize = true;
            this.labelSegment7.Location = new System.Drawing.Point(322, 25);
            this.labelSegment7.Name = "labelSegment7";
            this.labelSegment7.Size = new System.Drawing.Size(13, 13);
            this.labelSegment7.TabIndex = 125;
            this.labelSegment7.Text = "1";
            // 
            // labelSegment4
            // 
            this.labelSegment4.AutoSize = true;
            this.labelSegment4.Location = new System.Drawing.Point(175, 25);
            this.labelSegment4.Name = "labelSegment4";
            this.labelSegment4.Size = new System.Drawing.Size(13, 13);
            this.labelSegment4.TabIndex = 124;
            this.labelSegment4.Text = "1";
            // 
            // labelSegment5
            // 
            this.labelSegment5.AutoSize = true;
            this.labelSegment5.Location = new System.Drawing.Point(225, 25);
            this.labelSegment5.Name = "labelSegment5";
            this.labelSegment5.Size = new System.Drawing.Size(13, 13);
            this.labelSegment5.TabIndex = 123;
            this.labelSegment5.Text = "1";
            // 
            // labelSegment2
            // 
            this.labelSegment2.AutoSize = true;
            this.labelSegment2.Enabled = false;
            this.labelSegment2.Location = new System.Drawing.Point(126, 27);
            this.labelSegment2.Name = "labelSegment2";
            this.labelSegment2.Size = new System.Drawing.Size(13, 13);
            this.labelSegment2.TabIndex = 122;
            this.labelSegment2.Text = "1";
            this.labelSegment2.Visible = false;
            // 
            // labelSegment3
            // 
            this.labelSegment3.AutoSize = true;
            this.labelSegment3.Enabled = false;
            this.labelSegment3.Location = new System.Drawing.Point(138, 19);
            this.labelSegment3.Name = "labelSegment3";
            this.labelSegment3.Size = new System.Drawing.Size(13, 13);
            this.labelSegment3.TabIndex = 121;
            this.labelSegment3.Text = "1";
            this.labelSegment3.Visible = false;
            // 
            // labelSegment0
            // 
            this.labelSegment0.AutoSize = true;
            this.labelSegment0.Enabled = false;
            this.labelSegment0.Location = new System.Drawing.Point(104, 25);
            this.labelSegment0.Name = "labelSegment0";
            this.labelSegment0.Size = new System.Drawing.Size(13, 13);
            this.labelSegment0.TabIndex = 120;
            this.labelSegment0.Text = "1";
            this.labelSegment0.Visible = false;
            // 
            // textBoxInc9
            // 
            this.textBoxInc9.Location = new System.Drawing.Point(412, 71);
            this.textBoxInc9.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxInc9.MaxLength = 4;
            this.textBoxInc9.Name = "textBoxInc9";
            this.textBoxInc9.Size = new System.Drawing.Size(41, 20);
            this.textBoxInc9.TabIndex = 119;
            this.textBoxInc9.Text = "1";
            this.textBoxInc9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxInc8
            // 
            this.textBoxInc8.Location = new System.Drawing.Point(363, 71);
            this.textBoxInc8.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxInc8.MaxLength = 2;
            this.textBoxInc8.Name = "textBoxInc8";
            this.textBoxInc8.Size = new System.Drawing.Size(31, 20);
            this.textBoxInc8.TabIndex = 118;
            this.textBoxInc8.Text = "1";
            this.textBoxInc8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxInc7
            // 
            this.textBoxInc7.Location = new System.Drawing.Point(314, 71);
            this.textBoxInc7.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxInc7.MaxLength = 2;
            this.textBoxInc7.Name = "textBoxInc7";
            this.textBoxInc7.Size = new System.Drawing.Size(31, 20);
            this.textBoxInc7.TabIndex = 117;
            this.textBoxInc7.Text = "1";
            this.textBoxInc7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxInc6
            // 
            this.textBoxInc6.Location = new System.Drawing.Point(265, 71);
            this.textBoxInc6.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxInc6.MaxLength = 2;
            this.textBoxInc6.Name = "textBoxInc6";
            this.textBoxInc6.Size = new System.Drawing.Size(31, 20);
            this.textBoxInc6.TabIndex = 116;
            this.textBoxInc6.Text = "1";
            this.textBoxInc6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxInc5
            // 
            this.textBoxInc5.Location = new System.Drawing.Point(216, 71);
            this.textBoxInc5.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxInc5.MaxLength = 2;
            this.textBoxInc5.Name = "textBoxInc5";
            this.textBoxInc5.Size = new System.Drawing.Size(31, 20);
            this.textBoxInc5.TabIndex = 115;
            this.textBoxInc5.Text = "1";
            this.textBoxInc5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxInc4
            // 
            this.textBoxInc4.Location = new System.Drawing.Point(167, 71);
            this.textBoxInc4.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxInc4.MaxLength = 2;
            this.textBoxInc4.Name = "textBoxInc4";
            this.textBoxInc4.Size = new System.Drawing.Size(31, 20);
            this.textBoxInc4.TabIndex = 114;
            this.textBoxInc4.Text = "1";
            this.textBoxInc4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxInc3
            // 
            this.textBoxInc3.Enabled = false;
            this.textBoxInc3.Location = new System.Drawing.Point(129, 65);
            this.textBoxInc3.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxInc3.MaxLength = 2;
            this.textBoxInc3.Name = "textBoxInc3";
            this.textBoxInc3.Size = new System.Drawing.Size(31, 20);
            this.textBoxInc3.TabIndex = 113;
            this.textBoxInc3.Text = "1";
            this.textBoxInc3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxInc3.Visible = false;
            // 
            // textBoxInc2
            // 
            this.textBoxInc2.Enabled = false;
            this.textBoxInc2.Location = new System.Drawing.Point(118, 73);
            this.textBoxInc2.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxInc2.MaxLength = 2;
            this.textBoxInc2.Name = "textBoxInc2";
            this.textBoxInc2.Size = new System.Drawing.Size(31, 20);
            this.textBoxInc2.TabIndex = 112;
            this.textBoxInc2.Text = "1";
            this.textBoxInc2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxInc2.Visible = false;
            // 
            // textBoxInc1
            // 
            this.textBoxInc1.Enabled = false;
            this.textBoxInc1.Location = new System.Drawing.Point(107, 67);
            this.textBoxInc1.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxInc1.MaxLength = 2;
            this.textBoxInc1.Name = "textBoxInc1";
            this.textBoxInc1.Size = new System.Drawing.Size(31, 20);
            this.textBoxInc1.TabIndex = 111;
            this.textBoxInc1.Text = "1";
            this.textBoxInc1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxInc1.Visible = false;
            // 
            // textBoxStart8
            // 
            this.textBoxStart8.Location = new System.Drawing.Point(363, 43);
            this.textBoxStart8.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxStart8.MaxLength = 2;
            this.textBoxStart8.Name = "textBoxStart8";
            this.textBoxStart8.Size = new System.Drawing.Size(31, 20);
            this.textBoxStart8.TabIndex = 110;
            this.textBoxStart8.Text = "00";
            this.textBoxStart8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxStart7
            // 
            this.textBoxStart7.Location = new System.Drawing.Point(314, 43);
            this.textBoxStart7.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxStart7.MaxLength = 2;
            this.textBoxStart7.Name = "textBoxStart7";
            this.textBoxStart7.Size = new System.Drawing.Size(31, 20);
            this.textBoxStart7.TabIndex = 109;
            this.textBoxStart7.Text = "00";
            this.textBoxStart7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxStart6
            // 
            this.textBoxStart6.Location = new System.Drawing.Point(265, 43);
            this.textBoxStart6.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxStart6.MaxLength = 2;
            this.textBoxStart6.Name = "textBoxStart6";
            this.textBoxStart6.Size = new System.Drawing.Size(31, 20);
            this.textBoxStart6.TabIndex = 108;
            this.textBoxStart6.Text = "00";
            this.textBoxStart6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxStart5
            // 
            this.textBoxStart5.Location = new System.Drawing.Point(216, 43);
            this.textBoxStart5.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxStart5.MaxLength = 2;
            this.textBoxStart5.Name = "textBoxStart5";
            this.textBoxStart5.Size = new System.Drawing.Size(31, 20);
            this.textBoxStart5.TabIndex = 107;
            this.textBoxStart5.Text = "00";
            this.textBoxStart5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxStart4
            // 
            this.textBoxStart4.Location = new System.Drawing.Point(167, 43);
            this.textBoxStart4.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxStart4.MaxLength = 2;
            this.textBoxStart4.Name = "textBoxStart4";
            this.textBoxStart4.Size = new System.Drawing.Size(31, 20);
            this.textBoxStart4.TabIndex = 106;
            this.textBoxStart4.Text = "00";
            this.textBoxStart4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxStart3
            // 
            this.textBoxStart3.Enabled = false;
            this.textBoxStart3.Location = new System.Drawing.Point(129, 37);
            this.textBoxStart3.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxStart3.MaxLength = 2;
            this.textBoxStart3.Name = "textBoxStart3";
            this.textBoxStart3.Size = new System.Drawing.Size(31, 20);
            this.textBoxStart3.TabIndex = 105;
            this.textBoxStart3.Text = "00";
            this.textBoxStart3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxStart3.Visible = false;
            // 
            // textBoxInc0
            // 
            this.textBoxInc0.Enabled = false;
            this.textBoxInc0.Location = new System.Drawing.Point(97, 71);
            this.textBoxInc0.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxInc0.MaxLength = 2;
            this.textBoxInc0.Name = "textBoxInc0";
            this.textBoxInc0.Size = new System.Drawing.Size(31, 20);
            this.textBoxInc0.TabIndex = 104;
            this.textBoxInc0.Text = "1";
            this.textBoxInc0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxInc0.Visible = false;
            // 
            // textBoxStart2
            // 
            this.textBoxStart2.Enabled = false;
            this.textBoxStart2.Location = new System.Drawing.Point(118, 45);
            this.textBoxStart2.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxStart2.MaxLength = 2;
            this.textBoxStart2.Name = "textBoxStart2";
            this.textBoxStart2.Size = new System.Drawing.Size(31, 20);
            this.textBoxStart2.TabIndex = 103;
            this.textBoxStart2.Text = "00";
            this.textBoxStart2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxStart2.Visible = false;
            // 
            // textBoxStart1
            // 
            this.textBoxStart1.Enabled = false;
            this.textBoxStart1.Location = new System.Drawing.Point(107, 39);
            this.textBoxStart1.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxStart1.MaxLength = 2;
            this.textBoxStart1.Name = "textBoxStart1";
            this.textBoxStart1.Size = new System.Drawing.Size(31, 20);
            this.textBoxStart1.TabIndex = 102;
            this.textBoxStart1.Text = "00";
            this.textBoxStart1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxStart1.Visible = false;
            // 
            // textBoxStart0
            // 
            this.textBoxStart0.Enabled = false;
            this.textBoxStart0.Location = new System.Drawing.Point(97, 43);
            this.textBoxStart0.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxStart0.MaxLength = 2;
            this.textBoxStart0.Name = "textBoxStart0";
            this.textBoxStart0.Size = new System.Drawing.Size(31, 20);
            this.textBoxStart0.TabIndex = 98;
            this.textBoxStart0.Text = "00";
            this.textBoxStart0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxStart0.Visible = false;
            // 
            // checkBoxIncrementChar
            // 
            this.checkBoxIncrementChar.AutoSize = true;
            this.checkBoxIncrementChar.Location = new System.Drawing.Point(476, 73);
            this.checkBoxIncrementChar.Name = "checkBoxIncrementChar";
            this.checkBoxIncrementChar.Size = new System.Drawing.Size(38, 17);
            this.checkBoxIncrementChar.TabIndex = 91;
            this.checkBoxIncrementChar.Text = "+1";
            this.checkBoxIncrementChar.UseVisualStyleBackColor = true;
            // 
            // textBoxStartPreview
            // 
            this.textBoxStartPreview.Location = new System.Drawing.Point(97, 100);
            this.textBoxStartPreview.Name = "textBoxStartPreview";
            this.textBoxStartPreview.ReadOnly = true;
            this.textBoxStartPreview.Size = new System.Drawing.Size(100, 20);
            this.textBoxStartPreview.TabIndex = 81;
            this.textBoxStartPreview.TabStop = false;
            this.textBoxStartPreview.Text = "01.01.0001.a";
            this.textBoxStartPreview.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 80;
            this.label7.Text = "Start position";
            // 
            // labelSegment1
            // 
            this.labelSegment1.AutoSize = true;
            this.labelSegment1.Enabled = false;
            this.labelSegment1.Location = new System.Drawing.Point(115, 21);
            this.labelSegment1.Name = "labelSegment1";
            this.labelSegment1.Size = new System.Drawing.Size(13, 13);
            this.labelSegment1.TabIndex = 60;
            this.labelSegment1.Text = "1";
            this.labelSegment1.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(27, 74);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 13);
            this.label16.TabIndex = 72;
            this.label16.Text = "Increment";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(52, 46);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(29, 13);
            this.label20.TabIndex = 68;
            this.label20.Text = "Start";
            // 
            // textBoxStart9
            // 
            this.textBoxStart9.Location = new System.Drawing.Point(412, 43);
            this.textBoxStart9.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.textBoxStart9.MaxLength = 4;
            this.textBoxStart9.Name = "textBoxStart9";
            this.textBoxStart9.Size = new System.Drawing.Size(41, 20);
            this.textBoxStart9.TabIndex = 66;
            this.textBoxStart9.Text = "0000";
            this.textBoxStart9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxStart10
            // 
            this.textBoxStart10.Location = new System.Drawing.Point(471, 43);
            this.textBoxStart10.MaxLength = 1;
            this.textBoxStart10.Name = "textBoxStart10";
            this.textBoxStart10.Size = new System.Drawing.Size(22, 20);
            this.textBoxStart10.TabIndex = 67;
            this.textBoxStart10.Text = "a";
            this.textBoxStart10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDelete.Location = new System.Drawing.Point(168, 355);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 82;
            this.buttonDelete.Text = "Delete";
            this.toolTip1.SetToolTip(this.buttonDelete, "Delete selected.");
            this.buttonDelete.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Location = new System.Drawing.Point(30, 180);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(842, 145);
            this.panel1.TabIndex = 83;
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(716, 402);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 84;
            this.buttonOk.Text = "Apply";
            this.buttonOk.UseVisualStyleBackColor = true;
            // 
            // buttonReset
            // 
            this.buttonReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReset.Location = new System.Drawing.Point(487, 402);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(75, 23);
            this.buttonReset.TabIndex = 85;
            this.buttonReset.Text = "Reset";
            this.toolTip1.SetToolTip(this.buttonReset, "Resets all changes.");
            this.buttonReset.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(797, 402);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 86;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClear.Location = new System.Drawing.Point(580, 402);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 87;
            this.buttonClear.Text = "Clear";
            this.toolTip1.SetToolTip(this.buttonClear, "Deletes all data.");
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // PositionDefinitionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 437);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.checkBoxLastCharSegment);
            this.Controls.Add(this.labelNextPositionPreview);
            this.Controls.Add(this.buttonCreatePosition);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxNumberOfSegments);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PositionDefinitionView";
            this.Text = "PositionDefinition";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox comboBoxNumberOfSegments;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button buttonCreatePosition;
		private System.Windows.Forms.Label labelNextPositionPreview;
		private System.Windows.Forms.CheckBox checkBoxLastCharSegment;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label labelSegment10;
		private System.Windows.Forms.Label labelSegment9;
		private System.Windows.Forms.Label labelSegment8;
		private System.Windows.Forms.Label labelSegment6;
		private System.Windows.Forms.Label labelSegment7;
		private System.Windows.Forms.Label labelSegment4;
		private System.Windows.Forms.Label labelSegment5;
		private System.Windows.Forms.Label labelSegment2;
		private System.Windows.Forms.Label labelSegment3;
		private System.Windows.Forms.Label labelSegment0;
		private System.Windows.Forms.TextBox textBoxInc9;
		private System.Windows.Forms.TextBox textBoxInc8;
		private System.Windows.Forms.TextBox textBoxInc7;
		private System.Windows.Forms.TextBox textBoxInc6;
		private System.Windows.Forms.TextBox textBoxInc5;
		private System.Windows.Forms.TextBox textBoxInc4;
		private System.Windows.Forms.TextBox textBoxInc3;
		private System.Windows.Forms.TextBox textBoxInc2;
		private System.Windows.Forms.TextBox textBoxInc1;
		private System.Windows.Forms.TextBox textBoxStart8;
		private System.Windows.Forms.TextBox textBoxStart7;
		private System.Windows.Forms.TextBox textBoxStart6;
		private System.Windows.Forms.TextBox textBoxStart5;
		private System.Windows.Forms.TextBox textBoxStart4;
		private System.Windows.Forms.TextBox textBoxStart3;
		private System.Windows.Forms.TextBox textBoxInc0;
		private System.Windows.Forms.TextBox textBoxStart2;
		private System.Windows.Forms.TextBox textBoxStart1;
		private System.Windows.Forms.TextBox textBoxStart0;
		private System.Windows.Forms.CheckBox checkBoxIncrementChar;
		private System.Windows.Forms.TextBox textBoxStartPreview;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label labelSegment1;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.TextBox textBoxStart9;
		private System.Windows.Forms.TextBox textBoxStart10;
		private System.Windows.Forms.Button buttonDelete;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.ErrorProvider errorProvider;
		private System.Windows.Forms.Button buttonOk;
		private System.Windows.Forms.Button buttonReset;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Button buttonClear;
	}
}