﻿using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views.UnitAssociationControl
{
	public interface IUnitAssociationView
	{
		TextBox TextBoxArea { get; }
		TextBox TextBoxCount { get; }
		TextBox TextBoxLength { get; }
	}
}