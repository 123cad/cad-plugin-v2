﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views.UnitAssociationControl
{
	public partial class UnitAssociationView : UserControl, IUnitAssociationView
	{
		public TextBox TextBoxCount => textBoxCount;
		public TextBox TextBoxLength => textBoxLength;
		public TextBox TextBoxArea => textBoxArea;
		public UnitAssociationView()
		{
			InitializeComponent();
		}
	}
}
