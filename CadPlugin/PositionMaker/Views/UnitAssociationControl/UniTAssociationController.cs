﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views.UnitAssociationControl
{
	class UnitAssociationController
	{
		private IUnitAssociationView view;

		public Models.UnitAssociations UnitAssociations { get; private set; } = new Models.UnitAssociations();
		public event Action AssociationChanged;
		public UnitAssociationController(IUnitAssociationView v)
		{
			view = v;
			v.TextBoxCount.TextChanged += (_, e)=>TextBox_TextChanged((TextBox)_, Models.Unit.Count);
			v.TextBoxLength.TextChanged += (_, e)=>TextBox_TextChanged((TextBox)_, Models.Unit.Length);
			v.TextBoxArea.TextChanged += (_, e)=>TextBox_TextChanged((TextBox)_, Models.Unit.Area);
			TextBox_TextChanged(v.TextBoxCount, Models.Unit.Count);
			TextBox_TextChanged(v.TextBoxLength, Models.Unit.Length);
			TextBox_TextChanged(v.TextBoxArea, Models.Unit.Area);
		}
		
		private void TextBox_TextChanged(TextBox t, Models.Unit u)
		{
			string s = t.Text;
			string[] vals = s.Split(' ');
			vals = vals.Where(x => !string.IsNullOrEmpty(x.Trim())).ToArray();
			UnitAssociations.SetUnit(u, vals);
			AssociationChanged?.Invoke();
		}
	}
}
