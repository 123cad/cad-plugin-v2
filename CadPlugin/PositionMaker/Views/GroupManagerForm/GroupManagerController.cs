﻿using CadPlugin.PositionMaker.Models;
using Inventory.Positions;
using Inventory.Positions.Definitions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views.GroupManagerForm
{
	class GroupManagerController
	{
		private IGroupManagerView view;

		private SinglePosition[] allPositions;
		private List<string> positionsInGroup;

		private PositionsManager manager;

		private PositionGroup currentGroup { get; set; } 
		/// <summary>
		/// When radiobutton create new group is checked, we don't create new group.
		/// </summary>
		private PositionGroup newGroup { get; set; } = new PositionGroup("");
		
		private ErrorProvider error = new ErrorProvider();

		public static void StartGroupManager(PositionsManager mgr)
		{
			IGroupManagerView v = new GroupManagerView();
			GroupManagerController ctr = new GroupManagerController(mgr, v);
			((Form)v).ShowDialog();
			ctr.error.Dispose();
		}

		private GroupManagerController(PositionsManager mgr, IGroupManagerView v)
		{
			view = v;
			manager = mgr;
			currentGroup = newGroup;
			allPositions = mgr.GetAllSegmentData().Select(x => new SinglePosition() { Position = x.GetPositionId(), Description = x.Description, Unit = x.UnitOfMeasurement }).ToArray();

			view.DgvAllPositions.VirtualMode = true;
			view.DgvAllPositions.CellValueNeeded += DgvAllPositions_CellValueNeeded;
			view.DgvAllPositions.RowCount = allPositions.Length;
			view.DgvAllPositions.AutoGenerateColumns = false;
			view.DgvGroupPositions.AutoGenerateColumns = true;

			view.AddPositionsToGroup.Click += AddPositionsToGroup_Click;
			view.RemovePositionsFromGroup.Click += RemovePositionsFromGroup_Click;


			view.NewGroupName.TextChanged += (_, e) =>
			 {
				 currentGroup.Name = view.NewGroupName.Text.Trim();
				 ValidateGroupName();
			 };
			view.Ok.Click += (_, e) =>
			{
				bool validated = ValidateGroupName();
				validated = validated && ValidateGroupData();
				if (!validated)
					return;
				mgr.Groups.Add(currentGroup);
				((Form)view).Close();
				
			};

			view.ExistingGroups.Enabled = false;
			if (mgr.Groups.Count == 0)
			{
				view.EditExistingGroup.Enabled = false;
			}
			else
			{
				view.ExistingGroups.DisplayMember = nameof(PositionGroup.Name);
				view.ExistingGroups.ValueMember = nameof(PositionGroup);
				view.ExistingGroups.DataSource = mgr.Groups;
				view.ExistingGroups.SelectedIndex = 0;
				view.EditExistingGroup.CheckedChanged += (_, e) =>
				{
					if (view.EditExistingGroup.Checked)
					{
						currentGroup = (PositionGroup)view.ExistingGroups.SelectedItem;
						view.NewGroupName.Enabled = false;
						view.ExistingGroups.Enabled = true;
					}
					else
					{
						currentGroup = newGroup;
						view.NewGroupName.Enabled = true;
						view.ExistingGroups.Enabled = false;
					}
					RefreshCurrentGroupPositions();
				};
				view.ExistingGroups.SelectedIndexChanged += (_, e) =>
				{
					var pos = (PositionGroup)view.ExistingGroups.SelectedItem;
					RefreshCurrentGroupPositions();
				};
			}
			view.DgvAllPositions.RowPrePaint += DgvAllPositions_RowPrePaint;
			view.DgvAllPositions.RowPostPaint += DgvAllPositions_RowPostPaint;
			view.DgvAllPositions.CellPainting += DgvAllPositions_CellPainting;
			//view.DgvAllPositions.sel
		}

		private void DgvAllPositions_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
		{
			if (e.RowIndex == -1)
				return;
			if (e.ColumnIndex != 0)
				return;
			e.Paint(e.ClipBounds, e.PaintParts);
			var v = allPositions[e.RowIndex];
			if (currentGroup.HasPosition(v.Position))
			{

				var pen = System.Drawing.Pens.Black;
				if (e.State.HasFlag(DataGridViewElementStates.Selected))
					pen = System.Drawing.Pens.White;
				e.Graphics.DrawLine(pen,
					e.CellBounds.X + 2,
					e.CellBounds.Y + e.CellBounds.Height - 4,
					e.CellBounds.X + e.CellBounds.Width - 4,
					e.CellBounds.Y + 2);
			}
			e.Handled = true;
		}

		private void DgvAllPositions_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
		{
			return;
			if (e.RowIndex == -1)
				return;
			//if (e.State.HasFlag(DataGridViewElementStates.Selected))
			{
				DataGridViewRow row = view.DgvAllPositions.Rows[e.RowIndex];
				//var rec = new System.Drawing.Rectangle(e.RowBounds.X + 1, e.RowBounds.Y, e.RowBounds.Width - 2, e.RowBounds.Height - 0);
				//e.Graphics.DrawRectangle(System.Drawing.Pens.Red, rec);
				var v = allPositions[e.RowIndex];
				if (currentGroup.HasPosition(v.Position))
				{

					var pen = System.Drawing.Pens.Black;
					if (e.State.HasFlag(DataGridViewElementStates.Selected))
						pen = System.Drawing.Pens.White;
					e.Graphics.DrawLine(pen,
						e.RowBounds.X + 2,
						e.RowBounds.Y + e.RowBounds.Height- 4,
						e.RowBounds.X + e.RowBounds.Width - 4,
						e.RowBounds.Y + 2);
				}
			}
		}

		private void DgvAllPositions_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
		{
			int index = e.RowIndex;
			if (index == -1)
				return;
			DataGridViewRow row = view.DgvAllPositions.Rows[index];
			string val = row.Cells[0].Value as string;
			if (currentGroup.HasPosition(val))
			{
				row.DefaultCellStyle.ForeColor = System.Drawing.Color.LightGray;
				//row.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.LightGray;
			}
			else
			{
				row.DefaultCellStyle.ForeColor = System.Drawing.SystemColors.ControlText;
				//row.DefaultCellStyle.SelectionForeColor= System.Drawing.SystemColors.ControlText;
			}
			
			//if (e.State.HasFlag(DataGridViewElementStates.Selected))
				//e.Graphics.FillRectangle(System.Drawing.Brushes.Red, e.RowBounds);
		}

		private bool ValidateGroupName()
		{
			if (view.EditExistingGroup.Checked)
				return true;
			error.Clear();
			string name = currentGroup.Name;
			if (string.IsNullOrEmpty(name))
			{
				error.SetError(view.NewGroupName, "Name of the group is empty!");
				return false;
			}
			if (manager.Groups.Where(x => x.Name == name).Any())
			{
				error.SetError(view.NewGroupName, "Group already exists!");
				return false; 
			}
			
			return true;
		}
		private bool ValidateGroupData()
		{
			if (!currentGroup.Positions.Any())
			{
				MessageBox.Show("Group is empty. Please add positions first.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return false;
			}
			return true;
		}


		private void AddPositionsToGroup_Click(object sender, EventArgs e)
		{
			if ((view.DgvAllPositions.SelectedRows?.Count ?? 0) == 0)
				return;
			List<int> selectedRows = view.DgvAllPositions.SelectedRows.Cast<DataGridViewRow>().Select(x => x.Index).ToList();
			currentGroup.AddPositions(selectedRows.Select(x => allPositions[x].Position).ToArray());
			//selectedRows.ForEach(x => currentGroup.AddPosition(allPositions[x].Position));
			RefreshCurrentGroupPositions();
			view.RemovePositionsFromGroup.Enabled = true;
			view.DgvAllPositions.Invalidate();
		}

		private void RemovePositionsFromGroup_Click(object sender, EventArgs e)
		{
			if ((view.DgvGroupPositions.SelectedRows?.Count ?? 0) == 0)
				return;
			List<string> selectedPositions = view.DgvGroupPositions.SelectedRows.Cast<DataGridViewRow>().Select(x => x.Cells[0].Value as string).ToList();
			selectedPositions.ForEach(x => currentGroup.DeletePosition(x));
			RefreshCurrentGroupPositions();
			if (selectedPositions.Count == 0)
				view.RemovePositionsFromGroup.Enabled = false;
			view.DgvAllPositions.Invalidate();
		}
		private void RefreshCurrentGroupPositions()
		{
			view.DgvGroupPositions.Rows.Clear();
			foreach(var v in currentGroup.Positions.OrderBy(x=>x))
			{
				view.DgvGroupPositions.Rows.Add(v);
			}

			/*view.DgvGroupPositions.DataSource = null;
			BindingSource source = new BindingSource();
			source.DataSource = currentGroup.Positions.Select(x=>new { Positions = x }).ToList();
			view.DgvGroupPositions.DataSource = source;*/
			view.RemovePositionsFromGroup.Enabled = currentGroup.Positions.Any();

			view.DgvAllPositions.Invalidate();
		}

		private void DgvAllPositions_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
		{
			if (e.RowIndex == -1 || e.ColumnIndex == -1)
				return;
			SinglePosition p = allPositions[e.RowIndex];
			switch(e.ColumnIndex)
			{
				case 0: e.Value = p.Position; break;
				case 1: e.Value = p.Description; break;
				case 2: e.Value = p.Unit; break;
			}
		}
	}
}
