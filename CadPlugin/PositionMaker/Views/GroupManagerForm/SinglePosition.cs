﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PositionMaker.Views.GroupManagerForm
{
	class SinglePosition
	{
		public string Position { get; set; }
		public string Description { get; set; }
		public string Unit { get; set; }
	}
}
