﻿namespace CadPlugin.PositionMaker.Views
{
	partial class GroupManagerView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GroupManagerView));
			this.comboBoxGroups = new System.Windows.Forms.ComboBox();
			this.radioButtonNewGroup = new System.Windows.Forms.RadioButton();
			this.radioButtonEditExisting = new System.Windows.Forms.RadioButton();
			this.textBoxGroupName = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.dataGridViewAllPositions = new System.Windows.Forms.DataGridView();
			this.ColumnAllPosition = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnAllDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ColumnAllUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.buttonAddPositions = new System.Windows.Forms.Button();
			this.buttonRemovePositions = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.dataGridViewGroupPositions = new System.Windows.Forms.DataGridView();
			this.ColumnPositionsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.label3 = new System.Windows.Forms.Label();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
			this.buttonOk = new System.Windows.Forms.Button();
			this.buttonCancel = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewAllPositions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewGroupPositions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
			this.SuspendLayout();
			// 
			// comboBoxGroups
			// 
			this.comboBoxGroups.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxGroups.FormattingEnabled = true;
			this.comboBoxGroups.Location = new System.Drawing.Point(173, 74);
			this.comboBoxGroups.Name = "comboBoxGroups";
			this.comboBoxGroups.Size = new System.Drawing.Size(121, 21);
			this.comboBoxGroups.TabIndex = 0;
			// 
			// radioButtonNewGroup
			// 
			this.radioButtonNewGroup.AutoSize = true;
			this.radioButtonNewGroup.Checked = true;
			this.radioButtonNewGroup.Location = new System.Drawing.Point(34, 35);
			this.radioButtonNewGroup.Name = "radioButtonNewGroup";
			this.radioButtonNewGroup.Size = new System.Drawing.Size(109, 17);
			this.radioButtonNewGroup.TabIndex = 1;
			this.radioButtonNewGroup.TabStop = true;
			this.radioButtonNewGroup.Text = "Create new group";
			this.radioButtonNewGroup.UseVisualStyleBackColor = true;
			// 
			// radioButtonEditExisting
			// 
			this.radioButtonEditExisting.AutoSize = true;
			this.radioButtonEditExisting.Location = new System.Drawing.Point(34, 75);
			this.radioButtonEditExisting.Name = "radioButtonEditExisting";
			this.radioButtonEditExisting.Size = new System.Drawing.Size(128, 17);
			this.radioButtonEditExisting.TabIndex = 2;
			this.radioButtonEditExisting.Text = "Update existing group";
			this.radioButtonEditExisting.UseVisualStyleBackColor = true;
			// 
			// textBoxGroupName
			// 
			this.textBoxGroupName.Location = new System.Drawing.Point(173, 34);
			this.textBoxGroupName.Name = "textBoxGroupName";
			this.textBoxGroupName.Size = new System.Drawing.Size(121, 20);
			this.textBoxGroupName.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(170, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(65, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "Group name";
			// 
			// dataGridViewAllPositions
			// 
			this.dataGridViewAllPositions.AllowUserToAddRows = false;
			this.dataGridViewAllPositions.AllowUserToDeleteRows = false;
			this.dataGridViewAllPositions.AllowUserToResizeColumns = false;
			this.dataGridViewAllPositions.AllowUserToResizeRows = false;
			this.dataGridViewAllPositions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dataGridViewAllPositions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewAllPositions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnAllPosition,
            this.ColumnAllDescription,
            this.ColumnAllUnit});
			this.dataGridViewAllPositions.Location = new System.Drawing.Point(34, 147);
			this.dataGridViewAllPositions.Name = "dataGridViewAllPositions";
			this.dataGridViewAllPositions.RowHeadersVisible = false;
			this.dataGridViewAllPositions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewAllPositions.Size = new System.Drawing.Size(263, 134);
			this.dataGridViewAllPositions.TabIndex = 5;
			// 
			// ColumnAllPosition
			// 
			this.ColumnAllPosition.HeaderText = "Position";
			this.ColumnAllPosition.Name = "ColumnAllPosition";
			this.ColumnAllPosition.ReadOnly = true;
			// 
			// ColumnAllDescription
			// 
			this.ColumnAllDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.ColumnAllDescription.HeaderText = "Description";
			this.ColumnAllDescription.Name = "ColumnAllDescription";
			this.ColumnAllDescription.ReadOnly = true;
			// 
			// ColumnAllUnit
			// 
			this.ColumnAllUnit.HeaderText = "Unit";
			this.ColumnAllUnit.Name = "ColumnAllUnit";
			this.ColumnAllUnit.ReadOnly = true;
			this.ColumnAllUnit.Width = 60;
			// 
			// buttonAddPositions
			// 
			this.buttonAddPositions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonAddPositions.Location = new System.Drawing.Point(312, 200);
			this.buttonAddPositions.Name = "buttonAddPositions";
			this.buttonAddPositions.Size = new System.Drawing.Size(50, 23);
			this.buttonAddPositions.TabIndex = 6;
			this.buttonAddPositions.Text = "> >";
			this.buttonAddPositions.UseVisualStyleBackColor = true;
			// 
			// buttonRemovePositions
			// 
			this.buttonRemovePositions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonRemovePositions.Location = new System.Drawing.Point(312, 229);
			this.buttonRemovePositions.Name = "buttonRemovePositions";
			this.buttonRemovePositions.Size = new System.Drawing.Size(50, 23);
			this.buttonRemovePositions.TabIndex = 7;
			this.buttonRemovePositions.Text = "< <";
			this.buttonRemovePositions.UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(31, 128);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(62, 13);
			this.label2.TabIndex = 8;
			this.label2.Text = "All positions";
			// 
			// dataGridViewGroupPositions
			// 
			this.dataGridViewGroupPositions.AllowUserToAddRows = false;
			this.dataGridViewGroupPositions.AllowUserToDeleteRows = false;
			this.dataGridViewGroupPositions.AllowUserToResizeColumns = false;
			this.dataGridViewGroupPositions.AllowUserToResizeRows = false;
			this.dataGridViewGroupPositions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dataGridViewGroupPositions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewGroupPositions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnPositionsName});
			this.dataGridViewGroupPositions.Location = new System.Drawing.Point(376, 146);
			this.dataGridViewGroupPositions.Name = "dataGridViewGroupPositions";
			this.dataGridViewGroupPositions.RowHeadersVisible = false;
			this.dataGridViewGroupPositions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewGroupPositions.Size = new System.Drawing.Size(141, 135);
			this.dataGridViewGroupPositions.TabIndex = 9;
			// 
			// ColumnPositionsName
			// 
			this.ColumnPositionsName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.ColumnPositionsName.HeaderText = "Positions";
			this.ColumnPositionsName.Name = "ColumnPositionsName";
			this.ColumnPositionsName.ReadOnly = true;
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(374, 128);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(108, 13);
			this.label3.TabIndex = 10;
			this.label3.Text = "Positions in the group";
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			// 
			// buttonOk
			// 
			this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonOk.Location = new System.Drawing.Point(363, 299);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(75, 23);
			this.buttonOk.TabIndex = 11;
			this.buttonOk.Text = "Ok";
			this.buttonOk.UseVisualStyleBackColor = true;
			// 
			// buttonCancel
			// 
			this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonCancel.Location = new System.Drawing.Point(444, 299);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(75, 23);
			this.buttonCancel.TabIndex = 12;
			this.buttonCancel.Text = "Cancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
			// 
			// GroupManagerView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(531, 334);
			this.Controls.Add(this.buttonCancel);
			this.Controls.Add(this.buttonOk);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.dataGridViewGroupPositions);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.buttonRemovePositions);
			this.Controls.Add(this.buttonAddPositions);
			this.Controls.Add(this.dataGridViewAllPositions);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.textBoxGroupName);
			this.Controls.Add(this.radioButtonEditExisting);
			this.Controls.Add(this.radioButtonNewGroup);
			this.Controls.Add(this.comboBoxGroups);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(547, 373);
			this.Name = "GroupManagerView";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "GroupManager";
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewAllPositions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewGroupPositions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox comboBoxGroups;
		private System.Windows.Forms.RadioButton radioButtonNewGroup;
		private System.Windows.Forms.RadioButton radioButtonEditExisting;
		private System.Windows.Forms.TextBox textBoxGroupName;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DataGridView dataGridViewAllPositions;
		private System.Windows.Forms.Button buttonAddPositions;
		private System.Windows.Forms.Button buttonRemovePositions;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DataGridView dataGridViewGroupPositions;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAllPosition;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAllDescription;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAllUnit;
		private System.Windows.Forms.ErrorProvider errorProvider1;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.Button buttonOk;
		private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPositionsName;
	}
}