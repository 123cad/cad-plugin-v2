﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views.GroupManagerForm
{
	interface IGroupManagerView
	{
		DataGridView DgvAllPositions { get; }
		DataGridView DgvGroupPositions { get; }

		RadioButton CreateNewGroup { get; }
		RadioButton EditExistingGroup { get; }

		TextBox NewGroupName { get; }
		ComboBox ExistingGroups { get; }

		Button AddPositionsToGroup { get; }
		Button RemovePositionsFromGroup { get; }
		Button Ok { get; }
	}
}
