﻿using CadPlugin.PositionMaker.Views.GroupManagerForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views
{
	public partial class GroupManagerView : Form, IGroupManagerView
	{
		public GroupManagerView()
		{
			InitializeComponent();
		}

		public Button AddPositionsToGroup => buttonAddPositions;

		public RadioButton CreateNewGroup => radioButtonNewGroup;

		public DataGridView DgvAllPositions => dataGridViewAllPositions;

		public DataGridView DgvGroupPositions => dataGridViewGroupPositions;

		public RadioButton EditExistingGroup => radioButtonEditExisting;

		public ComboBox ExistingGroups => comboBoxGroups;

		public TextBox NewGroupName => textBoxGroupName;

		public Button Ok => buttonOk;

		public Button RemovePositionsFromGroup => buttonRemovePositions;
		private void buttonCancel_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}
