﻿using CadPlugin.PositionMaker.Models;
using CadPlugin.PositionMaker.Views.TreeSegmentDisplay;
using Inventory.Positions;
using Inventory.Positions.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.PositionMaker.Views
{
	public partial class PositionDefinitionView : Form
	{
		private PositionDefinition definition = new PositionDefinition();
		public PositionsManager manager { get; private set; } = new PositionsManager();
		/// <summary>
		/// Manager which is used as initial data source.
		/// </summary>
		private PositionsManager sourceManager { get; set; }
		//private ITreeSegmentDisplayView treeView;
		private TreeSegmentDisplayController treeController;
		private int TotalSegments = 7;
		
		class SegmentControls
		{
			public Label Label;
			public TextBox Start;
			public TextBox Increment;
		}
		/// <summary>
		/// Segment control groups from end.
		/// </summary>
		private SegmentControls[] AllSegmentControls;
		public PositionDefinitionView()
		{
			InitializeComponent();

			Height = 500;

			AllSegmentControls = new SegmentControls[TotalSegments];
			int i = 0;
			AllSegmentControls[i++] = new SegmentControls { Label = labelSegment10, Start = textBoxStart10, Increment = null };
			AllSegmentControls[i++] = new SegmentControls { Label = labelSegment9, Start = textBoxStart9, Increment = textBoxInc9 };
			AllSegmentControls[i++] = new SegmentControls { Label = labelSegment8, Start = textBoxStart8, Increment = textBoxInc8 };
			AllSegmentControls[i++] = new SegmentControls { Label = labelSegment7, Start = textBoxStart7, Increment = textBoxInc7 };
			AllSegmentControls[i++] = new SegmentControls { Label = labelSegment6, Start = textBoxStart6, Increment = textBoxInc6 };
			AllSegmentControls[i++] = new SegmentControls { Label = labelSegment5, Start = textBoxStart5, Increment = textBoxInc5 };
			AllSegmentControls[i++] = new SegmentControls { Label = labelSegment4, Start = textBoxStart4, Increment = textBoxInc4 };
			//AllSegmentControls[i++] = new SegmentControls { Label = labelSegment3, Start = textBoxStart3, Increment = textBoxInc3 };
			//AllSegmentControls[i++] = new SegmentControls { Label = labelSegment2, Start = textBoxStart2, Increment = textBoxInc2 };
			//AllSegmentControls[i++] = new SegmentControls { Label = labelSegment1, Start = textBoxStart1, Increment = textBoxInc1 };
			//AllSegmentControls[i++] = new SegmentControls { Label = labelSegment0, Start = textBoxStart0, Increment = textBoxInc0 };

			TreeSegmentDisplayView td = new TreeSegmentDisplayView();
			treeController = new TreeSegmentDisplayController(td, manager);
			treeController.SelectedSegmentChanged += () => UpdateNextNewPositionPreview();
			panel1.Controls.Add(td);
			td.Dock = DockStyle.Fill;

			comboBoxNumberOfSegments.Items.Clear();
			comboBoxNumberOfSegments.Items.AddRange(Enumerable.Range(2, TotalSegments - 1).Select(x => (object)x).ToArray());
			comboBoxNumberOfSegments.SelectedIndexChanged += (_, e) => NumberOfSegmentsChanged();
			checkBoxLastCharSegment.CheckedChanged += (_, e) => NumberOfSegmentsChanged();
			comboBoxNumberOfSegments.SelectedIndex = 0;



			buttonCreatePosition.Click += ButtonCreatePosition_Click;
			buttonDelete.Click += ButtonDelete_Click;

			//textBoxStart0.TextChanged += TextBoxStart_TextChanged;
			//textBoxStart1.TextChanged += TextBoxStart_TextChanged;
			//textBoxStart2.TextChanged += TextBoxStart_TextChanged;
			//textBoxStart3.TextChanged += TextBoxStart_TextChanged;
			textBoxStart4.TextChanged += TextBoxStart_TextChanged;
			textBoxStart5.TextChanged += TextBoxStart_TextChanged;
			textBoxStart6.TextChanged += TextBoxStart_TextChanged;
			textBoxStart7.TextChanged += TextBoxStart_TextChanged;
			textBoxStart8.TextChanged += TextBoxStart_TextChanged;
			textBoxStart9.TextChanged += TextBoxStart_TextChanged;
			textBoxStart10.TextChanged += TextBoxStart_TextChanged;

			//textBoxInc0.TextChanged += TextBoxInc_TextChanged;
			//textBoxInc1.TextChanged += TextBoxInc_TextChanged;
			//textBoxInc2.TextChanged += TextBoxInc_TextChanged;
			//textBoxInc3.TextChanged += TextBoxInc_TextChanged;
			textBoxInc4.TextChanged += TextBoxInc_TextChanged;
			textBoxInc5.TextChanged += TextBoxInc_TextChanged;
			textBoxInc6.TextChanged += TextBoxInc_TextChanged;
			textBoxInc7.TextChanged += TextBoxInc_TextChanged;
			textBoxInc8.TextChanged += TextBoxInc_TextChanged;
			textBoxInc9.TextChanged += TextBoxInc_TextChanged;
			checkBoxIncrementChar.CheckedChanged += (_, e) => 
					{ ((SegmentDefinitionChar)definition.Segments.Last()).Incremented = checkBoxIncrementChar.Checked; };

			//textBoxStart0.KeyPress += TextBoxStart_KeyPress;
			//textBoxStart1.KeyPress += TextBoxStart_KeyPress;
			//textBoxStart2.KeyPress += TextBoxStart_KeyPress;
			//textBoxStart3.KeyPress += TextBoxStart_KeyPress;
			textBoxStart4.KeyPress += TextBoxStart_KeyPress;
			textBoxStart5.KeyPress += TextBoxStart_KeyPress;
			textBoxStart6.KeyPress += TextBoxStart_KeyPress;
			textBoxStart7.KeyPress += TextBoxStart_KeyPress;
			textBoxStart8.KeyPress += TextBoxStart_KeyPress;
			textBoxStart9.KeyPress += TextBoxStart_KeyPress;
			textBoxStart10.KeyPress += TextBoxStart_KeyPress;

			buttonOk.Click += (_, e) => { DialogResult = DialogResult.OK; Close(); };
			buttonReset.Click += (_, e) => Reset(false);

		}
		public void InitializeWithExistingData(PositionsManager mgr)
		{
			try
			{
				PositionDefinition def = PositionDefinition.CreateFrom(mgr);
				definition = def;
				manager = mgr.Clone();
				sourceManager = manager.Clone();
				comboBoxNumberOfSegments.SelectedItem = def.NumberOfSegments;
				treeController.SetNewDataSource(manager);
				comboBoxNumberOfSegments.Enabled = false;
				checkBoxLastCharSegment.Enabled = false;
				InitializeDefinitionData();
			}
			catch
			{
				return;
			}
		}
		private void InitializeDefinitionData()
		{
			// If char segment is not used, skip it.
			int charSegmentFix = definition.UseLastCharSegment ? 0 : 1;
			// One array goes from 0, other from the end.
			var segmentDefs = definition.Segments.Reverse().ToArray();
			for (int i = definition.NumberOfSegments - 1; i >= 0; i--)
			{
				var defData = segmentDefs[i];
				var viewData = AllSegmentControls[i + charSegmentFix];
				viewData.Start.Text = defData.StartValue;
				var seg = defData as SegmentDefinitionNumber;
				if (seg != null)
					viewData.Increment.Text = seg.Increment.ToString();
				else
				{
					// If char segment.
					bool? b = (defData as SegmentDefinitionChar)?.Incremented;
					if (b.HasValue)
						checkBoxIncrementChar.Checked = b.Value;
				}
			}
		}
		/// <summary>
		/// Resets data to initial state. If clearComplete, all data is deleted.
		/// </summary>
		/// <param name="mgr"></param>
		private void Reset(bool clearComplete = false)
		{
			bool freshStart = sourceManager == null || sourceManager != null && clearComplete;
			if (sourceManager != null && !clearComplete)
			{
				manager = sourceManager.Clone();
			}
			else
			{
				definition = new PositionDefinition();
				manager = new PositionsManager();
				comboBoxNumberOfSegments.SelectedIndex = 0;
				comboBoxNumberOfSegments.Enabled = true;
				checkBoxLastCharSegment.Enabled = true;
				NumberOfSegmentsChanged();
			}
			treeController.SetNewDataSource(manager);
			InitializeDefinitionData();
		}

		private void TextBoxStart_KeyPress(object sender, KeyPressEventArgs e)
		{
			TextBox box = (TextBox)sender;
			if (box == textBoxStart10)
			{
				// Allow only a-zA-Z0-9
				if ((char)33 <= e.KeyChar && e.KeyChar <= 126)
				{
					// Value is ok.
				}
				else
					e.Handled = true;
			}
			else
			{
				if ('0' <= e.KeyChar && e.KeyChar <= '9')
				{

				}
				else
					e.Handled = true;
			}
		}

		private void TextBoxInc_TextChanged(object sender, EventArgs e)
		{
			TextBox tb = (TextBox)sender;
			SegmentDefinitionNumber n = tb.Tag as SegmentDefinitionNumber;
			int i;
			errorProvider.SetError(tb, null);
			if (!int.TryParse(tb.Text, out i))
			{
				errorProvider.SetError(tb, "Increment is not a value!");
				return;
			}
			if (i > n.MaxNumber)
			{
				errorProvider.SetError(tb, $"Increment is bigger than max: {n.MaxNumber} !");
				return;
			}
			if (n != null)
				n.Increment = i;
			UpdateNextNewPositionPreview();
		}

		private void TextBoxStart_TextChanged(object sender, EventArgs e)
		{
			TextBox tb = (TextBox)sender;
			SegmentDefinitionNumber n = tb.Tag as SegmentDefinitionNumber;
			errorProvider.SetError(tb, null);
			if (n != null)
			{
				int i;
				if (!int.TryParse(tb.Text, out i))
				{
					errorProvider.SetError(tb, "Start is not a value!");
					return;
				}
				n.StartValue = tb.Text;

			}
			else
			{
				SegmentDefinitionChar c = tb.Tag as SegmentDefinitionChar;
				if (c != null)
					c.StartValue = string.IsNullOrEmpty(tb.Text)?" ":tb.Text.Substring(0, 1);
			}
			UpdateNextNewPositionPreview();
		}

		private void ButtonDelete_Click(object sender, EventArgs e)
		{
			if (treeController.SelectedSegment == null)
			{
				MessageBox.Show("Position not selected.");
				return;
			}
			manager.DeleteSegment(treeController.SelectedSegment);
			treeController.DataStructureUpdated();

			if (!manager.FirstSegmentsCollection.Any())
			{
				comboBoxNumberOfSegments.Enabled = true;
				checkBoxLastCharSegment.Enabled = true;
			}
		}

		private void ButtonCreatePosition_Click(object sender, EventArgs e)
		{
			Segment seg = null;
			SegmentData current = treeController.SelectedSegment as SegmentData;
			if (treeController.SelectedSegment == null || treeController.SelectedSegment.Parent == null)
			{
				seg = manager.CreateNewSegment(definition);
			}
			else
				seg = treeController.SelectedSegment.Parent.CreateNewChild(definition);
			if (current != null)
			{
				var sd = seg as SegmentData;
				if (sd != null)
				{
					sd.UnitOfMeasurement = current.UnitOfMeasurement;
					sd.VatPercent = current.VatPercent;
				}
			}

			treeController.DataStructureUpdated();

			// Set newly created Segment as selected.
			treeController.SelectedSegment = seg;

			comboBoxNumberOfSegments.Enabled = false;
			checkBoxLastCharSegment.Enabled = false;			
		}
		

		private void NumberOfSegmentsChanged()
		{
			if (comboBoxNumberOfSegments.SelectedItem == null)
				return;
			int n = (int)comboBoxNumberOfSegments.SelectedItem;// int.Parse((string)comboBoxNumberOfSegments.SelectedItem);
			bool useChar = checkBoxLastCharSegment.Checked;

			definition.SetNumberOfSegments(n, useChar);

			int currentSegment = n;
			for (int i = 0; i <TotalSegments; i++)
			{
				var seg = AllSegmentControls[i];
				if (i == 0)
				{
					if (!useChar)
					{
						seg.Label.Text = "";
						seg.Start.Enabled = false;
						checkBoxIncrementChar.Enabled = false;
					}
					else
					{
						var sg = definition.Segments[currentSegment - 1];
						seg.Label.Text = currentSegment.ToString();
						seg.Start.Text = sg.StartValue;
						seg.Start.Enabled = true;
						checkBoxIncrementChar.Enabled = true;
						seg.Start.Tag = sg;
						currentSegment--;
					}
				}
				else
				{
					if (currentSegment >0)
					{
						SegmentDefinitionNumber sg = (SegmentDefinitionNumber)definition.Segments[currentSegment - 1];
						seg.Label.Text = currentSegment.ToString();
						seg.Start.Enabled = true;
						seg.Increment.Enabled = true;
						seg.Start.Text = sg.StartValue;
						seg.Increment.Text = sg.Increment.ToString();
						seg.Start.Tag = sg;
						seg.Increment.Tag = sg;
						currentSegment--;
					}
					else
					{
						seg.Label.Text = "";
						seg.Start.Enabled = false;
						seg.Increment.Enabled = false;
					}
				}
			}
			textBoxStartPreview.Text = definition.GetIdWithStartValuesFromSegment();
			UpdateNextNewPositionPreview();
			treeController.SetNumberOfSegmentColumns(n);
		}
		private void UpdateStartPositionPreview()
		{
			textBoxStartPreview.Text = definition.GetIdWithStartValuesFromSegment();
		}
		private void UpdateNextNewPositionPreview()
		{
			string s = "";
			if (!manager.FirstSegmentsCollection.Any())
			{
				/*for (int i = 0; i < definition.NumberOfSegments; i++)
				{
					if (i > 0)
						s += Segment.SegmentSeparator;
					s += definition.Segments[i].StartValue;
				}*/
				s = definition.GetIdWithStartValuesFromSegment();
			}
			else
			{
				if (treeController.SelectedSegment == null || treeController.SelectedSegment.Parent == null)
				{
					s = definition.Segments[0].GetNextValue(manager.FirstSegmentsCollection.Last().Id);
					//for (int i = 1; i < definition.NumberOfSegments; i++)
					//{
						s += Segment.SegmentSeparator + definition.GetIdWithStartValuesFromSegment(1);// definition.Segments[i].StartValue;
					//}
				}
				else
				{
					var seg = treeController.SelectedSegment;
					s = "";
						s = seg.Parent.GetIdPrefix();
					s += Segment.SegmentSeparator;
					s += definition.Segments[seg.LevelIndex].GetNextValue(seg.Parent.Children.Last().Id);
					if (seg.LevelIndex + 1 < definition.NumberOfSegments)
					{
						s += Segment.SegmentSeparator;
						s += definition.GetIdWithStartValuesFromSegment(seg.LevelIndex + 1);
					}

						//for (int i = seg.LevelIndex; i < definition.NumberOfSegments; i++)
						//{
						//	s += Segment.SegmentSeparator;
						//	s += definition.Segments[i].GetNextValue(seg.Parent.Children.Last().Id);
						//}
				}
			}
			labelNextPositionPreview.Text = "Create: " + s;
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
			Close();
		}

		private void buttonClear_Click(object sender, EventArgs e)
		{
			Reset(true);
		}
	}
}
