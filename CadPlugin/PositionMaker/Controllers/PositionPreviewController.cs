﻿using CadPlugin.PositionMaker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.PositionMaker.Controllers
{
	class PositionPreviewController
	{
		/// <summary>
		/// Creates preview from definitions. count represents number of preview values.
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="count"></param>
		/// <param name="seg1"></param>
		/// <param name="seg2"></param>
		/// <param name="seg3"></param>
		/// <param name="seg4"></param>
		/// <returns></returns>
		public bool CreatePreviewNames(PositionPointDefinition pos, int count, ref string[] seg1, ref string[] seg2, ref string[] seg3, ref string[] seg4)
		{
			int segment1 = 1;
			int segment2 = 2;
			int segment3 = 120;
			char segment4 = 'a';


			List<string> p = new List<string>();
			p.Add(pos.GetPositionName(segment1, segment2, segment3, segment4));
			p.Add(pos.GetPositionName(segment1, segment2, segment3 + pos.Segment3.Increment, segment4));
			p.Add("...");
			p.Add(pos.GetPositionName(segment1, segment2 + pos.Segment2.Increment, segment3, segment4));
			p.Add("...");
			p.Add(pos.GetPositionName(segment1 + pos.Segment1.Increment, segment2, segment3, segment4));
			p.Add(pos.GetPositionName(segment1 + pos.Segment1.Increment, segment2, segment3 + pos.Segment3.Increment, segment4));



			//return p;
			throw new Exception();
		}
	}
}
