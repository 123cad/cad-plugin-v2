﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin
{
	/// <summary>
	/// Used by activation.
	/// </summary>
	class Version
	{
		private static int __Major = 3;
		private static int __Minor = 8;
		public static int Major {
			get
			{
				return __Major;
			}
			set
			{
				__Major = value;
				UpdateNumber();
			}
		}
		public static int Minor
		{
			get
			{
				return __Minor;
			}
			set
			{
				__Minor = value;
				UpdateNumber();
			}
		}

		/// <summary>
		/// Version returned as 2 digits (37 for 3.7).
		/// </summary>
		public static int Number = 0;

		static Version()
		{
			AssemblyName name = System.Reflection.Assembly.GetExecutingAssembly().GetName();
			Major = name.Version.Major;
			Minor = name.Version.Minor;
		}
		private static void UpdateNumber()
		{
			Number = Major * 10 + Minor;
		}
		public static new string ToString()
		{
			return "123CAD " + Major + "." + Minor;
		}
	}
}
