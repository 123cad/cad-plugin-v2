﻿Libraries are conditionally referenced from "Common Debug"/"Common Release" using 
Condition=" $(DefineConstants.Contains('DEBUG')) "
and 
Condition=" !$(DefineConstants.Contains('DEBUG')) " .
This is done so appropriate dll is used during development. 
Any constant (compilation symbol) can be used:
Condition=" $(DefineConstants.Contains('DEBUG')) And $(DefineConstants.Contains('AUTOCAD')) " .

Also, this can be done with global macros:
Condition=" '$(ConfigurationName)' == 'Debug' ".

******************
Also, when loading assembly, file name is not important at all. Assembly signature is. If Assembly.LoadFrom 
loads file "file1.dll" with assembly with name "test1" probed will be the file "test1.dll".
Therefore, in project file in every single property group redefine
<AssemblyName>Autocad</AssemblyName>


------------------------------------
Pre-build events:
Copies all files from destination folder (Debug/Release) to Libraries directory, from where dlls are loaded.
In this way, Debug and Release versions are using same dll loading process.
(not sure, but there is limitation about from where LoadFrom can occur - maybe only current folder or subfolders is allowed => i think this is only for probing and setting it up in config file)

------------------------------------
Post-build events:
Copy compiled library to Common Debug/Release folder.

