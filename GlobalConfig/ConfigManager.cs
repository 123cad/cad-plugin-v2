﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GlobalConfig
{
	/// <summary>
	/// Works with global config file (save/load, create, etc...)
	/// Every module can have its own section in config file, and it requires that part from this module.
	/// </summary>
	public class ConfigManager
	{
		// Initially load file (if it exists. if not...)
		private static ConfigManager __Instance;
		public static ConfigManager Instance
		{
			get
			{
				if (__Instance == null)
					__Instance = new ConfigManager(); 
				return __Instance;
			}
		}
		private string configFileName;


		/// <summary>
		/// Name of log section in config file.
		/// </summary>
		public string LogConfigSection { get; private set; }

		private ConfigManager()
		{
			using (RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\123cad"))
			{
				if (key != null)
				{
					string path = key.GetValue("LocationDir") as string;
					if (path != null)
					{
						string filePath = path + "config.xml";
						if (File.Exists(filePath))
						{
							configFileName = filePath;
						}
					}

				}

				LogConfigSection = "logs";
			}
		}

		/// <summary>
		/// Returns section in main config file.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public XmlNode GetSection(string sectionName)
		{
			if (!File.Exists(configFileName))
				return null;
			XmlDocument doc = new XmlDocument();

			doc.Load(configFileName);
			
			XmlElement node = doc["CAD"];
			if (node == null)
				return null;
			node = node[sectionName];
			
			return node;
		}
		

	}
}
