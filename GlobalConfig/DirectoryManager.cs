﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GlobalConfig
{
	/// <summary>
	/// Manages document directories for 123CAD.
	/// </summary>
	public class DirectoryManager
	{

		/// <summary>
		/// Full directory path of 123CAD documents root (with end \).
		/// </summary>
		public string DocumentsDir { get; private set; }
		/// <summary>
		/// Full directory path of installation folder (with end \).
		/// </summary>
		public string InstallationDir { get; }
		/// <summary>
		/// Full path of directory where SewageImages are stored (with end \).
		/// </summary>
		public string InstallationSewageImagesDir { get; }
		/// <summary>
		/// Predefined directory for log files (with end \).
		/// </summary>
		public string DocumentsLogDir { get; private set; }
		/// <summary>
		/// Predefined directory for ASCII files (with end \).
		/// </summary>
		public string DocumentsAsciiDir { get; private set; }
		/// <summary>
		/// Predefined directory for Profiler (with end \).
		/// </summary>
		public string DocumentsProfilerDir { get; private set; }
        public string DocumentsSewageDir { get; private set; }
		private static DirectoryManager __Instance;
		public static DirectoryManager Instance
		{
			get
			{
				if (__Instance == null)
					__Instance = new DirectoryManager();
				return __Instance;
			}
		}

		private DirectoryManager()
		{
			// If documents directory is not found, create it in default location.
			DocumentsDir = null;
			using (RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\123cad", true))
			{
				if (key != null)
				{
					string s = key.GetValue("DocumentsDir") as string;
					if (!string.IsNullOrEmpty(s))
						DocumentsDir = s;
				}
				else
					throw new ApplicationException("123cad is not installed! No registry entry is found!");
				string location = key.GetValue("LocationDir") as string;
				InstallationDir = location;
				InstallationSewageImagesDir = location + "Support\\SewageIcons\\";

				if (DocumentsDir == null)
					DocumentsDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\123CAD Documents\\";

				if (DocumentsDir.Last() != '\\')
					DocumentsDir += '\\';


				// Create 123CAD directories
				DocumentsLogDir = DocumentsDir + "Logs\\";
				DocumentsAsciiDir = DocumentsDir + "ASCII\\";
				DocumentsProfilerDir = DocumentsDir + "Profiler\\";
                DocumentsSewageDir = DocumentsDir + "Sewage\\";

				CreateDirectoriesIfNeeded(key);
			}
		}

		private void CreateDirectoriesIfNeeded(RegistryKey key)
		{
			string error = "";
			try
			{
				if (!Directory.Exists(DocumentsDir))
				{
					Directory.CreateDirectory(DocumentsDir);
					key.SetValue("DocumentsDir", DocumentsDir);
				}
			}
			catch (Exception e)
			{
				error += "Documents directory can't be created! " + e.Message + "\n";
			}
			try
			{
				if (!Directory.Exists(DocumentsLogDir))
					Directory.CreateDirectory(DocumentsLogDir);
			}
			catch (Exception e)
			{
				error += "Log directory can't be created! " + e.Message + "\n";
			}
			try
			{
				if (!Directory.Exists(DocumentsAsciiDir))
					Directory.CreateDirectory(DocumentsAsciiDir);
			}
			catch (Exception e)
			{
				error += "ASCII directory can't be created! " + e.Message + "\n";
            }
            try
            {
                if (!Directory.Exists(DocumentsProfilerDir))
                    Directory.CreateDirectory(DocumentsProfilerDir);
            }
            catch (Exception e)
            {
                error += "Profile directory can't be created! " + e.Message + "\n";
            }
            try
            {
                if (!Directory.Exists(DocumentsSewageDir))
                    Directory.CreateDirectory(DocumentsSewageDir);
            }
            catch (Exception e)
            {
                error += "Sewage directory can't be created! " + e.Message + "\n";
            }
            if (error != "")
			{
				MessageBox.Show(error, "Problem with creating documents directory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}
	}
}
