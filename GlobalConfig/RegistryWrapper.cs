﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalConfig
{

    public class C123CADRegistryPaths
    {
        /// <summary>
        /// Relative path of 123CAD in HKCU.
        /// </summary>
        public const string ApplicationHKCUPath = "Software\\123cad\\";

        /// <summary>
        /// Name of value which holds location of 123cad installation.
        /// </summary>
        public const string ValueLocationDir = "LocationDir";
        public const string ValueHelpFileLocation = "Help.chm";
        public const string ValueSupportDir = "v3.7.0.0.Support.Directory";

        /// <summary>
        /// Relative path to Applicatio nHKCUPath.
        /// </summary>
        public const string SubpathSewage = "Sewage\\";
        /// <summary>
        /// Relative path to Application HKCUPath.
        /// </summary>
        public const string SubpathSewageShafts = "Sewage\\Shafts\\";
        /// <summary>
        /// Relative path to Application HKCUPath.
        /// </summary>
        public const string SubpathAttributes = "Attributes\\";
    }
    /// <summary>
    /// Manager for accessing registry to 123CAD key.
    /// </summary>
    public class RegistryWrapper
    {
        /// <summary>
        /// Full path under HKCU.
        /// </summary>
        public string Path { get; private set; }
        /// <summary>
        /// HKCU/HKLM...
        /// </summary>
        public RegistryHive Hive { get; }
        /// <summary>
        /// win32/x64
        /// </summary>
        public RegistryView View { get; }

        public RegistryWrapper (RegistryHive hive = RegistryHive.CurrentUser, RegistryView view = RegistryView.Registry64)
        {
            Hive = hive;
            View = view;
        }

        /// <summary>
        /// Create if not found. 
        /// Dispose when finished.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public RegistryKey OpenSubkey(string path)
        {
            var key = RegistryKey.OpenBaseKey(Hive, View);
            return key.OpenSubKey(path);
        }

        public string GetValue(string path, string valueKey)
        {
            var key = RegistryKey.OpenBaseKey(Hive, View);
            var v = key.OpenSubKey(path);
            if (v == null)
                return null;
            using (v)
            {
                return v.GetValue(valueKey) as string;
            }
        }
        public void SetValue(string path, string valueKey, string value)
        {
            var key = RegistryKey.OpenBaseKey(Hive, View);
            using (var v = key.CreateSubKey(path))
            {
                try
                {
                    v.SetValue(valueKey, value);
                }
                catch (System.Security.SecurityException se)
                {
                    // Maybe log to file?
                    Debug.Fail("RegistryWrapper.SetValue faile! " + se.Message);
                }
            }
        }

        /// <summary>
        /// Returns values as pairs in dictionary.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="keyValuePairs"></param>
        public void GetValues(string path, Dictionary<string, string> keyValuePairs)
        {

            var key = RegistryKey.OpenBaseKey(Hive, View);
            var v = key.OpenSubKey(path);
            using (v)
            {
                foreach (var pair in keyValuePairs)
                    keyValuePairs[pair.Key] = v.GetValue(pair.Key) as string;
            }
        }
        /// <summary>
        /// Sets key/value from dictionary to registry.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="keyValuePairs"></param>
        public void SetValues(string path, Dictionary<string, string> keyValuePairs)
        {

            var key = RegistryKey.OpenBaseKey(Hive, View);
            var v = key.CreateSubKey(path);
            using (v)
            {
                foreach (var pair in keyValuePairs)
                    v.SetValue(pair.Key, pair.Value);
            }
        }

    }
}
