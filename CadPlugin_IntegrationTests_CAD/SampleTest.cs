﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin_IntegrationTests_CAD
{
    // Sample test - NUnit needs at least 1 test, otherwise reports an error.
    //               Put here some tips related to tests.

    // CADBloke from github says that is STA is not set, exception is thrown.
    [TestFixture, Apartment(System.Threading.ApartmentState.STA)]
    public class SampleTest
    {
        [Test]
        public void TestMethod()
        {
            //Assert.Pass();
        }
    }
}
