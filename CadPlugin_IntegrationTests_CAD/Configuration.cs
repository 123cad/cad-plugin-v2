﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin_IntegrationTests_CAD
{
    static class Configuration
    {
        #region Loaded settings
        public static bool RunSingleGroup { get; set; }
        /// <summary>
        /// Should CAD be closed automatically.
        /// </summary>
        public static bool Autoclose { get; set; } 

        public static bool OutputNod { get; set; }

        public static string NUnitResultsFileName { get; set; } 

        public static string NUnitLogLevel { get; set; }

        public static string NUnitConsoleOutputFileName { get; set; } 

        /// <summary>
        /// Wait for user input at the end (works only on console).
        /// </summary>
        public static bool NUnitWaitEnd { get; set; }

        public static string OutputHtmlFileName { get; set; } 
        #endregion

        public static string WorkingDirectory { get; set; }
        public static string NUnitResultsFullFilePath { get; set; }

        static Configuration()
        {
            WorkingDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Initialize();

            try
            {
                string configDir = Directory.GetParent(WorkingDirectory).FullName;
                configDir = Directory.GetParent(configDir).FullName;
                string configFileName = "configuration.json";

                string content = File.ReadAllText(configDir + "\\" + configFileName);
                JObject o = JObject.Parse(content);
                if (o.ContainsKey("RunSingleGroup"))
                    RunSingleGroup = o["RunSingleGroup"].Value<bool>();
                if (o.ContainsKey("Autoclose"))
                    Autoclose = o["Autoclose"].Value<bool>();
                if (o.ContainsKey("OutputNOD"))
                    OutputNod = o["OutputNOD"].Value<bool>() && RunSingleGroup;
                if (!RunSingleGroup)
                { 
                    Autoclose = true;
                    OutputNod = false;
                }
                if (o.ContainsKey("NUnit"))
                {
                    var v = (JObject)o["NUnit"];
                    if (v.ContainsKey("ResultsFileName"))
                        NUnitResultsFileName = v["ResultsFileName"].Value<string>();
                    if (v.ContainsKey("LogLevel"))
                        NUnitLogLevel = v["LogLevel"].Value<string>();
                    if (v.ContainsKey("Wait"))
                        NUnitWaitEnd = v["Wait"].Value<bool>();
                    if (v.ContainsKey("ConsoleRedirect"))
                        NUnitConsoleOutputFileName = v["ConsoleRedirect"].Value<string>();
                }
                if (o.ContainsKey("OutputHtmlFileName"))
                    OutputHtmlFileName = o["OutputHtmlFileName"].Value<string>();
            }
            catch(System.Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Erro occured while loading Configuration. Using default values...\n" + e);
                Initialize();
            }

            NUnitResultsFullFilePath = $"{WorkingDirectory}\\{NUnitResultsFileName}";
            // Overwrite.
            File.WriteAllText(NUnitResultsFullFilePath, "");

        }
        private static void Initialize()
        {
            RunSingleGroup = false;
            Autoclose = true;
            NUnitResultsFileName = "NUnit result - default.xml";
            NUnitLogLevel = "Off";
            NUnitConsoleOutputFileName = "output.txt";
            NUnitWaitEnd = false;
            OutputHtmlFileName = "NUnit html generated.html";
        }

        public static IEnumerable<string> GetNUnitArgs()
        {
            // No start spaces for arg - NUnit will report an error.
            List<string> nUnitArgs = new List<string>
            {
                "--trace=" + NUnitLogLevel,
                $"--result={NUnitResultsFullFilePath}",
                "/out=" + NUnitConsoleOutputFileName
            };
            if (NUnitWaitEnd)
                nUnitArgs.Add("--wait");
            if (RunSingleGroup)
            {
                nUnitArgs.Add("--where=cat==" + TestHelper.RunGroupName);
            }
            return nUnitArgs;
        }
    }
}
