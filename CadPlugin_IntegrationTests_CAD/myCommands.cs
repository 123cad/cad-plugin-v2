﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnitLite;
using System.IO;
using System.Threading;
using System.Reflection;
using System.Diagnostics;
using NUnit.Framework;
using CadPlugin;
using NetworkKS.Networks;
using System.Windows.Threading;
using NetworkKS.Isybau;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif


// This line is not mandatory, but improves loading performances
[assembly: CommandClass(typeof(CadPlugin_IntegrationTests_CAD.myCommands))]


namespace CadPlugin_IntegrationTests_CAD
{
    public static class TestHelper
    {

        /*
        [Category(TestHelper.RunGroupName)]
        */
        /// <summary>
        /// Find this attribute to see which tests are run when RunSingleGroup is true.
        /// </summary>
        public const string RunGroupName = "ONLYME";
        
        private static Dispatcher mainThread;

        public static void Initialize()
        {
            mainThread = Dispatcher.CurrentDispatcher;
        }
        public static void RunOnUIThread(Action a)
        {
            mainThread.Invoke(a);
        }
        public static T RunOnUIThread<T>(Func<T> f) where T : class
        {
            T res = null;
            Action a = () => res = f();
            mainThread.Invoke(a);
            return res;
        }
    }
    public class myCommands
    {
        [CommandMethod("RunNUnitIntegrationTests", CommandFlags.Session), STAThread]
        
        public void RunTests()
        {
            TestHelper.Initialize();
            IEnumerable<string> nUnitArgs = Configuration.GetNUnitArgs();

            Stopwatch sw = Stopwatch.StartNew();
            // - Returns count of failed tests.
            // -4 = no tests found
            int testReturnCode = -11111;
            Thread thread = new Thread(new ThreadStart(() =>
            {
                testReturnCode = new AutoRun().Execute(nUnitArgs.ToArray());
                sw.Stop();

                var doc = Application.DocumentManager.MdiActiveDocument;
                var ed = doc.Editor;

                if (Configuration.RunSingleGroup)
                    doc.Editor.ZoomExtents(); ;

                if (!File.Exists(Configuration.NUnitResultsFullFilePath))
                    throw new FileNotFoundException("NUnitLite Results not created!");

                TestRunner runner = TestRunner.Bricscad;
                string msg = $"Tests have been ran with ";
#if BRICSCAD
                runner = TestRunner.Bricscad;
                msg += "BRICSCAD";
#endif
#if AUTOCAD
                runner = TestRunner.Autocad;
                msg += "AUTOCAD";
#if ACADCORECONSOLE
                runner = TestRunner.AutocadConsole;
                msg += " CORE";
#endif
#endif
                TestResultsData resData = new TestResultsData()
                {
                    ElapsedMilisecond = sw.ElapsedMilliseconds,
                    Runner = runner,
                    ErrorCode = testReturnCode
                };
                if (testReturnCode == 0)
                    resData.Result = TestResult.Passed;
                else if (testReturnCode > 0)
                    resData.Result = TestResult.Failed;
                else
                    resData.Result = TestResult.OtherError;
                TestResultsBox.ShowDialog(resData);

                Task task = Task.Factory.StartNew(() =>
                {
                    string resultHtml = CreateHtmlReport();
                    return resultHtml;
                })
                .ContinueWith(t =>
                {
                    Process.Start(t.Result);
                });

                //string resultHtml = CreateHtmlReport();
                //Process.Start(resultHtml);

                if (Configuration.OutputNod)
                    CadPlugin.Common.NODExport.ExportNODToFile(doc);

                task.Wait();
                if (Configuration.Autoclose)
                {
#if BRICSCAD
                    doc.CloseAndDiscard();
#endif
                    Application.Quit();
#if BRICSCAD
                    // Because Window remains which asks to save drawing.
                    Process.GetCurrentProcess().Kill();
#endif
                }
            }));
            thread.Start();
            /* Check result of Execute method (it seems it says number of failing tests) - we can use this 
             * Add result converter
             * Test difference between AutocadConsole and full Autocad (unittests we should run in console, because we don't need visual results)
             * OK Add Bricscad support
             * OK Check if question is needed at the end (like should results file be opened), or just close test and open results (or open results if there are failed tests)
             * OK Show this in message box (like all tests passed), or number of failed test
             * OK Add to TrustedLocation so .dll is loaded to Autocad without prompt
             * OK Check everything with Project runners (acad/bcad)
             * OK write some sample tests and run them
             */
        }
        /// <summary>
        /// Creates html file from nunit results file and returns full path.
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        private string CreateHtmlReport()
        {
            string binPath = Configuration.WorkingDirectory;// Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string outputHtml = Configuration.WorkingDirectory + "/" + Configuration.OutputHtmlFileName;
            // Overwrite.
            File.WriteAllText(outputHtml, "");
            binPath = Directory.GetParent(binPath).FullName;
            string generator = binPath + "/ReportUnit.exe";

            using (Process process = new Process())
            {
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.CreateNoWindow = true;

                process.StartInfo.FileName = generator;

                string location = @"C:\Users\Dev\Documents\123CAD\123CAD Projects\Production\Publish\CAD-plugin V2\";
                string args = "" +
                    $" \"{Configuration.NUnitResultsFullFilePath}\"" +
                    $" \"{outputHtml}\"" + 
                    $" \"{location}\""
                    ;
                process.StartInfo.Arguments = args;

                process.Start();

                process.WaitForExit();
            }
            return outputHtml;
        }
    }
}
