﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin_IntegrationTests_CAD
{
    public enum TestRunner
    {
        Bricscad,
        Autocad,
        AutocadConsole
    }
    public enum TestResult
    {
        /// <summary>
        /// All tests passed.
        /// </summary>
        Passed,
        /// <summary>
        /// 1 or more tests failed.
        /// </summary>
        Failed,
        /// <summary>
        /// Test framework error.
        /// </summary>
        OtherError
    }
    public class TestResultsData
    {
        public TestRunner Runner { get; set; }
        public long ElapsedMilisecond { get; set; }
        public TestResult Result { get; set; }
        public int ErrorCode { get; set; }
    }
}
