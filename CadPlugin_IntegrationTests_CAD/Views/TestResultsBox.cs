﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin_IntegrationTests_CAD
{
    public partial class TestResultsBox : Form
    {
        public TestResultsBox(TestResultsData data)
        {
            InitializeComponent();
            Initialize(data);
        }
        internal static void ShowDialog(TestResultsData data )
        {
            TestResultsBox box = new TestResultsBox(data);
            box.ShowDialog();            
        }
        private void Initialize(TestResultsData data)
        {
            string program = "";
            switch (data.Runner)
            {
                case TestRunner.Bricscad:
                    program = "BRICSCAD";
                    break;
                case TestRunner.Autocad:
                    program = "AUTOCAD";
                    break;
                case TestRunner.AutocadConsole:
                    program = "AUTOCAD CORE";
                    break;
            } 
            labelCADProgram.Text = program;

            labelTime.Text = data.ElapsedMilisecond + "ms";

            switch (data.Result)
            {
                case TestResult.Passed:
                    pictureBox.BackgroundImage = Properties.Resource.Passed;
                    labelTestResults.ForeColor = Color.Green;
                    labelTestResults.Text = "PASSED";
                    break;
                case TestResult.Failed:
                    pictureBox.BackgroundImage = Properties.Resource.Failed;
                    labelTestResults.ForeColor = Color.DarkRed;
                    labelTestResults.Text = $"FAILED ({data.ErrorCode} tests)";
                    break;
                case TestResult.OtherError:
                    pictureBox.BackgroundImage = Properties.Resource.Error;
                    labelTestResults.ForeColor = Color.Blue;
                    labelTestResults.Text = $"Other error ({data.ErrorCode})";
                    break;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
