﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin_IntegrationTests_CAD.Miscelaneous
{
    [TestFixture]
    class PolylineTests
    {
        [Test]
        public void SplitTest()
        {
            var doc = Application.DocumentManager.MdiActiveDocument;
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                Polyline pl = tw.CreateEntity<Polyline>();
                pl.AddVertexAt(0, new Point2d(), 0, 0, 0);
                pl.AddVertexAt(1, new Point2d(3, 0), 1, 0, 0);
                pl.AddVertexAt(2, new Point2d(3, 2), 0, 0, 0);

                Point3d[] points = new Point3d[]
                {
                    //pl.GetPoint3dAt(0),
                    //pl.GetPoint3dAt(1),
                    pl.GetPointAtDist(pl.GetDistanceAtParameter(1.5)),
                    //pl.GetPoint3dAt(2)
                };
                Point3dCollection cl = new Point3dCollection(points);
                var objects = pl.GetSplitCurves(cl);

                foreach(DBObject obj in objects)
                {
                    DBObject o = obj;
                    tw.MS.AppendEntity(o as Entity);
                    tw.Tr.AddNewlyCreatedDBObject(o, true);
                }

                pl.TransformBy(Matrix3d.Displacement(new Vector3d(0, -1, 0)));

                tw.Commit();
            }

        }
    }
}
