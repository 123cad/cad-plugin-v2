﻿Unit testing of CAD related API, from inside Acad/Bcad using NUnitLite.

Running NUnitLite:
- Acad 2015 - test results are displayed in separate Console
- Acad 2015 console - test results are NOT displayed in console
- Bcad - test results are NOT displayed in console.
* Explored a lot about problems with NUnit console output, and no solution exists

*    This project is not meant to explore data in the drawing (manual testing). It only runs short Unit tests and shows results.
**   Autocad must to have Trusted locations set to .dll folder (Debug/Release), otherwise acadconsole will not load .dll (and no error message appears). Running this in regular Autocad
     causes Load file dialog to be opened.

							- MANUAL - 
						- Running tests - 

This project is cad plugin (not classic unit test project) - load it to Autocad/Bricscad and run command "RunNUnitTests". It will run Unit tests from this project and show results.
Automatization:
.scr file is used to automatically execute commands (netload - for loading this .dll and "RunUnitTests" to execute tests).
This file is passed as argument to Acad/Bcad.

Next to existing configurations, this project has additional one: DebugAutocadConsole. We can choose if we wan't to run test in full Autocad or just in console, but we have 
manually to set this in "Configuration Manager" (DebugAutocad solution configuration to use DebugAutocad/DebugAutocadConsole).

---------------------------------------------------------

To run tests as non debug from Tools menu:
- Set desired SolutionConfiguration (remember that Autocad can have DebugAutocad and DebugAutocadConsole!)
- Compile projects 
- Run tests (for selected configuration!!!) as debug (from Solution explorer) or from Tools menu
- (or set current project to be test project, and then run as debug/non-debug)

*   This project is not included in Batch Build, and therefore must be compiled against current configuration!
**  When project is not selected it can be run only as Debug, but in Tools menu there are 3 options to run it as non Debug (these are only shortcuts which are
	identical to Debug tab in project settings).
	Keep in mind few important things: 
										- Project must be manually compiled before running tests
										- Batch Build doens't compile tests 
										- Project is compiled only against current configuration (DebugAutocad/DebugBricscad ...)
										- For Autocad project is compiled only against selected configuration (console or non-console)
										- DLL file to load is defined in .scr file - currently Debug version is loaded, Release tests are not run
										  (in short, run tests only with Debug)



						- Showing results - 

NUnitLite outputs xml file which is further processed to .html using ReportUnit (required .exe is in "bin" folder). 
File is opened in browser only if test results fail.
Both .xml and .html file are created in Configuration directory.

(ExtentsReport exists as well, but there is no filter in the page, i don't like it. And it is successor of ReportUnit)
(I plan to create custom html)
 