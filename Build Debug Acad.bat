REM Use BuildRun.bat to execute batch script directly.
cd "%~dp0"
REM clear the log file.
set logfile="Log Build Debug Acad.txt"
echo  > %logfile%
devenv "cad-plugin.sln" /rebuild "DebugAutocad|Any CPU" /out %logfile%
