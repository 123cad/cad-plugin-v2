﻿using CadPlugin;
using NetworkKS.Views.ConnectionPositionsViews;
using NUnit.Framework;
using SharedUtilities.GlobalDependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[SetUpFixture]
class MyGlobalSetup
{
    [OneTimeSetUp]
    public void Setup()
    {
        // Needed because we use CAD to calculate arc length, but in non CAD code.
        // So any test which requires arc calculation
        //CadPlugin.KSopt.Networks.PipeCalculationHelperManager.Calculator = new CadPlugin.KSopt.CADModels.PipeCalculationHelper();

        
        GlobalDI.Kernel.Rebind<ConnectionPositionController>().To<ConnectionPositionControllerNull>();
    }
}