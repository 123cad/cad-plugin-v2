﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin_RunTester_CAD.Miscelaneous
{
    //[TestFixture]
    class DatabaseTests
    {
        //[Test]
        public void EraseAndRestore_Test()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            ObjectId ptId = ObjectId.Null;
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                DBPoint p = tw.CreateEntity<DBPoint>();
                ptId = p.ObjectId;
                tw.Commit();
            }
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                tw.GetObjectWrite<DBObject>(ptId).Erase();
                tw.Commit();
            }
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                DBObject obj = tw.Tr.GetObject(ptId, OpenMode.ForWrite, true);
                Assert.IsTrue(obj.IsErased);
                obj.Unerase();
                Assert.IsFalse(obj.IsErased);
                tw.Commit();
            }
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                DBObject pt = null;
                Assert.DoesNotThrow(()=> pt= tw.GetObjectRead<DBObject>(ptId));
                Assert.NotNull(pt);
                tw.Commit();
            }
        }
        //[Test]
        public void OpenErasedObject_Test()
        {
#if BRICSCAD 
            //some methods not found by Autocad, leaving for now.
            //NOTE This test causes Bricscad process not to be closed at the end (even after Application.Quit).
            Document doc = Application.DocumentManager.MdiActiveDocument;
            ObjectId ptId = ObjectId.Null;
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                DBPoint p = tw.CreateEntity<DBPoint>();
                ptId = p.ObjectId;
                p.Erase();
                tw.Commit();
            }

            string filename = "unittest.dwg";
            doc.CloseAndSave(filename);
            doc = Application.DocumentManager.Open(filename);
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                DBPoint p = null;
                Assert.Throws<Exception>(()=> tw.GetObjectRead<DBObject>(ptId));
                tw.Commit();
            }
            doc.CloseAndDiscard();
#endif
        }
        //[Test]
        public void OpenNonExistingObject_Test()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            ObjectId ptId = ObjectId.Null;
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                DBPoint p = new DBPoint();
                tw.MS.UpgradeOpen();
                tw.MS.AppendEntity(p);
                ptId = p.ObjectId;
                p.Dispose();
                tw.Abort(); 
            }
            //string fileName = "unittest db.dwg";
            //Database db = new Database(true, true);
            //db.SaveAs(fileName, DwgVersion.AC2To22);
            //Document doc1 = Application.DocumentManager.Open(fileName);
            //using (TransactionWrapper tw = doc1.StartTransaction())
            //{
            //    ObjectId oids = tw.CreateEntity<DBPoint>().ObjectId;
            //    ptId = oids;
            //    // Do commit even for readonly operations.
            //    tw.Commit();
            //}
            //doc1.CloseAndDiscard();

            //NOTE For now not possible to load non existing object. 
            //NOTE It is marked erased (not sure when it becomes unerased).
            //NOTE However, we store handle to NOD, and load it when dwg is opened
            //NOTE so not possible to have ObjectId which belongs to another Object 
            //NOTE (it will be ObjectId.Null).

            using (TransactionWrapper tw = doc.StartTransaction())
            {
                ObjectId id = ptId;// tw.Db.GetObjectId(false, new Handle(12345), 0);
                Assert.AreNotEqual(ObjectId.Null, id);
                tw.Tr.GetObject(id, OpenMode.ForRead, true);
                Assert.Throws<Exception>(()=> tw.Tr.GetObject(id, OpenMode.ForRead));
                tw.Commit();
            }

        }
    }
}
