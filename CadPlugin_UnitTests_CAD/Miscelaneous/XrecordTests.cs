﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin_RunTester_CAD.Miscelaneous
{
    //[TestFixture]
    class XrecordTests
    {
        //[Test]
        public void Xrecord_Undo_Persistence()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            ObjectId oid;
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                DBDictionary dic = tw.NOD.OpenOrCreateSubDictionary(tw.Tr, "TEST");
                dic.OpenOrCreateXrecord(tw.Tr, "t").SetData(new TypedValue((int)DxfCode.Int16, 123));
                // Do commit even for readonly operations.
                oid = tw.CreateEntity<Line>().ObjectId;
                tw.Commit();
            }

            doc.SendStringToExecute("._UNDO ._M ", true, false, true);


            using (TransactionWrapper tw = doc.StartTransaction())
            {
                DBDictionary dic = tw.NOD.OpenSubDictionary(tw.Tr, "TEST");
                dic.UpgradeOpen();
                dic.OpenOrCreateXrecord(tw.Tr, "t").SetData(new TypedValue((int)DxfCode.Int16, 34));

                // Do commit even for readonly operations.
                tw.Commit();
            }

            using (TransactionWrapper tw = doc.StartTransaction())
            {
                DBDictionary dic = tw.NOD.OpenSubDictionary(tw.Tr, "TEST");
                Xrecord r = dic.OpenXrecord(tw.Tr, "t");
                var v = r.Data.AsArray()[0].ToString();
                tw.GetObjectWrite<DBObject>(oid).Erase();
                // Do commit even for readonly operations.
                tw.Commit();
            }

            doc.SendStringToExecute("._UNDO ._B", true, false, true);


            using (TransactionWrapper tw = doc.StartTransaction())
            {
                DBDictionary dic = tw.NOD.OpenSubDictionary(tw.Tr, "TEST");
                Xrecord r = dic.OpenXrecord(tw.Tr, "t");
                var v = r.Data.AsArray()[0].ToString();

                bool b = tw.Tr.GetObject(oid, OpenMode.ForRead, true).IsErased;
                // Do commit even for readonly operations.
                tw.Commit();
            }

        }
    }
}
