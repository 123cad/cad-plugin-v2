﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin_RunTester_CAD.Miscelaneous
{
    //[TestFixture]
    class UneraseTest
    {
        //[Test]
        public void TestUnerase()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;

            ObjectId ptId = ObjectId.Null;

            using (TransactionWrapper tw = doc.StartTransaction())
            {
                DBPoint p = tw.CreateEntity<DBPoint>();
                ptId = p.ObjectId;

                // Do commit even for readonly operations.
                tw.Commit();
            }

            using (TransactionWrapper tw = doc.StartTransaction())
            {
                tw.GetObjectWrite<DBObject>(ptId).Erase();


                // Do commit even for readonly operations.
                tw.Commit();
            }


            using (TransactionWrapper tw = doc.StartTransaction())
            {
                DBObject obj = tw.Tr.GetObject(ptId, OpenMode.ForRead, true);
                obj.Unerase();

                // Do commit even for readonly operations.
                tw.Commit();
            }

            using (TransactionWrapper tw = doc.StartTransaction())
            {
                var pt = tw.GetObjectRead<DBObject>(ptId);


                // Do commit even for readonly operations.
                tw.Commit();
            }


        }
    }
}
