﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Diagnostics;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin_RunTester_CAD.Miscelaneous
{
    //[TestFixture]
    class TransactionTests
    {
        public void TransactionSpeedTest()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            int entities = 10000;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            using (Transaction tr = doc.Database.TransactionManager.StartTransaction())
            {
                BlockTable bt = tr.GetObject(doc.Database.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                for (int i = 0; i < entities; i++)
                {
                    var pt = new Polyline();
                    btr.AppendEntity(pt);
                    tr.AddNewlyCreatedDBObject(pt, true);
                }
                tr.Commit();
            }
            sw.Stop();
            long single = sw.ElapsedMilliseconds;
            sw.Restart();

            for (int i = 0; i < entities; i++)
            {
                using (Transaction tr = doc.Database.TransactionManager.StartTransaction())
                {
                    BlockTable bt = tr.GetObject(doc.Database.BlockTableId, OpenMode.ForRead) as BlockTable;
                    BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                    var pt = new Polyline();
                    btr.AppendEntity(pt);
                    tr.AddNewlyCreatedDBObject(pt, true);
                    tr.Commit();
                }
            }
            sw.Stop();
            long multiple = sw.ElapsedMilliseconds;
            long total = single + multiple;
        }
    }
}
