﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin_RunTester_CAD.Miscelaneous
{
    //[TestFixture]
    class LineTests
    {
        //[Test]
        public void IntersectionTest_ParallelLines()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;

            Point3d p1 = new Point3d(0, 0, 0);
            Point3d p2 = new Point3d(5, 0, 0);
            Point3d p3 = new Point3d(5, 1, 0);
            Point3d p4 = new Point3d(10, 1, 0);

            using (Line l1 = new Line(p1, p2))
            using (Line l2 = new Line(p3, p4))
            {
                Point3dCollection p3d = new Point3dCollection();
                l1.IntersectWith(l2, Intersect.ExtendBoth, p3d, 0, 0);
                List<Point3d> list = p3d.Cast<Point3d>().ToList();
                // 0 points returned.
            }
        }
        //[Test]
        public void IntersectionTest_Colinear()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;

            Point3d p1 = new Point3d(0, 0, 0);
            Point3d p2 = new Point3d(5, 0, 0);
            Point3d p3 = new Point3d(5, 0, 0);
            Point3d p4 = new Point3d(8, 0, 0);

            using (Line l1 = new Line(p1, p2))
            using (Line l2 = new Line(p3, p4))
            {
                Point3dCollection p3d = new Point3dCollection();
                l1.IntersectWith(l2, Intersect.ExtendBoth, p3d, 0, 0);
                List<Point3d> list = p3d.Cast<Point3d>().ToList();
                // When lines are colinear, 0 points are returned. Same as when they are parallel.
            }
        }
    }
}
