﻿using System;
using System.Text;
using System.Collections.Generic;
using NUnit;
using NUnit.Framework;
using NetworkKS.SewageElements;
using MyUtilities.Geometry;
using System.Linq;
using my = MyExceptions;
using CadPlugin_RunTester_CAD;

namespace CadPlugin_UnitTests_CAD.Tests.SewageElements
{
    /// <summary>
    /// Summary description for PipeTests
    /// </summary>
    [TestFixture]
    public class PipeTests
    {
        [Test]
        public void GetDistance2dAtPoint_StraightSegments_CorrectDistance()
        {
            // Prepare
            Point3d[] vertices = new Point3d[]
             {
                new Point3d(),
                new Point3d(2,0,-1),
                new Point3d(2, 2, -1)
             };
            List<PipeSegment> segs = new List<PipeSegment>();
            for (int i = 0; i < vertices.Length - 1; i++)
            {
                segs.Add(new PipeSegment()
                {
                    Start = vertices[i],
                    End = vertices[i + 1]
                });
            }
            Pipe p = new Pipe();
            p.Diameter = 0.3;
            p.AddPipeSegment(segs.ToArray());
            Point3d point = new Point3d(1, 0, 0);

            // Execute
            double d = p.GetDistance2dAtPoint(point);

            // Assert
            Assert.AreEqual(1, d, 0.001);

            point = new Point3d(2, 1, 0);
            d = p.GetDistance2dAtPoint(point);
            Assert.AreEqual(3, d);
        }
        [Test]
        public void GetDistance2dAtPoint_ArcSegments_CorrectDistance()
        {
            // Prepare
            Point3d[] vertices = new Point3d[]
             {
                new Point3d(),
                new Point3d(2,0,-1),
                new Point3d(2, 2, -1)
             };
            List<PipeSegment> segs = new List<PipeSegment>();
            for (int i = 0; i < vertices.Length - 1; i++)
            {
                double bulge = i == 0 ? 1 : 0;
                segs.Add(new PipeSegment()
                {
                    Start = vertices[i],
                    End = vertices[i + 1],
                    Bulge = bulge
                });
            }
            Pipe p = new Pipe();
            p.Diameter = 0.3;
            p.AddPipeSegment(segs.ToArray());
            Point3d point = new Point3d(1, -1, 0);

            // Execute
            double d = p.GetDistance2dAtPoint(point);

            // Assert
            Assert.AreEqual(Math.PI / 2, d, 0.001);

            point = new Point3d(2, 1, 0);
            d = p.GetDistance2dAtPoint(point);
            Assert.AreEqual(Math.PI + 1, d);
        }
        [Test]
        public void GetClosestPointTo_StraightSegments_Pass()
        {
            // Prepare
            Point3d[] vertices = new Point3d[]
             {
                new Point3d(),
                new Point3d(2,0,-1),
                new Point3d(2, 2, -1)
             };
            List<PipeSegment> segs = new List<PipeSegment>();
            for (int i = 0; i < vertices.Length - 1; i++)
            {
                segs.Add(new PipeSegment()
                {
                    Start = vertices[i],
                    End = vertices[i + 1]
                });
            }
            Pipe p = new Pipe();
            p.Diameter = 0.3;
            p.AddPipeSegment(segs.ToArray());
            Point3d point = new Point3d(1, -1, 0);

            // Execute
            var pt = p.GetClosestPointTo(point);

            // Assert
            Assert.AreEqual(new Point3d(1, 0, -0.5), pt);

            point = new Point3d(3, 1, 0);
            pt = p.GetClosestPointTo(point);
            Assert.AreEqual(new Point3d(2, 1, -1), pt);
        }
        [Test]
        public void GetClosestPointTo_ArcSegments_Pass()
        {
            // Prepare
            Point3d[] vertices = new Point3d[]
             {
                new Point3d(),
                new Point3d(2,0,-1),
                new Point3d(2, 2, -1)
             };
            List<PipeSegment> segs = new List<PipeSegment>();
            for (int i = 0; i < vertices.Length - 1; i++)
            {
                double bulge = i == 0 ? 1 : 0;
                segs.Add(new PipeSegment()
                {
                    Start = vertices[i],
                    End = vertices[i + 1],
                    Bulge = bulge
                });
            }
            Pipe p = new Pipe();
            p.Diameter = 0.3;
            p.AddPipeSegment(segs.ToArray());
            Point3d point = new Point3d(1, -2, 0);

            // Execute
            var pt = p.GetClosestPointTo(point);

            // Assert
            Assert.AreEqual(new Point3d(1, -1, -0.5), pt);

            point = new Point3d(3, 1, 0);
            pt = p.GetClosestPointTo(point);
            Assert.AreEqual(new Point3d(2, 1, -1), pt);
        }
        [Test]
        public void GetClosestPointTo_StraightSegmentsOnPipe_Pass()
        {
            // Prepare
            Point3d[] vertices = new Point3d[]
             {
                new Point3d(),
                new Point3d(2,0,-1),
                new Point3d(2, 2, -1)
             };
            List<PipeSegment> segs = new List<PipeSegment>();
            for (int i = 0; i < vertices.Length - 1; i++)
            {
                segs.Add(new PipeSegment()
                {
                    Start = vertices[i],
                    End = vertices[i + 1]
                });
            }
            Pipe p = new Pipe();
            p.Diameter = 0.3;
            p.AddPipeSegment(segs.ToArray());
            Point3d point = new Point3d(1, 0, -0.5);

            // Execute
            var pt = p.GetClosestPointTo(point);

            // Assert
            Assert.AreEqual(new Point3d(1, 0, -0.5), pt);

            point = new Point3d(3, 1, 0);
            pt = p.GetClosestPointTo(point);
            Assert.AreEqual(new Point3d(2, 1, -1), pt);
        }
        [Test]
        public void GetClosestPointTo_ArcSegmentsOnPipe_Pass()
        {
            // Prepare
            Point3d[] vertices = new Point3d[]
             {
                new Point3d(),
                new Point3d(2,0,-1),
                new Point3d(2, 2, -1)
             };
            List<PipeSegment> segs = new List<PipeSegment>();
            for (int i = 0; i < vertices.Length - 1; i++)
            {
                double bulge = i == 0 ? 1 : 0;
                segs.Add(new PipeSegment()
                {
                    Start = vertices[i],
                    End = vertices[i + 1],
                    Bulge = bulge
                });
            }
            Pipe p = new Pipe();
            p.Diameter = 0.3;
            p.AddPipeSegment(segs.ToArray());
            Point3d point = new Point3d(1, -1, -0.5);

            // Execute
            var pt = p.GetClosestPointTo(point);

            // Assert
            Assert.AreEqual(new Point3d(1, -1, -0.5), pt);

            point = new Point3d(3, 1, 0);
            pt = p.GetClosestPointTo(point);
            Assert.AreEqual(new Point3d(2, 1, -1), pt);
        }
        [Test]
        public void GetClosestPointDistance2d_StraightPipe_()
        {
            // Prepare
            Point3d[] vertices = new Point3d[]
             {
                new Point3d(),
                new Point3d(2,0,-1),
                new Point3d(2, 2, -1)
             };
            List<PipeSegment> segs = new List<PipeSegment>();
            for (int i = 0; i < vertices.Length - 1; i++)
            {
                segs.Add(new PipeSegment()
                {
                    Start = vertices[i],
                    End = vertices[i + 1]
                });
            }
            Pipe p = new Pipe();
            p.Diameter = 0.3;
            p.AddPipeSegment(segs.ToArray());
            Point3d point = new Point3d(1, -1, 0);

            // Execute
            double d = p.GetClosestPointDistance2d(point);

            // Assert
            Assert.AreEqual(1, d, 0.001);

            point = new Point3d(3, 1, 0);
            d = p.GetClosestPointDistance2d(point);
            Assert.AreEqual(3, d);
        }
        [Test]
        public void GetClosestPointDistance2d_ArcPipe_()
        {
            // Prepare
            Point3d[] vertices = new Point3d[]
             {
                new Point3d(),
                new Point3d(2,0,-1),
                new Point3d(2, 2, -1)
             };
            List<PipeSegment> segs = new List<PipeSegment>();
            for (int i = 0; i < vertices.Length - 1; i++)
            {
                double bulge = i == 0 ? 1 : 0;
                segs.Add(new PipeSegment()
                {
                    Start = vertices[i],
                    End = vertices[i + 1],
                    Bulge = bulge
                });
            }
            Pipe p = new Pipe();
            p.Diameter = 0.3;
            p.AddPipeSegment(segs.ToArray());
            Point3d point = new Point3d(1, -1.1, 0);

            // Execute
            double d = p.GetClosestPointDistance2d(point);

            // Assert
            Assert.AreEqual(Math.PI / 2, d, 0.001);

            point = new Point3d(3, 1, 0);
            d = p.GetClosestPointDistance2d(point);
            Assert.AreEqual(Math.PI + 1, d);
        }
    }
}
