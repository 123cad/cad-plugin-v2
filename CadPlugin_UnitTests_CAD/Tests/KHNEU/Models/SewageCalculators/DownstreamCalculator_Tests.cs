﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.KHNeu.Models;
using CadPlugin.KHNeu.Models.Datas;
using CadPlugin.KHNeu.Models.SewageCalculators;
using CadPlugin.KHNeu.Models.TerrainHeights;
using CadPlugin.KHNeu.Views.UserControls.PipeModels;
using CadPlugin.KHNeu.Views.UserControls.ShaftModels;
using CadPlugin_RunTester_CAD;
using FakeItEasy;
using MyUtilities.Geometry;
using NUnit.Framework;


namespace CadPlugin_UnitTests_CAD.Tests.KHNEU.Models.SewageCalculators
{
	class DownstreamCalculator_Tests
	{

		private const WaterDirection downstream = WaterDirection.Downstream;


		[Test]
		/// <summary>
		///	Pipe is going at slope 0, length is 10 - diameter of start/end shaft.
		///	MinSlope - no
		///	MinDelta (2) - no
		/// Angle - no
		/// Segments - no
		/// </summary>
		public void CalculateNextShaft_Test1()
		{
			ITerrainHeight terrainCalc = A.Fake<ITerrainHeight>();
			double shaft2Height = 12;
			A.CallTo(() => terrainCalc.GetHeight(A<MyUtilities.Geometry.Point3d>.Ignored))
				.Returns(shaft2Height);

			ShaftData shaft1Data = new ShaftData()
			{
				Center = new MyUtilities.Geometry.Point2d(0, 0),
				Diameter = 2,
				HeightDMP = 12,
				HeightSMP = 10
			};
			ShaftData shaft2Data = new ShaftData()
			{
				Diameter = 1,
				HeightDMP = 12,
				HeightSMP = 10
			};
			PipeData pipeData = new PipeData()
			{
			};

			#region

			ShaftModel shaft1 = new ShaftModel(true)
			{
				HeightDMP = 12,
				UseMinDelta = false,
				Diameter = 2,
				HeightSMP = shaft1Data.HeightSMP,
				InnerSlope = 10,
				MinDelta = 2
			};
			ShaftModel shaft2 = new ShaftModel(true)
			{
				HeightDMP = shaft2Height,
				HeightSMP = shaft2Data.HeightSMP,
				UseMinDelta = false,
				//HeightSMP = 9.8,//
				Diameter = 2,
				InnerSlope = 10,
				MinDelta = 2
			};
			PipeModel pipe = new PipeModel(downstream)
			{
				// What is needed?
				SegmentLength = 1,
				StartEndSegmentLength = 1,
				UseSegments = false,
			};

			#endregion

			DataHistory historyMgr = A.Fake<DataHistory>();
			A.CallTo(() => historyMgr.Shaft1).Returns(shaft1Data);
			A.CallTo(() => historyMgr.Pipe).Returns(pipeData);
			A.CallTo(() => historyMgr.Shaft2).Returns(shaft2Data);

			var nextPoint = new MyUtilities.Geometry.Point3d(10, 0, 0);
			DownstreamCalculator calculator = new DownstreamCalculator();
			calculator.CalculateNextShaft(
				terrainCalc,
				historyMgr,
				shaft1, pipe, shaft2,
				nextPoint
			);


			Assert.AreEqual(nextPoint.GetAs2d(), shaft2Data.Center);
		}

		[Test]
		/// <summary>
		///	Pipe is going at slope 0, length is 10 - diameter of start/end shaft.
		///	MinSlope - no
		///	MinDelta (2) - no
		/// Angle - no
		/// Segments - yes
		/// </summary>
		public void CalculateNextShaft_Test2()
		{
			ITerrainHeight terrainCalc = A.Fake<ITerrainHeight>();
			double shaft2Height = 12;
			A.CallTo(() => terrainCalc.GetHeight(A<MyUtilities.Geometry.Point3d>.Ignored))
				.Returns(shaft2Height);

			ShaftData shaft1Data = new ShaftData()
			{
				Center = new MyUtilities.Geometry.Point2d(0, 0),
				Diameter = 2,
				HeightDMP = 12,
				HeightSMP = 10
			};
			ShaftData shaft2Data = new ShaftData()
			{
				Diameter = 1,
				HeightDMP = 12,
				HeightSMP = 10
			};
			PipeData pipeData = new PipeData()
			{
			};

			#region

			ShaftModel shaft1 = new ShaftModel(true)
			{
				HeightDMP = 12,
				UseMinDelta = false,
				Diameter = 2,
				HeightSMP = shaft1Data.HeightSMP,
				InnerSlope = 10,
				MinDelta = 2
			};
			ShaftModel shaft2 = new ShaftModel(true)
			{
				HeightDMP = shaft2Height,
				HeightSMP = shaft2Data.HeightSMP,
				UseMinDelta = false,
				//HeightSMP = 9.8,//
				Diameter = 1,
				InnerSlope = 10,
				MinDelta = 2
			};
			PipeModel pipe = new PipeModel(downstream)
			{
				// What is needed?
				SegmentLength = 2,
				StartEndSegmentLength = 1,
				UseSegments = true,
			};

			#endregion

			DataHistory historyMgr = A.Fake<DataHistory>();
			A.CallTo(() => historyMgr.Shaft1).Returns(shaft1Data);
			A.CallTo(() => historyMgr.Pipe).Returns(pipeData);
			A.CallTo(() => historyMgr.Shaft2).Returns(shaft2Data);

			var nextPoint = new MyUtilities.Geometry.Point3d(10, 0, 0);
			DownstreamCalculator calculator = new DownstreamCalculator();
			calculator.CalculateNextShaft(
				terrainCalc,
				historyMgr,
				shaft1, pipe, shaft2,
				nextPoint
			);

			Assert.AreEqual(nextPoint.GetAs2d(), shaft2Data.Center);
		}
		[Test]
		/// <summary>
		///	Pipe is going at slope 0, length is 10 - diameter of start/end shaft.
		///	MinSlope - yes
		///	MinDelta (2) - no
		/// Angle - no
		/// Segments - no
		/// </summary>
		public void CalculateNextShaft_Test_3_1()
		{
			ITerrainHeight terrainCalc = A.Fake<ITerrainHeight>();
			double shaft2Height = 12;
			A.CallTo(() => terrainCalc.GetHeight(A<MyUtilities.Geometry.Point3d>.Ignored))
				.Returns(shaft2Height);

			ShaftData shaft1Data = new ShaftData()
			{
				Center = new MyUtilities.Geometry.Point2d(0, 0),
				Diameter = 2,
				HeightDMP = 12,
				HeightSMP = 10
			};
			ShaftData shaft2Data = new ShaftData()
			{
				Diameter = 1,
				HeightDMP = 12,
				HeightSMP = 10
			};
			PipeData pipeData = new PipeData()
			{
			};

			#region

			ShaftModel shaft1 = new ShaftModel(true)
			{
				HeightDMP = 12,
				UseMinDelta = false,
				Diameter = 2,
				HeightSMP = shaft1Data.HeightSMP,
				InnerSlope = 10,
				MinDelta = 2
			};
			ShaftModel shaft2 = new ShaftModel(true)
			{
				HeightDMP = shaft2Height,
				HeightSMP = shaft2Data.HeightSMP,
				UseMinDelta = false,
				//HeightSMP = 9.8,//
				Diameter = 1,
				InnerSlope = 10,
				MinDelta = 2
			};
			PipeModel pipe = new PipeModel(downstream)
			{
				UseSegments = false,
				UseMinSlope = true
			};

			#endregion

			DataHistory historyMgr = A.Fake<DataHistory>();
			A.CallTo(() => historyMgr.Shaft1).Returns(shaft1Data);
			A.CallTo(() => historyMgr.Pipe).Returns(pipeData);
			A.CallTo(() => historyMgr.Shaft2).Returns(shaft2Data);

			var nextPoint = new MyUtilities.Geometry.Point3d(10, 0, 0);
			DownstreamCalculator calculator = new DownstreamCalculator();
			calculator.CalculateNextShaft(
				terrainCalc,
				historyMgr,
				shaft1, pipe, shaft2,
				nextPoint
			);

			Assert.AreEqual(nextPoint.GetAs2d(), shaft2Data.Center);
		}
		[Test]
		/// <summary>
		///	Pipe is going at slope 0, length is 10 - diameter of start/end shaft.
		///	MinSlope - yes
		///	MinDelta (2) - no
		/// Angle - yes
		/// Segments - no
		/// </summary>
		public void CalculateNextShaft_Test_3_2()
		{
			ITerrainHeight terrainCalc = A.Fake<ITerrainHeight>();
			double shaft2Height = 12;
			A.CallTo(() => terrainCalc.GetHeight(A<MyUtilities.Geometry.Point3d>.Ignored))
				.Returns(shaft2Height);

			ShaftData shaft1Data = new ShaftData()
			{
				Center = new MyUtilities.Geometry.Point2d(0, 0),
				Diameter = 2,
				HeightDMP = 12,
				HeightSMP = 10
			};
			ShaftData shaft2Data = new ShaftData()
			{
				Diameter = 1,
				HeightDMP = 12,
				HeightSMP = 10
			};
			PipeData pipeData = new PipeData()
			{
			};

			#region

			ShaftModel shaft1 = new ShaftModel(true)
			{
				HeightDMP = 12,
				UseMinDelta = false,
				Diameter = 2,
				HeightSMP = shaft1Data.HeightSMP,
				InnerSlope = 10,
				MinDelta = 2
			};
			ShaftModel shaft2 = new ShaftModel(true)
			{
				HeightDMP = shaft2Height,
				HeightSMP = shaft2Data.HeightSMP,
				UseMinDelta = false,
				//HeightSMP = 9.8,//
				Diameter = 1,
				InnerSlope = 10,
				MinDelta = 2
			};
			PipeModel pipe = new PipeModel(downstream)
			{
				UseSegments = false,
				UseMinSlope = false,
				UseAngleStep = true,
				AngleStep = 12
			};

			#endregion

			DataHistory historyMgr = A.Fake<DataHistory>();
			A.CallTo(() => historyMgr.Shaft1).Returns(shaft1Data);
			A.CallTo(() => historyMgr.Pipe).Returns(pipeData);
			A.CallTo(() => historyMgr.Shaft2).Returns(shaft2Data);
			A.CallTo(() => historyMgr.GetLastCreatedPipe()).Returns(
				new PipeData()
				{
					Point1 = new MyUtilities.Geometry.Point3d(-10, 0, 0),
					Point2 = new MyUtilities.Geometry.Point3d(-shaft2Data.Diameter, 0, 0)
				});

			var nextPoint = new MyUtilities.Geometry.Point3d(10, 0, 0);
			DownstreamCalculator calculator = new DownstreamCalculator();
			calculator.CalculateNextShaft(
				terrainCalc,
				historyMgr,
				shaft1, pipe, shaft2,
				nextPoint
			);
			Assert.AreEqual(nextPoint.GetAs2d(), shaft2Data.Center);

			nextPoint = new MyUtilities.Geometry.Point3d(10, 0.1, 0);
			Matrix3d matrix = Matrix3d.CreateRotation(new Vector3d(1, 0, 0), 5);
			nextPoint = matrix.Transform(nextPoint);
			calculator = new DownstreamCalculator();
			calculator.CalculateNextShaft(
				terrainCalc,
				historyMgr,
				shaft1, pipe, shaft2,
				nextPoint
			);
			Assert.AreEqual(nextPoint.GetAs2d(), shaft2Data.Center);
		}
	}
}
