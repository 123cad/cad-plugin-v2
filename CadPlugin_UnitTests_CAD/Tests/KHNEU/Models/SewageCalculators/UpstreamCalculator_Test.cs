﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.KHNeu.Models.TerrainHeights;
using CadPlugin_RunTester_CAD;
using FakeItEasy;
using my = MyUtilities.Geometry;
using CadPlugin.KHNeu.Models.Datas;
using CadPlugin.KHNeu.Views.UserControls.ShaftModels;
using CadPlugin.KHNeu.Views.UserControls.PipeModels;
using CadPlugin.KHNeu.Models;
using CadPlugin.KHNeu.Models.SewageCalculators;

namespace CadPlugin_UnitTests_CAD.Tests.KHNEU.Models.SewageCalculators
{
    class UpstreamCalculator_Test
    {
        private static WaterDirection waterflow = WaterDirection.Upstream;
        [Test]
        public void CalculateNextShaft_Test1()
        {
            // No min slope or delta for pipe.
            // Calculation depends on shaft2 depth.
            double terrainHeight = 102;
            var terrain = A.Fake<ITerrainHeight>();
            A.CallTo(() => terrain.GetHeight(A<my.Point3d>.Ignored)).Returns(terrainHeight);
            var history = new DataHistory(waterflow);
            ShaftData shaft1 = history.Shaft1;
            {
                shaft1.Diameter = 1000;
                shaft1.Center = new my.Point2d(-shaft1.Radius, 0);
                shaft1.HeightDMP = terrainHeight - 2;
                shaft1.HeightSMP = 98.6;
            }
            PipeData pipe = history.Pipe;
            {
                pipe.Diameter = 400;
            };
            ShaftData shaft2 = history.Shaft2;
            {
                shaft2.Center = new my.Point2d(0, 0);
                shaft2.Diameter = 1000;
                shaft2.HeightDMP = shaft1.HeightDMP;
                shaft2.HeightSMP = shaft1.HeightSMP;
            };

            ShaftModel shaft1M = new ShaftModel(true)
            {
                Diameter = shaft1.Diameter,
                HeightDMP = shaft1.HeightDMP,
                HeightSMP = shaft1.HeightSMP,
                InnerSlope = 10,
                MinDelta = 2,
                UseMinDelta = false
            };
            PipeModel pipeM = new PipeModel(waterflow)
            {
                UseSegments = false,
                UseMinSlope = false,
                Diameter = pipe.Diameter
            };
            ShaftModel shaft2M = new ShaftModel(true)
            {
                Diameter = shaft2.Diameter,
                HeightDMP = shaft2.HeightDMP,
                HeightSMP = shaft2.HeightSMP,
                InnerSlope = 10,
                MinDelta = 2,
                UseMinDelta = false
            };

            var calculator = new UpstreamCalculator();
            double pipeLendth2d = 10;
            var nextPoint = new my.Point3d(pipeLendth2d + shaft2.Radius, 0, terrainHeight);

            calculator.CalculateNextShaft(terrain, history, shaft1M, pipeM, shaft2M, nextPoint);

        }
    }
}
