﻿using CadPlugin.KHNeu.Models.PipeCalculators.Old_Refactor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using CadPlugin.KHNeu.Models.TerrainHeights;
using CadPlugin_RunTester_CAD;
using NUnit.Framework;
using my = MyUtilities.Geometry;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin_UnitTests_CAD.Tests.KHNEU.Models.PipeCalculators
{
    class SegmentedCalculatorUpstream_Tests
    {
        [Test]
        public void CalculatePointWithMinslopeAndDelta_Test1()
        {
            var lengths = new SegmentedLengthIterator(2, 0, 5, 10);
            var nonSegmentedPipe = new my.Vector3d(-12, 0, 0);
            var point2 = new my.Point3d(12, 0, 0);
            var terrain = A.Fake<ITerrainHeight>();
            A.CallTo(() => terrain.GetHeight(A<my.Point3d>.Ignored)).Returns(1);
            double minSlope = 0;
            double minDelta = 1;
            double shaft2Radius = 1;

            var p = SegmentedCalculatorUpstream.CalculatePointWithMinSlopeAndDelta(
                lengths, nonSegmentedPipe, point2, 0, terrain, minSlope, minDelta, 0, shaft2Radius);

        }
    }
}
