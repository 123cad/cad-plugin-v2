﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.KHNeu.Models;
using CadPlugin.KHNeu.Models.Datas;
using CadPlugin.KHNeu.Models.PipeCalculators;
using CadPlugin.KHNeu.Views.UserControls.PipeModels;
using CadPlugin_RunTester_CAD;
using FakeItEasy;
using MyUtilities.Geometry;
using NUnit.Framework;

namespace CadPlugin_UnitTests_CAD.Tests.KHNEU.Models.PipeCalculators
{
	[Category(TestHelper.RunGroupName)]
	class PipeCalculatorDownstream_Tests
	{
		private const WaterDirection downstream = WaterDirection.Downstream;

		private PipeData previousPipe = new PipeData()
		{
			Point1 = new MyUtilities.Geometry.Point3d(-10, 0, 0),
			Point2 = new MyUtilities.Geometry.Point3d(0, 0, 0)
		};

		private Vector2d previousPipeDirection = new Vector2d(1, 0);
		[Test]
		public void AdjustPipeDirectionAngle()
		{
			Point2d startSMP = new Point2d(0, 0);
			double shaft1Radius = 0.5;
			double shaft2Radius = 0.5;
			Point2d nextPoint = new Point2d(10, 0);
			DataHistory historyMgr = A.Fake<DataHistory>();
			A.CallTo(() => historyMgr.GetLastCreatedPipe())
				.Returns(previousPipe);
			A.CallTo(() => historyMgr.IsFirstPipe).Returns(false);


			PipeModel pipe = new PipeModel(downstream)
			{
				UseSegments = false,
				UseMinSlope = false,
				UseAngleStep = true,
			};

			Point2d result = new Point2d();

			var angleCalc = new PipeCalculatorDownstream();
			angleCalc.Initialize(startSMP, shaft1Radius, shaft2Radius, nextPoint);
			
			pipe.AngleStep = 12;
			bool retVal = angleCalc.AdjustPipeDirectionAngle(historyMgr, pipe, ref result);
			Assert.IsTrue(retVal);
			double angle = previousPipeDirection.GetAngleDeg(result.GetAsVector());
			Assert.AreEqual(0, angle);

			nextPoint = new Point2d(10, 0.1);
			angleCalc.Initialize(startSMP, shaft1Radius, shaft2Radius, nextPoint);
			retVal = angleCalc.AdjustPipeDirectionAngle(historyMgr, pipe, ref result);
			Assert.IsTrue(retVal);
			angle = previousPipeDirection.GetAngleDeg(result.GetAsVector());
			Assert.AreEqual(0, angle, 1e-5);


		}
	}
}
