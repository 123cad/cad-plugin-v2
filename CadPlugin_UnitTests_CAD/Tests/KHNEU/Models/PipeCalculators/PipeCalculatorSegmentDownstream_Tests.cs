﻿using CadPlugin.KHNeu.Models.PipeCalculators.Old_Refactor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using CadPlugin.KHNeu.Models.TerrainHeights;
using CadPlugin_RunTester_CAD;
using NUnit.Framework;
using my = MyUtilities.Geometry;
using CadPlugin.KHNeu.Views.UserControls.ShaftModels;
using CadPlugin.KHNeu.Models.TerrainHeights.TerrainHeightCalculators;
using CadPlugin.KHNeu.Views.UserControls.PipeModels;
using CadPlugin.KHNeu.Models;
using CadPlugin.KHNeu.Models.PipeCalculators;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin_UnitTests_CAD.Tests.KHNEU.Models.PipeCalculators
{
    class PipeCalculatorSegmentDownstream_Tests
	{
	    private const WaterDirection downstream = WaterDirection.Downstream;

        [Test]
        public void AdjustForSegment_Test_1()
        {
			// Pipe is going at slope 0, length is 10.
			// MinSlope - yes
			// MinDelta (2) - no
			// Angle - no
			// Segments - yes
			// ***
			// Result:  extend or shrink pipe vector, to keep pipe slope.

	        my.Point3d pipeStart = new my.Point3d(0, 0, 10);
			// Negative Z goes up?
	        my.Vector3d nonSegmentedPipe = new my.Vector3d(10, 0, 0);
	        ITerrainHeight terrainCalc = A.Fake<ITerrainHeight>();
	        double shaft2Height = 12;
	        A.CallTo(() => terrainCalc.GetHeight(A<my.Point3d>.Ignored))
		        .Returns(shaft2Height);
	        ShaftModel shaft1 = new ShaftModel(true)
	        {
		        HeightDMP = 12,
		        UseMinDelta = false,
		        Diameter = 2,
		        HeightSMP = pipeStart.Z,
		        InnerSlope = 10,
		        MinDelta = 2
	        };
	        ShaftModel shaft2 = new ShaftModel(true)
	        {
		        HeightDMP = shaft2Height,
		        UseMinDelta = false,
				//HeightSMP = 9.8,//
		        Diameter = 2,
		        InnerSlope = 10,
		        MinDelta = 2
	        };
			PipeModel pipe = new PipeModel(downstream)
			{
				// What is needed?
				SegmentLength = 1,
				StartEndSegmentLength = 1,
				UseSegments = true,
				UseMinSlope = true,

	        };
	        shaft2.HeightSMP = pipeStart.Z + nonSegmentedPipe.Z - shaft2.GetInnerSlopeDelta();

	        var segmentCalculator = new PipeCalculatorSegmentDownstream();
	        var res = segmentCalculator.AdjustForSegment(pipeStart, nonSegmentedPipe, shaft1, pipe, shaft2, terrainCalc);
	        Assert.AreEqual(new my.Vector3d(10, 0, 0), res);

	        pipe.SegmentLength = 2;
			res = segmentCalculator.AdjustForSegment(pipeStart, nonSegmentedPipe, shaft1, pipe, shaft2, terrainCalc);
	        Assert.AreEqual(new my.Vector3d(10, 0, 0), res);

			pipe.SegmentLength = 1;
			pipe.StartEndSegmentLength = 0.6;
			res = segmentCalculator.AdjustForSegment(pipeStart, nonSegmentedPipe, shaft1, pipe, shaft2, terrainCalc);
			Assert.AreEqual(new my.Vector3d(10.2, 0, 0), res);

			pipe.SegmentLength = 1;
			pipe.StartEndSegmentLength = 0.4;
			res = segmentCalculator.AdjustForSegment(pipeStart, nonSegmentedPipe, shaft1, pipe, shaft2, terrainCalc);
			Assert.AreEqual(new my.Vector3d(9.8, 0, 0), res);

			pipe.SegmentLength = 5;
			pipe.StartEndSegmentLength = 1.5;
			res = segmentCalculator.AdjustForSegment(pipeStart, nonSegmentedPipe, shaft1, pipe, shaft2, terrainCalc);
			Assert.AreEqual(new my.Vector3d(8, 0, 0), res);
		}
        [Test]
        public void AdjustForSegment_Test_2()
        {
			// Pipe is going at slope 0, length is 10 .
			// MinSlope - yes
			// MinDelta (2) - no
			// Angle - no
			// Segments - yes
			// ***
			// Result:  extend or shrink pipe vector, to keep pipe slope.

	        my.Point3d pipeStart = new my.Point3d(0, 0, 10);
			// Negative Z goes up?
	        my.Vector3d nonSegmentedPipe = new my.Vector3d(10, 0, -10);
	        ITerrainHeight terrainCalc = A.Fake<ITerrainHeight>();
	        double shaft2Height = 12;
	        A.CallTo(() => terrainCalc.GetHeight(A<my.Point3d>.Ignored))
		        .Returns(shaft2Height);
	        ShaftModel shaft1 = new ShaftModel(true)
	        {
		        HeightDMP = 12,
		        UseMinDelta = false,
		        Diameter = 2,
		        HeightSMP = pipeStart.Z,
		        InnerSlope = 10,
		        MinDelta = 2
	        };
	        ShaftModel shaft2 = new ShaftModel(true)
	        {
		        HeightDMP = shaft2Height,
		        UseMinDelta = false,
				//HeightSMP = 9.8,//
		        Diameter = 2,
		        InnerSlope = 10,
		        MinDelta = 2
	        };
			PipeModel pipe = new PipeModel(downstream)
			{
				// What is needed?
				SegmentLength = 1,
				StartEndSegmentLength = 1,
				UseSegments = true,
				UseMinSlope = true,

	        };
	        shaft2.HeightSMP = pipeStart.Z + nonSegmentedPipe.Z - shaft2.GetInnerSlopeDelta();

	        var segmentCalculator = new PipeCalculatorSegmentDownstream();
	        var res = segmentCalculator.AdjustForSegment(pipeStart, nonSegmentedPipe, shaft1, pipe, shaft2, terrainCalc);
	        double length = 14;
	        double d = Math.Sqrt(length * length / 2);
	        Assert.AreEqual(new my.Vector3d(d, 0, -d), res);

	        pipe.SegmentLength = 2;
			res = segmentCalculator.AdjustForSegment(pipeStart, nonSegmentedPipe, shaft1, pipe, shaft2, terrainCalc);
			length = 14;
			d = Math.Sqrt(length * length / 2);
			Assert.AreEqual(new my.Vector3d(d, 0, -d), res);

			pipe.SegmentLength = 1;
			pipe.StartEndSegmentLength = 0.6;
			res = segmentCalculator.AdjustForSegment(pipeStart, nonSegmentedPipe, shaft1, pipe, shaft2, terrainCalc);
			length = 14.2;
			d = Math.Sqrt(length * length / 2);
			Assert.AreEqual(new my.Vector3d(d, 0, -d), res);

			pipe.SegmentLength = 1;
			pipe.StartEndSegmentLength = 0.4;
			res = segmentCalculator.AdjustForSegment(pipeStart, nonSegmentedPipe, shaft1, pipe, shaft2, terrainCalc);
			length = 13.8;
			d = Math.Sqrt(length * length / 2);
			Assert.AreEqual(new my.Vector3d(d, 0, -d), res);

			pipe.SegmentLength = 5;
			pipe.StartEndSegmentLength = 1.5;
			res = segmentCalculator.AdjustForSegment(pipeStart, nonSegmentedPipe, shaft1, pipe, shaft2, terrainCalc);
			length = 13;
			d = Math.Sqrt(length * length / 2);
			Assert.AreEqual(new my.Vector3d(d, 0, -d), res);
		}
		//[Test]
		public void AdjustForSegment_Test2()
		{
			// Pipe is going at slope 100, length is 10 - diameter of start/end shaft.
			// MinSlope - no
			// MinDelta (2) - no (min delta is used
			// Angle - no
			// Segments - yes
			// ***
			// Result:  extend or shrink pipe vector, to keep pipe slope.

			my.Point3d pipeStart = new my.Point3d(0, 0, 10);
			// Negative Z goes up?
			my.Vector3d nonSegmentedPipe = new my.Vector3d(10, 0, 0);
			ITerrainHeight terrainCalc = A.Fake<ITerrainHeight>();
			double shaft2Height = 12;
			A.CallTo(() => terrainCalc.GetHeight(A<my.Point3d>.Ignored))
				.Returns(shaft2Height);
			ShaftModel shaft1 = new ShaftModel(true)
			{
				HeightDMP = 12,
				UseMinDelta = false,
				Diameter = 2,
				HeightSMP = pipeStart.Z,
				InnerSlope = 10,
				MinDelta = 2
			};
			ShaftModel shaft2 = new ShaftModel(true)
			{
				HeightDMP = shaft2Height,
				UseMinDelta = false,
				//HeightSMP = 9.8,//
				Diameter = 2,
				InnerSlope = 10,
				MinDelta = 2
			};
			PipeModel pipe = new PipeModel(downstream)
			{
				// What is needed?
				SegmentLength = 1,
				StartEndSegmentLength = 1,
				UseSegments = true,
				UseMinSlope = true,

			};
			shaft2.HeightSMP = pipeStart.Z + nonSegmentedPipe.Z - shaft2.GetInnerSlopeDelta();

			var segmentCalculator = new PipeCalculatorSegmentDownstream();
			var res = segmentCalculator.AdjustForSegment(pipeStart, nonSegmentedPipe, shaft1, pipe, shaft2, terrainCalc);
			Assert.AreEqual(new my.Vector3d(10, 0, 0), res);

			pipe.SegmentLength = 2;
			res = segmentCalculator.AdjustForSegment(pipeStart, nonSegmentedPipe, shaft1, pipe, shaft2, terrainCalc);
			Assert.AreEqual(new my.Vector3d(10, 0, 0), res);

			pipe.SegmentLength = 1;
			pipe.StartEndSegmentLength = 0.6;
			res = segmentCalculator.AdjustForSegment(pipeStart, nonSegmentedPipe, shaft1, pipe, shaft2, terrainCalc);
			Assert.AreEqual(new my.Vector3d(10.2, 0, 0), res);

			pipe.SegmentLength = 1;
			pipe.StartEndSegmentLength = 0.4;
			res = segmentCalculator.AdjustForSegment(pipeStart, nonSegmentedPipe, shaft1, pipe, shaft2, terrainCalc);
			Assert.AreEqual(new my.Vector3d(9.8, 0, 0), res);

			pipe.SegmentLength = 5;
			pipe.StartEndSegmentLength = 1.5;
			res = segmentCalculator.AdjustForSegment(pipeStart, nonSegmentedPipe, shaft1, pipe, shaft2, terrainCalc);
			Assert.AreEqual(new my.Vector3d(8, 0, 0), res);
		}
	}
}
