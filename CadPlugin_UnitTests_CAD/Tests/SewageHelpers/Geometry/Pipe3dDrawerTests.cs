﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin_RunTester_CAD;
using SewageUtilities.Helpers.Geometry;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin_UnitTests_CAD.Tests.SewageHelpers
{
    [TestFixture]
    class Pipe3dDrawerTests
    {
        Document doc = Application.DocumentManager.MdiActiveDocument;

        public class JointData
        {
            private static int id = 0;
            public int Id = id++;
            public string Name = "";

            public double StartBulge = 0;
            public double EndBulge = 0;
            public double Diameter = 0.2;
            public Point3d Joint = new Point3d();
            public Vector3d StartVector = new Vector3d();
            public Vector3d EndVector = new Vector3d();

            public Point3d ResultStartPoint;
            public Point3d ResultEndPoint;
            public double ResultRadius;
            public double ResultTotalLength;
            public double ResultTurns;
            public bool ResultTwist;
            public double ResultHeight;

            public JointData(Vector3d startVector, Vector3d endVector, Point3d resultStartPoint
                            , Point3d resultEndPoint, double resultRadius, double resultTotalLength
                            , double resultTurns, bool resultTwist)
            {
                StartVector = startVector;
                EndVector = endVector;
                ResultStartPoint = resultStartPoint;
                ResultEndPoint = resultEndPoint;
                ResultRadius = resultRadius;
                ResultTotalLength = resultTotalLength;
                ResultTurns = resultTurns;
                ResultTwist = resultTwist;
            }
            public override string ToString()
            {
                return $"\"{Name}:id {Id}\"";
            }
        }
        public class HelixTestData
        {
            public static IEnumerable<TestCaseData> Data
            {
                get
                {
                    JointData jd;
                    Point3d joint = new Point3d();
                    jd = new JointData(new Vector3d(1, 0, 0),
                                        new Vector3d(0, -1, 0),
                                           new Point3d(joint.X - 0.11, joint.Y, joint.Z),
                                           new Point3d(joint.X, joint.Y - 0.11, joint.Z),
                                           resultRadius: 0.11,
                                           resultTotalLength: 0.225991,
                                           resultTurns: 0.25, true)
                    {
                        Name = "End-Start = 90deg (CW)"
                    };
                    yield return new TestCaseData(jd);

                    joint = new Point3d(1, 1, 1);
                    jd = new JointData(new Vector3d(1, 0, 0),
                                        new Vector3d(0, -1, 0),
                                           new Point3d(joint.X- 0.11, joint.Y, joint.Z),
                                           new Point3d(joint.X, joint.Y- 0.11, joint.Z),
                                           resultRadius: 0.11,
                                           resultTotalLength: 0.225991,
                                           resultTurns: 0.25, true)
                    {
                        Joint = joint,
                        Name = "End-Start = 90deg (CW)"
                    };
                    yield return new TestCaseData(jd);
                    //TODO Test slucajevi za rotirano

                    // Start is arc.
                    // This is little bit problematic. Connection point to joint arc is not good.
                    joint = new Point3d(1, 1, 1);
                    jd = new JointData(new Vector3d(0, -1, 0),
                                        new Vector3d(0, -1, 0),
                                           new Point3d(joint.X - 0.1089, joint.Y + 0.010928, joint.Z), // Not so good, should be better.
                                           new Point3d(joint.X, joint.Y - 0.1095, joint.Z),
                                           resultRadius: 0.144,
                                           resultTotalLength: 0.1778,
                                           resultTurns: 0.23408, true)
                    {
                        Joint = new Point3d(1, 1, 1),
                        Name = "Start bulge 1, Joint connection = 90deg (CW)",
                        StartBulge = 1
                    };
                    //yield return new TestCaseData(jd);
                }
            }
        }
        [TestCaseSource(typeof(HelixTestData), nameof(HelixTestData.Data))]
        public void DrawSegmentsJointWithHelix_BothLineSegments(JointData data)
        {

            using (TransactionWrapper tw = doc.StartTransaction())
            {
                Point3d joint = data.Joint;
                Vector3d start = data.StartVector;
                Vector3d end = data.EndVector;
                
                double startBulge = data.StartBulge;
                double endBulge = data.EndBulge;
                double pipeDiameter = data.Diameter;
                Helix h = Pipe3dDrawer.DrawSegmentsJointWithHelix(
                    tw, joint, start, startBulge, end, endBulge, pipeDiameter);

                // This difference could be because of different reference point on arc.
                //Assert.AreEqual(data.ResultStartPoint, h.StartPoint);
                //Assert.AreEqual(data.ResultEndPoint, h.EndPoint);
                //Assert.AreEqual(data.ResultRadius, h.BaseRadius, 0.0001);
                //Assert.AreEqual(data.ResultRadius, h.TopRadius, 0.0001);
                ////Assert.AreEqual(data.ResultTotalLength, h.TotalLength, 0.0001);
                //Assert.AreEqual(data.ResultTurns, h.Turns, 0.0001);
                //Assert.IsTrue(data.ResultTwist); // Could be changed with still haveing correct drawing.
                //Assert.AreEqual(data.ResultHeight, h.TurnHeight, 0.0001);
                //Assert.AreEqual(data.ResultHeight, h.Height, 0.0001);

                 h.Erase();
                tw.Commit();
            }
        }

    }
}
