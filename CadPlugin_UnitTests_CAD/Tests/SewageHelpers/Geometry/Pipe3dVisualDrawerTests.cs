﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin_RunTester_CAD;
using CadPlugin.Common;
using CadPlugin.Sewage.Pipe3dProfiles;
using CadPlugin.Sewage.Pipe3dProfiles.PipeProfileCreators;
using SewageUtilities.Helpers;
using static SewageUtilities.Geometry.Pipe3dVisualDrawer;
using SewageUtilities.Geometry;


#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin_UnitTests_CAD.Tests.SewageHelpers
{
    [TestFixture]
    class Pipe3dVisualDrawerTests
    {
        Document doc = Application.DocumentManager.MdiActiveDocument;

        
        public class TestData
        {
            public static IEnumerable<TestCaseData> ReducedData
            {
                get
                {
                    Point3d start = new Point3d(2, 0, 0);
                    Point3d end = new Point3d(3, 1, 1);
                    double diameter = 0.2;
                    double reduceStart = 0.1;
                    double reduceEnd = 0.2;
                    double bulge = 0;
                    Point3d resultStart = new Point3d();
                    Point3d resultEnd = new Point3d();
                    yield return new TestCaseData(
                        start, end, diameter, reduceStart, reduceEnd, bulge,
                                        resultStart, resultEnd);

                    start = new Point3d();
                    end = new Point3d(1, 1, 1);
                    diameter = 0.2;
                    reduceStart = 0.1;
                    reduceEnd = 0.2;
                    bulge = 1;
                    yield return new TestCaseData(
                        start, end, diameter, reduceStart, reduceEnd, bulge,
                                        resultStart, resultEnd);
                }
            }

            public static IEnumerable<TestCaseData> PipeSegmentData
            {
                get
                {
                    double diameter = 0.2;
                    PipeVertex[] vertices = new PipeVertex[]
                    {
                        new PipeVertex(new Point3d()),
                        new PipeVertex(new Point3d(1, 0, 0)),
                        new PipeVertex(new Point3d(1, -1, 0)),
                    };
                    List<Point3d> points = new List<Point3d>()
                    {
                        vertices[0].Point,
                        new Point3d(0.879, 0, 0),
                        new Point3d(1, -0.121, 0),
                        vertices.Last().Point
                    };
                    yield return new TestCaseData(diameter, vertices, points);
                }
            }
        }
        [TestCaseSource(typeof(TestData), nameof(TestData.ReducedData))]
        public void DrawSegmentReduced_Tests(Point3d start, Point3d end, 
            double diameter, double reduceStart, double reduceEnd, double bulge,
            Point3d resultStartPoint, Point3d resultEndPoint)
        {

            TestHelper.RunOnUIThread(
                () =>
                {
                    using (TransactionWrapper tw = doc.StartTransaction())
                    {
                        var profile = new PipeProfileCircle();
                        ComplexSolidEntitiesCollection ents = new ComplexSolidEntitiesCollection();
                        profile.CreateEntityBase(new PipeParameters(diameter), ents);
                        var solid = Pipe3dVisualDrawer.DrawSegment(tw, start, end, ents.First(), diameter, bulge);
                        var solidReduced = Pipe3dVisualDrawer.DrawSegmentReduced(tw, start, end, ents.First(), diameter * 1.1, reduceStart, reduceEnd, bulge);
                        tw.Commit();
                    }
                });
        }

        [TestCaseSource(typeof(TestData), nameof(TestData.PipeSegmentData))]
        public void DrawPipeSegments_Tests(double diameter, IEnumerable<PipeVertex> vertices, IEnumerable<Point3d> points)
        {
            TestHelper.RunOnUIThread(
                () =>
                {
                    using (TransactionWrapper tw = doc.StartTransaction())
                    {
                        var profile = new PipeProfileCircle();
                        ComplexSolidEntitiesCollection ents = new ComplexSolidEntitiesCollection();
                        profile.CreateEntityBase(new PipeParameters(diameter), ents);
                        DrawPipeSegments(tw, diameter, ents, vertices.ToArray());

                        // Do commit even for readonly operations.
                        tw.Commit();
                    }

                }
                );
        }
    }
}
