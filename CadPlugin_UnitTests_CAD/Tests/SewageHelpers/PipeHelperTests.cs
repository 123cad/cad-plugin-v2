﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using CadPlugin.SewageHelpers;
using SewageUtilities.Helpers;
using CadPlugin_RunTester_CAD;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin_UnitTests_CAD.Tests.SewageHelpers
{
    [TestFixture]
    class PipeHelperTests
    {
        Document doc = Application.DocumentManager.MdiActiveDocument;

        [TestCase(0, -10, 10, 10, -10)]
        [TestCase(0, -10, 10, 5, -5)]
        [TestCase(0, -10, 10, 0, 0)]
        [TestCase(5, -5, 10, 0, 5)]
        [TestCase(5, -5, 10, 5, 0)]
        [TestCase(5, -5, 10, 10, -5)]
        public void InterpolateHeight_Tests(double startHeight,double endHeight, 
                                            double total2dLength, double length2d,
                                            double result)
        {
            var res = PipeHelper.InterpolateHeight(startHeight, endHeight, total2dLength, length2d);
            Assert.AreEqual(result, res, 0.0001);
        }
    }
}
