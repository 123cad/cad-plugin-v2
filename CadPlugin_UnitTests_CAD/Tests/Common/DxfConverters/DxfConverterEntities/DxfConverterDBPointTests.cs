﻿using CadPlugin.Common.DxfConverters.DxfConverterEntities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using CadPlugin_RunTester_CAD.Tests;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif


namespace CadPlugin_RunTester_CAD.Tests.Common.DxfConverters.DxfConverterEntities
{
    [TestFixture]
    class DxfConverterDBPointTests
    {
        Document doc = Application.DocumentManager.MdiActiveDocument;
        Database db;
        [OneTimeSetUp]
        public void Setup()
        {
            db = doc.Database;
        }
        [TearDown]
        public void Destroy()
        {
            Helpers.DeleteAllEntities();
        }
        [Test]
        public void SaveEntity_WhenEntityExists_Pass()
        {
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                DxfConverterDBPoint converter = new DxfConverterDBPoint();
                DBPoint p = tw.CreateEntity<DBPoint>();
                p.Position = new Point3d(1, 2, 3);
                p.Color = Color.FromColor(System.Drawing.Color.Red);
                p.Thickness = 0.43;
                var t = converter.SaveEntity(tw, p);

                int i = 0;
                Assert.AreEqual(t.ElementAt(i++).Value, p.GetType().Name);
                Assert.AreEqual(t.ElementAt(i++).Value, p.Position);
                //Assert.AreEqual(t.ElementAt(i++).Value, p.Handle.Value);
                i++;
                Assert.AreEqual(t.ElementAt(i++).Value, p.Thickness);
                Assert.AreEqual(t.ElementAt(i++).Value, p.Color);

                p.Color = Color.FromColor(System.Drawing.Color.Red);
                t = converter.SaveEntity(tw, p);
                Assert.AreEqual(t.ElementAt(4).Value, p.Color);

                tw.Commit();
            }
        }
        [Test]
        public void SaveEntity_WhenEntityIsWrong_ThrowException()
        {
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                DxfConverterDBPoint converter = new DxfConverterDBPoint();
                Line p = tw.CreateEntity<Line>();
                Assert.Throws<MyExceptions.NotSupportedException>(() => converter.SaveEntity(tw, p));

                tw.Commit();
            }
        }
        [Test]
        public void SaveEntity_WhenEntityIsNull_ThrowException()
        {
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                DxfConverterDBPoint converter = new DxfConverterDBPoint();
                Assert.Throws<MyExceptions.NotSupportedException>(() => converter.SaveEntity(tw, null));

                // Do commit even for readonly operations.
                tw.Commit();
            }
        }
        [Test]
        public void LoadToEntity_DataIsGood_Should()
        {
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                DxfConverterDBPoint converter = new DxfConverterDBPoint();
                TypedValue[] values = new TypedValue[]
                {
                    new TypedValue((int)DxfCode.Start, nameof(DBPoint)),
                    new TypedValue((int)DxfCode.XCoordinate, new Point3d(1,2,3)),
                    new TypedValue((int)DxfCode.Handle, 123),
                    new TypedValue((int)DxfCode.Thickness, 0.23),
                    new TypedValue((int)DxfCode.Color, Color.FromColor(System.Drawing.Color.Green))
                };
                var r = (DBPoint)converter.LoadEntity(tw, values);

                Assert.AreEqual(values[1].Value, r.Position);
                Assert.AreEqual(values[3].Value, r.Thickness);
                Assert.AreEqual(values[4].Value, r.Color);

                tw.Commit();
            }
        }
    }
}
