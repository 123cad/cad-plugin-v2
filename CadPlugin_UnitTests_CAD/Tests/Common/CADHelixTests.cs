﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin_RunTester_CAD.Tests.Common
{
    [TestFixture]
    class CADHelixTests
    {
        [Test]
        public void DrawHelix_Test()
        {
            var doc = Application.DocumentManager.MdiActiveDocument;
            Point3d start = new Point3d();
            Point3d end = new Point3d(1, 0, -1);
            double bulge = 1;

            CadPlugin.Common.CADHelixHelper.DrawHelix(doc, start, end, bulge);

            //var doc = Application.DocumentManager.MdiActiveDocument;
            //Point3d start = new Point3d();
            //Point3d end = new Point3d(2, 1, -1);
            //double bulge = 1;
            //
            //start = new Point3d(-1, 1, 0);
            //end = new Point3d(1, 1, -1);
            //CadPlugin.Common.CADHelixHelper.DrawHelix(doc, start, end, bulge);
            //
            //start = new Point3d(-1, 1, 0);
            //end = new Point3d(1, 1, -1);
            //CadPlugin.Common.CADHelixHelper.DrawHelix(doc, start, end, bulge/2);
            //
            //start = new Point3d(-1, 1, 0);
            //end = new Point3d(1, 1, -1);
            //CadPlugin.Common.CADHelixHelper.DrawHelix(doc, start, end, -bulge);
            //
            //start = new Point3d(-1, 1, 0);
            //end = new Point3d(1, 2, -13);
            //CadPlugin.Common.CADHelixHelper.DrawHelix(doc, start, end, -bulge);

        }
    }
}
