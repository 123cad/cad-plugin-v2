﻿using CadPlugin.Common;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin_RunTester_CAD.Tests.Common
{
    [TestFixture]
    class CADPolylineHelperTests
    {

        static PolylineVertex<Point2d> Create(double x, double y, double bulge = 0)
        {
            return new PolylineVertex<Point2d>(new Point2d(x, y), bulge);
        }
        class CADPolylineData_Line
        {
            #region Parameters
            public static IEnumerable<TestCaseData> TestCaseParameters_2Points
            {
                get
                {
                    yield return Parameters_1();
                    yield return Parameters_2();
                    yield return Parameters_3();
                    yield return Parameters_4();
                    yield return Parameters_5();
                }
            }
            static TestCaseData Parameters_1()
            {
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0),
                        Create(1, 0)
                    };
                double[] parameters = new double[] { 0.6 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0),
                        Create(0.6, 0),
                        Create(1, 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            static TestCaseData Parameters_2()
            {
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0),
                        Create(1, 0)
                    };
                double[] parameters = new double[] { 0.6, 1.2 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0),
                        Create(0.6, 0),
                        Create(1, 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            static TestCaseData Parameters_3()
            {
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0),
                        Create(Math.Sqrt(2), 0)
                    };
                double[] parameters = new double[] { 0.5 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0),
                        Create(Math.Sqrt(2) / 2, 0),
                        Create(Math.Sqrt(2), 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            static TestCaseData Parameters_4()
            {
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0),
                        Create(1, 1)
                    };
                double[] parameters = new double[] { 0.5, 1.2 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0),
                        Create(0.5, 0.5),
                        Create(1, 1),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            static TestCaseData Parameters_5()
            {
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0),
                        Create(0.5, 0),
                        Create(1, 0)
                    };
                double[] parameters = new double[] { 1 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0),
                        Create(0.5, 0),
                        Create(1, 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            #endregion

            #region Distance
            public static IEnumerable<TestCaseData> TestCaseDistance_2Points
            {
                get
                {
                    yield return Distance_1();
                    yield return Distance_2();
                    yield return Distance_3();
                    yield return Distance_4();
                    yield return Distance_5();
                }
            }
            static TestCaseData Distance_1()
            {
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0),
                        Create(2.5, 0)
                    };
                double[] parameters = new double[] { 1.25 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0),
                        Create(1.25, 0),
                        Create(2.5, 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            static TestCaseData Distance_2()
            {
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0),
                        Create(1, 0)
                    };
                double[] parameters = new double[] { 0.6, 1.2 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0),
                        Create(0.6, 0),
                        Create(1, 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            static TestCaseData Distance_3()
            {
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0),
                        Create(Math.Sqrt(2), 0)
                    };
                double[] parameters = new double[] { Math.Sqrt(2) / 2 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0),
                        Create(Math.Sqrt(2) / 2, 0),
                        Create(Math.Sqrt(2), 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            static TestCaseData Distance_4()
            {
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0),
                        Create(1, 1)
                    };
                double[] parameters = new double[] { Math.Sqrt(2)/2, 2 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0),
                        Create(0.5, 0.5),
                        Create(1, 1),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            static TestCaseData Distance_5()
            {
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0),
                        Create(0.5, 0),
                        Create(1, 0)
                    };
                double[] parameters = new double[] { 0.5 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0),
                        Create(0.5, 0),
                        Create(1, 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            #endregion
        }
        class CADPolylineData_Arc
        {
            #region Parameters
            public static IEnumerable<TestCaseData>  TestCaseParameters_Arc_2Points
            {
                get
                {
                    yield return Parameter_2Points_1();
                    yield return Parameter_2Points_2();
                }
            }
            public static IEnumerable<TestCaseData> TestCaseParameters_Arc_3Points
            {
                get
                {
                    yield return Parameter_3Points_1();
                    yield return Parameter_3Points_2();
                    yield return Parameter_3Points_3();
                }
            }
            static TestCaseData Parameter_2Points_1()
            {
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0, 1),
                        Create(1, 0)
                    };
                double[] parameters = new double[] { 0.5 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0, 0.4142),
                        Create(0.5, -0.5, 0.4142),
                        Create(1, 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            static TestCaseData Parameter_2Points_2()
            {
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0, 1),
                        Create(1, 0)
                    };
                double[] parameters = new double[] { 0.25 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0, 0.19891),
                        Create(0.5 - Math.Sqrt(2) / 4, - Math.Sqrt(2) / 4, 0.66817),
                        Create(1, 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            static TestCaseData Parameter_3Points_1()
            {
                // Half circle (start to end), 3 start points (1 in the middle).
                // Adding 1 break point, on 0.25.
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0, 0.4141),
                        Create(0.5, -0.5, 0.4141),
                        Create(1, 0)
                    };
                double[] parameters = new double[] { 0.5 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0, 0.19891),
                        Create(0.5 - Math.Sqrt(2) / 4, -Math.Sqrt(2) / 4, 0.198961),
                        Create(0.5, -0.5, 0.4142),
                        Create(1, 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            static TestCaseData Parameter_3Points_2()
            {
                // Half circle (start to end), 3 start points (1 in the middle).
                // Adding 2 break points, on 0.25 and 0.75.
                // Bulge changes for all.
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0, 0.4141),
                        Create(0.5, -0.5, 0.4141),
                        Create(1, 0)
                    };
                double[] parameters = new double[] { 0.5, 1.5 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0, 0.19891),
                        Create(0.5 - Math.Sqrt(2) / 4, -Math.Sqrt(2) / 4, 0.19891),
                        Create(0.5, -0.5, 0.19891),
                        Create(0.5 + Math.Sqrt(2) / 4, -Math.Sqrt(2) / 4, 0.19891),
                        Create(1, 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            static TestCaseData Parameter_3Points_3()
            {
                // Half circle (start to end), 3 start points (1 in the middle).
                // Adding 1 break point, on 0.5 (same position as middle point - seee what happens).
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0, 0.4141),
                        Create(0.5, -0.5, 0.4141),
                        Create(1, 0)
                    };
                double[] parameters = new double[] { 0.5 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0, 0.19891),
                        Create(0.5 - Math.Sqrt(2) / 4, -Math.Sqrt(2) / 4, 0.19891),
                        Create(0.5, -0.5, 0.4142),
                        Create(1, 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            #endregion
            #region Distances
            public static IEnumerable<TestCaseData> TestCaseDistance_Arc_2Points
            {
                get
                {
                    yield return Distance_2Points_1();
                    yield return Distance_2Points_2();
                }
            }
            public static IEnumerable<TestCaseData> TestCaseDistance_Arc_3Points
            {
                get
                {
                    yield return Distance_3Points_1();
                    yield return Distance_3Points_2();
                    yield return Distance_3Points_3();
                }
            }
            static TestCaseData Distance_2Points_1()
            {
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0, 1),
                        Create(1, 0)
                    };
                double[] parameters = new double[] { 0.785398 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0, 0.4142),
                        Create(0.5, -0.5, 0.4142),
                        Create(1, 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            static TestCaseData Distance_2Points_2()
            {
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0, 1),
                        Create(1, 0)
                    };
                double[] parameters = new double[] { 0.3927 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0, 0.19891),
                        Create(0.5 - Math.Sqrt(2) / 4, - Math.Sqrt(2) / 4, 0.66817),
                        Create(1, 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            static TestCaseData Distance_3Points_1()
            {
                // Half circle (start to end), 3 start points (1 in the middle).
                // Adding 1 break point, on 0.25.
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0, 0.4141),
                        Create(0.5, -0.5, 0.4141),
                        Create(1, 0)
                    };
                double[] parameters = new double[] { 0.3927 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0, 0.19891),
                        Create(0.5 - Math.Sqrt(2) / 4, -Math.Sqrt(2) / 4, 0.198961),
                        Create(0.5, -0.5, 0.4142),
                        Create(1, 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            static TestCaseData Distance_3Points_2()
            {
                // Half circle (start to end), 3 start points (1 in the middle).
                // Adding 2 break points, on 0.25 and 0.75.
                // Bulge changes for all.
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0, 0.4141),
                        Create(0.5, -0.5, 0.4141),
                        Create(1, 0)
                    };
                double[] parameters = new double[] { 0.3927, 1.1781 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0, 0.19891),
                        Create(0.5 - Math.Sqrt(2) / 4, -Math.Sqrt(2) / 4, 0.19891),
                        Create(0.5, -0.5, 0.19891),
                        Create(0.5 + Math.Sqrt(2) / 4, -Math.Sqrt(2) / 4, 0.19891),
                        Create(1, 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }
            static TestCaseData Distance_3Points_3()
            {
                // Half circle (start to end), 3 start points (1 in the middle).
                // Adding 1 break point, on 0.5 (same position as middle point - seee what happens).
                PolylineVertex<Point2d>[] vertices = new PolylineVertex<Point2d>[]
                    {
                        Create(0, 0, 0.4141),
                        Create(0.5, -0.5, 0.4141),
                        Create(1, 0)
                    };
                double[] parameters = new double[] { 0.3927 };
                PolylineVertex<Point2d>[] result = new PolylineVertex<Point2d>[]
                {
                        Create(0, 0, 0.19891),
                        Create(0.5 - Math.Sqrt(2) / 4, -Math.Sqrt(2) / 4, 0.19891),
                        Create(0.5, -0.5, 0.4142),
                        Create(1, 0),
                };
                return new TestCaseData(vertices, parameters, result);
            }

            #endregion

        }
        /// <summary>
        /// Used for easier debugging of test - complex data types are hard to check for fields.
        /// </summary>
        /// <param name="expected"></param>
        /// <param name="calculated"></param>
        private void AssertEqual(IEnumerable<PolylineVertex<Point2d>> expected, IEnumerable<PolylineVertex<Point2d>> calculated)
        {
            Assert.AreEqual(expected.Count(), calculated.Count());
            var en1 = expected.GetEnumerator();
            var en2 = calculated.GetEnumerator();
            int index = 0;
            while (en1.MoveNext() && en2.MoveNext())// && just to have all in 1 line, collection are equal length
            {
                var p1 = en1.Current;
                var p2 = en2.Current;

                Assert.AreEqual(p1, p2, $"Not equal at index [{index}]");
                index++;
            }
        }

        [TestCaseSource(typeof(CADPolylineData_Line), nameof(CADPolylineData_Line.TestCaseParameters_2Points))]
        public void SplitPolylineAtParameters_Line(IEnumerable<PolylineVertex<Point2d>> vertices, IEnumerable<double> parameters, IEnumerable<PolylineVertex<Point2d>> result)
        {
            var r = CADPolylineHelper.SplitPolylineAtParameters(vertices, parameters.ToArray()).ToList();
            AssertEqual(result, r);
        }
        [TestCaseSource(typeof(CADPolylineData_Arc), nameof(CADPolylineData_Arc.TestCaseParameters_Arc_2Points))]
        public void SplitPolylineAtParameters_Arc_2Points(IEnumerable<PolylineVertex<Point2d>> vertices, IEnumerable<double> parameters, IEnumerable<PolylineVertex<Point2d>> result)
        {
            var r = CADPolylineHelper.SplitPolylineAtParameters(vertices, parameters.ToArray()).ToList();
            AssertEqual(result, r);
        }
        [TestCaseSource(typeof(CADPolylineData_Arc), nameof(CADPolylineData_Arc.TestCaseParameters_Arc_3Points))]
        public void SplitPolylineAtParameters_Arc_3Points(IEnumerable<PolylineVertex<Point2d>> vertices, IEnumerable<double> parameters, IEnumerable<PolylineVertex<Point2d>> result)
        {
            var r = CADPolylineHelper.SplitPolylineAtParameters(vertices, parameters.ToArray()).ToList();
            AssertEqual(result, r);
        }

        [TestCaseSource(typeof(CADPolylineData_Line), nameof(CADPolylineData_Line.TestCaseDistance_2Points))]
        public void SplitPolylineAtDistance_Line(IEnumerable<PolylineVertex<Point2d>> vertices, IEnumerable<double> parameters, IEnumerable<PolylineVertex<Point2d>> result)
        {
            var r = CADPolylineHelper.SplitPolylineAtDistances(vertices, parameters.ToArray()).ToList();
            AssertEqual(result, r);
        }
        [TestCaseSource(typeof(CADPolylineData_Arc), nameof(CADPolylineData_Arc.TestCaseDistance_Arc_2Points))]
        public void SplitPolylineAtDistance_Arc_2Points(IEnumerable<PolylineVertex<Point2d>> vertices, IEnumerable<double> parameters, IEnumerable<PolylineVertex<Point2d>> result)
        {
            var r = CADPolylineHelper.SplitPolylineAtDistances(vertices, parameters.ToArray()).ToList();
            AssertEqual(result, r);
        }
        [TestCaseSource(typeof(CADPolylineData_Arc), nameof(CADPolylineData_Arc.TestCaseDistance_Arc_3Points))]
        public void SplitPolylineAtDistance_Arc_3Points(IEnumerable<PolylineVertex<Point2d>> vertices, IEnumerable<double> parameters, IEnumerable<PolylineVertex<Point2d>> result)
        {
            var r = CADPolylineHelper.SplitPolylineAtDistances(vertices, parameters.ToArray()).ToList();
            AssertEqual(result, r);
        }


        class InterpolationData
        {
            public static IEnumerable<TestCaseData> TestData_1BreakPoint
            {
                get
                {
                    List<Point3d> result;
                    double startZ = 1.5;
                    double deltaZ = 1;
                    double endZ = startZ - deltaZ;
                    double interpolation = 1.0 / 2; // 1 Break point.
                    double bulge = 1;
                    result = new List<Point3d>()
                    {
                        new Point3d(1,0,startZ),
                        new Point3d(1.5, -0.5, startZ - deltaZ * interpolation),
                        new Point3d(2, 0, startZ - deltaZ)
                    };
                    yield return new TestCaseData(result.First().GetAs2d(), result.Last().GetAs2d(), bulge, startZ, endZ, interpolation, result);
                }
            }
            public static IEnumerable<TestCaseData> TestData_2BreakPoints
            {
                get 
                {
                    List<Point3d> result;
                    double startZ = 1.5;
                    double deltaZ = 1;
                    double endZ = startZ - deltaZ;
                    double interpolation = 1.0 / 3; // 2 break points
                    double bulge = 1;
                    result = new List<Point3d>()
                    {
                        new Point3d(1, 0, startZ),
                        new Point3d(1 + 0.25, 0 - 0.433, startZ - deltaZ * interpolation),
                        new Point3d(1 + 0.75, 0 - 0.433, startZ - deltaZ * interpolation * 2),
                        new Point3d(2, 0, startZ - deltaZ)
                    };
                    yield return new TestCaseData(result.First().GetAs2d(), result.Last().GetAs2d(), bulge, startZ, endZ, interpolation, result);

                    bulge = 0.33;
                    result = new List<Point3d>()
                    {
                        new Point3d(1, 0, startZ),
                        new Point3d(1 + 0.3228, 0 - 0.1461, startZ - deltaZ * interpolation),
                        new Point3d(1 + 0.6771, 0 - 0.1461, startZ - deltaZ * interpolation * 2),
                        new Point3d(2, 0, startZ - deltaZ)
                    };
                    yield return new TestCaseData(result.First().GetAs2d(), result.Last().GetAs2d(), bulge, startZ, endZ, interpolation, result);
                }
            }

        }

        [TestCaseSource(typeof(InterpolationData), nameof(InterpolationData.TestData_1BreakPoint))]
        public void InterpolateTest_1BreakPoint_Create1ExtraPoint(Point2d start, Point2d end, double bulge, double startZ, double endZ, double interpolationParameter, List<Point3d> result)
        {

            var res = CADPolylineHelper.Interpolate(start, end, bulge, startZ, endZ, interpolationParameter);
            Assert.AreEqual(result.Count, res.Count());
            for (int i = 0; i < result.Count; i++)
            {
                Assert.That(result[i], Is.EqualTo(res.ElementAt(i)).Using(CadPlugin.Common.Comparers.CADComparer.Get<Point3d>()));
            }
        }
        [TestCaseSource(typeof(InterpolationData), nameof(InterpolationData.TestData_2BreakPoints))]
        public void InterpolateTest_2BreakPoints_Create1ExtraPoint(Point2d start, Point2d end, double bulge, double startZ, double endZ, double interpolationParameter, List<Point3d> result)
        {
            var res = CADPolylineHelper.Interpolate(start, end, bulge, startZ, endZ, interpolationParameter);
            Assert.AreEqual(result.Count, res.Count());
            for (int i = 0; i < result.Count; i++)
            {
                Assert.That(result[i], Is.EqualTo(res.ElementAt(i)).Using(CadPlugin.Common.Comparers.CADComparer.Get<Point3d>()), $"at [{i}]");
            }

            ////var p = CADPolylineHelper.Interpolate(start, end, bulge, startZ, deltaZ, 0.05);
            //
            //var doc = Application.DocumentManager.MdiActiveDocument;
            //using (TransactionWrapper tw = doc.StartTransaction())
            //{
            //    var pl = tw.CreateEntity<Polyline3d>();
            //    pl.AddPoints(res.ToArray());
            //
            //    //pl = tw.CreateEntity<Polyline3d>();
            //    //pl.AddPoints(p.ToArray());
            //
            //    // Do commit even for readonly operations.
            //    tw.Commit();
            //}

        }
    }
}
