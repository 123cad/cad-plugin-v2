﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin_RunTester_CAD.Tests.Common.Extensions
{
    [TestFixture]
    class CADDatabaseExtensionsTests
    {

        private static Document doc = Application.DocumentManager.MdiActiveDocument;
        private static Database db = doc.Database;
        private static int TotalNumberOfEntities = 5;
        private static int NumberOfPoints = 2;
        private static int NumberOfCircles = 1;
        [SetUp]
        public void PrepareEntities()
        {
            if (NumberOfPoints + NumberOfCircles > TotalNumberOfEntities)
                throw new System.Exception("More points than entities.");
            int count = 3;
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                for (int i = 0; i < TotalNumberOfEntities; i++)
                {
                    if (i < NumberOfPoints)
                        tr.CreateEntity<DBPoint>(btr);
                    else if (i < NumberOfPoints + NumberOfCircles)
                        tr.CreateEntity<Circle>(btr);
                    else
                        tr.CreateEntity<Polyline>(btr);
                }

                // Do commit even for readonly operations.
                tr.Commit();
            }
        }
        [Test]
        public void GetAllEntities_TransactionExists_FetchEntities()
        {

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

                var l = db.GetAllEntities(tr);
                Assert.AreEqual(TotalNumberOfEntities, l.Count());

                // Do commit even for readonly operations.
                tr.Commit();
            }
            db.DeleteAllEntities();

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

                var l = db.GetAllEntities(tr);
                Assert.AreEqual(0, l.Count(), "Not all entities are deleted!");

                // Do commit even for readonly operations.
                tr.Commit();
            }


        }
        [Test]
        public void GetAllEntities_TransactionDoesntExist_FetchObjectIds()
        {
            IEnumerable<ObjectId> oids = db.GetAllEntities();
            Assert.AreEqual(TotalNumberOfEntities, oids.Count());

            db.DeleteAllEntities();
            oids = db.GetAllEntities();
            Assert.AreEqual(0, oids.Count());
        }

        [Test]
        public void GetAllEntities_OfSpecificType_Pass()
        {

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                var l  = db.GetAllEntities<Entity>(tr);
                Assert.AreEqual(TotalNumberOfEntities, l.Count());

                l = db.GetAllEntities<DBPoint>(tr);
                Assert.AreEqual(NumberOfPoints, l.Count());

                l = db.GetAllEntities<Circle>(tr);
                Assert.AreEqual(NumberOfCircles, l.Count());

                l = db.GetAllEntities<MText>(tr);
                Assert.AreEqual(0, l.Count());

                // Do commit even for readonly operations.
                tr.Commit();
            }
            db.DeleteAllEntities();

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

                var l = db.GetAllEntities<Entity>(tr);
                Assert.AreEqual(0, l.Count());

                // Do commit even for readonly operations.
                tr.Commit();
            }

        }

    }
}
