﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Diagnostics;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin_RunTester_CAD.Tests.Common.Extensions
{
    [TestFixture]
    class XrecordReaderExtensionsTests
    {

        private static Document doc = Application.DocumentManager.MdiActiveDocument;
        private static Database db = doc.Database;
        private static int TotalNumberOfEntities = 500;
        
        [Test]
        public void GetAllAs_Basic()
        {
            ObjectId oid;
            string entryName = "UnitTest_XrecordReader";
            using (TransactionWrapper tw = doc.StartTransaction())
            {
                var xrec = tw.NOD.OpenOrCreateXrecord(tw, "UnitTest_XrecordReader");
                ResultBufferBuilder b = new ResultBufferBuilder();
                IEnumerable<int> list = Enumerable.Range(0, TotalNumberOfEntities);
                b.Add(list.ToArray());
                xrec.SetData(b);
                oid = xrec.ObjectId;
                // Do commit even for readonly operations.
                tw.Commit();
            }



            using (TransactionWrapper tw = doc.StartTransaction())
            {
                var xrec = tw.NOD.OpenOrCreateXrecord(tw, "UnitTest_XrecordReader");
                XrecordReader rd = new XrecordReader(xrec);

                Stopwatch sw2 = Stopwatch.StartNew();
                sw2.Restart();
                rd.GetAllAs<int>();
                sw2.Stop();
                Stopwatch sw1 = Stopwatch.StartNew();
                for (int i = 0; i < TotalNumberOfEntities; i++)
                {
                    rd.GetInt(i);
                }
                sw1.Stop();

                //Console.WriteLine($"Time1: {time1} and Time2: {time2}");

                // Do commit even for readonly operations.
                tw.Commit();
            }


        }

    }
}
