﻿#define RUN_GROUP
#define AUTOCLOSE
#if RUN_GROUP
//#undef AUTOCLOSE
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnitLite;
using System.IO;
using System.Threading;
using System.Reflection;
using System.Diagnostics;
using NUnit.Framework;
using System.Windows.Threading;


#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices.Core;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif


// This line is not mandatory, but improves loading performances
[assembly: CommandClass(typeof(CadPlugin_RunTester_CAD.myCommands))]


namespace CadPlugin_RunTester_CAD
{
    public static class TestHelper
    {

        /*
        [Category(TestHelper.RunGroupName)]
        */
        /// <summary>
        /// Find this attribute to see which tests are run when RunSingleGroup is true.
        /// </summary>
        public const string RunGroupName = "ONLYME";

        private static Dispatcher mainThread;

        public static void Initialize()
        {
            mainThread = Dispatcher.CurrentDispatcher;
        }
        public static void RunOnUIThread(Action a)
        {
            mainThread.Invoke(a);
        }
        public static T RunOnUIThread<T>(Func<T> f) where T : class
        {
            T res = null;
            Action a = () => res = f();
            mainThread.Invoke(a);
            return res;
        }
    }

    public class myCommands
    {
        /// <summary>
        /// NUnitLite outputs all results to this file (in Configuration folder).
        /// </summary>
        public static string NUnitLiteResultsFileName = "NUnitLite Results.xml";
        /// <summary>
        /// Converted NUnitLite output file to html file.
        /// </summary>
        public static string NUnitLiteHtmlFileName = "ReportUnit_generated.html";

        [CommandMethod("RunNUnitTests", CommandFlags.Session)]
        public void RunTests()
        {
            TestHelper.Initialize();
            int testReturnCode = -11111;
            Thread thread = new Thread(new ThreadStart(() =>
            {
                string directory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string nunitResultsFile = directory + "\\" + NUnitLiteResultsFileName;
                string[] nUnitArgs = new string[]
                {
                    //"--trace=verbose", // Outputs additional log file.
                    "--result=" + nunitResultsFile,
                    //"--wait" // User must press enter to continue, at the end of tests.
#if RUN_GROUP
                    "--where=cat==" + TestHelper.RunGroupName
#endif
                };


                Stopwatch sw = Stopwatch.StartNew();
                // - Returns count of failed tests.
                // -4 = no tests found
                testReturnCode = new AutoRun().Execute(nUnitArgs);
                sw.Stop();

                if (!File.Exists(nunitResultsFile))
                    throw new FileNotFoundException("NUnitLie Results not created!");

                var doc = Application.DocumentManager.MdiActiveDocument;
                //using (var v = doc.LockDocument()) // Not sure if needed, but doesn't bother us.
                // {
                var ed = doc.Editor;
                //ed.WriteMessage($"Tests are finished! \nResults can be found in {nunitResultsFile}");

                Action<Color, string> writeResultColor = (i, j) => { };
                TestRunner runner = TestRunner.Bricscad;
                string msg = $"Tests have been run with ";
#if BRICSCAD
                runner = TestRunner.Bricscad;
                msg += "BRICSCAD";
#endif
#if AUTOCAD
                runner = TestRunner.Autocad;
                msg += "AUTOCAD";
#if ACADCORECONSOLE
                runner = TestRunner.AutocadConsole;
                msg += " CORE";
#endif
#endif
                TestResultsData resData = new TestResultsData()
                {
                    ElapsedMilisecond = sw.ElapsedMilliseconds,
                    Runner = runner,
                    ErrorCode = testReturnCode
                };
                if (testReturnCode == 0)
                    resData.Result = TestResult.Passed;
                else if (testReturnCode > 0)
                    resData.Result = TestResult.Failed;
                else
                    resData.Result = TestResult.OtherError;
                TestResultsBox.ShowDialog(resData);

                //if (testReturnCode != 0)
                {
                    var task = Task.Factory.StartNew(() =>
                    {
                        string resultHtml = CreateHtmlReport(nunitResultsFile, directory);
                        return resultHtml;
                    }).ContinueWith(t =>
                        Process.Start(t.Result)
                    );
                    task.Wait();
                }

#if AUTOCLOSE
#if BRICSCAD
            doc.CloseAndDiscard();
#endif
            // Doesn't
            Application.Quit();
#if BRICSCAD
            Process.GetCurrentProcess().Kill();

#endif
            //Process.GetCurrentProcess().Close();
#endif

                /* Check result of Execute method (it seems it says number of failing tests) - we can use this 
                 * Add result converter
                 * Test difference between AutocadConsole and full Autocad (unittests we should run in console, because we don't need visual results)
                 * OK Add Bricscad support
                 * OK Check if question is needed at the end (like should results file be opened), or just close test and open results (or open results if there are failed tests)
                 * OK Show this in message box (like all tests passed), or number of failed test
                 * OK Add to TrustedLocation so .dll is loaded to Autocad without prompt
                 * OK Check everything with Project runners (acad/bcad)
                 * OK write some sample tests and run them
                 */

            }));
            thread.Start();
        }
        /// <summary>
        /// Creates html file from nunit results file and returns full path.
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        private string CreateHtmlReport(string inputNUnitXmlFile, string resultsDirectory)
        {
            string binPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string outputHtml = resultsDirectory + "/" + NUnitLiteHtmlFileName;
            binPath = Directory.GetParent(binPath).FullName;
            string generator = binPath + "/ReportUnit.exe";

            using (Process process = new Process())
            {
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.CreateNoWindow = true;

                process.StartInfo.FileName = generator;

                string location = @"C:\Users\Dev\Documents\123CAD\123CAD Projects\Production\Publish\CAD-plugin V2\";
                string args = "" +
                    $" \"{inputNUnitXmlFile}\"" +
                    $" \"{outputHtml}\"" +
                    $" \"{location}\""
                    ;
                process.StartInfo.Arguments = args;

                process.Start();
            }
            return outputHtml;
        }
    }
}
