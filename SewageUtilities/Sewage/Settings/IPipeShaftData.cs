﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyUtilities.Geometry;

namespace CadPlugin.Sewage.Create.Settings
{
	//NOTE Exists only to prevent anyone to change StartShaft/EndShaft manually.
	// It has to be done using Toggle_ShaftCreate method.

	public interface IPipeShaftData
	{ 
		IPipeData Pipe { get; }

		IShaftData StartShaft { get; }

		IShaftData EndShaft { get; }
		/// <summary>
		/// Indicates if start point has been assigned.
		/// </summary>
		 bool StartPointSet { get; set;  }

		/// <summary>
		/// Indicates if end point has been assigned.
		/// </summary>
		bool EndPointSet { get; set; }
		/// <summary>
		/// If StartShaft exists, StartPoint is CenterPipeOutPoint (Z is pipe_out height) - otherwise is StartPipePoint.
		/// </summary>
		Point3d StartPoint { get; set; }
		/// <summary>
		/// If EndShaft exists, EndPoint is CenterPipeInPoint (Z is pipe_in height) - otherwise is EndPipePoint.
		/// </summary>
		Point3d EndPoint { get; set; }
		

		void ToggleStartShaftCreate(bool create);
		void ToggleEndShaftCreate(bool create);

		/// <summary>
		/// Saves current start shaft data, so it can be restored later.
		/// </summary>
		void SaveCurrentStartShaft();

		/// <summary>
		/// Saves current end shaft data, so it can be restored later.
		/// </summary>
		void SaveCurrentEndShaft();

		/// <summary>
		/// Restores start shaft data if it exists, or sets new data if not.
		/// </summary>
		void RestoreStartShaft();
		/// <summary>
		/// Restores end shaft data if it exists, or sets new data if not.
		/// </summary>
		void RestoreEndShaft();

		/// <summary>
		/// Get flag indicating if start shaft can be restored.
		/// </summary>
		bool SavedStartShaftExists { get; }
		/// <summary>
		/// Get flag indicating if end shaft can be restored.
		/// </summary>
		bool SavedEndShaftExists { get; }

		PipeShaftData Clone();


		/// <summary>
		/// Use the slope in pipe data as minimum slope that pipe must have.
		/// </summary>
		bool ApplyMinPipeSlope { get; set; }

		/// <summary>
		/// Represents minimum pipe slope (used together with ApplyMinPipeSlope).
		/// </summary>
		double MinPipeSlope { get; set; }

		bool ApplyEndShaftPipeOutDelta { get; }
		bool ApplyStartShaftPipeOutDelta { get; }

	}
}

