﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyUtilities.Geometry;
using CadPlugin.Sewage.Persistence;

namespace CadPlugin.Sewage.Create.Settings
{ 
	/// <summary>
	/// Determines how pipe slope is calculated (one value is fixed, other is calculated).
	/// </summary>
	public enum PipeSlopeSource
	{
		/// <summary>
		/// Pipe slope is fixed (what user types), delta is calculated.
		/// </summary>
		PipeSlope, 
		/// <summary>
		/// Terrain to pipe out delta is fixed, pipe slope is calculated.
		/// </summary>
		PipeOutDelta
	}
	/// <summary>
	/// Holds data (pipe, start and end shaft (both are optional)).
	/// </summary>
	public class PipeShaftData : IPipeShaftData
	{
		// Dual members exist to provide different access.
		// Via interface, data can only be seen. Via class reference, 
		// data can also be set.

		private ShaftData __StartShaftData = null;
		private ShaftData __EndShaftData = null;
		public ShaftData StartShaftData
		{
			get
			{
				return __StartShaftData;
			}
			set
			{
				if (__StartShaftData != null)
					__StartShaftData.PipeOutHeightUpdate -= UpdateStartPipeOutHeight;
				__StartShaftData = value;
				if (value != null)
					__StartShaftData.PipeOutHeightUpdate += UpdateStartPipeOutHeight;
			}
		}
		public ShaftData EndShaftData
		{
			get
			{
				return __EndShaftData;
			}
			set
			{
				__EndShaftData = value;
			}
		}
		public PipeData PipeData { get; private set; }

		public IShaftData StartShaft { get { return StartShaftData; } }
		public IShaftData EndShaft { get { return EndShaftData; } }
		public IPipeData Pipe { get { return PipeData; } }
		
		private bool __ApplyMinPipeSlope = true;
		//private bool __ApplyMinDeltaShaftTerrainPipeOut = true;
		/// <summary>
		/// Use the slope in pipe data as minimum slope that pipe must have.
		/// </summary>
		public bool ApplyMinPipeSlope
		{
			get { return __ApplyMinPipeSlope; }
			set { __ApplyMinPipeSlope = value; }
		}
		
		public double MinPipeSlope { get; set; }

		/// <summary>
		/// Indicates if start point has been assigned.
		/// </summary>
		public bool StartPointSet { get; set; }

		/// <summary>
		/// Indicates if end point has been assigned.
		/// </summary>
		public bool EndPointSet { get; set; }
		/// <summary>
		/// If StartShaft exists, StartPoint is CenterPipeOutPoint (Z is pipe_out height) - otherwise is StartPipePoint.
		/// </summary>
		public Point3d StartPoint
		{
			get
			{
				return StartShaft != null ? StartShaft.CenterPipeOutPoint : PipeData.StartPoint;
			}
			set
			{
				if (StartShaft != null)
				{
					double delta = value.Z - StartShaftData.PipeOutHeight;
					StartShaftData.PipeOutHeight = value.Z;
					StartShaftData.BottomHeight += delta;
					StartShaftData.Center = new Point2d(value.X, value.Y);
				}
				else
					PipeData.StartPoint = value;
				StartPointSet = true;
			}
		}
		/// <summary>
		/// If EndShaft exists, EndPoint is CenterPipeInPoint (Z is pipe_in height) - otherwise is EndPipePoint.
		/// </summary>
		public Point3d EndPoint
		{
			get
			{
				return EndShaft != null ? new Point3d(EndShaftData.Center.X, EndShaftData.Center.Y, EndShaft.PipeOutHeight + EndShaft.InnerSlopeDeltaH) : PipeData.EndPoint;
			}
			set
			{
				if (EndShaft != null)
				{
					double delta = value.Z - EndShaftData.PipeOutHeight - EndShaft.InnerSlopeDeltaH;
					EndShaftData.PipeOutHeight = value.Z - EndShaft.InnerSlopeDeltaH;
					EndShaftData.BottomHeight += delta;
					EndShaftData.Center = new Point2d(value.X, value.Y);
				}
				else
					PipeData.EndPoint = value;
				EndPointSet = true;
			}
		}

		public bool ApplyEndShaftPipeOutDelta { get { return EndShaftData != null && EndShaftData.MinPipeOutDeltaUsed; } }
		public bool ApplyStartShaftPipeOutDelta { get { return StartShaftData != null && StartShaftData.MinPipeOutDeltaUsed; } }
		#region Save shafts state
			/*
			 * User can save current start/end shaft data, and restore it later.
			 */
			/// <summary>
			/// Saved data for start shaft.
			/// </summary>
		private ShaftData savedStartShaft = null;
		private Point3d savedPipeStartPoint;
		/// <summary>
		/// Saved data for end shaft.
		/// </summary>
		private ShaftData savedEndShaft = null;
		private Point3d savedPipeEndPoint;
		public bool SavedStartShaftExists { get { return savedStartShaft != null; } }
		public bool SavedEndShaftExists { get { return savedEndShaft != null; } }
		#endregion
		
		public PipeShaftData()
		{
			PipeData = new PipeData();
			MinPipeSlope = 0.05;
			DataPersistence.LoadLastMinValues(this);
		}
		
		//NOTE  StartPoint represents pipe out. When pipe out height is updated, 
		// this point has to be updated also.

		/// <summary>
		/// Performs needed operations when shaft has been changed.
		/// </summary>
		public void MoveNextShaft()
		{
			/*double pipeIn = PipeData.EndPoint.Z;
			if (EndShaft != null)
				// Not sure why is this?
				EndShaftData.PipeOutHeight = pipeIn - EndShaft.InnerSlopeDeltaH;
			if (StartShaft != null)
				StartShaftData.PipeOutHeightUpdate -= UpdateStartPipeOutHeight;
			StartShaftData = EndShaftData;
			if (StartShaftData != null)
				StartShaftData.PipeOutHeightUpdate += UpdateStartPipeOutHeight;
			if (EndShaft != null)
				EndShaftData = EndShaftData.Clone();
			else
				EndShaftData = new ShaftData();
			EndPointSet = false;
			PipeData.StartPoint = PipeData.EndPoint;*/

			// Clear saved data.
			savedStartShaft = null;
			savedEndShaft = null;
		}
		/// <summary>
		/// Toggles start shaft creation. No effect if start shaft is already being created.
		/// </summary>
		/// <param name="create"></param>
		public void ToggleStartShaftCreate(bool create)
		{
			// If no changes, return.
			if (StartShaft != null && create ||
				StartShaft == null && !create)
				return;
			if (StartShaft != null)
			{
				PipeData.StartPoint = StartShaft.CenterPipeOutPoint;
				StartShaftData = null;
			}
			else
			{
				StartShaftData = new ShaftData();
				DataPersistence.LoadLastStartShaft(StartShaftData);
			}
		}
		/// <summary>
		/// Toggles end shaft creation. No effect if start shaft is already being created.
		/// </summary>
		/// <param name="create"></param>
		public void ToggleEndShaftCreate(bool create)
		{
			// If no changes, return.
			if (EndShaft != null && create ||
				EndShaft == null && !create)
				return;
			if (EndShaft != null)
			{
				PipeData.EndPoint = EndShaft.CenterPipeOutPoint.Add(new Vector3d(0,0,EndShaft.InnerSlopeDeltaH));
				EndShaftData = null;
			}
			else
			{
				EndShaftData = new ShaftData();
				DataPersistence.LoadLastEndShaft(EndShaftData);
			}
			EndPointSet = false;
		}
		public void SaveCurrentStartShaft()
		{
			savedStartShaft = StartShaftData;
			savedPipeStartPoint = PipeData.StartPoint;
		}
		public void SaveCurrentEndShaft()
		{
			savedEndShaft = EndShaftData;
			savedPipeEndPoint = PipeData.EndPoint;
		}
		public void RestoreStartShaft()
		{
			if (savedStartShaft != null)
			{
				StartShaftData = savedStartShaft;
				PipeData.StartPoint = savedPipeStartPoint;
				savedStartShaft = null;
			}
			else
				StartShaftData = new ShaftData();
		}
		public void RestoreEndShaft()
		{
			if (savedEndShaft != null)
			{
				EndShaftData = savedEndShaft;
				PipeData.EndPoint = savedPipeEndPoint;
				savedEndShaft = null;
			}
			else
				EndShaftData = new ShaftData();
		}
		/// <summary>
		/// When pipe out height for start shaft is updated, 
		/// update start pipe height also (and shaft, if it exists).
		/// </summary>
		/// <param name="d"></param>
		private void UpdateStartPipeOutHeight(double d)
		{
			Point3d p;
			p = PipeData.StartPoint;
			PipeData.StartPoint = new Point3d(p.X, p.Y, d);
		}
		public PipeShaftData Clone()
		{
			PipeShaftData d = new PipeShaftData();
			d.PipeData = PipeData.Clone();
			if (StartShaft != null)
				d.StartShaftData = StartShaftData.Clone();
			if (EndShaft != null)
				d.EndShaftData = EndShaftData.Clone();
			d.StartPointSet = StartPointSet;
			d.EndPointSet = EndPointSet;

			return d;
		}
	}
}

