﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Sewage.Plannings
{
	class TextDisplayOrdered
	{

		/// <summary>
		/// Displayed shafts texts, in order of appearance.
		/// </summary>
		public IList<TextDisplaySingle> ShaftDisplayOrder { get; private set; }
		/// <summary>
		/// Displayed pipe texts, in order of appearance.
		/// </summary>
		public IList<TextDisplaySingle> PipeDisplayOrder { get; private set; }

		public TextDisplayOrdered()
		{
			ShaftDisplayOrder = new List<TextDisplaySingle>();
			PipeDisplayOrder = new List<TextDisplaySingle>();
		}
	}
}
