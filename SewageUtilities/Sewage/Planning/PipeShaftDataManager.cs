﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CadPlugin.Sewage.Create.Settings;
using MyUtilities.Geometry;
using CadPlugin.Sewage.Planning;

namespace CadPlugin.Sewage.Plannings
{
	public abstract partial class PipeShaftDataManager
	{
		//NOTE This class can have access to shaft data, and to set values.
		// All others don't have this ability. Interface is to hide setters.

		protected PipeShaftData __Data { get; set; }
		public virtual IPipeShaftData Data { get { return __Data; } }

		/// <summary>
		/// Terrain calculator used for calculation of z height.
		/// </summary>
		public virtual TerrainCalculator TerrainHeightCalculator { get; set; }

		public virtual PipeCalculation Calculation
		{
			get;
			set;
		}
		/// <summary>
		/// Indicates if previous pipe exists in history.
		/// </summary>
		/// <returns></returns>
		public bool PreviousDataExists
		{
			get
			{
				return dataHistory.Count > 0;
			}
		}
		/// <summary>
		/// Holds all previous PipeShaftData objects (last one is data for previous pipe/shaft).
		/// </summary>
		public IEnumerable<PipeShaftData> DataHistory { get { return dataHistory; } }
		protected List<PipeShaftData> dataHistory { get; set; }
		

		public PipeShaftDataManager()
		{
			__Data = new PipeShaftData();
			dataHistory = new List<PipeShaftData>();
		}
		public virtual void CreateStartShaft(bool create)
		{
			Data.ToggleStartShaftCreate(create);
		}

		public virtual void CreateEndShaft(bool create)
		{
			Data.ToggleEndShaftCreate(create);
		}
		public void ToggleSegmentsCreation(bool create)
		{
			__Data.PipeData.SegmentsUsed = create;
		}
		public void ToggleStepAngleUsed(bool used)
		{
			__Data.PipeData.AngleSteppingUsed = used;
		}
		/// <summary>
		/// Moves data to the next pipe: End shaft becomes start shaft, pipe and end shaft are new.
		/// </summary>
		[Obsolete("Use move next shaft instead!")]
		public void MoveToNextPipe()
		{
			bool firstPipe = PreviousDataExists;
			if (!Data.EndPointSet)
				throw new NotSupportedException("Unable to move to the next pipe. End point is not set!");
			dataHistory.Add(__Data.Clone());
			__Data.MoveNextShaft();
		}

		/// <summary>
		/// Moves to next pipe section (which concatenates to current one - upstream/downstream).
		/// </summary>
		public abstract void MoveNextShaft();

		/// <summary>
		/// Updates point which is being moved (end point for downstream, start point for upstream).
		/// If terrain is not provided, it is calculated with default calculator.
		/// </summary>
		public abstract void UpdateVariablePoint(Point3d point, double? terrainHeight = null);


		/// <summary>
		/// For inherited classes to perform action upon updating start point.
		/// </summary>
		protected virtual void StartPointUpdated() { }
		/// <summary>
		/// For inherited classes to perform action upon updating end point.
		/// </summary>
		protected virtual void EndPointUpdated() { }


		/// <summary>
		/// Sets new position for start point, with recalculation of data.
		/// </summary>
		/// <param name="position">Position with z as terrain height</param>
		public virtual void SetStartPoint(Point3d position)
		{
			if (Data.StartShaft != null)
			{
				double terrain = position.Z;
				UpdateShaft(__Data.StartShaftData, terrain, ShaftFields.Terrain);
				position = new Point3d(position.X, position.Y, __Data.StartShaftData.PipeOutHeight); 
			}
			Data.StartPoint = position;
			//SetEndPoint(position);
			StartPointUpdated();
		}

		/// <summary>
		/// Sets new position for end point, with keeping deltas.
		/// </summary>
		/// <param name="position">Z parameter is terrain height.</param>
		public void SetEndPoint(Point3d position)
		{
			if (Data.EndShaft != null)
			{
				double terrain = position.Z;
				UpdateEndShaft(terrain, ShaftFields.Terrain);

				// Z coordinate will anyway be updated on refresh.
			}
			Data.EndPoint = position;
			EndPointUpdated();
		}
		/// <summary>
		/// Sets point which is variable (not fixed - can be moved in segment);
		/// </summary>
		/// <param name="position"></param>
		public abstract void SetVariablePoint(Point3d position);


		/// <summary>
		/// Recalculates data with current position.
		/// </summary>
		public void Recalculate()
		{			
			RecalculatePoint();

			__Data.PipeData.StartPoint = Calculation.StartPointOffset;
			__Data.PipeData.EndPoint = Calculation.EndPointOffset;
			// This is needed in case when length is 0, and pipe slope is fixed.
			//if (__Data.ApplyMinDeltaShaftTerrainPipeOut)
				__Data.PipeData.Slope = MyUtilities.Helpers.Calculations.GetSlope(__Data.PipeData.StartPoint, __Data.PipeData.EndPoint);
			Data.StartPoint = Calculation.StartPoint;
			Data.EndPoint = Calculation.EndPoint;
			__Data.PipeData.Direction = Calculation.PipeVector.GetUnitVector();
			if (__Data.PipeData.SegmentsUsed)
			{
				double totalLength = Calculation.StartPointOffset.GetVectorTo(Calculation.EndPointOffset).Length;
				double innerSegmentsLength = totalLength - 2 * __Data.PipeData.StartEndSegmentLength;
				__Data.PipeData.SegmentCount = (int)Math.Round(innerSegmentsLength / __Data.PipeData.SegmentLength);
			}
		}
		/// <summary>
		/// Recalculates variable point by subclass.
		/// </summary>
		protected abstract void RecalculatePoint();

	}
}

