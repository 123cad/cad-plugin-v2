﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CadPlugin.Sewage.Create.Settings;
using MyUtilities.Geometry;


namespace CadPlugin.Sewage.Plannings
{
	/// <summary>
	/// Draws pipe with shafts. Shafts are represented as offsets,
	/// so pipe will be drawn from start point + offset to end point - offset.
	/// </summary>
	public abstract class OffsetCalculator : PipeCalculation
	{
		/// <summary>
		/// Gets offset at which start point is drawn.
		/// </summary>
		public double StartOffset { get; private set; }
		/// <summary>
		/// Gets offset at end point.
		/// </summary>
		public double EndOffset { get; private set; }

		public OffsetCalculator(double startOffset, double endOffset):base()
		{
			StartOffset = startOffset;
			EndOffset = endOffset;
		}
		
		/// <summary>
		/// When angle stepping is used, update start/end (upstream/downstream) so angle to previous pipe
		/// segment is multiplicate of step.
		/// </summary>
		protected abstract void AdjustAngleStep(PipeShaftDataManager mgr, ref Point3d start, ref Point3d end);
		/*
		/// <summary>
		/// Sets new end point.
		/// </summary>
		public override void UpdatePosition(PipeShaftDataManager mgr, TerrainCalculator terrainCalc)
		{
			IPipeShaftData data = mgr.Data;
			Point3d start = data.StartPoint;
			Point3d end = data.EndPoint;
			AdjustAngleStep(mgr, ref start, ref end);

			Point2d center2d = new Point2d(start.X, start.Y);
			Point2d mouse2d = new Point2d(end.X, end.Y);
			Vector2d direction2d = center2d.GetVectorTo(mouse2d);
			double mouseDistance2d = direction2d.Length;
			
			Vector2d direction2dUnit = direction2d.GetUnitVector();
			Point2d startPoint = GetStartPoint(direction2dUnit, center2d);
			Point2d endPoint = GetEndPoint(direction2dUnit, mouse2d);


			Vector2d pipeXY = startPoint.GetVectorTo(endPoint);
			// Length which includes offset.
			double adjustedLength = pipeXY.Length;
			if (adjustedLength < StartOffset + EndOffset)
			{
				// Shafts can't overlap.
				pipeXY = direction2dUnit.Multiply(StartOffset + EndOffset);
			}
			double slope = data.MinPipeSlope;
			// If slope is to be calculated from pipe out delta.
			// First calculate delta, and then check with min slope.
			if (data.ApplyMinDeltaShaftTerrainPipeOut)
			{
				Point2d pipeEndPoint2d = startPoint.Add(pipeXY);
				double tempDelta = data.MinShaftDelta;// data.EndShaft.TerrainHeight - data.EndShaft.PipeOutHeight;
				// Because shaft height is considered to be fixed for in and out pipe.
				double terrainHeight = terrainCalc.GetHeight(mouse2d);// pipeEndPoint2d);
				double deltaH = start.Z - (terrainHeight - tempDelta + data.EndShaft.InnerSlopeDeltaH);
				slope = Common.Calculations.GetSlope(new Vector3d(pipeXY.X, pipeXY.Y, -deltaH));
			}
			if (data.ApplyMinPipeSlope)
			{
				if (slope < data.MinPipeSlope)
					slope = data.MinPipeSlope;
			}
			//((PipeData)data.Pipe).Slope = slope;
			Vector3d pipeVector = GetVectorWithSlope(pipeXY, slope);

			PipeVector = pipeVector;
			StartPoint = start;
			StartPointOffset = new Point3d(startPoint.X, startPoint.Y, start.Z);
			EndPointOffset = StartPointOffset.Add(pipeVector);
			// End point is on the same height as pipe end point, but moved for EndOffset value.
			Vector2d endOffset = direction2dUnit.Multiply(EndOffset);

			// NOTE Calculated end point XY must be equal to provided end point; only Z can be different.
			EndPoint = EndPointOffset.Add(new Vector3d(endOffset.X, endOffset.Y, 0));
			// System.Diagnostics.Debug.Assert(Math.Abs(EndPoint.X - newPoint.X) < 0.00001, "Calculated end point X in offsetCalc is moved!");
			// System.Diagnostics.Debug.Assert(Math.Abs(EndPoint.Y - newPoint.Y) < 0.00001, "Calculated end point Y in offsetCalc is moved!");
		}*/

		/// <summary>
		/// Adjusts start point with offset.
		/// </summary>
		/// <returns></returns>
		protected virtual Point2d GetStartPoint(Vector2d unit, Point2d start)
		{
			// To improve performance little bit.
			if (StartOffset == 0)
				return start;
			Vector2d offset = unit.Multiply(StartOffset);
			return start.Add(offset);
		}
		/// <summary>
		/// Adjusts end point, if needed.
		/// </summary>
		/// <param name="direction"></param>
		/// <param name="end"></param>
		/// <returns></returns>
		protected virtual Point2d GetEndPoint(Vector2d unit, Point2d end)
		{
			// To improve performance little bit.
			if (EndOffset == 0)
				return end;
			Vector2d offset = unit.Multiply(EndOffset);
			return end.Add(offset.Negate());
		}
	}
}

