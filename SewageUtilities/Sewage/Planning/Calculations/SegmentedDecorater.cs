﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CadPlugin.Sewage.Create.Settings;
using MyUtilities.Geometry;
using CadPlugin.Sewage.Planning.Calculations;

namespace CadPlugin.Sewage.Plannings
{
	/// <remarks>When line is calculated, it can be segmented, which matches Decorator pattern.</remarks>
	public abstract class SegmentedDecorater : PipeCalculation
	{
		public double SegmentLength { get; private set; }
		public double StartEndSegmentLength { get; private set; }
		public virtual PipeCalculation Calculation
		{
			get;
			private set;
		}
		public SegmentedDecorater(PipeCalculation calc, double segmentLength) : this(calc, segmentLength, 0)
		{

		}
		public SegmentedDecorater(PipeCalculation calc, double segmentLength, double startEndSegmentLength)
		{
			SegmentLength = segmentLength;
			StartEndSegmentLength = startEndSegmentLength;
			Calculation = calc;
		}


		/*public override void UpdatePosition(PipeShaftDataManager mgr, TerrainCalculator calc)
		{
			// Shaft has only one height - center of the shaft height. This height is used for pipe in and for pipe out.
			// If no delta is included in calculation, then small difference exists.
			
			IPipeShaftData data = mgr.Data;
			Calculation.UpdatePosition(mgr, calc);

			Vector3d pipeVector = Calculation.PipeVector;
			double length = pipeVector.Length;

			Vector3d segmentedPipeVector;
			Point3d endPointOffset;
			int segmentCount = GetLowerSegmentCount(length, SegmentLength, StartEndSegmentLength);// GetNumberOfSegments(length, SegmentLength, StartEndSegmentLength);
			SegmentedLengthIterator lengths = new SegmentedLengthIterator(SegmentLength, StartEndSegmentLength, segmentCount, segmentCount + 1);
			if (data.ApplyMinPipeSlope && !data.ApplyMinDeltaShaftTerrainPipeOut)// .SlopeSource == PipeSlopeSource.PipeSlope)
			{
				// Apply only min pipe slope.
				segmentedPipeVector = SegmentedCalculator.CalculatePointWithMinSlope(lengths, pipeVector);//, Calculation.StartPointOffset);
				//segmentedPipeVector = Calculation.StartPointOffset.GetVectorTo(pt);
				//double segmentedLength = segmentCount * SegmentLength + 2*StartEndSegmentLength;
				//segmentedPipeVector = pipeVector.GetUnitVector().Multiply(segmentedLength);
			}
			else 
			{
				if (data.EndShaft == null)
					throw new NotSupportedException("Fixed pipe out delta is supported only when end shaft exitst!");
				Point3d? calculatedPoint = null;
				double minDelta = data.MinShaftDelta - data.EndShaft.InnerSlopeDeltaH;
				double endRadius = data.EndShaft != null? data.EndShaft.Radius : 0 ;
				if (!data.ApplyMinPipeSlope)
					calculatedPoint = SegmentedCalculator.CalculatePointWithMinDelta(lengths, pipeVector, Calculation.StartPointOffset, calc, minDelta, endRadius);
				else
					calculatedPoint = SegmentedCalculator.CalculatePointWithMinSlopeAndDelta(lengths, pipeVector, Calculation.StartPointOffset, calc, mgr.Data.MinPipeSlope, minDelta, endRadius);
				//double delta = data.EndShaft.TerrainHeight - data.EndShaft.PipeOutHeight - data.EndShaft.InnerSlopeDeltaH;
				//Point3d? calculatedPoint = SegmentedCalculator.Calculate(Calculation.StartPointOffset, Calculation.PipeVector, segmentedLengths, calc, delta);

				segmentedPipeVector = Calculation.StartPointOffset.GetVectorTo(calculatedPoint.Value);
				//segmentedPipeVector = new Vector3d(pipe2d.X, pipe2d.Y, -delta);
			}
			// New end point of the pipe.
			endPointOffset = Calculation.StartPointOffset.Add(segmentedPipeVector);

			// Since pipe end point is moved, calculate how much to move end shaft.
			Vector3d deltaVector = Calculation.EndPointOffset.GetVectorTo(endPointOffset);

			// IMPORTANT: Set these to current instance, not decorated instance! 
			PipeVector = segmentedPipeVector;
			StartPointOffset = Calculation.StartPointOffset;
			EndPoint = Calculation.EndPoint.Add(deltaVector);
			EndPointOffset = endPointOffset;
			System.Diagnostics.Debug.WriteLine("end: " + EndPoint);
		}*/
		/// <summary>
		/// Returns segment count for first segmented length smaller than length.
		/// </summary>
		public static int GetLowerSegmentCount(double length, double segmentLength, double startEndSegmentsLength)
		{
			// Example: length = 9.6, segmentLength = 2. Returned values are (4 and 5).
			double segmentCountD = (length - startEndSegmentsLength * 2) / segmentLength;
			return (int)segmentCountD;
		}
		/// <summary>
		/// Calculates number of segments, in a given length (excluding start and end segment).
		/// </summary>
		public static int GetNumberOfSegments(double length, double segmentLength, double startEndSegmentLength)
		{
			double segmentsD = (length - startEndSegmentLength * 2) / segmentLength;
			int segmentCount = (int)segmentsD;
			if (segmentsD - segmentCount > 0.5)
				segmentCount++;
			return segmentCount;
		}
		
	}
}

