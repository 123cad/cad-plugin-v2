﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CadPlugin.Sewage.Create.Settings;
using MyUtilities.Geometry;

namespace CadPlugin.Sewage.Plannings
{
	/// <summary>
	/// Fixed direction considers only pipe (without offset).
	/// </summary>
	public class FixedDirectionCalculator : PipeCalculation
	{ 
		/// <summary>
		/// Fixed direction which every pipe must have.
		/// </summary>
		public Vector3d Direction { get; private set; }
		/// <summary>
		/// Fixed slope as tangens (positive slope = pipe goes down).
		/// </summary>
		public double FixedSlope { get; private set; }

		public FixedDirectionCalculator(Vector3d direction)
		{
			Direction = direction;
			Vector2d v = new Vector2d(direction.X, direction.Y);
			if (v.IsZeroLength())
				FixedSlope = direction.Z < 0 ? double.MaxValue : double.MinValue;
			else
				FixedSlope = -direction.Z / v.Length;
		}

		public override void UpdatePosition(PipeShaftDataManager mgr, TerrainCalculator terrainCalc)
		{
			// Make a projection point from mouse position on to direction line (determined by direction vector)
			// at 90 degrees, and calculate distance from projected point to start point.
			// This is done so only moving mouse upwards/downwards direction line length is actually changed.
			// Moving mouse on the line which cuts direction line by 90 deg doesn't change length.

			IPipeShaftData data = mgr.Data;
			Point2d point = new Point2d(data.EndPoint.X, data.EndPoint.Y);

			Point3d start = data.Pipe.StartPoint;
			Vector2d mouseXY = new Point2d(start.X, start.Y).GetVectorTo(new Point2d(point.X, point.Y));
			Vector2d directionXY = new Vector2d(Direction.X, Direction.Y);
			double angleXY = mouseXY.GetAngleRad(directionXY);
			// Length on direction vector.
			double projectedLengthXY = mouseXY.Length * Math.Cos(angleXY);

			// x-y plane vector of the pipe (needed is z coordinate only)
			Vector2d pipeXY = directionXY.GetUnitVector().Multiply(projectedLengthXY);
			Vector3d pipe = GetVectorWithSlope(pipeXY, FixedSlope);

			PipeVector = pipe;
			// StartPointOffset is not changed.
			EndPointOffset = data.Pipe.StartPoint.Add(pipe);
			EndPoint = EndPointOffset;
		}

	}
}

