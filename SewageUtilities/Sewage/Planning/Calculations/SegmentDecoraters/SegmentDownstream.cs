﻿using MyUtilities.Geometry;
using CadPlugin.Sewage.Create.Settings;
using CadPlugin.Sewage.Plannings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Sewage.Planning.Calculations.SegmentDecoraters
{
	class SegmentDownstream : SegmentedDecorater
	{
		public SegmentDownstream(PipeCalculation calc, double segmentLength) : base(calc, segmentLength)
		{
		}
		public SegmentDownstream(PipeCalculation calc, double segmentLength, double startEndSegmentLength) : base(calc, segmentLength, startEndSegmentLength)
		{
		}

		public override void UpdatePosition(PipeShaftDataManager mgr, TerrainCalculator calc)
		{
			// Shaft has only one height - center of the shaft height. This height is used for pipe in and for pipe out.
			// If no delta is included in calculation, then small difference exists.

			IPipeShaftData data = mgr.Data;
			Calculation.UpdatePosition(mgr, calc);

			Vector3d pipeVector = Calculation.PipeVector;
			double length = pipeVector.Length;

			Vector3d segmentedPipeVector;
			Point3d endPointOffset;
			int segmentCount = GetLowerSegmentCount(length, SegmentLength, StartEndSegmentLength);// GetNumberOfSegments(length, SegmentLength, StartEndSegmentLength);
			SegmentedLengthIterator lengths = new SegmentedLengthIterator(SegmentLength, StartEndSegmentLength, segmentCount, segmentCount + 1);
			if (data.ApplyMinPipeSlope && !data.ApplyEndShaftPipeOutDelta)// .SlopeSource == PipeSlopeSource.PipeSlope)
			{
				// Apply only min pipe slope.
				segmentedPipeVector = SegmentedCalculator.CalculatePointWithMinSlope(lengths, pipeVector);//, Calculation.StartPointOffset);
																										  //segmentedPipeVector = Calculation.StartPointOffset.GetVectorTo(pt);
																										  //double segmentedLength = segmentCount * SegmentLength + 2*StartEndSegmentLength;
																										  //segmentedPipeVector = pipeVector.GetUnitVector().Multiply(segmentedLength);
			}
			else
			{
				if (data.EndShaft == null)
					throw new NotSupportedException("Fixed pipe out delta is supported only when end shaft exitst!");
				Point3d? calculatedPoint = null;
				double minDelta = data.EndShaft.MinPipeOutDelta - data.EndShaft.InnerSlopeDeltaH;
				double endRadius = data.EndShaft != null ? data.EndShaft.Radius : 0;
				if (!data.ApplyMinPipeSlope)
					calculatedPoint = SegmentedCalculator.CalculatePointWithMinDelta(lengths, pipeVector, Calculation.StartPointOffset, calc, minDelta, endRadius);
				else
					calculatedPoint = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(lengths, pipeVector, Calculation.StartPointOffset, calc, mgr.Data.MinPipeSlope, minDelta, endRadius);
				//double delta = data.EndShaft.TerrainHeight - data.EndShaft.PipeOutHeight - data.EndShaft.InnerSlopeDeltaH;
				//Point3d? calculatedPoint = SegmentedCalculator.Calculate(Calculation.StartPointOffset, Calculation.PipeVector, segmentedLengths, calc, delta);

				segmentedPipeVector = Calculation.StartPointOffset.GetVectorTo(calculatedPoint.Value);
				//segmentedPipeVector = new Vector3d(pipe2d.X, pipe2d.Y, -delta);
			}
			// New end point of the pipe.
			endPointOffset = Calculation.StartPointOffset.Add(segmentedPipeVector);

			// Since pipe end point is moved, calculate how much to move end shaft.
			Vector3d deltaVector = Calculation.EndPointOffset.GetVectorTo(endPointOffset);

			// IMPORTANT: Set these to current instance, not decorated instance! 
			PipeVector = segmentedPipeVector;
			StartPoint = Calculation.StartPoint;
			StartPointOffset = Calculation.StartPointOffset;
			EndPoint = Calculation.EndPoint.Add(deltaVector);
			EndPointOffset = endPointOffset;
			System.Diagnostics.Debug.WriteLine("end: " + EndPoint);
		}
	}
}
