﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtilities.Geometry;
using CadPlugin.Sewage.Plannings;
using System.Diagnostics;
using CadPlugin.Sewage.Planning.Calculations.SegmentDecoraters;

namespace CadPlugin.Sewage.Planning.Calculations
{
	
	public class SegmentedCalculator
	{
		/// <summary>
		/// Updates provided pipe vector (extends or shrinks - slope is not affected), so length is segmented.
		/// Not depending on anything except length.
		/// </summary>
		/// <param name="nonSegmentedPipe">This is considered to be pipe with minSlope angle set.</param>
		public static Vector3d CalculatePointWithMinSlope(SegmentedLengthIterator lengths, Vector3d nonSegmentedPipe)
		{
			double desired2dLength = nonSegmentedPipe.GetAs2d().Length;
			Vector3d direction = nonSegmentedPipe.GetUnitVector();
			Vector3d min = direction.Multiply(lengths.MinLength);
			Vector3d max = direction.Multiply(lengths.MaxLength);

			Vector3d v;
			double currentClosestMax = direction.Multiply(lengths.MaxLength).GetAs2d().Length;
			double deltaMax = Math.Abs(currentClosestMax - desired2dLength);
			double closestLength = lengths.MaxLength;
			if (lengths.MinLengthValid)
			{
				// If max segments is 1.
				double currentClosestMin = direction.Multiply(lengths.MinLength).GetAs2d().Length;
				double deltaMin = Math.Abs(currentClosestMin - desired2dLength);
				if (deltaMin < deltaMax)
				{
					closestLength = lengths.MinLength;
				}
			}
			v = direction.Multiply(closestLength);

			return v;
		}

		/// <summary>
		/// Calculates end point of pipe when delta is fixed (point doesn't include delta!).
		/// </summary>
		public static Point3d CalculatePointWithMinDelta(SegmentedLengthIterator lengths, Vector3d nonSegmentedPipe, Point3d startPoint, 
															TerrainCalculator terrainCalc, double minDelta, double endShaftRadius = 0)
		{
			double desiredXYLength = nonSegmentedPipe.GetAs2d().Length;

			Point2d startPointXY = startPoint.GetAs2d();
			Vector3d nonSegmentedUnit = nonSegmentedPipe.GetUnitVector();
			// XY terrain reference points (along XY direction of pipe), used to determine terrain line equation.
			Point2d terrainPoint2d1 = startPointXY.Add(nonSegmentedUnit.GetAs2d().GetUnitVector());
			Point2d terrainPoint2d2 = terrainPoint2d1.Add(nonSegmentedUnit.GetAs2d().GetUnitVector());
			double terrainHeight1 = terrainCalc.GetHeight(terrainPoint2d1);
			double terrainHeight2 = terrainCalc.GetHeight(terrainPoint2d2);


			// Transform all point to system with startPointXY as coordinates start.
			Vector2d offsetCenterVector = startPointXY.GetAsVector();
			Point2d terrainP1_XY = terrainPoint2d1.Add(offsetCenterVector.Negate());
			Point2d terrainP2_XY = terrainPoint2d2.Add(offsetCenterVector.Negate());
			Point2d? pointResult_Z = null;

			{
				// Transform to XY-Z plane. 
				// Suffix _Z for this plane.
				Point2d start_Z = new Point2d(0, startPoint.Z + minDelta);
				Point2d terrainP1_Z = new Point2d(terrainP1_XY.GetAsVector().Length, terrainHeight1);
				Point2d terrainP2_Z = new Point2d(terrainP2_XY.GetAsVector().Length, terrainHeight2);

				Line2dParameters lineParams = new Line2dParameters(terrainP1_Z, terrainP2_Z);
				{
					// Adjust terrain height to center of shaft.
					// If terrain line is going up, then need to move up (terrain height in shaft center
					// becomes terrain height at pipe in xy).
					// If going down, then need to move down.
					// This is achieved acording to K sign.
					double delta = lineParams.K * endShaftRadius;
					lineParams.MoveY(delta);
				}
				
				Func<double, SearchPointParams<Point2d>> calculation = (d) =>
				{
					Point2d? pt = LineCircleIntersection(start_Z, d, lineParams, desiredXYLength);
					if (pt == null)
						return SearchPointParams<Point2d>.Null;
					SearchPointParams<Point2d> pParams = new SearchPointParams<Point2d>(pt.Value, pt.Value.X);
					return pParams;
				};
				ClosestPointFinder<Point2d> finder = new ClosestPointFinder<Point2d>();
				finder.Search(calculation, desiredXYLength, lengths);
				Point2d? res_Z = finder.GetFinalPoint(desiredXYLength).Point;

				Point2d closestPoint_Z = res_Z.Value;
				closestPoint_Z = closestPoint_Z.Add(new Vector2d(0, -minDelta));
				pointResult_Z = closestPoint_Z;
			}
			Point2d resultXY = nonSegmentedUnit.GetAs2d().GetUnitVector().Multiply(pointResult_Z.Value.X).GetAsPoint();
			Point3d result = resultXY.GetWithHeight(pointResult_Z.Value.Y);
			result = result.Add(offsetCenterVector.GetAs3d());
			return result;
		}

		/// <summary>
		/// Calculates end point of pipe with both min slope and <c>System.</c>min delta are used (end point doesn't include delta).
		/// </summary>
		/// <param name="nonSegmentedPipe">From start to end point. Considered is that slope of this paramater is min slope.</param>
		public static Point3d? CalculatePointWithMinSlopeAndDeltaDownstream(SegmentedLengthIterator lengths, Vector3d nonSegmentedPipe, Point3d startPoint, TerrainCalculator terrainCalc, double minSlope, double minDelta, double endShaftRadius = 0)
		{
			// Recalculate with minSlope (it can happen that nonSegmentedPipe is with minDelta and not slope).
			// Calculate first with minSlope, and then check minDelta.
			Vector2d t = nonSegmentedPipe.GetAs2d();
			double newZ = Math.Sign(nonSegmentedPipe.Z) * t.Length * minSlope;
			Vector3d minSlopeNonSegmentedPipe = new Vector3d(t.X, t.Y, newZ);

			double referenceLengthXY = minSlopeNonSegmentedPipe.GetAs2d().Length;
			Vector2d directionXY = minSlopeNonSegmentedPipe.GetAs2d().GetUnitVector();
			Vector3d direction = minSlopeNonSegmentedPipe.GetUnitVector();

			Point3d terrain1 = startPoint.Add(direction);
			Point3d terrain2 = startPoint.Add(direction.Multiply(2));
			double height1 = terrainCalc.GetHeight(terrain1.GetAs2d());
			double height2 = terrainCalc.GetHeight(terrain2.GetAs2d());
			terrain1 = terrain1.GetAs2d().GetWithHeight(height1);
			terrain2 = terrain2.GetAs2d().GetWithHeight(height2);

			// Set start point as coordinate center.
			Vector3d offsetVector = startPoint.GetAs2d().GetAsVector().GetAs3d();
			Point3d center = new Point3d(0, 0, startPoint.Z);
			Point2d terrainXY1 = terrain1.GetAs2d().Add(offsetVector.GetAs2d().Negate());
			Point2d terrainXY2 = terrain2.GetAs2d().Add(offsetVector.GetAs2d().Negate());

			// Transform to xy-z plane
			Point2d center_z = new Point2d(0, center.Z + minDelta);
			Point2d terrain1_z = new Point2d(terrainXY1.GetAsVector().Length, height1);
			Point2d terrain2_z = new Point2d(terrainXY2.GetAsVector().Length, height2);

			Line2dParameters lineParams = new Line2dParameters(terrain1_z, terrain2_z);
			{
				// Adjust terrain height to center of shaft.
				// If terrain line is going up, then need to move up (terrain height in shaft center
				// becomes terrain height at pipe in xy).
				// If going down, then need to move down.
				// This is achieved acording to K sign.
				double delta = lineParams.K * endShaftRadius;
				lineParams.MoveY(delta);
			}

			Func<double, SearchPointParams<Point3d>> calculation = (d) =>
			{
				double segmentedLength = d;

				// For 1 length:
				// - calculate point with length (with min slope)
				// - get terrain height for that point
				// - if terrain to point delta is > minDelta => point is good
				// - else calculate point with minDelta
				// -	calculate slope for that point
				// -	if slope is > minSlope => point is good
				// -	else
				//-			only possibility is that both slope and delta are != min values.
				//-			(not solved yet)

				Vector3d pipeSegmented = direction.Multiply(segmentedLength);

				Point3d endPoint = startPoint.Add(pipeSegmented);
				double delta = terrainCalc.GetHeight(endPoint.GetAs2d()) - endPoint.Z;

				Point3d? resultPoint = null;
				if (delta < minDelta)
				{

					// Find circle intersecestion
					Point2d? p = LineCircleIntersection(center_z, segmentedLength, lineParams, referenceLengthXY);
					if (!p.HasValue)
						return SearchPointParams<Point3d>.Null;

					Point2d result_z = p.Value.Add(new Vector2d(0, -minDelta));
					if (-(result_z.Y - startPoint.Z) / result_z.X + 0.0001 < minSlope)
					{
						// Try to find a point (where both delta and slope are not equal to their minimum values).
						//Debug.Assert(false, "Point doesn't have min slope or min delta");
						return SearchPointParams<Point3d>.Null;
					}

					// Now we have good point, translate it to original coordinate system.
					Point2d pt = directionXY.Multiply(result_z.X).GetAsPoint();
					Point3d pt3d = pt.GetWithHeight(result_z.Y);
					pt3d = pt3d.Add(offsetVector);

					resultPoint = pt3d;
				}
				else
					//TEST Slope is not checked here?
					resultPoint = endPoint;
				if (!resultPoint.HasValue)
					return SearchPointParams<Point3d>.Null;
				return new SearchPointParams<Point3d>(resultPoint.Value, startPoint.GetVectorTo(resultPoint.Value).GetAs2d().Length)
				{
					Length2D = startPoint.GetVectorTo(resultPoint.Value).GetAs2d().Length
				};
			};
			ClosestPointFinder<Point3d> finder = new ClosestPointFinder<Point3d>();
			finder.Search(calculation, referenceLengthXY, lengths);
			if (!finder.LeftPointFound && !finder.RightPointFound)
				return null;
			Point3d res = finder.GetFinalPoint(referenceLengthXY).Point;
			return res;
		}

		/// <summary>
		/// Calculates end point of pipe with both min slope and <c>System.</c>min delta are used (end point doesn't include delta).
		/// </summary>
		/// <param name="nonSegmentedPipe">From end to start point. Considered is that slope of this paramater is min slope.</param>
		/// <param name="endPointTerrain">Terrain height for end point (which is fixed)</param>
		/// <param name="terrainCalc">Calculator for start point.</param>
		public static Point3d? CalculatePointWithMinSlopeAndDeltaUpstream(SegmentedLengthIterator lengths, Vector3d nonSegmentedPipe, ref Point3d endPoint, 
																			double endPointTerrain, TerrainCalculator terrainCalc, double minSlope, double minDelta, 
																			double startPointDelta, double endShaftRadius = 0)
		{
			// Recalculate with minSlope (it can happen that nonSegmentedPipe is with minDelta and not slope).
			// Calculate first with minSlope, and then check minDelta.
			Vector2d t = nonSegmentedPipe.GetAs2d();
			double newZ = Math.Sign(nonSegmentedPipe.Z) * t.Length * minSlope;
			Vector3d minSlopeNonSegmentedPipe = new Vector3d(t.X, t.Y, newZ);

			double referenceLengthXY = minSlopeNonSegmentedPipe.GetAs2d().Length;
			Vector2d directionXY = minSlopeNonSegmentedPipe.GetAs2d().GetUnitVector();
			Vector3d direction = minSlopeNonSegmentedPipe.GetUnitVector();

			Point3d terrain1 = endPoint.Add(direction);
			Point3d terrain2 = endPoint.Add(direction.Multiply(2));
			double height1 = terrainCalc.GetHeight(terrain1.GetAs2d());
			double height2 = terrainCalc.GetHeight(terrain2.GetAs2d());
			terrain1 = terrain1.GetAs2d().GetWithHeight(height1);
			terrain2 = terrain2.GetAs2d().GetWithHeight(height2);

			// Set start point as coordinate center.
			Vector3d offsetVector = endPoint.GetAs2d().GetAsVector().GetAs3d();
			Point3d center = new Point3d(0, 0, endPoint.Z);
			Point2d terrainXY1 = terrain1.GetAs2d().Add(offsetVector.GetAs2d().Negate());
			Point2d terrainXY2 = terrain2.GetAs2d().Add(offsetVector.GetAs2d().Negate());

			// Transform to xy-z plane
			Point2d center_z = new Point2d(0, center.Z + minDelta);
			Point2d terrain1_z = new Point2d(terrainXY1.GetAsVector().Length, height1);
			Point2d terrain2_z = new Point2d(terrainXY2.GetAsVector().Length, height2);

			Line2dParameters lineParams = new Line2dParameters(terrain1_z, terrain2_z);
			{
				// Adjust terrain height to center of shaft.
				// If terrain line is going up, then need to move up (terrain height in shaft center
				// becomes terrain height at pipe in xy).
				// If going down, then need to move down.
				// This is achieved acording to K sign.
				double delta = lineParams.K * endShaftRadius;
				lineParams.MoveY(delta);
			}

			// Needed to use endPoint inside lambda.
			Point3d endPoint1 = endPoint;

			Func<double, SearchPointParams<Point3d>> calculation = (d) =>
			{
				double segmentedLength = d;

				// For 1 length:
				// - calculate point with length (with min slope)
				// - get terrain height for that point
				// - if terrain to point delta is > minDelta => point is good
				// - else calculate point with minDelta
				// -	calculate slope for that point
				// -	if slope is > minSlope => point is good
				// -	else
				//-			only possibility is that both slope and delta are != min values.
				//-			(not solved yet)

				Vector3d pipeSegmented = direction.Multiply(segmentedLength);
				Point3d endPointTemp = new Point3d(endPoint1.X, endPoint1.Y, endPoint1.Z);
				Point3d startPoint = endPointTemp.Add(pipeSegmented);
				double startHeight = terrainCalc.GetHeight(startPoint.GetAs2d());
				// Adjust start point delta.
				Vector3d offsetZ = new Vector3d(0, 0, -startPoint.Z +  (startHeight - startPointDelta));
				startPoint = startPoint.Add(offsetZ);
				endPointTemp = endPointTemp.Add(offsetZ);

				// Start point height is fixed (as delta from terrain).
				// Heights of start and end point are adjusted equally, 
				// so slope of pipe is not changed.
				double deltaZ = (startHeight - startPointDelta) - startPoint.Z;
				Vector3d deltaZVector = new Vector3d(0, 0, deltaZ);
				startPoint = startPoint.Add(deltaZVector);
				Point3d tempEndPoint = endPointTemp.Add(deltaZVector);

				double delta = endPointTerrain - tempEndPoint.Z;

				Point3d? resultStartPoint = null;
				if (delta < minDelta)
				{
					double endHeight = endPointTerrain;// terrainCalc.GetHeight(tempEndPoint.GetAs2d());
					center_z = new Point2d(0, endHeight - minDelta + startPointDelta);
					// Find circle intersecestion
					Point2d? p = LineCircleIntersection(center_z, segmentedLength, lineParams, referenceLengthXY);
					if (!p.HasValue)
						return SearchPointParams<Point3d>.Null;
					tempEndPoint = new Point3d(tempEndPoint.X, tempEndPoint.Y, endHeight - minDelta);

					Point2d result_z = p.Value.Add(new Vector2d(0, -startPointDelta));
					if ((result_z.Y - tempEndPoint.Z) / result_z.X + 0.0001 < minSlope)
					{
						// Try to find a point (where both delta and slope are not equal to their minimum values).
						//Debug.Assert(false, "Point doesn't have min slope or min delta");
						return SearchPointParams<Point3d>.Null;
					}

					// Now we have good point, translate it to original coordinate system.
					Point2d pt = directionXY.Multiply(result_z.X).GetAsPoint();
					Point3d pt3d = pt.GetWithHeight(result_z.Y);
					pt3d = pt3d.Add(offsetVector);

					resultStartPoint = pt3d;
				}
				else
					resultStartPoint = startPoint;
				if (!resultStartPoint.HasValue)
					return SearchPointParams<Point3d>.Null;
				return new SearchPointParams<Point3d>(resultStartPoint.Value, tempEndPoint.GetVectorTo(resultStartPoint.Value).GetAs2d().Length)
							{
								ExtraPoint = tempEndPoint,
								Length2D = resultStartPoint.Value.GetVectorTo(endPointTemp).GetAs2d().Length
							};
			};
			ClosestPointFinder<Point3d> finder = new ClosestPointFinder<Point3d>();
			finder.Search(calculation, referenceLengthXY, lengths);
			if (!finder.LeftPointFound && !finder.RightPointFound)
				return null;
			SearchPointParams<Point3d> res = finder.GetFinalPoint(referenceLengthXY);
			// Only height should be changed.
			Debug.Assert(DoubleHelper.AreEqual(endPoint.X, res.ExtraPoint.X, 0.0001), "upstream endPoint.X is changed!");
			Debug.Assert(DoubleHelper.AreEqual(endPoint.Y, res.ExtraPoint.Y, 0.0001), "upstream endPoint.Y is changed!");
			endPoint = res.ExtraPoint;
			return res.Point;
		}



		/// <summary>
		/// Calculates segmented pipe position, for fixed delta (and variable terrain height).
		/// </summary>
		/// <param name="nonSegmentedPipe"></param>
		/// <param name="segmentedLengths"></param>
		/// <param name="terrainCalc"></param>
		/// <param name="delta"></param>
		/// <param name="minSlope">If used, min slope has to be taken into account.</param>
		public static Point3d? Calculate(Point3d pipeStartPoint, Vector3d nonSegmentedPipe, List<double> segmentedLengths, TerrainCalculator terrainCalc, double delta, double? minSlope = null)
		{
			#region In this region, Point2d means Z = 0
			Point2d p1XY = pipeStartPoint.GetAs2d().Add(nonSegmentedPipe.GetAs2d());
			Point2d p2XY = pipeStartPoint.GetAs2d().Add(nonSegmentedPipe.GetAs2d().Add(nonSegmentedPipe.GetAs2d().GetUnitVector()));
			Vector2d terrainVector = p1XY.GetVectorTo(p2XY);
			if (!DoubleHelper.AreEqual(nonSegmentedPipe.GetAs2d().GetAngleDeg(terrainVector), 0, 0.00001))
				throw new InvalidOperationException("Terrain points XY angle is different!");
			double desired2dLength = nonSegmentedPipe.GetAs2d().Length;

			double h1 = terrainCalc.GetHeight(p1XY);
			double h2 = terrainCalc.GetHeight(p2XY);
			#endregion

			// Segmented line draws a circle (adding delta to end is same as adding delta to center point of rotation).
			// Then, we need to discover points where terrain line cuts through this imaginary circle (when we have 2
			// of them, choose closest one to desired2dLength).

			#region In this region, 2d means (xy, z)
			// Everything is moving in a single plane, so everything is translated in this plane to make calculation easier.
			// Transfer all into 2d (xy, z).
			Point2d startPointA = new Point2d(0, pipeStartPoint.Z + delta);// new Point2d(pipeStartPoint.GetAs2d().GetAsVector().Length, pipeStartPoint.Z + delta);
			Point2d terrain1pA = new Point2d(p1XY.Add(pipeStartPoint.GetAs2d().GetAsVector().Negate()).GetAsVector().Length, h1);
			Point2d terrain2pA = new Point2d(p2XY.Add(pipeStartPoint.GetAs2d().GetAsVector().Negate()).GetAsVector().Length, h2);
			List<Point2d> ptsA = new List<Point2d>();

			Line2dParameters lineParams = new Line2dParameters(terrain1pA, terrain2pA);
			
			foreach (double segmentedLength in segmentedLengths) 
			{
				Point2d? pt = LineCircleIntersection(startPointA, segmentedLength, lineParams, desired2dLength);
				if (pt == null)
					continue;
				if (minSlope != null)
				{
					// If new point slope is too small, don't use it.
					double slope = startPointA.GetAsVector().GetAngleDeg(pt.Value.GetAsVector());
					if (slope < minSlope.Value)
						continue;
				}
				ptsA.Add(pt.Value);
			}

			Debug.Assert(ptsA.Count != 0, "No segmented pipe with delta point on the terrain line found!");

			if (ptsA.Count == 0)
			{
				MyLog.MyTraceSource.Error("Segmented pipe with delta does not have intersection point with ground!");
				return null;
			}

			Point2d currentXY = ptsA[0];
			double currentLengthXY = Math.Abs(currentXY.X - startPointA.X);
			// For checking, if we have points on both sides of desired point, then closest one is the answer.
			// If there are points only for the left or right side, then closest point doesn't have to be the answer (but we consider it as answer).
			bool leftExists = false;
			bool rightExists = false;
			foreach (Point2d pt in ptsA)
			{
				double length = Math.Abs(pt.X - startPointA.X);
				double lengthDelta = length - desired2dLength;
				double currentLengthDelta = currentLengthXY - desired2dLength;
				if (lengthDelta < 0)
					leftExists |= true;
				if (lengthDelta > 0)
					rightExists |= true;
				if (Math.Abs(lengthDelta) < Math.Abs(currentLengthDelta))
				{
					currentLengthXY = length;
					currentXY = pt;
				}
			}
			#endregion

			//Debug.WriteLine("Best length (closest to current point) = " + currentLengthXY.ToString("0.00"));
			if (!(leftExists && rightExists))
			{
				// One of them must be true.
				// If we get to here, add some more points for calculation. Basicaly, when we have points on the both left and right, then chosen point
				// is the answer.
				Debug.WriteLine("Closest point is calculated only from point on the " + (leftExists ? "left" : "right") + " point from desired one.");
				//Debug.Assert(false, "Closest point calculated only from one side");
				//MyLog.MyTraceSource.Error("Closest point calculated from only " + (leftExists ? "left" : "right") + " . Maybe there is better point.");
			}
			// Do this for all segmented lenghts, and when distance from desired2Length becomes smaller and then bigger, we know what to do.
			// Some optimization is possible, to calculate first lengths which are around desired one, and one before and after if needed.
			Vector2d pipeXY = nonSegmentedPipe.GetAs2d().GetUnitVector();
			pipeXY = pipeXY.Multiply(currentXY.X);
			Point3d result = new Point3d(pipeXY.X, pipeXY.Y, currentXY.Y).Add(pipeStartPoint.GetAs2d().GetWithHeight(0).GetAsVector());
			return result;
		}

		public static Point2d LinesIntersection(Point2d p1_1, Point2d p1_2, Point2d p2_1, Point2d p2_2)
		{
			// y = kx + n
			// y - y1 = (y2 - y1) / (x2 - x1)  * (x - x1)
			// k = (y2 - y1) / (x2 - x1);
			// n = y1 - x1 * k;
			double k1 = (p1_2.Y - p1_1.Y) / (p1_2.X - p1_1.X);
			double k2 = (p2_2.Y - p2_1.Y) / (p2_2.X - p2_1.X);
			double n1 = p1_1.Y - p1_1.X * k1;
			double n2 = p2_2.Y - p2_2.X * k2;

			// y1 = k1 * x1 + n1
			// y1 = k2 * x2 + n2
			// Intersection point (x1 = x2 = x, y1 = y2 = y)
			// k1 * x + n1 = k2 * x + n2
			// x = (n1 - n2)/(k2 - k1)
			// y = k1 * x + n1
			double x0 = (n1 - n2) / (k2 - k1);
			double y0 = x0 * k1 + n1;

			return new Point2d(x0, y0);
		}
		/// <summary>
		/// Calculates intersection point between line and circle (from 2 results uses the one which is closer to referenceLength.
		/// </summary>
		public static Point2d? LineCircleIntersection(Point2d centerCircle, double radius, Line2dParameters lineParams, double referenceXYLength)
		{
			// (x - p) ^ 2 + (y - q) ^ 2 = r ^ 2
			double p = centerCircle.X;
			double q = centerCircle.Y;
			double r = radius;

			// y = k * x + n
			double k = lineParams.K; // (p2.Y - p1.Y) / (p2.X - p1.X);
			double n = lineParams.N; // p1.Y - p1.X * k;

			// Replacing y and solving for x:
			// a * x ^ 2 + b * x + c
			double a = 1 + k * k;
			double b = -2 * (p - k * (n - q));
			double c = (n - q) * (n - q) - r * r + p * p;

			double D = b * b - 4 * a * c;
			if (D < 0)
				return null;
			double x1 = (-b + Math.Sqrt(D)) / (2 * a);
			double x2 = (-b - Math.Sqrt(D)) / (2 * a);

			double length1 = x1 - centerCircle.X;
			double length2 = x2 - centerCircle.X;

			double x;
			double y;
			if (Math.Abs(length1 - referenceXYLength) < Math.Abs(length2 - referenceXYLength))
			{
				x = x1;
			}
			else
				x = x2;
			y = k * x + n;
			return new Point2d(x, y);

		}
	}
}
