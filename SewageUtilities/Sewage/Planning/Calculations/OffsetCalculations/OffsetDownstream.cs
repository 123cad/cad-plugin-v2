﻿using MyUtilities.Geometry;
using CadPlugin.Sewage.Create.Settings;
using CadPlugin.Sewage.Plannings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Sewage.Planning.Calculations.OffsetCalculations
{
	class OffsetDownstream : Plannings.OffsetCalculator
	{
		public OffsetDownstream(double startOffset, double endOffset) : base(startOffset, endOffset)
		{
		}
		

		/// <summary>
		/// Updates end point, to have adjusted angle.
		/// </summary>
		/// <param name="mgr"></param>
		/// <param name="start"></param>
		/// <param name="end"></param>
		protected override void AdjustAngleStep(PipeShaftDataManager mgr, ref Point3d start, ref Point3d end)
		{
			if (mgr.Data.Pipe.AngleSteppingUsed && mgr.PreviousDataExists)
			{
				Vector3d previousDirection = mgr.DataHistory.Last().Pipe.Direction;
				Vector3d currentDirection = start.GetVectorTo(end);
				double currentAngle = previousDirection.GetAngleDegXY(currentDirection);
				int steps = (int)(Math.Round(currentAngle / mgr.Data.Pipe.AngleStepDeg));
				double newAngle = steps * mgr.Data.Pipe.AngleStepDeg;
				double angleOffset = newAngle - currentAngle;
				Matrix3d rotationMatrix = Matrix3d.CreateRotation(new Vector3d(0, 0, 1), angleOffset);
				currentDirection = rotationMatrix.Transform(currentDirection);
				end = start.Add(currentDirection);
			}
		}
		internal void AdjustAngle_Test(PipeShaftDataManager mgr, ref Point3d start, ref Point3d end)
		{
			AdjustAngleStep(mgr, ref start, ref end);
		}


		/// <summary>
		/// Sets new end point.
		/// </summary>
		public override void UpdatePosition(PipeShaftDataManager mgr, TerrainCalculator terrainCalc)
		{
			IPipeShaftData data = mgr.Data;
			Point3d start = data.StartPoint;
			Point3d end = data.EndPoint;
			AdjustAngleStep(mgr, ref start, ref end);

			Point2d center2d = new Point2d(start.X, start.Y);
			Point2d mouse2d = new Point2d(end.X, end.Y);
			Vector2d direction2d = center2d.GetVectorTo(mouse2d);
			// If direction vector is 0 length, move it just a little bit, 
			// so calculation won't fail.
			if (direction2d.Length < 0.000001)
			{
				direction2d = new Vector2d(0.000001, 0);
				mouse2d = center2d.Add(direction2d);
			}
			double mouseDistance2d = direction2d.Length;

			Vector2d direction2dUnit = direction2d.GetUnitVector();
			Point2d startPoint = GetStartPoint(direction2dUnit, center2d);
			Point2d endPoint = GetEndPoint(direction2dUnit, mouse2d);


			Vector2d pipeXY = startPoint.GetVectorTo(endPoint);
			// Length which includes offset.
			double adjustedLength = pipeXY.Length;
			if (adjustedLength < StartOffset + EndOffset)
			{
				// Shafts can't overlap.
				pipeXY = direction2dUnit.Multiply(StartOffset + EndOffset);
			}
			double slope = data.MinPipeSlope;
			// If slope is to be calculated from pipe out delta.
			// First calculate delta, and then check with min slope.
			if (data.ApplyEndShaftPipeOutDelta)
			{
				Point2d pipeEndPoint2d = startPoint.Add(pipeXY);
				double tempDelta = data.EndShaft.MinPipeOutDelta;// data.EndShaft.TerrainHeight - data.EndShaft.PipeOutHeight;
													  // Because shaft height is considered to be fixed for in and out pipe.
				double terrainHeight = terrainCalc.GetHeight(mouse2d);// pipeEndPoint2d);
				// Delta from start pipe height to end shaft height, to calculate slope.
				double deltaH = start.Z - (terrainHeight - tempDelta + data.EndShaft.InnerSlopeDeltaH);
				slope = MyUtilities.Helpers.Calculations.GetSlope(new Vector3d(pipeXY.X, pipeXY.Y, -deltaH));
			}
			if (data.ApplyMinPipeSlope)
			{
				if (slope < data.MinPipeSlope)
					slope = data.MinPipeSlope;
			}
			//((PipeData)data.Pipe).Slope = slope;
			Vector3d pipeVector = GetVectorWithSlope(pipeXY, slope);

			PipeVector = pipeVector;
			StartPoint = start;
			StartPointOffset = new Point3d(startPoint.X, startPoint.Y, start.Z);
			EndPointOffset = StartPointOffset.Add(pipeVector);
			// End point is on the same height as pipe end point, but moved for EndOffset value.
			Vector2d endOffset = direction2dUnit.Multiply(EndOffset);

			// NOTE Calculated end point XY must be equal to provided end point; only Z can be different.
			EndPoint = EndPointOffset.Add(new Vector3d(endOffset.X, endOffset.Y, 0));
			// System.Diagnostics.Debug.Assert(Math.Abs(EndPoint.X - newPoint.X) < 0.00001, "Calculated end point X in offsetCalc is moved!");
			// System.Diagnostics.Debug.Assert(Math.Abs(EndPoint.Y - newPoint.Y) < 0.00001, "Calculated end point Y in offsetCalc is moved!");
		}

	}
}
