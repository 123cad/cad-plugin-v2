﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CadPlugin.Sewage.Create.Settings;
using MyUtilities.Geometry;


namespace CadPlugin.Sewage.Plannings
{
	/// <remarks>
	/// Calculates end of the pipe.
	/// Can include start/end shaft.
	/// </remarks>
	public abstract class PipeCalculation
	{ 
		/// <summary>
		/// Absolute start point - center of shaft if it exists, or start of pipe.
		/// </summary>
		public virtual Point3d StartPoint
		{
			get;protected set;
		}
		/// <summary>
		/// Absolute end point - center of shaft if exists, or end of pipe.
		/// </summary>
		public virtual Point3d EndPoint
		{
			get;
			protected set;
		}

		/// <summary>
		/// Point which has same height as EndPoint, but is closer to StartPoint by offset.
		/// </summary>
		public virtual Point3d EndPointOffset
		{
			get;
			protected set;
		}
		/// <summary>
		/// Point which has same height as StartPoint, but is closer to EndPoint by offset.
		/// </summary>
		public Point3d StartPointOffset
		{
			get;protected set;
		}

		/// <summary>
		/// Vector which represents the pipe (from pipe start point to the end point).
		/// </summary>
		public Vector3d PipeVector { get; protected set; }
			
		
		/// <summary>
		/// Performs calculation regarding to start point.
		/// "data" object is only being read.
		/// TerrainCalculator is because xy can be changed, so we need to preserve delta from terrain while calculating.
		/// </summary>
		public abstract void UpdatePosition(PipeShaftDataManager data,TerrainCalculator terrainCalc);


		/// <summary>
		/// Passes 2d vector and slope - returns 3d vector which has provided slope.
		/// (2d projection of returned 3d vector is same as passed 2d vector.)
		/// </summary>
		protected Vector3d GetVectorWithSlope(Vector2d xyVector, double slope)
		{

			double length2d = xyVector.Length;
			double depth = length2d * slope;

			Vector3d pipe = new Vector3d(xyVector.X, xyVector.Y, -depth);
			
			return pipe;
		}

		/// <summary>
		/// Returns 2d (in x-y plane) vector between 3d points.
		/// </summary>
		public static Vector2d GetXYPlaneVector(Point3d start, Point3d end)
		{
			Vector3d t = start.GetVectorTo(end);
			Vector2d v2 = new Vector2d(t.X, t.Y);
			return v2;
		}

	}
}

