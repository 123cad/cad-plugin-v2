﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Sewage.Create.Settings;
using System.Drawing;
using System.Reflection;

namespace CadPlugin.Sewage.Plannings
{
	enum TextDisplayType
	{
		Pipe, 
		Shaft
	}
	enum DisplayScale
	{
		Percent = 0,
		Permille = 1
	}

	/// <summary>
	/// Interface for those labels which support scaling.
	/// </summary>
	interface IDisplayScaled
	{
		DisplayScale Scale { get; set; }
	}
	/// <summary>
	/// For those labels which support decimal digits.
	/// </summary>
	interface ITextDecimals
	{
		/// <summary>
		/// Valid value is 0 to 5.
		/// </summary>
		int Digits { get; set; }
	}

	/// <summary>
	/// Stores data objects which are displayed (null means no display).
	/// </summary>
	class DisplaySelectedData
	{
		internal IShaftData ShaftData;
		internal IPipeData PipeData;
		internal IPipeData PreviousPipeData;

		/// <summary>
		/// Sets pipe data, previous pipe data and shaft data (with start shaft).
		/// </summary>
		internal static DisplaySelectedData CreateWithStartShaft(PipeShaftDataManager mgr)
		{
			DisplaySelectedData d = new DisplaySelectedData();
			d.PipeData = mgr.Data.Pipe;
			d.ShaftData = mgr.Data.StartShaft;
			if (mgr.PreviousDataExists)
				d.PreviousPipeData = mgr.DataHistory.Last().Pipe;
			return d;
		}
		/// <summary>
		/// Sets pipe data, previous pipe data and shaft data (with end shaft).
		/// </summary>
		internal static DisplaySelectedData CreateWithEndShaft(PipeShaftDataManager mgr)
		{

			DisplaySelectedData d = new DisplaySelectedData();
			d.PipeData = mgr.Data.Pipe;
			d.ShaftData = mgr.Data.EndShaft;
			if (mgr.PreviousDataExists)
				d.PreviousPipeData = mgr.DataHistory.Last().Pipe;
			return d;
		}
	}
	abstract class TextDisplaySingle
	{
		/// <summary>
		/// Unique name of text object.
		/// </summary>
		public string Name { get { return GetType().Name; } }
		public abstract TextDisplayType TextType { get; }
		
		/// <summary>
		/// Returns value formatted for display.
		/// </summary>
		protected virtual string GetValue(DisplaySelectedData data)
		{
			return GetValue(data);
		}
		/// <summary>
		/// Prefix text which appears before value.
		/// </summary>
		public string Description { get; set; }
		/// <summary>
		/// Text drawn at the end.
		/// </summary>
		public string Suffix { get; set; }
		public double TextSize { get; set; }
		public Color TextColor { get; set; }
		/// <summary>
		/// Is text is displayed while jigging.
		/// </summary>
		public bool DrawInJig { get; set; }
		/// <summary>
		/// Is drawn as final text (when users moves to next shaft, this data is drawn).
		/// </summary>
		public bool DrawAsFinal { get; set; }

		protected TextDisplaySingle()
		{
			TextSize = 0.3;
			TextColor = Create.AdditionalSettings.AdditionalSettings.DefaultTextColor;
			DrawInJig = true;
			DrawAsFinal = true;
		}
		
		/// <summary>
		/// Returns displayed text with Description as prefix.
		/// </summary>
		public string GetText(DisplaySelectedData data)
		{
			if (!TextExists(data))
				return "";
			string s = GetValue(data);
			return Description + " " + s + Suffix;
		}
		/// <summary>
		/// Indicates if text exists for current manager.
		/// </summary>
		public virtual bool TextExists(DisplaySelectedData data)
		{
			return true;
		}
	}
}
namespace CadPlugin.Sewage.Plannings.Texts
{
	class TextFactory
	{
		private Dictionary<string, PipeTextDisplay> allPipes = new Dictionary<string, PipeTextDisplay>();
		private Dictionary<string, ShaftTextDisplay> allShafts = new Dictionary<string, ShaftTextDisplay>();
		private bool initialized = false;
		private static TextFactory __Instance = null;
		public static TextFactory Instance
		{
			get
			{
				if (__Instance == null)
					__Instance = new TextFactory();
				return __Instance;
			}
		}
		private void initialize()
		{
			List<Type> allPipeClasses = Assembly.GetExecutingAssembly().GetTypes().Where(a => a.IsClass && a.Namespace != null && a.Namespace.Equals(@"CadPlugin.Sewage.Plannings.Texts.Pipes")).ToList();
			List<Type> allShaftClasses = Assembly.GetExecutingAssembly().GetTypes().Where(a => a.IsClass && a.Namespace != null && a.Namespace.Equals(@"CadPlugin.Sewage.Plannings.Texts.Shafts")).ToList();
			foreach (Type t in allPipeClasses)
			{
				PipeTextDisplay p = (PipeTextDisplay)Activator.CreateInstance(t);
				allPipes.Add(t.Name, p);
			}
			foreach (Type t in allShaftClasses)
			{
				ShaftTextDisplay p = (ShaftTextDisplay)Activator.CreateInstance(t);
				allShafts.Add(t.Name, p);
			}
			initialized = true;
		}
		public IEnumerable<PipeTextDisplay> GetAllPipes()
		{
			if (!initialized)
				initialize();
			return allPipes.Values;
		}
		public IEnumerable<ShaftTextDisplay> GetAllShafts()
		{
			if (!initialized)
				initialize();
			return allShafts.Values;
		}
		public PipeTextDisplay GetPipe(string className)
		{
			if (!initialized)
				initialize();
			if (!allPipes.ContainsKey(className))
				return null;
			return allPipes[className];
		}
		public ShaftTextDisplay GetShaft(string className)
		{
			if (!initialized)
				initialize();

			if (!allShafts.ContainsKey(className))
				return null;
			return allShafts[className];
		}
	}
	abstract class PipeTextDisplay : TextDisplaySingle
	{
		public sealed override TextDisplayType TextType
		{
			get
			{
				return TextDisplayType.Pipe;
			}
		}
	}
	abstract class ShaftTextDisplay : TextDisplaySingle
	{
		public sealed override TextDisplayType TextType
		{
			get
			{
				return TextDisplayType.Shaft;
			}
		}
	}
}
// Put shaft objects only, to be used via reflection.
namespace CadPlugin.Sewage.Plannings.Texts.Shafts
{
	class ShaftCover : ShaftTextDisplay, ITextDecimals
	{
		//public override string Name { get { return "Cover"; } }
		public ShaftCover()
		{
			Description = "Cover";
			Digits = 2;
		}
		public int Digits { get; set; }
		public override bool TextExists(DisplaySelectedData data)
		{
			return data.ShaftData != null;
		}
		protected override string GetValue(DisplaySelectedData data)
		{
			double d = data.ShaftData.CoverHeight;
			string s = DoubleHelper.ToStringInvariant(d, Digits);//*DisplayFormat(d)*/ + " m";
			return s;
		}
	}
	class ShaftTerrain : ShaftTextDisplay, ITextDecimals
	{
		//public override string Name { get { return "Terrain"; } }
		public ShaftTerrain()
		{
			Description = "Terrain";
			Digits = 2;
		}
		public int Digits { get; set; }
		public override bool TextExists(DisplaySelectedData data)
		{
			return data.ShaftData != null;
		}
		protected override string GetValue(DisplaySelectedData data)
		{
			double d = data.ShaftData.TerrainHeight;
			string s = DoubleHelper.ToStringInvariant(d, Digits);//*DisplayFormat(d)*/+ " m";
			return s;
		}
	}
	class ShaftPipeOut : ShaftTextDisplay, ITextDecimals
	{
		//public override string Name { get { return "Pipeout"; } }
		public ShaftPipeOut()
		{
			Description = "Pipe out";
			Digits = 2;
		}
		public int Digits { get; set; }
		public override bool TextExists(DisplaySelectedData data)
		{
			return data.ShaftData != null;
		}
		protected override string GetValue(DisplaySelectedData data)
		{
			double d = data.ShaftData.PipeOutHeight;
			string s = DoubleHelper.ToStringInvariant(d, Digits);//*DisplayFormat(d)*/ + " m";
			return s;
		}
	}
	class ShaftBottom : ShaftTextDisplay, ITextDecimals
	{
		//public override string Name { get { return "Bottom"; } }
		public ShaftBottom()
		{
			Description = "Bottom";
			Digits = 2;
		}
		public int Digits { get; set; }
		public override bool TextExists(DisplaySelectedData data)
		{
			return data.ShaftData != null;
		}
		protected override string GetValue(DisplaySelectedData data)
		{
			double d = data.ShaftData.BottomHeight;
			string s = DoubleHelper.ToStringInvariant(d, Digits);//*DisplayFormat(d)*/ + " m";
			return s;
		}
	}
	class ShaftDepth : ShaftTextDisplay, ITextDecimals
	{
		//public override string Name { get { return "Depth"; } }
		public ShaftDepth()
		{
			Description = "Depth";
			Digits = 2;
		}
		public int Digits { get; set; }
		public override bool TextExists(DisplaySelectedData data)
		{
			return data.ShaftData != null;
		}
		protected override string GetValue(DisplaySelectedData data)
		{
			double d = data.ShaftData.TerrainHeight - data.ShaftData.BottomHeight;
			string s = DoubleHelper.ToStringInvariant(d, Digits);//*DisplayFormat(d)*/ + " m";
			return s;
		}
	}
	class ShaftPipeAngle : ShaftTextDisplay, ITextDecimals
	{
		//public override string Name { get { return "Angle"; } }
		public ShaftPipeAngle()
		{
			Description = "Pipe angle";
			Digits = 2;
		}
		public int Digits { get; set; }
		public override bool TextExists(DisplaySelectedData data)
		{
			return data.PreviousPipeData != null && data.ShaftData != null;
		}
		protected override string GetValue(DisplaySelectedData data)
		{
			string s = "";
			double angleDeg = data.PreviousPipeData.Direction.GetAngleDegXY(data.PipeData.Direction);
			s = DoubleHelper.ToStringInvariant(angleDeg, Digits); ///*DisplayFormat(angleDeg)*/ + " °";

			return s;
		}
	}
	/// <summary>
	/// Draws custom text from user.
	/// </summary>
	class CustomText : ShaftTextDisplay
	{
		public CustomText()
		{
			Description = "Custom text";
		}
		protected override string GetValue(DisplaySelectedData data)
		{
			return "";
		}
	}
}
// Put pipe objects only, to be used via reflection.
namespace CadPlugin.Sewage.Plannings.Texts.Pipes
{
	class PipeSegmentsCount : PipeTextDisplay
	{
		//public override string Name { get { return "Segments count"; } }
		public PipeSegmentsCount()
		{
			Description = "Segments";
		}
		protected override string GetValue(DisplaySelectedData data)
		{
			if (!data.PipeData.SegmentsUsed)
				return "-";
			string s = data.PipeData.SegmentCount.ToString();
			return s;
		}
	}
	class PipeLength2d : PipeTextDisplay, ITextDecimals
	{
		//public override string Name { get { return "Pipe length 2d"; } }
		public PipeLength2d()
		{
			Description = "Length 2d";
			Digits = 2;
		}
		public int Digits { get; set; }
		protected override string GetValue(DisplaySelectedData data)
		{
			MyUtilities.Geometry.Point3d p1 = data.PipeData.StartPoint;
			MyUtilities.Geometry.Point3d p2 = data.PipeData.EndPoint;
			double length = new MyUtilities.Geometry.Point2d(p1.X, p1.Y).GetVectorTo(new MyUtilities.Geometry.Point2d(p2.X, p2.Y)).Length;
			string s = DoubleHelper.ToStringInvariant(length, Digits);//*DisplayFormat(length)*/ + " m";
			return s;
		}
	}
	class PipeLength3d : PipeTextDisplay, ITextDecimals
	{
		//public override string Name { get { return "Pipe length 3d"; } }
		public PipeLength3d()
		{
			Description = "Length 3d";
			Digits = 2;
		}
		public int Digits { get; set; }

		protected override string GetValue(DisplaySelectedData data)
		{
			double length = data.PipeData.StartPoint.GetVectorTo(data.PipeData.EndPoint).Length;
			string s = DoubleHelper.ToStringInvariant(length, Digits);//* DisplayFormat(length)*/ + " m";
			return s;
		}
	}
	class PipeSlope : PipeTextDisplay, ITextDecimals, IDisplayScaled
	{
		//public override string Name { get { return "Pipe slope"; } }
		public PipeSlope()
		{
			Description = "Slope";
			Digits = 2;
			Scale = DisplayScale.Percent;
		}

		public int Digits { get; set; }

		public DisplayScale Scale { get; set; }

		protected override string GetValue(DisplaySelectedData data)
		{
			int scale = 0;
			string sign = "";
			switch (Scale)
			{
				case DisplayScale.Percent:
					scale = 100;
					sign = "%";
					break;
				case DisplayScale.Permille:
					scale = 1000;
					sign = "‰";
					break;
			}
			double d = data.PipeData.Slope * scale;
			return DoubleHelper.ToStringInvariant(d, Digits)/*DisplayFormat(d)*/ + " " + sign;
		}
	}
	/// <summary>
	/// Drawn at the beggining of the pipe (next to shaft).
	/// </summary>
	class PipeOut : PipeTextDisplay, ITextDecimals
	{
		public PipeOut()
		{
			Description = "Pipe out";
			Digits = 2;
		}

		public int Digits { get; set; }
		protected override string GetValue(DisplaySelectedData data)
		{
			double d = data.PipeData.StartPoint.Z;
			string s = DoubleHelper.ToStringInvariant(d, Digits); //DisplayFormat(d);// + "m";
			return s;
		}
	}
	/// <summary>
	/// Drawn at the end of the pipe (next to shaft).
	/// </summary>
	class PipeIn : PipeTextDisplay, ITextDecimals
	{
		public PipeIn()
		{
			Description = "Pipe in";
			Digits = 2;
		}
		public int Digits { get; set; }

		protected override string GetValue(DisplaySelectedData data)
		{
			double d = data.PipeData.EndPoint.Z;			
			string s = DoubleHelper.ToStringInvariant(d, Digits); // DisplayFormat(d);// + "m";
			return s;
		}
	}
	/// <summary>
	/// Draws custom text from user.
	/// </summary>
	class CustomText : PipeTextDisplay
	{
		public CustomText()
		{
			Description = "Custom text";
		}
		protected override string GetValue(DisplaySelectedData data)
		{
			return "";
		}
	}



}
