﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyUtilities.Geometry;

namespace CadPlugin.Sewage.Plannings
{
	public class Face3dHeight : TerrainCalculator
	{
		public Point3d P1 { get; set; }
		public Point3d P2 { get; set; }
		public Point3d P3 { get; set; }
		public Face3dHeight(Point3d p1, Point3d p2, Point3d p3)
		{
			P1 = p1;
			P2 = p2;
			P3 = p3;
		}

		public override double GetHeight(Point3d point)
		{
			return MyUtilities.Helpers.Calculations.PointHeightInPlane(P1, P2, P3, new Point2d(point.X, point.Y));
		}
		public override double GetHeight(Point2d point)
		{
			return MyUtilities.Helpers.Calculations.PointHeightInPlane(P1, P2, P3, point);
		}

	}
}

