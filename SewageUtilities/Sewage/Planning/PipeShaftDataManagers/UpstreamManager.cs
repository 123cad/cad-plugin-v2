﻿using MyUtilities.Geometry;
using CadPlugin.Sewage.Plannings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Sewage.Planning.PipeShaftDataManagers
{
	class UpstreamManager : PipeShaftDataManager
	{
		public override void MoveNextShaft()
		{
			bool firstPipe = PreviousDataExists;
			if (!Data.StartPointSet)
				throw new NotSupportedException("Unable to move to the next pipe. Start point is not set!");
			dataHistory.Add(__Data.Clone());

			// End shaft becomes current start shaft.
			__Data.EndShaftData = __Data.StartShaftData;
			// New start shaft is clones or created.
			if (Data.StartShaft != null)
				__Data.StartShaftData = __Data.EndShaftData.Clone();
			else
				__Data.StartShaftData = new Create.Settings.ShaftData();

			__Data.StartPointSet = false;
			// Start point offset is always used.
			__Data.StartShaftData.MinPipeOutDeltaUsed = true;


			// Notify Data that shaft has been changed.
			__Data.MoveNextShaft();
		}

		public override void SetVariablePoint(Point3d position)
		{
			SetStartPoint(position);
		}

		public override void UpdateVariablePoint(Point3d point, double? terrainHeight = null)
		{
			SetStartPoint(point);
			Recalculate();
		}

		protected override void EndPointUpdated()
		{
			__Data.StartPointSet = false;
		}

		protected override void RecalculatePoint()
		{
			TerrainCalculator calc;
			double terrainH;


			calc = TerrainHeightCalculator;
			System.Diagnostics.Debug.Assert(calc != null, "Terrain calculator does not exist in upstream!");
			//NOTE If user sets height manually, additional property is needed.
			/*if (Data.StartShaft == null)
				calc = TerrainHeightCalculator;
			else
				// (i think this is used because user can change value manualy.
				// Otherwise, good value is set in calculator, so no need to fetch terrain).
				calc = new PointTerrainHeight(new Point3d(0, 0, Data.StartShaft.TerrainHeight));*/

			Calculation.UpdatePosition(this, calc);
			terrainH = calc.GetHeight(Calculation.StartPoint);

			if (Data.StartShaft != null)
			{
				UpdateStartShaft(terrainH, ShaftFields.Terrain);
			}
			if (Data.EndShaft != null)
			{ 
				double t = 0;
				t = Calculation.EndPointOffset.Z - Data.EndShaft.InnerSlopeDeltaH;
				UpdateEndShaft(t, ShaftFields.PipeOut);
			}
		}
	}
}
