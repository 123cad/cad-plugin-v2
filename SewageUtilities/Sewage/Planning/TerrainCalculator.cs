﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyUtilities.Geometry;

namespace CadPlugin.Sewage.Plannings
{
	public abstract class TerrainCalculator
	{ 
		
		/// <summary>
		/// Returns height for provided point (z is ignored).
		/// </summary>
		public abstract double GetHeight(Point3d point);
		public abstract double GetHeight(Point2d point);
	}
}

