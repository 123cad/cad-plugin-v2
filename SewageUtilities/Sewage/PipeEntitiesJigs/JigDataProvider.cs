﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.PipeEntitiesJigs
{
	/// <summary>
	/// Adapter between jigger and data. Jigger notifies about mouse moved, 
	/// data is calculated and values are updated from outside.
	/// </summary>
	public class JigDataProvider
	{
		/// <summary>
		/// Cursor has moved, recalculate and update data.
		/// </summary>
		public event Action<Point3d> UpdateData;

		public virtual Point3d StartPoint
		{
			get;
			set;
		}

		public virtual Point3d EndPoint
		{
			get;
			set;
		}

		public virtual Point3d LineStartPoint
		{
			get;
			set;
		}

		public virtual Point3d LineEndPoint
		{
			get;
			set;
		}

		public void CursorMove(Point3d position)
		{
			if (UpdateData != null)
				UpdateData.Invoke(position);
		}
		
	}
}

