﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.PipeEntitiesJigs
{
	public abstract class JiggerCalculation
	{ 
		/// <summary>
		/// Center of jig operation.
		/// </summary>
		public virtual Point3d Center { get; private set; }

		public JigDataProvider Data { get; private set; }


		public virtual Point3d GetLineStartPoint()
		{
			// By default, line goes from center of jig operation.
			return Data.LineStartPoint;
		}

		/// <summary>
		/// Recalculates data, returning information if anything has been changed.
		/// </summary>
		public abstract SamplerStatus Update(Point3d mousePosition);

		public virtual Point3d GetLineEndPoint()
		{
			return Data.LineEndPoint;
		}

		/// <summary>
		/// By default, Entities are moved relative to line end point.
		/// </summary>
		/// <param name="currentPoint">Current position of entities.</param>
		/// <returns></returns>
		public virtual Vector3d GetMoveVectorForEntities(Point3d currentPoint)
		{
			return currentPoint.GetVectorTo(Data.EndPoint);
		}

		protected JiggerCalculation(Point3d center)
		{
			Center = center;
			Data = new JigDataProvider();
			Data.LineStartPoint = Center;
		}

	}
}

