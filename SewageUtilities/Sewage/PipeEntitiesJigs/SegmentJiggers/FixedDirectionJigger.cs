﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.PipeEntitiesJigs
{
	public class FixedDirectionJigger : SegmentJigger
	{
		private Vector3d Direction { get; set; }
		public FixedDirectionJigger(Point3d center, Vector3d direction, double segmentLength):base (center, 0, segmentLength)
		{
			Direction = direction.GetNormal();
			Vector2d v = new Vector2d(direction.X, direction.Y);
			if (v.Length == 0)
				Slope = -double.MaxValue;
			else
				Slope = -direction.Z / v.Length;
		}
		public override SamplerStatus Update(Point3d mousePosition)
		{
			Data.CursorMove(mousePosition);
			
			return SamplerStatus.OK;
		}
	}
}