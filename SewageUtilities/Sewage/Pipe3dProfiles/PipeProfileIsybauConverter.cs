﻿using Isybau2015.Isybau2015.ReferenceTableEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Sewage.Pipe3dProfiles
{

    public class PipeProfileIsybauConverter
    {
        public static PipeProfile FromS104(S104_Profiltyp p)
        {
            PipeProfile pr = PipeProfile.Other;
            switch (p)
            {
                case S104_Profiltyp.Kreisprofil:
                    pr = PipeProfile.Circle;
                    break;
                case S104_Profiltyp.Eiprofil:
                    pr = PipeProfile.Egg;
                    break;
                case S104_Profiltyp.Maulprofil:
                    pr = PipeProfile.Mouth;
                    break;
                case S104_Profiltyp.Rechteckprofil_G:
                    pr = PipeProfile.RectangleClosed;
                    break;
                case S104_Profiltyp.Kreisprofil_D:
                    pr = PipeProfile.Ring;
                    break;
                case S104_Profiltyp.Rechteckprofil_O:
                    pr = PipeProfile.RectangleOpen;
                    break;
                case S104_Profiltyp.Eiprofil_U:
                    pr = PipeProfile.EggUnequal;
                    break;
                case S104_Profiltyp.Maulprofil_U:
                    pr = PipeProfile.MouthUnequal;
                    break;
                case S104_Profiltyp.Trapezprofil:
                    pr = PipeProfile.Trapez;
                    break;
                case S104_Profiltyp.Doppeltrapezprofil:
                    pr = PipeProfile.TrapezDouble;
                    break;
                case S104_Profiltyp.U_foermig:
                    pr = PipeProfile.RectangleArc;
                    break;
                case S104_Profiltyp.Bogenfoermig:
                case S104_Profiltyp.oval:
                case S104_Profiltyp.andereProfilart:
                case S104_Profiltyp.R:
                case S104_Profiltyp.E:
                case S104_Profiltyp.Z:
                    break;
            }
            return pr;
        }
        public static PipeProfile FromG205(G205_Profilart p)
        {
            PipeProfile pr = PipeProfile.Other;
            switch (p)
            {
                case G205_Profilart.Kreisprofil:
                    pr = PipeProfile.Circle;
                    break;
                case G205_Profilart.Eiprofil:
                    pr = PipeProfile.Egg;
                    break;
                case G205_Profilart.Maulprofil:
                    pr = PipeProfile.Mouth;
                    break;
                case G205_Profilart.Rechteckprofil_G:
                    pr = PipeProfile.RectangleClosed;
                    break;
                case G205_Profilart.Kreisprofil_D:
                    pr = PipeProfile.Ring;
                    break;
                case G205_Profilart.Rechteckprofil_O:
                    pr = PipeProfile.RectangleOpen;
                    break;
                case G205_Profilart.Eiprofil_U:
                    pr = PipeProfile.EggUnequal;
                    break;
                case G205_Profilart.Maulprofil_U:
                    pr = PipeProfile.MouthUnequal;
                    break;
                case G205_Profilart.Trapezprofil:
                    pr = PipeProfile.Trapez;
                    break;
                case G205_Profilart.Doppeltrapezprofil:
                    pr = PipeProfile.TrapezDouble;
                    break;
                case G205_Profilart.U_foermig:
                    pr = PipeProfile.RectangleArc;
                    break;
                case G205_Profilart.Bogenfoermig:
                case G205_Profilart.oval:
                case G205_Profilart.andereProfilart:
                    break;
            }
            return pr;
        }
        public static S104_Profiltyp ToS104(PipeProfile p)
        {
            S104_Profiltyp pt = S104_Profiltyp.andereProfilart;
            switch (p)
            {
                case PipeProfile.Other:
                    break;
                case PipeProfile.Circle:
                    pt = S104_Profiltyp.Kreisprofil;
                    break;
                case PipeProfile.Egg:
                    pt = S104_Profiltyp.Eiprofil;
                    break;
                case PipeProfile.Mouth:
                    pt = S104_Profiltyp.Maulprofil;
                    break;
                case PipeProfile.RectangleClosed:
                    pt = S104_Profiltyp.Rechteckprofil_G;
                    break;
                case PipeProfile.Ring:
                    pt = S104_Profiltyp.Kreisprofil_D;
                    break;
                case PipeProfile.RectangleOpen:
                    pt = S104_Profiltyp.Rechteckprofil_O;
                    break;
                case PipeProfile.EggUnequal:
                    pt = S104_Profiltyp.Eiprofil_U;
                    break;
                case PipeProfile.MouthUnequal:
                    pt = S104_Profiltyp.Maulprofil_U;
                    break;
                case PipeProfile.Trapez:
                    pt = S104_Profiltyp.Trapezprofil;
                    break;
                case PipeProfile.TrapezDouble:
                    pt = S104_Profiltyp.Doppeltrapezprofil;
                    break;
                case PipeProfile.RectangleArc:
                    pt = S104_Profiltyp.U_foermig;
                    break;
            }
            return pt;
        }
        public static G205_Profilart ToG205(PipeProfile p)
        {
            G205_Profilart pt = G205_Profilart.andereProfilart;
            switch (p)
            {
                case PipeProfile.Other:
                    break;
                case PipeProfile.Circle:
                    pt = G205_Profilart.Kreisprofil;
                    break;
                case PipeProfile.Egg:
                    pt = G205_Profilart.Eiprofil;
                    break;
                case PipeProfile.Mouth:
                    pt = G205_Profilart.Maulprofil;
                    break;
                case PipeProfile.RectangleClosed:
                    pt = G205_Profilart.Rechteckprofil_G;
                    break;
                case PipeProfile.Ring:
                    pt = G205_Profilart.Kreisprofil_D;
                    break;
                case PipeProfile.RectangleOpen:
                    pt = G205_Profilart.Rechteckprofil_O;
                    break;
                case PipeProfile.EggUnequal:
                    pt = G205_Profilart.Eiprofil_U;
                    break;
                case PipeProfile.MouthUnequal:
                    pt = G205_Profilart.Maulprofil_U;
                    break;
                case PipeProfile.Trapez:
                    pt = G205_Profilart.Trapezprofil;
                    break;
                case PipeProfile.TrapezDouble:
                    pt = G205_Profilart.Doppeltrapezprofil;
                    break;
                case PipeProfile.RectangleArc:
                    pt = G205_Profilart.U_foermig;
                    break;
            }
            return pt;
        }
        public static string ToLongString(PipeProfile p, bool includeIndex = true)
        {
            var s = ToG205(p);
            return ToLongString(s, includeIndex);
        }
        public static string ToLongString(S104_Profiltyp p, bool includeIndex = true)
        {
            var s = ToG205(FromS104(p));
            return ToLongString(s, includeIndex);
        }
        public static string ToLongString(G205_Profilart p, bool includeIndex = true)
        {
            string s = "";
            switch (p)
            {
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Kreisprofil:
                    s = "Kreisprofil";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Eiprofil:
                    s = "Eiprofil (H/B=3/2)";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Maulprofil:
                    s = "Maulprofil (H/B=1.66/2)";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Rechteckprofil_G:
                    s = "Rechteckprofil (geschlossen)";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Kreisprofil_D:
                    s = "Kreisprofil (doppelwandig)";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Rechteckprofil_O:
                    s = "Rechteckprofil (offen)";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Eiprofil_U:
                    s = "Eiprofil (H/B ungleich 3/2)";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Maulprofil_U:
                    s = "Maulprofil (H/B ungleich 1.66/2)";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Trapezprofil:
                    s = "Trapezprofil";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Doppeltrapezprofil:
                    s = "Doppeltrapezprofil";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.U_foermig:
                    s = "U-förmig";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Bogenfoermig:
                    s = "Bogenförmig";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.oval:
                    s = "oval";
                    break;
                default:
                    s = "andere Profilart";
                    break;
            }
            if (includeIndex)
                s = (int)p + " - " + s;
            return s;
        }
        public static string ToShortString(PipeProfile p, bool includeIndex = true)
        {
            var s = ToG205(p);
            return ToShortString(s, includeIndex);
        }
        public static string ToShortString(S104_Profiltyp p, bool includeIndex = true)
        {
            var s = ToG205(FromS104(p));
            return ToShortString(s, includeIndex);
        }
        public static string ToShortString(G205_Profilart p, bool includeIndex = true)
        {
            string s = "";
            switch (p)
            {
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Kreisprofil:
                    s = "DN";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Eiprofil:
                    s = "EI";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Maulprofil:
                    s = "M";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Rechteckprofil_G:
                    s = "RG";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Kreisprofil_D:
                    s = "DND";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Rechteckprofil_O:
                    s = "RO";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Eiprofil_U:
                    s = "Ei";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Maulprofil_U:
                    s = "M";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Trapezprofil:
                    s = "T";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Doppeltrapezprofil:
                    s = "DT";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.U_foermig:
                    s = "U";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.Bogenfoermig:
                    s = "BG";
                    break;
                case Isybau2015.Isybau2015.ReferenceTableEnums.G205_Profilart.oval:
                    s = "O";
                    break;
                default:
                    s = "AP";
                    break;
            }
            if (includeIndex)
                s = (int)p + " - " + s;
            return s;
        }
    }
}
