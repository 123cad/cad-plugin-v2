﻿using Isybau2015.Isybau2015.ReferenceTableEnums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Sewage.Pipe3dProfiles
{
    
    /// <summary>
    /// All supported parameters for a pipe.
    /// </summary>
    public class PipeParameters
    {
        public PipeProfile Profile { get; set; } = PipeProfile.Circle;
        public G200_Kantentyp PipeType { get; set; } = G200_Kantentyp.Haltung;
        /// <summary>
        /// Inner diameter in horizontal layout.
        /// </summary>
        public double Diameter { get; set; } 
        /// <summary>
        /// Inner diameter in vertical layout (not used in circle profiles).
        /// </summary>
        public double Height { get; set; }
        /// <summary>
        /// Thickness of the pipe (
        /// </summary>
        public double WallThickness { get; set; } = 0.2;
        /// <summary>
        /// Distance between double pipes (doppel).
        /// </summary>
        public double DoublePipeDistance { get; set; } = 1;
		
        public PipeParameters(double diameter = 1)
        {
            Diameter = diameter;
            Height = Height;
        }
        public PipeParameters(PipeParameters source)
        {
            Profile = source.Profile;
            Diameter = source.Diameter;
            Height = source.Height;
            WallThickness = source.WallThickness;
            DoublePipeDistance = source.DoublePipeDistance;
        }
        /// <summary>
        /// Creates default parameters for circle profile.
        /// </summary>
        /// <param name="diameter"></param>
        /// <returns></returns>
        public static PipeParameters CreateCircle(double diameter)
        {
            var p = new PipeParameters(diameter)
            {
                Height = diameter,
                Profile = PipeProfile.Circle
            };
            return p;
        }

        public PipeParameters Clone()
        {
            return new PipeParameters(this);
        }

        /// <summary>
        /// Sets current value from source values.
        /// </summary>
        /// <param name="source"></param>
        public void ApplyFrom(PipeParameters source)
        {
            Profile = source.Profile;
            Diameter = source.Diameter;
            Height = source.Height;
            WallThickness = source.WallThickness;
            DoublePipeDistance = source.DoublePipeDistance;
        }

    }
}
