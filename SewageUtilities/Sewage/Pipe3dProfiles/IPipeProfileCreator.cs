﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Inventories.Pipes;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Sewage.Pipe3dProfiles
{
    /// <summary>
    /// Interface for creating pipe profile entities (entities from which pipe base profile is created).
    /// </summary>
    public interface IPipeProfileCreator
    {
        /// <summary>
        /// Creates entity base (with optional hollowEntity). Returns if operation was successfull, with setting Entity. 
        /// (hollowEntity is optional). All entities are drawn relative to (0,0), and not moved after.
        /// Drawn at the bottom of the pipe.
        /// </summary>
        /// <param name="diameter"></param>
        /// <param name="entities"></param>
        /// <returns></returns>
        bool CreateEntityBase(PipeParameters pipeParameters, ComplexSolidEntitiesCollection entities);

        /// <summary>
        /// Gets total width of the pipe (if it is double, this is from minX to maxX).
        /// </summary>
        /// <returns></returns>
        double GetBaseTotalWidth(PipeParameters pipeParameters);

        /// <summary>
        /// Distance from bottom to center of the pipe.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        double CenterOffset(PipeParameters parameters);

    }


}
