﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isybau2015.Isybau2015.ReferenceTableEnums;
using CadPlugin.Sewage.Pipe3dProfiles.PipeProfileCreators;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Sewage.Pipe3dProfiles
{
    public class Pipe3dProfileManager
    {
        public static IPipeProfileCreator GetProfile(S104_Profiltyp p)
        {
            PipeProfile pr = PipeProfileIsybauConverter.FromS104(p);            
            return GetProfile(pr);
        }
        public static IPipeProfileCreator GetProfile(G205_Profilart p)
        {
            PipeProfile pr = PipeProfileIsybauConverter.FromG205(p);            
            return GetProfile(pr);
        }
        public static IPipeProfileCreator GetProfile(PipeProfile pr)
        {
            IPipeProfileCreator profile = PipeProfileCircle.Instance;
            switch (pr)
            {
                case PipeProfile.Other:
                    break;
                case PipeProfile.Circle:
                    profile = PipeProfileCircle.Instance;
                    break;
                case PipeProfile.Egg:
                    profile = PipeProfileEgg.Instance;
                    break;
                case PipeProfile.Mouth:
                    profile = PipeProfileMouth.Instance;
                    break;
                case PipeProfile.RectangleClosed:
                    profile = PipeProfileRectangleClosed.Instance;
                    break;
                case PipeProfile.Ring:
                    profile = PipeProfileRing.Instance;
                    break;
                case PipeProfile.RectangleOpen:
                    profile = PipeProfileRectangleOpen.Instance;
                    break;
                case PipeProfile.EggUnequal:
                    profile = PipeProfileEggUnequal.Instance;
                    break;
                case PipeProfile.MouthUnequal:
                    profile = PipeProfileMouthUnequal.Instance;
                    break;
                case PipeProfile.Trapez:
                    profile = PipeProfileTrapez.Instance;
                    break;
                case PipeProfile.TrapezDouble:
                    profile = PipeProfileTrapezDouble.Instance;
                    break;
                case PipeProfile.RectangleArc:
                    profile = PipeProfileRectangleArc.Instance;
                    break;
            }
            return profile;
        }
    }
}
