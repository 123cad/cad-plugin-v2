﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Sewage.Pipe3dProfiles.PipeProfileCreators.PipeProfileBases;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif


namespace CadPlugin.Sewage.Pipe3dProfiles.PipeProfileCreators
{
    public class PipeProfileMouth : IPipeProfileCreator
    {
        private static PipeProfileMouth _Instance;
        public static PipeProfileMouth Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new PipeProfileMouth();
                return _Instance;
            }
        }

        public double GetBaseTotalWidth(PipeParameters pipeParameters)
        {
            return MouthProfile.GetOuterDiameter(pipeParameters.Diameter);
        }
        public bool CreateEntityBase(PipeParameters pipeParameters, ComplexSolidEntitiesCollection entities)
        {
            //baseEntity = new SolidBaseEntity();
            //hollowEntity = new SolidBaseEntity();
            var complex = MouthProfile.CreateProfile(pipeParameters.Diameter);
            entities.Add(new ComplexSolidProfileManager(complex));
            return complex != null;
            //baseEntity = EggProfile.CreateBaseInner(pipe.Diameter);
            //hollowEntity = EggProfile.CreateBaseOuter(pipe.Diameter);
            //var startPosition = pipe.StartPoint.ToCADPoint();
            //Matrix3d translate = Matrix3d.Displacement(startPosition.GetAsVector());
            //baseEntity.Entity.TransformBy(translate);
            //hollowEntity.Entity.TransformBy(translate);
            //return true;
        }

        public double CenterOffset(PipeParameters parameters)
        {
            throw new NotImplementedException("Pipe Mouth profile center is not calculated yet.");
            // If this is really needed, create
        }
    }
}
