﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Sewage.Pipe3dProfiles.PipeProfileCreators.PipeProfileBases
{
    /// <summary>
    /// Creates 2d egg profile (Ei) which is H/B=3/2 where B is diameter in horizontal position, and H is vertical height.
    /// </summary>
    public class EggProfile
    {
        //NOTE Top and Bottom have their own radius, and there is distance between them (
        //NOTE 2 hals circles and 2 (almost straight) lines connecting them.

        class Calculated
        {
            /// <summary>
            /// Total inner height (TopRadius + BottomRadius + distance between their centers).
            /// </summary>
            public double H { get; }
            /// <summary>
            /// Radius of top circle.
            /// </summary>
            public double TopRadius { get; }
            /// <summary>
            /// Radius of bottom circle.
            /// </summary>
            public double BottomRadius { get; }
            public double TopBulge { get; }
            public double BottomBulge { get; } 
            public double SideBulge { get; } 
            public double TopBulgeOffset { get;  }
            public double BottomBulgeOffset { get;  }
            /// <summary>
            /// Distance between top and bottom arc center.
            /// </summary>
            public double TopToBottomArcDistance { get; }

            /// <summary>
            /// Distance from top pipe arc center to bottom of the pipe.
            /// </summary>
            public double CenterToBottom => TopToBottomArcDistance + BottomRadius;

            public Calculated(double diameter)
            {
                // Initial data.
                // 1.1 is to make arc bigger - this helps transition from top arc to the line (to keep height unchanged, subtract offset).
                TopBulge = 1.1;
                // 0.5 makes arc smaller - also transition (to keep height unchanged, add offset).
                BottomBulge = 0.5;
                SideBulge = 0.1;

                double radius = diameter / 2;
                // When bulge != 0, adjust height so total height doesn't change.
                TopBulgeOffset = (TopBulge - 1.0) * radius;

                
                H = 1.5 * diameter;
                BottomRadius = radius / 2;

                // Distance between center of top and bottom circle.
                // But because those are not circles after applying bulges, height has to be adjusted by offset.
                TopToBottomArcDistance = H - radius - BottomRadius;

                BottomBulgeOffset = (BottomBulge - 1) * BottomRadius;

            }
        }
        // For graphic representation, check dwg file in current folder.

        public static ComplexSolidProfile CreateProfile(double diameter)
        {
            var baseEntity = CreateBaseOuter(diameter);
            var hollowEntity = CreateBaseInner(diameter);

            ComplexSolidProfile complex = new ComplexSolidProfile(baseEntity);
            complex.InnerEntities.Add(hollowEntity);
            return complex;
        }


        /// <summary>
        /// Center of the pipe (top arc) to bottom.
        /// </summary>
        /// <param name="diameter"></param>
        /// <returns></returns>
        public static double GetBottom(double diameter)
        {
            return new Calculated(diameter).CenterToBottom;
        }

        /// <summary>
        /// Returns outer diameter for provided inner pipe diameter.
        /// </summary>
        /// <param name="diameter"></param>
        /// <returns></returns>
        public static double GetOuterDiameter(double diameter)
        {
            var e = CreateBaseOuter(diameter);
            // Another way which is faster?
            double w = e.GeometricExtents.MaxPoint.X - e.GeometricExtents.MinPoint.X;
            e.Dispose();
            return w;
        }
        /// <summary>
        /// Creates inner profile for pipe (hollow part).
        /// </summary>
        /// <param name="diameter"></param>
        /// <returns></returns>
        public static Entity CreateBaseInner(double diameter)
        {
            //ComplexSolidEntities t = new ComplexSolidEntities();

            double radius = diameter / 2;

            var d = new Calculated(diameter);

            Point2d entityCenter = new Point2d(0, 0);

            //Vector2d absoluteCenterVector = center.GetAsVector();
            //absoluteCenterVector = new Vector2d();
            Point2d[] pts = new Point2d[4];
            pts[0] = new Point2d(-radius, -d.TopBulgeOffset);
            pts[1] = new Point2d(radius, -d.TopBulgeOffset);
            pts[2] = new Point2d(d.BottomRadius, -(d.TopToBottomArcDistance /*- topBulgeOffset*/ - d.BottomBulgeOffset));
            pts[3] = new Point2d(-d.BottomRadius, -(d.TopToBottomArcDistance /*- topBulgeOffset*/ - d.BottomBulgeOffset));

            Polyline pl = new Polyline();
            pl.Closed = true;
            //t.BaseProfileEntity = pl;
            pl.AddVertexAt(0, pts[0], -d.TopBulge, 0, 0);
            pl.AddVertexAt(1, pts[1], -d.SideBulge, 0, 0);
            pl.AddVertexAt(2, pts[2], -d.BottomBulge, 0, 0);
            pl.AddVertexAt(3, pts[3], -d.SideBulge, 0, 0);//*/
                                                        // By my mistake it appeared that order of points defines rotation.
            /*pl.AddVertexAt(0, pts[0], sideBulge, 0, 0);
            pl.AddVertexAt(1, pts[3], bottomBulge, 0, 0);
            pl.AddVertexAt(2, pts[2], sideBulge, 0, 0);
            pl.AddVertexAt(3, pts[1], topBulge, 0, 0);//*/

            // Pipe center is positioned at the inner bottom of the pipe.
            Point2d pipeCenter = new Point2d(0, d.CenterToBottom);
            pl.TransformBy(Matrix3d.Displacement(pipeCenter.GetAs3d().GetAsVector()));


            // For some reason sold entity is created rotated. Cancel that rotation with this.
            //Matrix3d rotation = Matrix3d.Rotation(Math.PI, Vector3d.ZAxis, new Point3d(0, 0, 0));
            //pl.TransformBy(rotation);
            //t.Offset = entityCenter.GetVectorTo(pts[0]);


            return pl;
        }

        /// <summary>
        /// Creates outer profile.
        /// </summary>
        /// <param name="diameter"></param>
        /// <returns></returns>
        public static Entity CreateBaseOuter(double diameter)
        {
            //NOTE Not really understand each step, but basicaly we started to draw inner polyline, 
            //NOTE and then just added some offset for points.
            //ComplexSolidEntities t = new ComplexSolidEntities();

            double radius = diameter / 2;
            //NOTE bulge is multiplicated by radius(1 means bulge = radius) to get distance from arc center to the center of the line between 2 points.
            // This will be complicated.
            // 1.1 is to make arc bigger - this helps transition from top arc to the line (to keep height unchanged, subtract offset).
            double topBulge = 1.1;
            // 0.5 makes arc smaller - also transition (to keep height unchanged, add offset).
            double bottomBulge = 0.5;
            double sideBulge = 0.05;
            double topBulgeOffset = (topBulge - 1.0) * radius; // When bulge != 0, adjust height so total height doesn't change.

            double H = 1.5 * diameter;
            double bottomRadius = radius / 2;
            double l = H - radius - bottomRadius;// Distance between center of top and bottom circle.
                                                 // But because those are not circles after applying bulges, height has to be adjusted by offset.
            double bottomBulgeOffset = (bottomBulge - 1) * bottomRadius;
            //l += bottomBulgeOffset;

            Point2d entityCenter = new Point2d(0, 0);

            //Vector2d absoluteCenterVector = center.GetAsVector();
            //absoluteCenterVector = new Vector2d();
            // Define clockwise, starting from top left point.
            Point2d[] pts = new Point2d[4];
            pts[0] = new Point2d(-radius * 1.2, -topBulgeOffset);
            pts[1] = new Point2d(radius * 1.2, -topBulgeOffset);
            pts[2] = new Point2d(bottomRadius * 1.5, -(H - radius + 0.5 * bottomRadius));///*- topBulgeOffset*/  + Math.Abs(bottomBulgeOffset * 1.5)));
			pts[3] = new Point2d(-bottomRadius * 1.5, -(H - radius + 0.5 * bottomRadius));//*- topBulgeOffset*/ + Math.Abs(bottomBulgeOffset * 1.5)));

            Polyline pl = new Polyline();
            pl.Closed = true;
            //t.BaseProfileEntity = pl;
            // Need to add in counterclockwise direction (otherwise it is rotated by 90deg).
            pl.AddVertexAt(0, pts[0], -topBulge, 0, 0);
            pl.AddVertexAt(1, pts[1], -sideBulge, 0, 0);
            pl.AddVertexAt(2, pts[2], 0, 0, 0);
            pl.AddVertexAt(3, pts[3], -sideBulge, 0, 0);
            /*pl.AddVertexAt(0, pts[0], sideBulge, 0, 0);
			pl.AddVertexAt(1, pts[3], 0, 0, 0);
			pl.AddVertexAt(2, pts[2], sideBulge, 0, 0);
			pl.AddVertexAt(3, pts[1], topBulge, 0, 0);*/

            // Pipe center is positioned at the inner bottom of the pipe.
            Point2d pipeCenter = new Point2d(0, l + bottomRadius);
            pl.TransformBy(Matrix3d.Displacement(pipeCenter.GetAs3d().GetAsVector()));

            // For some reason, making solid3d rotates the object. With this, that is neutralized.
            //Matrix3d rotation = Matrix3d.Rotation(Math.PI, Vector3d.ZAxis, new Point3d(0, 0, 0));
            //pl.TransformBy(rotation);
            //t.Offset = entityCenter.GetVectorTo(pts[0]);


            return pl;
        }
    }
}
