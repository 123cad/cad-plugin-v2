﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Sewage.Pipe3dProfiles.PipeProfileCreators.PipeProfileBases
{
    /// <summary>
    /// Circle profile without ring.
    /// </summary>
    public class CircleProfile
    {
        public static ComplexSolidProfile CreateProfile(double diameter)
        {
            var baseEntity = CreateBase(diameter);
            //var hollowEntity = CreateBaseInner(diameter);
            ComplexSolidProfile complex = new ComplexSolidProfile(baseEntity);
            //complex.InnerEntities.Add(hollowEntity);
            return complex;
        }
        public static double GetBottom(double diameter)
        {
            return diameter / 2;
        }
        public static Entity CreateBase(double diameter)
        {
            Circle c = new Circle();
            if (diameter < 0.001)
                diameter = 0.01;
            c.Diameter = diameter;
            c.Center = c.Center.Add(new Vector3d(0, diameter / 2, 0));

            // For some reason, making solid3d rotates the object. With this, that is neutralized.
            Matrix3d rotation = Matrix3d.Rotation(Math.PI, Vector3d.ZAxis, new Point3d(0, 0, 0));
            //c.TransformBy(rotation);
            return c;
        }
        /*public static Entity CreateBaseOuter(double diameter)
		{
			Circle c = new Circle();
			if (diameter < 0.001)
				diameter = 0.01;
			c.Diameter = diameter * 1.2;
			c.Center = c.Center.Add(new Vector3d(0, diameter / 2, 0));

			// For some reason, making solid3d rotates the object. With this, that is neutralized.
			Matrix3d rotation = Matrix3d.Rotation(Math.PI, Vector3d.ZAxis, new Point3d(0, 0, 0));
			c.TransformBy(rotation);
			return c;
		}*/
    }
}
