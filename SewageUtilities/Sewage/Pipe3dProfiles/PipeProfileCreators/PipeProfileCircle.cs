﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Sewage.Pipe3dProfiles.PipeProfileCreators.PipeProfileBases;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif


namespace CadPlugin.Sewage.Pipe3dProfiles.PipeProfileCreators
{
    /// <summary>
    /// Circle profile.
    /// </summary>
    public class PipeProfileCircle : IPipeProfileCreator
    {
        private static PipeProfileCircle _Instance;
        public static PipeProfileCircle Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new PipeProfileCircle();
                return _Instance;
            }
        }


        public double GetBaseTotalWidth(PipeParameters pipeParameters)
        {
            return pipeParameters.Diameter;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="diameter"></param>
        /// <param name="entities"></param>
        /// <returns></returns>
        public bool CreateEntityBase(PipeParameters pipeParameters, ComplexSolidEntitiesCollection entities)
        {
            var complex = CircleProfile.CreateProfile(pipeParameters.Diameter);
            entities.Add(new ComplexSolidProfileManager(complex));
            return complex != null;
        }

        public double CenterOffset(PipeParameters parameters)
        {
            return CircleProfile.GetBottom(parameters.Diameter);
        }
    }
}
