﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Sewage.Pipe3dProfiles.PipeProfileCreators.PipeProfileBases;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif


namespace CadPlugin.Sewage.Pipe3dProfiles.PipeProfileCreators
{
    public class PipeProfileRectangleArc : IPipeProfileCreator
    {
        private static PipeProfileRectangleArc _Instance;
        public static PipeProfileRectangleArc Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new PipeProfileRectangleArc();
                return _Instance;
            }
        }

        public double GetBaseTotalWidth(PipeParameters pipeParameters)
        {
            return RectangleProfilArc.GetOuterDiameter(pipeParameters.Diameter);
        }
        public bool CreateEntityBase(PipeParameters pipeParameters, ComplexSolidEntitiesCollection entities)
        {
            var ent = RectangleProfilArc.CreateProfile(pipeParameters.Diameter);
            if (ent != null)
                entities.Add(new ComplexSolidProfileManager(ent));
            return ent != null;
            // H/B=3/2 egg profile.
            //baseEntity = new SolidBaseEntity();
            //hollowEntity = new SolidBaseEntity();
            //return RectangleProfilArc.CreateProfile(pipe.Diameter, entities);
            //baseEntity = EggProfile.CreateBaseInner(pipe.Diameter);
            //hollowEntity = EggProfile.CreateBaseOuter(pipe.Diameter);
            //var startPosition = pipe.StartPoint.ToCADPoint();
            //Matrix3d translate = Matrix3d.Displacement(startPosition.GetAsVector());
            //baseEntity.Entity.TransformBy(translate);
            //hollowEntity.Entity.TransformBy(translate);
            //return true;
        }

        public double CenterOffset(PipeParameters parameters)
        {
            throw new NotImplementedException();
        }
    }
}
