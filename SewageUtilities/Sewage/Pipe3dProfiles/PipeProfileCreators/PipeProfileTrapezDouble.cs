﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Sewage.Pipe3dProfiles.PipeProfileCreators.PipeProfileBases;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif


namespace CadPlugin.Sewage.Pipe3dProfiles.PipeProfileCreators
{
    /// <summary>
    /// Mirrored Trapez profile.
    /// </summary>
    public class PipeProfileTrapezDouble : IPipeProfileCreator
    {

        private static PipeProfileTrapezDouble _Instance;
        public static PipeProfileTrapezDouble Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new PipeProfileTrapezDouble();
                return _Instance;
            }
        }

        public double GetBaseTotalWidth(PipeParameters pipeParameters)
        {
            double d = TrapezProfil.GetOuterDiameter(pipeParameters.Diameter);
            // Pipe outer displacement between 2 trapez.
            d = d + d + d * 0.1 * 2;
            return d;
        }
        public double CenterOffset(PipeParameters parameters)
        {
            return TrapezProfil.GetBottom(parameters.Diameter);
        }

        public bool CreateEntityBase(PipeParameters pipeParameters, ComplexSolidEntitiesCollection entities)
        {
            bool success = true;
            ComplexSolidProfile complex = TrapezProfil.CreateProfile(pipeParameters.Diameter);
            success = complex != null;
            var ext = complex.BaseProfileEntity.GeometricExtents;
            double width = ext.MaxPoint.X - ext.MinPoint.X;
            Matrix3d move = Matrix3d.Displacement(new Vector3d(-(width / 2) * 1.1, 0, 0));
            complex?.BaseProfileEntity.TransformBy(move);
            if (success)
            {
                entities.Add(new ComplexSolidProfileManager(complex));
                complex = TrapezProfil.CreateProfile(pipeParameters.Diameter);
                move = Matrix3d.Displacement(new Vector3d(width / 2 * 1.1, 0, 0));
                complex?.BaseProfileEntity.TransformBy(move);
                if (complex != null)
                    entities.Add(new ComplexSolidProfileManager(complex));
                success = complex != null;
            }
            return success;
        }
    }
}
