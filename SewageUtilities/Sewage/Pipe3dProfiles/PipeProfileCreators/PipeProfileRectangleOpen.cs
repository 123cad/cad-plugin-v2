﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Sewage.Pipe3dProfiles.PipeProfileCreators.PipeProfileBases;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif


namespace CadPlugin.Sewage.Pipe3dProfiles.PipeProfileCreators
{
    public class PipeProfileRectangleOpen : IPipeProfileCreator
    {

        private static PipeProfileRectangleOpen _Instance;
        public static PipeProfileRectangleOpen Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new PipeProfileRectangleOpen();
                return _Instance;
            }
        }
        public double GetBaseTotalWidth(PipeParameters pipeParameters)
        {
            return RectangleProfileOpen.GetOuterDiameter(pipeParameters.Diameter);
        }
        public bool CreateEntityBase(PipeParameters pipeParameters, ComplexSolidEntitiesCollection entities)
        {
            var complex = RectangleProfileOpen.CreateProfile(pipeParameters.Diameter);
            if (complex != null)
                entities.Add(new ComplexSolidProfileManager(complex));
            return complex != null;
        }

        public double CenterOffset(PipeParameters parameters)
        {
            throw new NotImplementedException();
        }
    }
}
