﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Sewage.Pipe3dProfiles
{
    public enum PipeProfile
    {
        Other = -1,
        /// <summary>
        /// Kreisprofil.
        /// </summary>
        Circle = 0,
        /// <summary>
        /// Eiprofil (H/B=3/2)
        /// </summary>
        Egg = 1,
        /// <summary>
        /// Maulprofil (H/B=1.66/2)
        /// </summary>
        Mouth = 2,
        /// <summary>
        /// Rechteckprofil (geschlossen)
        /// </summary>
        RectangleClosed = 3,
        /// <summary>
        /// Kreisprofil (doppelwandig)
        /// </summary>
        Ring = 4,
        /// <summary>
        /// Rechteckprofil (offen)
        /// </summary>
        RectangleOpen = 5,
        /// <summary>
        /// Eiprofil (H/B != 3/2) (ungleich)
        /// </summary>
        EggUnequal = 6,
        /// <summary>
        /// Maulprofil (H/B != 3/2) (ungleich)
        /// </summary>
        MouthUnequal = 7,
        /// <summary>
        /// Trapezprofil.
        /// </summary>
        Trapez = 8,
        /// <summary>
        /// Doppeltrapezprofil.
        /// </summary>
        TrapezDouble = 9,
        /// <summary>
        /// U-foermig.
        /// </summary>
        RectangleArc = 10,
    }
}
