﻿using MyUtilities.Geometry;
using CadPlugin;
using CadPlugin.Sewage.Create.Settings;
using CadPlugin.Sewage.Create.Totals;
using CadPlugin.Sewage.Planning;
using CadPlugin.Sewage.Plannings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CadPlugin.Sewage.Persistence;
using System.Drawing;

namespace CadPlugin.Sewage.Create
{
	/// <summary>
	/// Additional data that has to be preserved after dialog is closed.
	/// </summary>
	public class ViewData
	{
		/// <summary>
		/// All labels for start shaft to be drawn (text, circles...).
		/// </summary>
		public bool DrawStartShaftLabels = true;
		/// <summary>
		/// All labels for end shaft to be drawn (text, circles...).
		/// </summary>
		public bool DrawEndShaftLabels = true;
		/// <summary>
		/// All labels for pipe to be drawn (arrow, text...).
		/// </summary>
		public bool DrawPipeLabels = true;

		/// <summary>
		/// When point for next shaft is to be reselected (point has been selected, 
		/// but it needs to be changed), this is last
		/// selected point. (this allows to draw outline of previously 
		/// selected point, to help the user).
		/// </summary>
		public Point3d? CanceledPoint = null;
	}

	class PipeShaftViewController
	{
		//NOTE Some shit is happening with the form, since UserControl had been added (i had to do it manually).
		// Something about being unable to find or load autocad.dll.
		// This manifests as not being able to save the file. But, when it is suceeded, 
		// all custom controls are removed from designer, and have to be added again.
		private IPipeShaftView view { get; set; }
		private PipeShaftDataManager dataManager { get; set; }
		private DataViewController viewController { get; set; }
		private TotalsViewController totalsController { get; set; }
		/// <summary>
		/// Tells if dialog is opened first time for the command.
		/// </summary>
		private bool firstRun = false;

		/// <summary>
		/// Start form for the first time.
		/// </summary>
		internal static CloseReason RunDownstream(PipeShaftDataManager data, ViewData viewData)
		{
			PipeShaftViewController controller = new PipeShaftViewController(data, WaterDirection.Downstream, viewData);
			CloseReason reason = controller.ShowDialog();
			return reason;
		}
		/// <summary>
		/// Start form for the first time.
		/// </summary>
		internal static CloseReason RunUpstream(PipeShaftDataManager data, ViewData viewData)
		{
			PipeShaftViewController controller = new PipeShaftViewController(data, WaterDirection.Upstream, viewData);

			CloseReason reason = controller.ShowDialog();
			return reason;
		}

		private PipeShaftViewController(PipeShaftDataManager mgr, WaterDirection direction, ViewData viewData)
		{
			firstRun = !mgr.PreviousDataExists;

			this.dataManager = mgr;
			view = new PipeShaftView();
			switch (direction)
			{
				case WaterDirection.Downstream:
					InitializeDownstream();
					break;
				case WaterDirection.Upstream:
					InitializeUpstream();
					break;
			}
			this.Initailize();
			//view.StartShaft.Description = "Start shaft";
			//view.EndShaft.Description = "End shaft";
			view.StartShaft.MinDelta.Text = "Delta";
			view.StartShaft.DrawLabels.Checked = viewData.DrawStartShaftLabels;
			view.EndShaft.DrawLabels.Checked = viewData.DrawEndShaftLabels;
			//TODO draw pipe labels.

			viewController = new DataViewController(view, mgr, firstRun, direction, viewData);
			viewController.Initialize();
			totalsController = new TotalsViewController(view.Totals);
			totalsController.UpdateData(mgr);
			view.OpenSettings += () => AdditionalSettings.SettingsController.Start();
			((Form)view).FormClosing += PipeShaftViewController_FormClosing;

			PipeCreateViewVisibleManager vmgr = new PipeCreateViewVisibleManager((PipeShaftView)view);
		}
		private void InitializeDownstream()
		{
			/*view.StartShaft.MinPipeOutDelta.Enabled = false;
			view.StartShaft.MinPipeOutDelta.Visible = false;
			view.StartShaft.MinPipeOutDeltaUsed.Enabled = false;
			view.StartShaft.MinPipeOutDeltaUsed.Visible = false;*/

			// By default image is downstream.

			if (dataManager.PreviousDataExists)
			{
				view.StartShaft.DrawLabels.Enabled = false;

				// Indicates that this should remain disabled through whole form life.
				view.StartShaft.DrawLabels.Tag = "disable";
			}

			view.SelectPoint.Text = "End point";
		}
		private void InitializeUpstream()
		{
			Control startShaft = (Control)view.StartShaft;
			Control endShaft = (Control)view.EndShaft;
			Point positionStart = startShaft.Location;
			Point positionEnd= endShaft.Location;
			startShaft.Location = positionEnd;
			endShaft.Location = positionStart;

			// For start shaft pipe out is fixed.
			view.StartShaft.MinPipeOutDeltaUsed.Visible = false;
			// This is always used for start shaft.
			((ShaftData)dataManager.Data.StartShaft).MinPipeOutDeltaUsed = true;
			view.WaterDirectionPicture.Image = Properties.Resources.pipe_upstream;

			if (dataManager.PreviousDataExists)
			{
				view.EndShaft.DrawLabels.Enabled = false;
				// Indicates that this should remain disabled through whole form life.
				view.EndShaft.DrawLabels.Tag = "disable";
			}

			view.SelectPoint.Text = "Start point";

		}

		private void PipeShaftViewController_FormClosing(object sender, FormClosingEventArgs e)
		{
			// If nothing else is created, all current data is valid. Allow closing.			
			if (view.CloseReason == CloseReason.Cancel ||
				view.CloseReason == CloseReason.Finish)
				return;

			// Check data - if it is invalid create settings.
			bool valid = viewController.CheckData();

			if (!valid)
				e.Cancel = true;
		}

		private CloseReason ShowDialog()
		{
			var reason = ((Form)view).ShowDialog();
			DataPersistence.SaveData((PipeShaftData)dataManager.Data, true, true, true);
			DataPersistence.SaveLabels(AdditionalSettings.AdditionalSettings.Instance.TextOrder);
			return view.CloseReason;
		}

		/// <summary>
		/// Sets DEFAULT values to all fields.
		/// </summary>
		private void Initailize()
		{			
			// TODO default data or data stored in registry (last used data)?

			// Start shaft
			view.StartShaft.CreateShaft.Checked = true;
			view.StartShaft.Diameter.Text = "1000";
			view.StartShaft.InnerSlope.Text = "2.5";
			view.StartShaft.InnerDelta.Text = (100.0 * 2.5).ToString("0.00");
			view.StartShaft.CoverHeight.Text = "0";
			view.StartShaft.CoverTerrainDelta.Text = "0";
			view.StartShaft.TerrainHeight.Text = "0";
			view.StartShaft.TerrainPipeOutDelta.Text = "0";
			view.StartShaft.PipeOutHeight.Text = "0";
			view.StartShaft.PipeOutBottomDelta.Text = "0";
			view.StartShaft.BottomHeight.Text = "0";
			view.StartShaft.Depth.Text = "0";

			// End shaft
			view.EndShaft.CreateShaft.Checked = true;
			view.EndShaft.Diameter.Text = "1000";
			view.EndShaft.InnerSlope.Text = "2.5";
			view.EndShaft.InnerDelta.Text = (100.0 * 2.5).ToString("0.00");
			view.EndShaft.CoverHeight.Text = "0";
			view.EndShaft.CoverTerrainDelta.Text = "0";
			view.EndShaft.TerrainHeight.Text = "0";
			view.EndShaft.TerrainPipeOutDelta.Text = "0";
			view.EndShaft.PipeOutHeight.Text = "0";
			view.EndShaft.PipeOutBottomDelta.Text = "0";
			view.EndShaft.BottomHeight.Text = "0";
			view.EndShaft.Depth.Text = "0";

			// Pipe
			view.MinPipeSlope.Text = "5";
			view.PipeDiameter.Text = "5";
			view.PipeSegmentLength.Text = "5";
			view.PipeStartEndSegmentLength.Text = "5";

			view.PipeLength = "0 m";
			view.PipeHeightDelta = "0 cm";
			view.SegmentCount = "0";
		}
		
		
		
	}
}
