﻿using CadPlugin.Sewage.Create.Settings;
using CadPlugin.Sewage.Plannings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Sewage.Create
{
	struct ActionsRequired
	{
		public bool Recalculate { get; set; }
		public bool CancelEndPoint { get; set; }
	}
	/// <summary>
	/// Used to determine actions for different states.
	/// </summary>
	class DataViewStateManager
	{
		//NOTE End point should be canceled only when change can cause changing of x/y coordinates.
		// This is when segments are used, and slope of the pipe changes.
		// If segments are not used, any new point is good.
		// (if slope is fixed, changing any value in start shaft causes recalculation, but pipe slope remains, 
		// therefore x/y coordinates are not changed. If delta is fixed, changing can cause x/y change).

		//NOTE Values are updated internally (for example, change terrain height moves all height preserving deltas between).

		/// <summary>
		/// What actions are required when specific field is changed.
		/// </summary>
		/// <returns></returns>
		public static ActionsRequired StartShaftValueChanged(ShaftFields field, IPipeShaftData data)
		{

			bool recalc = false;
			bool cancel = false;
			switch (field)
			{
				case ShaftFields.Diameter:
					recalc = true;
					cancel = true;
					break;
				case ShaftFields.InnerSlope:
					// Pipe out is set manually by the user, therefore it is not related to inner slope.
					// However, if pipe out becomes related to inner slope, both actions should be performed.
					recalc = false;
					cancel = false;
					break;
				case ShaftFields.Cover:
					recalc = false;
					cancel = false;
					break;
				case ShaftFields.CoverTerrainDelta:
					recalc = false;
					cancel = false;
					break;
				case ShaftFields.Terrain:
					recalc = true;
					cancel = data.Pipe.SegmentsUsed && (data.EndShaft == null ? false : ((ShaftData)data.EndShaft).MinPipeOutDeltaUsed);
					break;
				case ShaftFields.TerrainPipeOutDelta:
					recalc = true;
					cancel = data.Pipe.SegmentsUsed && (data.EndShaft == null ? false : ((ShaftData)data.EndShaft).MinPipeOutDeltaUsed);
					break;
				case ShaftFields.PipeOut:
					recalc = true;
					cancel = data.Pipe.SegmentsUsed && (data.EndShaft == null ? false : ((ShaftData)data.EndShaft).MinPipeOutDeltaUsed);
					break;
				case ShaftFields.PipeOutBottomDelta:
					recalc = false;
					cancel = false;
					break;
				case ShaftFields.Bottom:
					recalc = false;
					cancel = false;
					break;
			}
			return new ActionsRequired()
			{
				Recalculate = recalc,
				CancelEndPoint = cancel
			};
		}
		/// <summary>
		/// What actions are required when specific field is changed.
		/// </summary>
		/// <returns></returns>
		public static ActionsRequired EndShaftValueChanged(ShaftFields field, IPipeShaftData data)
		{

			bool recalc = false;
			bool cancel = false;
			switch (field)
			{
				case ShaftFields.Diameter:
					recalc = true;
					cancel = true;
					break;
				case ShaftFields.InnerSlope:
					// Pipe out is set manually by the user, therefore it is not related to inner slope.
					// However, if pipe out becomes related to inner slope, both actions should be performed.
					recalc = false;
					cancel = false;
					break;
				case ShaftFields.Cover:
					recalc = false;
					cancel = false;
					break;
				case ShaftFields.CoverTerrainDelta:
					recalc = false;
					cancel = false;
					break;
				case ShaftFields.Terrain:
					recalc = true;
					cancel = data.Pipe.SegmentsUsed && (data.EndShaft == null ? false : ((ShaftData)data.EndShaft).MinPipeOutDeltaUsed);
					break;
				case ShaftFields.TerrainPipeOutDelta:
					recalc = true;
					cancel = data.Pipe.SegmentsUsed && (data.EndShaft == null ? false : ((ShaftData)data.EndShaft).MinPipeOutDeltaUsed);
					break;
				case ShaftFields.PipeOut:
					// Changing pipe out does not affect pipe slope - just pipe
					recalc = true;
					cancel = data.Pipe.SegmentsUsed && (data.EndShaft == null ? false : ((ShaftData)data.EndShaft).MinPipeOutDeltaUsed);
					break;
				case ShaftFields.PipeOutBottomDelta:
					recalc = false;
					cancel = false;
					break;
				case ShaftFields.Bottom:
					recalc = false;
					cancel = false;
					break;
			}
			return new ActionsRequired()
			{
				Recalculate = recalc,
				CancelEndPoint = cancel
			};
		}

		public static ActionsRequired PipeValueChanged(PipeFields field, IPipeShaftData data)
		{
			bool recalc = false;
			bool cancel = false;
			switch(field)
			{
				case PipeFields.Diameter:
					recalc = false;
					cancel = false;
					break;
				case PipeFields.MinSlope:
					recalc = true;// data.Pipe.SegmentsUsed && data.ApplyMinPipeSlope;
					cancel = data.Pipe.SegmentsUsed && data.ApplyMinPipeSlope;
					break;
				case PipeFields.SegmentLength:
					recalc = data.Pipe.SegmentsUsed;
					cancel = data.Pipe.SegmentsUsed;
					break;
				case PipeFields.StartEndSegmentLength:
					recalc = data.Pipe.SegmentsUsed;
					cancel = data.Pipe.SegmentsUsed;
					break;
				case PipeFields.SegmentsUsed:
					// If segments used becomes active.
					recalc = data.Pipe.SegmentsUsed;
					cancel = data.Pipe.SegmentsUsed;
					break;
				case PipeFields.StepAngle:
					recalc = data.Pipe.AngleSteppingUsed;
					cancel = data.Pipe.AngleSteppingUsed;
					break;
				case PipeFields.StepAngleUsed:
					recalc = data.Pipe.AngleSteppingUsed;
					cancel = data.Pipe.AngleSteppingUsed;
					break;
				case PipeFields.Slope:
					recalc = data.Pipe.SegmentsUsed;
					cancel = data.Pipe.SegmentsUsed;
					break;
				case PipeFields.MinDelta:
					recalc = (data.EndShaft == null ? false : ((ShaftData)data.EndShaft).MinPipeOutDeltaUsed); ;
					cancel = recalc;
					break;
			}
			return new ActionsRequired()
			{
				Recalculate = recalc,
				CancelEndPoint = cancel
			};
		}
	}
}
