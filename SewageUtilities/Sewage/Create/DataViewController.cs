﻿using MyUtilities.Geometry;
using CadPlugin.Sewage.Create.Settings;
using CadPlugin.Sewage.Create.ShaftControls;
using CadPlugin.Sewage.Plannings;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CadPlugin.Common;

namespace CadPlugin.Sewage.Create
{
	internal class DataViewController
	{
		public static double MinimumSegmentLength = 0.1;
		public IPipeShaftView View { get; private set; }
		public PipeShaftDataManager Manager { get; set; }
		private bool firstRun = false;
		/// <summary>
		/// When user selects a point, and it is canceled (due to data change which requires 
		/// new point selection), this is point which was selected previously.
		/// </summary>
		//public Point3d? canceledPoint { get; private set; }
		private ViewData ViewData;
		private IPipeShaftViewManager ViewManager;

		/// <summary>
		/// Text of currently focused textbox.
		/// </summary>
		private static string focusedTextBoxContent = null;
		private WaterDirection direction;
		/// <summary>
		/// Indicates data errors.
		/// </summary>
		private ErrorProvider globalError = null;
		/// <summary>
		/// When delta is so small that pipe in will go above terrain, this is warning message.
		/// </summary>
		private ErrorProvider minDeltaError = null;
		/// <summary>
		/// Point has been selected when dialog opened.
		/// </summary>
		public bool PointWasSelected;

		public DataViewController(IPipeShaftView view, PipeShaftDataManager mgr, bool firstRun, WaterDirection direction, ViewData viewData)
		{
			this.firstRun = firstRun;
			ViewData = viewData;
			View = view;
			Manager = mgr;
			this.direction = direction;

			switch (direction)
			{
				case WaterDirection.Downstream:
					PointWasSelected = mgr.Data.EndPointSet;
					ViewManager = new PipeShaftViewManagerDownstream((PipeShaftView)view, mgr, this);
					break;
				case WaterDirection.Upstream:
					PointWasSelected = mgr.Data.StartPointSet;
					ViewManager = new PipeShaftViewManagerUpstream((PipeShaftView)view, mgr, this);
					break;
			}

			if (firstRun)
				view.StepAngleUsed.Enabled = false;
			else
				view.StepAngleUsed.Checked = Manager.Data.Pipe.AngleSteppingUsed;

			view.StartShaft.DrawLabels.Checked = ViewData.DrawStartShaftLabels;
			view.EndShaft.DrawLabels.Checked = ViewData.DrawEndShaftLabels;

			((Form)view).FormClosing += (_, e) =>
			{
				// If end point is set, then pipe is not drawn. Check with user.
				ViewData.DrawStartShaftLabels = view.StartShaft.DrawLabels.Checked;
				ViewData.DrawEndShaftLabels = view.EndShaft.DrawLabels.Checked;
				if (view.CloseReason == CloseReason.Finish)
				{
					bool cancel = direction == WaterDirection.Downstream && Manager.Data.EndPointSet ||
									direction == WaterDirection.Upstream && Manager.Data.StartPointSet;
					if (cancel)
					{
						DialogResult res = MessageBox.Show("Point is selected, but pipe is not drawn.\n Proceed?", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
						if (res == DialogResult.Cancel)
							e.Cancel = true;
					}
				}
				if (view.CloseReason == CloseReason.Cancel)
				{ 
					bool cancel = Manager.DataHistory != null && Manager.DataHistory.Count() > 0;
					if (cancel)
					{
						DialogResult res = MessageBox.Show("Draw pipes and shafts will be deleted.\n Proceed?", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
						if (res == DialogResult.Cancel)
							e.Cancel = true;
					}
				}
			};
		}
		private void InitializeGlobalDataError()
		{
			if (globalError != null)
				return;
			globalError = new ErrorProvider();
			globalError.BlinkStyle = ErrorBlinkStyle.NeverBlink;
		}

		/// <summary>
		/// Tells if provided text is different than saved one
		/// (helps to determine if user has changed the value)
		/// </summary>
		bool HasFocusedTextChanged (string x)
		{
			// If null is provided, it is considered as different text
			// (because saved text can be null also).
			if (x == null)
				return true;
			bool b = x != focusedTextBoxContent;
			return b;
		}
		/// <summary>
		/// Saves text when entering to edit mode of TextBox,
		/// so it can be compared later to determine if user
		/// has changed the text.
		/// </summary>
		private void SaveFocusedText(object o, EventArgs e)
		{
			focusedTextBoxContent = null;
			TextBox t = o as TextBox;
			if (t == null)
				return;
			focusedTextBoxContent = t.Text;
		}
		/// <summary>
		/// Clears focused text. Until SaveFocusedText is called, 
		/// all calls to HasFocusedTextChanged will indicate that
		/// text has been changed.
		/// </summary>
		private void ClearFocusedText()
		{
			focusedTextBoxContent = null;
		}

		/// <summary>
		/// Checks if text inside text box is valid decimal number, 
		/// and sets Error to TextBox if it isn't.
		/// </summary>
		private bool CheckTextBoxValue(TextBox t)
		{
			InitializeGlobalDataError();
			globalError.SetError(t, null);

			System.Diagnostics.Debug.WriteLine("Checking value: " + t.Text);
			if (!DoubleHelper.IsValidString(t.Text))
			{
				globalError.SetError(t, "Invalid value");
				return false;
			}
			if (!CheckData())
				return false;
			return true;
		}
		private void InitializeStartShaft()
		{
			View.StartShaft.CreateShaft.CheckedChanged += (_, __) =>
			{
				bool newState = View.StartShaft.CreateShaft.Checked;
				// If newState is true, restore old state. 
				if (newState)
				{
					Manager.Data.RestoreStartShaft();
					if (!(View.EndShaft.DrawLabels.Tag as string == "disable"))
						View.StartShaft.DrawLabels.Enabled = true;
				}
				else
				{
					Manager.Data.SaveCurrentStartShaft();
					Manager.CreateStartShaft(false);
					View.StartShaft.DrawLabels.Enabled = false;
				}
				//CancelEndPoint();
				Recalculate();
				RefreshView();
			};

			/* Downstream - not used. Upstream - always used.
			View.StartShaft.MinPipeOutDeltaUsed.CheckedChanged += (sender, __) =>
			{
				View.StartShaft.MinPipeOutDeltaUsed.Enabled = View.StartShaft.MinPipeOutDeltaUsed.Checked;
				((ShaftData)Manager.Data.StartShaft).MinPipeOutDeltaUsed = View.StartShaft.MinPipeOutDeltaUsed.Checked;

				MinValue_CheckedChanged((CheckBox)sender);
			};
			View.StartShaft.MinPipeOutDeltaUsed.Checked = Manager.Data.ApplyStartShaftPipeOutDelta;*/

			Action<double, ShaftFields> startShaftUpdate = (d, type) =>
			{
				switch (type)
				{
					case ShaftFields.Diameter: d /= 1000.0; break;
					case ShaftFields.InnerSlope:
					case ShaftFields.CoverTerrainDelta:
					case ShaftFields.PipeOutBottomDelta:
						d /= 100.0;
						break;
				}
				Manager.UpdateStartShaft(d, type);

				ActionsRequired ar = DataViewStateManager.StartShaftValueChanged(type, Manager.Data);
				if (ar.Recalculate)
					Recalculate();
				if (ar.CancelEndPoint)
					CancelSelectedPoint();
			};

			View.StartShaft.MinPipeOutDelta.Enabled = View.StartShaft.MinPipeOutDeltaUsed.Checked;
			View.StartShaft.MinPipeOutDeltaUsed.Checked = Manager.Data.ApplyStartShaftPipeOutDelta;
			View.StartShaft.MinPipeOutDeltaUsed.CheckedChanged += (sender, __) =>
			{
				//View.StartShaft.MinPipeOutDelta.Enabled = View.StartShaft.MinPipeOutDeltaUsed.Checked;
				((ShaftData)Manager.Data.StartShaft).MinPipeOutDeltaUsed = View.StartShaft.MinPipeOutDeltaUsed.Checked;

				MinValue_CheckedChanged((CheckBox)sender);

				ViewManager.ToggleMinDeltaStart(View.StartShaft.MinPipeOutDeltaUsed.Checked);
			};

			Action<TextBox> startShaftHandler = (t) =>
			{
				string s = t.Text;
				if (!HasFocusedTextChanged(s))
					return;
				if (!CheckTextBoxValue(t))
					return;
				double d = DoubleHelper.ParseInvariantDouble(s);
				if (object.ReferenceEquals(t, View.StartShaft.Diameter))
					startShaftUpdate(d, ShaftFields.Diameter);
				if (object.ReferenceEquals(t, View.StartShaft.InnerSlope))
					startShaftUpdate(d, ShaftFields.InnerSlope);
				if (object.ReferenceEquals(t, View.StartShaft.CoverHeight))
					startShaftUpdate(d, ShaftFields.Cover);
				if (object.ReferenceEquals(t, View.StartShaft.CoverTerrainDelta))
					startShaftUpdate(d, ShaftFields.CoverTerrainDelta);
				if (object.ReferenceEquals(t, View.StartShaft.TerrainHeight))
					startShaftUpdate(d, ShaftFields.Terrain);
				if (object.ReferenceEquals(t, View.StartShaft.TerrainPipeOutDelta))
					startShaftUpdate(d, ShaftFields.TerrainPipeOutDelta);
				if (object.ReferenceEquals(t, View.StartShaft.PipeOutHeight))
				{
					((ShaftData)Manager.Data.StartShaft).MinPipeOutDelta = Manager.Data.StartShaft.TerrainHeight - d;
					startShaftUpdate(d, ShaftFields.PipeOut);
				}
				if (object.ReferenceEquals(t, View.StartShaft.PipeOutBottomDelta))
					startShaftUpdate(d, ShaftFields.PipeOutBottomDelta);
				if (object.ReferenceEquals(t, View.StartShaft.BottomHeight))
					startShaftUpdate(d, ShaftFields.Bottom);
				if (object.ReferenceEquals(t, View.StartShaft.MinPipeOutDelta))
				{

					((ShaftData)Manager.Data.StartShaft).MinPipeOutDelta = d;// Manager.Data.EndShaft.TerrainHeight - Manager.Data.EndShaft.PipeOutHeight;
					startShaftUpdate(d, ShaftFields.TerrainPipeOutDelta);
					// Update this value always, but checkbox controls if it is used.
				}

					RefreshView();
			};
			EventHandler startLostFocus = (o, __) =>
			{
				TextBox t = (TextBox)o;
				startShaftHandler(t);
			};
			// Commit changes on Enter keypress.
			KeyEventHandler startKeyHandler = (o, e) =>
			{
				TextBox t = (TextBox)o;
				if (e.KeyCode == Keys.Enter)
					startShaftHandler(t);
				focusedTextBoxContent = t.Text;
			};
			{
				// Start shaft.
				View.StartShaft.Diameter.GotFocus += SaveFocusedText;
				View.StartShaft.Diameter.LostFocus += startLostFocus;
				View.StartShaft.Diameter.KeyDown += startKeyHandler;

				View.StartShaft.InnerSlope.GotFocus += SaveFocusedText;
				View.StartShaft.InnerSlope.LostFocus += startLostFocus;
				View.StartShaft.InnerSlope.KeyDown += startKeyHandler;

				View.StartShaft.CoverHeight.GotFocus += SaveFocusedText;
				View.StartShaft.CoverHeight.LostFocus += startLostFocus;
				View.StartShaft.CoverHeight.KeyDown += startKeyHandler;

				View.StartShaft.CoverTerrainDelta.GotFocus += SaveFocusedText;
				View.StartShaft.CoverTerrainDelta.LostFocus += startLostFocus;
				View.StartShaft.CoverTerrainDelta.KeyDown += startKeyHandler;

				View.StartShaft.TerrainHeight.GotFocus += SaveFocusedText;
				View.StartShaft.TerrainHeight.LostFocus += startLostFocus;
				View.StartShaft.TerrainHeight.KeyDown += startKeyHandler;

				View.StartShaft.TerrainPipeOutDelta.GotFocus += SaveFocusedText;
				View.StartShaft.TerrainPipeOutDelta.LostFocus += startLostFocus;
				View.StartShaft.TerrainPipeOutDelta.KeyDown += startKeyHandler;

				View.StartShaft.PipeOutHeight.GotFocus += SaveFocusedText;
				View.StartShaft.PipeOutHeight.LostFocus += startLostFocus;
				View.StartShaft.PipeOutHeight.KeyDown += startKeyHandler;

				View.StartShaft.PipeOutBottomDelta.GotFocus += SaveFocusedText;
				View.StartShaft.PipeOutBottomDelta.LostFocus += startLostFocus;
				View.StartShaft.PipeOutBottomDelta.KeyDown += startKeyHandler;

				View.StartShaft.BottomHeight.GotFocus += SaveFocusedText;
				View.StartShaft.BottomHeight.LostFocus += startLostFocus;
				View.StartShaft.BottomHeight.KeyDown += startKeyHandler;

				View.StartShaft.MinPipeOutDelta.GotFocus += SaveFocusedText;
				View.StartShaft.MinPipeOutDelta.LostFocus += startLostFocus;
				View.StartShaft.MinPipeOutDelta.KeyDown += startKeyHandler;
			}
			View.StartShaft.MinPipeOutDeltaUsed.CheckedChanged += (_, __) =>
			{
				TextBox tb = View.StartShaft.MinPipeOutDelta;
				bool nextEnabled = View.StartShaft.MinPipeOutDeltaUsed.Checked;
				if (nextEnabled)
				{
					startShaftHandler(tb);
				}
				else
				{
					if (minDeltaError != null)
						minDeltaError.SetError(tb, null);
					ClearFocusedText();
				}
			};
			ViewManager.InitializeStartShaft();
		}
		private void InitializeEndShaft()
		{
			View.EndShaft.CreateShaft.CheckedChanged += (_, __) =>
			{
				bool newState = View.EndShaft.CreateShaft.Checked;
				if (newState)
				{
					View.EndShaft.MinPipeOutDeltaUsed.Enabled = true;
					Manager.Data.RestoreEndShaft();
					if (!(View.EndShaft.DrawLabels.Tag as string == "disable"))
						View.EndShaft.DrawLabels.Enabled = true;
				}
				else
				{
					//view.MinPipeSlope.Checked = true;
					View.EndShaft.MinPipeOutDeltaUsed.Enabled = false;
					Manager.Data.SaveCurrentEndShaft();
					Manager.CreateEndShaft(false);
					View.EndShaft.DrawLabels.Enabled = false;
				}
				//CancelEndPoint();
				Recalculate();
				RefreshView();
			};
			Action<double, ShaftFields> endShaftUpdate = (d, type) =>
			{
				switch (type)
				{
					case ShaftFields.Diameter: d /= 1000.0; break;
					case ShaftFields.InnerSlope:
					case ShaftFields.CoverTerrainDelta:
					case ShaftFields.PipeOutBottomDelta:
						d /= 100.0;
						break;
				}
				Manager.UpdateEndShaft(d, type);

				ActionsRequired ar = DataViewStateManager.EndShaftValueChanged(type, Manager.Data);
				if (ar.Recalculate)
					Recalculate();
				if (ar.CancelEndPoint)
					CancelSelectedPoint();
			};
			View.EndShaft.MinPipeOutDeltaUsed.CheckedChanged += (sender, __) =>
			{
				//View.EndShaft.MinPipeOutDelta.Enabled = View.EndShaft.MinPipeOutDeltaUsed.Checked;
				((ShaftData)Manager.Data.EndShaft).MinPipeOutDeltaUsed = View.EndShaft.MinPipeOutDeltaUsed.Checked;

				MinValue_CheckedChanged((CheckBox)sender);
				ViewManager.ToggleMinDeltaEnd(View.EndShaft.MinPipeOutDeltaUsed.Checked);
			};
			View.EndShaft.MinPipeOutDelta.Enabled = Manager.Data.ApplyEndShaftPipeOutDelta;
			View.EndShaft.MinPipeOutDeltaUsed.Checked = Manager.Data.ApplyEndShaftPipeOutDelta;

			Action<TextBox> endShaftHandler = (t) => 
			{
				string s = t.Text;
				if (!HasFocusedTextChanged(s))
					return;
				if (!CheckTextBoxValue(t))
					return;
				double d = DoubleHelper.ParseInvariantDouble(s);
				if (object.ReferenceEquals(t, View.EndShaft.Diameter))
					endShaftUpdate(d, ShaftFields.Diameter);
				if (object.ReferenceEquals(t, View.EndShaft.InnerSlope))
					endShaftUpdate(d, ShaftFields.InnerSlope);
				if (object.ReferenceEquals(t, View.EndShaft.CoverHeight))
					endShaftUpdate(d, ShaftFields.Cover);
				if (object.ReferenceEquals(t, View.EndShaft.CoverTerrainDelta))
					endShaftUpdate(d, ShaftFields.CoverTerrainDelta);
				if (object.ReferenceEquals(t, View.EndShaft.TerrainHeight))
					endShaftUpdate(d, ShaftFields.Terrain);
				if (object.ReferenceEquals(t, View.EndShaft.MinPipeOutDelta))
				{
					if (minDeltaError == null)
					{
						minDeltaError = new ErrorProvider();
						minDeltaError.Icon = SystemIcons.Warning;
						minDeltaError.BlinkStyle = ErrorBlinkStyle.NeverBlink;
					}
					else
						minDeltaError.Clear();
					if (d < Manager.Data.EndShaft.InnerSlopeDeltaH)
					{
						string msg = "Delta is too small. Pipe in height will be above terrain!";

						minDeltaError.Clear();
						minDeltaError.SetError(t, msg);
					}

					//if (Manager.Data.ApplyEndShaftPipeOutDelta)
						((ShaftData)Manager.Data.EndShaft).MinPipeOutDelta = d;// Manager.Data.EndShaft.TerrainHeight - Manager.Data.EndShaft.PipeOutHeight;

					endShaftUpdate(d, ShaftFields.TerrainPipeOutDelta);

					// Update this value always, but checkbox controls if it is used.
				}
				if (object.ReferenceEquals(t, View.EndShaft.PipeOutHeight))
				{
					((ShaftData)Manager.Data.EndShaft).MinPipeOutDelta = Manager.Data.EndShaft.TerrainHeight - d;
					endShaftUpdate(d, ShaftFields.PipeOut);
				}
				if (object.ReferenceEquals(t, View.EndShaft.PipeOutBottomDelta))
					endShaftUpdate(d, ShaftFields.PipeOutBottomDelta);
				if (object.ReferenceEquals(t, View.EndShaft.BottomHeight))
					endShaftUpdate(d, ShaftFields.Bottom);

				RefreshView();
			};
			EventHandler endLostFocus = (o, __) =>
			{
				TextBox t = (TextBox)o;
				endShaftHandler(t);
			};
			KeyEventHandler endKeyHandler = (o, e) =>
			{
				TextBox t = (TextBox)o;
				if (e.KeyCode == Keys.Enter)
					endShaftHandler(t);
				focusedTextBoxContent = t.Text;
			};
			{
				// End shaft.
				View.EndShaft.Diameter.GotFocus += SaveFocusedText;
				View.EndShaft.Diameter.LostFocus += endLostFocus;
				View.EndShaft.Diameter.KeyDown += endKeyHandler;

				View.EndShaft.InnerSlope.GotFocus += SaveFocusedText;
				View.EndShaft.InnerSlope.LostFocus += endLostFocus;
				View.EndShaft.InnerSlope.KeyDown += endKeyHandler;

				View.EndShaft.CoverHeight.GotFocus += SaveFocusedText;
				View.EndShaft.CoverHeight.LostFocus += endLostFocus;
				View.EndShaft.CoverHeight.KeyDown += endKeyHandler;

				View.EndShaft.CoverTerrainDelta.GotFocus += SaveFocusedText;
				View.EndShaft.CoverTerrainDelta.LostFocus += endLostFocus;
				View.EndShaft.CoverTerrainDelta.KeyDown += endKeyHandler;

				View.EndShaft.TerrainHeight.GotFocus += SaveFocusedText;
				View.EndShaft.TerrainHeight.LostFocus += endLostFocus;
				View.EndShaft.TerrainHeight.KeyDown += endKeyHandler;

				View.EndShaft.TerrainPipeOutDelta.GotFocus += SaveFocusedText;
				View.EndShaft.TerrainPipeOutDelta.LostFocus += endLostFocus;
				View.EndShaft.TerrainPipeOutDelta.KeyDown += endKeyHandler;

				View.EndShaft.PipeOutHeight.GotFocus += SaveFocusedText;
				View.EndShaft.PipeOutHeight.LostFocus += endLostFocus;
				View.EndShaft.PipeOutHeight.KeyDown += endKeyHandler;

				// Pipe out is set automatically.
				View.EndShaft.PipeOutBottomDelta.GotFocus += SaveFocusedText;
				View.EndShaft.PipeOutBottomDelta.LostFocus += endLostFocus;
				View.EndShaft.PipeOutBottomDelta.KeyDown += endKeyHandler;

				View.EndShaft.BottomHeight.GotFocus += SaveFocusedText;
				View.EndShaft.BottomHeight.LostFocus += endLostFocus;
				View.EndShaft.BottomHeight.KeyDown += endKeyHandler;

				View.EndShaft.MinPipeOutDelta.GotFocus += SaveFocusedText;
				View.EndShaft.MinPipeOutDelta.LostFocus += endLostFocus;
				View.EndShaft.MinPipeOutDelta.KeyDown += endKeyHandler;
				/*View.EndShaft.MinPipeOutDelta.EnabledChanged += (_, __) =>
				{
					TextBox tb = View.EndShaft.MinPipeOutDelta;
					bool nextEnabled = tb.Enabled;
					if (nextEnabled)
					{
						endShaftHandler(tb);
					}
					else
					{
						if (minDeltaError != null)
							minDeltaError.SetError(tb, null);
						ClearFocusedText();
					}
				};*/

				View.EndShaft.MinPipeOutDeltaUsed.CheckedChanged += (_, __) =>
				{
					TextBox tb = View.EndShaft.MinPipeOutDelta;
					bool nextEnabled = View.EndShaft.MinPipeOutDeltaUsed.Checked;
					if (nextEnabled)
					{
						endShaftHandler(tb);
					}
					else
					{
						if (minDeltaError != null)
							minDeltaError.SetError(tb, null);
						ClearFocusedText();
					}
				};
			}
			ViewManager.InitializeEndShaft();

		}
		private void InitializePipe()
		{
			View.SegmentsUsed.CheckedChanged += (_, __) =>
			{
				bool used = View.SegmentsUsed.Checked;
				Manager.ToggleSegmentsCreation(used);
				View.PipeSegmentLength.Enabled = used;
				View.PipeStartEndSegmentLength.Enabled = used;
				ActionsRequired ar = DataViewStateManager.PipeValueChanged(PipeFields.SegmentsUsed, Manager.Data);
				if (ar.Recalculate)
					Recalculate();
				if (ar.CancelEndPoint)
					CancelSelectedPoint();
				RefreshView();
			};
			View.StepAngleUsed.CheckedChanged += (_, __) =>
			{
				bool used = View.StepAngleUsed.Checked;
				View.StepAngle.Enabled = used;
				Manager.ToggleStepAngleUsed(used);
				ActionsRequired ar = DataViewStateManager.PipeValueChanged(PipeFields.StepAngleUsed, Manager.Data);
				if (ar.Recalculate)
					Recalculate();
				if (ar.CancelEndPoint)
					CancelSelectedPoint();
				RefreshView();
			};

			View.MinPipeSlopeUsed.CheckedChanged += (_, __) =>
			{
				View.MinPipeSlope.Enabled = View.MinPipeSlopeUsed.Checked;
				Manager.Data.ApplyMinPipeSlope = View.MinPipeSlopeUsed.Checked;
			};
			View.MinPipeSlopeUsed.Checked = Manager.Data.ApplyMinPipeSlope;
			Action<double, PipeFields> pipeUpdate = (d, type) =>
			{
				switch (type)
				{
					case PipeFields.Diameter: d /= 1000.0; break;
					case PipeFields.MinSlope: d /= 100.0; break;
				}
				Manager.UpdatePipe(d, type);

				ActionsRequired ar = DataViewStateManager.PipeValueChanged(type, Manager.Data);
				if (ar.Recalculate)
					Recalculate();
				if (ar.CancelEndPoint)
					CancelSelectedPoint();
			};
			Action<TextBox> pipeHandler = (t) =>
			{
				string s = t.Text;
				if (!HasFocusedTextChanged(s))
					return;
				if (!CheckTextBoxValue(t))
				{
					CancelSelectedPoint();
					return;
				}
				double d = DoubleHelper.ParseInvariantDouble(s);
				if (object.ReferenceEquals(t, View.PipeDiameter))
					pipeUpdate(d, PipeFields.Diameter);
				if (object.ReferenceEquals(t, View.MinPipeSlope))
				{
					//Manager.Data.MinPipeSlope = Manager.Data.Pipe.Slope;
					pipeUpdate(d, PipeFields.MinSlope);
					// Update this value always, but checkbox controls if it is used.
				}
				if (object.ReferenceEquals(t, View.PipeSegmentLength))
				{
					// Don't allow value less than 0.1 m for segment length.
					if (d < MinimumSegmentLength)
					{
						d = MinimumSegmentLength;
						t.Text = DoubleHelper.ToStringInvariant(d, 2);
					}
					pipeUpdate(d, PipeFields.SegmentLength);
				}
				if (object.ReferenceEquals(t, View.PipeStartEndSegmentLength))
					pipeUpdate(d, PipeFields.StartEndSegmentLength);
				if (object.ReferenceEquals(t, View.StepAngle))
					pipeUpdate(d, PipeFields.StepAngle);

				RefreshView();
			};
			EventHandler pipeLostFocus = (o, __) =>
			{
				TextBox t = (TextBox)o;
				pipeHandler(t);
			};
			KeyEventHandler pipeKeyHandler = (o, e) =>
			{
				TextBox t = (TextBox)o;
				if (e.KeyCode == Keys.Enter)
					pipeHandler(t);
				focusedTextBoxContent = t.Text;
			};
			{
				// Pipe.
				View.PipeDiameter.GotFocus += SaveFocusedText;
				View.PipeDiameter.LostFocus += pipeLostFocus;
				View.PipeDiameter.KeyDown += pipeKeyHandler;

				View.MinPipeSlope.GotFocus += SaveFocusedText;
				View.MinPipeSlope.LostFocus += pipeLostFocus;
				View.MinPipeSlope.KeyDown += pipeKeyHandler;

				View.PipeSegmentLength.GotFocus += SaveFocusedText;
				View.PipeSegmentLength.LostFocus += pipeLostFocus;
				View.PipeSegmentLength.KeyDown += pipeKeyHandler;

				View.PipeStartEndSegmentLength.GotFocus += SaveFocusedText;
				View.PipeStartEndSegmentLength.LostFocus += pipeLostFocus;
				View.PipeStartEndSegmentLength.KeyDown += pipeKeyHandler;

				View.StepAngle.GotFocus += SaveFocusedText;
				View.StepAngle.LostFocus += pipeLostFocus;
				View.StepAngle.KeyDown += pipeKeyHandler;
			}
		}


		private void MinValue_CheckedChanged(CheckBox sender)
		{
			if (!(View.MinPipeSlopeUsed.Checked || View.EndShaft.MinPipeOutDeltaUsed.Checked))
			{
				// If both are unchecked, check the opposite one.
				if (object.ReferenceEquals(sender, View.MinPipeSlopeUsed))
					View.EndShaft.MinPipeOutDeltaUsed.Checked = true;
				else
					View.MinPipeSlopeUsed.Checked = true;
				// This event will be triggered again, so return current trigger.
				return;
			}
			bool pointSet = false;
			switch (direction)
			{
				case WaterDirection.Downstream:pointSet = Manager.Data.EndPointSet; break;
				case WaterDirection.Upstream:pointSet = Manager.Data.StartPointSet; break;
			}
			
			if (!pointSet)
			{
				View.EndShaft.TerrainPipeOutDelta.Text = "-";
				View.MinPipeSlope.Text = "-";
				if (Manager.Data.ApplyMinPipeSlope)
				{
					View.MinPipeSlope.Text = DoubleHelper.ToStringInvariant(Manager.Data.MinPipeSlope, 2);
				}
				if (Manager.Data.ApplyEndShaftPipeOutDelta)
				{
					double pDelta = Manager.Data.EndShaft.TerrainHeight - Manager.Data.EndShaft.PipeOutHeight;
					View.EndShaft.TerrainPipeOutDelta.Text = DoubleHelper.ToStringInvariant(pDelta, 2);
				}
			}

			ActionsRequired ar = DataViewStateManager.PipeValueChanged(PipeFields.MinSlope, Manager.Data);
			if (ar.Recalculate)
				Recalculate();
			if (ar.CancelEndPoint)
				CancelSelectedPoint();
			RefreshView();
		}
		
		/// <summary>
		/// Displays data from data object.
		/// </summary>
		public void Initialize()
		{
			RefreshView();

			InitializeStartShaft();
			InitializeEndShaft();
			InitializePipe();

			if (firstRun)
			{
				//View.StartShaft.CreateShaft.Checked = true;
				//View.EndShaft.CreateShaft.Checked = true;
			}
			else
			{
				switch(direction)
				{
					case WaterDirection.Downstream: View.StartShaft.CreateShaft.Enabled = false; break;
					case WaterDirection.Upstream: View.EndShaft.CreateShaft.Enabled = false; break;
				}
				
				// Default checked - depends on previous selection.
			}
			View.StartShaft.CreateShaft.Checked = Manager.Data.StartShaft != null;
			View.EndShaft.CreateShaft.Checked = Manager.Data.EndShaft != null;

			ViewData.CanceledPoint = null;
			switch (direction)
			{
				case WaterDirection.Downstream:
					if (!Manager.Data.EndPointSet)
						CancelSelectedPoint();
					else
						ViewData.CanceledPoint = Manager.Data.EndPoint;
					break;
				case WaterDirection.Upstream:
					if (!Manager.Data.StartPointSet)
						CancelSelectedPoint();
					else
						ViewData.CanceledPoint = Manager.Data.StartPoint;
					break;
			}
			
			if (Manager.Data.Pipe.SegmentLength < MinimumSegmentLength)
				((PipeData)Manager.Data.Pipe).SegmentLength = MinimumSegmentLength;

			//View.EndShaft.TerrainPipeOutDelta.Enabled = false;

			CheckData();
		}
		

		/// <summary>
		/// Variable point is canceled (end point for downstream,
		/// start point for upstream)
		/// </summary>
		private void CancelSelectedPoint()
		{
			// If already canceled, don't do it again.


			if (!View.DrawRequest.Enabled)
				return;
			View.DrawRequest.Enabled = false;
			switch (direction)
			{
				case WaterDirection.Downstream:Manager.Data.EndPointSet = false; break;
				case WaterDirection.Upstream:Manager.Data.StartPointSet = false; break;
			}

			View.PipeSlope.Text = "-";

			ToolTip t = new ToolTip();
			t.ShowAlways = true;

			string msg = "Please select End point first.";
			if (direction == WaterDirection.Upstream)
				msg = "Please select start point first";
			t.SetToolTip(View.DrawRequestPanel, msg);
			
			Color color = Color.Black;
			View.DrawRequestPanel.MouseLeave += (_, __) =>
			{
				t.Hide(View.DrawRequestPanel);
				View.SelectPoint.ForeColor = color;
			};
			View.DrawRequestPanel.MouseEnter += (_, __) =>
			{
				//t.Hide(View.DrawRequestPanel);
				//t.Show("Please select End point first.", View.DrawRequestPanel);
				//t.Show("Please select End point first.", View.DrawRequestPanel);
				t.Show(msg, View.DrawRequestPanel, View.DrawRequestPanel.Width, View.DrawRequestPanel.Height / 2);

				//System.Diagnostics.Debug.WriteLine(Cursor.Position);
				View.SelectPoint.ForeColor = Color.Red;
			};
			
		}
		/// <summary>
		/// Repopulates data values to view.
		/// </summary>
		private void RefreshView()
		{
			//bool endPointSet = Manager.Data.EndPointSet;
			Func<double, string> format = d1 => { return DoubleHelper.ToStringInvariant(d1, 2); };
			Func<double, string> doubleToCm = val =>
			{
				string res = "";
				val = val * 100.0;
				double rounded = Math.Round(val);

				if (Math.Abs(rounded - val) >= 0.01)
					res = format(val);
				else
					res = ((int)rounded).ToString();
				return res;
			};

			Action<IShaftData, IShaftControlView> setShaftData = (data, view) =>
			{
				if (data != null)
				{
					view.CreateShaft.Checked = true;
					int d = ((int)(data.Diameter * 1000.0));
					view.Diameter.Text = d.ToString();
					double slope = data.InnerSlope * 100.0;
					view.InnerSlope.Text = format(slope);
					view.CoverHeight.Text = format(data.CoverHeight);
					double delta1 = data.CoverHeight - data.TerrainHeight;
					view.CoverTerrainDelta.Text = doubleToCm(delta1);
					view.TerrainHeight.Text = format(data.TerrainHeight);
					double delta2 = data.TerrainHeight - data.PipeOutHeight;
					view.TerrainPipeOutDelta.Text = format(delta2);
					view.PipeOutHeight.Text = format(data.PipeOutHeight);
					double delta3 = data.PipeOutHeight - data.BottomHeight;
					view.PipeOutBottomDelta.Text = doubleToCm(delta3);
					view.BottomHeight.Text = format(data.BottomHeight);

					double depth = data.TerrainHeight - data.BottomHeight;
					view.Depth.Text = format(depth);
					double innerSlope = data.InnerSlopeDeltaH * 100;
					view.InnerDelta.Text = "Delta: " + format(innerSlope) + "cm";
				}
				else
				{
					view.CreateShaft.Checked = false;
				}
			};

			setShaftData(Manager.Data.StartShaft, View.StartShaft);
			setShaftData(Manager.Data.EndShaft, View.EndShaft);

			View.EndShaft.MinPipeOutDeltaUsed.Enabled = Manager.Data.EndShaft != null;
			if (Manager.Data.EndShaft != null)
			{
				View.EndShaft.MinPipeOutDeltaUsed.Checked = Manager.Data.EndShaft.MinPipeOutDeltaUsed;
				View.EndShaft.MinPipeOutDelta.Enabled = Manager.Data.EndShaft.MinPipeOutDeltaUsed;
				View.EndShaft.MinPipeOutDelta.Text = format(Manager.Data.EndShaft.MinPipeOutDelta);
			}

			if (Manager.Data.StartShaft != null)
				View.StartShaft.MinPipeOutDelta.Text = format(Manager.Data.StartShaft.MinPipeOutDelta);

			double slopeP = Manager.Data.Pipe.Slope * 100;
			View.PipeSlope.Text = format(slopeP);
			View.MinPipeSlope.Text = format(Manager.Data.MinPipeSlope * 100.0);
			int dm = (int)(Manager.Data.Pipe.Diameter * 1000.0);
			View.PipeDiameter.Text = dm.ToString();
			View.PipeSegmentLength.Text = format(Manager.Data.Pipe.SegmentLength);
			View.PipeStartEndSegmentLength.Text = format(Manager.Data.Pipe.StartEndSegmentLength);

			Point3d v1 = Manager.Data.Pipe.StartPoint;
			Point3d v2 = Manager.Data.Pipe.EndPoint;
			double length2d = new Point2d(v1.X, v1.Y).GetVectorTo(new Point2d(v2.X, v2.Y)).Length;
			double length = new Point3d(v1.X, v1.Y, v1.Z).GetVectorTo(new Point3d(v2.X, v2.Y, v2.Z)).Length;
			View.PipeLength2d = format(length2d);
			View.PipeLength = format(length);
			string heightDelta = "0";
			if (Manager.Data.StartPointSet && Manager.Data.EndPointSet)
			{
				double heightD = Manager.Data.StartPoint.Z - Manager.Data.EndPoint.Z;
				heightD *= 100.0;
				heightDelta = ((int)(Math.Round(heightD))).ToString();
			}
			View.PipeHeightDelta = heightDelta;
			View.SegmentCount = Manager.Data.Pipe.SegmentCount.ToString();

			View.PipeAngle = "-";
			View.StepAngle.Text = format(Manager.Data.Pipe.AngleStepDeg);
			//View.StepAngleUsed.Checked = Manager.Data.Pipe.AngleSteppingUsed;
			if (Manager.PreviousDataExists)
			{
				double angle = Manager.DataHistory.Last().Pipe.Direction.GetAngleDegXY(Manager.Data.Pipe.Direction);
				View.PipeAngle = format(angle);
			}
			if (!Manager.Data.Pipe.SegmentsUsed)
			{
				View.PipeSegmentLength.Text = "-";
				View.PipeStartEndSegmentLength.Text = "-";
			}
			View.SegmentsUsed.Checked = Manager.Data.Pipe.SegmentsUsed;
		}
		
		/// <summary>
		/// Recalculates data if no segments are used and end point is set.
		/// </summary>
		private void Recalculate()
		{
			bool pointSet = false;
			switch (direction)
			{
				case WaterDirection.Downstream:
					pointSet = Manager.Data.EndPointSet;
					break;
				case WaterDirection.Upstream:
					pointSet = Manager.Data.StartPointSet;
					break;
			}
			if (Manager.Data.Pipe.SegmentsUsed ||
				!pointSet) 
				return;
			Manager.Recalculate();
		}



		/// <summary>
		/// Checks if all data is valid (when data is valid, it is also updated in data structure).
		/// </summary>
		/// <returns></returns>
		public bool CheckData(bool checkStartShaft = true, bool checkPipe = true, bool checkEndShaft = true)
		{
			InitializeGlobalDataError();
			globalError.Clear();
			Func<TextBox, bool> checkTextBoxForDouble = (t) =>
			{
				//error.Clear();
				globalError.SetError(t, null);
				string s = t.Text;
				
				if (!DoubleHelper.IsValidString(s))
				{
					globalError.SetError(t, "Invalid value!");
					return false;
				}
				return true;
			};

			bool valid = true;
			if (checkStartShaft && View.StartShaft.CreateShaft.Checked)
			{
				// Commented are automatically calculated, no need to check.
				valid &= checkTextBoxForDouble(View.StartShaft.Diameter);
				valid &= checkTextBoxForDouble(View.StartShaft.InnerSlope);
				//valid &= checkTextBoxForDouble(View.StartShaft.CoverHeight);
				valid &= checkTextBoxForDouble(View.StartShaft.CoverTerrainDelta);
				//valid &= checkTextBoxForDouble(View.StartShaft.TerrainHeight);
				valid &= checkTextBoxForDouble(View.StartShaft.TerrainPipeOutDelta);
				if (Manager.Data.ApplyStartShaftPipeOutDelta)
					valid &= checkTextBoxForDouble(View.StartShaft.MinPipeOutDelta);
				//valid &= checkTextBoxForDouble(View.StartShaft.PipeOutHeight);
				valid &= checkTextBoxForDouble(View.StartShaft.PipeOutBottomDelta);
				//valid &= checkTextBoxForDouble(View.StartShaft.BottomHeight);
			}

			if (checkEndShaft && View.EndShaft.CreateShaft.Checked)
			{
				// Commented are automatically calculated, no need to check.
				valid &= checkTextBoxForDouble(View.EndShaft.Diameter);
				valid &= checkTextBoxForDouble(View.EndShaft.InnerSlope);
				//valid &= checkTextBoxForDouble(View.EndShaft.CoverHeight);
				valid &= checkTextBoxForDouble(View.EndShaft.CoverTerrainDelta);
				//valid &= checkTextBoxForDouble(View.EndShaft.TerrainHeight);
				if (Manager.Data.ApplyEndShaftPipeOutDelta)
					valid &= checkTextBoxForDouble(View.EndShaft.MinPipeOutDelta);
				//valid &= checkTextBoxForDouble(View.EndShaft.PipeOutHeight);
				valid &= checkTextBoxForDouble(View.EndShaft.PipeOutBottomDelta);
				//valid &= checkTextBoxForDouble(View.EndShaft.BottomHeight);
			}

			if (checkPipe)
			{
				if (Manager.Data.ApplyMinPipeSlope)
					valid &= checkTextBoxForDouble(View.MinPipeSlope);
				valid &= checkTextBoxForDouble(View.PipeDiameter);
				if (Manager.Data.Pipe.SegmentsUsed)
				{
					valid &= checkTextBoxForDouble(View.PipeSegmentLength);
					valid &= checkTextBoxForDouble(View.PipeStartEndSegmentLength);
				}
				if (Manager.Data.Pipe.AngleSteppingUsed)
					valid &= checkTextBoxForDouble(View.StepAngle);
			}

			return valid;
		}

	}
}
