﻿using CadPlugin.Sewage.Planning;
using CadPlugin.Sewage.Plannings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Sewage.Create.Totals
{
	class TotalsViewController
	{
		private Totals view;

		public TotalsViewController(Totals view)
		{
			this.view = view;
		}

		public void UpdateData(PipeShaftDataManager mgr)
		{
			if (!mgr.PreviousDataExists)
			{

				view.PipesCount = "0";
				view.PipesLength = "0.00 m";
				view.PipesSegments = "0";
				view.PipesStartEndSegments = "0";
				view.ShaftsCount = "0";
			}
			else
			{
				int pipesNr = mgr.DataHistory.Count();
				int segmentsNr = mgr.DataHistory.Sum(x => x.PipeData.SegmentCount);
				int startendSegments = mgr.DataHistory.Sum(x => Math.Abs(x.PipeData.StartEndSegmentLength) < 0.01 ? 0 : 2);
				double length = mgr.DataHistory.Sum(x => x.PipeData.StartPoint.GetVectorTo(x.PipeData.EndPoint).Length);

				int shaftsCount = mgr.DataHistory.Sum(x => x.EndShaft != null ? 1 : 0);
				if (mgr.DataHistory.First().StartShaft != null)
					shaftsCount++;

				view.PipesCount = pipesNr.ToString();
				view.PipesLength = length.ToString("0.00") + " m";
				view.PipesSegments = segmentsNr.ToString();
				view.PipesStartEndSegments = startendSegments.ToString();
				view.ShaftsCount = shaftsCount.ToString();
			}

		}
	}
}
