﻿namespace CadPlugin.Sewage.Create.Totals
{
	partial class Totals
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.labelShaftsCount = new System.Windows.Forms.Label();
			this.labelSegmentsCount = new System.Windows.Forms.Label();
			this.labelPipeLength = new System.Windows.Forms.Label();
			this.labelPipesCreated = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.labelStartEndSegment = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.labelStartEndSegment);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.labelShaftsCount);
			this.groupBox1.Controls.Add(this.labelSegmentsCount);
			this.groupBox1.Controls.Add(this.labelPipeLength);
			this.groupBox1.Controls.Add(this.labelPipesCreated);
			this.groupBox1.Location = new System.Drawing.Point(3, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(270, 102);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Totals";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(164, 28);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(79, 13);
			this.label1.TabIndex = 7;
			this.label1.Text = "Shafts created:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(7, 62);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(60, 13);
			this.label2.TabIndex = 6;
			this.label2.Text = "Segments: ";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 45);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(69, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "Total length: ";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 28);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(78, 13);
			this.label4.TabIndex = 4;
			this.label4.Text = "Pipes created: ";
			// 
			// labelShaftsCount
			// 
			this.labelShaftsCount.AutoSize = true;
			this.labelShaftsCount.Location = new System.Drawing.Point(249, 28);
			this.labelShaftsCount.Name = "labelShaftsCount";
			this.labelShaftsCount.Size = new System.Drawing.Size(13, 13);
			this.labelShaftsCount.TabIndex = 3;
			this.labelShaftsCount.Text = "0";
			// 
			// labelSegmentsCount
			// 
			this.labelSegmentsCount.AutoSize = true;
			this.labelSegmentsCount.Location = new System.Drawing.Point(122, 62);
			this.labelSegmentsCount.Name = "labelSegmentsCount";
			this.labelSegmentsCount.Size = new System.Drawing.Size(19, 13);
			this.labelSegmentsCount.TabIndex = 2;
			this.labelSegmentsCount.Text = "12";
			// 
			// labelPipeLength
			// 
			this.labelPipeLength.AutoSize = true;
			this.labelPipeLength.Location = new System.Drawing.Point(109, 45);
			this.labelPipeLength.Name = "labelPipeLength";
			this.labelPipeLength.Size = new System.Drawing.Size(45, 13);
			this.labelPipeLength.TabIndex = 1;
			this.labelPipeLength.Text = "12.54 m";
			// 
			// labelPipesCreated
			// 
			this.labelPipesCreated.AutoSize = true;
			this.labelPipesCreated.Location = new System.Drawing.Point(128, 28);
			this.labelPipesCreated.Name = "labelPipesCreated";
			this.labelPipesCreated.Size = new System.Drawing.Size(13, 13);
			this.labelPipesCreated.TabIndex = 0;
			this.labelPipesCreated.Text = "0";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(7, 81);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(104, 13);
			this.label5.TabIndex = 8;
			this.label5.Text = "Start/End segments:";
			// 
			// labelStartEndSegment
			// 
			this.labelStartEndSegment.AutoSize = true;
			this.labelStartEndSegment.Location = new System.Drawing.Point(122, 81);
			this.labelStartEndSegment.Name = "labelStartEndSegment";
			this.labelStartEndSegment.Size = new System.Drawing.Size(19, 13);
			this.labelStartEndSegment.TabIndex = 9;
			this.labelStartEndSegment.Text = "12";
			// 
			// Totals
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.groupBox1);
			this.Name = "Totals";
			this.Size = new System.Drawing.Size(276, 108);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label labelShaftsCount;
		private System.Windows.Forms.Label labelSegmentsCount;
		private System.Windows.Forms.Label labelPipeLength;
		private System.Windows.Forms.Label labelPipesCreated;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label labelStartEndSegment;
		private System.Windows.Forms.Label label5;
	}
}
