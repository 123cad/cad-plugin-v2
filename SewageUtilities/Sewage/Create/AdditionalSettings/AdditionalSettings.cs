﻿using CadPlugin.Sewage.Plannings;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Sewage.Create.AdditionalSettings
{
	class AdditionalSettings
	{
		/// <summary>
		/// Depends on background color of autocad/bricsad.
		/// </summary>
		public static Color DefaultTextColor = Color.Black;
		internal TextDisplayOrdered TextOrder { get; private set; }
		/// <summary>
		/// Color of the arrow displaying downstream direction.
		/// </summary>
		internal Color ArrowColor { get; set; }
		/// <summary>
		/// Draws water direction arrow inside pipe - scaled to pipe diameter.
		/// </summary>
		internal bool DrawArrowOnPipe { get; set; }
		/// <summary>
		/// Draws water direction arrow before text - scaled to text size.
		/// </summary>
		internal bool DrawArrowInText { get; set; }
		/// <summary>
		/// Indicates if angle between old pipe and new one should be displayed.
		/// </summary>
		internal bool ShowAngle { get; set; }
		private static AdditionalSettings __Instance;
		public static AdditionalSettings Instance
		{
			get
			{
				if (__Instance == null)
				{
					__Instance = new AdditionalSettings();
					Persistence.DataPersistence.LoadLabels(__Instance.TextOrder);
				}
				return __Instance;
			}
		}
		private AdditionalSettings()
		{
			// Load default values.
			TextOrder = new TextDisplayOrdered();
			ArrowColor = DefaultTextColor;
			ShowAngle = true;
		}
	}
}
