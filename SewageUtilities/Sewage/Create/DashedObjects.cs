﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Sewage.Plannings;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Sewage.Create
{
	/// <summary>
	/// When reselecting end point, show old objects as dashed - this class does it.
	/// </summary>
	class DashedObjects
	{
		/// <summary>
		/// Gets flag indicating if any object has been created.
		/// </summary>
		public bool ObjectsCreated { get; private set; }
		private Database db;
		private Editor ed;
		private List<ObjectId> createdObjects;
		public static string DashedLayer = "123sewage_dashed_temp";
		public DashedObjects(Database db, Editor ed)
		{
			this.db = db;
			this.ed = ed;
			createdObjects = new List<ObjectId>();
		}

		public void Create(PipeShaftDataManager manager, MyUtilities.Geometry.Point3d? oldPoint)
		{
			ObjectsCreated = oldPoint.HasValue;// manager.Data.EndPointSet;
			if (ObjectsCreated)
			{
				Common.CADLayerHelper.AddLayerIfDoesntExist(db, DashedLayer);
				//ObjectId linetypeId = Common.CADLineTypeHelper.CreateLineType(db, "123cad_dashed", 0.5, -0.5);
				ObjectId linetypeId = Common.CADLineTypeHelper.GetLineTypeId(db, "DASHED2");
				Common.CADLayerHelper.SetLayerLineType(db, DashedLayer, linetypeId);
				using (Transaction tr = db.TransactionManager.StartTransaction())
				{
					BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
					BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
					List<Entity> temp = EntitiesManager.DrawEntities(db, tr, btr, manager.Data);
					foreach (Entity e in temp)
						e.Layer = DashedLayer;
					createdObjects.AddRange(temp.Select(x => x.ObjectId));

					tr.Commit();
				}
			}
			ed.Regen();
		}
		public void Delete()
		{
			if (ObjectsCreated)
			{
				using (Transaction tr = db.TransactionManager.StartTransaction())
				{
					foreach (ObjectId id in createdObjects)
					{
						DBObject obj = tr.GetObject(id, OpenMode.ForWrite);
						obj.Erase();
					}
					tr.Commit();
				}
				Common.CADLayerHelper.DeleteLayers(db, DashedLayer);
			}

		}

	}
}
