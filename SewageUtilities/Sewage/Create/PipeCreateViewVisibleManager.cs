﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Sewage.Create
{
	/// <summary>
	/// Makes form transparent.
	/// </summary>
	class PipeCreateViewVisibleManager
	{
		private PipeShaftView View;
		public PipeCreateViewVisibleManager(PipeShaftView view)
		{
			View = view;

			view.KeyDown += View_KeyDown;
			view.KeyUp += View_KeyUp;
		}

		private void View_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			View.Opacity = 1;
		}

		private void View_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == System.Windows.Forms.Keys.ShiftKey)
			{
				View.Opacity = 0.05;
			}
		}
	}
}
