﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.Sewage.Create.ShaftControls
{
	public partial class ShaftControl : UserControl, IShaftControlView
	{
		public ShaftControl()
		{
			InitializeComponent();
			checkBoxCreateShaft.CheckedChanged += (_, __) => { /*groupBoxShaft.Enabled = checkBoxCreateShaft.Checked && checkBoxCreateShaft.Enabled;*/ };
			checkBoxCreateShaft.EnabledChanged += (_, __) => { /*groupBoxShaft.Enabled = checkBoxCreateShaft.Enabled && checkBoxCreateShaft.Checked;*/ };

			/*textBoxMinShaftDelta.VisibleChanged += (_, __) =>
			{
				labelMinDelta.Visible = textBoxMinShaftDelta.Visible;
			};*/
			SetGermanLanguage();
		}
		private void SetGermanLanguage()
		{
			labelDiameter.Text = "Durchmesser:";
			labelInnerSlope.Text = "Innen-Neigung:";
			labelTerrainHeight.Text = "Deckel-Höhe:";
			labelDelta.Text = "Delta:";
			labelPipeOut.Text = "Sohl-Höhe:";
			checkBoxMinShaftDelta.Text = "Min.-Delta";
		}
		/*
		/// <summary>
		/// Enables all controls.
		/// </summary>
		public void EnableControl()
		{
			ToggleEnableState(true);
		}
		/// <summary>
		/// Disables all controls.
		/// </summary>
		public void DisableControl()
		{
			ToggleEnableState(false);
		}

		private void ToggleEnableState(bool enabled)
		{
			textBoxShaftDiameter.Enabled = enabled;
			textBoxShaftInnerSlope.Enabled = enabled;
			textBoxShaftCover.Enabled = enabled;
			textBoxShaftCoverTerrainDelta.Enabled = enabled;
			textBoxShaftTerrain.Enabled = enabled;
			textBoxShaftTerrainPipeOutDelta.Enabled = enabled;
			textBoxShaftPipeOut.Enabled = enabled;
			textBoxShaftPipeOutBottomDelta.Enabled = enabled;
			textBoxShaftBottom.Enabled = enabled;

			textBoxMinShaftDelta.Enabled = enabled;
		}
		*/

		public TextBox CoverHeight
		{
			get
			{
				return textBoxShaftCover;
			}
		}

		public TextBox CoverTerrainDelta
		{
			get
			{
				return textBoxShaftCoverTerrainDelta;
			}
		}

		public CheckBox CreateShaft
		{
			get
			{
				return checkBoxCreateShaft;
			}			
		}

		public TextBox Depth
		{
			get
			{
				return textBoxShaftDepth;
			}
		}

		public TextBox Diameter
		{
			get
			{
				return textBoxShaftDiameter;
			}
		}

		public Label InnerDelta
		{
			get
			{
				return labelInnerDelta;
			}
		}

		public TextBox InnerSlope
		{
			get
			{
				return textBoxShaftInnerSlope;
			}
		}

		public TextBox PipeOutBottomDelta
		{
			get
			{
				return textBoxShaftPipeOutBottomDelta;
			}
			
		}

		public TextBox TerrainHeight
		{
			get
			{
				return textBoxShaftTerrain;
			}
		}

		public TextBox TerrainPipeOutDelta
		{
			get
			{
				return textBoxShaftTerrainPipeOutDelta;
			}
		}
		public string Description
		{
			set
			{
				checkBoxCreateShaft.Text = "" + value;
				groupBoxShaft.Text = value;// + " settings";
			}
			get
			{
				return "";
			}
		}
		public TextBox PipeOutHeight { get { return textBoxShaftPipeOut; } }
		public TextBox BottomHeight { get { return textBoxShaftBottom; } }

		public TextBox MinPipeOutDelta
		{
			get
			{
				return textBoxMinShaftDelta; 
			}
		}

		public CheckBox MinPipeOutDeltaUsed
		{
			get
			{
				return checkBoxMinShaftDelta;
			}
		}
		public CheckBox DrawLabels
		{
			get
			{
				return checkBoxDrawLabels;
			}
		}

		public Label MinDelta
		{
			get
			{
				return labelMinDelta;
			}
		}

	}
}
