﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.Sewage.Create.ShaftControls
{
	interface IShaftControlView
	{
		CheckBox CreateShaft { get; }
		TextBox Diameter { get; }
		TextBox InnerSlope { get; }
		Label InnerDelta { get; }
		TextBox CoverHeight { get; }
		TextBox CoverTerrainDelta { get; }
		TextBox TerrainHeight { get; }
		TextBox TerrainPipeOutDelta { get; }
		TextBox PipeOutHeight { get; }
		TextBox PipeOutBottomDelta { get; }
		TextBox BottomHeight { get; }
		/// <summary>
		/// Terrain height to Bottom delta.
		/// </summary>
		TextBox Depth { get; }
		/// <summary>
		/// Value of minimum distance from terrain height to pipe out.
		/// </summary>
		TextBox MinPipeOutDelta { get; }
		CheckBox MinPipeOutDeltaUsed { get; }
		CheckBox DrawLabels { get; }

		Label MinDelta { get; }


		/// <summary>
		/// Text displayed to the user, to describe (ie. start/end shaft)
		/// </summary>
		string Description { set; }



		
	}
}
