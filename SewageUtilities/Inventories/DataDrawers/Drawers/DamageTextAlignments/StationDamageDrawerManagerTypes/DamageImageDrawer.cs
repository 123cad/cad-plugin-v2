﻿using CadPlugin.Inventories.Drawers.DamageDescriptionAlignments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.Damages;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.DataDrawers.Drawers.DamageTextAlignments.StationDamageDrawerManagerTypes
{
	enum ImageAlignment
	{
		Horizontal,
		PerpendicularToPipe
	}

	/// <summary>
	/// Draws damage codes as images.
	/// </summary>
	class DamageImageDrawer : StationDamagesDrawer
	{
		private CadImageDrawerWrapper imageDrawWrapper;
		protected readonly ImageAlignment imageAlignment;

		public DamageImageDrawer(CadImageDrawerWrapper cadWrapper, ImageAlignment alignment = ImageAlignment.PerpendicularToPipe)
		{
			imageDrawWrapper = cadWrapper;
			imageAlignment = alignment;
		}

		/// <summary>
		/// Image must be oriented so crack is in top position (0 on the clock).
		/// Converts clock position to angle in radians.
		/// For clock = 
		/// </summary>
		/// <param name="clock"></param>
		/// <returns></returns>
		private static double ClockPositionToAngleRad(int clock)
		{
			double angleDeg = -clock * 30;
			return angleDeg * Math.PI / 180;
		}

		/// <summary>
		/// Sets position of images in station. They are aligned in one line.
		/// </summary>
		/// <param name="stationDistance"></param>
		/// <param name="position"></param>
		/// <param name="damages"></param>
		/// <returns></returns>
		public override StationDamageDescriptionAligned AlignStationTexts(double stationDistance, double radius, PositionVector position, IEnumerable<SingleDamage> damages)
		{
			var drawnDamages = damages.Where(x => x.IsDrawn).ToList();
			StationDamageDescriptionAligned t = new StationDamageDescriptionAligned(position, stationDistance);
			int numberOfText = drawnDamages.Count;

			// Text is aligned with pipe considered to be vertical.
			// Then everything is just rotated to match the pipe angle.

			Vector3d directionVector = Vector3d.YAxis;
			Vector3d textDirection = Vector3d.XAxis;

			double totalHeight = TextHeight;
			// Width of picture is same as height of picture.
			double totalWidth = numberOfText * (TextHeight + TextPadding) - TextPadding;

			double step = TextHeight + TextPadding;

			double distance = TextOffset + radius;
			t.PositionTextMerge = position.Position.Add(textDirection.FromCADVector().Multiply(distance)).ToCADPoint();

			Point3d bottomLeftImagePoint = t.PositionTextMerge.FromCADPoint().
										Add(directionVector.FromCADVector().Negate().Multiply(TextHeight / 2)).
										ToCADPoint();
			
			foreach (SingleDamage dp in drawnDamages)
			{
				SingleDescriptionPosition p = new SingleDescriptionPosition();
				p.Text = dp.GetDisplayText();
				p.DamageClass = dp.DamageClass;
				// Not rotating anymore.
				p.RotationRad = 0;//ClockPositionToAngleRad(dp.ClockPosition);
				var temp = bottomLeftImagePoint;
				p.Position = new Point3d(temp.X, temp.Y, position.Position.Z);
				t.Texts.Add(p);

				bottomLeftImagePoint = bottomLeftImagePoint.Add(textDirection.MultiplyBy(step));
			}

			Vector3d tV = new Vector3d(position.Direction.X, position.Direction.Y, 0);
			double angleRad = directionVector.GetAngleTo(tV);
			// Yaxis (relative vector to which everything is drawn) is

			// Image damages are always parallel to X = 0. 
			if (position.Direction.X > 0)
				angleRad *= -1;
			// Text must be -Pi/2 to Pi/2 respectively to the user horizont.
			if (position.Direction.Y < 0)
				angleRad += Math.PI;
			t.AngleDeg = angleRad * 180 / Math.PI;
			return t;
		}

		public override IEnumerable<Entity> DrawText(TransactionData data, StationDamageDescriptionAligned station, bool draw3d)
		{
			List<Entity> entities = new List<Entity>();

			AdjustHeight adjustZ = new AdjustHeight(draw3d);

			var pos = station.Position.Position;
			pos = adjustZ.GetPoint(pos);
			//if (!draw3d)
				//pos = new MyUtilities.Geometry.Point3d(pos.X, pos.Y, 0);

			imageDrawWrapper.DrawDamages(data, entities, station, TextHeight, TextHeight);

			DrawDamageStationAndClass(data, station, adjustZ, entities, pos.ToCADPoint(), station.Texts.Max(x => x.DamageClass));

			Matrix3d transform = new Matrix3d();
			double angleRad = station.AngleDeg * Math.PI / 180;
			switch (imageAlignment)
			{
				case ImageAlignment.Horizontal:
					var moveVector = pos.GetVectorTo(station.PositionTextMerge.FromCADPoint());
					var oldPoint = moveVector.GetAsPoint();
					var newVector = moveVector.ToCADVector().RotateBy(angleRad, Vector3d.ZAxis);
					moveVector = oldPoint.GetVectorTo(newVector.FromCADVector().GetAsPoint());
					transform = Matrix3d.Displacement(moveVector.ToCADVector());
					break;
				case ImageAlignment.PerpendicularToPipe:
					transform = Matrix3d.Rotation(angleRad, Vector3d.ZAxis, station.Position.Position.ToCADPoint());
					break;
			}

			foreach (Entity e in entities)
			{
				
				/*string layer = "";
				if (SewageObject is Pipe)
					layer = Pipe.TextDamageLayer;
				else
					layer = Shaft.TextLayer;
				CADLayerHelper.AddLayerIfDoesntExist(SewageObject.TransactionData.Db, layer);
				e.Layer = layer;*/
				e.TransformBy(transform);
			}
			return entities;
		}
	}
}
