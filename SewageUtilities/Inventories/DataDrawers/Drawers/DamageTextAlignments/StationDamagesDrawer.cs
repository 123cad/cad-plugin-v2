﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.Colors;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.Drawers.DamageDescriptionAlignments
{
	public abstract class StationDamagesDrawer
	{
		private static double __TextHeight;
		/// <summary>
		/// Gets or sets heigth of the text (updates all other dimensions).
		/// </summary>
		public static double TextHeight
		{
			get
			{
				return __TextHeight;
			}
			set
			{
				var val = value;
				if (val < 0.05)
					val = 0.1;
				__TextHeight = val;
				TextPadding = TextHeight * 0.3;
				TextOffset = TextHeight * 3.0 / 4;
				TextDistance = TextOffset * 0.9;
				StationDistance = TextOffset / 2;
				StationUnderlineDistance = TextPadding / 2;
				DamageClassPadding = TextHeight * 0.2;
			}
		}
		/// <summary>
		/// Vertical distance between text on the single station.
		/// </summary>
		protected static double TextPadding { get; private set; }
		public SewageObject SewageObject { get; private set; }

        //NOTE
        // Text is drawn according to Y axis. All points are calculated according to this direction.
        // Real direction is considered as angle, in which all entities are rotated, upon adding to the drawing.
        // So all text appears 90 deg rotated from the pipe.

        // For details, check picture in the folder.

        /// <summary>
        /// Distance of the merge point from the pipe (x coordinate).
        /// </summary>
        protected static double TextOffset { get; private set; }
        /// <summary>
        /// Distance text from the merge point (by x coordinate)
        /// </summary>
        protected static double TextDistance { get; private set; }
        /// <summary>
        /// Distance of the station text from the pipe (x coordinate).
        /// </summary>
        protected static double StationDistance { get; private set; }
        /// <summary>
        /// How much is station text above underline.
        /// </summary>
        protected static double StationUnderlineDistance { get; private set; }
        protected static double DamageClassPadding { get; private set; }

        static StationDamagesDrawer()
        {
            TextHeight = 0.5;
        }

        /// <summary>
        /// Aligns damages which belong to the single station.
        /// </summary>
        public abstract StationDamageDescriptionAligned AlignStationTexts(double stationDistance, double radius, PositionVector position, IEnumerable<Damages.SingleDamage> damages);// IEnumerable<DamageDrawingProperties> station)


        /// <summary>
        /// Draws descriptions which have already been aligned.
		/// If draw3d is false, all elements should have z=0.
        /// </summary>
        /// <param name="station"></param>
        public abstract IEnumerable<Entity> DrawText(TransactionData data, StationDamageDescriptionAligned station, bool draw3d);

		protected void DrawDamageStationAndClass(TransactionData data, StationDamageDescriptionAligned station, AdjustHeight adjustZ, List<Entity> entities, Point3d stationPosition, DamageClass maxDamageClass)
		{
			// Draw station distance and Damage class.
			string stationDistanceText = DoubleHelper.ToStringInvariant(station.Distance, 2);
			MText t = new MText();
			Line underline = new Line();
			data.Btr.AppendEntity(t);
			data.Btr.AppendEntity(underline);
			data.Tr.AddNewlyCreatedDBObject(t, true);
			data.Tr.AddNewlyCreatedDBObject(underline, true);
			entities.Add(t);
			entities.Add(underline);
			//t.Color = TextColor;
			t.Contents = stationDistanceText;
			t.TextHeight = TextHeight;
			t.Location = adjustZ.GetPoint(station.Position.Position.ToCADPoint());// new Point3d(station.Position.Position.X, station.Position.Position.Y, 0);

			// Move to final place.
			Extents3d ext = t.GeometricExtents;
			double deltaX = ext.MaxPoint.X - ext.MinPoint.X;
			underline.StartPoint = adjustZ.GetPoint(stationPosition.Add(new Vector3d(-deltaX - StationDistance, 0, 0)));
			underline.EndPoint = adjustZ.GetPoint(stationPosition);
			t.TransformBy(Matrix3d.Displacement(new Vector3d(-deltaX - StationDistance, StationUnderlineDistance + (ext.MaxPoint.Y - ext.MinPoint.Y), 0)));

			// Draw damage class (from InventoryManager)
			Point3d topLeftRectangle = underline.StartPoint.Add(new Vector3d(0, -StationUnderlineDistance, 0));
			MText damage = data.CreateEntity<MText>();// new MText();
			Polyline damagePl = data.CreateEntity<Polyline>(); //new Polyline3d();
			entities.Add(damage);
			entities.Add(damagePl);
			damage.Contents = DamageClassConverter.ToString(maxDamageClass);
			damage.TextHeight = TextHeight;
			//damage.Color = TextColor;
			ext = damage.GeometricExtents;
			double damageLenght = ext.MaxPoint.X - ext.MinPoint.X;
			double rectLength = DamageClassPadding * 2 + damageLenght;
			double rectHeigth = DamageClassPadding * 2 + TextHeight;
			damage.Location = topLeftRectangle.Add(new Vector3d(DamageClassPadding, -DamageClassPadding, 0));
			damage.Location = adjustZ.GetPoint(damage.Location);

			damagePl.AddPoints(topLeftRectangle.GetAs2d()
								, topLeftRectangle.Add(new Vector3d(rectLength, 0, 0)).GetAs2d()
								, topLeftRectangle.Add(new Vector3d(rectLength, -rectHeigth, 0)).GetAs2d()
								, topLeftRectangle.Add(new Vector3d(0, -rectHeigth, 0)).GetAs2d()
								);
			damagePl.Elevation = adjustZ.GetPoint(topLeftRectangle).Z;
			damagePl.Color = ColorAssociations.GetDamageClassColor(maxDamageClass);
			damagePl.LineWeight = LineWeight.LineWeight030;
			damagePl.Closed = true;
		}
		/// <summary>
		/// If we want 2d drawing, all z heights set to 0.
		/// </summary>
		protected class AdjustHeight
		{
			private bool draw3d = true;
			public AdjustHeight(bool draw3d)
			{
				this.draw3d = draw3d;
			}
			public Point3d GetPoint(Point3d pt)
			{
				if (draw3d)
					return pt;
				return pt.GetOnZ();
			}
			public MyUtilities.Geometry.Point3d GetPoint(MyUtilities.Geometry.Point3d pt)
			{
				if (draw3d)
					return pt;
				return pt.GetOnZ();
			} 
		}
	}
}
