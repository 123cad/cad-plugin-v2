﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CadPlugin.Inventories;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Inventories.Drawers.DamageDescriptionAlignments
{
	/// <summary>
	/// All text in this station is on good position (angle says when drawing how much should be rotated)
	/// </summary>
	public class StationDamageDescriptionAligned
	{ 
		/// <summary>
		/// Rotation angle of this station.
		/// </summary>
		public virtual double AngleDeg
		{
			get;
			set;
		}

		/// <summary>
		/// TopLeft corner of text (in 2d because 3d is not important)
		/// </summary>
		public virtual Point2d TopLeft
		{
			get;
			private set;
		}

		/// <summary>
		/// TopLeft corner of text (in 2d because 3d is not important)
		/// </summary>
		public virtual Point2d BottomLeft
		{
			get;
			private set;
		}
		/// <summary>
		/// Position of station on the pipe.
		/// </summary>
		public PositionVector Position { get; private set; }
		/// <summary>
		/// Position where all text lines split.
		/// </summary>
		public Point3d PositionTextMerge { get; set; }
		public virtual IList<SingleDescriptionPosition> Texts
		{
			get;
			private set;
		}
		/// <summary>
		/// Distance of this station.
		/// </summary>
		public double Distance { get; private set; }
		public StationDamageDescriptionAligned(PositionVector p, double distance)
		{
			Texts = new List<SingleDescriptionPosition>();
			Position = p;
			Distance = distance;
		}


		
	}
}

