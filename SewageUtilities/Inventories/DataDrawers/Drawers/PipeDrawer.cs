﻿using CadPlugin.Common;
using CadPlugin.Inventories.Pipes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.Damages;
using CadPlugin.Inventories.DataDrawers.Damages;
using refTables = Isybau2015.ReferenceTables;
using refTablesEnum = Isybau2015.Isybau2015.ReferenceTableEnums;
using abwasser = Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens;
using inspizierte = Isybau2015.Identifikations.Datenkollektives.Zustandsdatenkollektivs.InspizierteAbwassertechnischeAnlagens;
using CadPlugin.Inventories.Linetypes;
using CadPlugin.Inventories.DataDrawers.Drawers.Linetypes;
using CadPlugin.Inventories.DataDrawers.Drawers;
using static CadPlugin.Inventories.InventoryManager;
using CadPlugin.Inventories.DataDrawers.Drawers.Layers;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.Drawers
{
	abstract class PipeDrawer
	{
		/// <summary>
		/// If false, all data is drawn on z = 0.
		/// </summary>
		public bool IsDrawn3d { get; private set; }

		/// <summary>
		/// If true, pipe and shafts are drawn as solids.
		/// </summary>
		public virtual bool IsDrawnVisual3d => false;

		/// <summary>
		/// Draws separate pipe with color of highest damage on it.
		/// </summary>
		public bool DrawDamagePipe { get; set; } = true;

		/// <summary>
		/// Is pipe label drawn.
		/// </summary>
		public abstract bool DrawPipeLabel { get; }

		public bool IsArrowDrawn { get; set; }
		public TransactionData Tr { get; private set; }
		public SewageObjectColorManager ColorManager { get; private set; } 
		public PipeLinetype PipeLinetypeManager { get; private set; }
		public DamageTextDrawer DamageTextManager { get; private set; }
		public DamageDrawerFilter DamageFilter { get; private set; }
		public TextSizeDefinitions TextSize { get; private set; }

		public InventoryManager.CreatedCADEntities CreatedEntities { get; private set; }
		/// <summary>
		/// Weight of line which represents highest pipe damage (== pipe length).
		/// </summary>
		public LineWeight PipeHighestDamageWeight { get; set; } = LineWeight.LineWeight050;
		/// <summary>
		/// Weight of line damage.
		/// </summary>
		public LineWeight PipeDamageLineWeight { get; set; } = LineWeight.LineWeight120;
		/// <summary>
		/// Weight of point damage.
		/// </summary>
		public LineWeight PipeDamageWeight { get; set; } = LineWeight.LineWeight080;

		private bool IsInitialized = false;

        public static readonly string BlockNameBCA = "Anschlusspunkt_Rohr_BCA";
        public static readonly string BlockNameBCC = "Rohr_Knickpunkt_BCC";
        public static readonly string BlockNamePipeMiddle = "Rohr_Daten";

		protected PipeDrawers.PipeSingleLineDrawer singleLineDrawer = null;

		public PipeDrawer(TransactionData data, SewageObjectColorManager pipeColor, PipeLinetype pipeLinetype, 
				DamageTextDrawer dmgText, TextSizeDefinitions textDef, DamageDrawerFilter damageFilter, CreatedCADEntities createdEntities, bool drawIn3d)
		{
			Tr = data;
			ColorManager = pipeColor;
			DamageTextManager = dmgText;
			PipeLinetypeManager = pipeLinetype;
			DamageFilter = damageFilter;
			TextSize = textDef;
			CreatedEntities = createdEntities;
			IsDrawn3d = drawIn3d;
			if (!IsInitialized)
			{
				// Initialized on first instance of pipe drawer.
				// PipeDrawer will only one use Linetypes, and we need to load them to the current document.
				//TODO ** Problems: flag IsInitialized is static, which means it will be loaded one time - next document
				// which wants to draw linetypes may fail (quick test shows it doesn't fail).
				IsInitialized = true;
				LinetypeManager.LoadLinetypes(data.Tr, data.Db);
                string blocksPath = @"C:\Program Files\Common Files\123CAD Blöcke\";

                CADBlockHelper.LoadBlockFromOutside(data.Db, blocksPath, BlockNameBCA);
                CADBlockHelper.LoadBlockFromOutside(data.Db, blocksPath, BlockNameBCC);
				CADBlockHelper.LoadBlockFromOutside(data.Db, blocksPath, BlockNamePipeMiddle);
			}
			IsArrowDrawn = true;
		}
		/// <summary>
		/// Draws pipe damages with text.
		/// </summary>
		/// <param name="pipe"></param>
		public abstract void DrawDamages(Pipe pipe);

		public abstract void DrawPipe(Pipe pipe);

		public void SetSingleLineDrawer(PipeDrawers.PipeSingleLineDrawer singleDrawer)
		{
			if (singleDrawer == null)
				throw new ArgumentNullException("SingleLine drawer can't be null!");

			singleLineDrawer = singleDrawer;
		}
		protected virtual void DrawArrow(Pipe pipe)
		{
			Polyline pl = new Polyline();
			Tr.Btr.AppendEntity(pl);
			Tr.Tr.AddNewlyCreatedDBObject(pl, true);
			pl.Color = ColorManager.ObjectColor.GetObjectColor();

			double diameter = pipe.Diameter;
			double arrowWidth = diameter * 0.8;
			double arrowLength = diameter * 1.5;
			MyUtilities.Geometry.Vector3d v3 = pipe.StartPoint.GetVectorTo(pipe.EndPoint);
			MyUtilities.Geometry.Vector2d v2 = v3.GetAs2d();
			MyUtilities.Geometry.Vector2d unit = v2.GetUnitVector();
            MyUtilities.Geometry.Vector2d startV = unit.Multiply(v2.Length / 2).Subtract(unit.Multiply(arrowLength / 2));

			MyUtilities.Geometry.Point2d start2 = pipe.StartPoint.GetAs2d().Add(startV);
            MyUtilities.Geometry.Point2d end2 = start2.Add(unit.Multiply(arrowLength));

			pl.AddVertexAt(0, end2.ToCADPoint(), 0, 0, 0);
			pl.AddVertexAt(0, start2.ToCADPoint(), 0, arrowWidth, 0);

			CreatedEntities.AddPipeEntity(pl);
			string layerName = LayerManager.Instance.GetMainDataLayer(pipe.Abwasser);
			pl.Layer = layerName;
			pl.Elevation = (pipe.StartPoint.Z + pipe.EndPoint.Z) / 2;
		}

		public PositionVector FindMiddle(List<MyUtilities.Geometry.Point3d> points, ref double totalLength)
        {
            PositionVector pos = new PositionVector();
            List<MyUtilities.Geometry.Vector3d> lines = new List<MyUtilities.Geometry.Vector3d>();
            List<double> lengths = new List<double>();
            for (int i = 0; i < points.Count - 1; i++)
            {
                var v = points[i].GetVectorTo(points[i + 1]);
                lengths.Add(v.Length);
                lines.Add(v);
            }

            totalLength = lengths.Sum();
            double middle = totalLength / 2;
            double currentTotalLength = 0;
            for (int i = 0; i < points.Count - 1; i++)
            {
                double currentMiddle = middle - currentTotalLength;
                double currentLength = lengths[i];
                if (currentMiddle <= currentLength)
                {
                    pos = new PositionVector();
                    pos.Direction = lines[i].GetUnitVector();
                    pos.Position = points[i].Add(pos.Direction.Multiply(currentMiddle));
                    break;
                }

                currentTotalLength += lengths[i];
            }

            return pos;
        }
		
        public BlockReference DrawMiddleBlock(Pipe p, List<MyUtilities.Geometry.Point3d> points)// PositionVector position)
        {
			if (points.Count < 2)
				return null;
            double totalLength = 0;
            PositionVector position = FindMiddle(points, ref totalLength);

            AbwassertechnischeAnlagen ab = p.Abwasser;
            Point3d middle = position.Position.ToCADPoint();
            Dictionary<string, string> atts = new Dictionary<string, string>();
            atts.Add("NAME", ab.Objektbezeichnung);
            atts.Add("STATUS", ab.Status.ToString() + " - " + Isybau2015.ReferenceTables.G.G105.Instance.GetValue(p.Abwasser.Status.ToString()));
            var owner = p.Abwasser?.GetOwner();
            if (owner != null)
                atts.Add("BETREIBER", owner);
            DateTime? dt1 = p?.Inspizierte?.OptischeInspektion?.Inspektionsdatum;
            DateTime? dt2 = p?.InspizierteUpstream?.OptischeInspektion?.Inspektionsdatum;
            string date = "";
            if (dt1.HasValue && dt2.HasValue)
            {
                // dt1 is after dt2
                if (dt1.Value.CompareTo(dt2) > 0)
                    date = p.Inspizierte.OptischeInspektion.InspektionsdatumFormatted;
                else
                    date = p.Inspizierte.OptischeInspektion.InspektionsdatumFormatted;
            }
            else if (dt1.HasValue || dt2.HasValue)
            {
                if (dt1.HasValue)
                    date = p.Inspizierte.OptischeInspektion.InspektionsdatumFormatted;
                else
                    date = p.InspizierteUpstream.OptischeInspektion.InspektionsdatumFormatted;
            }
            atts.Add("DATE-INSP", date);
            atts.Add("ART", p.Abwasser.Entwaesserungsart);
            var kante = (abwasser.Objektarts.Kante)p.Abwasser.Auswahlelement;
            atts.Add("TYPE", refTables.G.G200.Instance.GetValue(kante.KantenTyp.ToString()));

            string funktion = "";
            switch (kante.KantenTyp)
            {
                case (int)refTablesEnum.G200_Kantentyp.Haltung:
                    var haltung = (abwasser.Objektarts.Kantes.Haltung)kante.Auswahlelement;
                    var t = haltung?.HaltungsFunktion;
                    if (t.HasValue)
                        funktion = t + " - " + refTables.G.G201.Instance.GetValue(t.ToString());
                    break;
                case (int)refTablesEnum.G200_Kantentyp.Leitung: 
                    var leitung = (abwasser.Objektarts.Kantes.Leitung)kante.Auswahlelement;
                    t = leitung?.LeitungsFunktion;
                    if (t.HasValue)
                        funktion = t + " - " + refTables.G.G202.Instance.GetValue(t.ToString());
                    break;
                case (int)refTablesEnum.G200_Kantentyp.Rinne:
                    var rinne = (abwasser.Objektarts.Kantes.Rinne)kante.Auswahlelement;
                    t = rinne?.RinnenFunktion;
                    if (t.HasValue)
                        funktion = t + " - " + refTables.G.G203.Instance.GetValue(t.ToString());
                    break;
                case (int)refTablesEnum.G200_Kantentyp.Gerinne:
                    var gerinne = (abwasser.Objektarts.Kantes.Gerinne)kante.Auswahlelement;
                    t = gerinne?.GerinnerFunktion;
                    if (t.HasValue)
                        funktion = t + " - " + refTables.G.G204.Instance.GetValue(t.ToString());
                    break;
            }
            atts.Add("FUNKTION", funktion);
            if (kante.Profil != null)
            {
                string part = kante.Profil.Profilart  + " - "+ refTables.G.G205.Instance.GetValue(kante.Profil.Profilart.ToString());
                atts.Add("PROFIL-ART", part);
            }
            atts.Add("3D-LAENGE", DoubleHelper.ToStringInvariant(totalLength, 3));
			Shafts.Shaft start = null, end = null;
			if (p.GetStartEndShaft(ref start, ref end))
			{
				double smp2dLength = start.Center.GetAs2d().GetVectorTo(end.Center.GetAs2d()).Length;
				atts.Add("2D-LAENGE", DoubleHelper.ToStringInvariant(smp2dLength, 2));
			}

            Color c = ColorManager.ObjectColor.GetObjectColor();
			Dictionary<string, Color> attColors = new Dictionary<string, Color>();
            attColors.Add("NAME", c);
			var middlePosition = middle;
            var br = Common.CADBlockHelper.CreateBlockReference(Tr.Tr, Tr.Bt, Tr.Btr, BlockNamePipeMiddle, middlePosition, atts, attColors);
			CADBlockHelper.SetAttributesTextSize(br, Tr.Tr, TextSize.PipeTextSize);

			CreatedEntities.AddPipeEntity(br);
			string layerName = LayerManager.Instance.GetMainDataLayer(p.Abwasser);
			br.Layer = layerName;
			return br;
		}
        protected void InsertBlockBCA(DamageStation st, Pipe pipe, SingleDamage dmg, PositionVector pos)
        {
            var attributes = new Dictionary<string, string>();
            attributes.Add("HOEHE", DoubleHelper.ToStringInvariant(pos.Position.Z, 2));
            attributes.Add("TYPE", "BCA");
            var rzus = ((SingleDamagePipe)dmg).rZustand;
			var aart = rzus?.Charakterisierung1 == "A" ? "Abzweiger" : "Stutzen";// rzus?.Parent?.RGrunddaten?.Anschlussdaten?.AnschlussArt;
            aart = aart ?? "";
           // aart = Isybau2015.ReferenceTables.G.G206.Instance.GetValue(aart);
            attributes.Add("ARTANSCHLUSS", aart);
			string ch2 = rzus?.Charakterisierung2 == "A" ? "Offen" : "Geschlossen";
			string art = "NOT SET";
			art = (pipe.Inspizierte?.OptischeInspektion?.Auswahlelement as inspizierte.OptischeInspektions.Rohrleitung)?.RGrunddaten?.Kanalart ??
				(pipe.InspizierteUpstream?.OptischeInspektion?.Auswahlelement as inspizierte.OptischeInspektions.Rohrleitung)?.RGrunddaten?.Kanalart ??"";
			attributes.Add("ART", art);
			//var art = pipe.Inspizierte.
			attributes.Add("CH2", ch2);
			//attributes.Add("ART", pipe.Inspizierte?.en);
			attributes.Add("DURCHMESSER", dmg.Q1.Trim()); 
            string date = pipe.Inspizierte?.OptischeInspektion?.InspektionsdatumFormatted;
            if (date == null)
                date = pipe.InspizierteUpstream?.OptischeInspektion?.InspektionsdatumFormatted;
            attributes.Add("DATE-INSP", date ?? "");
			var position = pos.Position;
            var br = Common.CADBlockHelper.CreateBlockReference(Tr.Tr, Tr.Bt, Tr.Btr, BlockNameBCA, position.ToCADPoint(), attributes);
			br.SetAttributesTextSize(Tr.Tr, TextSize.PipeTextSize);

			CreatedEntities.AddPipeEntity(br);

			var layers = LayerManager.Instance.GetStatusDataLayer(pipe.Abwasser);
			br.Layer = layers.Station;
		}
		protected void InsertBlockBCC(DamageStation st, Pipe pipe, SingleDamage dmg, PositionVector pos)
        {
            var attributes = new Dictionary<string, string>();
            attributes.Add("HOEHE", DoubleHelper.ToStringInvariant(pos.Position.Z, 2));
            attributes.Add("TYPE", "BCC");
            string ch1 = "";
            string ch2 = "";
            switch (dmg.Ch1?.Trim()??"")
            {
                case "A": ch1 = "Links"; break;
                case "B": ch1 = "Rechts"; break;
            }
            switch (dmg.Ch2?.Trim()??"")
            {
                case "A":ch2 = "Oben";break;
                case "B":ch2 = "Unten";break;
            }
            attributes.Add("CH1", ch1);
            attributes.Add("CH2", ch2);
            attributes.Add("WINKEL", dmg.Q1?.Trim()??"");
            string date = pipe.Inspizierte?.OptischeInspektion?.InspektionsdatumFormatted;
            if (date == null)
                date = pipe.InspizierteUpstream?.OptischeInspektion?.InspektionsdatumFormatted;
            attributes.Add("DATE-INSP", date ?? "");

			var position = pos.Position;
			var br = Common.CADBlockHelper.CreateBlockReference(Tr.Tr, Tr.Bt, Tr.Btr, BlockNameBCC, position.ToCADPoint(), attributes);
			br.SetAttributesTextSize(Tr.Tr, TextSize.PipeTextSize);

			CreatedEntities.AddPipeEntity(br);
			var layers = LayerManager.Instance.GetStatusDataLayer(pipe.Abwasser);
			br.Layer = layers.Station;
		}
	}
}
