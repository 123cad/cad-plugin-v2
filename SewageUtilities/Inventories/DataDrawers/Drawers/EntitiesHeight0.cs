﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.DataDrawers.Drawers
{
	class EntitiesHeight0
	{
		public static void SetZ(IEnumerable<Entity> entities, Transaction tr, double newZ = 0)
		{
			foreach(var e in entities)
			{
				if (SetZ(e as Line, newZ, tr)) continue;
				if (SetZ(e as BlockReference, newZ, tr)) continue;
				if (SetZ(e as Circle, newZ, tr)) continue;
				if (SetZ(e as DBPoint, newZ, tr)) continue;
				if (SetZ(e as Polyline, newZ, tr)) continue;
				if (SetZ(e as Polyline3d, newZ, tr)) continue;
				if (SetZ(e as DBText, newZ, tr)) continue;
				if (SetZ(e as MText, newZ, tr)) continue;
			}
		}
		private static bool CheckEntity(Entity e)
		{
			if (e == null)
				return false;
			if (!e.IsWriteEnabled)
				e.UpgradeOpen();
			return true;
		}
		private static bool SetZ(Line l, double z, Transaction tr)
		{
			if (!CheckEntity(l))
				return false;
			l.StartPoint = new Point3d(l.StartPoint.X, l.StartPoint.Y, z);
			l.EndPoint = new Point3d(l.EndPoint.X, l.EndPoint.Y, z);
			return true;
		}
		private static bool SetZ(BlockReference br, double z, Transaction tr)
		{
			if (!CheckEntity(br))
				return false;
			double deltaZ = br.Position.Z - z;
			Matrix3d move = Matrix3d.Displacement(new Vector3d(0, 0, -deltaZ));
			br.TransformBy(move);
			//br.Position = new Point3d(br.Position.X, br.Position.Y, z);
			return true;
		}
		private static bool SetZ(Circle c, double z, Transaction tr)
		{
			if (!CheckEntity(c))
				return false;
			c.Center = new Point3d(c.Center.X, c.Center.Y, z);
			return true;
		}
		private static bool SetZ(DBPoint p, double z, Transaction tr)
		{
			if (!CheckEntity(p))
				return false;
			p.Position = new Point3d(p.Position.X, p.Position.Y, z);
			return true;
		}
		private static bool SetZ(Polyline pl, double z, Transaction tr)
		{
			if (!CheckEntity(pl))
				return false;
			pl.Elevation = z;
			return true;
		}
		private static bool SetZ(Polyline3d pl, double z, Transaction tr)
		{
			if (!CheckEntity(pl))
				return false;
			pl.SetZ(tr, z);
			return true;
		}
		private static bool SetZ(DBText t, double z, Transaction tr)
		{
			if (!CheckEntity(t))
				return false;
			t.Position = new Point3d(t.Position.X, t.Position.Y, z);
			return true;
		}
		private static bool SetZ(MText t, double z, Transaction tr)
		{
			if (!CheckEntity(t))
				return false;
			t.Location = new Point3d(t.Location.X, t.Location.Y, z);
			return true;
		}


	}
}






