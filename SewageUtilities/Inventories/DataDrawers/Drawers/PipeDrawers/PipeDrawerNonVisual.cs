﻿using CadPlugin.Inventories.Drawers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.DataDrawers.Drawers.Linetypes;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.Pipes;
using CadPlugin.Inventories.Damages;
using CadPlugin.Inventories.DataDrawers.Drawers.Layers;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers
{
	/// <summary>
	/// Represents the base class for any drawer which is not visual 3d.
	/// All non visual 3d drawers have some things in common, and this is the place to group those things.
	/// </summary>
	abstract class PipeDrawerNonVisual : PipeDrawer
	{
		public override bool DrawPipeLabel => true;
		public PipeDrawerNonVisual(TransactionData data, SewageObjectColorManager pipeColor, PipeLinetype pipeLinetype, DamageTextDrawer dmgText, TextSizeDefinitions textDef, DamageDrawerFilter damageFilter, InventoryManager.CreatedCADEntities createdEntities, bool drawIn3d) 
							: base(data, pipeColor, pipeLinetype, dmgText, textDef, damageFilter, createdEntities, drawIn3d)
		{
		}
		/// <summary>
		/// Draws pipe line with provided setting (no text is drawn, just pure pipe).
		/// </summary>
		/// <param name="pipe"></param>
		/// <param name="linetype"></param>
		/// <param name="c"></param>
		/// <returns></returns>
		protected abstract IEnumerable<Entity> CreatePipeLineWithSettings(Pipe pipe, string linetype, Color c, string layerName = null);
		/// <summary>
		/// Draws pipe damages and texts.
		/// </summary>
		protected void DrawPipeDamages(Pipe pipe, FilteredDrawingData filteredData)
		{
			if (filteredData.Stations.Count == 0)
				return;
			// This method is used to ensure order of drawn data.
			// Entity which is drawn last appears on top of other.
			// We want to have pipeHighestDamage at the bottom, line damage at the top, point damage at the top.
			// Otherwise, longer damages would hide shorter ones.
			DamageClass highest = DamageClass.ClassNotSet;
			if (filteredData.Stations.Count > 0)
			foreach(var st in filteredData.Stations)
			{
				if (st.Damages.Count == 0)
					continue;
					foreach (var dmg in st.Damages)
						if (highest < dmg.DamageClass)
							highest = dmg.DamageClass;
			}

			DrawHighestPipeDamagePipe(pipe, highest);
			DrawPipeDamages(pipe, highest, filteredData, false, true);
			DrawPipeDamages(pipe, highest, filteredData, true, false);
			DrawPipeStationText(pipe, highest, filteredData);
		}
		private void DrawPipeDamages(Pipe pipe, DamageClass highest, FilteredDrawingData filteredData, bool drawPointDamage, bool drawLineDamage)
		{
			var layer = LayerManager.Instance.GetStatusDataLayer(pipe.Abwasser);
			foreach (FilteredStation fst in filteredData.Stations)
			{
				DamageStation st = fst.Station;
				double startDistance = st.Distance;
				PositionVector position = pipe.GetPositionAtDistance(startDistance).Value;
				foreach (SingleDamage dmg in fst.Damages)
				{
					// Check if line damage is drawn.
					if (dmg.IsLineDamage && !drawLineDamage)
						continue;
					// Check if point damage is drawn.
					if (!dmg.IsLineDamage && !drawPointDamage)
						continue;
					var pos = pipe.GetPositionAtDistance(st.Distance);
					if (pos.HasValue)
					{
						switch (dmg.Code)
						{
							case "BCA":
								InsertBlockBCA(st, pipe, dmg, pos.Value);
								break;
							case "BCC":
								InsertBlockBCC(st, pipe, dmg, pos.Value);
								break;
						}
					}
					if (!dmg.IsDrawn)
						continue;
					double endDistance = startDistance + dmg.Length;
					IEnumerable<MyUtilities.Geometry.Point3d> pts = pipe.GetPoints(startDistance, endDistance);
					Color c = ColorManager.DamageColor.GetDamageColor(dmg.DamageClass);
					Curve line = CreateFromPoints(pts);
					line.LineWeight = dmg.IsLineDamage ? PipeDamageLineWeight : PipeDamageWeight;
					line.Color = c;
					CreatedEntities.AddPipeEntity(line);
					line.Layer = layer.Station;
				}
			}
		}
		private void DrawPipeStationText(Pipe pipe, DamageClass highest, FilteredDrawingData filteredData)
		{
			var layer = LayerManager.Instance.GetStatusDataLayer(pipe.Abwasser);
			foreach (FilteredStation fst in filteredData.Stations)
			{
				DamageStation st = fst.Station;
				double startDistance = st.Distance;
				PositionVector position = pipe.GetPositionAtDistance(startDistance).Value;
				
				var positionAdjusted = position;
				/*if (!IsDrawn3d)
				{
					position.Direction = position.Direction.GetAs2d().GetAs3d();
					position.Position = position.Position.GetOnHeight0();
				}*/
				var d  = DamageTextManager.DrawText(ColorManager.DamageColor, TextSize.DamageText, IsDrawn3d, st, fst.Damages, position);
				foreach (var p in d)
				{
					CreatedEntities.AddPipeEntity(p);
					p.Layer = layer.Station;
				}
				//DamageTextManager.AddText(st, positionAdjusted);
			}

		}

		private void DrawHighestPipeDamagePipe(Pipe pipe, DamageClass highest)
		{
			if (DrawDamagePipe && pipe.IsPipeComplete)
			{
				IEnumerable<MyUtilities.Geometry.Point3d> pts = pipe.GetPoints(0, pipe.Length);
				Color c = Inventories.Colors.ColorAssociations.GetDamageClassColor(highest);
				Curve line = CreateFromPoints(pts);
				line.LineWeight = PipeHighestDamageWeight;
				line.Color = c;
				CreatedEntities.AddPipeEntity(line);
				var layers = LayerManager.Instance.GetStatusDataLayer(pipe.Abwasser);
				line.Layer = layers.HighestObjectClass;
			}
		}

		/// <summary>
		/// Creates line/polyline3d for pipe depending on points.
		/// Curve is returned because it is base for both Line and Polyline3d.
		/// </summary>
		protected Curve CreateFromPoints(IEnumerable<MyUtilities.Geometry.Point3d> points)
		{
			Curve t = null;
			int pointCount = points.Count();
			if (pointCount == 2)
			{
				Line l = new Line();
				Tr.Btr.AppendEntity(l);
				Tr.Tr.AddNewlyCreatedDBObject(l, true);
				l.StartPoint = points.First().ToCADPoint();
				l.EndPoint = points.Last().ToCADPoint();
				/*if (!IsDrawn3d)
				{
					l.StartPoint = new Point3d(l.StartPoint.X, l.StartPoint.Y, 0);
					l.EndPoint = new Point3d(l.EndPoint.X, l.EndPoint.Y, 0);
				}*/
				t = l;
			}
			else if (pointCount > 2)
			{
				Polyline3d pl = Tr.Tr.CreateEntity<Polyline3d>(Tr.Btr);// new Polyline3d();
				foreach (MyUtilities.Geometry.Point3d pt in points)
				{
					var p = pt;// IsDrawn3d ? pt : pt.GetOnHeight0();
						pl.AppendPoint(p.ToCADPoint());
				}
				t = pl;
			}
			return t;
		}
	}
}
