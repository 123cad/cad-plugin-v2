﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.Pipes;
using System.Diagnostics;
using CadPlugin.Inventories.Damages;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.DataDrawers.Drawers.Linetypes;
using CadPlugin.Inventories.DataDrawers.Drawers;
using CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers;
using CadPlugin.Inventories.DataDrawers.Drawers.Layers;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace   CadPlugin.Inventories.Drawers.PipeDrawers
{
	/// <summary>
	/// Draws pipe as 2 polyline with some width, distant by diameter.
	/// </summary>
	class PipeDoubleLineFillDrawer : PipeDrawerNonVisual
	{
		public PipeDoubleLineFillDrawer(TransactionData data, SewageObjectColorManager pipeColor, PipeLinetype pipeLinetype, DamageTextDrawer dmgText, TextSizeDefinitions textDef, DamageDrawerFilter damageFilter, InventoryManager.CreatedCADEntities createdEntities, bool drawIn3d)
							: base(data, pipeColor, pipeLinetype, dmgText, textDef, damageFilter, createdEntities, drawIn3d)
		{
		}

		public override void DrawDamages(Pipe pipe)
		{
			DamageFilter.Clear();
			foreach (DamageStation st in pipe.GetDamagesCollection())
				DamageFilter.AddDamages(st, st/*.Where(x => x.Code != "BCA" && x.Code != "BCC")*/);
			FilteredDrawingData filteredData = DamageFilter.GetFilteredData();
			
			DrawPipeDamages(pipe, filteredData);
		}
		protected override IEnumerable<Entity> CreatePipeLineWithSettings(Pipe pipe, string linetype, Color c, string layerName = null)
		{
			MyUtilities.Geometry.Point3d[] pts = pipe.GetAllPoints().ToArray();

			IList<Point2d> newPtsL = CADCommonHelper.CreateOffsetPoints(pts.Select(x => new Point2d(x.X, x.Y)).ToList(), -pipe.Diameter / 2);
			IList<Point2d> newPtsR = CADCommonHelper.CreateOffsetPoints(pts.Select(x => new Point2d(x.X, x.Y)).ToList(), pipe.Diameter / 2);


			Polyline middle = Tr.Tr.CreateEntity<Polyline>(Tr.Btr);
			for (int i = 0; i < pts.Length; i++)
			{
				middle.AddVertexAt(i, pts[i].GetAs2d().ToCADPoint(), 0, pipe.Diameter, pipe.Diameter);
			}
			middle.Color = c;
			middle.Linetype = linetype;

			double width = 0;
			// Left and Right are not good, since method interpetates negative as making curve radius smaller - we can't know that 
			// (and it is not important).
			Polyline left = Tr.Tr.CreateEntity<Polyline>(Tr.Btr);// new Polyline3d();
			Polyline right = Tr.Tr.CreateEntity<Polyline>(Tr.Btr);// new Polyline3d();
			for (int i = 0; i < pts.Length; i++)
			{
				left.AddVertexAt(i, newPtsL[i], 0, width, width);
				right.AddVertexAt(i, newPtsR[i], 0, width, width);
			}


			//left.Color = c;
			//right.Color = c;
			left.Linetype = linetype;
			right.Linetype = linetype;
			
			left.Layer = layerName;
			right.Layer = layerName;
			middle.Layer = layerName;

			return new List<Entity> { left, right, middle };
		}

		public override void DrawPipe(Pipe pipe)
		{
			if (!pipe.IsPipeComplete)
				return;
			Color c = ColorManager.ObjectColor.GetObjectColor();
			string lineStyle = PipeLinetypeManager.GetLinetype();// GetEntwaesserungsartLinetype(pipe.Abwasser);
			string layerName = LayerManager.Instance.GetMainDataLayer(pipe.Abwasser);
			IEnumerable<Entity> created = CreatePipeLineWithSettings(pipe, lineStyle, c, layerName);


			bool first = true;
			foreach(var e in created)
			{
				// Only first affects the view - optimization.
				CreatedEntities.AddPipeEntity(e, first);
				first = false;
			}

			singleLineDrawer.DrawPipeWithLabelOffset(pipe);
		}
	}
}
