﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.Pipes;
using CadPlugin.Inventories.Damages;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.DataDrawers.Drawers.Linetypes;
using CadPlugin.Inventories.DataDrawers.Drawers;
using CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers;
using CadPlugin.Inventories.DataDrawers.Drawers.Layers;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace   CadPlugin.Inventories.Drawers.PipeDrawers
{
	/// <summary>
	/// Draws a pipe as a single polyline (2d) with width of a diameter.
	/// </summary>
	class PipeSingleLineWidthDrawer : PipeDrawerNonVisual
	{
		public PipeSingleLineWidthDrawer(TransactionData data, SewageObjectColorManager pipeColor, PipeLinetype pipeLinetype, DamageTextDrawer dmgText, TextSizeDefinitions textDef, DamageDrawerFilter damageFilter, InventoryManager.CreatedCADEntities createdEntities, bool drawIn3d)
							: base(data, pipeColor, pipeLinetype, dmgText, textDef, damageFilter, createdEntities, drawIn3d)
		{
		}

		public override void DrawDamages(Pipe pipe)
		{
			DamageFilter.Clear();
			foreach (DamageStation st in pipe.GetDamagesCollection())
				DamageFilter.AddDamages(st, st/*.Where(x => x.Code != "BCA" && x.Code != "BCC")*/);
			FilteredDrawingData filteredData = DamageFilter.GetFilteredData();

			DrawPipeDamages(pipe, filteredData);
		}

		protected override IEnumerable<Entity> CreatePipeLineWithSettings(Pipe pipe, string linetype, Color c, string layerName = null)
		{
			IEnumerable<MyUtilities.Geometry.Point3d> pts = pipe.GetAllPoints();
			double width = pipe.Diameter;
			Polyline pl = Tr.Tr.CreateEntity<Polyline>(Tr.Btr);// new Polyline3d();
			int index = 0;
			foreach (MyUtilities.Geometry.Point3d pt in pts)
			{
				pl.AddVertexAt(index++, pt.GetAs2d().ToCADPoint(), 0, width, width);
			}

			pl.Linetype = linetype;
			pl.Color = c;
			pl.Layer = layerName;
			yield return pl;
		}

		public override void DrawPipe(Pipe pipe)
		{
			if (!pipe.IsPipeComplete)
				return;

			string lineStyle = PipeLinetypeManager.GetLinetype();// GetEntwaesserungsartLinetype(pipe.Abwasser);
			Color c = ColorManager.ObjectColor.GetObjectColor();
			string layerName = LayerManager.Instance.GetMainDataLayer(pipe.Abwasser);
			IEnumerable<Entity> create = CreatePipeLineWithSettings(pipe, lineStyle, c, layerName);

			foreach(var pl in create)
				CreatedEntities.AddPipeEntity(pl, true);

			singleLineDrawer.DrawPipe(pipe);
			

		}

	}
}
