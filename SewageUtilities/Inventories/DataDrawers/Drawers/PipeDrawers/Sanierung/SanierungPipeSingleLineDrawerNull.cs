﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.Pipes;
using CadPlugin.Inventories.Damages;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.DataDrawers.Drawers.Linetypes;
using CadPlugin.Inventories.DataDrawers.Drawers;
using CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers;
using CadPlugin.Inventories.DataDrawers.Drawers.Layers;
using Inventory.CAD;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Sanierungs;
using CadPlugin.Inventories.DataDrawers.Drawers.Sanierungs;
using CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers.Sanierung;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace   CadPlugin.Inventories.Drawers.PipeDrawers
{
	/// <summary>
	/// Draws pipe as a single line.
	/// </summary>
	class SanierungPipeSingleLineDrawerNull:PipeSingleLineDrawerNull, ISanierungDataSource 
	{
		private bool drawConcept;
		private bool drawProcedures;
		private Dictionary<Pipe, ObjectMassnahmes> objectMassnahmesCollection = new Dictionary<Pipe, ObjectMassnahmes>();
		 
		public SanierungPipeSingleLineDrawerNull(TransactionData data, SewageObjectColorManager pipeColor, 
											PipeLinetype pipeLinetype, DamageTextDrawer dmgText, 
											TextSizeDefinitions textDef, DamageDrawerFilter damageFilter, 
											InventoryManager.CreatedCADEntities createdEntities
											)
							: base(data, pipeColor, pipeLinetype, dmgText, textDef, damageFilter, createdEntities)
		{
			//this.drawConcept = drawConcept;
			//this.drawProcedures = drawProcedures;
		}
		
		public void SetObjectMassnahmes(IEnumerable<Tuple<SewageObject, ObjectMassnahmes>> objs)
		{
			objectMassnahmesCollection = objs.Where(x=>x.Item1 is Pipe).ToDictionary(x => x.Item1 as Pipe, x => x.Item2);
		}
		
		public override void DrawDamages(Pipe pipe)
		{
			
		}

		public override void DrawPipe(Pipe pipe)
		{
			if (!drawConcept)
				return;
			base.DrawPipe(pipe);
			// Draw rectangle with ArtMassnahme text code.
			foreach (var v in DataDrawers.Drawers.Sanierungs.DrawSymbol.DrawPipeSymbol(Tr, pipe, false, IsDrawn3d))
			{
				v.Layer = LayerManager.SanierungConceptLayerPipe;
				CreatedEntities.AddPipeEntity(v);
			}
		}

		public override void DrawPipeWithLabelOffset(Pipe pipe)
		{
			base.DrawPipeWithLabelOffset(pipe);
			foreach (var v in DataDrawers.Drawers.Sanierungs.DrawSymbol.DrawPipeSymbol(Tr, pipe, false, IsDrawn3d))
			{
				v.Layer = LayerManager.SanierungConceptLayerPipe;
				CreatedEntities.AddPipeEntity(v);
			}
		}
	}
}
