﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.Pipes;
using CadPlugin.Inventories.Damages;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.DataDrawers.Drawers.Linetypes;
using CadPlugin.Inventories.DataDrawers.Drawers;
using CadPlugin.Inventories.DataDrawers.Drawers.Layers;
using Isybau2015.Isybau2015.ReferenceTableEnums;
using Inventory.CAD;
using CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers.Sanierung;
using CadPlugin.Inventories.DataDrawers.Drawers.Sanierungs;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Sanierungs;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace   CadPlugin.Inventories.Drawers.PipeDrawers
{
	class SanierungPipeVisual3dDrawer:PipeVisual3dDrawer, ISanierungDataSource
	{

		private bool drawConcept;
		private bool drawProcedures;
		private Dictionary<Pipe, ObjectMassnahmes> objectMassnahmesCollection = new Dictionary<Pipe, ObjectMassnahmes>();
		public SanierungPipeVisual3dDrawer(TransactionData data, SewageObjectColorManager pipeColor, PipeLinetype pipeLinetype, 
									DamageTextDrawer dmgText, TextSizeDefinitions textDef, DamageDrawerFilter damageFilter, 
									InventoryManager.CreatedCADEntities createdEntities,
											bool drawConcept, bool drawProcedures
											)
							: base(data, pipeColor, pipeLinetype, dmgText, textDef, damageFilter, createdEntities)
		{
			this.drawConcept = drawConcept;
			this.drawProcedures = drawProcedures;
		}
		public void SetObjectMassnahmes(IEnumerable<Tuple<SewageObject, ObjectMassnahmes>> objs)
		{
			objectMassnahmesCollection = objs.Where(x=>x.Item1 is Pipe).ToDictionary(x => x.Item1 as Pipe, x => x.Item2);
		}
		public override void DrawDamages(Pipe pipe)
		{
			if (!drawProcedures)
				return;
			if (objectMassnahmesCollection == null
				|| !objectMassnahmesCollection.ContainsKey(pipe))
				return;
			ObjectMassnahmes o = objectMassnahmesCollection[pipe];
			foreach (Massnahme m in o.Massnahmes)
			{
				IEnumerable<Entity> entities = ProcedureDrawer.DrawPipeSingleProcedure(Tr, o.Object, pipe, m);
				foreach (var e in entities)
				{
					e.Layer = LayerManager.SanierungProcedureLayerPipe;
					CreatedEntities.AddPipeEntity(e);
				}
			}

			
		}

		public override void DrawPipe(Pipe pipe)
		{
			if (!drawConcept)
				return;
			base.DrawPipe(pipe);

			// Draw rectangle with ArtMassnahme text code.
			foreach (var v in DataDrawers.Drawers.Sanierungs.DrawSymbol.DrawPipeSymbol(Tr, pipe, true, true))
			{
				v.Layer = LayerManager.SanierungConceptLayerPipe;
				CreatedEntities.AddPipeEntity(v);
			}
		}
	}
}
