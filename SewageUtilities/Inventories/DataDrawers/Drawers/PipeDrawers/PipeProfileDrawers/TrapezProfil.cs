﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers.PipeProfileDrawers
{
	public class TrapezProfil
	{
		/*
		 * Adding points starting from the top left and going 
		 * in counter-clockwise direction.
		 *  _          _
		 *	\\        //
		 *	 \\		 //
		 *	  \\____//	 
		 *     \____/
		 * 
		 * Draw as horizontal plate, and then add depth to the middle, and adjust ends.
		 */

		/// <summary>
		/// Thickness of pipe wall relative to diameter.
		/// </summary>
		private static double WallThicknessPercent = 0.2;
		public static ComplexSolidProfile CreateProfile(double diameter)
		{
			var baseEntity = CreateBaseOuter(diameter);
			//hollowEntity = CreateBaseInner(diameter);
			ComplexSolidProfile complex = new ComplexSolidProfile(baseEntity);
			return complex;
		}

		/// <summary>
		/// Returns outer diameter for provided inner pipe diameter.
		/// </summary>
		/// <param name="diameter"></param>
		/// <returns></returns>
		public static double GetOuterDiameter(double diameter)
		{
			return diameter * (1 + WallThicknessPercent);
		}
		public static Entity CreateBaseOuter(double diameter)
		{
			double radius = diameter / 2;
			Polyline pl = new Polyline();
			//pl.Elevation = startPosition.Z;
			pl.Closed = true;
			double depth = radius;
			double thickness = radius * WallThicknessPercent;
			List<Point2d> pts = new List<Point2d>();
			//Point2d middle = new Point2d(0, -0.4);
			Point2d start = new Point2d(-radius - thickness, 0);
			//centerOffset = new Point2d().GetVectorTo(start);
			//Vector2d offset = start.GetVectorTo(startPosition.GetAs2d());
			var last = start;
			pts.Add(start); // 0

			// TEST
			pts.Clear();
			last = new Point2d(-radius, 0); pts.Add(last);
			last = new Point2d(-radius / 2, -radius);pts.Add(last);
			last = new Point2d(radius / 2, -radius);pts.Add(last);
			last = new Point2d(radius, 0); pts.Add(last);

			var offsetPoints = CADCommonHelper.CreateOffsetPoints(pts, thickness);

			// Pipe center is positioned at the inner bottom of the pipe.
			Point2d pipeCenter = new Point2d(0, +depth);
			
			pl.AddPoints(offsetPoints[0], offsetPoints[1], offsetPoints[2], offsetPoints[3],
						pts[3], pts[2], pts[1], pts[0]);
			
			Matrix3d move = Matrix3d.Displacement(pipeCenter.GetAs3d().GetAsVector());
			pl.TransformBy(move);
			//ENDTEST











			/*last = new Point2d(-radius / 2, - radius - thickness); pts.Add(last); // 1
			last = new Point2d(radius / 2, -radius - thickness); pts.Add(last); // 2
			last = new Point2d(radius + thickness, 0); pts.Add(last); // 3
			last = new Point2d(radius, 0); pts.Add(last); // 4
			last = new Point2d(radius / 2, -radius); pts.Add(last); // 5
			last = new Point2d(-radius / 2, -radius); pts.Add(last); // 6
			last = new Point2d(-radius, 0); pts.Add(last);//7



			var v1 = pts[7].GetVectorTo(pts[6]);
			v1 = v1.RotateBy(-Math.PI / 2);









			pts[2] = pts[2].Add(new Vector2d(0, -depth));
			pts[5] = pts[5].Add(new Vector2d(0, -depth));

			double angle = pts[0].GetVectorTo(pts[5]).GetAngleTo(Vector2d.XAxis);
			var vector = pts[0].GetVectorTo(pts[1]);
			vector = vector.RotateBy(-angle);
			pts[1] = pts[0].Add(vector);
			vector = pts[4].GetVectorTo(pts[3]);
			vector = vector.RotateBy(angle);
			pts[3] = pts[4].Add(vector);

			for (int i = 0; i < pts.Count; i++)
			{
				Point2d pt = pts[i];
				//pt = pt.Add(offset);
				pl.AddVertexAt(i, pt, 0, 0, 0);
			}*/

			// For some reason, making solid3d rotates the object. With this, that is neutralized.
			//Matrix3d rotation = Matrix3d.Rotation(Math.PI, Vector3d.ZAxis, new Point3d(0, 0, 0));
			//pl.TransformBy(rotation);

			return pl;
		}
	}
}
