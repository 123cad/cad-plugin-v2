﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers.PipeProfileDrawers
{

	public class RectangleProfilArc
	{

		/*
		 * Adding points starting from the top left and going 
		 * in counter-clockwise direction.
		 *   _____________
		 *	|             |
		 *	|			  |
		 *	|			  |
		 *   \           /
		 *     \       /
		 *       \___/
		 */

		/// <summary>
		/// Thickness of pipe wall relative to diameter.
		/// </summary>
		private static double WallThicknessPercent = 0.2;
		public static ComplexSolidProfile CreateProfile(double diameter)
		{
			var baseEntity = CreateBaseOuter(diameter);
			ComplexSolidProfile complex = new ComplexSolidProfile(baseEntity);
			//hollowEntity = CreateBaseInner(diameter);
			return complex;
		}

		/// <summary>
		/// Returns outer diameter for provided inner pipe diameter.
		/// </summary>
		/// <param name="diameter"></param>
		/// <returns></returns>
		public static double GetOuterDiameter(double diameter)
		{
			return diameter;
		}
		public static Entity CreateBaseOuter(double diameter)
		{
			//var startPosition = pipe.StartPoint.ToCADPoint();
			double radius = diameter / 2;
			Polyline pl = new Polyline();
			//pl.Elevation = startPosition.Z;
			pl.Closed = true;
			double depth = radius;
			double thickness = radius;// * WallThicknessPercent ;
			List<Point2d> pts = new List<Point2d>();
			Point2d start = new Point2d(-radius, 0);
			var centerOffset = new Point2d().GetVectorTo(start);
			//Vector2d offset = start.GetVectorTo(startPosition.GetAs2d());
			var last = start;
			pts.Add(start);
			last = last.Add(new Vector2d(0, -depth)); pts.Add(last);
			last = last.Add(new Vector2d(2 * radius, 0)); pts.Add(last);
			last = last.Add(new Vector2d(0, depth)); pts.Add(last);

			// Pipe center is positioned at the inner bottom of the pipe.
			Point2d pipeCenter = new Point2d(0, depth);
			Vector2d pipeCenterOffset = pipeCenter.GetAsVector();

			double bulgeOffset = 0;// If we have a bulge, we need to move pipe to the bottom of the arc.
			for (int i = 0; i < pts.Count; i++)
			{
				Point2d pt = pts[i];
				pt = pt.Add(pipeCenterOffset);
				double bulge = 0;
				if (i == 1)
				{
					bulge = 0.5;
					double length = pts[i].GetVectorTo(pts[i + 1]).Length;
					// Bulge is % of radius between 2 points.
					bulgeOffset = bulge * (length / 2);
				}
				pl.AddVertexAt(i, pt, bulge, 0, 0);
			}

			// Move the entity to the bottom of the pipe.
			Matrix3d move = Matrix3d.Displacement(new Vector3d(0, bulgeOffset, 0));
			pl.TransformBy(move);


			// For some reason, making solid3d rotates the object. With this, that is neutralized.
			//Matrix3d rotation = Matrix3d.Rotation(Math.PI, Vector3d.ZAxis, new Point3d(0, 0, 0));
			//pl.TransformBy(rotation);

			return pl;
		}
	}
}
