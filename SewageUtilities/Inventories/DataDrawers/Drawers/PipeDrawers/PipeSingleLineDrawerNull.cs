﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.Pipes;
using CadPlugin.Inventories.Damages;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.DataDrawers.Drawers.Linetypes;
using CadPlugin.Inventories.DataDrawers.Drawers;
using CadPlugin.Inventories.DataDrawers.Drawers.Layers;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace   CadPlugin.Inventories.Drawers.PipeDrawers
{
	class PipeSingleLineDrawerNull : PipeSingleLineDrawer
	{
		public PipeSingleLineDrawerNull(TransactionData data, SewageObjectColorManager pipeColor, PipeLinetype pipeLinetype, DamageTextDrawer dmgText, TextSizeDefinitions textDef, DamageDrawerFilter damageFilter, InventoryManager.CreatedCADEntities createdEntities)
							: base(data, pipeColor, pipeLinetype, dmgText, textDef, damageFilter,  createdEntities, false)
		{
		}

		public override void DrawDamages(Pipe pipe)
		{
		}


		public override void DrawPipe(Pipe pipe)
		{
			string layerName = LayerManager.Instance.GetMainDataLayer(pipe.Abwasser);

			foreach (var v in PipeLabelDrawer.DrawLabels(Tr, pipe, TextSize.PipeTextSize))
			{
				v.Layer = layerName;
				CreatedEntities.AddPipeEntity(v);
			}
			if (IsArrowDrawn)
				DrawArrow(pipe);
		}
		public override void DrawPipeWithLabelOffset(Pipe pipe)
		{
			string layerName = LayerManager.Instance.GetMainDataLayer(pipe.Abwasser, true);

			if (DrawPipeLabel)
				foreach (var v in PipeLabelDrawer.DrawLabels(Tr, pipe, TextSize.PipeTextSize, true, pipe.Diameter))
				{
					v.Layer = layerName;
					CreatedEntities.AddPipeEntity(v);
				}
			if (IsArrowDrawn)
				DrawArrow(pipe);
		}
	}
}
