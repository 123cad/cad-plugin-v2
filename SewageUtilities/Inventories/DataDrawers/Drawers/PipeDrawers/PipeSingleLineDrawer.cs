﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.Pipes;
using CadPlugin.Inventories.Damages;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.DataDrawers.Drawers.Linetypes;
using CadPlugin.Inventories.DataDrawers.Drawers;
using CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers;
using CadPlugin.Inventories.DataDrawers.Drawers.Layers;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace   CadPlugin.Inventories.Drawers.PipeDrawers
{
	/// <summary>
	/// Draws pipe as a single line.
	/// </summary>
	class PipeSingleLineDrawer : PipeDrawerNonVisual
	{
		public PipeSingleLineDrawer(TransactionData data, SewageObjectColorManager pipeColor, PipeLinetype pipeLinetype, DamageTextDrawer dmgText, TextSizeDefinitions textDef, DamageDrawerFilter damageFilter, InventoryManager.CreatedCADEntities createdEntities, bool drawIn3d)
							: base(data, pipeColor, pipeLinetype, dmgText, textDef, damageFilter, createdEntities, drawIn3d)
		{
		}

		public override void DrawDamages(Pipe pipe)
		{

			DamageFilter.Clear();
			foreach (DamageStation st in pipe.GetDamagesCollection())
				DamageFilter.AddDamages(st, st/*.Where(x => x.Code != "BCA" && x.Code != "BCC")*/);
			FilteredDrawingData filteredData = DamageFilter.GetFilteredData();


			DrawPipeDamages(pipe, filteredData);

		}
		protected override IEnumerable<Entity> CreatePipeLineWithSettings(Pipe pipe, string linetype, Color c, string layerName)
		{
			IEnumerable<MyUtilities.Geometry.Point3d> pts = pipe.GetAllPoints();
			/*if (!IsDrawn3d)
				pts = pts.Select(x => x.GetOnHeight0()).ToList();*/
			Entity t = CreateFromPoints(pts);
			if (t == null)
				yield break;

			t.Linetype = linetype;
			t.Color = c;
			t.Layer = layerName;
			yield return t;
		}


		private void DrawPipe(Pipe pipe, bool labelOffset)
		{
			if (!pipe.IsPipeComplete)
				return;
			string lineStyle = PipeLinetypeManager.GetLinetype();// GetEntwaesserungsartLinetype(pipe.Abwasser);
			string layerName = LayerManager.Instance.GetMainDataLayer(pipe.Abwasser);
			Color c = ColorManager.ObjectColor.GetObjectColor();
			IEnumerable<Entity> create = CreatePipeLineWithSettings(pipe, lineStyle, c, layerName);

			foreach(var e in create)
				CreatedEntities.AddPipeEntity(e, true);

			IEnumerable<Entity> labelEntities = null;
			if (DrawPipeLabel)
			{
				if (labelOffset)
					labelEntities = PipeLabelDrawer.DrawLabels(Tr, pipe, TextSize.PipeTextSize, true, pipe.Diameter);
				else
					labelEntities = PipeLabelDrawer.DrawLabels(Tr, pipe, TextSize.PipeTextSize);
				string layerNameLabel = LayerManager.Instance.GetMainDataLayer(pipe.Abwasser, true);

				foreach (var v in labelEntities)
				{
					v.Layer = layerNameLabel;
					CreatedEntities.AddPipeEntity(v);
				}
			}

			if (IsArrowDrawn)
				DrawArrow(pipe);
		}
		public override void DrawPipe(Pipe pipe)
		{
			DrawPipe(pipe, false);
		}
		/// <summary>
		/// Draws pipes labels, with optional moving it to radius distance from the pipe.
		/// </summary>
		/// <param name="pipe"></param>
		/// <param name="labelOffset">Distance of label depends on pipe diameter.</param>
		public virtual void DrawPipeWithLabelOffset(Pipe pipe)
		{
			DrawPipe(pipe, true);
		}

	}
}
