﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.Pipes;
using CadPlugin.Inventories.Damages;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.DataDrawers.Drawers.Linetypes;
using CadPlugin.Inventories.DataDrawers.Drawers;
using CadPlugin.Inventories.DataDrawers.Drawers.Layers;
using Isybau2015.Isybau2015.ReferenceTableEnums;
using CadPlugin.Sewage.Pipe3dProfiles;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.Drawers.PipeDrawers
{
    class PipeVisual3dDrawer : PipeDrawer
	{
		public override bool DrawPipeLabel => true;

		public override bool IsDrawnVisual3d => true;

		public PipeVisual3dDrawer(TransactionData data, SewageObjectColorManager pipeColor, PipeLinetype pipeLinetype, DamageTextDrawer dmgText, TextSizeDefinitions textDef, DamageDrawerFilter damageFilter, InventoryManager.CreatedCADEntities createdEntities)
							: base(data, pipeColor, pipeLinetype, dmgText, textDef, damageFilter, createdEntities, true)
		{
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="pipe"></param>
		/// <param name="diameter">Diameter of drawn entity doesn't necessary have to be pipe diameter.</param>
		/// <param name="points"></param>
		/// <param name="totalPipeWidth"></param>
		/// <returns></returns>
		private List<Solid3d> DrawComplexSolids(Pipe pipe, double diameter, IEnumerable<MyUtilities.Geometry.Point3d> points, ref double totalPipeWidth)
		{
			S104_Profiltyp pt = S104_Profiltyp.andereProfilart;
			var kante = pipe.Abwasser.Auswahlelement as Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Kante;
            try
            {
				int t = kante?.Profil?.Profilart ?? 0;
				pt = (S104_Profiltyp)t;
            }
			catch
            {
            }
			IPipeProfileCreator profile = Sewage.Pipe3dProfiles.Pipe3dProfileManager.GetProfile(pt);
			//We have classes for drawing different profiles now.
			//IPipeBase_S104 baseDrawer;
			//switch (kante?.Profil?.Profilart)
			//{
			//	default: baseDrawer = PipeBase_S104_00.Instance; break;
			//	case 1: baseDrawer = PipeBase_S104_01.Instance; break;
			//	case 2: baseDrawer = PipeBase_S104_02.Instance; break;
			//	case 3: baseDrawer = PipeBase_S104_03.Instance; break;
			//	case 4: baseDrawer = PipeBase_S104_04.Instance; break;
			//	case 5: baseDrawer = PipeBase_S104_05.Instance; break;
			//	case 6: baseDrawer = PipeBase_S104_06.Instance; break;
			//	case 7: baseDrawer = PipeBase_S104_07.Instance; break;
			//	case 8: baseDrawer = PipeBase_S104_08.Instance; break;
			//	case 9: baseDrawer = PipeBase_S104_09.Instance; break;
			//	case 10: baseDrawer = PipeBase_S104_10.Instance; break;
			//}
			List<Solid3d> solids = new List<Solid3d>();
			using (ComplexSolidEntitiesCollection complexCollection = new ComplexSolidEntitiesCollection())
			{
				var param = new PipeParameters(diameter);
				if (profile.CreateEntityBase(param, complexCollection))
				{
					totalPipeWidth = profile.GetBaseTotalWidth(param);
					foreach (var complex in complexCollection)
					{
						var solid = CADSolid3dHelper.CreateSolidAlongPath(Tr.GetAsWrapper(), points, complex);
						solids.Add(solid);
					}
				}
			}

			return solids;
		}

		public override void DrawDamages(Pipe pipe)
		{
			DamageClass highestClass = DamageClass.ClassNotSet;
			DamageFilter.Clear();
			foreach (DamageStation st in pipe.GetDamagesCollection())
				DamageFilter.AddDamages(st, st/*.Where(x => x.Code != "BCA" && x.Code != "BCC")*/);
			FilteredDrawingData filteredData = DamageFilter.GetFilteredData();
			var layer = LayerManager.Instance.GetStatusDataLayer(pipe.Abwasser);
			foreach (FilteredStation fst in filteredData.Stations)
			{
				DamageStation st = fst.Station;
				double startDistance = st.Distance;
				PositionVector position = pipe.GetPositionAtDistance(startDistance).Value;
				foreach (SingleDamage dmg in st)
				{
					if (highestClass < dmg.DamageClass)
						highestClass = dmg.DamageClass;
					var pos = pipe.GetPositionAtDistance(st.Distance);
					if (pos.HasValue)
					{
						switch (dmg.Code)
						{
							case "BCA":
								InsertBlockBCA(st, pipe, dmg, pos.Value);
								break;
							case "BCC":
								InsertBlockBCC(st, pipe, dmg, pos.Value);
								break;
						}
					}
					if (!dmg.IsDrawn)
						continue;
					double endDistance = startDistance + dmg.Length;
					List<MyUtilities.Geometry.Point3d> pts = pipe.GetPoints(startDistance, endDistance).ToList();
					Color c = ColorManager.DamageColor.GetDamageColor(dmg.DamageClass);
					// Higher damage has bigger damage ring, so rings are not overlapping.
					// Damage class can be [-1,5] so we add 1.005 to be >0 (so ring is always bigger than a pipe.
					double damageRingDiameter = DataDrawers.Drawers.DamageRingSize.GetRingDiameterForDamage(pipe.Diameter, dmg.DamageClass);
					//Solid3d solid = Common.CADSolid3dHelper.CreateCylinderAlongPath(Tr, pts, damageRingDiameter);
					double totalWidth = pipe.Diameter;
					var solids = DrawComplexSolids(pipe, damageRingDiameter, pts, ref totalWidth);

					solids.ForEach(x =>
					{
						x.Color = c;
						x.Layer = layer.Station;
						CreatedEntities.AddPipeEntity(x, true);
					});
					//solid.Color = c;
					//solid.Layer = layer.Station;
					//CreatedEntities.AddPipeEntity(solid, true);
				}
				foreach (var v in DamageTextManager.DrawText(ColorManager.DamageColor, TextSize.DamageText, IsDrawn3d, st, fst.Damages, position))
				{
					v.Layer = layer.Station;
					CreatedEntities.AddPipeEntity(v);
				}
				//DamageTextManager.AddText(st, position);
			}
			//DamageTextManager.DrawAllText(ColorManager.DamageColor, true);
			//DamageTextManager.Clear();

			// Draw damage pipe.
			if (DrawDamagePipe && pipe.IsPipeComplete)
			{
				IEnumerable<MyUtilities.Geometry.Point3d> pts = pipe.GetPoints(0, pipe.Length);
				//double diameter = DamageRingSize.GetRingDiamaterForCompletePipe(pipe.Diameter, highestClass);
				//Solid3d solid = Common.CADSolid3dHelper.CreateCylinderAlongPath(Tr, pts, diameter);
				//solid.Color = Inventories.Colors.ColorAssociations.GetDamageClassColor(highestClass);
				//solid.Layer = layer.HighestObjectClass;
				//
				//// Should it be drawn if class not set?
				//if (highestClass == DamageClass.ClassNotSet)
				//	solid.Color = Color.FromColor(System.Drawing.Color.Yellow);
				//
				//CreatedEntities.AddPipeEntity(solid);


				double totalWidth = pipe.Diameter;
				double diameter = DamageRingSize.GetRingDiamaterForCompletePipe(pipe.Diameter, highestClass);
				var solids = DrawComplexSolids(pipe, diameter, pts, ref totalWidth);
				solids.ForEach(solid =>
				{
					solid.Color = Inventories.Colors.ColorAssociations.GetDamageClassColor(highestClass);
					solid.Layer = layer.HighestObjectClass;

					// Should it be drawn if class not set?
					if (highestClass == DamageClass.ClassNotSet)
						solid.Color = Color.FromColor(System.Drawing.Color.Yellow);

					CreatedEntities.AddPipeEntity(solid, true); 
				});
			}

		}
		
		public override void DrawPipe(Pipe pipe)
		{
			if (!pipe.IsPipeComplete)
				return;
			List<MyUtilities.Geometry.Point3d> pts = pipe.GetAllPoints().ToList();

			double totalWidth = pipe.Diameter;
			List<Solid3d> solids = DrawComplexSolids(pipe, pipe.Diameter, pts, ref totalWidth);

			/*switch (kante.KantenTyp)
			{
				case (int)G200_Kantentyp.Rinne:
					Vector2d rV;
					Entity r = GetRinneBaseEntity(pipe.Diameter / 2, pts[0].ToCADPoint(), out rV);
					solid = CADSolid3dHelper.CreateEntityAlongPath(Tr, pts, r, rV);
					r.Dispose();
					//AdjustRinnePoints(points);
					break;
				case (int)G200_Kantentyp.Gerinne:
					Vector2d rG;
					Entity gr = GetGerinneBaseEntity(pipe.Diameter / 2, pts[0].ToCADPoint(), out rG);
					solid = CADSolid3dHelper.CreateEntityAlongPath(Tr, pts, gr, rG);
					gr.Dispose();
					break;
				default:
					solid = Common.CADSolid3dHelper.CreateCylinderAlongPath(Tr, pts, pipe.Diameter);
					break;
			}*/

			var color = ColorManager.ObjectColor.GetObjectColor();
			string layerName = LayerManager.Instance.GetMainDataLayer(pipe.Abwasser);
			solids.ForEach(x =>
			{
				x.Color = color;
				x.Layer = layerName;
				CreatedEntities.AddPipeEntity(x);
			});


			//CreatedEntities.AddPipeEntity(solid, true);
			//solid.Layer = layerName;
			if (DrawPipeLabel)
				foreach (var v in PipeLabelDrawer.DrawLabels(Tr, pipe, TextSize.PipeTextSize, true, totalWidth))
				{
					v.Layer = layerName;
					CreatedEntities.AddPipeEntity(v);
				}

			if (IsArrowDrawn)
				DrawArrow(pipe);
		}
	}
}
