﻿using CadPlugin.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Inventories.Pipes;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.DataDrawers.Drawers
{
	/// <summary>
	/// Draws text aligned with pipe, with pipe info.
	/// </summary>
	class PipeLabelDrawer
	{
		//NOTE When pressure pipe is needed to be included, check SewageCommands - there is a copy of this class.
		//NOTE And make single system for drawing labels for both places! (move label draw there, move complete code from there to here).

		//NOTE Slope is calculated as delta(z) / smpLength2d ! currently we don't have access to shaft, so it is not possible to do.

		/// <summary>
		/// Draws pipe labels (arrow of waterflow and others), while preserving 
		/// text to be readable by the user (not invertet more than Pi/2).
		/// 
		/// </summary>
		/// <param name="data"></param>
		/// <param name="pipe"></param>
		/// <param name="pipeTotalWidth">Diameter of the pipe is inner diameter. Pipe can have a wall, so width is used.</param>
		/// <param name="offsetByPipeDiameter">Should text be offset from the pipe by diameter.</param>
		/// <returns></returns>
		public static IEnumerable<Entity> DrawLabels(TransactionData data, Pipe pipe, double textHeight, bool offsetByPipeDiameter = false, double pipeTotalWidth = 0)
		{
			List<Entity> ents = new List<Entity>();
			//initializeLayer(data);
			//var labelLayerId = data.Lt[pipeTextLayerName];
			if (!data.Btr.IsWriteEnabled)
				data.Btr.UpgradeOpen();

			double length = pipe.Length;
			int diameterMM = (int)(pipe.Diameter * 1000);
			//double textOffset = textHeight / 2.0 + (offsetByPipeDiameter ? pipe.Diameter / 2 : 0);
			var kante = pipe.Abwasser.Auswahlelement as Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Kante;
			string material = kante?.Material;
			MyUtilities.Geometry.Vector3d pipeVector = pipe.StartPoint.GetVectorTo(pipe.EndPoint);
			MyUtilities.Geometry.Vector2d pipe2d = pipeVector.GetAs2d();

			//string lengthText = DoubleHelper.ToStringInvariant(length, 2);
			//string diameterText = diameterMM.ToString();
			double percent = MyUtilities.Helpers.Calculations.GetSlope(pipeVector) * 100;
			double promil = percent * 10;
			MyUtilities.Geometry.Point3d middlePoint = pipe.StartPoint.Add(pipeVector.Multiply(0.5));
			//Point3d lineCenter = new Point3d(centerX, centerY, centerZ);

			DBText materialText = data.Tr.CreateEntity<DBText>(data.Btr);// new DBText();
			DBText diameterText = data.Tr.CreateEntity<DBText>(data.Btr);// new DBText();
			DBText slopeText = data.Tr.CreateEntity<DBText>(data.Btr);// new DBText();
			DBText lengthText = data.Tr.CreateEntity<DBText>(data.Btr);// new DBText();
			//materialText.LayerId = labelLayerId;
			//diameterText.LayerId = labelLayerId;
			//slopeText.LayerId = labelLayerId;
			//lengthText.LayerId = labelLayerId;
			ents.Add(materialText);
			ents.Add(diameterText);
			ents.Add(slopeText);
			ents.Add(lengthText);

			materialText.Height = textHeight;
			diameterText.Height = textHeight;
			slopeText.Height = textHeight;
			lengthText.Height = textHeight;
			if (string.IsNullOrEmpty(material))
				material = " ";
			string materialString= material; // Because if completely empty GeometricExtents will throw exception.
			string diameterString = diameterMM.ToString();
			string slopeString = DoubleHelper.ToStringInvariant(Math.Abs(promil), 1) + "‰";// (Math.Abs(promil)).ToString("0.000", CultureInfo.InvariantCulture) + "‰";
			if (promil > 1e6)
				slopeString = "↓ ‰";
			else if (promil < -1e6)
				slopeString = "↑ ‰";
			double shaftSmpLength = 0;//Length from start to end shaft, SMP points. In 2d.
			Shafts.Shaft start = null, end = null;
			if (pipe.GetStartEndShaft(ref start, ref end))
			{
				shaftSmpLength = start.Center.GetAs2d().GetVectorTo(end.Center.GetAs2d()).Length;
			}
			else
			{
				// If one of shafts doesn't exist.
				// Without this length is 0.
				//shaftSmpLength = 
			}
			string lengthString = DoubleHelper.ToStringInvariant(Math.Abs(shaftSmpLength), 2) + " ";// + "m";


			// No spacing between text is used, but we add space in the text.
			double spacingBetweenText = 0;// diameterText.GeometricExtents.ToRectangle().Height / 2;
			// Use spacing between text or use spacing with space character.
			bool useSpacing = false;

			double pipeLength2d = pipe.StartPoint.GetAs2d().GetVectorTo(pipe.EndPoint.GetAs2d()).Length;
			//materialText.TextString = "P";
			double arrowWidth = textHeight * 2;//materialText.GeometricExtents.ToRectangle().Height * 2;
			double arrowHeight = textHeight / 3;
			{
				string tempDiameter = diameterString + " ";
				string tempMaterial = materialString + " - ";
				string tempSlope = slopeString + " - ";
				string tempLength = lengthString + " ";
				if (DoesFitToSingleLine(pipeLength2d, 0, materialText, tempDiameter, tempMaterial, tempSlope, tempLength))
				{
					materialString = tempMaterial;
					diameterString = tempDiameter;
					slopeString = tempSlope;
					lengthString = tempLength;
					spacingBetweenText = textHeight / 2;
				}
				else
				{
					spacingBetweenText = textHeight / 2;//materialText.GeometricExtents.ToRectangle().Height / 2;
				}


				materialText.TextString = materialString;
				diameterText.TextString = diameterString;
				slopeText.TextString = slopeString;
				lengthText.TextString = lengthString;
			}

			// Position of pipe text (relative to pipe middle point, in direction from start point to end point.):
			// *Upper left: material
			// *Upper center: arrow
			// *Upper right: diameter
			// *Lower left: slope
			// *Lower right: lenght
			// Distance between text in the same line: arrow width.
			// Text position is calculated from the bottom left of the text.
			MyUtilities.Geometry.Rectangle<double> materialTextExtents = materialText.GeometricExtents.ToRectangle();
			MyUtilities.Geometry.Rectangle<double> diameterTextExtents = diameterText.GeometricExtents.ToRectangle();
			MyUtilities.Geometry.Rectangle<double> slopeTextExtents = slopeText.GeometricExtents.ToRectangle();
			MyUtilities.Geometry.Rectangle<double> lengthTextExtents = lengthText.GeometricExtents.ToRectangle();

			SingleText materialSingleText = new SingleText(materialTextExtents);
			SingleText diameterSingleText = new SingleText(diameterTextExtents);
			SingleText slopeSingleText = new SingleText(slopeTextExtents);
			SingleText lengthSingleText = new SingleText(lengthTextExtents);
			// Make extents bigger, because no space is applied.
			var arrowExtents = new MyUtilities.Geometry.Rectangle<double>(0, 0, arrowHeight, arrowWidth * (useSpacing ? 1 : 1.3));
			var arrowSingleText = new SingleText(arrowExtents) { IsArrow = true };

			calculateTextPositions(middlePoint.ToCADPoint(), pipeTotalWidth, pipeLength2d, offsetByPipeDiameter, arrowWidth, arrowHeight,
									spacingBetweenText,
									diameterSingleText, materialSingleText, lengthSingleText, slopeSingleText, arrowSingleText);


			double diameterOffset = pipeTotalWidth / 2.0;
			materialText.Position = materialSingleText.Position;// new Point3d(centerX - materialTextExtents.Width - 1.5 * arrowWidth, centerY + diameterOffset, centerZ);
			diameterText.Position = diameterSingleText.Position;//new Point3d(centerX + 1.5 * arrowWidth, centerY + diameterOffset, centerZ);
			slopeText.Position = slopeSingleText.Position;// new Point3d(centerX - slopeTextExtents.Width - 0.5 * arrowWidth, centerY - slopeTextExtents.Height - diameterOffset, centerZ);
			lengthText.Position = lengthSingleText.Position;// new Point3d(centerX + 0.5 * arrowWidth, centerY - lengthTextExtents.Height - diameterOffset, centerZ);

			// By default arrow is pointing from start point to end point.
			// But text angle must always be -Pi/2:Pi/2, so if text angle is Pi/2:3/2Pi then 
			// text is inverted and arrow must point to the opposite side.
			double angleRad = MyUtilities.Helpers.Calculations.GetAngleUserTextAdjustedRad(pipeVector.GetAs2d());
			bool invertedArrow = Math.Cos(angleRad) < 0;
			Polyline arrow = data.Tr.CreateEntity<Polyline>(data.Btr);
			int[] vertexIndexes = new int[] { 0, 1, 2 };
			// If arrow should be rotated by 180, then just reorder vertices insertion order.
			if (invertedArrow)
				vertexIndexes = new int[] { 0, 0, 0 };
			arrow.AddVertexAt(vertexIndexes[0], new Point2d(arrowSingleText.Position.X, arrowSingleText.Position.Y), 0, 0, 0);
			arrow.AddVertexAt(vertexIndexes[1], new Point2d(arrowSingleText.Position.X + arrowWidth / 2, arrowSingleText.Position.Y), 0, arrowHeight, 0);
			arrow.AddVertexAt(vertexIndexes[2], new Point2d(arrowSingleText.Position.X + arrowWidth, arrowSingleText.Position.Y), 0, 0, 0);
			arrow.Elevation = middlePoint.Z;

			//arrow.AddVertexAt(vertexIndexes[0], new Point2d(centerX - 0.5 * arrowWidth, centerY + diameterOffset + diameterTextExtents.Height / 2), 0, 0, 0);
			//arrow.AddVertexAt(vertexIndexes[1], new Point2d(centerX, centerY + diameterOffset + diameterTextExtents.Height / 2), 0, arrowHeight, 0);
			//arrow.AddVertexAt(vertexIndexes[2], new Point2d(centerX  + 0.5 * arrowWidth, centerY + diameterOffset + diameterTextExtents.Height / 2), 0, 0, 0);

			//data.Btr.AppendEntity(arrow);
			ents.Add(arrow);
			//modelSpace.AppendEntity(arrow);
			//tr.AddNewlyCreatedDBObject(arrow, true);

			double rotationAngle = angleRad;
			if (rotationAngle < 0)
				rotationAngle += 2 * Math.PI;
			if (Math.Cos(rotationAngle) < 0)
				rotationAngle -= Math.PI;
			Matrix3d rotation = Matrix3d.Rotation(rotationAngle, Vector3d.ZAxis, middlePoint.ToCADPoint());
			materialText.TransformBy(rotation);
			diameterText.TransformBy(rotation);
			slopeText.TransformBy(rotation);
			lengthText.TransformBy(rotation);
			arrow.TransformBy(rotation);

			return ents;
		}
		/// <summary>
		/// Single text displayed for pipe label.
		/// </summary>
		private class SingleText
		{
			public bool IsArrow = false;
			public MyUtilities.Geometry.Rectangle<double> Rectangle;
			public Point3d Position;
			public SingleText(MyUtilities.Geometry.Rectangle<double> rec, Point3d pt = new Point3d())
			{
				Rectangle = rec;
				Position = pt; 
			}
		}
		/// <summary>
		/// Checks if all text can fit to a single row.
		/// </summary>
		/// <param name="pipeWidth"></param>
		/// <param name="texts"></param>
		/// <param name="tester">Used to test for extents (according to current DB settings)</param>
		/// <returns></returns>
		private static bool DoesFitToSingleLine(double pipeWidth, double arrowWidth, DBText tester, params string[] textsWithSpacing)
		{
			bool res = false;
			double width = 0;
			width += arrowWidth;
			foreach(string s in textsWithSpacing)
			{
				tester.TextString = s;
				var extents = tester.GeometricExtents.ToRectangle();
				width += extents.Width;
			}
			if (width < 0.9 * pipeWidth)
				res = true;
			return res;
		}
		/// <summary>
		/// Calculates text positions in a single row or in multiple rows, when pipe length is too small.
		/// </summary>
		/// <param name="center"></param>
		/// <param name="pipeWidth"></param>
		/// <param name="pipeLenght2d"></param>
		/// <param name="offsetByPipeDiameter"></param>
		/// <param name="arrowWidth"></param>
		/// <param name="arrowHeight"></param>
		/// <param name="spacingBetweenText"></param>
		/// <param name="texts">Order: arrow, material, diameter,...</param>
		private static void calculateTextPositions(Point3d center, 
													double pipeWidth, 
													double pipeLenght2d, 
													bool offsetByPipeDiameter, 
													double arrowWidth, double arrowHeight,
													double spacingBetweenText,
													params SingleText[] texts)
													//SingleText materialText,
													//SingleText diameterText,
													//SingleText slopeText,
													//SingleText lengthText,
													//SingleText arrowText)
		{
			double pipeRadius = pipeWidth / 2;
			double offsetFromPipe = offsetByPipeDiameter ? pipeRadius : 0;
			// Offset between texts in the same row.
			double textHorizontalOffset = spacingBetweenText;// arrowWidth / 2;
			// Offset between rows.
			double textRowOffset = textHorizontalOffset / 2;
			{
				//var arrowExtents = new MyUtilities.Geometry.Rectangle<double>(0, 0, arrowHeight, arrowWidth);
				//var arrowText = new SingleText(arrowExtents);
				var elements = texts.ToList();

				/*
				 * Labels are drawn at the "center" point (which is center point of the pipe).
				 * If pipe is short, then labels are divided into parts, trying to fit as many
				 * texts as possible in a single line. But, in any case, at least 1 text will be
				 * put in 1 line.
				 * 
				 * Process is done in 3 steps:
				 * 1 - Divide all texts into rows (depending on total text length and pipe length)
				 * 2 - Align texts in a single row, relative to (0,0) (they are aligned respectively to x = 0, but y has 
				 *	   more parts:
				 *	   0 row - goes above the pipe, distanced for pipe radius (+ space between rows)
				 *	   1 row - goes below the pipe, distanced also for pipe radius (+ space between rows)
				 *	   other rows - go below line above by space between rows.
				 *	   )
				 * 3 - Make final alignment - here text rows are translated to absolute position (relative to "center"), 
				 *     and 2 possibilities for horizontal alignment between rows:
				 *     1 - center alignment - every row middleX is at "center.X" position.
				 *     2 - left alignment - widest row middleX is aligned to "center.X", and all other rows are aligned
				 *		   to the start of that row (all rows start x is equal)
				 *	   
				 */

				 // Texts are divided into rows (inner list represents all texts in a single row).
				List<List<SingleText>> rows = new List<List<SingleText>>();
				double allowedTextWidth = pipeLenght2d * 0.9;
				int currentRow = 0;
				while (elements.Any())
				{
					// Width of all remaining elements.
					double remainingWidth = elements.Sum(x => x.Rectangle.Width + textHorizontalOffset) - textHorizontalOffset;
					bool isSingleLine = remainingWidth < allowedTextWidth;

					if (isSingleLine)
					{
						// All remaining elements are in the single row.
						rows.Add(elements.ToList());						
						// Stop iteration
						elements.Clear();
					}
					else
					{
						// Find how many texts can fit in current line.
						int rowCount = 0;
						double width = 0;
						for (int i = 0; i < elements.Count; i++)
						{
							var elem = elements[i];
							double w = elem.Rectangle.Width;
							if (width + textHorizontalOffset + w < allowedTextWidth)
								width += textHorizontalOffset + w;
							else
							{
								rowCount = i;
								break;
							}
						}
						// Above pipe there is only an arrow.
						//if (currentRow == 0 || rowCount == 0)
						//rowCount = 1;
						if (rowCount == 0)
							rowCount = 1;
						// Diameter and material are in the same row.
						//if (currentRow == 1)
							//rowCount = 2;// arrow, diameter and material are in the same row.
						// Add new row.
						rows.Add(elements.GetRange(0, rowCount));
						// Remove elements added to the row.
						elements = elements.Skip(rowCount).ToList();
					}
					currentRow++;
				}

				// Texts are divided into rows. Now need to align them (y is same for single row, x 
				// is set starting from 0)
				// Top Y coordinate for current row (text goes up to this position).
				double rowTopY = offsetFromPipe + textRowOffset;
				// Needed so rowTopY is top for the first row.
				rowTopY += rows[0].Max(x=>x.Rectangle.Height);
				for (int i = 0; i < rows.Count; i++)
				{
					var row = rows[i];
					double rowHeight = row.Max(x => x.Rectangle.Height);
					double rowBottomY = rowTopY - rowHeight;
					double currentX = 0;
					for (int j = 0; j < row.Count; j++)
					{
						var elem = row[j];
						elem.Position = new Point3d(currentX, rowBottomY, 0);
						if (elem.IsArrow)
							elem.Position = elem.Position.Add(new Vector3d(0, rowHeight / 2, 0));

						currentX += elem.Rectangle.Width + textHorizontalOffset;
					}
					// For next row, set topY.
					rowTopY = rowBottomY - textRowOffset;
					// If this is first row, there is pipe diameter
					if (i == 0)
						rowTopY -= pipeRadius + pipeRadius + textRowOffset;
				}


				// Set rows final horizontal alignment, and positions at the "center".
				bool centerAligned = true;// If false, left aligned. If true, center aligned.
				// If left aligned, need to know widest row.
				double totalMaxWidth = rows.Max(x => x.Sum(y => y.Rectangle.Width) + (x.Count - 1) * textHorizontalOffset);
				// Apply additionalOffset (text is left aligned, but center is not in the middle of widest row, but 
				// on the center of the arrow - first texts).
				double additionalOffsetX = centerAligned ? 0 : totalMaxWidth / 2 - arrowWidth / 2;
				// But applied only when arrow is alone in the first row
				if (rows[0].Count > 1)
					additionalOffsetX = 0;
				foreach(List<SingleText> row in rows)
				{
					double offsetX = 0;
					if (centerAligned)
					{
						double rowWidth = row.Sum(x => x.Rectangle.Width + textHorizontalOffset) - textHorizontalOffset;
						offsetX = -rowWidth / 2;
					}
					else
						offsetX = -totalMaxWidth / 2;
					// Move to final position, and align horizontaly.
					Vector3d moveX = new Vector3d(offsetX + additionalOffsetX + center.X, center.Y, center.Z);
					foreach (SingleText st in row)
						st.Position = st.Position.Add(moveX);
				}				
			}			
		}		
	}
}
