﻿using CadPlugin.Inventories.Linetypes;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.Drawers.Linetypes
{
	abstract class PipeLinetype
	{
		protected SewageObject obj { get; private set; }
		public void SetSewageObject(SewageObject s)
		{
			obj = s;
			Refresh();
		}
		protected virtual void Refresh()
		{

		}

		public abstract string GetLinetype();

		protected static string GetEntwaesserungsartLinetype(AbwassertechnischeAnlagen ab)
		{
			string lineType = "CONTINUOUS";
			switch (ab.Entwaesserungsart)
			{
				case "KR":
				case "DR":
				case "GR":
					lineType = LinetypeManager.Dashed_21;
					break;
				case "KS":
				case "DS":
				case "GS":
					lineType = LinetypeManager.Continuous;
					break;
				case "KM":
				case "DM":
				case "GM":
					lineType = LinetypeManager.Dashed_31p1;
					break;
			}
			return lineType;
		}
	}
}
