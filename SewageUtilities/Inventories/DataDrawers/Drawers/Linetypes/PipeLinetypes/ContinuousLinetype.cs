﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.Drawers.Linetypes.PipeLinetypes
{
	class ContinuousLinetype : PipeLinetype
	{
		public override string GetLinetype()
		{
			return Inventories.Linetypes.LinetypeManager.Continuous;
		}
	}
}
