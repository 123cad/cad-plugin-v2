﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.Drawers.DamageDrawerFilters
{
	class StationHighestDamage:DamageDrawerFilter
	{
		public override FilteredDrawingData GetFilteredData()
		{
			// Get only stations which have at least one damage of highest class, and then create FilteredStations.
			var data = Data.Select(x =>
			{
				var t = new FilteredStation() { Station = x.Key };
				DamageClass highest = x.Value.Max(y => y.DamageClass);
				t.Damages.AddRange(x.Value.Where(m => m.DamageClass == highest));
				return t;
			});
			var p = new FilteredDrawingData();
			p.Stations.AddRange(data);
			return p;
		}
	}
}
