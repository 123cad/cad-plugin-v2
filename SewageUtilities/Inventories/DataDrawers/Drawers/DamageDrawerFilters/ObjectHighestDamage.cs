﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.Drawers.DamageDrawerFilters
{
	class ObjectHighestDamage : DamageDrawerFilter
	{
		public override FilteredDrawingData GetFilteredData()
		{
			DamageClass highest = Data.Max(x => x.Value.Max(y => y.DamageClass));
			// Get only stations which have at least one damage of highest class, and then create FilteredStations.
			var data = Data.Where(q=>q.Value.FirstOrDefault(r=>r.DamageClass == highest) != null).Select(x =>
			{
				var t = new FilteredStation() { Station = x.Key };
				t.Damages.AddRange(x.Value.Where(m=>m.DamageClass == highest));
				return t;
			});
			var p = new FilteredDrawingData();
			p.Stations.AddRange(data);
			return p;
		}
	}
}
