﻿using CadPlugin.Common;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.Shafts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Inventories.DataDrawers.Drawers;
using static CadPlugin.Inventories.InventoryManager;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.Drawers
{
	public abstract class ShaftDrawer
	{
		public enum ShaftType
		{
			Circle,
			Rectangle
		}

		/// <summary>
		/// Indicates if data is drawn in 3d (pipes and shafts as 3d solids). 
		/// </summary>
		public bool IsDrawn3d { get; private set; }
		/// <summary>
		/// Diameter of damage cylinder in percent, relative to pipe diameter.
		/// </summary>
		protected static double percentageOfDamageRing = 1.05; // 1.1 = 10% bigger

		public bool DrawDamageShaft { get; set; } = true;

		public TransactionData Tr { get; private set; }
		public SewageObjectColorManager ColorManager { get; private set; }
		public DamageTextDrawer DamageTextManager { get; private set; }
		public DamageDrawerFilter DamageFilter { get; private set; }
		public TextSizeDefinitions TextSize { get; private set; }
		public InventoryManager.CreatedCADEntities CreatedEntities { get; private set; }

		public ShaftDrawer(TransactionData data, SewageObjectColorManager colorManager, DamageTextDrawer damageTxt,
			TextSizeDefinitions textDef, DamageDrawerFilter damageFilter, CreatedCADEntities createdEntities, bool drawIn3d)
		{
			Tr = data;
			ColorManager = colorManager;
			DamageTextManager = damageTxt;
			DamageFilter = damageFilter;
			TextSize = textDef;
			CreatedEntities = createdEntities;
			IsDrawn3d = drawIn3d;
		}
		/// <summary>
		/// Draws pipe damages with text.
		/// </summary>
		/// <param name="shaft"></param>
		public abstract void DrawDamages(Shaft shaft, ShaftType shaftType = ShaftType.Circle);

		public abstract void DrawShaft(Shaft shaft, ShaftType shaftType = ShaftType.Circle);

		/// <summary>
		/// Creates base for a shaft (it can be circle or rectangle), with its center in position point.
		/// Entity IS added to ModelSpace and Transaction.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="t"></param>
		/// <param name="position"></param>
		/// <param name="lengthX">For circle this is diameter.</param>
		/// <param name="lengthY">For circle this is ignored.</param>
		/// <returns></returns>
		public static Entity CreateShaftBase(TransactionData data, ShaftType t, Point3d position, double lengthX, double lengthY = 0)
		{
			double centerX = 0;// position.X;
			double centerY = 0;// position.Y;
			double centerZ = 0;// position.Z;
			double centerOffsetX = lengthX / 2;
			double centerOffsetY = lengthY / 2;

			Entity e = null;
            // NOTE If Polyline3d is not added to Database, solid creation will fail. Same does not apply to Circle.
            // I think problem is with AppendVertex, which is database resident.

			switch (t)
			{
				case ShaftType.Rectangle:
                    //Polyline3d polyBase = data.Tr.CreateEntity<Polyline3d>(data.Btr);
					Polyline polyBase = data.CreateEntity<Polyline>();                  
					polyBase.Closed = true;
					Point3d[] pts = new Point3d[4];
					pts[0] = new Point3d(centerX - centerOffsetX, centerY + centerOffsetY, 0/*position.Z*/);
					pts[1] = new Point3d(centerX + centerOffsetX, centerY + centerOffsetY, 0/*position.Z*/);
					pts[2] = new Point3d(centerX + centerOffsetX, centerY - centerOffsetY, 0/*position.Z*/);
					pts[3] = new Point3d(centerX - centerOffsetX, centerY - centerOffsetY, 0/*position.Z*/);
					polyBase.AddPoints(pts.Select(x=>x.GetAs2d()).ToArray());
					//polyBase.Elevation = position.Z;

					//for (int i = 0; i < 4; i++)
						//polyBase.AppendPoint(pts[i]);
					e = polyBase;
					break;
				case ShaftType.Circle:
					Circle c = data.Tr.CreateEntity<Circle>(data.Btr);
                    //c.Center = position;
                    double diameter = lengthX;
                    if (diameter < 0.05)
                        diameter = 0.05;
					c.Diameter = diameter;
					e = c;
					break;
			}
			return e;
		}
	}
}
