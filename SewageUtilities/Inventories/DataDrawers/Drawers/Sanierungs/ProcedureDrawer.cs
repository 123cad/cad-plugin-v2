﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventory.CAD;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Sanierungs;
using CadPlugin.Common;
using Isybau2015.Isybau2015.ReferenceTableEnums;
using geom = MyUtilities.Geometry;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.DataDrawers.Drawers.Sanierungs
{
	/// <summary>
	/// Draws sanierung procedures (like stations for damages).
	/// </summary>
	class ProcedureDrawer
	{
		private static double StationTextHeight = 0.3;
		private static double ProcedureTextHeight = StationTextHeight * 1.2;
		/// <summary>
		/// 
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="obj"></param>
		/// <param name="pipe"></param>
		/// <param name="massnahme"></param>
		/// <param name="diameterUsed">If true, pipe diameter will be drawn. </param>
		/// <returns></returns>
		public static IEnumerable<Entity> DrawPipeSingleProcedure(TransactionData tr, SingleObject obj, Pipes.Pipe pipe, Massnahme massnahme, bool diameterUsed = false)
		{
			List<Entity> entities = new List<Entity>();
			var c = Isybau2015.Isybau2015.Colors.Verfahrensbezeichnung.GetVerfahrensbezeichnung(massnahme.Verfahrensbezeichnung);
			var color = Color.FromColor(c);
			double stationDistance = massnahme.Lagedaten.StationierungAnfang;
			// Half of total line (which crosses pipe) lenght for station.
			double stationLineLength = 0.5 + (diameterUsed ? pipe.Diameter/2 : 0);
			// Offset of the line which is parallel to pipe - when sanierung is line.
			double sanierungLineOffset = stationLineLength - 0.25;
			double lineTextPadding = stationLineLength * 0.1;

			MText procedureText = CreateProcedureText(tr, massnahme, ProcedureTextHeight);
			MText sanierungNameText = CreateSanierungText(tr, massnahme, StationTextHeight);
			MText stationText = CreateStationDistanceText(tr, massnahme, stationDistance, StationTextHeight);
			entities.Add(procedureText);
			entities.Add(sanierungNameText);
			entities.Add(stationText);

			geom.Vector3d pipeVector = pipe.StartPoint.GetVectorTo(pipe.EndPoint);
			geom.Vector3d pipeUnitVector = pipeVector.GetUnitVector();
			geom.Vector3d rightPerpendicular = pipeVector.GetWithZ().CrossProduct(geom.Vector3d.ZAxis).GetUnitVector();

			Line stationLine = tr.CreateEntity<Line>();
			stationLine.StartPoint = new Point3d(-stationLineLength, 0, 0);// pipe.StartPoint.Add(pipeVector.Multiply(stationDistance)).Add(rightPerpendicular.Multiply(stationLineLength / 2)).ToCADPoint();
			stationLine.EndPoint = new Point3d(stationLineLength, 0, 0);// pipe.StartPoint.Add(pipeVector.Multiply(stationDistance)).Add(rightPerpendicular.Negate().Multiply(stationLineLength / 2)).ToCADPoint();
			entities.Add(stationLine);


			// Rotate to correct position.
			bool isLineSanierung = massnahme.Sanierungsumfang == (int)S102_Sanierungsumfang.Gesamte
									|| massnahme.Sanierungsumfang == (int)S102_Sanierungsumfang.Partiell;
			geom.Point3d stationPoint = pipe.StartPoint.Add(pipeUnitVector.Multiply(stationDistance));
			double textPadding = stationLineLength;
			textPadding *= 1.2;
			AlignTextToStation(stationText, sanierungNameText, procedureText, stationLine, textPadding, stationPoint, rightPerpendicular);
			if (isLineSanierung)
			{
				// Create additional station line with distance
				// Move procedure name to the middle of the pipe
				double stationEndDistance = massnahme.Lagedaten.StationierungEnde;
				MText sanierungEndText = CreateSanierungText(tr, massnahme, StationTextHeight);
				MText stationEndText = CreateStationDistanceText(tr, massnahme, stationEndDistance, StationTextHeight);
				Line stationEndLine = tr.CreateEntity<Line>();
				stationEndLine.StartPoint = new Point3d(-stationLineLength, 0, 0);
				stationEndLine.EndPoint = new Point3d(stationLineLength, 0, 0);
				entities.Add(sanierungEndText);
				entities.Add(stationEndText);
				entities.Add(stationEndLine);

				var stationEndPoint = pipe.StartPoint.Add(pipeUnitVector.Multiply(stationEndDistance));
				AlignTextToStation(stationEndText, sanierungEndText, procedureText, stationEndLine, textPadding, stationEndPoint, rightPerpendicular);

				geom.Point3d middle = pipe.StartPoint.Add(pipeUnitVector.Multiply((stationDistance + stationEndDistance) / 2));

				var perpendicular = rightPerpendicular;
				// Line and text side is switched. 
				if (Math.Abs(geom.Vector3d.XAxis.GetAngleRadXY(pipeVector)) > Math.PI/2)
					perpendicular = perpendicular.Negate();

				AlignLineTextToMiddle(procedureText, stationLineLength, middle, perpendicular, pipeVector);

				// Line which connects start and end.
				Line sanierungLine = tr.CreateEntity<Line>();
				entities.Add(sanierungLine);
				sanierungLine.StartPoint = pipe.StartPoint.Add(pipeUnitVector.Multiply(stationDistance).Add(perpendicular.Multiply(sanierungLineOffset))).ToCADPoint();
				sanierungLine.EndPoint = pipe.StartPoint.Add(pipeUnitVector.Multiply(stationEndDistance).Add(perpendicular.Multiply(sanierungLineOffset))).ToCADPoint();
			}
			foreach (var t in entities)
				t.Color = color;
			return entities;
		}
		public static IEnumerable<Entity> DrawShaftSingleProcedure(TransactionData tr, SingleObject obj, Shafts.Shaft shaft, IEnumerable<Massnahme> massnahmes)
		{
			List<Entity> allEntities = new List<Entity>();
			geom.Vector3d initialVector = new geom.Vector3d(-shaft.Radius * 0.7 , -shaft.Radius * 0.7, 0);
			if (massnahmes.Count() > 1)
				initialVector = new geom.Vector3d(-shaft.Radius * 1.5 , 0, 0);
			geom.Point3d endTextCurrentPosition = shaft.Center.Add(initialVector);
			geom.Point3d lineStartPoint = shaft.IsCoverSet ? shaft.CoverPoint : shaft.Center;

			// To align all texts, we will do that at the end.
			List<Tuple<MText, MText, geom.Point3d>> textPairs = new List<Tuple<MText, MText, geom.Point3d>>();
			double maxProcedureTextWidth = -1;
			double maxSanierungNameTextWidth = -1;
			double bottomZ = shaft.Center.Z;

			foreach (Massnahme massnahme in massnahmes)
			{
				List<Entity> entities = new List<Entity>();
				var c = Isybau2015.Isybau2015.Colors.Verfahrensbezeichnung.GetVerfahrensbezeichnung(massnahme.Verfahrensbezeichnung);
				var color = Color.FromColor(c);
				double stationDistance = massnahme.Lagedaten.StationierungAnfang;
				endTextCurrentPosition = new geom.Point3d(endTextCurrentPosition.X, endTextCurrentPosition.Y, bottomZ + stationDistance);

				MText procedureText = CreateProcedureText(tr, massnahme, ProcedureTextHeight);
				MText sanierungNameText = CreateSanierungText(tr, massnahme, StationTextHeight);
				entities.Add(procedureText);
				entities.Add(sanierungNameText);

				Line line = tr.CreateEntity<Line>();
				entities.Add(line);
				line.StartPoint = lineStartPoint.ToCADPoint();
				line.EndPoint = endTextCurrentPosition.ToCADPoint();


				var recProcedure = procedureText.GeometricExtents.ToRectangle();
				var recName = sanierungNameText.GeometricExtents.ToRectangle();
				if (maxProcedureTextWidth < recProcedure.Width)
					maxProcedureTextWidth = recProcedure.Width;
				if (maxSanierungNameTextWidth < recName.Width)
					maxSanierungNameTextWidth = recName.Width;


				textPairs.Add(new Tuple<MText, MText, geom.Point3d>(procedureText, sanierungNameText, endTextCurrentPosition));

				foreach (var t in entities)
					t.Color = color;
				allEntities.AddRange(entities);
				endTextCurrentPosition = endTextCurrentPosition.Add(new geom.Vector3d(0, -recProcedure.Height * 1.2, 0));
			}
			// Align all texts
			double offsetProcedureText = maxProcedureTextWidth + maxSanierungNameTextWidth * 1.2;
			double offsetText = maxSanierungNameTextWidth * 1.05;


			foreach(var t in textPairs)
			{
				var vector = t.Item3.GetAsVector();
				Matrix3d moveProcedure = Matrix3d.Displacement(vector.Add(new geom.Vector3d(-offsetProcedureText, 0, 0)).ToCADVector());
				Matrix3d moveName = Matrix3d.Displacement(vector.Add(new geom.Vector3d(-offsetText, 0, 0)).ToCADVector());
				t.Item1.TransformBy(moveProcedure);
				t.Item2.TransformBy(moveName);
			}
			textPairs.Clear();

			return allEntities;
		}
		private static MText CreateProcedureText(TransactionData tr, Massnahme mass, double procedureTextHeight)
		{
			MText t = tr.CreateEntity<MText>();
			t.TextHeight = procedureTextHeight;
			string commentText = "";
			if (!string.IsNullOrEmpty(mass.Kommentar))
				commentText = $"\\H0.4x;{mass.Kommentar}\\H2.5x;"; // 2.5 cancels out 0.4, because next multiplication will use 0.4.
			string procedureText = mass.Verfahrensbezeichnung + $"{commentText}\\P";
			string yearText = $"\\H0.3x;{mass.Abnahmedatum.Year.ToString()}"; // H0.5x means text size.
			t.Contents = procedureText + yearText;
			t.LineSpacingFactor = 0.7;// Distance of second line to first line.
			t.Location = new Point3d(0, procedureTextHeight, 0);
			return t;
		}
		private static MText CreateSanierungText(TransactionData tr, Massnahme mass, double textHeight)
		{
			MText text = tr.CreateEntity<MText>();
			text.TextHeight = textHeight;
			text.Contents = mass.BezeichnungMassnahme;
			text.Location = new Point3d(0, textHeight, 0);
			return text;
		}
		private static MText CreateStationDistanceText(TransactionData tr, Massnahme mass, double distance, double textHeight)
		{
			MText text = tr.CreateEntity<MText>();
			text.TextHeight = textHeight;
			text.Contents = DoubleHelper.ToStringInvariant(distance, 2);
			text.Location = new Point3d(0, textHeight, 0);
			return text;
		}
		/// <summary>
		/// Provide all texts aligned with bottom left part (of first line) to (0,0,0), and line center is on (0,0,0)
		/// </summary>
		/// <param name="stationText"></param>
		/// <param name="sanierungText"></param>
		/// <param name="procedureText"></param>
		/// <param name="pipeOffset"></param>
		/// <param name="stationPoint"></param>
		/// <param name="rightPerpendicular"></param>
		private static void AlignTextToStation(MText stationText, MText sanierungText, MText procedureText, Line stationLine,
												double pipeOffset, geom.Point3d stationPoint, geom.Vector3d rightPerpendicular)
		{
			// Align texts on (0,0,0), and then rotate and translate to final position.
			var recSanierung = sanierungText.GeometricExtents.ToRectangle();
			var recStation = stationText.GeometricExtents.ToRectangle();
			var recProcedure = procedureText.GeometricExtents.ToRectangle();
			double offsetY = -recSanierung.Height / 2; // We want text in the middle of the line.
			Matrix3d move = Matrix3d.Displacement(new Vector3d(-pipeOffset - recStation.Width, offsetY, 0));
			stationText.TransformBy(move);
			move = Matrix3d.Displacement(new Vector3d(pipeOffset, offsetY, 0));
			sanierungText.TransformBy(move);
			move = Matrix3d.Displacement(new Vector3d(pipeOffset + recSanierung.Width * 1.2, offsetY, 0));
			procedureText.TransformBy(move);

			List<Entity> texts = new List<Entity> { stationText, sanierungText, procedureText, stationLine };
			double angle = new geom.Vector2d(1, 0).GetAngleRad(rightPerpendicular.GetAs2d());
			// Keep text aligned to the user.
			bool rotateText = Math.Abs(angle) > Math.PI / 2;
			if (rotateText)
				angle += Math.PI;
			Matrix3d rotation = Matrix3d.Rotation(angle, Vector3d.ZAxis, new Point3d());// stationPoint.ToCADPoint());
			texts.ForEach(x => x.TransformBy(rotation));
			Matrix3d translation = Matrix3d.Displacement(stationPoint.GetAsVector().ToCADVector());
			texts.ForEach(x => x.TransformBy(translation));


		}
		private static void AlignLineTextToMiddle(MText procedureText, double pipeOffset, geom.Point3d lineSanierungMiddle, geom.Vector3d perpendicular, geom.Vector3d pipeVector)
		{
			var pipeUnitVector = pipeVector.GetWithZ().GetUnitVector();
			// We use positioning of procedure, so method is consistent.
			// But for line damage procedure appears in the middle of the line sanierung.
			procedureText.Location = new Point3d(0, ProcedureTextHeight / 2, 0);
			procedureText.Rotation = 0;

			double angleRad = geom.Vector3d.XAxis.GetAngleRadXY(perpendicular);
			if (Math.Abs(angleRad) > Math.PI / 2)
				angleRad += Math.PI;
			geom.Point3d middle = lineSanierungMiddle;// pipe.StartPoint.Add(pipeUnitVector.Multiply((stationDistance + stationEndDistance) / 2));
			Matrix3d rotate = Matrix3d.Rotation(angleRad, Vector3d.ZAxis, new Point3d());
			Matrix3d move = Matrix3d.Displacement(middle.GetAsVector().Add(perpendicular.Multiply(pipeOffset)).ToCADVector());
			var m = move.PostMultiplyBy(rotate);
			procedureText.TransformBy(m);
			//procedureText.TransformBy(rotate);
			//procedureText.TransformBy(move);
			

		}
	}
}
