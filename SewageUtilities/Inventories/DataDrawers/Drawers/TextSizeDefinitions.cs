﻿using CadPlugin.Inventories.View.SelectDrawingOptionsTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.Drawers
{

	public class TextSizeDefinitions
	{
		public double ShaftBlockAttributeName = 0.5;
		public double ShaftBlockAttributeNotName = 0.4;
		/// <summary>
		/// Text size of all damage related text (kodes on the station, distance and class).
		/// </summary>
		public double DamageText = 0.4;
		/// <summary>
		/// Text size of all pipe related text.
		/// </summary>
		public double PipeTextSize = 0.5;

		public TextSizeDefinitions()
		{

		}
		public TextSizeDefinitions(Scale scale)
		{
			switch (scale)
			{
				case Scale._5000:
					ShaftBlockAttributeName = 2;
					ShaftBlockAttributeNotName = 1.9;
					DamageText = 2;
					PipeTextSize = 2;
					break;
				case Scale._1000:
					ShaftBlockAttributeName = 1.62;
					ShaftBlockAttributeNotName = 1.52;
					DamageText = 0.8;
					PipeTextSize = 1.6;
					break;
				case Scale._500:
					ShaftBlockAttributeName = 0.81;
					ShaftBlockAttributeNotName = 0.72;
					DamageText = 0.5;
					PipeTextSize = 0.8;
					break;
				case Scale._250:
					ShaftBlockAttributeName = 0.5;
					ShaftBlockAttributeNotName = 0.45;
					DamageText = 0.5;
					PipeTextSize = 0.5;
					break;
			}
		}
	}
}
