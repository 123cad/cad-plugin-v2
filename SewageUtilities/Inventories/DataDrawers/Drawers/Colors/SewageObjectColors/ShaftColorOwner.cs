﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Inventories.Pipes;
using CadPlugin.Inventories.Colors;

#if BRICSCAD
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.Drawers.Colors
{
	class ShaftColorOwner : SewageObjectColor
	{
		public override Color GetObjectColor()
		{
			Color c;
			string owner = SewageObjectHelpers.GetOwner(obj.Abwasser);
			if (owner == "K")
			{
				c = ColorAssociations.GetEntwaesserungColor(obj.Abwasser.Entwaesserungsart);
			}
			else
				c = Color.FromColor(System.Drawing.Color.Gray);

			return c;
		}
	}
}
