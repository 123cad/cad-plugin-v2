﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using   CadPlugin.Inventories.Colors;
using   CadPlugin.Inventories.Shafts;
using Isybau2015.Isybau2015.ReferenceTableEnums;

#if BRICSCAD
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Colors;
#endif

namespace   CadPlugin.Inventories.Drawers.Colors
{
	class ShaftColorStatus : SewageObjectColor
	{
		public override Color GetObjectColor()
		{
			Color c = null;
			switch (obj.Abwasser.Status)
			{
				case (int)G105_Status.vorhanden:
					c = ColorAssociations.GetEntwaesserungColor(obj.Abwasser.Entwaesserungsart);
					break;
				case (int)G105_Status.ausserBetrieb:
					c = Color.FromColor(System.Drawing.Color.LightGray);
					break;
				default:
					c = Color.FromColorIndex(ColorMethod.ByColor, 7);
					break;
			}
			return c;
		}
	}
}
