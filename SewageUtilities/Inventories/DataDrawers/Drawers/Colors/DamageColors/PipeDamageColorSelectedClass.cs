﻿using   CadPlugin.Inventories.Pipes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using   CadPlugin.Inventories.Colors;

#if BRICSCAD
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Colors;
#endif
namespace   CadPlugin.Inventories.Drawers.Colors
{
	class PipeDamageColorSelectedClass : DamageColor
	{
		/// <summary>
		/// Only this class has color - all others are of the same color.
		/// </summary>
		private HashSet<DamageClass> selectedClasses { get; set; }
		/// <summary>
		/// Color for classes which are not selected.
		/// </summary>
		public static Color NonSelectedClass { get; private set; }
		public PipeDamageColorSelectedClass(IEnumerable<DamageClass> selected)
		{
			selectedClasses = new HashSet<DamageClass>();
			foreach (DamageClass dc in selected)
				if (!selectedClasses.Contains(dc))
					selectedClasses.Add(dc);
			NonSelectedClass = Color.FromColor(System.Drawing.Color.Gray);
		}
		public override Color GetDamageColor(DamageClass dg)
		{
			if (selectedClasses.Contains(dg))
				return ColorAssociations.GetDamageClassColor(dg);
			else
				return NonSelectedClass;
		}
	}
}
