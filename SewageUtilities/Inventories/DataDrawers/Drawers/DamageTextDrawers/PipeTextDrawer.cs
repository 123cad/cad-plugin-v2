﻿using CadPlugin.Inventories.Drawers;
using CadPlugin.Inventories.Drawers.DamageDescriptionAlignments;
using CadPlugin.Inventories.Pipes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Inventories.Damages;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.DataDrawers.Drawers.DamageTextDrawers
{
	class PipeTextDrawer : DamageTextDrawer
	{
		public PipeTextDrawer(TransactionData tr, StationDamagesDrawer damageDrawer) : base(tr, damageDrawer)
		{
		}

		public override IEnumerable<Entity> DrawText(DamageColor colorMgr, double textSize, bool draw3d, DamageStation st, IEnumerable<SingleDamage> filteredStationDamages, PositionVector position)
		{
			List<Entity> ents = new List<Entity>();
			StationDamagesDrawer.TextHeight = textSize;

			// yield not good for methods which have side effects - not just return collection.
			// Because these methods are invoked only when collection is iterated.
			double pipeRadius = ((Pipe)st.ParentObject).Diameter / 2;
			StationDamageDescriptionAligned al = damagesDescriptiondrawer.AlignStationTexts(st.Distance, pipeRadius, position, filteredStationDamages);
			if (al.Texts.Count == 0)
				return ents;
			foreach (SingleDescriptionPosition sp in al.Texts)
			{
				sp.TextColor = colorMgr.GetDamageColor(sp.DamageClass);
			}
			var d = damagesDescriptiondrawer.DrawText(Tr, al, draw3d);
			foreach (var p in d)
				ents.Add(p);
			return ents;
			
		}
	}
}
