﻿using CadPlugin.Inventories.Drawers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.Drawers.DamageDescriptionAlignments;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.Damages;
using CadPlugin.Inventories.Shafts;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.DataDrawers.Drawers.DamageTextDrawers
{
	class ShaftTextDrawer : DamageTextDrawer
	{
		public ShaftTextDrawer(TransactionData tr, StationDamagesDrawer damageDrawer) : base(tr, damageDrawer)
		{
		}

		public override IEnumerable<Entity> DrawText(DamageColor colorMgr, double textSize, bool draw3d, DamageStation st, IEnumerable<SingleDamage> filteredStationDamages, PositionVector position)
		{
			StationDamagesDrawer.TextHeight = textSize;

			double shaftRadius = ((Shaft)st.ParentObject).Radius;

			StationDamageDescriptionAligned aligned = damagesDescriptiondrawer.AlignStationTexts(st.Distance, shaftRadius, position, filteredStationDamages);
			foreach (SingleDescriptionPosition sp in aligned.Texts)
			{
				sp.TextColor = colorMgr.GetDamageColor(sp.DamageClass);
			}
			var d = damagesDescriptiondrawer.DrawText(Tr, aligned, draw3d);
			foreach (var p in d)
				yield return p;
		}
	}
}
