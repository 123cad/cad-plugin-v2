﻿using CadPlugin.Inventories.Damages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.Drawers
{
	public class StationTextData
	{
		public DamageStation Station { get; private set; }
		public PositionVector Position { get; private set; }
		public StationTextData(DamageStation st, PositionVector pos)
		{
			Station = st;
			Position = pos;
		}
	}
}
