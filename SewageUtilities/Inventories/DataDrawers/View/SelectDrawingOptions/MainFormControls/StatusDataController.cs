﻿using CadPlugin.Inventories.View.SelectDrawingOptionsTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.View.SelectDrawingOption.MainFormControls
{
	class StatusDataController
	{
		public event Action<DrawDamage> DamageDrawDrawTypeChanged;

		private StatusDataView view { get; set; }
		private SelectedDrawingOptions options { get; set; }
		public DrawDamage CurrentDrawDamageType => view.CurrentDrawDamageSelection;
		public StatusDataController(StatusDataView v, SelectedDrawingOptions opt, bool drawStatus)
		{
			view = v;
			options = opt;
			view.ContainerControl.Enabled = drawStatus;
			view.DamageDrawDrawTypeChanged += e =>
			{
				DamageDrawDrawTypeChanged?.Invoke(e);
			};
			view.Initialize(opt);
		}

		/// <summary>
		/// Hides selection of classes for drawing, and returns 
		/// height pixel count for which control is smaller.
		/// </summary>
		public int HideClassesSelection()
		{
			return view.HideClassesSelection();
		}
		/// <summary>
		/// Shows classes selection and returns height of hidden part.
		/// </summary>
		/// <returns></returns>
		public int ShowClassesSelection()
		{
			return view.ShowClassesSelection();
		}


	}
}
