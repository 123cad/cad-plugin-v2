﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CadPlugin.Inventories.View.SelectDrawingOptionsTypes;
using CadPlugin.Inventories.Colors;

namespace CadPlugin.Inventories.DataDrawers.View.SelectDrawingOption.MainFormControls
{
	public partial class StatusDataView : UserControl
	{
		public event Action<DrawDamage> DamageDrawDrawTypeChanged;
		public DrawDamage CurrentDrawDamageSelection => (DrawDamage)comboBoxDrawDamageTypes.SelectedValue;
		public Control ContainerControl => this;
		public StatusDataView()
		{
			InitializeComponent();
		}
		public void Initialize(SelectedDrawingOptions opts)
		{
			Action<int> createDataBinding = (index) =>
			{
				CheckBox drawCheckbox = null;
				CheckBox colorCheckbox = null;
				Label previewColor = null;
				string dataSourceProperty = "";
				switch (index)
				{
					case 0:
						drawCheckbox = checkBoxDraw0;
						colorCheckbox = checkBoxColorClass0;
						previewColor = labelPreview0;
						dataSourceProperty = nameof(DamageClassesSelected.Class0);
						break;
					case 1:
						drawCheckbox = checkBoxDraw1;
						colorCheckbox = checkBoxColorClass1;
						previewColor = labelPreview1;
						dataSourceProperty = nameof(DamageClassesSelected.Class1);
						break;
					case 2:
						drawCheckbox = checkBoxDraw2;
						colorCheckbox = checkBoxColorClass2;
						previewColor = labelPreview2;
						dataSourceProperty = nameof(DamageClassesSelected.Class2);
						break;
					case 3:
						drawCheckbox = checkBoxDraw3;
						colorCheckbox = checkBoxColorClass3;
						previewColor = labelPreview3;
						dataSourceProperty = nameof(DamageClassesSelected.Class3);
						break;
					case 4:
						drawCheckbox = checkBoxDraw4;
						colorCheckbox = checkBoxColorClass4;
						previewColor = labelPreview4;
						dataSourceProperty = nameof(DamageClassesSelected.Class4);
						break;
					case 5:
						drawCheckbox = checkBoxDraw5;
						colorCheckbox = checkBoxColorClass5;
						previewColor = labelPreview5;
						dataSourceProperty = nameof(DamageClassesSelected.Class5);
						break;
				}

				drawCheckbox.CheckedChanged += (t, __)=>
				{
					CheckBox cb = (CheckBox)t;
					bool draw = cb.Checked;
					colorCheckbox.Enabled = draw;
					previewColor.Text = "";
					previewColor.BackColor = groupBox.BackColor;
					if (draw)
					{
						Color backColor;
						if (colorCheckbox.Checked)
						{
							DamageClass dcc = DamageClassConverter.Convert(index);
							backColor = ColorAssociations.GetDamageClassColor(dcc).ColorValue;
						}
						else
							backColor = ColorAssociations.DamageClass0.ColorValue;
						previewColor.BackColor = backColor;
					}
					else
						previewColor.Text = "-";
				};
				colorCheckbox.CheckedChanged += (t, __) =>
				{
					CheckBox cb = (CheckBox)t;
					bool color = cb.Checked;
					previewColor.BackColor = ColorAssociations.DamageClass0.ColorValue;
					if (color)
					{
						DamageClass dcc = DamageClassConverter.Convert(index);
						previewColor.BackColor = ColorAssociations.GetDamageClassColor(dcc).ColorValue;
					}
				};

				Binding bd = new Binding(nameof(CheckBox.Checked), opts.DrawDamagesWithClasses, dataSourceProperty);
				bd.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
				drawCheckbox.DataBindings.Add(bd);

				bd = new Binding(nameof(CheckBox.Checked), opts.ColorDamagesWithClasses, dataSourceProperty);
				bd.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;				
				colorCheckbox.DataBindings.Add(bd);

			};

			createDataBinding(0);
			createDataBinding(1);
			createDataBinding(2);
			createDataBinding(3);
			createDataBinding(4);
			createDataBinding(5);
			

			string ch = nameof(CheckBox.Checked);
			checkBoxDrawDamageObject.DataBindings.Add(ch, opts, nameof(SelectedDrawingOptions.DrawObjectWithHighestDamageClass),true, DataSourceUpdateMode.OnPropertyChanged);

			List<KeyValuePair<string, DrawDamage>> damages = new List<KeyValuePair<string, DrawDamage>>();
			damages.Add(new KeyValuePair<string, DrawDamage>("All", DrawDamage.All));
			damages.Add(new KeyValuePair<string, DrawDamage>("Pipe/Shaft highest class", DrawDamage.HighestPipeClassOnly));
			damages.Add(new KeyValuePair<string, DrawDamage>("Station highest class", DrawDamage.HighestStationClassOnly));
			damages.Add(new KeyValuePair<string, DrawDamage>("Selected", DrawDamage.SelectedClasses));
			comboBoxDrawDamageTypes.ValueMember = "Value";
			comboBoxDrawDamageTypes.DisplayMember = "Key";
			comboBoxDrawDamageTypes.DataSource = damages;
			comboBoxDrawDamageTypes.SelectedIndexChanged += (_, __) =>
			{
				DrawDamage current = (DrawDamage)comboBoxDrawDamageTypes.SelectedValue;
				groupBox.Enabled = current == DrawDamage.SelectedClasses;
				DamageDrawDrawTypeChanged?.Invoke(current);
			};
			comboBoxDrawDamageTypes.SelectedIndex = 0;

			Type enumType = typeof(DamageDescriptionDisplayType);
			comboBoxDrawDamageCode.DisplayMember = "Key";
			comboBoxDrawDamageCode.ValueMember = "Value";
			comboBoxDrawDamageCode.DataSource = Enum.GetNames(enumType).
								Select(x =>
											new KeyValuePair<string, DamageDescriptionDisplayType>(x,
												(DamageDescriptionDisplayType)Enum.Parse(enumType, x))
										).ToList();
			comboBoxDrawDamageCode.SelectedIndexChanged += (_, __) =>
			{
				DamageDescriptionDisplayType t = (DamageDescriptionDisplayType)comboBoxDrawDamageCode.SelectedValue;
				opts.DamageDisplayType = t;
			};
			comboBoxDrawDamageCode.SelectedIndex = 0;


		}
		public int HideClassesSelection()
		{
			int height = 0;
			height = -groupBox.Height;
			// If already hidden, do nothing.
			if (groupBox.Visible)
				Height += height;
			else
				height = 0;
			groupBox.Visible = false;
			return height;
		}
		public int ShowClassesSelection()
		{
			int height = 0;
			height = groupBox.Height;
			if (!groupBox.Visible)
				Height += height;
			else
				height = 0;
			groupBox.Visible = true;
			return height;
		}
	}
}
