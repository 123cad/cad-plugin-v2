﻿namespace   CadPlugin.Inventories.DataDrawers.View.SelectDrawingOption.MainFormControls
{
	partial class StatusDataView
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.comboBoxDrawDamageTypes = new System.Windows.Forms.ComboBox();
			this.checkBoxDrawDamageObject = new System.Windows.Forms.CheckBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.label2 = new System.Windows.Forms.Label();
			this.comboBoxDrawDamageCode = new System.Windows.Forms.ComboBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.groupBox = new System.Windows.Forms.GroupBox();
			this.labelPreview5 = new System.Windows.Forms.Label();
			this.labelPreview4 = new System.Windows.Forms.Label();
			this.labelPreview3 = new System.Windows.Forms.Label();
			this.labelPreview2 = new System.Windows.Forms.Label();
			this.labelPreview1 = new System.Windows.Forms.Label();
			this.labelPreview0 = new System.Windows.Forms.Label();
			this.checkBoxColorClass5 = new System.Windows.Forms.CheckBox();
			this.checkBoxColorClass4 = new System.Windows.Forms.CheckBox();
			this.checkBoxColorClass3 = new System.Windows.Forms.CheckBox();
			this.checkBoxColorClass2 = new System.Windows.Forms.CheckBox();
			this.checkBoxColorClass1 = new System.Windows.Forms.CheckBox();
			this.checkBoxColorClass0 = new System.Windows.Forms.CheckBox();
			this.checkBoxDraw5 = new System.Windows.Forms.CheckBox();
			this.checkBoxDraw4 = new System.Windows.Forms.CheckBox();
			this.checkBoxDraw3 = new System.Windows.Forms.CheckBox();
			this.checkBoxDraw2 = new System.Windows.Forms.CheckBox();
			this.checkBoxDraw1 = new System.Windows.Forms.CheckBox();
			this.checkBoxDraw0 = new System.Windows.Forms.CheckBox();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.groupBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 10);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(78, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Draw damages";
			// 
			// comboBoxDrawDamageTypes
			// 
			this.comboBoxDrawDamageTypes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBoxDrawDamageTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxDrawDamageTypes.FormattingEnabled = true;
			this.comboBoxDrawDamageTypes.Location = new System.Drawing.Point(101, 7);
			this.comboBoxDrawDamageTypes.Name = "comboBoxDrawDamageTypes";
			this.comboBoxDrawDamageTypes.Size = new System.Drawing.Size(165, 21);
			this.comboBoxDrawDamageTypes.TabIndex = 1;
			// 
			// checkBoxDrawDamageObject
			// 
			this.checkBoxDrawDamageObject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.checkBoxDrawDamageObject.AutoSize = true;
			this.checkBoxDrawDamageObject.Location = new System.Drawing.Point(6, 237);
			this.checkBoxDrawDamageObject.Name = "checkBoxDrawDamageObject";
			this.checkBoxDrawDamageObject.Size = new System.Drawing.Size(228, 17);
			this.checkBoxDrawDamageObject.TabIndex = 2;
			this.checkBoxDrawDamageObject.Text = "Draw pipe/shaft with highest damage color";
			this.toolTip1.SetToolTip(this.checkBoxDrawDamageObject, "Draws a pipe or shaft and sets color to highest damage.");
			this.checkBoxDrawDamageObject.UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(3, 206);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(92, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Draw damages as";
			// 
			// comboBoxDrawDamageCode
			// 
			this.comboBoxDrawDamageCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBoxDrawDamageCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxDrawDamageCode.FormattingEnabled = true;
			this.comboBoxDrawDamageCode.Location = new System.Drawing.Point(101, 203);
			this.comboBoxDrawDamageCode.Name = "comboBoxDrawDamageCode";
			this.comboBoxDrawDamageCode.Size = new System.Drawing.Size(165, 21);
			this.comboBoxDrawDamageCode.TabIndex = 4;
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.Controls.Add(this.groupBox);
			this.panel1.Location = new System.Drawing.Point(3, 34);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(263, 161);
			this.panel1.TabIndex = 5;
			// 
			// groupBox
			// 
			this.groupBox.Controls.Add(this.labelPreview5);
			this.groupBox.Controls.Add(this.labelPreview4);
			this.groupBox.Controls.Add(this.labelPreview3);
			this.groupBox.Controls.Add(this.labelPreview2);
			this.groupBox.Controls.Add(this.labelPreview1);
			this.groupBox.Controls.Add(this.labelPreview0);
			this.groupBox.Controls.Add(this.checkBoxColorClass5);
			this.groupBox.Controls.Add(this.checkBoxColorClass4);
			this.groupBox.Controls.Add(this.checkBoxColorClass3);
			this.groupBox.Controls.Add(this.checkBoxColorClass2);
			this.groupBox.Controls.Add(this.checkBoxColorClass1);
			this.groupBox.Controls.Add(this.checkBoxColorClass0);
			this.groupBox.Controls.Add(this.checkBoxDraw5);
			this.groupBox.Controls.Add(this.checkBoxDraw4);
			this.groupBox.Controls.Add(this.checkBoxDraw3);
			this.groupBox.Controls.Add(this.checkBoxDraw2);
			this.groupBox.Controls.Add(this.checkBoxDraw1);
			this.groupBox.Controls.Add(this.checkBoxDraw0);
			this.groupBox.Controls.Add(this.label11);
			this.groupBox.Controls.Add(this.label10);
			this.groupBox.Controls.Add(this.label9);
			this.groupBox.Controls.Add(this.label8);
			this.groupBox.Controls.Add(this.label7);
			this.groupBox.Controls.Add(this.label6);
			this.groupBox.Controls.Add(this.label5);
			this.groupBox.Controls.Add(this.label4);
			this.groupBox.Controls.Add(this.label3);
			this.groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox.Location = new System.Drawing.Point(0, 0);
			this.groupBox.Name = "groupBox";
			this.groupBox.Size = new System.Drawing.Size(263, 161);
			this.groupBox.TabIndex = 0;
			this.groupBox.TabStop = false;
			this.groupBox.Text = "Selection by damage class";
			this.groupBox.Visible = false;
			// 
			// labelPreview5
			// 
			this.labelPreview5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.labelPreview5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
			this.labelPreview5.Location = new System.Drawing.Point(218, 135);
			this.labelPreview5.Name = "labelPreview5";
			this.labelPreview5.Size = new System.Drawing.Size(30, 14);
			this.labelPreview5.TabIndex = 26;
			this.labelPreview5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// labelPreview4
			// 
			this.labelPreview4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.labelPreview4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
			this.labelPreview4.Location = new System.Drawing.Point(218, 117);
			this.labelPreview4.Name = "labelPreview4";
			this.labelPreview4.Size = new System.Drawing.Size(30, 14);
			this.labelPreview4.TabIndex = 25;
			this.labelPreview4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// labelPreview3
			// 
			this.labelPreview3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.labelPreview3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
			this.labelPreview3.Location = new System.Drawing.Point(218, 99);
			this.labelPreview3.Name = "labelPreview3";
			this.labelPreview3.Size = new System.Drawing.Size(30, 14);
			this.labelPreview3.TabIndex = 24;
			this.labelPreview3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// labelPreview2
			// 
			this.labelPreview2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.labelPreview2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
			this.labelPreview2.Location = new System.Drawing.Point(218, 81);
			this.labelPreview2.Name = "labelPreview2";
			this.labelPreview2.Size = new System.Drawing.Size(30, 14);
			this.labelPreview2.TabIndex = 23;
			this.labelPreview2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// labelPreview1
			// 
			this.labelPreview1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.labelPreview1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
			this.labelPreview1.Location = new System.Drawing.Point(218, 63);
			this.labelPreview1.Name = "labelPreview1";
			this.labelPreview1.Size = new System.Drawing.Size(30, 14);
			this.labelPreview1.TabIndex = 22;
			this.labelPreview1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// labelPreview0
			// 
			this.labelPreview0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.labelPreview0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
			this.labelPreview0.Location = new System.Drawing.Point(218, 45);
			this.labelPreview0.Name = "labelPreview0";
			this.labelPreview0.Size = new System.Drawing.Size(30, 14);
			this.labelPreview0.TabIndex = 21;
			this.labelPreview0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// checkBoxColorClass5
			// 
			this.checkBoxColorClass5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.checkBoxColorClass5.AutoSize = true;
			this.checkBoxColorClass5.Location = new System.Drawing.Point(182, 135);
			this.checkBoxColorClass5.Name = "checkBoxColorClass5";
			this.checkBoxColorClass5.Size = new System.Drawing.Size(15, 14);
			this.checkBoxColorClass5.TabIndex = 20;
			this.checkBoxColorClass5.UseVisualStyleBackColor = true;
			// 
			// checkBoxColorClass4
			// 
			this.checkBoxColorClass4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.checkBoxColorClass4.AutoSize = true;
			this.checkBoxColorClass4.Location = new System.Drawing.Point(182, 117);
			this.checkBoxColorClass4.Name = "checkBoxColorClass4";
			this.checkBoxColorClass4.Size = new System.Drawing.Size(15, 14);
			this.checkBoxColorClass4.TabIndex = 19;
			this.checkBoxColorClass4.UseVisualStyleBackColor = true;
			// 
			// checkBoxColorClass3
			// 
			this.checkBoxColorClass3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.checkBoxColorClass3.AutoSize = true;
			this.checkBoxColorClass3.Location = new System.Drawing.Point(182, 99);
			this.checkBoxColorClass3.Name = "checkBoxColorClass3";
			this.checkBoxColorClass3.Size = new System.Drawing.Size(15, 14);
			this.checkBoxColorClass3.TabIndex = 18;
			this.checkBoxColorClass3.UseVisualStyleBackColor = true;
			// 
			// checkBoxColorClass2
			// 
			this.checkBoxColorClass2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.checkBoxColorClass2.AutoSize = true;
			this.checkBoxColorClass2.Location = new System.Drawing.Point(182, 81);
			this.checkBoxColorClass2.Name = "checkBoxColorClass2";
			this.checkBoxColorClass2.Size = new System.Drawing.Size(15, 14);
			this.checkBoxColorClass2.TabIndex = 17;
			this.checkBoxColorClass2.UseVisualStyleBackColor = true;
			// 
			// checkBoxColorClass1
			// 
			this.checkBoxColorClass1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.checkBoxColorClass1.AutoSize = true;
			this.checkBoxColorClass1.Location = new System.Drawing.Point(182, 63);
			this.checkBoxColorClass1.Name = "checkBoxColorClass1";
			this.checkBoxColorClass1.Size = new System.Drawing.Size(15, 14);
			this.checkBoxColorClass1.TabIndex = 16;
			this.checkBoxColorClass1.UseVisualStyleBackColor = true;
			// 
			// checkBoxColorClass0
			// 
			this.checkBoxColorClass0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.checkBoxColorClass0.AutoSize = true;
			this.checkBoxColorClass0.Location = new System.Drawing.Point(182, 45);
			this.checkBoxColorClass0.Name = "checkBoxColorClass0";
			this.checkBoxColorClass0.Size = new System.Drawing.Size(15, 14);
			this.checkBoxColorClass0.TabIndex = 15;
			this.checkBoxColorClass0.UseVisualStyleBackColor = true;
			// 
			// checkBoxDraw5
			// 
			this.checkBoxDraw5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.checkBoxDraw5.AutoSize = true;
			this.checkBoxDraw5.Location = new System.Drawing.Point(142, 135);
			this.checkBoxDraw5.Name = "checkBoxDraw5";
			this.checkBoxDraw5.Size = new System.Drawing.Size(15, 14);
			this.checkBoxDraw5.TabIndex = 14;
			this.checkBoxDraw5.UseVisualStyleBackColor = true;
			// 
			// checkBoxDraw4
			// 
			this.checkBoxDraw4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.checkBoxDraw4.AutoSize = true;
			this.checkBoxDraw4.Location = new System.Drawing.Point(142, 117);
			this.checkBoxDraw4.Name = "checkBoxDraw4";
			this.checkBoxDraw4.Size = new System.Drawing.Size(15, 14);
			this.checkBoxDraw4.TabIndex = 13;
			this.checkBoxDraw4.UseVisualStyleBackColor = true;
			// 
			// checkBoxDraw3
			// 
			this.checkBoxDraw3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.checkBoxDraw3.AutoSize = true;
			this.checkBoxDraw3.Location = new System.Drawing.Point(142, 99);
			this.checkBoxDraw3.Name = "checkBoxDraw3";
			this.checkBoxDraw3.Size = new System.Drawing.Size(15, 14);
			this.checkBoxDraw3.TabIndex = 12;
			this.checkBoxDraw3.UseVisualStyleBackColor = true;
			// 
			// checkBoxDraw2
			// 
			this.checkBoxDraw2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.checkBoxDraw2.AutoSize = true;
			this.checkBoxDraw2.Location = new System.Drawing.Point(142, 81);
			this.checkBoxDraw2.Name = "checkBoxDraw2";
			this.checkBoxDraw2.Size = new System.Drawing.Size(15, 14);
			this.checkBoxDraw2.TabIndex = 11;
			this.checkBoxDraw2.UseVisualStyleBackColor = true;
			// 
			// checkBoxDraw1
			// 
			this.checkBoxDraw1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.checkBoxDraw1.AutoSize = true;
			this.checkBoxDraw1.Location = new System.Drawing.Point(142, 63);
			this.checkBoxDraw1.Name = "checkBoxDraw1";
			this.checkBoxDraw1.Size = new System.Drawing.Size(15, 14);
			this.checkBoxDraw1.TabIndex = 10;
			this.checkBoxDraw1.UseVisualStyleBackColor = true;
			// 
			// checkBoxDraw0
			// 
			this.checkBoxDraw0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.checkBoxDraw0.AutoSize = true;
			this.checkBoxDraw0.Location = new System.Drawing.Point(142, 45);
			this.checkBoxDraw0.Name = "checkBoxDraw0";
			this.checkBoxDraw0.Size = new System.Drawing.Size(15, 14);
			this.checkBoxDraw0.TabIndex = 9;
			this.checkBoxDraw0.UseVisualStyleBackColor = true;
			// 
			// label11
			// 
			this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(212, 23);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(45, 13);
			this.label11.TabIndex = 8;
			this.label11.Text = "Preview";
			// 
			// label10
			// 
			this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(175, 23);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(31, 13);
			this.label10.TabIndex = 7;
			this.label10.Text = "Color";
			// 
			// label9
			// 
			this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(137, 23);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(32, 13);
			this.label9.TabIndex = 6;
			this.label9.Text = "Draw";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(36, 135);
			this.label8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(83, 14);
			this.label8.TabIndex = 5;
			this.label8.Text = "Class 5";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(36, 117);
			this.label7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(83, 14);
			this.label7.TabIndex = 4;
			this.label7.Text = "Class 4";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(36, 99);
			this.label6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(83, 14);
			this.label6.TabIndex = 3;
			this.label6.Text = "Class 3";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(36, 81);
			this.label5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(83, 14);
			this.label5.TabIndex = 2;
			this.label5.Text = "Class 2";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(36, 63);
			this.label4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(83, 14);
			this.label4.TabIndex = 1;
			this.label4.Text = "Class 1";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(36, 45);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(83, 14);
			this.label3.TabIndex = 0;
			this.label3.Text = "Class 0";
			// 
			// StatusDataView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.comboBoxDrawDamageCode);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.checkBoxDrawDamageObject);
			this.Controls.Add(this.comboBoxDrawDamageTypes);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.panel1);
			this.Name = "StatusDataView";
			this.Size = new System.Drawing.Size(269, 260);
			this.panel1.ResumeLayout(false);
			this.groupBox.ResumeLayout(false);
			this.groupBox.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox comboBoxDrawDamageTypes;
		private System.Windows.Forms.CheckBox checkBoxDrawDamageObject;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox comboBoxDrawDamageCode;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.GroupBox groupBox;
		private System.Windows.Forms.Label labelPreview5;
		private System.Windows.Forms.Label labelPreview4;
		private System.Windows.Forms.Label labelPreview3;
		private System.Windows.Forms.Label labelPreview2;
		private System.Windows.Forms.Label labelPreview1;
		private System.Windows.Forms.Label labelPreview0;
		private System.Windows.Forms.CheckBox checkBoxColorClass5;
		private System.Windows.Forms.CheckBox checkBoxColorClass4;
		private System.Windows.Forms.CheckBox checkBoxColorClass3;
		private System.Windows.Forms.CheckBox checkBoxColorClass2;
		private System.Windows.Forms.CheckBox checkBoxColorClass1;
		private System.Windows.Forms.CheckBox checkBoxColorClass0;
		private System.Windows.Forms.CheckBox checkBoxDraw5;
		private System.Windows.Forms.CheckBox checkBoxDraw4;
		private System.Windows.Forms.CheckBox checkBoxDraw3;
		private System.Windows.Forms.CheckBox checkBoxDraw2;
		private System.Windows.Forms.CheckBox checkBoxDraw1;
		private System.Windows.Forms.CheckBox checkBoxDraw0;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
	}
}
