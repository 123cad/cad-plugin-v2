﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace   CadPlugin.Inventories.View.SelectDrawingOptionsTypes
{
	public enum ObjectColors
	{
		/// <summary>
		/// Default color is used.
		/// </summary>
		None = 0,
		/// <summary>
		/// Color is defined by status (status 0 = is in progress; color is by entwaesserung).
		/// </summary>
		Status = 1,
		/// <summary>
		/// Color is defined by the owner.
		/// </summary>
		Owner = 2
	}
	public enum AdditionalPipeLinetype
	{
		None,
		/// <summary>
		/// Only a single line is drawn for a pipe.
		/// </summary>
		SingleLineWidthWidth, 
		/// <summary>
		/// 
		/// </summary>
		Double, 
		DoubleFilledMiddle,
		DoubleFilledAll
	}
	public enum DrawDamage
	{
		/// <summary>
		/// Draws all damages.
		/// </summary>
		All, 
		/// <summary>
		/// Only damages which have class same as highest class on the pipe.
		/// </summary>
		HighestPipeClassOnly, 
		/// <summary>
		/// From all damages on a single station, draw highest ones.
		/// </summary>
		HighestStationClassOnly,
		/// <summary>
		/// User provided which classes to draw.
		/// </summary>
		SelectedClasses
	}
	public enum DrawView
	{
		View2d,
		View3d, 
		ViewVisual3d
	}
	public enum DamageDescriptionDisplayType
    {
        Text,
        /// <summary>
        /// Images are always horizontal (x = 0).
        /// </summary>
        ImageHorizontal,
        /// <summary>
        /// Images are perpendicular to the pipe.
        /// </summary>
        ImagePerpendicular
    }
	public class DamageClassesSelected
	{
        public bool Class0 { get; set; }  = true;
		public bool Class1 { get; set; }  = true;
		public bool Class2 { get; set; }  = true;
		public bool Class3 { get; set; }  = true;
		public bool Class4 { get; set; }  = true;
		public bool Class5 { get; set; } = true;
		public bool DrawAll
		{
			get
			{
				return Class0 &&
                        Class1 &&
						Class2 &&
						Class3 &&
						Class4 &&
						Class5;
			}
		}
	public DamageClassesSelected()
		{

		}
	public DamageClassesSelected(DamageClassesSelected src)
		{
			Class0 = src.Class0;
			Class1 = src.Class1;
			Class2 = src.Class2;
			Class3 = src.Class3;
			Class4 = src.Class4;
			Class5 = src.Class5;

		}
		public void Apply(DamageClassesSelected src)
		{
			Class0 = src.Class0;
			Class1 = src.Class1;
			Class2 = src.Class2;
			Class3 = src.Class3;
			Class4 = src.Class4;
			Class5 = src.Class5;
		}
		public IEnumerable<DamageClass> GetSelectedClasses ()
		{
			List<DamageClass> list = new List<DamageClass>();
            if (Class0)
                list.Add(DamageClass.Class0);
			if (Class1)
				list.Add(DamageClass.Class1);
			if (Class2)
				list.Add(DamageClass.Class2);
			if (Class3)
				list.Add(DamageClass.Class3);
			if (Class4)
				list.Add(DamageClass.Class4);
			if (Class5)
				list.Add(DamageClass.Class5);
			return list;
		}
	}

	public enum Scale
	{
		_5000,
		_1000,
		_500,
		_250
	}


	public class SelectedDrawingOptions
	{
		public ObjectColors ColorOfObjects { get; set; } = ObjectColors.None;
		public DrawView DrawType { get; set; } = DrawView.View3d;
		public DrawDamage DrawDamageSource { get; set; } = DrawDamage.All;
		public DamageDescriptionDisplayType DamageDisplayType { get; set; } = DamageDescriptionDisplayType.Text;
		/// <summary>
		/// Color is applied to damages which have selected classes only.
		/// </summary>
		public DamageClassesSelected ColorDamagesWithClasses { get; private set; }
		/// <summary>
		/// Only damages with selected classes are drawn.
		/// </summary>
		public DamageClassesSelected DrawDamagesWithClasses { get; private set; }

		/// <summary>
		/// Indicates if pipe/shaft should be drawn with colored in color of highest damage class.
		/// </summary>
		public bool DrawObjectWithHighestDamageClass { get; set; } = true;

		public bool DrawSingleLine { get; set; } = true;

		public AdditionalPipeLinetype DrawAdditionalLines { get; set; } = AdditionalPipeLinetype.None;
		public bool UseOnlyContinuousLinetype { get; set; } = false;

		public Scale TextScale { get; set; } = Scale._250;

		/// <summary>
		/// Draw only sanierung data.
		/// </summary>
		public bool DrawOnlySanierung { get; set; } = false;
		public bool DrawSanierungConcept { get; set; } = true;
		public bool DrawSanierungProcedures { get; set; } = true;


		public SelectedDrawingOptions()
		{
			ColorDamagesWithClasses = new DamageClassesSelected();
			DrawDamagesWithClasses = new DamageClassesSelected();

		}
	}
}
