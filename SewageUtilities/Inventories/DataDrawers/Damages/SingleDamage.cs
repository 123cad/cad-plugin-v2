﻿using Isybau2015.Identifikations.Datenkollektives.Zustandsdatenkollektivs.InspizierteAbwassertechnischeAnlagens.OptischeInspektions.Rohrleitungs.Inspektionsdaten;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.Damages
{
	public abstract class SingleDamage
	{
        public abstract string Code { get; }
        public abstract string Ch1{ get; }
        public abstract string Ch2{ get; }
        public abstract string Q1 { get; }
        public abstract string Q2 { get; }
        public DamageClass DamageClass { get; set; }
		public bool IsLineDamage { get; set; }
		/// <summary>
		/// Both point and line damage have length.
		/// </summary>
		public double Length { get; set; }
        /// <summary>
        /// Not all damages are drawn. 
        /// With this we can have all of them in a collection.
        /// </summary>
        public bool IsDrawn { get; set; } = true;

        /// <summary>
        /// Position of the damage expressed as clock position [0-12].
        /// </summary>
        public int ClockPosition { get; set; } = 0;

        public SingleDamage()
        {
        }

		public string GetDisplayText()
		{
			return Code + " " + Ch1 + " " + Ch2;
		}
	}
}
