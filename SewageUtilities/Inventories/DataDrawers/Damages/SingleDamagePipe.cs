﻿using CadPlugin.Inventories.Damages;
using Isybau2015.Identifikations.Datenkollektives.Zustandsdatenkollektivs.InspizierteAbwassertechnischeAnlagens.OptischeInspektions.Rohrleitungs.Inspektionsdaten;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.DataDrawers.Damages
{
    class SingleDamagePipe : SingleDamage
    {
        public override string Ch1 => rZustand.Charakterisierung1;

        public override string Ch2 => rZustand.Charakterisierung2;

        public override string Code => rZustand.InspektionsKode;

        public override string Q1 => rZustand.Auswahlelement1NumerischFormatted;

        public override string Q2 => rZustand.Auswahlelement2NumerischFormatted;
        public  RZustand rZustand { get; private set; }
        public SingleDamagePipe(RZustand rzus)
        {
            rZustand = rzus;
        }
    }
}
