﻿using CadPlugin.Inventories.Pipes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.Damages
{
	/// <summary>
	/// Only stations inside single pipe can be compared.
	/// </summary>
	public class DamageStation:IEnumerable<SingleDamage>
	{
        public readonly SewageObject ParentObject;
		public double Distance { get; private set; }
		public PositionVector Position { get; private set; }
		private List<SingleDamage> damages { get; set; }
		public DamageStation(SewageObject obj,  double distance, PositionVector v)
		{
            ParentObject = obj;
			Position = v;
			Distance = distance;
			damages = new List<SingleDamage>();
		}
		public void AddDamage(SingleDamage sd)
		{
			damages.Add(sd);
		}
		public IEnumerator<SingleDamage> GetEnumerator()
		{
			return damages.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public DamageClass GetHighestDamageClass()
		{
			DamageClass dmg = DamageClass.ClassNotSet;
			foreach (SingleDamage d in damages)
				if (dmg < d.DamageClass)
					dmg = d.DamageClass;
			return dmg;
		}

		/// <summary>
		/// Unique inside single pipe.
		/// </summary>
		public override int GetHashCode()
		{
			// Double must not be used as hash.
			// This way we save 3 decimal places.
			return (int)(Distance * 1000);
		}
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj))
				return true;
			var o = obj as DamageStation;
			if (o == null)
				return false;
			else
				return this == o;
		}
		public static bool operator ==(DamageStation s1, DamageStation s2)
		{
			return Math.Abs(s1.Distance - s2.Distance) < 0.0001;
		}
		public static bool operator !=(DamageStation s1, DamageStation s2)
		{
			return !(s1 == s2);
		}
	}
}
