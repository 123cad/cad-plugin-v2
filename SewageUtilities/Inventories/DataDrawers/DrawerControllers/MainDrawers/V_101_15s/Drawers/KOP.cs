﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries.Geometriedatens;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Knotens;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Knotens.Schachts;
using CadPlugin.Common;
using CadPlugin.Inventories.Drawers;

using Inventory.CAD;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Inventories.DrawerControllers.MainDrawers.V_101_15s.Drawers
{
	/// <summary>
	/// Shaft cover.
	/// </summary>
	class KOP : ICustomDrawer, ICustomDrawerPunkt
	{

		public BlockReference Draw(TransactionData data, SewageObject obj, Punkt point, Point3d position, BlockAssociations blocks, DrawerManager drawer)
		{
			AbwassertechnischeAnlagen ab = obj.Abwasser;
			//Point3d p3 = new Point3d(p.X, p.Y, p.Z);
			Dictionary<string, string> atts = new Dictionary<string, string>();
			atts.Add("BEZEICHNUNG", ab.Objektbezeichnung);
			atts.Add("TYPE", GetType().Name);
			atts.Add("ART", ab.Entwaesserungsart);
			atts.Add("HOEHE", DoubleHelper.ToStringInvariant(position.Z, 3));
            var owner = ab?.GetOwner();
            if (owner != null)
                atts.Add("BETREIBER", owner);
            //BlockReference br = Common.CADBlockHelper.CreateBlockReference(data.Tr, data.Bt, data.ModelSpace, blockName, p3, atts);
            BlockReference br = MainDrawer.CreateBlockReference(data, typeof(V_101_15), GetType(), blocks, ab, point, position, atts);
			double defaultScale = 0.625;
			Schacht s = ShaftSewageObjectHelper.GetSchacht(ab);
			if (s != null)
			{
				if (s.Abdeckung != null)
				{
					Abdeckung d = s.Abdeckung;
					if (d.LaengeDeckelIsSet)
						defaultScale = d.LaengeDeckel;
				}
			}
			Common.CADBlockHelper.ScaleBlockReference(data.Tr, br, defaultScale);
			br.MakeAttributesInvisible(data.Tr );

			return br;
		}
	}
}
