﻿Every MainDrawer represents one entry in table V101.

Idea is to get MainDrawer from MainDrawerManager by parameter GeoobjektArt (V101) and then from IDrawerFactory to get associated IDrawer (one of values from V106).
In this way we can easily control which GeoobjektArt supports which PunktattributAbwasser.

Every MainDrawer can have Drawers - these are all supported PunktattributAbwasser (if it is empty, all are supported)

MainDrawer is responsible for determining what will be drawn, and which objects will be sent for specialized drawing (for example, shaft point is drawn as a block, which is done in class SMP/DMP) .