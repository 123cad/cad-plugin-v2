﻿using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries.Geometriedatens;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Knotens;
using CadPlugin.Common;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Knotens.Schachts;
using CadPlugin.Inventories.Drawers;

using Inventory.CAD;


#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif


namespace CadPlugin.Inventories.DrawerControllers.MainDrawers.V_101_25s.Drawers
{
	class SBW:ICustomDrawer, ICustomDrawerPunkt
	{
		public BlockReference Draw(TransactionData data, SewageObject obj, Punkt point, Point3d position, BlockAssociations blocks, DrawerManager drawer)
		{
			AbwassertechnischeAnlagen ab = obj.Abwasser;
			//string blockName = blocks.GetBlockPath(typeof(V_101_1), GetType()).Name;
			//Point3d p3 = new Point3d(p.X, p.Y, p.Z);
			Dictionary<string, string> atts = new Dictionary<string, string> ();
			atts.Add("BEZEICHNUNG", ab.Objektbezeichnung);

			atts.Add("TYPE", GetType().Name);
			atts.Add("HOEHE", DoubleHelper.ToStringInvariant(position.Z, 3));

			//BlockReference br = MainDrawer.CreateBlockReference(data, typeof(V_101_3), GetType(), blocks, ab, point, p, atts);
			//MainDrawer.ApplyAttributeColor(data.Tr, br.AttributeCollection, "BEZEICHNUNG", ab);
			//Common.CADBlockHelper.ScaleBlockReference(data.Tr, br, diameter);

			string blockName = blocks.GetBlockPath(typeof(V_101_25), GetType()).Name;

			BlockReference br = MainDrawer.CreateBlockReference(data, typeof(V_101_25), GetType(), blocks, ab, point, position, atts);

			return br;

		}
	}
}
