﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries.Geometriedatens;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries;
using static Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries.Geometriedaten;
using CadPlugin.Common;
using Inventory.CAD;
using CadPlugin.Inventories.Drawers;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Inventories.DrawerControllers
{ 
	interface ICustomDrawer
	{
	}
	

	/// <summary>
	/// Specific drawer (depending on PunktattributAbwasser) for Punkt.
	/// </summary>
	interface ICustomDrawerPunkt
	{
		/// <summary>
		/// position represents actual position of Punkt (Punkt xyz is ignored). Allows overriding punkt position.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="ab"></param>
		/// <param name="point"></param>
		/// <param name="position"></param>
		/// <param name="blocks"></param>
		
		BlockReference Draw(TransactionData data, SewageObject obj, Punkt point, Point3d position, BlockAssociations blocks, DrawerManager drawer);
	}

}
