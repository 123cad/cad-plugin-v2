﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
using Teigha.GraphicsInterface;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.GraphicsInterface;
#endif

namespace CadPlugin.Inventories.DataDrawers.CadImages
{
	class ImageBorderManager
	{
		public class ImageBorderOverrule:DrawableOverrule
		{
			public override void ViewportDraw(Drawable drawable, ViewportDraw vd)
			{
				base.ViewportDraw(drawable, vd);
			}
		}
		public static ImageBorderOverrule BorderOverrule { get; private set; }

		public static void InitializeOverrule()
		{
			if (BorderOverrule != null)
				return;
			var doc = Application.DocumentManager.MdiActiveDocument;
			BorderOverrule = new ImageBorderOverrule();
			Overrule.AddOverrule(RXObject.GetClass(typeof(RasterImage)), BorderOverrule, false);
			Overrule.Overruling = true;
		}
	}
}
