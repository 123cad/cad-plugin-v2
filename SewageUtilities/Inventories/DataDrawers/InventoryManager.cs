﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using geometry = Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using Inventory.CAD;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries.Geometriedatens;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries;
using System.Diagnostics;
using static Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries.Geometriedaten;
using Isybau2015.Identifikations.Datenkollektives.Zustandsdatenkollektivs.InspizierteAbwassertechnischeAnlagens.OptischeInspektions;
using Isybau2015.Identifikations.Datenkollektives.Zustandsdatenkollektivs.InspizierteAbwassertechnischeAnlagens.OptischeInspektions.Rohrleitungs.Inspektionsdaten;
using CadPlugin.Common;
using CadPlugin.Inventories.Drawers;
using CadPlugin.Inventories.Drawers.PipeDrawers;
using CadPlugin.Inventories.Drawers.Colors;
using CadPlugin.Inventories.DrawerControllers;
using CadPlugin.Inventories.Pipes;
using CadPlugin.Inventories.Shafts;
using CadPlugin.Inventories.Drawers.ShaftDrawers;
using CadPlugin.Inventories.Drawers.DamageDescriptionAlignments;
using CadPlugin.Inventories.DataDrawers.Drawers.Linetypes;
using CadPlugin.Inventories.DataDrawers.Drawers;
using CadPlugin.Inventories.DataDrawers.Drawers.DamageDrawerFilters;
using CadPlugin.Inventories.DataDrawers.Drawers.DamageTextDrawers;
using CadPlugin.Inventories.DataDrawers.Drawers.Layers;
using System.Runtime.CompilerServices;
using System.IO;
using System.Collections;
using CadPlugin.Inventories.DataDrawers;
using CadPlugin.Inventories.DataDrawers.Drawers.PipeDrawers.Sanierung;
using BlocksAttributes;


#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace   CadPlugin.Inventories
{
	public class InventoryManager
	{
		/// <summary>
		/// Holds created entities.
		/// </summary>
		public class CreatedCADEntities
		{
			/// <summary>
			/// Entities which represent pipe (no arrow, no damages...). Used only for geometric extents calculation.
			/// </summary>
			public List<Entity> PipeGeometricExtentsEntities { get; private set; } = new List<Entity>();
			/// <summary>
			/// Entities which represent shaft (no damages...). Used only for geometric extents calculation.
			/// </summary>
			public List<Entity> ShaftGeometricExtentsEntities { get; private set; } = new List<Entity>();
			public IEnumerable<Entity> GetAllGeometricExtentsEntities()
				=> PipeGeometricExtentsEntities.Concat(ShaftGeometricExtentsEntities);
			/// <summary>
			/// Blocks for which Attribute labels have to be created.
			/// </summary>
			public List<BlockReference> AttributeBlocks { get; private set; } = new List<BlockReference>();
			private List<Entity> allCreatedEntities { get; set; } = new List<Entity>();
			/// <summary>
			/// Absolutely all created entities.
			/// </summary>
			public IEnumerable<Entity> AllCreatedEntities => allCreatedEntities;

			/// <summary>
			/// DEBUGGING: entity handle - source code line where it was created.
			/// </summary>
			public readonly static Dictionary<long, string> EntitySources = new Dictionary<long, string>();
			/// <summary>
			/// We don't need full path in EntitySource. So project path can be removed from full file name.
			/// This index is used to remove this path.
			/// </summary>
			public int startPathIndex = 0;

			public CreatedCADEntities()
			{
				EntitySources.Clear();
			}
			private void AddEntitySource(Entity e, string fileName, int lineNumber)
			{
#if DEBUG
				if (startPathIndex == 0)
				{
					string projectDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName;
					startPathIndex = projectDirectory.Length;
				}
				long handle = e.ObjectId.Handle.Value;
				if (!EntitySources.ContainsKey(handle))
					EntitySources.Add(e.ObjectId.Handle.Value, $"{fileName.Substring(startPathIndex)} line:{lineNumber}");

#endif
			}
			/// <summary>
			/// Add block which will get Attribute label.
			/// </summary>
			/// <param name="br"></param>
			public void AddAttributeBlock(BlockReference br, [CallerFilePath] string callerFile = "", [CallerLineNumber] int callerLine = 0)
			{
				AttributeBlocks.Add(br);

				AddEntitySource(br, callerFile, callerLine);
			}

			/// <summary>
			/// Adds entity to pipe entities (it is not mandatory to add new entity
			/// to this instance, since it is used only to calculate geometric extents).
			/// </summary>
			/// <param name="e"></param>
			/// <param name="affectsView">Indicates if entity affects the view, so it will participate in view extents</param>
			public void AddPipeEntity(Entity e, bool affectsView = false, [CallerFilePath] string callerFile = "", [CallerLineNumber] int callerLine = 0)
			{
				if (affectsView)
					PipeGeometricExtentsEntities.Add(e);
				allCreatedEntities.Add(e);
				
				AddEntitySource(e, callerFile, callerLine);
			}

			/// <summary>
			/// Adds entity to shaft entities (it is not mandatory to add new entity
			/// to this instance, since it is used only to calculate geometric extents).
			/// </summary>
			/// <param name="affectsView">Indicates if entity affects the view, so it will participate in view extents</param>
			public void AddShaftEntity(Entity e, bool affectsView = false, [CallerFilePath] string callerFile = "", [CallerLineNumber] int callerLine = 0)
			{
				if (affectsView)
					ShaftGeometricExtentsEntities.Add(e);
				allCreatedEntities.Add(e);

				AddEntitySource(e, callerFile, callerLine);
			}

			/// <summary>
			/// Clears all collections.
			/// </summary>
			public void ClearAll()
			{
				AttributeBlocks.Clear();
				PipeGeometricExtentsEntities.Clear();
				ShaftGeometricExtentsEntities.Clear();
			}
		}

		private static SewageObjectsCollection objectCollection = new SewageObjectsCollection();

		/// <summary>
		/// Collection of all objects loaded from inventary.
		/// </summary>
		public static IEnumerable<SewageObject> AllObjects;

		public static void DrawData(Document doc, Inventory.CAD.DisplayData data)
		{
			MyLog.MyTraceSource.Information("Start drawing inventary data (objects count: " + data.Objects.Count() + ")");
			Editor ed = doc.Editor;
			Database db = doc.Database;

			View.SelectDrawingOptionsTypes.SelectedDrawingOptions opt = View.SelectDrawingOptionsTypes.MainForm.Show(data.DrawGeometry, data.DrawDamages, data.DrawOrder);
			if (opt == null)
			{
				MyLog.MyTraceSource.Information("Canceled inventary draw!");
				doc.Editor.WriteMessage("Abbruch!\n");
				return;
			}

			DrawDataWithSelectedOptions(doc, data, ed, db, opt);

			MyLog.MyTraceSource.Information("End drawing inventory data");
			//doc.SendStringToExecute("ZOOM ", true, false, false);
			//doc.SendStringToExecute("_e ", true, false, false); 
		}
		[Conditional("TEST")]
		public static void DrawDataWithSelectedOptionsTest(Document doc, DisplayData data, Editor ed, Database db, View.SelectDrawingOptionsTypes.SelectedDrawingOptions opt)
		{
			DrawDataWithSelectedOptions(doc, data, ed, db, opt);
		}
		/// <summary>
		/// Draws data with options provided.
		/// </summary>
		private static void DrawDataWithSelectedOptions(Document doc, DisplayData data, Editor ed, Database db, View.SelectDrawingOptionsTypes.SelectedDrawingOptions opt)
		{
			CreatedCADEntities createdEntities = new CreatedCADEntities();
			List<ObjectId> attributesIds = new List<ObjectId>();
			using (TransactionWrapper tr = doc.StartTransaction())
			{

				PipeDrawer pipeDrawer;
				ShaftDrawer shaftDrawer;
				StationDamagesDrawer damagesDescriptionDrawer;
				CreateData(opt, tr, out pipeDrawer, out shaftDrawer, out damagesDescriptionDrawer, createdEntities);
				DrawerManager drawerMgr = new DrawerManager(pipeDrawer, shaftDrawer);
				LayerManager.Instance.Initialize(db);

				BlockAssociations blocks = new BlockAssociations();
				if (opt.DrawType == View.SelectDrawingOptionsTypes.DrawView.ViewVisual3d)
				{
					blocks.Initialize3d(db);
				}
				else
				{
					blocks.Initialize2d(db);
				}

				//DamageTextDrawer.DefaultTextHeight = DamageTextDrawer.DefaultTextHeight * (int)opt.Scale / 1000.0;

				try
				{
					objectCollection.Clear();
					CadProgressBar.StartLongOperation(data.Objects.Count());
					var createObjects = data.Objects.Select(x =>
					{
						SewageObject so = SewageObject.CreateByType(objectCollection, x);
						return new { SewageO = so, SinleO = x };

					}).Where(x => x.SewageO != null).ToList();
					AllObjects = createObjects.Select(x => x.SewageO).ToList();
					if (opt.DrawOnlySanierung)
					{
						var dataSource = createObjects.Select(x =>
						{
							ObjectMassnahmes om = data.OrderStammdaten.GetMassnahmesForOrder(x.SinleO);
							return new Tuple<SewageObject, ObjectMassnahmes>(x.SewageO, om);
						}).ToList();
						
						((ISanierungDataSource)pipeDrawer).SetObjectMassnahmes(dataSource);
						((ISanierungDataSource)shaftDrawer).SetObjectMassnahmes(dataSource);
					}
					//foreach (SingleObject obj in data.Objects)
					foreach (var o in AllObjects)
					{
						//SewageObject sObj = SewageObject.CreateByType(obj);
						//if (sObj == null)
							//continue;
						AbwassertechnischeAnlagen ab = o.Abwasser;
						if (ab.Geometrie == null || ab.Geometrie.GeometrieDaten == null)
							continue;
						Geometrie gm = ab.Geometrie;
						geometry.Geometriedaten gmd = gm.GeometrieDaten;
						MainDrawer m = MainDrawerManager.GetFactory(gm.GeoObjektart);
						if (m == null)
						{
							MyLog.MyTraceSource.Warning("Maindrawer for GeoobjektArt: " + gm.GeoObjektart + " is not found!");
							continue;
						}
						var cMgr = o is Pipe ? drawerMgr.Pipe.ColorManager : drawerMgr.Shaft.ColorManager;
						cMgr.Initialize(o);
						pipeDrawer.PipeLinetypeManager.SetSewageObject(o);



						if (data.DrawGeometry || data.DrawOrder)
						{
							m.Draw(new Common.TransactionData(tr), o, blocks, drawerMgr);
						}
						if (data.DrawDamages || data.DrawOrder)
						{
							if (o is Pipe)
								pipeDrawer.DrawDamages((Pipe)o);
							if (o is Shaft)
								shaftDrawer.DrawDamages((Shaft)o);

						}
						CadProgressBar.PerformStep();
					}
				}
				finally
				{
					CadProgressBar.EndLongOperation();
				}

				attributesIds = createdEntities.AttributeBlocks.Select(x => x.ObjectId).ToList();

				//NOTE Needed are all created entities for zooming. However, this means
				// that we need all to track all created entities. 
				// For now there are attributeBlock which are good enough.
				var viewEntities = createdEntities.GetAllGeometricExtentsEntities();
				//if (createdEntities.AttributeBlocks.Count > 0)
				CADZoomHelper.ZoomToEntities(ed, viewEntities, 15);

				if (!pipeDrawer.IsDrawn3d)// opt.DrawType == View.SelectDrawingOptionsTypes.DrawView.View2d)
					EntitiesHeight0.SetZ(createdEntities.AllCreatedEntities, tr);

				tr.Commit();
			}
			// Create labels for all BlockReferences.
			_123CADAttributes.BlockAttributesFacade.CreateLabel(doc, db, attributesIds, _123CADAttributes._123AttributesProperties.ALL);
			ed.Regen();
			// Remove unused layers.
			LayerManager.Instance.Clean(db);
		}

		private static void CreateData(View.SelectDrawingOptionsTypes.SelectedDrawingOptions opt, TransactionWrapper tw,
								out PipeDrawer pipe,
								out ShaftDrawer shaft,
								out StationDamagesDrawer damagesDrawer,
								CreatedCADEntities createdEntities)
		{
			Common.TransactionData tr = new Common.TransactionData(tw);
			SewageObjectColor pipeObjectColor = null;
			SewageObjectColor shaftObjectColor = null;
			DamageColor pipeDamageColor = null;
			DamageColor shaftDamageColor = null;
			PipeLinetype pipeLinetype = null;
			if (opt.DrawOnlySanierung)
			{
				pipeObjectColor = new PipeColorSanierungConcept();
				shaftObjectColor = new ShaftColorSanierungConcept();
				//if (opt.DrawSanierungConcept)
				//	pipeLinetype = new DataDrawers.Drawers.Linetypes.PipeLinetypes.SanierungConceptLinetype();
				//else
					pipeLinetype = new DataDrawers.Drawers.Linetypes.PipeLinetypes.SanierungProcedureLinetype();
			}
			else
			{
				switch (opt.ColorOfObjects)
				{
					case View.SelectDrawingOptionsTypes.ObjectColors.None:
						pipeObjectColor = new DefaultColor();
						shaftObjectColor = new DefaultColor();
						pipeLinetype = new DataDrawers.Drawers.Linetypes.PipeLinetypes.ContinuousLinetype();
						break;
					case View.SelectDrawingOptionsTypes.ObjectColors.Status:
						pipeObjectColor = new PipeColorStatus();
						shaftObjectColor = new ShaftColorStatus();
						pipeLinetype = new DataDrawers.Drawers.Linetypes.PipeLinetypes.StatusLinetype();
						break;
					case View.SelectDrawingOptionsTypes.ObjectColors.Owner:
						pipeObjectColor = new PipeColorOwner();
						shaftObjectColor = new ShaftColorOwner();
						pipeLinetype = new DataDrawers.Drawers.Linetypes.PipeLinetypes.OwnerLinetype();
						break;
				}
			}
			// Make sure to draw anything with continuous linetype, if user has chosen.
			if (opt.UseOnlyContinuousLinetype)
				pipeLinetype = new DataDrawers.Drawers.Linetypes.PipeLinetypes.ContinuousLinetype();

			//TODO Add sanierung procedure colors.
			if (!opt.ColorDamagesWithClasses.DrawAll)
			{
				pipeDamageColor = new PipeDamageColorSelectedClass(opt.ColorDamagesWithClasses.GetSelectedClasses());
				shaftDamageColor = new ShaftDamageColorSelectedClass(opt.ColorDamagesWithClasses.GetSelectedClasses());
			}
			else
			{
				pipeDamageColor = new DamageColor();
				shaftDamageColor = new DamageColor();
			}

			//TODO Use sanierung
			switch (opt.DamageDisplayType)
			{
				case View.SelectDrawingOptionsTypes.DamageDescriptionDisplayType.Text:
					damagesDrawer = new DataDrawers.Drawers.DamageTextAlignments.StationDamageDrawerManagerTypes.DamageTextDrawer();
					break;
				default:
					CADImageManager imgMgr = new CADImageManager(tw, tw.Db);

					DataDrawers.CadImageDrawerWrapper wrapper = new DataDrawers.CadImageDrawerWrapper(imgMgr);
					var alignment = DataDrawers.Drawers.DamageTextAlignments.StationDamageDrawerManagerTypes.ImageAlignment.Horizontal;
					if (opt.DamageDisplayType == View.SelectDrawingOptionsTypes.DamageDescriptionDisplayType.ImagePerpendicular)
						alignment = DataDrawers.Drawers.DamageTextAlignments.StationDamageDrawerManagerTypes.ImageAlignment.PerpendicularToPipe;
					damagesDrawer = new DataDrawers.Drawers.DamageTextAlignments.StationDamageDrawerManagerTypes.DamageImageDrawer(wrapper, alignment);
					break;
			}
			DamageTextDrawer pipeDamageTextDrawer = new PipeTextDrawer(tr, damagesDrawer);
			DamageTextDrawer shaftDamageTextDrawer = new ShaftTextDrawer(tr, damagesDrawer);
			DamageDrawerFilter damageFilter = null;
			switch (opt.DrawDamageSource)
			{
				case View.SelectDrawingOptionsTypes.DrawDamage.All:
					damageFilter = new AllDamages();
					break;
				case View.SelectDrawingOptionsTypes.DrawDamage.HighestPipeClassOnly:
					damageFilter = new ObjectHighestDamage();
					break;
				case View.SelectDrawingOptionsTypes.DrawDamage.HighestStationClassOnly:
					damageFilter = new StationHighestDamage();
					break;
				case View.SelectDrawingOptionsTypes.DrawDamage.SelectedClasses:
					damageFilter = new SelectedDamages(opt.DrawDamagesWithClasses.GetSelectedClasses());
					break;
			}
			SewageObjectColorManager pipeColorManager = new SewageObjectColorManager(pipeObjectColor, pipeDamageColor);
			SewageObjectColorManager shaftColorManager = new SewageObjectColorManager(shaftObjectColor, shaftDamageColor);

			bool draw3d = false;
			bool drawVisual3d = false;
			switch (opt.DrawType)
			{
				case View.SelectDrawingOptionsTypes.DrawView.View2d:
					break;
				case View.SelectDrawingOptionsTypes.DrawView.View3d:
					draw3d = true;
					break;
				case View.SelectDrawingOptionsTypes.DrawView.ViewVisual3d:
					draw3d = true;
					drawVisual3d = true;
					break;
			}
			TextSizeDefinitions textDef = new TextSizeDefinitions(opt.TextScale);
			if (drawVisual3d)
			{
				if (opt.DrawOnlySanierung)
				{
					pipe = new SanierungPipeVisual3dDrawer(tr, pipeColorManager, pipeLinetype, pipeDamageTextDrawer, textDef, damageFilter, createdEntities, opt.DrawSanierungConcept, opt.DrawSanierungProcedures);
					shaft = new SanierungShaftVisual3dDrawer(tr, pipeColorManager, shaftDamageTextDrawer, textDef, damageFilter, createdEntities, opt.DrawSanierungConcept, opt.DrawSanierungProcedures);
				}
				else
				{
					pipe = new PipeVisual3dDrawer(tr, pipeColorManager, pipeLinetype, pipeDamageTextDrawer, textDef, damageFilter, createdEntities);
					shaft = new ShaftVisual3dDrawer(tr, pipeColorManager, shaftDamageTextDrawer, textDef, damageFilter, createdEntities);
				}
			}
			else
			{
				// If single line not drawn and additional line not drawn, pipe becomes Null drawer.
				// If single line is drawn, but additional line is not drawn, pipe becomes PipeSingleLineDrawer 
				// (single drawer in the drawer becomes PipeSingleLineDrawerNull - but is not important because PipeSingleLineDrawer does not call it).
				// Otherwise, user additional drawn line, with 
				PipeSingleLineDrawer singleLineDrawer;
				if (opt.DrawSingleLine)
				{
					if (opt.DrawOnlySanierung)
						singleLineDrawer = new SanierungPipeSingleLineDrawer(tr, pipeColorManager, pipeLinetype, pipeDamageTextDrawer, textDef, damageFilter, createdEntities, draw3d, opt.DrawSanierungConcept, opt.DrawSanierungProcedures);
					else
						singleLineDrawer = new PipeSingleLineDrawer(tr, pipeColorManager, pipeLinetype, pipeDamageTextDrawer, textDef, damageFilter, createdEntities, draw3d);
				}
				else
				{
					if (opt.DrawOnlySanierung)
						singleLineDrawer = new SanierungPipeSingleLineDrawerNull(tr, pipeColorManager, pipeLinetype, pipeDamageTextDrawer, textDef, damageFilter, createdEntities);
					else
						singleLineDrawer = new PipeSingleLineDrawerNull(tr, pipeColorManager, pipeLinetype, pipeDamageTextDrawer, textDef, damageFilter, createdEntities);

				}

				var pipeNull = new PipeSingleLineDrawerNull(tr, pipeColorManager, pipeLinetype, pipeDamageTextDrawer, textDef, damageFilter, createdEntities);

				switch (opt.DrawAdditionalLines)
				{
					case View.SelectDrawingOptionsTypes.AdditionalPipeLinetype.None:
						// Pipe becomes whatever user selected (if not drawn single line and no additional lines, then 
						// it is Null drawer).
						pipe = singleLineDrawer;
						singleLineDrawer = pipeNull;
						break;
					case View.SelectDrawingOptionsTypes.AdditionalPipeLinetype.SingleLineWidthWidth:
						if (opt.DrawOnlySanierung)
							pipe = new SanierungPipeSingleLineWidthDrawer(tr, pipeColorManager, pipeLinetype, pipeDamageTextDrawer, textDef, damageFilter, createdEntities, draw3d, opt.DrawSanierungConcept, opt.DrawSanierungProcedures);
						else
							pipe = new PipeSingleLineWidthDrawer(tr, pipeColorManager, pipeLinetype, pipeDamageTextDrawer, textDef, damageFilter, createdEntities, draw3d);
						break;
					case View.SelectDrawingOptionsTypes.AdditionalPipeLinetype.Double:
						if (opt.DrawOnlySanierung)
							pipe = new SanierungPipeDoubleLineDrawer(tr, pipeColorManager, pipeLinetype, pipeDamageTextDrawer, textDef, damageFilter, createdEntities, draw3d, opt.DrawSanierungConcept, opt.DrawSanierungProcedures);
						else
							pipe = new PipeDoubleLineDrawer(tr, pipeColorManager, pipeLinetype, pipeDamageTextDrawer, textDef, damageFilter, createdEntities, draw3d);
						break;
					case View.SelectDrawingOptionsTypes.AdditionalPipeLinetype.DoubleFilledMiddle:
						if (opt.DrawOnlySanierung)
							pipe = new SanierungPipeDoubleLineFillDrawer(tr, pipeColorManager, pipeLinetype, pipeDamageTextDrawer, textDef, damageFilter, createdEntities, draw3d, opt.DrawSanierungConcept, opt.DrawSanierungProcedures);
						else 
							pipe = new PipeDoubleLineFillDrawer(tr, pipeColorManager, pipeLinetype, pipeDamageTextDrawer, textDef, damageFilter, createdEntities, draw3d);
						break;
					case View.SelectDrawingOptionsTypes.AdditionalPipeLinetype.DoubleFilledAll:
						if (opt.DrawOnlySanierung)
							pipe = new SanierungPipeDoubleLineAllDrawer(tr, pipeColorManager, pipeLinetype, pipeDamageTextDrawer, textDef, damageFilter, createdEntities, draw3d, opt.DrawSanierungConcept, opt.DrawSanierungProcedures);
						else
							pipe = new PipeDoubleLineAllDrawer(tr, pipeColorManager, pipeLinetype, pipeDamageTextDrawer, textDef, damageFilter, createdEntities, draw3d);
						break;
					default:
						throw new NotSupportedException("Pipe drawer not assigned!");
				}
				pipe.SetSingleLineDrawer(singleLineDrawer);
				if (opt.DrawOnlySanierung)
				{
					shaft = new SanierungShaft3dDrawer(tr, shaftColorManager, shaftDamageTextDrawer, textDef, damageFilter, createdEntities, draw3d, opt.DrawSanierungConcept, opt.DrawSanierungProcedures);
					shaft.DrawDamageShaft = opt.DrawObjectWithHighestDamageClass;

				}
				else
				{
					shaft = new Shaft3dDrawer(tr, shaftColorManager, shaftDamageTextDrawer, textDef, damageFilter, createdEntities, draw3d);
					shaft.DrawDamageShaft = opt.DrawObjectWithHighestDamageClass;
				}
			}
			pipe.DrawDamagePipe = opt.DrawObjectWithHighestDamageClass;


		}
	}
}
