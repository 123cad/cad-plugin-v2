﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories
{
	/// <summary>
	/// Isybau damage classes.
	/// </summary>
	public enum DamageClass
	{
		/// <summary>
		/// Damage not set.
		/// </summary>
		ClassNotSet = -1,
		/// <summary>
		/// Inspected but no damages found.
		/// </summary>
		Class0 = 0,
		Class1 = 1,
		Class2 = 2, 
		Class3 = 3,
		Class4 = 4,
		Class5 = 5
	}

	public class DamageClassConverter
	{
		public static DamageClass Convert(int i)
		{
			DamageClass dg = DamageClass.ClassNotSet;
			switch (i)
			{
				case 0: dg = DamageClass.Class0; break;
				case 1: dg = DamageClass.Class1; break;
				case 2: dg = DamageClass.Class2; break;
				case 3: dg = DamageClass.Class3; break;
				case 4: dg = DamageClass.Class4; break;
				case 5: dg = DamageClass.Class5; break;
			}
			return dg;
		}
        public static string ToString(DamageClass dmg)
        {
            string s = "";
            switch (dmg)
            {
                case DamageClass.Class0:
                    s = "0";
                    break;
                case DamageClass.Class1:
                    s = "1";
                    break;
                case DamageClass.Class2:
                    s = "2";
                    break;
                case DamageClass.Class3:
                    s = "3";
                    break;
                case DamageClass.Class4:
                    s = "4";
                    break;
                case DamageClass.Class5:
                    s = "5";
                    break;
            }
            return s;
        }
	}
}
