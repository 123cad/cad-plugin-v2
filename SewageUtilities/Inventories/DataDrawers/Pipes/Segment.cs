﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.Pipes
{
	public abstract class Segment
	{
		public Point3d StartPoint { get; private set; }
		public Point3d EndPoint { get; private set; }
		/// <summary>
		/// Length of this Segment.
		/// </summary>
		public double Length { get; protected set; }
		/// <summary>
		/// Station can consist of multiple segments. Every segment represents
		/// interval of the pipe. First Segment has StartDistance = 0, 
		/// second.StartDistance = first.Length...
		/// </summary>
		public double StartDistance { get; private set; }
		public double EndDistance { get; protected set; }

		public Segment(Point3d start, Point3d end, double startDistance)
		{
			StartPoint = start;
			EndPoint = end;
			StartDistance = startDistance;
		}
		/// <summary>
		/// Tells if distance belongs to this Segment.
		/// </summary>
		/// <param name="distance"></param>
		/// <returns></returns>
		public bool DoesBelongToSegment(double distance)
		{
			return StartDistance <= distance && distance <= EndDistance;				
		}
		/// <summary>
		/// Indicates if any part of provided interval belongs to this Segment.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <returns></returns>
		public bool DoesBelongToSegment(double start, double end)
		{
			return DoesBelongToSegment(start) || DoesBelongToSegment(end);
		}
		public abstract PositionVector? GetPositionAtDistance(double distance);

		/// <summary>
		/// Returns points for this segment.
		/// If interval is bigger than segment, then start and end points are returned (with intermediate points, if there are any).
		/// </summary>
		/// <returns></returns>
		public abstract IEnumerable<Point3d> GetPoints(double startDistance, double endDistance);

		public abstract IEnumerable<Point3d> GetAllPoints();

	}
}
