﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtilities.Geometry;

namespace CadPlugin.Inventories.Pipes.Segments
{
	public class LineSegment : Segment
	{
		private Vector3d direction { get; set; }
		public LineSegment(Point3d start, Point3d end, double startDistance):base(start, end, startDistance)
		{
			Vector3d v = start.GetVectorTo(end);
			Length = v.Length;
			EndDistance = StartDistance + Length;
			direction = v.GetUnitVector();
		}
		public override IEnumerable<Point3d> GetAllPoints()
		{
			var v = new List<Point3d>();
			v.Add(StartPoint);
			v.Add(EndPoint);
			return v;
		}

		public override IEnumerable<Point3d> GetPoints(double startDistance, double endDistance)
		{
			List<Point3d> pts = new List<Point3d>();
			if (startDistance > EndDistance || endDistance < StartDistance)
				return pts;
			Point3d start, end;
			if (startDistance < StartDistance)
				start = StartPoint;
			else
			{
				// Here come only if we know point exists.
				start = GetPositionAtDistance(startDistance).Value.Position;
			}
			if (endDistance > EndDistance)
				end = EndPoint;
			else
			{
				// Here come only if we know point exists.
				end = GetPositionAtDistance(endDistance).Value.Position;
			}
			pts.Add(start);
			pts.Add(end);
			return pts;
		}

		public override PositionVector? GetPositionAtDistance(double distance)
		{
			if (distance < StartDistance || distance > EndDistance)
				return null;
			double d = distance - StartDistance;
			PositionVector pp = new PositionVector();
			pp.Direction = direction;
			pp.Position = StartPoint.Add(direction.Multiply(d));
			return pp;
		}
	}
}
