﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs;
using Isybau2015.Identifikations.Datenkollektives.Zustandsdatenkollektivs;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries.Geometriedatens;
using System.Diagnostics;
using Isybau2015.Identifikations.Datenkollektives.Zustandsdatenkollektivs.InspizierteAbwassertechnischeAnlagens.OptischeInspektions;
using Isybau2015.Identifikations.Datenkollektives.Zustandsdatenkollektivs.InspizierteAbwassertechnischeAnlagens.OptischeInspektions.Knotens.Inspektionsdaten;
using CadPlugin.Inventories.Damages;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Objektarts.Knotens;
using CadPlugin.Inventories.DataDrawers.Damages;
using CadPlugin.Inventories.DataDrawers;
using SewageUtilities.Helpers;


namespace CadPlugin.Inventories.Shafts
{
	public class Shaft : SewageObject
	{
		public static double DefaultRadius = 1;
		/// <summary>
		/// Position of the shaft. Z represents bottom shaft height, and 
		/// it is counted as station = 0m.
		/// Represented as SMP point.
		/// </summary>
		public Point3d Center { get; private set; }
		/// <summary>
		/// Represents position of cover point. Z is top of shaft.
		/// Represented as DMP point.
		/// </summary>
		public Point3d CoverPoint { get; private set; }
		public bool IsCoverSet { get; private set; }
		public double Depth { get; private set; }
		public DamageClass MaxAutoDamageClass { get; private set; }
		private DamagesCollection damages { get; set; }
		private List<Damages.DamageStation> damageStations { get; set; }
		public double Radius { get; private set; }
		/// <summary>
		/// If complete, it has all points required to draw the shaft.
		/// And it can be drawn in 3d.
		/// </summary>
		public bool IsShaftComplete { get; private set; } = false;

		#region Manual class
		private DamageClass __ManualDamageClass = 0;
		public DamageClass ManualDamageClass
		{
			get
			{
				if (!IsManualDamageClassSet)
					throw new FieldAccessException("ManualDamageClass is not set!");
				return __ManualDamageClass;
			}
			private set
			{
				__ManualDamageClass = value;
				IsManualDamageClassSet = true;
			}

		}
		public bool IsManualDamageClassSet { get; private set; }
		#endregion
		#region Damages iterator
		/// <summary>
		/// This is used so we can have iterator over collection.
		/// Without this GetEnumerator has to be defined, which seems to be confusing
		/// because Damages are not intuitive collection of Pipe.
		/// </summary>
		public class DamagesCollection
		{
			private Shaft p;
			public DamagesCollection(Shaft p)
			{
				this.p = p;
			}
			public IEnumerator<Damages.DamageStation> GetEnumerator()
			{
				return p.damageStations.GetEnumerator();
			}
		}
		#endregion
		public Shaft(SewageObjectsCollection owner, AbwassertechnischeAnlagen abwasser, InspizierteAbwassertechnischeAnlagen insp, InspizierteAbwassertechnischeAnlagen inspUp) : base(owner, abwasser, insp, inspUp)
		{
			damages = new DamagesCollection(this);
			damageStations = new List<Damages.DamageStation>();
			Initialize();
		}
		private void Initialize()
		{
			Center = new Point3d();
			bool centerFound = false;
			Punkt p = SewageObjectHelpers.GetPunktByAttribut(Abwasser, "SMP");
			if (p != null)
			{
				centerFound = true;
				Center = p.GetPoint3d();
				Punkt dmp = SewageObjectHelpers.GetPunktByAttribut(Abwasser, "DMP");
				if (dmp == null)
				{
					IsShaftComplete = false;
				}
				else
				{
					if (!dmp.RechtswertIsSet)
						dmp.Rechtswert = p.Rechtswert;
					if (!dmp.HochwertIsSet)
						dmp.Hochwert = p.Hochwert;
					CoverPoint = new Point3d(dmp.Rechtswert, dmp.Hochwert, dmp.Punkthoehe);
					IsCoverSet = true;
				}
			}
			if (!centerFound)
			{
				IsShaftComplete = false;
				Debug.WriteLine("Shaft: " + Abwasser.Objektbezeichnung + " does not have a center!");
				MyLog.MyTraceSource.Information("Shaft: " + Abwasser.Objektbezeichnung + " does not have a center!");
			}
			Depth = 0;
			double d = 0;
			if (ShaftSewageObjectHelper.GetSchachtDepth(Abwasser, ref d))
				Depth = d;
			else
				IsShaftComplete = false;
			Schacht sc = ShaftSewageObjectHelper.GetSchacht(Abwasser);
			ShaftSewageObjectHelper.SMPData data = null;
			if (sc != null)
				data = ShaftSewageObjectHelper.Get_SMP_Data(sc);
			// Set default data.
			if (data == null)
				data = new ShaftSewageObjectHelper.SMPData();
			Radius = data.Laenge / 2;
			// Initialize damages
			InitializeDamages();
			MaxAutoDamageClass = GetHighestDamageClass();
		}
		private static bool LineDamageWarningDisplayed = false;
		private void InitializeDamages()
		{
			if (Inspizierte != null &&
				Inspizierte.OptischeInspektion != null)
			{
				Knoten k = Inspizierte.OptischeInspektion.Auswahlelement as Knoten;
				if (k != null)
				{
					MaxAutoDamageClass = 0;
					Dictionary<int, List<SingleDamage>> damagesStations = new Dictionary<int, List<SingleDamage>>();
					// Find all damages.
					foreach (KZustand kzus in k.KZustands)
					{
						if (kzus.InspektionsKode == "DCA")
							continue;
						int distance = (int)(kzus.VertikaleLage * 1000);
						if (!damagesStations.ContainsKey(distance))
							damagesStations.Add(distance, new List<SingleDamage>());
						List<SingleDamage> damages = damagesStations[distance];

						if (k.Bewertung != null && k.Bewertung.KlasseManuellIsSet)
							this.ManualDamageClass = DamageClassConverter.Convert(k.Bewertung.KlasseManuell);

						DamageClass maxClass = kzus.Klassifizierung != null ? DamageClassConverter.Convert(kzus.Klassifizierung.MaxSKeAuto) : DamageClass.ClassNotSet;
						SingleDamage sd = new SingleDamageShaft(kzus);
						sd.DamageClass = maxClass;

						//Debug.Assert(LineDamageWarningDisplayed, "Line damage for shaft not supported yet!");
						if (!LineDamageWarningDisplayed)
							MyLog.MyTraceSource.Warning("Line damage for shaft not yet supported!");
						LineDamageWarningDisplayed = true;
						sd.Length = 0.3;

						damages.Add(sd);
					}
					foreach (KeyValuePair<int, List<SingleDamage>> station in damagesStations)
					{
						double distance = station.Key / 1000.0;
						PositionVector pv = new PositionVector() { Position = Center, Direction = new Vector3d(0, 0, 1) };
						/*if (pv == null)
						{
							MyLog.MyTraceSource.Warning("Position on shaft not found for station: " + distance.ToString("0.000"));
							continue;
						}*/
						DamageStation ds = new DamageStation(this, distance, pv);
						foreach (SingleDamage sd in station.Value)
							ds.AddDamage(sd);
						this.damageStations.Add(ds);
					}
					//this.damageStations = damagesStations.Select(x=> new damagesta;

				}
			}
		}
		public IEnumerable<Point3d> GetPoints(double lowestHeight, double highestHeight)
		{
			// Check parameters.
			List<Point3d> pts = new List<Point3d>();
			pts.Add(Center);
			pts.Add(Center.Add(new Vector3d(0, 0, Depth)));
			return pts;
		}
		public IEnumerable<Point3d> GetAllPoints()
		{
			return GetPoints(0, Depth);
		}
		/// <summary>
		/// Station is calculated from SMP point (as shaft bottom) to up (along Z axis).
		/// d represents distance from bottom (station 0).
		/// </summary>
		/// <param name="d"></param>
		/// <returns></returns>
		public PositionVector? GetPositionAtStation(double d)
		{
			if (d > Depth)
				MyLog.MyTraceSource.Warning($"Shaft station is bigger than depth (drawn above shaft). Shaft: {this.Abwasser.Objektbezeichnung} Station:{d} Depth:{Depth}");
				//return null;
			PositionVector pv = new PositionVector();
			pv.Position = Center.Add(new Vector3d(0,0,d));
			pv.Direction = new Vector3d(0, 0, 1);
			return pv;
		}
		public DamagesCollection GetDamagesCollection()
		{
			return damages;
		}
		public DamageClass GetHighestDamageClass()
		{
			DamageClass dmg = DamageClass.ClassNotSet;
			foreach (Damages.DamageStation st in damageStations)
			{
				DamageClass dc = st.GetHighestDamageClass();
				if (dmg < dc)
					dmg = dc;
			}
			return dmg;
		}

	}
}
