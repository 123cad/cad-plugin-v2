﻿using CadPlugin.Inventories.GeometryAssignment.Data;
using Inventory.CAD.GeometryAssignment.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.GeometryAssignment.View
{
    /// <summary>
    /// Manages pipes and shafts - when shaft is update, it updates pipe also.
    /// </summary>
    class MainFormDataManager
    {

        /// <summary>
        /// Dictionary with keys represented by shaft name. This way, shafts are associated with pipes.
        /// If shaft doesn't exist, ShaftAdapter is null.
        /// </summary>
        Dictionary<string, Tuple<ShaftAdapter, List<PipeAdapter>>> shaftsWithPipes;
        /// <summary>
        /// Pipe and Shaft control are monitored for assigning new geometry for pipe/shaft.
        /// But, user can change directly cad entity on the drawing, so CadManager is used, since it 
        /// monitors these changes.
        /// </summary>
        public MainFormDataManager(PipeControl pc, ShaftControl sc, DataManagerEventArgs args, CadManager cadMgr)
        {
            shaftsWithPipes = new Dictionary<string, Tuple<ShaftAdapter, List<PipeAdapter>>>();
            // For performance reasons, keep already tested and failed shaft names.
            foreach(var pipe in args.Pipes)
            {
                string startShaftName = pipe.FromShaft;
                string endShaftName = pipe.ToShaft;
                List<PipeAdapter> startShaftPipes = null;
                List<PipeAdapter> endShaftPipes = null;
                if (!string.IsNullOrEmpty(startShaftName))
                {
                    if (!shaftsWithPipes.ContainsKey(startShaftName))
                    {
                        Shaft sh = args.Shafts.FirstOrDefault(x => x.Name == startShaftName);
                        ShaftAdapter sha = null;
                        if (sh != null)
                            sha = new ShaftAdapter(sh);
                        shaftsWithPipes.Add(startShaftName, new Tuple<ShaftAdapter, List<PipeAdapter>>(sha, new List<PipeAdapter>()));
                    }
                    startShaftPipes = shaftsWithPipes[startShaftName].Item2;
                }
                if (!shaftsWithPipes.ContainsKey(endShaftName))
                {
                    if (!shaftsWithPipes.ContainsKey(endShaftName))
                    {
                        Shaft sh = args.Shafts.FirstOrDefault(x => x.Name == endShaftName);
                        ShaftAdapter sha = null;
                        if (sh != null)
                            sha = new ShaftAdapter(sh);
                        shaftsWithPipes.Add(endShaftName, new Tuple<ShaftAdapter, List<PipeAdapter>>(sha, new List<PipeAdapter>()));
                    }
                    endShaftPipes = shaftsWithPipes[endShaftName].Item2;
                }
                PipeAdapter pd = new PipeAdapter(pipe);
                if (startShaftPipes != null)
                    startShaftPipes.Add(pd);
                if (endShaftPipes != null)
                    endShaftPipes.Add(pd);

            }
            sc.ShaftGeometryAssigned += Sc_ShaftGeometryAssigned;
            cadMgr.PipeUpdated += PipeGeometryUpdated;
            cadMgr.ShaftUpdated += ShaftGeometryUpdated;
        }
        /// <summary>
        /// Only compares references.
        /// </summary>
        private class ReferenceDistinctOnly<T> : IEqualityComparer<T> where T:PipeAdapter
        {
            public bool Equals(T x, T y) => ReferenceEquals(x, y);

            public int GetHashCode(T obj)
            {

                return obj.Name.GetHashCode();
            }
        }
        /// <summary>
        /// Invoke when geometry for shaft is changed, so pipes need to be updated.
        /// </summary>
        /// <param name="updatedShaft"></param>
        public void ShaftGeometryUpdated(ShaftAdapter updatedShaft)
        {
            Sc_ShaftGeometryAssigned(updatedShaft);
        }
        /// <summary>
        /// Invoke when geometry for pipe is changed.
        /// </summary>
        /// <param name="updatedShaft"></param>
        public void PipeGeometryUpdated(PipeAdapter updatedPipe)
        {
            // Nothing, no one depends on pipe.
        }
        private void Sc_ShaftGeometryAssigned(Data.ShaftAdapter updatedShaft)
        {
            string shaftName = updatedShaft.Name;
            if (!shaftsWithPipes.ContainsKey(shaftName))
            {
                return;
            }
            List<PipeAdapter> associatedPipes = shaftsWithPipes[shaftName].Item2;
            // We need to update pipe only 1 time - and it can be in multiple shafts.
            IEnumerable<PipeAdapter> pds = associatedPipes.Distinct(new ReferenceDistinctOnly<PipeAdapter>());
            var center = updatedShaft.CenterBottom;
            foreach(PipeAdapter pd in pds)
            {
                // If object is assigned, pipe doesn't depend on shaft center anymore.
                if (pd.IsCadObjectAssigned )//|| !pd.GeometryExists)
                    continue;
                if (pd.IsGeometryAssigned)
                {
                    // If geometry has already been assigned, we can update it.
                    pd.UpdateShaftPoint(updatedShaft.Name, center);
                    continue;
                }
                // If geometry is not assigned, and 
                ShaftAdapter startShaft = shaftsWithPipes[pd.FromShaft].Item1;
                ShaftAdapter endShaft = shaftsWithPipes[pd.ToShaft].Item1;

                if (startShaft == null || endShaft == null)
                    continue;

                if (!(startShaft.IsGeometryAssigned && endShaft.IsGeometryAssigned))
                    continue;

                pd.AssignGeometry(new List<MyUtilities.Geometry.Point3d>()
                {
                    startShaft.CenterBottom,
                    endShaft.CenterBottom
                });
            }
        }
        
    }
}
