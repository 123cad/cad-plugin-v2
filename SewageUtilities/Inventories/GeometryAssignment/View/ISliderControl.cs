﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.GeometryAssignment.View
{
    public interface ISliderControl
    {
         event Action<ZoomValue> ZoomChanged;
         ZoomValue Current { get; set; }

    }
    public class ZoomValue
    {
        /// <summary>
        /// [0-10] where 0 is smallest, 10 is highest zoom.
        /// </summary>
        public int Value;
        public ZoomValue(int value)
        {
            Value = value;
        }
    }

}
