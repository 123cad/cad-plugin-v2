﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CadPlugin.Inventories.GeometryAssignment.CadInterface;

namespace CadPlugin.Inventories.GeometryAssignment.View
{
    public partial class SliderControl : UserControl, ISliderControl

    {
        public SliderControl()
        {
            InitializeComponent();
            trackBar.Maximum = ZoomManager.MaxZoomLevel;
            trackBar.Value = ZoomManager.InitialZoomLevel;
            trackBar.ValueChanged += (_, e) =>
            {

                ZoomChanged?.Invoke(Current);

            };
            /*MouseWheel += (_, e) =>
            {
                bool updated = false;

                if (e.Delta > 0)
                {
                    if (trackBar.Value + trackBar.SmallChange <= trackBar.Maximum)
                    {
                        trackBar.Value += trackBar.SmallChange;
                        updated = true;
                    }
                }
                else
                {
                    if (trackBar.Value - trackBar.SmallChange >= trackBar.Minimum)
                    {
                        trackBar.Value -= trackBar.SmallChange;
                        updated = true;
                    }
                }
                if (updated)
                    ZoomChanged?.Invoke(Current);
            };*/
        }

        public ZoomValue Current
        {
            get
            {
                return new ZoomValue(trackBar.Value);
            }
            set
            {
                trackBar.Value = value.Value;
            }
        }

        public event Action<ZoomValue> ZoomChanged;

        private void trackBar_MouseEnter(object sender, EventArgs e)
        {
            trackBar.Focus();
        }

        private void trackBar_MouseLeave(object sender, EventArgs e)
        {
            ParentForm.Focus();
        }
    }
}
