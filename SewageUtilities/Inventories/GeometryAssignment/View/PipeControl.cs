﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using CadPlugin.Inventories.GeometryAssignment.Data;
using Inventory.CAD.GeometryAssignment.Data;
using System.Reflection;

namespace CadPlugin.Inventories.GeometryAssignment.View
{
    public partial class PipeControl : UserControl
    {
        private readonly MainForm form;
        private BindingSource pipeSource;// = new BindingSource();
        private BindingList<PipeAdapter> pipeList;// = new BindingList<PipeAdapter>();

        internal event Action<IAdapter> SelectionChanged;

        /// <summary>
        /// Currently selected pipe.
        /// </summary>
        internal PipeAdapter SelectedPipe { get; private set; }

        /// <summary>
        /// Pipe is not selected any more.
        /// </summary>
        public event Action CancelPipeSelection;

        private CadManager cadMgr { get; set; }

        /// <summary>
        /// Raised when user assigns new geometry for a pipe.
        /// </summary>
        internal event Action<PipeAdapter> PipeGeometryAssigned;
        


        internal PipeControl(MainForm toolbar, CadManager mgr)
        {
            form = toolbar;
            cadMgr = mgr;
            form.GlobalKeyUp += Form_GlobalKeyUp;
            InitializeComponent();
            dgvPipes.AutoGenerateColumns = false;
            //dgvPipes.DataSource = new BindingSource(pipeList, null);

            typeof(DataGridView).InvokeMember(
               "DoubleBuffered",
               BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty,
               null,
               dgvPipes,
               new object[] { true });
            InitializeColumns();

        }

        private void InitializeEvents()
        {
            dgvPipes.DataError += (_, e) =>
            {
                errorProviderDgvCell.Clear();

                // This is enough to 
                if (e.Context.HasFlag(DataGridViewDataErrorContexts.LeaveControl))
                {
                    dgvPipes.RefreshEdit();
                    dgvPipes.EndEdit();
                    e.Cancel = false;
                    return;
                }

                var t = dgvPipes.EditingControl as TextBox;
                if (t != null)
                {
                    errorProviderDgvCell.SetError(t, "Invalid value!");
                    // Move the icon so it is visible inside TextBox.
                    errorProviderDgvCell.SetIconPadding(t, -20);
                }
            };
            // To support DataError event - if user clicks inside dgv, no LeaveControl flag
            // is set, so we handle that here.
            dgvPipes.MouseDown += (_, e) =>
            {
                if (!dgvPipes.IsCurrentCellInEditMode)
                    return;
                var info = dgvPipes.HitTest(e.X, e.Y);
                if (info.ColumnIndex != dgvPipes.CurrentCell.ColumnIndex ||
                    info.RowIndex != dgvPipes.CurrentCell.RowIndex)
                {
                    dgvPipes.CommitEdit(DataGridViewDataErrorContexts.Commit | DataGridViewDataErrorContexts.LeaveControl);

                }
                if (info.Type == DataGridViewHitTestType.None)
                {
                    // If no data error occurs, cell will still be in edit mode.
                    if (dgvPipes.IsCurrentCellInEditMode)
                        dgvPipes.EndEdit();
                    if (info.Type == DataGridViewHitTestType.None)
                        dgvPipes.CurrentCell = null;
                }
            };

            PipeAdapter currentAdapter = null;
            dgvPipes.CellMouseClick += (_, e) =>
            {
                if (e.ColumnIndex == -1)
                    return;
                if (e.RowIndex == -1)
                {
                    var column = dgvPipes.Columns[e.ColumnIndex];
                    string sort = $"{column.DataPropertyName} ASC";
                    if (sort == pipeSource.Sort)
                    {
                        sort = $"{column.DataPropertyName} DESC";
                    }

                    pipeSource.Sort = sort;

                    /*ListSortDirection dir = ListSortDirection.Ascending;
                    if (dgvPipes.SortedColumn != null && e.ColumnIndex == dgvPipes.SortedColumn.Index)
                    {
                        if (dgvPipes.SortOrder == SortOrder.Ascending)
                            dir = ListSortDirection.Descending;
                    }
                    dgvPipes.Sort(dgvPipes.Columns[e.ColumnIndex], dir);*/
                    //pipeSource.DataSource = pipeList.OrderBy(x=>x.)
                }
            };
            dgvPipes.CellMouseClick += (_, e)=>
            {
                if (e.Button == MouseButtons.Right && e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    if (dgvPipes.CurrentCell != dgvPipes[e.ColumnIndex, e.RowIndex] ||
                        !dgvPipes.IsCurrentCellInEditMode)
                    {
                        dgvPipes.CurrentCell = dgvPipes[e.ColumnIndex, e.RowIndex];
                        currentAdapter = dgvPipes.CurrentRow.DataBoundItem as PipeAdapter;
                        var rec = dgvPipes.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                        contextMenuStripPipeData.Show(dgvPipes, new Point(e.Location.X + rec.X, e.Location.Y + rec.Y));
                    }
                }
            };
            toolStripMenuItemPipeData.Click += (_, e) =>
            {
                string name = dgvPipes[ColumnPipeName.Index, dgvPipes.CurrentCell.RowIndex].Value as string;
                var d = DataFetch.DataViews.PipeDataParserController.Start(cadMgr, name);
                if (d != null)
                {
                    if (d.IsDiameterSet)
                        currentAdapter.Diameter = d.Diameter;
                    if (d.IsMaterialSet)
                        currentAdapter.Material = d.Material;
                    if (d.IsSlopeSet)
                        currentAdapter.SlopePermille = d.Slope;
                }
                currentAdapter = null;
            };
            dgvPipes.CellEndEdit += (_, e) =>
            {
                errorProviderDgvCell.Clear();
            };



            dgvPipes.LostFocus += (_, __) =>
            {
                if (!form.IsSelectionMode)
                    CancelPipeSelection?.Invoke();
            };
            

            dgvPipes.CellMouseDoubleClick += (_, e) =>
            {
                // Enter edit mode on click.
                if (e.Button == MouseButtons.Left)
                {
                    if (!dgvPipes.IsCurrentCellInEditMode)
                        dgvPipes.BeginEdit(false);
                }
            };
            dgvPipes.CellContentClick += (_, e) =>
            {
                if (e.ColumnIndex == -1 || e.RowIndex == -1)
                    return;
                if (ColumnPipeEntity.Index == e.ColumnIndex)
                {
                    PipeAdapter sa = dgvPipes.Rows[e.RowIndex].DataBoundItem as PipeAdapter;
                    // Select new object for a pipe.
                    SelectNewEntity(sa);
                    dgvPipes.CurrentCell = dgvPipes[ColumnPipeName.Name, e.RowIndex];
                }
                else if (ColumnPipeDirection.Index == e.ColumnIndex)
                {
                    // Reverse pipe direction.
                    PipeAdapter pa = dgvPipes.Rows[e.RowIndex].DataBoundItem as PipeAdapter;
                    if (!pa.IsCadObjectAssigned)
                        return;
                    cadMgr.ReversePipePoints(pa);
                }
            };
            dgvPipes.RowsAdded += (_, e) =>
            {
                int columnIndex = ColumnPipeEntity.Index;
                for (int i = e.RowIndex; i < e.RowIndex + e.RowCount; i++)
                {
                    dgvPipes[columnIndex, i].Value = "...";
                    dgvPipes[columnIndex, i].ToolTipText = "Select new entity for shaft";
                };
            };
            dgvPipes.CurrentCellChanged += DgvPipes_CurrentCellChanged;

            // Direction column is DataBinded, so it is notified about changing.
            // But, we don't need to see text on the button. 
            // So in this event text for a cell is 
            dgvPipes.CellFormatting += (_, e) =>
            {
                if (e.RowIndex < 0 || e.ColumnIndex != ColumnPipeDirection.Index)
                    return;
                e.FormattingApplied = true;
                e.Value = "";
            };

            dgvPipes.CellPainting += (_, e) =>
            {
                // Button column does not have possibility of disabling the button.
                // To keep it simple, we just don't draw the button.

                if (e.RowIndex == -1)
                    return;
                if (e.ColumnIndex == ColumnPipeDirection.Index)
                {
                    PipeAdapter pa = dgvPipes.Rows[e.RowIndex].DataBoundItem as PipeAdapter;
                    if (!pa.IsCadObjectAssigned)
                    {
                        e.PaintBackground(e.CellBounds, false);
                        using (Pen br = new Pen(Color.LightGray))
                        {
                            Rectangle res = new Rectangle(e.CellBounds.X, e.CellBounds.Y, e.CellBounds.Width, e.CellBounds.Height - 1);
                            // e.Graphics.FillRectangle(br, res);
                            //e.Graphics.FillRectangle(br, e.CellBounds);
                            // e.Graphics.DrawRectangle(br, res);
                        }
                    }
                    else
                    {

                        e.PaintBackground(e.CellBounds, false);
                        e.PaintContent(e.CellBounds);
                        Rectangle res = new Rectangle(e.CellBounds.X + e.CellBounds.Width / 2 - 8,
                                                        e.CellBounds.Y + e.CellBounds.Height / 2 - 8,
                                                        16,
                                                        16);
                        e.Graphics.DrawImage(Properties.ResourceInventory.arrow_sync, res);
                    }
                    e.Handled = true;
                }
                else if (e.ColumnIndex == ColumnPipeEntity.Index)
                {
                    e.PaintBackground(e.CellBounds, false);
                    e.PaintContent(e.CellBounds);

                    Rectangle res = new Rectangle(e.CellBounds.X + e.CellBounds.Width / 2 - 8,
                                                    e.CellBounds.Y + e.CellBounds.Height / 2 - 8,
                                                    16,
                                                    16);
                    e.Graphics.DrawImage(Properties.ResourceInventory.select, res);
                    e.Handled = true;
                }
            };
            dgvPipes.CellFormatting += (_, e) =>
            {
                /*if (e.RowIndex == -1)
                    return;
                if (e.ColumnIndex != ColumnPipeDirection.Index)
                    return;
                PipeAdapter pa = dgvPipes.Rows[e.RowIndex].DataBoundItem as PipeAdapter;
                bool enabled = pa.IsCadObjectAssigned;
                DataGridViewButtonColumn column= dgvPipes.Columns[nameof(ColumnPipeDirection)] as DataGridViewButtonColumn;
                */


            };
        }

        internal void Initialize(IEnumerable<Pipe> pipes)
        {
            if (pipes != null)
                pipeList = new SortableBindingList<PipeAdapter>(pipes.Select(x => new PipeAdapter(x)).ToList());
            else
                pipeList = new SortableBindingList<PipeAdapter>();
            pipeSource = new BindingSource(pipeList, null);
            dgvPipes.DataSource = pipeSource;
            InitializeEvents();
        }
        internal List<PipeAdapter> GetAllPipes()
        {
            return pipeList.ToList();
        }
        internal void CreateNewPipe(ShaftAdapter start, ShaftAdapter end)
        {
            //pipeList.Add(new PipeAdapter(start, end));
            var row = dgvPipes.Rows[dgvPipes.RowCount - 1];
            row.Cells[ColumnPipeEntity.Index].Value = "Select";
        }

        private void DgvPipes_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dgvPipes.CurrentCell == null)
            {
                SelectedPipe = null;
                CancelPipeSelection?.Invoke();
            }
            else
            {
                PipeAdapter pd = dgvPipes.CurrentRow.DataBoundItem as PipeAdapter;
                SelectedPipe = pd;
                // This caused flickering on Show form.
                //formVisibility.TempHide();
                //cadMgr.SetObjectSelected(pd);
                //formVisibility.TempShow();
                SelectionChanged?.Invoke(pd);
            }
        }

        private void Form_GlobalKeyUp(Keys obj)
        {
            switch (obj)
            {
                case Keys.Escape:
                    if (dgvPipes.IsCurrentCellInEditMode)
                    {
                        dgvPipes.CancelEdit();
                        dgvPipes.EndEdit();
                    }
                    break;
                case Keys.Enter:
                    if (dgvPipes.IsCurrentCellInEditMode)
                    {                        
                        dgvPipes.EndEdit();
                    }
                    break;
            }
        }
        /// <summary>
        /// Assigns new pipe entity to selected row.
        /// </summary>
        /// <param name="pa"></param>
        private void SelectNewEntity(PipeAdapter pa)
        {
            //form.Visible = false;
            if (cadMgr.AssignPipeEntity(pa))
            {
                PipeGeometryAssigned?.Invoke(pa);
            }
            //form.Visible = true;
        }
        private void InitializeColumns()
        {
            DataGridView pipes = dgvPipes;

            DataGridViewButtonColumn columnButton;
            DataGridViewTextBoxColumn columnText;
            DataGridViewComboBoxColumn columnCombo;
            DataGridViewCheckBoxColumn columnCheck;

            columnButton = pipes.Columns[nameof(ColumnPipeDirection)] as DataGridViewButtonColumn;
            Debug.Assert(columnButton != null);
            columnButton.DataPropertyName = nameof(Data.PipeAdapter.IsCadObjectAssigned);
            

            columnCheck = pipes.Columns[nameof(ColumnPipeGeometryExists)] as DataGridViewCheckBoxColumn;
            Debug.Assert(columnCheck != null);
            columnCheck.DataPropertyName = nameof(Data.PipeAdapter.IsybauGeometryExists);

            columnCheck = pipes.Columns[nameof(ColumnPipeGeometryAssigned)] as DataGridViewCheckBoxColumn;
            Debug.Assert(columnCheck != null);
            columnCheck.DataPropertyName = nameof(Data.PipeAdapter.IsGeometryAssigned);

            columnText = pipes.Columns[nameof(ColumnPipeName)] as DataGridViewTextBoxColumn;
            Debug.Assert(columnText != null);
            columnText.DataPropertyName = nameof(Data.PipeAdapter.Name);// "Name";

            columnCombo = pipes.Columns[nameof(ColumnPipeObjektArt)] as DataGridViewComboBoxColumn;
            Debug.Assert(columnCombo != null);
            columnCombo.DataSource = Isybau2015.ReferenceTables.Common.Objektart.Get().Values;
            columnCombo.DisplayMember = nameof(Isybau2015.ReferenceTables.ReferencePair.Value);// "Value";
            columnCombo.ValueMember = nameof(Isybau2015.ReferenceTables.ReferencePair.Key); //"Key";
            columnCombo.DataPropertyName = nameof(Data.PipeAdapter.ObjektArt);// "ObjektArt";
            columnCombo.ReadOnly = true;


            columnCombo = pipes.Columns[nameof(ColumnPipeGeoobjektArt)] as DataGridViewComboBoxColumn;
            Debug.Assert(columnCombo != null);
            // Pipe objects can have only specific GeoObjektArt
            HashSet<string> geoObjektArts = new HashSet<string>();
            geoObjektArts.Add("4");
            geoObjektArts.Add("5");
            geoObjektArts.Add("6");
            geoObjektArts.Add("7");
            columnCombo.DataSource = Isybau2015.ReferenceTables.Common.GeoobjektArt.Get().Values.Where(x => geoObjektArts.Contains(x.Key)).ToList();
            columnCombo.DisplayMember = nameof(Isybau2015.ReferenceTables.ReferencePair.Value);// "Value";
            columnCombo.ValueMember = nameof(Isybau2015.ReferenceTables.ReferencePair.Key); //"Key";
            columnCombo.DataPropertyName = nameof(Data.PipeAdapter.GeoObjektArt);// "GeoobjektArt";

            columnText = pipes.Columns[nameof(ColumnPipeSlope)] as DataGridViewTextBoxColumn;
            Debug.Assert(columnText != null);
            columnText.DataPropertyName = nameof(Data.PipeAdapter.SlopePermille);// "Slope";
            columnText.HeaderText = "Slope (‰)";
            columnText.InheritedStyle.Format = "N3";

            columnText = pipes.Columns[nameof(ColumnPipeMaterial)] as DataGridViewTextBoxColumn;
            Debug.Assert(columnText != null);
            columnText.DataPropertyName = nameof(Data.PipeAdapter.Material);// "Material";


            columnText = pipes.Columns[nameof(ColumnPipeDiameter)] as DataGridViewTextBoxColumn;
            Debug.Assert(columnText != null);
            columnText.DataPropertyName = nameof(Data.PipeAdapter.Diameter);// "Diameter";
            columnText.HeaderText = "Diameter (mm)";
            columnText.InheritedStyle.Format = "N0";


        }

        private void buttonRemoveKante_Click(object sender, EventArgs e)
        {
            if (dgvPipes.CurrentCell != null)
            {
                PipeAdapter pa = dgvPipes.CurrentRow.DataBoundItem as PipeAdapter;
                pipeList.Remove(pa);
            }
        }

        private void dgvPipes_MouseEnter(object sender, EventArgs e)
        {
            dgvPipes.Focus();
        }

        private void dgvPipes_MouseLeave(object sender, EventArgs e)
        {
            ParentForm.Focus();
        }
    }
}
