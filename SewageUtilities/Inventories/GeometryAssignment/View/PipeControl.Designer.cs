﻿namespace CadPlugin.Inventories.GeometryAssignment.View
{
    partial class PipeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvPipes = new System.Windows.Forms.DataGridView();
            this.ColumnPipeEntity = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ColumnPipeGeometryExists = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColumnPipeGeometryAssigned = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColumnPipeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPipeObjektArt = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColumnPipeGeoobjektArt = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColumnPipeDirection = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ColumnPipeSlope = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPipeMaterial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPipeDiameter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPipeEmpty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errorProviderDgvCell = new System.Windows.Forms.ErrorProvider(this.components);
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStripPipeData = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemPipeData = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPipes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderDgvCell)).BeginInit();
            this.contextMenuStripPipeData.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvPipes
            // 
            this.dgvPipes.AllowUserToAddRows = false;
            this.dgvPipes.AllowUserToDeleteRows = false;
            this.dgvPipes.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPipes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPipes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPipes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnPipeEntity,
            this.ColumnPipeGeometryExists,
            this.ColumnPipeGeometryAssigned,
            this.ColumnPipeName,
            this.ColumnPipeObjektArt,
            this.ColumnPipeGeoobjektArt,
            this.ColumnPipeDirection,
            this.ColumnPipeSlope,
            this.ColumnPipeMaterial,
            this.ColumnPipeDiameter,
            this.ColumnPipeEmpty});
            this.dgvPipes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPipes.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPipes.Location = new System.Drawing.Point(0, 0);
            this.dgvPipes.MultiSelect = false;
            this.dgvPipes.Name = "dgvPipes";
            this.dgvPipes.RowHeadersVisible = false;
            this.dgvPipes.Size = new System.Drawing.Size(793, 234);
            this.dgvPipes.TabIndex = 3;
            this.dgvPipes.MouseEnter += new System.EventHandler(this.dgvPipes_MouseEnter);
            this.dgvPipes.MouseLeave += new System.EventHandler(this.dgvPipes_MouseLeave);
            // 
            // ColumnPipeEntity
            // 
            this.ColumnPipeEntity.HeaderText = "Entity";
            this.ColumnPipeEntity.Name = "ColumnPipeEntity";
            this.ColumnPipeEntity.Text = "Update...";
            this.ColumnPipeEntity.ToolTipText = "Update only if pipe is not straight from start shaft to end shaft.";
            this.ColumnPipeEntity.Width = 55;
            // 
            // ColumnPipeGeometryExists
            // 
            this.ColumnPipeGeometryExists.HeaderText = "Geometry";
            this.ColumnPipeGeometryExists.Name = "ColumnPipeGeometryExists";
            this.ColumnPipeGeometryExists.ReadOnly = true;
            this.ColumnPipeGeometryExists.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnPipeGeometryExists.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColumnPipeGeometryExists.Width = 60;
            // 
            // ColumnPipeGeometryAssigned
            // 
            this.ColumnPipeGeometryAssigned.HeaderText = "Assigned";
            this.ColumnPipeGeometryAssigned.Name = "ColumnPipeGeometryAssigned";
            this.ColumnPipeGeometryAssigned.ReadOnly = true;
            this.ColumnPipeGeometryAssigned.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnPipeGeometryAssigned.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColumnPipeGeometryAssigned.Width = 60;
            // 
            // ColumnPipeName
            // 
            this.ColumnPipeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnPipeName.HeaderText = "Bezeichnung";
            this.ColumnPipeName.MinimumWidth = 90;
            this.ColumnPipeName.Name = "ColumnPipeName";
            // 
            // ColumnPipeObjektArt
            // 
            this.ColumnPipeObjektArt.HeaderText = "ObjektArt";
            this.ColumnPipeObjektArt.Name = "ColumnPipeObjektArt";
            this.ColumnPipeObjektArt.ReadOnly = true;
            this.ColumnPipeObjektArt.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColumnPipeObjektArt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ColumnPipeGeoobjektArt
            // 
            this.ColumnPipeGeoobjektArt.HeaderText = "GeoobjektArt";
            this.ColumnPipeGeoobjektArt.Name = "ColumnPipeGeoobjektArt";
            this.ColumnPipeGeoobjektArt.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnPipeGeoobjektArt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColumnPipeGeoobjektArt.Width = 120;
            // 
            // ColumnPipeDirection
            // 
            this.ColumnPipeDirection.HeaderText = "Direction";
            this.ColumnPipeDirection.Name = "ColumnPipeDirection";
            this.ColumnPipeDirection.Width = 60;
            // 
            // ColumnPipeSlope
            // 
            dataGridViewCellStyle2.Format = "N3";
            dataGridViewCellStyle2.NullValue = null;
            this.ColumnPipeSlope.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColumnPipeSlope.HeaderText = "Slope";
            this.ColumnPipeSlope.Name = "ColumnPipeSlope";
            // 
            // ColumnPipeMaterial
            // 
            this.ColumnPipeMaterial.HeaderText = "Material";
            this.ColumnPipeMaterial.Name = "ColumnPipeMaterial";
            // 
            // ColumnPipeDiameter
            // 
            this.ColumnPipeDiameter.HeaderText = "Diameter";
            this.ColumnPipeDiameter.Name = "ColumnPipeDiameter";
            // 
            // ColumnPipeEmpty
            // 
            this.ColumnPipeEmpty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnPipeEmpty.HeaderText = "";
            this.ColumnPipeEmpty.Name = "ColumnPipeEmpty";
            this.ColumnPipeEmpty.ReadOnly = true;
            // 
            // errorProviderDgvCell
            // 
            this.errorProviderDgvCell.ContainerControl = this;
            // 
            // dataGridViewButtonColumn1
            // 
            this.dataGridViewButtonColumn1.HeaderText = "Entity";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.Text = "Update...";
            this.dataGridViewButtonColumn1.ToolTipText = "Update only if pipe is not straight from start shaft to end shaft.";
            this.dataGridViewButtonColumn1.Width = 55;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Bezeichnung";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 60;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Slope";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 50;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.HeaderText = "Material";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 70;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.HeaderText = "ObjektArt";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.ReadOnly = true;
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.HeaderText = "GeoobjektArt";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 120;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Diameter";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.HeaderText = "";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Diameter";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn7.HeaderText = "";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // contextMenuStripPipeData
            // 
            this.contextMenuStripPipeData.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemPipeData});
            this.contextMenuStripPipeData.Name = "contextMenuStripColumnName";
            this.contextMenuStripPipeData.Size = new System.Drawing.Size(136, 26);
            // 
            // toolStripMenuItemPipeData
            // 
            this.toolStripMenuItemPipeData.Name = "toolStripMenuItemPipeData";
            this.toolStripMenuItemPipeData.Size = new System.Drawing.Size(135, 22);
            this.toolStripMenuItemPipeData.Text = "Select Data ";
            // 
            // PipeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgvPipes);
            this.Name = "PipeControl";
            this.Size = new System.Drawing.Size(793, 234);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPipes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderDgvCell)).EndInit();
            this.contextMenuStripPipeData.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvPipes;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.ErrorProvider errorProviderDgvCell;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripPipeData;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemPipeData;
        private System.Windows.Forms.DataGridViewButtonColumn ColumnPipeEntity;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnPipeGeometryExists;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnPipeGeometryAssigned;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPipeName;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnPipeObjektArt;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnPipeGeoobjektArt;
        private System.Windows.Forms.DataGridViewButtonColumn ColumnPipeDirection;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPipeSlope;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPipeMaterial;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPipeDiameter;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPipeEmpty;
    }
}
