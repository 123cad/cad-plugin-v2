﻿using Inventory.CAD.GeometryAssignment.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.GeometryAssignment.Data
{
    /// <summary>
    /// Adapter to a class from inventory - we want to use
    /// databinding (exact property names are required) from this class.
    /// </summary>
    public class PipeAdapter :INotifyPropertyChanged, IAdapter
    {
        /// <summary>
        /// Associated object from the drawing. If null, points can exist (from shafts),
        /// but if not null, geometry points are ONLY from this object (and are updated accordingly).
        /// </summary>
        public ObjectId AssociatedCadEntity { get; private set; }
        /// <summary>
        /// Pipe from Inventory to which this class is adapting.
        /// </summary>
        private Pipe inventoryObject { get; set; }

        /// <summary>
        /// If user has assigned geometry to this object (yet to be created).
        /// </summary>
        public bool IsGeometryAssigned => inventoryObject.IsGeometryAssigned;

        /// <summary>
        /// If Geometry already existed in inventory.
        /// </summary>
        public bool IsybauGeometryExists => inventoryObject.GeometryExists;
        /// <summary>
        /// Geometry can be assigned from shaft (only points). This tells if 
        /// </summary>
        public bool IsCadObjectAssigned => !AssociatedCadEntity.IsNull;
        public string Name => inventoryObject.Name;
        public string FromShaft => inventoryObject.FromShaft;
        public string ToShaft => inventoryObject.ToShaft;
        public string ObjektArt => inventoryObject.ObjektArt.ToString();
        public string Material
        {
            get
            {
                return inventoryObject.Material;
            }
            set
            {
                if (inventoryObject.Material != value)
                {
                    inventoryObject.Material = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public double SlopePermille
        {
            get
            {
                return inventoryObject.SlopePermille;
            }
            set
            {
                if (inventoryObject.SlopePermille != value)
                {
                    inventoryObject.SlopePermille = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string GeoObjektArt
        {
            get
            {
                return inventoryObject.GeoobjektArt;
            }
            set
            {
                if (inventoryObject.GeoobjektArt != value)
                {
                    inventoryObject.GeoobjektArt = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public int Diameter
        {
            get
            {
                return inventoryObject.Diameter;
            }
            set
            {
                if (inventoryObject.Diameter != value)
                {
                    inventoryObject.Diameter = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public PipeAdapter(Pipe p)
        {
            inventoryObject = p;
        }

        public IEnumerable<ObjectId> GetAssociatedObjectIds()
        {
            yield return AssociatedCadEntity;
        }

        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Sets point for this object and objectId if it exists (if shaft assigns points, then this is null).
        /// </summary>
        public void AssignGeometry(List<MyUtilities.Geometry.Point3d> points, ObjectId? oid = null)
        {
            if (oid != null)
            {
                AssociatedCadEntity = oid.Value;
                NotifyPropertyChanged(nameof(IsCadObjectAssigned));
            }
            var pts = points.Select(x => new Inventory.CAD.Geometry.Point(x)).ToList();
            double oldSlope = inventoryObject.SlopePermille;
            inventoryObject.SetPoints(pts);
            double newSlope = inventoryObject.SlopePermille;
            if (!DoubleHelper.AreEqual(oldSlope, newSlope, 0.1))
                NotifyPropertyChanged(nameof(SlopePermille));

            NotifyPropertyChanged(nameof(IsGeometryAssigned));
            
        }

        /// <summary>
        /// Updates pipe's existing point.
        /// </summary>
        public void UpdateShaftPoint(string shaftName, MyUtilities.Geometry.Point3d newShaftCenter)
        {
            // If user assigned entity for this pipe, shafts does not control it anymore.
            if (IsCadObjectAssigned)
                return;
            // If no points are set, update not possible.
            if (!inventoryObject.IsGeometryAssigned)
                return;

            if (inventoryObject.FromShaft == shaftName)
                inventoryObject.UpdateStartPoint(new Inventory.CAD.Geometry.Point(newShaftCenter));
            if (inventoryObject.ToShaft == shaftName)
                inventoryObject.UpdateEndPoint(new Inventory.CAD.Geometry.Point(newShaftCenter));
        }
        /// <summary>
        /// Start and end point exist.
        /// </summary>
        public bool PointsExist => inventoryObject.Points.Count() >= 2;
        public MyUtilities.Geometry.Point3d Start => inventoryObject.Points.First().GetAsMyPoint3d();
        public MyUtilities.Geometry.Point3d End => inventoryObject.Points.Last().GetAsMyPoint3d();
        

    }
}
