﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.GeometryAssignment.Data
{
    public interface IAdapter
    {
        string Name { get; }
        /// <summary>
        /// Currently only 1 entity per object is possible, but 
        /// it can change in future.
        /// </summary>
        /// <returns></returns>
        IEnumerable<ObjectId> GetAssociatedObjectIds();

        /// <summary>
        /// Has user assigned an object to this object.
        /// </summary>
        bool IsCadObjectAssigned { get; }

        /// <summary>
        /// Has geometry been assigned to this object in current session.
        /// </summary>
        bool IsGeometryAssigned { get; }

        /// <summary>
        /// Has geometry existed in inventary before invoking geometry assignment.
        /// </summary>
        bool IsybauGeometryExists { get; }
    }
}
