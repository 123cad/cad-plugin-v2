﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventory.CAD.GeometryAssignment.Data;
using System.Runtime.CompilerServices;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.GeometryAssignment.Data
{
    /// <summary>
    /// Adapter to a class from inventory - we want to use
    /// databinding (exact property names are required) from this class.
    /// </summary>
    public class ShaftAdapter : INotifyPropertyChanged, IAdapter
    {
        public ObjectId AssociatedCadEntity { get; private set; }
        private Shaft inventoryObject { get; set; }


        /// <summary>
        /// If user has assigned geometry to this object (yet to be created).
        /// </summary>
        public bool IsGeometryAssigned => inventoryObject.IsGeometryAssigned;
        /// <summary>
        /// If Geometry already exists.
        /// </summary>
        public bool IsybauGeometryExists => inventoryObject.GeometryExists;

        public bool IsCadObjectAssigned => !AssociatedCadEntity.IsNull;

        /// <summary>
        /// Shaft position with height of shaft bottom.
        /// </summary>
        public MyUtilities.Geometry.Point3d CenterBottom
        {
            get
            {
                var p = inventoryObject.CenterSMP;
                return new MyUtilities.Geometry.Point3d(p.X, p.Y, p.Z);
            }
            set
            {
                inventoryObject.CenterSMP = new Inventory.CAD.Geometry.Point(value.X, value.Y, value.Z);
            }
        }

        public string Name
        {
            get
            {
                return inventoryObject.Name;
            }
        }
        public string ObjektArt
        {
            get
            {
                return inventoryObject.ObjektArt.ToString();
            }
        }

        public string GeoobjektArt
        {
            get
            {
                return inventoryObject.GeoobjektArt;

            }
            set
            {
                if (inventoryObject.GeoobjektArt != value)
                {
                    inventoryObject.GeoobjektArt = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public int DiameterMM
        {
            get
            {
                return inventoryObject.DiameterMM;

            }
            set
            {
                if (inventoryObject.DiameterMM != value)
                {
                    inventoryObject.DiameterMM = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        public ShaftAdapter(Shaft s)
        {
            AssociatedCadEntity = ObjectId.Null;
            inventoryObject = s;
        }

        public IEnumerable<ObjectId> GetAssociatedObjectIds()
        {
            yield return AssociatedCadEntity;
        }

        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void AssignCadEntity(ObjectId oid, MyUtilities.Geometry.Point3d pointSMP)
        {
            AssociatedCadEntity = oid;

            inventoryObject.CenterSMP = new Inventory.CAD.Geometry.Point(pointSMP.X, pointSMP.Y, pointSMP.Z);
            NotifyPropertyChanged(nameof(IsGeometryAssigned));
        }
    }
}
