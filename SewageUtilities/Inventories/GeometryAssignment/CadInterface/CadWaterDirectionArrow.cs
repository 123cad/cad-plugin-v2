﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using myG = MyUtilities.Geometry;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.GeometryAssignment
{
    /// <summary>
    /// Draws arrow between from start to end point, indicating waterflow direction.
    /// </summary>
    public class CadWaterDirectionArrow:IDisposable
    {
        internal class ArrowSize
        {/// <summary>
         /// Total length of the arrow (head + tail)
         /// </summary>
            internal double TotalLength { get; private set; }
            /// <summary>
            /// Widest part of arrow head.
            /// </summary>
            internal double Width { get; private set; }
            /// <summary>
            /// Length of arrow head.
            /// </summary>
            internal double TopLength { get; private set; }
            /// <summary>
            /// Length of arrow tail.
            /// </summary>
            internal double BottomLength { get; private set; }
            /// <summary>
            /// Width of arrow tail.
            /// </summary>
            internal double TailWidth { get; private set; }
            /// <summary>
            /// Sets arrow size scaled to length (shorter arrow = scaled smaller),
            /// </summary>
            /// <param name="length"></param>
            /// <param name="width">If 0 width is not scaled - provided one is used.</param>
            internal ArrowSize(double length, double width = 0)
            {
                TotalLength = length;
                if (width == 0)
                    Width = length * 0.33;
                else
                    Width = width;
                TopLength = length * 0.6;
                BottomLength = TotalLength - TopLength;
                TailWidth = Width * 0.33;
            }
        }
        private readonly Document doc;

        /// <summary>
        /// All objects which are part of arrow.
        /// </summary>
        private readonly List<ObjectId> arrowObjects = new List<ObjectId>();

        internal ArrowSize ArrowDimensions { get; private set; } = new ArrowSize(2);

        internal CadWaterDirectionArrow(Document doc)
        {
            this.doc = doc;
        }
        internal void SetArrow(myG.Point3d start, myG.Point3d end)
        {
            CancelArrow();
            ArrowSize arrow = ArrowDimensions;
            myG.Vector2d pipe2 = start.GetVectorTo(end).GetAs2d();
            // Because of long pipes, arrow has scaled length.
            // But, if pipe is shorter than some length, value is fixed.
            // Again but, if pipe is shorter than last desired arrow length, 0.9 of arrow length is used.
            // *For example: 10% is arrow length, 2m default length. If pipe is shorter than 20m, 
            // *arrow is set to be fixed 2m. But, if pipe is shorter than 2m/0.9, arrow gets length of 90% of pipe.
            // *Which means for arrow length: a) pipe>20m => arrow=pipe*0.1 b) 20m>pipe>2.5 =>arrow = 2 c) 2.222>pipe => arrow=0.9*pipe
            double desiredLength = 0.1 * pipe2.Length;
            if (pipe2.Length < 20)
                desiredLength = 2;
            if (pipe2.Length < 2.22)
                desiredLength = 0.9 * pipe2.Length;
            arrow = new ArrowSize(desiredLength);
            myG.Vector2d unit2 = pipe2.GetUnitVector();
            myG.Point2d middle = start.GetAs2d().Add(pipe2.Multiply(0.5));
            myG.Point2d arrowStart = middle.Add(unit2.Negate().Multiply(arrow.BottomLength));

            List<myG.Point2d> points = new List<myG.Point2d>();
            points.Add(arrowStart);
            myG.Point2d temp = arrowStart.Add(unit2.Multiply(arrow.BottomLength));
            points.Add(temp);
            temp = temp.Add(unit2.Multiply(arrow.TopLength));
            points.Add(temp);

            using (Transaction tr = doc.Database.TransactionManager.StartTransaction())
            {
                TransactionData data = new TransactionData(doc, tr);
                Polyline pl = tr.CreateEntity<Polyline>(data.Btr);
                pl.AddPoints(points.Select(x=>x.ToCADPoint()).ToArray());
                pl.Elevation = start.Z;
                pl.SetStartWidthAt(0, arrow.TailWidth);
                pl.SetEndWidthAt(0, arrow.TailWidth);
                pl.SetStartWidthAt(1, arrow.Width);

                arrowObjects.Add(pl.ObjectId);

                tr.Commit();
            }
        }
        internal void CancelArrow()
        {
            if (arrowObjects.Count == 0)
                return;

            using (Transaction tr = doc.Database.TransactionManager.StartTransaction())
            {
                foreach(ObjectId id in arrowObjects)
                {
                    DBObject obj = tr.GetObject(id, OpenMode.ForWrite);
                    obj.Erase();
                }
                tr.Commit();
            }
            arrowObjects.Clear();
        }

        public void Dispose()
        {
            CancelArrow();
        }
    }
}
