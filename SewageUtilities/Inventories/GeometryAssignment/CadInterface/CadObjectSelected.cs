﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.GeometryAssignment.CadInterface;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.GeometryAssignment
{
    public class CadObjectSelected:IDisposable
    {
        private Document doc { get; set; }

        /// <summary>
        /// All selected object, including created ones.
        /// </summary>
        private readonly List<ObjectId> selectedObjects = new List<ObjectId>();
        /// <summary>
        /// Objects which are created, and need to be disposed.
        /// </summary>
        private readonly List<ObjectId> createdObjects = new List<ObjectId>();
        private readonly ZoomManager zoomMgr;
        internal CadObjectSelected(Document doc, ZoomManager zoomMgr)
        {
            this.doc = doc;
            this.zoomMgr = zoomMgr;
        }

        /// <summary>
        /// Selects provided object, and optionally, zooms to them.
        /// </summary>
        internal void SelectObjects(IEnumerable<ObjectId> oids)
        {
            CancelSelection();
            using (Transaction tr = doc.Database.TransactionManager.StartTransaction())
            {
                List<Entity> ents = new List<Entity>();
                // If
                //Point3d? center = null;
                //double width = 1;
                //double height = 1;

                foreach (var id in oids)
                {
                    Entity e = tr.GetObject(id, OpenMode.ForWrite) as Entity;
                    ents.Add(e);
                    e.Highlight();
                    selectedObjects.Add(e.ObjectId);
                    /*if (center == null)
                    {
                        DBPoint p = e as DBPoint;
                        Circle c = e as Circle;
                        BlockReference br = e as BlockReference;
                        if (p != null)
                            center = p.Position;
                        if (c != null)
                            center = c.Center;
                        if (br != null)
                            center = br.Position;
                        if (center.HasValue)
                        {
                            var extents = e.GeometricExtents;
                            width = extents.MaxPoint.X - extents.MinPoint.X;
                            width *= 1.2;
                            height = extents.MaxPoint.Y - extents.MinPoint.Y;
                            height *= 1.2;
                        }
                    }*/
                }
                /*if (zoom)
                {
                    if (center.HasValue && ents.Count == 1)
                        CADCommonHelper.ZoomTo(doc.Editor, center.Value.FromCADPoint(), width, height);
                    else
                        CADCommonHelper.ZoomToEntities(doc.Editor, ents, 20);
                }*/

                tr.Commit();
            }
            zoomMgr.ZoomToObjects(selectedObjects);
        }
        /// <summary>
        /// Creates points, line which connects them, and makes them selected.
        /// (only pipes are supported, since shaft can't have geometry without an entity)
        /// </summary>
        /// <param name="pts"></param>
        /// <param name="zoom"></param>
        internal void CreateSelectedObject(IEnumerable<MyUtilities.Geometry.Point3d> pts)
        {
            CancelSelection();
            List<ObjectId> oids = new List<ObjectId>();
            using (Transaction tr = doc.Database.TransactionManager.StartTransaction())
            {
                TransactionData data = new TransactionData(doc, tr);
                List<DBPoint> ents = new List<DBPoint>();
                DBPoint last = null;
                foreach (var pt in pts)
                {
                    DBPoint p = tr.CreateEntity<DBPoint>(data.Btr);
                    ents.Add(p);
                    oids.Add(p.ObjectId);
                    p.Position = pt.ToCADPoint();
                    p.Highlight();

                    createdObjects.Add(p.ObjectId);
                    selectedObjects.Add(p.ObjectId);

                    if (last != null)
                    {
                        Line l = tr.CreateEntity<Line>(data.Btr);
                        l.StartPoint = last.Position;
                        l.EndPoint = p.Position;
                        l.Highlight();

                        createdObjects.Add(l.ObjectId);
                        selectedObjects.Add(l.ObjectId);
                    }

                    last = p;
                }

                tr.Commit();
            }
            zoomMgr.ZoomToObjects(oids);
        }
        
        internal void CancelSelection()
        {
            if (selectedObjects.Count == 0)
                return;
            using (Transaction tr = doc.Database.TransactionManager.StartTransaction())
            {
                foreach(var oid in selectedObjects)
                {
                    Entity obj = tr.GetObject(oid, OpenMode.ForWrite) as Entity;
                    obj.Unhighlight();
                }
                foreach(var oid in createdObjects)
                {
                    DBObject obj = tr.GetObject(oid, OpenMode.ForWrite);
                    obj.Erase();
                }
                tr.Commit();
            }
            createdObjects.Clear();
            selectedObjects.Clear();
        }

        public void Dispose()
        {
            CancelSelection();
        }
    }
}
