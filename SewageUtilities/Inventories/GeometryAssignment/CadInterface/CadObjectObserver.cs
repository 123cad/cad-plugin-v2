﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.GeometryAssignment
{

    //NOTE Autocad doesn't support transaction from ObjectModified event! Check forum for details.

    /// <summary>
    /// Responsible for observing if object has been updated in the drawing, and to notify others.
    /// </summary>
    public class CadObjectObserver:IDisposable
    {
        private Document doc { get; set; }
        private Database db { get; set; }

        private Dictionary<ObjectId, object> observers = new Dictionary<ObjectId, object>();

        public event Action<DBObject, object> ObjectModified;

        internal CadObjectObserver(Document doc)
        {
            this.doc = doc;
            db = doc.Database;
            db.ObjectModified += Db_ObjectModified;
        }

        private void Db_ObjectModified(object sender, ObjectEventArgs e)
        {

            return;
#pragma warning disable CS0162 // Unreachable code detected
            if (observers.Any())
#pragma warning restore CS0162 // Unreachable code detected
            {
                // How to notify exactly the class that we need for changed object?
                // next to object keep Action? or just keep Action?
                //NOTE NOT POSSIBLE to start transaction from ObjectModified event.
                if (observers.ContainsKey(e.DBObject.ObjectId))
                    ObjectModified?.Invoke(e.DBObject, observers[e.DBObject.ObjectId]);
            }
        }
        /// <summary>
        /// Only 1 object can observe 1 ObjectId.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="obj"></param>
        internal void Add(ObjectId id, object obj)
        {
            if (id.IsNull)
                return;
            if (!observers.ContainsKey(id))
                observers.Add(id, null);
            observers[id] = obj;
        }
        /// <summary>
        /// Remove object from observing by id.
        /// </summary>
        /// <param name="id"></param>
        internal void Remove(ObjectId id)
        {
            if (observers.ContainsKey(id))
                observers.Remove(id);
        }
        /// <summary>
        /// Remove object from observing.
        /// </summary>
        /// <param name="obj"></param>
        internal void Remove(object obj)
        {
            ObjectId? key = null;
            foreach(var pair in observers)
                if (pair.Value == obj)
                {
                    key = pair.Key;
                    break;
                }
            if (key.HasValue)
                observers.Remove(key.Value);
        }

        public void Dispose()
        {
            observers.Clear();
            db.ObjectModified -= Db_ObjectModified;
        }
    }
}
