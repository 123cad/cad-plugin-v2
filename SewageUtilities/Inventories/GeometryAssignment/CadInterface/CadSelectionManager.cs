﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Inventories.GeometryAssignment.Data;
using Inventory.CAD.GeometryAssignment.Data;
using CadPlugin.Inventories.GeometryAssignment.CadInterface;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.GeometryAssignment
{
    /// <summary>
    /// Manages selection of objects on the drawing.
    /// </summary>
    public class CadSelectionManager:IDisposable
    {
        private readonly Document doc;
        private readonly CadTextSelection textSelection;
        private readonly CadWaterDirectionArrow arrowDrawer;
        private readonly CadObjectSelected selectedObject;
        private readonly ZoomManager zoomManager;

        public  CadSelectionManager(Document doc, DataManagerEventArgs args, ZoomManager zoomMgr)
        {
            this.doc = doc;
            zoomManager = zoomMgr;
            var allText = args.Pipes.Select(x => x.Name).Union(args.Shafts.Select(x => x.Name)).Distinct();
            textSelection = new CadTextSelection(doc, zoomMgr, allText);
            arrowDrawer = new CadWaterDirectionArrow(doc);
            selectedObject = new CadObjectSelected(doc, zoomMgr);
        }


        /// <summary>
        /// Updates selection. Returns false if nothing is to be selected for object.
        /// </summary>
        /// <param name="obj"></param>
        public  bool SelectionChanged(IAdapter obj)
        {
            bool selected = false;
            CancelSelection();
            // Cancel all previous selections.
            PipeAdapter pa = obj as PipeAdapter;
            ShaftAdapter sa = obj as ShaftAdapter;
            if (pa != null)
            {
                if (pa.IsCadObjectAssigned)
                {
                    // If object is assigned, it is selected.
                    selectedObject.SelectObjects(Enumerable.Repeat(pa.AssociatedCadEntity, 1));
                    arrowDrawer.SetArrow(pa.Start, pa.End);
                    selected = true;
                }
                else
                {
                    // If object is not assigned.
                    if (pa.IsGeometryAssigned)
                    {
                        // If geometry is assigned.
                        // Create entities.
                        selectedObject.CreateSelectedObject(new MyUtilities.Geometry.Point3d[] { pa.Start, pa.End });
                        arrowDrawer.SetArrow(pa.Start, pa.End);
                        selected = true;
                    }
                    else
                    {
                        // If geometry is not assigned.
                        // Zoom to possible texts.
                        selected = textSelection.SelectObjectsForText(pa.Name);
                    }
                }
            }
            if (sa != null)
            {
                // ShaftAdapter either has cad object or not. Geometry does not depend
                // on any other object, so it can't exist without entity.
                if (sa.IsCadObjectAssigned)
                {
                    // Cad object is assigned.
                    selectedObject.SelectObjects(Enumerable.Repeat(sa.AssociatedCadEntity, 1));
                    selected = true;
                }
                else
                {
                    // Cad object is not assigned.
                    selected = textSelection.SelectObjectsForText(sa.Name);
                }


            }
            return selected;
        }
        /// <summary>
        /// Cancels all selections.
        /// </summary>
        internal void CancelSelection()
        {
            arrowDrawer.CancelArrow();
            selectedObject.CancelSelection();
        }
        public void Dispose()
        {
            // Cancel all selection.
            arrowDrawer.Dispose();
            selectedObject.Dispose();
        }
    }
}
