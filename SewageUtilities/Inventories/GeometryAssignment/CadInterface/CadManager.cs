﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using System.Diagnostics;
using CadPlugin.Inventories.GeometryAssignment.Data;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.GeometryAssignment
{
    /// <summary>
    /// Used for CAD operations.
    /// IDisposable because it can attach to ObjectChanged event, so we need a way to detach it.
    /// </summary>
    public class CadManager:IDisposable
    {
        /// <summary>
        /// Objects which are currently selected.
        /// </summary>
        private List<ObjectId> lastSelectedObjects = null;

        /// <summary>
        /// User can change current document, and we want to associate this instance only with 
        /// single one. Since command invokes this constructor, it is enough to get current document there.
        /// </summary>
        public readonly Document document;

        private readonly CadObjectObserver observer;

        /// <summary>
        /// Invoked when geometry for shaft is changed by the user (directly changed the cad entity).
        /// </summary>
        public event Action<ShaftAdapter> ShaftUpdated;
        /// <summary>
        /// Invoked when geometry for pipe is changed by the user (directly changed the cad entity).
        /// </summary>
        public event Action<PipeAdapter> PipeUpdated;

        /// <summary>
        /// Indicates if changes in observed object should be ignored.
        /// (usually when updating CAD object, we don't want to invoke event 2 times.)
        /// </summary>
        private bool RaiseObjectUpdatedEvent = true;

        /// <summary>
        /// For private purpose - not to lock same document multiple times.
        /// </summary>
        private DocumentLock currentDocumentLock = null;

        /// <summary>
        /// Initialized with current document.
        /// </summary>
        public CadManager():this(Application.DocumentManager.MdiActiveDocument)
        {
        }
        public CadManager(Document doc)
        {
            document = doc;
            observer = new CadObjectObserver(doc);
            observer.ObjectModified += Observer_ObjectModified;
            currentDocumentLock = document.LockDocument();
        }
        
        /// <summary>
        /// Sets object as selected (for example it can be Highlighted).
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="zoom">Should drawing be zoomed to the object.</param>
        /*public void SetObjectSelected(IAdapter obj, bool zoom = false)
        {
            // When communicating with Document form modeless dialog, lock the document.
            UnselectCurrentObject();
            IEnumerable<ObjectId> oids = obj.GetAssociatedObjectIds();
            SetObjectSelectedState(oids, true);

            lastSelectedObjects = oids.ToList();
            lastSelectedObjects = lastSelectedObjects.Where(x => x != ObjectId.Null).ToList();
            if (zoom)
            {
                ZoomToSelectedEntities(oids);
            }
        }*/
        

        /// <summary>
        /// Unselects object which is currently selected.
        /// </summary>
        public void UnselectCurrentObject()
        {
            if (lastSelectedObjects != null)
            {
                SetObjectSelectedState(lastSelectedObjects, false);
                lastSelectedObjects = null;
            }
        }
        /// <summary>
        /// Sets selected state for objects.
        /// </summary>
        /// <param name="oids"></param>
        /// <param name="selected">true - select, false - unselect</param>
        private void SetObjectSelectedState(IEnumerable<ObjectId> oids, bool selected)
        {
            // When communicating with Document form modeless dialog, lock the document.
            Database db = document.Database;
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                foreach (ObjectId oid in oids)
                {
                    //Debug.Assert(oid != ObjectId.Null, "Set selected object null");
                    if (oid.IsNull)
                        continue;
                    Entity e = tr.GetObject(oid, OpenMode.ForWrite) as Entity;
                    if (selected)
                        e.Highlight();
                    else
                        e.Unhighlight();
                }
                tr.Commit();
            }
        }
        /// <summary>
        /// Returns the text selected by the user.
        /// </summary>
        /// <returns></returns>
        public string GetTextString()
        {
            string s = null;
            // When communicating with Document form modeless dialog, lock the document.
            Editor ed = document.Editor;
            PromptEntityOptions ent = new PromptEntityOptions("Select Text");
            ent.SetRejectMessage("Object is not text!");
            ent.AddAllowedClass(typeof(MText), true);
            ent.AddAllowedClass(typeof(DBText), true);

            PromptEntityResult res = ed.GetEntity(ent);
            if (res.Status == PromptStatus.OK)
            {
                using (Transaction tr = document.Database.TransactionManager.StartTransaction())
                {
                    Entity e = tr.GetObject(res.ObjectId, OpenMode.ForRead) as Entity;
                    MText mt = e as MText;
                    DBText t = e as DBText;
                    if (mt != null)
                        s = mt.Contents;
                    if (t != null)
                        s = t.TextString;
                }
            }
            ed.WriteMessage("\n");
            return s;
        }
        /// <summary>
        /// Assigns entity to a shaft. Returns false in case of error or if user canceled.
        /// </summary>
        /// <returns></returns>
        public bool AssignShaftEntity(ShaftAdapter shaft)
        {
            // When communicating with Document form modeless dialog, lock the document.
            Editor ed = document.Editor;
            PromptEntityOptions ent = new PromptEntityOptions("Select Shaft:");
            ent.SetRejectMessage("Object can be: BlockReference, Circle!");
            ent.AddAllowedClass(typeof(BlockReference), true);
            ent.AddAllowedClass(typeof(Circle), true);

            PromptEntityResult res = ed.GetEntity(ent);

            if (!res.ObjectId.IsNull)
            {
                List<MyUtilities.Geometry.Point3d> points = new List<MyUtilities.Geometry.Point3d>();
                MyUtilities.Geometry.Point3d center = new MyUtilities.Geometry.Point3d();
                using (Transaction tr = document.Database.TransactionManager.StartTransaction())
                {
                    TransactionData t = new TransactionData(document, tr);
                    Entity e = tr.GetObject(res.ObjectId, OpenMode.ForRead) as Entity;
                    Circle c = e as Circle;
                    BlockReference br = e as BlockReference;
                    if (c != null)
                    {
                        center = c.Center.FromCADPoint();
                    }
                    if (br != null)
                    {
                        center = br.Position.FromCADPoint();
                    }
                }
                // If old entity exists, remove it.
                observer.Remove(shaft.AssociatedCadEntity);
                shaft.AssignCadEntity(res.ObjectId, center);
                observer.Add(res.ObjectId, shaft);
            }

            ed.WriteMessage("\n");
            return res.Status == PromptStatus.OK && !res.ObjectId.IsNull;
        }
        /// <summary>
        /// Sets pipe object (Line, Polyline, Polyline2d, Polyline3d).
        /// </summary>
        /// <returns></returns>
        public bool AssignPipeEntity(PipeAdapter pipe)
        {
            // When communicating with Document form modeless dialog, lock the document.
            Editor ed = document.Editor;
            PromptEntityOptions ent = new PromptEntityOptions("Select Pipe:");
            ent.SetRejectMessage("Object can be: Line, Polyline, Polyline3d!");
            ent.AddAllowedClass(typeof(Line), true);
            ent.AddAllowedClass(typeof(Polyline), true);
            ent.AddAllowedClass(typeof(Polyline2d), true);
            ent.AddAllowedClass(typeof(Polyline3d), true);

            PromptEntityResult res = ed.GetEntity(ent);

            if (!res.ObjectId.IsNull)
            {
                List<MyUtilities.Geometry.Point3d> points = new List<MyUtilities.Geometry.Point3d>();
                using (Transaction tr = document.Database.TransactionManager.StartTransaction())
                {
                    Entity e = tr.GetObject(res.ObjectId, OpenMode.ForRead) as Entity;
                    Line l = e as Line;
                    if (l != null)
                    {
                        points.Add(l.StartPoint.FromCADPoint());
                        points.Add(l.EndPoint.FromCADPoint());
                    }
                    else
                    {
                        var v = CADPolylineHelper.GetPoints(e, tr)?.Select(x => x.FromCADPoint());
                        if (v != null)
                            points.AddRange(v.ToArray());
                    }
                }
                observer.Remove(pipe.AssociatedCadEntity);
                pipe.AssignGeometry(points, res.ObjectId);
                observer.Add(res.ObjectId, pipe);
            }

            ed.WriteMessage("\n");
            return res.Status == PromptStatus.OK && !res.ObjectId.IsNull;
        }

        public  void ReversePipePoints(PipeAdapter pa)
        {
            if (pa.AssociatedCadEntity.IsNull)
                throw new ArgumentNullException("Reverse pipe points: entity is null!");
            try
            {
                List<Point3d> points = null;
                RaiseObjectUpdatedEvent = false;
                using (Transaction tr = document.Database.TransactionManager.StartTransaction())
                {
                    Entity e = tr.GetObject(pa.AssociatedCadEntity, OpenMode.ForWrite) as Entity;
                    CADPolylineHelper.ReversePoints(e, tr);
                    if (e is Line)
                    {
                        points = new List<Point3d>()
                        {
                            ((Line)e).StartPoint,
                            ((Line)e).EndPoint
                        };
                    }
                    else
                    {
                        points = CADPolylineHelper.GetPoints(e, tr);
                    }
                    
                    tr.Commit();
                }
                if (points != null)
                    pa.AssignGeometry(points.Select(x=>x.FromCADPoint()).ToList() , pa.AssociatedCadEntity);
            }
            finally
            {
                RaiseObjectUpdatedEvent = true;
            }
            PipeUpdated?.Invoke(pa);
        }

        /// <summary>
        /// Adds object to be automatically updated when CAD entity is changed.
        /// </summary>
        /// <param name="adapter"></param>
        /// <param name="id"></param>
        public void AddAutomaticModifying(IAdapter adapter, ObjectId id)
        {
            observer.Add(id, adapter);
        }

        /// <summary>
        /// Remove object from automatic modification.
        /// </summary>
        /// <param name="adapter"></param>
        public void RemoveAutomaticModifying(IAdapter adapter)
        {
            observer.Remove(adapter);
        }

        private void Observer_ObjectModified(DBObject arg1, object arg2)
        {
            PipeAdapter pd = arg2 as PipeAdapter;
            ShaftAdapter sd = arg2 as ShaftAdapter;
            if (pd == null && sd == null)
                return;
            List<MyUtilities.Geometry.Point3d> points = new List<MyUtilities.Geometry.Point3d>();
            if (pd != null)
            {
                Line l = arg1 as Line;
                Polyline pl = arg1 as Polyline;
                Polyline2d pl2 = arg1 as Polyline2d;
                Polyline3d pl3 = arg1 as Polyline3d;

                MyUtilities.Geometry.Point3d
                    start = new MyUtilities.Geometry.Point3d(),
                    end = new MyUtilities.Geometry.Point3d();
                if (l != null)
                {
                    start = l.StartPoint.FromCADPoint();
                    end = l.EndPoint.FromCADPoint();
                }
                else if (pl != null)
                {
                    start = pl.GetPoint3dAt(0).FromCADPoint();
                    end = pl.GetPoint3dAt(pl.NumberOfVertices - 1).FromCADPoint();
                }
                else if (pl2 != null)
                {
                    start = pl2.GetPointAtParameter(0).FromCADPoint();
                    int counter = 0;
                    foreach (var v in pl2) counter++;
                    end = pl2.GetPointAtParameter(counter - 1).FromCADPoint();
                }
                else if (pl3 != null)
                {

                    start = pl3.GetPointAtParameter(0).FromCADPoint();
                    int counter = 0;
                    foreach (var v in pl3) counter++;
                    end = pl3.GetPointAtParameter(counter - 1).FromCADPoint();
                }
                else
                {
                    return;
                }
                points.Add(start);
                points.Add(end);
                pd.AssignGeometry(new List<MyUtilities.Geometry.Point3d>()
                {
                    start,
                    end
                },
                arg1.ObjectId
                    );

                // When this object will get event invoked, but from other place.
                if (RaiseObjectUpdatedEvent)
                    PipeUpdated?.Invoke(pd);
            }
            else
            {
                BlockReference br = arg1 as BlockReference;
                Circle cr = arg1 as Circle;
                MyUtilities.Geometry.Point3d center = new MyUtilities.Geometry.Point3d();
                if (br != null)
                {
                    center = br.Position.FromCADPoint();
                }
                else if (cr != null)
                {
                    center = cr.Center.FromCADPoint();
                }
                else
                {
                    return;
                }
                sd.CenterBottom = center;
                if (RaiseObjectUpdatedEvent)
                    ShaftUpdated?.Invoke(sd);
            }
        }
        public void Dispose()
        {
            if (currentDocumentLock != null)
            {
                currentDocumentLock.Dispose();
                currentDocumentLock = null;
            }
            observer.Dispose();
        }

        ~CadManager()
        {
            if (currentDocumentLock != null)
            {
                Debug.Fail("Document lock was not Disposed!");
                currentDocumentLock.Dispose();
                currentDocumentLock = null;
            }
        }
    }
}
