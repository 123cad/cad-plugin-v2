﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.GeometryAssignment.CadInterface
{
    /// <summary>
    /// Maintains zoom level same while selected objects are changed.
    /// Zoom is not dependent on the objects, but on the drawing itself.
    /// No matter what is selected, zoom level stays the same (but can be changed).
    /// </summary>
    public class ZoomManager
    {
        class ExtentsData
        {
            /// <summary>
            /// Max Width visible (when zoomed out). However, it also depends on Height 
            /// (cad maintains aspect ratio).
            /// </summary>
            public double Width;
            /// <summary>
            /// Max Height visible (when zoomed out). However, it also depends on Width 
            /// (cad maintains aspect ratio).
            /// </summary>
            public double Height;
            private double Scale;
            public Point3d Center;
            /// <summary>
            /// Used to have smaller steps when zoomed in. x^2
            /// </summary>
            private double[] nonLinearMultiplicators = new double[MaxZoomLevel + 1];
            public ExtentsData (double width, double height, Point3d center = new Point3d())
            {
                for (int i = 0; i <= MaxZoomLevel; i++)
                {
                    double d = (double)i / MaxZoomLevel;
                    d *= d;
                    nonLinearMultiplicators[i] = d;
                }

                Width = width;
                Height = height;
                Scale = height / width;
                if (Width > 100)
                {
                    Width = 100;
                    Height = Width * Scale;
                }
                else if (Height > 100)
                {
                    Height = 100;
                    Width = Height / Scale;
                }

                Center = center;

            }
            public ViewRectangle GetExtentsForZoom(int zoomLevel)
            {
                int deltaZoomLevel = MaxZoomLevel - zoomLevel;
                // Fixed value is used as minimum displayed (so text is not too zoomed).
                double newWidth =  3 + Width * nonLinearMultiplicators[deltaZoomLevel];
                double newHeight = Scale * 3 + Height * nonLinearMultiplicators[deltaZoomLevel];

                ViewRectangle vr = new ViewRectangle(newWidth, newHeight, Center.FromCADPoint());
                return vr;
            }
        }
        public const int InitialZoomLevel = 4;
        public const int MaxZoomLevel = 15;
        /// <summary>
        /// Zoom step in percent.
        /// </summary>
        public int ZoomStep = 30;
        public int CurrentZoomLevel { get; private set; } = InitialZoomLevel;
        public bool ZoomActive { get; set; } = true;

        private Document document;
        private ExtentsData data = null;

        public ZoomManager(Document doc)
        {
            document = doc;

            // We can use complete drawing Extents, but with performance impact.
            // However, for this, 100 seems to be enough.
            double width = 100;
            double height = 100;
            Point3d center = new Point3d(width / 2, height / 2, 0);
            data = new ExtentsData(width, height, center);
        }

        public void ZoomToObjects(IEnumerable<ObjectId> oids)
        {
            var ext = data.GetExtentsForZoom(CurrentZoomLevel);
            var ext2 = CADCommonHelper.GetObjectsExtents(document.Database, oids);
            double x = (ext2.MaxPoint.X + ext2.MinPoint.X) / 2;
            double y = (ext2.MaxPoint.Y + ext2.MinPoint.Y) / 2;
            data.Center = new MyUtilities.Geometry.Point3d(x, y, 0).ToCADPoint();
            CADZoomHelper.ZoomTo(document.Editor,data.Center.FromCADPoint(), ext.Width, ext.Height);
            
        }

        /// <summary>
        /// Applies zoom to current view. Allowed levels [0-10].
        /// </summary>
        /// <param name="level"></param>
        public void SetNewZoomLevel(int level)
        {
            if (level < 0)
                level = 0;
            else if (level > MaxZoomLevel)
                level = MaxZoomLevel;

            double delta = level - CurrentZoomLevel;
            CurrentZoomLevel = level;


            var ext = data.GetExtentsForZoom(CurrentZoomLevel);
            CADZoomHelper.ZoomTo(document.Editor, data.Center.FromCADPoint(), ext.Width, ext.Height);

        }


    }
}
