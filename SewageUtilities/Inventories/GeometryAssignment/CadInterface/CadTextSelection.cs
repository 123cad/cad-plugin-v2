﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using CadPlugin.Inventories.GeometryAssignment.CadInterface;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.GeometryAssignment
{
    /// <summary>
    /// Used to zoom drawing to objects selected. 
    /// When initialized, all texts in the drawing are checked, 
    /// and connected to the ones loaded from the Isybau file.
    /// This is done for performance reasons.
    /// 
    /// To help performance, simple relationship is used - all text in DBText or only first line of MText
    /// is checked.
    /// </summary>
    public class CadTextSelection
    {
        /// <summary>
        /// Association - text; objects in which text has been found.
        /// </summary>
        private Dictionary<string, List<ObjectId>> texts = new Dictionary<string, List<ObjectId>>();

        /// <summary>
        /// Currently associated database.
        /// </summary>
        private readonly Document doc;
        private readonly ZoomManager zoomManager;

        public CadTextSelection(Document doc, ZoomManager zoomMgr, IEnumerable<string> text)
        {
            this.doc = doc;
            zoomManager = zoomMgr;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            // Create dictionary where unique texts can be associated with multiple objects - all objects will be selected.
            texts = text.Distinct().ToDictionary(x => x, x => new List<ObjectId>());

            // Get all texts by iterating ModelSpace.
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                TransactionData data = new TransactionData(doc, tr);
                foreach (ObjectId oid in data.Btr)
                {
                    Entity e = tr.GetObject(oid, OpenMode.ForRead) as Entity;
                    DBText dt = e as DBText;
                    MText mt = e as MText;
                    if (dt == null && mt == null)
                        continue;
                    string s = null;
                    if (dt != null)
                    {
                        // All text from DBText.
                        s = dt.TextString.Trim();
                    }
                    if (mt != null)
                    {
                        // Only first line of MTEXT.
                        s = mt.GetTextInLine(0);// CADMTextHelper.GetTextInLine
                    }
                    if (s != null && texts.ContainsKey(s))
                    {
                        texts[s].Add(oid);
                    }
                }
            }
            



            /* Get all texts by SelectionSet
             
            // Start a transaction
            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                TypedValue[] filterCriteria = new TypedValue[1];
                filterCriteria.SetValue(new TypedValue((int)DxfCode.Start, "INSERT"), 0);
                SelectionFilter filter = new SelectionFilter()
                // Request for objects to be selected in the drawing area
                PromptSelectionResult acSSPrompt = doc.Editor.GetSelection();

                // If the prompt status is OK, objects were selected
                if (acSSPrompt.Status == PromptStatus.OK)
                {
                    SelectionSet acSSet = acSSPrompt.Value;

                    // Step through the objects in the selection set
                    foreach (SelectedObject acSSObj in acSSet)
                    {
                        // Check to make sure a valid SelectedObject object was returned
                        if (acSSObj != null)
                        {
                            // Open the selected object for write
                            Entity acEnt = acTrans.GetObject(acSSObj.ObjectId, OpenMode.ForRead) as Entity;
                            if (acEnt)
                            
                        }
                    }
                }

                // Dispose of the transaction
            }*/
        }
        /// <summary>
        /// Returns false if no text for the object.
        /// </summary>
        /// <param name="s"></param>
        public bool SelectObjectsForText(params string[] s)
        {
            bool textExists = true;
            IEnumerable<string> t = s.Where(x => texts.ContainsKey(x));
           
            IEnumerable<ObjectId> oids = t.SelectMany(x => texts[x]);
            if (!oids.Any())
            {
                textExists = false;
                return textExists;
            }
            using (Transaction tr = doc.Database.TransactionManager.StartTransaction())
            {
                List<Entity> ents = new List<Entity>();
                foreach (ObjectId oid in oids)
                {
                    Entity e = tr.GetObject(oid, OpenMode.ForWrite) as Entity;
                    ents.Add(e);
                }

                tr.Commit();
            }
            zoomManager.ZoomToObjects(oids);

            return textExists;
        }
    }
}
