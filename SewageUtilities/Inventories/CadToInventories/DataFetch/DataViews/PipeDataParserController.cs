﻿using CadPlugin.Inventories.CadToInventories.MainToolbars;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.Inventories.CadToInventories.DataFetch.DataViews
{
    class PipeDataParserController
    {
        private static bool canceled = false;
        private PipeLoadedData data;
        private CadManager mgr;
        private IPipeDataParserView view { get; set; }
        public static PipeLoadedData Start(CadManager mgr)
        {
            PipeLoadedData d = new PipeLoadedData();
            IPipeDataParserView f = new PipeDataParserView(d);
            PipeDataParserController pd = new PipeDataParserController(f, d, mgr);
            canceled = true;
            ((Form)f).ShowDialog();

            if (canceled)
                return null;
            return d;
        }
        private PipeDataParserController(IPipeDataParserView v, PipeLoadedData pd, CadManager cadMgr)
        {
            mgr = cadMgr;
            data = pd;
            v.SelectAll += () =>
            {
                // Select the text.
                string s = cadMgr.GetTextString();
                if (s == null)
                    return;
                // Parse the text, and populate data object.

                string[] p = s.Trim().Split('-');
                Debug.Assert(p.Length == 3, $"Text for pipe is not 3 sections long! It has {p.Length} sections");

                string[] p1 = p[0].Trim().Split(' ');
                Debug.Assert(p1.Length == 2, $"Text \"diameter\\material\" is invalid! {p1}");
                int diameter = 0;
                if (int.TryParse(p1[0], out diameter))
                {
                    data.Diameter = diameter;
                }

                string material = p1[1].Trim();
                if (Isybau2015.ReferenceTables.G.G102.Instance[material] != null)
                    data.Material = material;

                string slopeString = p[2].Trim().Split(' ')[0];
                slopeString = slopeString.Replace(',', '.');
                double slope = 0;
                if (DoubleHelper.TryParseInvariant(slopeString, out slope))
                {
                    data.Slope = slope;
                }
            };
            v.SelectDiameter += () =>
            {
                // Select the text, parse it and set to the object.
                string s = cadMgr.GetTextString();
                if (s == null)
                    return;
                s = s.Trim().Split(' ')[0];
                int diameter = 0;
                if (int.TryParse(s, out diameter))
                {
                    data.Diameter = diameter;
                }
            };
            v.SelectMaterial += () =>
            {
                // Select the text, parse it and set to the object.
                string s = cadMgr.GetTextString();
                if (s == null)
                    return;
                s = s.Trim().Split(' ')[1];
                string material = s.Trim();
                if (Isybau2015.ReferenceTables.G.G102.Instance[material] != null)
                    data.Material = material;
                // If material not allowed, don't set it.
            };
            v.SelectSlope+= () =>
            {
                // Select the text, parse it and set to the object.
                string s = cadMgr.GetTextString();
                if (s == null)
                    return;
                string slopeString = s.Trim();
                slopeString.Replace(',', '.');
                double slope = 0;
                if (DoubleHelper.TryParseInvariant(slopeString, out slope))
                {
                    data.Slope = slope;
                }
            };


            v.Apply += () =>
            {
                canceled = false;
                ((Form)v).Close();
            };
            v.Cancel += () =>
             {
                ((Form)v).Close();
             };


        }
    }
}
