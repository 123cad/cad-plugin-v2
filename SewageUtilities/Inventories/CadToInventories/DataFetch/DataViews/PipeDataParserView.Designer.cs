﻿namespace CadPlugin.Inventories.CadToInventories.DataFetch.DataViews
{
    partial class PipeDataParserView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PipeDataParserView));
            this.textBoxDiameter = new System.Windows.Forms.TextBox();
            this.textBoxMaterial = new System.Windows.Forms.TextBox();
            this.textBoxSlope = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonDiameter = new System.Windows.Forms.Button();
            this.buttonSlope = new System.Windows.Forms.Button();
            this.buttonMaterial = new System.Windows.Forms.Button();
            this.buttonSelectAllText = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonApply = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxDiameter
            // 
            resources.ApplyResources(this.textBoxDiameter, "textBoxDiameter");
            this.textBoxDiameter.Name = "textBoxDiameter";
            // 
            // textBoxMaterial
            // 
            resources.ApplyResources(this.textBoxMaterial, "textBoxMaterial");
            this.textBoxMaterial.Name = "textBoxMaterial";
            // 
            // textBoxSlope
            // 
            resources.ApplyResources(this.textBoxSlope, "textBoxSlope");
            this.textBoxSlope.Name = "textBoxSlope";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // buttonDiameter
            // 
            resources.ApplyResources(this.buttonDiameter, "buttonDiameter");
            this.buttonDiameter.Name = "buttonDiameter";
            this.buttonDiameter.TabStop = false;
            this.buttonDiameter.UseVisualStyleBackColor = true;
            // 
            // buttonSlope
            // 
            resources.ApplyResources(this.buttonSlope, "buttonSlope");
            this.buttonSlope.Name = "buttonSlope";
            this.buttonSlope.TabStop = false;
            this.buttonSlope.UseVisualStyleBackColor = true;
            // 
            // buttonMaterial
            // 
            resources.ApplyResources(this.buttonMaterial, "buttonMaterial");
            this.buttonMaterial.Name = "buttonMaterial";
            this.buttonMaterial.TabStop = false;
            this.buttonMaterial.UseVisualStyleBackColor = true;
            // 
            // buttonSelectAllText
            // 
            resources.ApplyResources(this.buttonSelectAllText, "buttonSelectAllText");
            this.buttonSelectAllText.Name = "buttonSelectAllText";
            this.buttonSelectAllText.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // buttonApply
            // 
            resources.ApplyResources(this.buttonApply, "buttonApply");
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            resources.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // PipeDataParserView
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonApply);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonSelectAllText);
            this.Controls.Add(this.buttonMaterial);
            this.Controls.Add(this.buttonSlope);
            this.Controls.Add(this.buttonDiameter);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxSlope);
            this.Controls.Add(this.textBoxMaterial);
            this.Controls.Add(this.textBoxDiameter);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PipeDataParserView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxDiameter;
        private System.Windows.Forms.TextBox textBoxMaterial;
        private System.Windows.Forms.TextBox textBoxSlope;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonDiameter;
        private System.Windows.Forms.Button buttonSlope;
        private System.Windows.Forms.Button buttonMaterial;
        private System.Windows.Forms.Button buttonSelectAllText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.Button buttonCancel;
    }
}