﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.Inventories.CadToInventories.DataFetch.DataViews
{
    interface IPipeDataParserView
    {
        event Action SelectDiameter;
        event Action SelectMaterial;
        event Action SelectSlope;

        /// <summary>
        /// Select text and parse it to the fields.
        /// </summary>
        event Action SelectAll;

        /// <summary>
        /// Apply changes.
        /// </summary>
        event Action Apply;
        event Action Cancel;

        TextBox Diameter { get; }
        TextBox Slope { get; }
        TextBox Material { get; }
    }
}
