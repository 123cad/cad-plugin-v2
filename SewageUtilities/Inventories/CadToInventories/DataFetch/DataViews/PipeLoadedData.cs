﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Inventories.CadToInventories.DataFetch.DataViews
{
    class PipeLoadedData:INotifyPropertyChanged
    {
        private int __Diameter;
        private string __Material;
        private double __Slope;

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        public bool IsDiameterSet { get; private set; } = false;
        public bool IsMaterialSet { get; private set; } = false;
        public bool IsSlopeSet { get; private set; } = false;
        public int Diameter
        {
            get
            {
                return __Diameter;
            }
            set
            {
                IsDiameterSet = true;
                __Diameter = value;
                OnPropertyChanged();
            }
        }
        public string Material
        {
            get
            {
                return __Material;
            }
            set
            {
                IsMaterialSet= true;
                __Material = value;
                OnPropertyChanged();
            }
        }
        public double Slope
        {
            get
            {
                return __Slope;
            }
            set
            {
                IsSlopeSet = true;
                __Slope = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Initialize without setting IsSet states.
        /// </summary>
        public PipeLoadedData():this(400, "B", 0)
        {

        }
        /// <summary>
        /// Initializes fields, without changing IsSet states.
        /// </summary>
        /// <param name="diameter"></param>
        /// <param name="material"></param>
        /// <param name="slope"></param>
        public PipeLoadedData(int diameter, string material, double slope)
        {
            __Diameter = diameter;
            __Material = material;
            __Slope = slope;
        }

    }
}
