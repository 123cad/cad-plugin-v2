﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Inventory.CAD.CadToInventories.Datas;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.CadToInventories.Data
{
    /// <summary>
    /// Adapter to a class from inventory - we want to use
    /// databinding (exact property names are required) from this class.
    /// </summary>
    class ShaftAdapter : INotifyPropertyChanged, IAdapter
    {
        public Shaft InventoryShaft { get; private set; }
        /// <summary>
        /// ObjectId of the object which represents the shaft.
        /// </summary>
        public ObjectId AssociatedObject { get; private set; } 

        /// <summary>
        /// Shaft position with height of shaft bottom.
        /// </summary>
        public MyUtilities.Geometry.Point3d CenterBottom
        {
            get
            {
                var p = InventoryShaft.CenterSMP;
                return new MyUtilities.Geometry.Point3d(p.X, p.Y, p.Z);
            }
            set
            {
                InventoryShaft.CenterSMP = new Inventory.CAD.Geometry.Point(value.X, value.Y, value.Z);
            }
        }

        public string Name
        {
            get
            {
                return InventoryShaft.Name;
            }
            set
            {
                if (InventoryShaft.Name != value)
                {
                    InventoryShaft.Name = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string ObjektArt
        {
            get
            {
                return InventoryShaft.ObjektArt.ToString();
            }
        }

        public string GeoobjektArt
        {
            get
            {
                return InventoryShaft.GeoobjektArt;

            }
            set
            {
                if (InventoryShaft.GeoobjektArt != value)
                {
                    InventoryShaft.GeoobjektArt = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public int Diameter
        {
            get
            {
                return InventoryShaft.Diameter;

            }
            set
            {
                if (InventoryShaft.Diameter != value)
                {
                    InventoryShaft.Diameter = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        public ShaftAdapter()
        {
            AssociatedObject = ObjectId.Null;
            InventoryShaft = new Shaft();
        }

        public IEnumerable<ObjectId> GetAssociatedObjectIds()
        {
            if (AssociatedObject != ObjectId.Null)
                yield return AssociatedObject;
        }
        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void SetCadEntity(ObjectId oid, MyUtilities.Geometry.Point3d pointSMP)
        {
            AssociatedObject = oid;

            InventoryShaft.CenterSMP = new Inventory.CAD.Geometry.Point(pointSMP.X, pointSMP.Y, pointSMP.Z);
        }
    }
}
