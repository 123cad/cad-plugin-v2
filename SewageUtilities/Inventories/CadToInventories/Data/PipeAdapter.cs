﻿using Inventory.CAD.CadToInventories.Datas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.CadToInventories.Data
{
    /// <summary>
    /// Adapter to a class from inventory - we want to use
    /// databinding (exact property names are required) from this class.
    /// </summary>
    class PipeAdapter:INotifyPropertyChanged, IAdapter
    {
        /// <summary>
        /// Object in drawing which is associated with this pipe.
        /// Line, Polyline, Polyline2d, Polyline3d.
        /// </summary>
        public ObjectId AssociatedCadObject { get; private set; }
        public Pipe InventoryPipe
        {
            get;private set;
        }
        public ShaftAdapter StartShaft { get; private set; }
        public ShaftAdapter EndShaft { get; private set; }
        public string Name
        {
            get
            {
                return InventoryPipe.Name;
            }
            set
            {
                if (InventoryPipe.Name != value)
                {
                    InventoryPipe.Name = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string ObjektArt
        {
            get
            {
                return this.InventoryPipe.ObjektArt.ToString();
            }
        }
        public string GeoobjektArt
        {
            get
            {
                return InventoryPipe.GeoobjektArt;

            }
            set
            {
                if (InventoryPipe.GeoobjektArt != value)
                {
                    InventoryPipe.GeoobjektArt = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public double Slope
        {
            get
            {
                return InventoryPipe.Slope;

            }
            set
            {
                if (InventoryPipe.Slope != value)
                {
                    InventoryPipe.Slope = value;
                    NotifyPropertyChanged();
                }
                InventoryPipe.Slope = value;
            }
        }
        public string Material
        {
            get
            {
                return InventoryPipe.Material;

            }
            set
            {
                if (InventoryPipe.Material != value)
                {
                    InventoryPipe.Material = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public int Diameter
        {
            get
            {
                return InventoryPipe.Diameter;

            }
            set
            {
                if (InventoryPipe.Diameter != value)
                {
                    InventoryPipe.Diameter = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        public PipeAdapter(ShaftAdapter start, ShaftAdapter end)
        {
            if (start == null || end == null)
                throw new NotSupportedException("pipe must have start and end shaft!");
            InventoryPipe = new Pipe();
            InventoryPipe.Name = start.Name;
            MyUtilities.Geometry.Point3d p = start.CenterBottom;
            InventoryPipe.Points.Add(new Inventory.CAD.Geometry.Point(p.X, p.Y, p.Z));
            p = end.CenterBottom;
            InventoryPipe.Points.Add(new Inventory.CAD.Geometry.Point(p.X, p.Y, p.Z));
            AssociatedCadObject = ObjectId.Null;
            StartShaft = start;
            EndShaft = end;
        }
        
        /// <summary>
        /// Invoke when shaft is updated.
        /// </summary>
        /// <param name="shaft"></param>
        public void UpdateShaft(ShaftAdapter shaft)
        {
            if (ReferenceEquals(StartShaft, shaft))
            {
                Name = shaft.Name;
                MyUtilities.Geometry.Point3d p = shaft.CenterBottom;
                InventoryPipe.Points[0] = new Inventory.CAD.Geometry.Point(p.X, p.Y, p.Z);
            }
            if (ReferenceEquals(EndShaft, shaft))
            {
                MyUtilities.Geometry.Point3d p = shaft.CenterBottom;
                InventoryPipe.Points[InventoryPipe.Points.Count - 1] = new Inventory.CAD.Geometry.Point(p.X, p.Y, p.Z);
            }
        }

        public IEnumerable<ObjectId> GetAssociatedObjectIds()
        {
            if (AssociatedCadObject != ObjectId.Null)
                yield return AssociatedCadObject;
            yield return StartShaft.AssociatedObject;
            yield return EndShaft.AssociatedObject;
        }

        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }



        /// <summary>
        /// Associates ObjectId with point to this pipe.
        /// </summary>
        /// <param name="oid"></param>
        /// <param name="points"></param>
        public void SetCadEntity(ObjectId oid, List<MyUtilities.Geometry.Point3d> points)
        {
            AssociatedCadObject = oid;
            InventoryPipe.Points.Clear();
            foreach(var p in points)
            {
                Inventory.CAD.Geometry.Point pt = new Inventory.CAD.Geometry.Point(p.X, p.Y, p.Z);
                InventoryPipe.Points.Add(pt);
            }
        }
    }
}
