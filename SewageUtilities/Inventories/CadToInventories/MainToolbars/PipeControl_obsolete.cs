﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CadPlugin.Inventories.CadToInventories.Data;
using System.Diagnostics;

namespace CadPlugin.Inventories.CadToInventories.MainToolbars
{
    public partial class PipeControl_obsolete : UserControl
    {
        private readonly MainToolbar form;
        private BindingSource pipeSource = new BindingSource();
        private BindingList<PipeAdapter> pipeList = new BindingList<PipeAdapter>();



        /// <summary>
        /// Indicates that selected pipe has been changed, and provides related ObjectId to selected pipe.
        /// </summary>
        internal event Action<PipeAdapter> PipeSelectionChanged;

        /// <summary>
        /// Currently selected pipe.
        /// </summary>
        internal PipeAdapter SelectedPipe { get; private set; }

        /// <summary>
        /// Pipe is not selected any more.
        /// </summary>
        public event Action CancelPipeSelection;

        private CadManager cadMgr;
        private DuplicateManager duplicatesMgr;
        /// <summary>
        /// true - no duplicates are found, data is valid.
        /// </summary>
        public bool IsDataValid => duplicatesMgr.IsValid;


        internal PipeControl_obsolete(MainToolbar toolbar, CadManager mgr)
        {
            form = toolbar;
            cadMgr = mgr;
            form.GlobalKeyUp += Form_GlobalKeyUp;
            InitializeComponent();
            dgvPipes.AutoGenerateColumns = false;
            dgvPipes.DataSource = new BindingSource(pipeList, null);
            duplicatesMgr = new DuplicateManager(dgvPipes, ColumnPipeName.Name);

            InitializeColumns();
            

            dgvPipes.DataError += (_, e) =>
            {
                errorProviderDgvCell.Clear();

                // This is enough to 
                if (e.Context.HasFlag(DataGridViewDataErrorContexts.LeaveControl))
                {
                    dgvPipes.RefreshEdit();
                    dgvPipes.EndEdit();
                    e.Cancel = false;
                    return;
                }

                var t = dgvPipes.EditingControl as TextBox;

                errorProviderDgvCell.SetError(t, "Invalid value!");
                // Move the icon so it is visible inside TextBox.
                errorProviderDgvCell.SetIconPadding(t, -20);
            };
            // To support DataError event - if user clicks inside dgv, no LeaveControl flag
            // is set, so we handle that here.
            dgvPipes.MouseDown += (_, e) =>
            {
                if (!dgvPipes.IsCurrentCellInEditMode)
                    return;
                var info = dgvPipes.HitTest(e.X, e.Y);
                if (info.ColumnIndex != dgvPipes.CurrentCell.ColumnIndex ||
                    info.RowIndex != dgvPipes.CurrentCell.RowIndex)
                {
                    dgvPipes.CommitEdit(DataGridViewDataErrorContexts.Commit | DataGridViewDataErrorContexts.LeaveControl);
                    
                }
                if (info.Type == DataGridViewHitTestType.None)
                {
                    // If no data error occurs, cell will still be in edit mode.
                    if (dgvPipes.IsCurrentCellInEditMode)
                        dgvPipes.EndEdit();
                    if (info.Type == DataGridViewHitTestType.None)
                        dgvPipes.CurrentCell = null;
                }
            };

            PipeAdapter currentAdapter = null;
            dgvPipes.CellMouseClick += (_, e)=>
            {
                if (e.Button != MouseButtons.Right)
                    return;
                
                if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    if (dgvPipes.CurrentCell != dgvPipes[e.ColumnIndex, e.RowIndex] ||
                        !dgvPipes.IsCurrentCellInEditMode)
                    {
                        dgvPipes.CurrentCell = dgvPipes[e.ColumnIndex, e.RowIndex];
                        currentAdapter = dgvPipes.CurrentRow.DataBoundItem as PipeAdapter;
                        var rec = dgvPipes.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                        contextMenuStripPipeData.Show(dgvPipes, new Point(e.Location.X + rec.X, e.Location.Y + rec.Y));

                    }
                }
            };
            toolStripMenuItemPipeData.Click += (_, e) =>
              {
                  var d = DataFetch.DataViews.PipeDataParserController.Start(cadMgr);
                  if (d != null)
                  {
                      if (d.IsDiameterSet)
                          currentAdapter.Diameter = d.Diameter;
                      if (d.IsMaterialSet)
                          currentAdapter.Material = d.Material;
                      if (d.IsSlopeSet)
                          currentAdapter.Slope = d.Slope;
                  }
                  currentAdapter = null;
              };

            dgvPipes.CellEndEdit += (_, e) =>
            {
                errorProviderDgvCell.Clear();
            };


            dgvPipes.CurrentCellChanged += DgvPipes_CurrentCellChanged;

            dgvPipes.LostFocus += (_, __) =>
            {
                if (!form.IsSelectionMode)
                    CancelPipeSelection?.Invoke();
            };

            dgvPipes.CellMouseDoubleClick += (_, e) =>
            {
                // Enter edit mode on click.
                if (e.Button == MouseButtons.Left)
                {
                    if (!dgvPipes.IsCurrentCellInEditMode)
                        dgvPipes.BeginEdit(false);
                }
            };
            dgvPipes.CellContentClick += (_, e) =>
            {
                DataGridViewColumn c = dgvPipes.Columns[e.ColumnIndex];
                if (c is DataGridViewButtonColumn && e.RowIndex >= 0)
                {
                    PipeAdapter sa = dgvPipes.Rows[e.RowIndex].DataBoundItem as PipeAdapter;
                    // Select new object for a pipe.
                    SelectNewEntity(sa);

                }
            };
            dgvPipes.RowsAdded += (_, e) =>
            {
                int columnIndex = ColumnPipeEntity.Index;
                for (int i = e.RowIndex; i < e.RowIndex + e.RowCount; i++)
                {
                    dgvPipes[columnIndex, i].Value = "...";
                    dgvPipes[columnIndex, i].ToolTipText = "Select new entity for shaft";
                };
            };
        }
        internal List<PipeAdapter> GetAllPipes()
        {
            return pipeList.ToList();
        }
        internal void CreateNewPipe(ShaftAdapter start, ShaftAdapter end)
        {
            pipeList.Add(new PipeAdapter(start, end));
            var row = dgvPipes.Rows[dgvPipes.RowCount - 1];
            row.Cells[ColumnPipeEntity.Index].Value = "Select";
        }

        private void DgvPipes_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dgvPipes.CurrentCell == null)
            {
                SelectedPipe = null;
                CancelPipeSelection?.Invoke();
            }
            else
            {
                PipeAdapter pd = dgvPipes.CurrentRow.DataBoundItem as PipeAdapter;
                SelectedPipe = pd;
                PipeSelectionChanged?.Invoke(pd);
            }
        }

        private void Form_GlobalKeyUp(Keys obj)
        {
            switch (obj)
            {
                case Keys.Escape:
                    if (dgvPipes.IsCurrentCellInEditMode)
                    {
                        dgvPipes.CancelEdit();
                        dgvPipes.EndEdit();
                    }
                    break;
                case Keys.Enter:
                    if (dgvPipes.IsCurrentCellInEditMode)
                    {                        
                        dgvPipes.EndEdit();
                    }
                    break;
            }
        }
        /// <summary>
        /// Call this when shaft is updated, and associated pipes need to be updated as well.
        /// </summary>
        /// <param name="sa"></param>
        internal void UpdateShaft(ShaftAdapter sa)
        {
            foreach (var v in pipeList)
                v.UpdateShaft(sa);
        }
        /// <summary>
        /// Deletes all pipes with this shaft.
        /// </summary>
        /// <param name="sa"></param>
        internal void RemoveShaft(ShaftAdapter sa)
        {
            List<PipeAdapter> removeList = new List<PipeAdapter>();
            foreach(var p in pipeList)
            {
                if (ReferenceEquals(p.StartShaft, sa) ||
                    ReferenceEquals(p.EndShaft, sa))
                    removeList.Add(p);
            }
            foreach (var v in removeList)
                pipeList.Remove(v);
        }
        /// <summary>
        /// Assigns new pipe entity.
        /// </summary>
        /// <param name="pa"></param>
        private void SelectNewEntity(PipeAdapter pa)
        {
            form.Visible = false;
            cadMgr.AssignPipeEntity(pa);
            form.Visible = true;
        }
        private void InitializeColumns()
        {
            DataGridView pipes = dgvPipes;

            DataGridViewTextBoxColumn columnText;
            DataGridViewComboBoxColumn columnCombo;

            columnText = pipes.Columns[nameof(ColumnPipeName)] as DataGridViewTextBoxColumn;
            Debug.Assert(columnText != null);
            columnText.DataPropertyName = nameof(Data.PipeAdapter.Name);// "Name";

            columnCombo = pipes.Columns[nameof(ColumnPipeObjektArt)] as DataGridViewComboBoxColumn;
            Debug.Assert(columnCombo != null);
            columnCombo.DataSource = Isybau2015.ReferenceTables.Common.Objektart.Get().Values;
            columnCombo.DisplayMember = nameof(Isybau2015.ReferenceTables.ReferencePair.Value);// "Value";
            columnCombo.ValueMember = nameof(Isybau2015.ReferenceTables.ReferencePair.Key); //"Key";
            columnCombo.DataPropertyName = nameof(Data.PipeAdapter.ObjektArt);// "ObjektArt";
            columnCombo.ReadOnly = true;


            columnCombo = pipes.Columns[nameof(ColumnPipeGeoobjektArt)] as DataGridViewComboBoxColumn;
            Debug.Assert(columnCombo != null);
            // Pipe objects can have only specific GeoObjektArt
            HashSet<string> geoObjektArts = new HashSet<string>();
            geoObjektArts.Add("4");
            geoObjektArts.Add("5");
            geoObjektArts.Add("6");
            geoObjektArts.Add("7");
            columnCombo.DataSource = Isybau2015.ReferenceTables.Common.GeoobjektArt.Get().Values.Where(x => geoObjektArts.Contains(x.Key)).ToList();
            columnCombo.DisplayMember = nameof(Isybau2015.ReferenceTables.ReferencePair.Value);// "Value";
            columnCombo.ValueMember = nameof(Isybau2015.ReferenceTables.ReferencePair.Key); //"Key";
            columnCombo.DataPropertyName = nameof(Data.PipeAdapter.GeoobjektArt);// "GeoobjektArt";

            columnText = pipes.Columns[nameof(ColumnPipeSlope)] as DataGridViewTextBoxColumn;
            Debug.Assert(columnText != null);
            columnText.DataPropertyName = nameof(Data.PipeAdapter.Slope);// "Slope";
            columnText.HeaderText = "Slope (‰)";
            columnText.InheritedStyle.Format = "N3";

            columnText = pipes.Columns[nameof(ColumnPipeMaterial)] as DataGridViewTextBoxColumn;
            Debug.Assert(columnText != null);
            columnText.DataPropertyName = nameof(Data.PipeAdapter.Material);// "Material";


            columnText = pipes.Columns[nameof(ColumnPipeDiameter)] as DataGridViewTextBoxColumn;
            Debug.Assert(columnText != null);
            columnText.DataPropertyName = nameof(Data.PipeAdapter.Diameter);// "Diameter";
            columnText.HeaderText = "Diameter (mm)";
            columnText.InheritedStyle.Format = "N0";


        }

        private void buttonRemoveKante_Click(object sender, EventArgs e)
        {
            if (dgvPipes.CurrentCell != null)
            {
                PipeAdapter pa = dgvPipes.CurrentRow.DataBoundItem as PipeAdapter;
                pipeList.Remove(pa);
            }
        }

        private void toolStripMenuItemPipeData_Click(object sender, EventArgs e)
        {

        }
    }
}
