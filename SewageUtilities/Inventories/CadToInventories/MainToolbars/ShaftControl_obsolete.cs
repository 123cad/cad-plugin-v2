﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CadPlugin.Inventories.CadToInventories.Data;
using System.Diagnostics;

namespace CadPlugin.Inventories.CadToInventories.MainToolbars
{
    public partial class ShaftControl_obsolete : UserControl
    {
        private readonly MainToolbar form;
        private BindingSource shaftSource = new BindingSource();
        private BindingList<ShaftAdapter> shaftList = new BindingList<ShaftAdapter>();

        /// <summary>
        /// Called when shafts have been selected and pipe should be created.
        /// </summary>
        internal event Action<ShaftAdapter, ShaftAdapter> CreatePipe;

        /// <summary>
        /// Occurs when shaft is updated, and provides that shaft.
        /// </summary>
        internal event Action<ShaftAdapter> ShaftUpdated;
        /// <summary>
        /// Occurs when shaft has been removed.
        /// </summary>
        internal event Action<ShaftAdapter> ShaftRemoved;

        /// <summary>
        /// Indicates that selected pipe has been changed, and provides related ObjectId to selected pipe.
        /// </summary>
        internal event Action<ShaftAdapter> ShaftSelectionChanged;

        /// <summary>
        /// Currently selected pipe.
        /// </summary>
        internal ShaftAdapter SelectedShaft { get; private set; }

        /// <summary>
        /// Shaft is not selected any more.
        /// </summary>
        public event Action CancelShaftSelection;

        /// <summary>
        /// Shaft that is selected to be start one.
        /// </summary>
        private ShaftAdapter SelectedStartShaft;
        /// <summary>
        /// Shaft that is selected to be end one.
        /// </summary>
        private ShaftAdapter SelectedEndShaft;

        private CadManager cadMgr;
        private DuplicateManager duplicatesMgr;
        /// <summary>
        /// true - no duplicates are found, data is valid.
        /// </summary>
        public bool IsDataValid => duplicatesMgr.IsValid;

        internal ShaftControl_obsolete(MainToolbar toolbar, CadManager mgr)
        {
            form = toolbar;
            cadMgr = mgr;
            form.GlobalKeyUp += Form_GlobalKeyUp;
            InitializeComponent();
            labelMessage.Text = "";

            dgvShafts.AutoGenerateColumns = false;
            dgvShafts.DataSource = new BindingSource(shaftList, null);
            duplicatesMgr = new DuplicateManager(dgvShafts, ColumnShaftName.Name); 

            InitializeColumns();

            dgvShafts.DataError += (_, e) =>
            {
                errorProviderDgvCell.Clear();

                // This is enough to 
                if (e.Context.HasFlag(DataGridViewDataErrorContexts.LeaveControl))
                {
                    dgvShafts.RefreshEdit();
                    dgvShafts.EndEdit();
                    e.Cancel = false;
                    return;
                }

                var t = dgvShafts.EditingControl as TextBox;

                errorProviderDgvCell.SetError(t, "Invalid value! Press Esc to cancel.");
                // Move the icon so it is visible inside TextBox.
                errorProviderDgvCell.SetIconPadding(t, -20);
            };
            // To support DataError event - if user clicks inside dgv, no LeaveControl flag
            // is set, so we handle that here.
            dgvShafts.MouseDown += (_, e) =>
            {

                if (!dgvShafts.IsCurrentCellInEditMode)
                    return;
                var info = dgvShafts.HitTest(e.X, e.Y);
                if (info.ColumnIndex != dgvShafts.CurrentCell.ColumnIndex ||
                    info.RowIndex != dgvShafts.CurrentCell.RowIndex)
                {
                    dgvShafts.CommitEdit(DataGridViewDataErrorContexts.Commit | DataGridViewDataErrorContexts.LeaveControl);
                }
                if (info.Type == DataGridViewHitTestType.None)
                {
                    // If no data error occurs, cell will still be in edit mode.
                    if (dgvShafts.IsCurrentCellInEditMode)
                        dgvShafts.EndEdit();
                    if (info.Type == DataGridViewHitTestType.None)
                        dgvShafts.CurrentCell = null;
                }
            };

            dgvShafts.CellEndEdit += (_, e) =>
            {
                errorProviderDgvCell.Clear();
            };

            dgvShafts.CurrentCellChanged += DgvShafts_CurrentCellChanged;

            dgvShafts.CellMouseDoubleClick += (_, e) =>
            {
                // Enter edit mode on click.
                if (e.Button == MouseButtons.Left)
                {
                    if (!dgvShafts.IsCurrentCellInEditMode)
                        dgvShafts.BeginEdit(false);
                }
            };
            dgvShafts.CellContentClick += (_, e) =>
            {
                DataGridViewColumn c = dgvShafts.Columns[e.ColumnIndex];
                if (c is DataGridViewButtonColumn && e.RowIndex >= 0)
                {
                    ShaftAdapter sa = dgvShafts.Rows[e.RowIndex].DataBoundItem as ShaftAdapter;
                    // Select new object for a shaft
                    form.Visible = false;
                    if (cadMgr.AssignShaftEntity(sa))
                    {
                        ShaftUpdated?.Invoke(sa);
                    }
                    form.Visible = true;
                }
                else if (c is DataGridViewComboBoxColumn)
                {
                    dgvShafts.BeginEdit(false);
                }
            };

            ShaftAdapter currentAdapter = null;
            dgvShafts.CellMouseClick += (_, e) =>
            {
                if (e.Button != MouseButtons.Right)
                    return;

                if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    if (dgvShafts.CurrentCell != dgvShafts[e.ColumnIndex, e.RowIndex] ||
                        !dgvShafts.IsCurrentCellInEditMode)
                    {
                        dgvShafts.CurrentCell = dgvShafts[e.ColumnIndex, e.RowIndex];
                        currentAdapter = dgvShafts.CurrentRow.DataBoundItem as ShaftAdapter;
                        var rec = dgvShafts.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                        contextMenuStripShaft.Show(dgvShafts, new Point(e.Location.X + rec.X, e.Location.Y + rec.Y));

                    }
                }
            };
            selectShaftNameToolStripMenuItem.Click += (_, e) =>
            {
                // Since this dialog is started as modeless, we need manually to set Visible.
                form.Visible = false;
                string s = cadMgr.GetTextString();
                form.Visible = true;
                currentAdapter.Name = s;
                currentAdapter = null;
            };

            dgvShafts.LostFocus += (_, __) =>
            {
                if (!form.IsSelectionMode)
                    CancelShaftSelection?.Invoke();
            };
            dgvShafts.RowsAdded += (_, e) =>
            {
                int columnIndex = ColumnShaftEntity.Index;
                for (int i = e.RowIndex; i < e.RowIndex + e.RowCount; i++)
                {
                    dgvShafts[columnIndex, i].Value = "...";
                    dgvShafts[columnIndex, i].ToolTipText = "Select new entity for shaft";
                };
            };
            // Needed, so user can close the form (cancel operation) with invalid value.
            /*form.FormClosing += (_, e) =>
            {
                if (dgvShafts.IsCurrentCellInEditMode)
                {
                    dgvShafts.CancelEdit();
                    dgvShafts.EndEdit();
                    // It has been set to true - suppose DGV set that.
                    e.Cancel = false;
                }
            };*/
        }

        private void buttonSetStartShaft_Click(object sender, EventArgs e)
        {
            errorProviderDgvCell.Clear();
            ShaftAdapter sa = GetCurrent();
            if (sa == null)
                return;
            if (string.IsNullOrEmpty(sa.Name))
            {
                MessageBox.Show("Shaft must have a name!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            SelectedStartShaft = sa;
            textBoxStartShaft.Text = sa.Name;
            
        }

        private void buttonSetEndShaft_Click(object sender, EventArgs e)
        {
            errorProviderDgvCell.Clear();
            ShaftAdapter sa = GetCurrent();
            if (sa == null)
                return;
            if (string.IsNullOrEmpty(sa.Name))
            {
                MessageBox.Show("Shaft must have a name!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            SelectedEndShaft = sa;
            textBoxEndShaft.Text = sa.Name;
        }
        private ShaftAdapter GetCurrent()
        {
            if (dgvShafts.CurrentCell == null)
                return null;
            if (dgvShafts.CurrentCell.RowIndex == -1)
                return null;
            return dgvShafts.CurrentRow.DataBoundItem as ShaftAdapter;
        }

        private void DgvShafts_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dgvShafts.CurrentCell == null)
            {
                SelectedShaft = null;
                CancelShaftSelection?.Invoke();
            }
            else
            {
                ShaftAdapter pd = dgvShafts.CurrentRow.DataBoundItem as ShaftAdapter;
                SelectedShaft = pd;
                ShaftSelectionChanged?.Invoke(pd);
            }
        }

        internal List<ShaftAdapter> GetAllShafts()
        {
            return shaftList.ToList();
        }
        private void buttonCreatePipe_Click(object sender, EventArgs e)
        {
            bool allValid = true;
            if (SelectedStartShaft == null)
            {
                allValid = false;
                errorProviderDgvCell.SetError(textBoxStartShaft, "Not set!");
            }
            if (SelectedEndShaft == null)
            {
                allValid = false;
                errorProviderDgvCell.SetError(textBoxEndShaft, "Not set!");
            }
            if (!allValid)
                return;
            CreatePipe?.Invoke(SelectedStartShaft, SelectedEndShaft);

            SelectedStartShaft = null;
            SelectedEndShaft = null;
            textBoxStartShaft.Text = "";
            textBoxEndShaft.Text = "";

            string pipeName = "";
            labelMessage.Text = $"Pipe {pipeName} created.";
            Task.Delay(1500).ContinueWith((d) =>
            {
                labelMessage.Invoke((Action)delegate { labelMessage.Text = ""; });
            });
        }

        private void Form_GlobalKeyUp(Keys obj)
        {
            switch (obj)
            {
                case Keys.Escape:
                    if (dgvShafts.IsCurrentCellInEditMode)
                    {
                        // Revert to old value (doesn't end CurrentCellEditMode)
                        dgvShafts.CancelEdit();
                        dgvShafts.EndEdit();
                    }
                    break;
                case Keys.Enter:
                    if (dgvShafts.IsCurrentCellInEditMode)
                    {
                        // Try to end editing mode. 
                        dgvShafts.EndEdit();
                    }
                    break;
            }
        }

        private void InitializeColumns()
        {
            DataGridView shafts = dgvShafts;

            DataGridViewTextBoxColumn columnText;
            DataGridViewComboBoxColumn columnCombo;

            columnText = shafts.Columns[nameof(ColumnShaftName)] as DataGridViewTextBoxColumn;
            Debug.Assert(columnText != null);
            columnText.DataPropertyName = nameof(Data.ShaftAdapter.Name);// "Name";

            columnCombo = shafts.Columns[nameof(ColumnShaftObjektArt)] as DataGridViewComboBoxColumn;
            Debug.Assert(columnCombo != null);
            columnCombo.DataSource = Isybau2015.ReferenceTables.Common.Objektart.Get().Values;
            columnCombo.DisplayMember = nameof(Isybau2015.ReferenceTables.ReferencePair.Value);// "Value";
            columnCombo.ValueMember = nameof(Isybau2015.ReferenceTables.ReferencePair.Key); //"Key";
            columnCombo.DataPropertyName = nameof(Data.ShaftAdapter.ObjektArt);// "ObjektArt";
            columnCombo.ReadOnly = true;

            columnCombo = shafts.Columns[nameof(ColumnShaftGeoobjektArt)] as DataGridViewComboBoxColumn;
            Debug.Assert(columnCombo != null);
            // Pipe objects can have only specific GeoObjektArt
            HashSet<string> geoObjektArts = new HashSet<string>();
            geoObjektArts.Add("1");
            geoObjektArts.Add("2");
            geoObjektArts.Add("3");
            geoObjektArts.Add("8");
            geoObjektArts.Add("9");
            geoObjektArts.Add("10");
            geoObjektArts.Add("11");
            geoObjektArts.Add("12");
            geoObjektArts.Add("13");
            columnCombo.DataSource = Isybau2015.ReferenceTables.Common.GeoobjektArt.Get().Values.Where(x => geoObjektArts.Contains(x.Key)).ToList();
            columnCombo.DisplayMember = nameof(Isybau2015.ReferenceTables.ReferencePair.Value);// "Value";
            columnCombo.ValueMember = nameof(Isybau2015.ReferenceTables.ReferencePair.Key); //"Key";
            columnCombo.DataPropertyName = nameof(Data.ShaftAdapter.GeoobjektArt);// "GeoobjektArt";

            columnText = shafts.Columns[nameof(ColumnShaftDiameter)] as DataGridViewTextBoxColumn;
            Debug.Assert(columnText != null);
            columnText.DataPropertyName = nameof(Data.ShaftAdapter.Diameter);// "Diameter";
            columnText.HeaderText = "Diameter (mm)";
            columnText.InheritedStyle.Format = "N0";
        }

        private void buttonAddKnoten_Click(object sender, EventArgs e)
        {
            ShaftAdapter sa = new ShaftAdapter();
            form.Visible = false;
            if (cadMgr.AssignShaftEntity(sa))
            {
                shaftList.Add(sa);
                var row = dgvShafts.Rows[dgvShafts.RowCount - 1];
                row.Cells[ColumnShaftEntity.Index].Value = "Select";

                int currentColumn = 0;
                if (dgvShafts.CurrentCell != null)
                    currentColumn = dgvShafts.CurrentCell.ColumnIndex;
                dgvShafts.CurrentCell = null;// dgvShafts[currentColumn, row.Index];
            }
            form.Visible = true;

        }

        private void buttonRemoveKnoten_Click(object sender, EventArgs e)
        {
            ShaftAdapter sa = GetCurrent();
            if (sa != null)
            {
                ShaftRemoved?.Invoke(sa);

                int currentRow = dgvShafts.CurrentCell.RowIndex;
                int currentColumn = dgvShafts.CurrentCell.ColumnIndex;
                shaftList.Remove(sa);
                if (shaftList.Count > 0)
                {
                    if (currentRow > 0)
                    {
                        if (currentRow >= shaftList.Count)
                            currentRow--;
                    }
                    dgvShafts.CurrentCell = dgvShafts[currentColumn, currentRow];
                }
            }
        }
    }
}
