﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.Inventories.CadToInventories.MainToolbars
{
    /// <summary>
    /// Dynamical tracker for duplicate names.
    /// Automatically sets background of duplicated names.
    /// When user enters to cell edit mode, text he is typing is dynamically checked against text in all other cells.
    /// ErrorProvider error is displayed in case name exists.
    /// </summary>
    class DuplicateManager
    {
        private DataGridView dgv { get; set; }
        /// <summary>
        /// Column name which holds the name of the object - this column is checked for duplicates.
        /// </summary>
        private string columnName { get; set; }
        /// <summary>
        /// Index which of column with columnName.
        /// </summary>
        private int columnIndex { get; set; }
        private Color cellDefaultBackgroundColor { get; set; }
        private Color cellErrorBackColor { get; set; }
        /// <summary>
        /// If no duplicates or empty names found, returns true.
        /// </summary>
        public bool IsValid { get; private set; }
        private ErrorProvider eProvider { get; set; }
        /// <summary>
        /// Control which is currently used and has focus to edit cell in name column.
        /// </summary>
        private DataGridViewTextBoxEditingControl textControl { get; set; } = null;
        /// <summary>
        /// All used names in cells other than currently edited one (in name column).
        /// </summary>
        private HashSet<string> currentNames { get; set; } = new HashSet<string>();
        public DuplicateManager(DataGridView view, string columnName)
        {
            dgv = view;
            this.columnName = columnName;
            columnIndex = view.Columns[columnName].Index;
            cellDefaultBackgroundColor = dgv.DefaultCellStyle.BackColor;
            cellErrorBackColor = Color.FromArgb(255, 201, 201);

            eProvider = new ErrorProvider();

            dgv.RowsAdded += Dgv_RowsAdded;
            dgv.RowsRemoved += Dgv_RowsRemoved;
            dgv.CellValueChanged += Dgv_CellValueChanged;

            dgv.CellBeginEdit += Dgv_CellBeginEdit;
            dgv.CellEndEdit += Dgv_CellEndEdit;

            dgv.EditingControlShowing += Dgv_EditingControlShowing;
        }

        private void Dgv_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            textControl = e.Control as DataGridViewTextBoxEditingControl;
            if (textControl == null)
                return;
            textControl.KeyUp += TextControl_KeyUp;
        }

        private void TextControl_KeyUp(object sender, KeyEventArgs e)
        {
            string s = textControl.EditingControlFormattedValue as string;
            eProvider.Clear();
            if (currentNames.Contains(s))
            {
                eProvider.SetIconPadding(textControl, -20);
                eProvider.SetError(textControl, "Name is taken.");
            }
        }

        private void Dgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            textControl.KeyUp -= TextControl_KeyUp;
            eProvider.Clear();
            currentNames.Clear();
            CheckNames();
        }

        private void Dgv_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            dgv[e.ColumnIndex, e.RowIndex].Style.BackColor = cellDefaultBackgroundColor;
            currentNames.Clear();
            foreach (DataGridViewRow row in dgv.Rows)
            {
                if (row.Index == e.RowIndex)
                    continue;
                string v = row.Cells[columnIndex].Value as string;
                if (!currentNames.Contains(v))
                    currentNames.Add(v);
            }
        }

        private void Dgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            CheckNames();
        }

        private void Dgv_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            CheckNames();
        }

        private void Dgv_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            CheckNames();
        }
        private void CheckNames()
        {
            Dictionary<string, List<DataGridViewCell>> dictionary = new Dictionary<string, List<DataGridViewCell>>();
            int count = dgv.Rows.Count;
            for (int i = 0; i < count; i++)
            {
                DataGridViewTextBoxCell cell = dgv[columnIndex, i] as DataGridViewTextBoxCell;
                string s = cell.Value as string;
                s = s ?? "";
                s = s.Trim();
                if (!dictionary.ContainsKey(s))
                    dictionary.Add(s, new List<DataGridViewCell>());
                dictionary[s].Add(cell);
                cell.Style.BackColor = cellDefaultBackgroundColor;
                cell.ToolTipText = null;
            }

            foreach(var pair in dictionary)
            {
                if (pair.Key == "" || pair.Value.Count > 1)
                {
                    foreach (var c in pair.Value)
                    {
                        c.Style.BackColor = cellErrorBackColor;
                        if (pair.Key == "")
                            c.ToolTipText = "Name not entered!";
                        else
                            c.ToolTipText = "Name is used more than once.";
                    }
                }
                pair.Value.Clear();
            }
            dictionary.Clear();

        }
    }
}
