﻿namespace CadPlugin.Inventories.CadToInventories.MainToolbars
{
    partial class ShaftControl_obsolete
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonCreatePipe = new System.Windows.Forms.Button();
            this.buttonSetEndShaft = new System.Windows.Forms.Button();
            this.buttonSetStartShaft = new System.Windows.Forms.Button();
            this.textBoxEndShaft = new System.Windows.Forms.TextBox();
            this.textBoxStartShaft = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvShafts = new System.Windows.Forms.DataGridView();
            this.ColumnShaftEntity = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ColumnShaftName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnShaftObjektArt = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColumnShaftGeoobjektArt = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColumnShaftDiameter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnShaftEmpty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errorProviderDgvCell = new System.Windows.Forms.ErrorProvider(this.components);
            this.buttonRemoveKnoten = new System.Windows.Forms.Button();
            this.buttonAddKnoten = new System.Windows.Forms.Button();
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStripShaft = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.selectShaftNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelMessage = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShafts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderDgvCell)).BeginInit();
            this.contextMenuStripShaft.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.labelMessage);
            this.groupBox1.Controls.Add(this.buttonCreatePipe);
            this.groupBox1.Controls.Add(this.buttonSetEndShaft);
            this.groupBox1.Controls.Add(this.buttonSetStartShaft);
            this.groupBox1.Controls.Add(this.textBoxEndShaft);
            this.groupBox1.Controls.Add(this.textBoxStartShaft);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(0, 222);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(404, 113);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Create Pipe";
            // 
            // buttonCreatePipe
            // 
            this.buttonCreatePipe.Location = new System.Drawing.Point(101, 84);
            this.buttonCreatePipe.Name = "buttonCreatePipe";
            this.buttonCreatePipe.Size = new System.Drawing.Size(75, 23);
            this.buttonCreatePipe.TabIndex = 6;
            this.buttonCreatePipe.Text = "Create pipe";
            this.buttonCreatePipe.UseVisualStyleBackColor = true;
            this.buttonCreatePipe.Click += new System.EventHandler(this.buttonCreatePipe_Click);
            // 
            // buttonSetEndShaft
            // 
            this.buttonSetEndShaft.Location = new System.Drawing.Point(217, 55);
            this.buttonSetEndShaft.Name = "buttonSetEndShaft";
            this.buttonSetEndShaft.Size = new System.Drawing.Size(123, 23);
            this.buttonSetEndShaft.TabIndex = 5;
            this.buttonSetEndShaft.Text = "Current as end shaft";
            this.buttonSetEndShaft.UseVisualStyleBackColor = true;
            this.buttonSetEndShaft.Click += new System.EventHandler(this.buttonSetEndShaft_Click);
            // 
            // buttonSetStartShaft
            // 
            this.buttonSetStartShaft.Location = new System.Drawing.Point(217, 26);
            this.buttonSetStartShaft.Name = "buttonSetStartShaft";
            this.buttonSetStartShaft.Size = new System.Drawing.Size(123, 23);
            this.buttonSetStartShaft.TabIndex = 4;
            this.buttonSetStartShaft.Text = "Current as start shaft";
            this.buttonSetStartShaft.UseVisualStyleBackColor = true;
            this.buttonSetStartShaft.Click += new System.EventHandler(this.buttonSetStartShaft_Click);
            // 
            // textBoxEndShaft
            // 
            this.textBoxEndShaft.Location = new System.Drawing.Point(76, 58);
            this.textBoxEndShaft.Name = "textBoxEndShaft";
            this.textBoxEndShaft.ReadOnly = true;
            this.textBoxEndShaft.Size = new System.Drawing.Size(100, 20);
            this.textBoxEndShaft.TabIndex = 3;
            // 
            // textBoxStartShaft
            // 
            this.textBoxStartShaft.Location = new System.Drawing.Point(76, 27);
            this.textBoxStartShaft.Name = "textBoxStartShaft";
            this.textBoxStartShaft.ReadOnly = true;
            this.textBoxStartShaft.Size = new System.Drawing.Size(100, 20);
            this.textBoxStartShaft.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "End shaft";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Start shaft";
            // 
            // dgvShafts
            // 
            this.dgvShafts.AllowUserToAddRows = false;
            this.dgvShafts.AllowUserToDeleteRows = false;
            this.dgvShafts.AllowUserToResizeRows = false;
            this.dgvShafts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvShafts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvShafts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnShaftEntity,
            this.ColumnShaftName,
            this.ColumnShaftObjektArt,
            this.ColumnShaftGeoobjektArt,
            this.ColumnShaftDiameter,
            this.ColumnShaftEmpty});
            this.dgvShafts.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvShafts.Location = new System.Drawing.Point(0, 0);
            this.dgvShafts.MultiSelect = false;
            this.dgvShafts.Name = "dgvShafts";
            this.dgvShafts.RowHeadersVisible = false;
            this.dgvShafts.Size = new System.Drawing.Size(691, 204);
            this.dgvShafts.TabIndex = 2;
            // 
            // ColumnShaftEntity
            // 
            this.ColumnShaftEntity.HeaderText = "Entity";
            this.ColumnShaftEntity.Name = "ColumnShaftEntity";
            this.ColumnShaftEntity.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnShaftEntity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColumnShaftEntity.Text = "Update";
            this.ColumnShaftEntity.Width = 55;
            // 
            // ColumnShaftName
            // 
            this.ColumnShaftName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnShaftName.FillWeight = 40F;
            this.ColumnShaftName.HeaderText = "Bezeichnung";
            this.ColumnShaftName.MinimumWidth = 65;
            this.ColumnShaftName.Name = "ColumnShaftName";
            // 
            // ColumnShaftObjektArt
            // 
            this.ColumnShaftObjektArt.HeaderText = "ObjektArt";
            this.ColumnShaftObjektArt.Name = "ColumnShaftObjektArt";
            this.ColumnShaftObjektArt.ReadOnly = true;
            // 
            // ColumnShaftGeoobjektArt
            // 
            this.ColumnShaftGeoobjektArt.HeaderText = "GeoobjektArt";
            this.ColumnShaftGeoobjektArt.Name = "ColumnShaftGeoobjektArt";
            this.ColumnShaftGeoobjektArt.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnShaftGeoobjektArt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ColumnShaftDiameter
            // 
            this.ColumnShaftDiameter.HeaderText = "Diameter (mm)";
            this.ColumnShaftDiameter.Name = "ColumnShaftDiameter";
            // 
            // ColumnShaftEmpty
            // 
            this.ColumnShaftEmpty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnShaftEmpty.HeaderText = "";
            this.ColumnShaftEmpty.Name = "ColumnShaftEmpty";
            this.ColumnShaftEmpty.ReadOnly = true;
            // 
            // errorProviderDgvCell
            // 
            this.errorProviderDgvCell.ContainerControl = this;
            // 
            // buttonRemoveKnoten
            // 
            this.buttonRemoveKnoten.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRemoveKnoten.Location = new System.Drawing.Point(697, 3);
            this.buttonRemoveKnoten.Name = "buttonRemoveKnoten";
            this.buttonRemoveKnoten.Size = new System.Drawing.Size(23, 23);
            this.buttonRemoveKnoten.TabIndex = 6;
            this.buttonRemoveKnoten.Text = "-";
            this.buttonRemoveKnoten.UseVisualStyleBackColor = true;
            this.buttonRemoveKnoten.Click += new System.EventHandler(this.buttonRemoveKnoten_Click);
            // 
            // buttonAddKnoten
            // 
            this.buttonAddKnoten.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddKnoten.Location = new System.Drawing.Point(697, 32);
            this.buttonAddKnoten.Name = "buttonAddKnoten";
            this.buttonAddKnoten.Size = new System.Drawing.Size(23, 23);
            this.buttonAddKnoten.TabIndex = 7;
            this.buttonAddKnoten.Text = "+";
            this.buttonAddKnoten.UseVisualStyleBackColor = true;
            this.buttonAddKnoten.Click += new System.EventHandler(this.buttonAddKnoten_Click);
            // 
            // dataGridViewButtonColumn1
            // 
            this.dataGridViewButtonColumn1.HeaderText = "Entity";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewButtonColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewButtonColumn1.Text = "Update";
            this.dataGridViewButtonColumn1.Width = 55;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.FillWeight = 40F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Bezeichnung";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 65;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.HeaderText = "ObjektArt";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.HeaderText = "GeoobjektArt";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Diameter (mm)";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.HeaderText = "";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // contextMenuStripShaft
            // 
            this.contextMenuStripShaft.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectShaftNameToolStripMenuItem});
            this.contextMenuStripShaft.Name = "contextMenuStrip1";
            this.contextMenuStripShaft.Size = new System.Drawing.Size(168, 26);
            // 
            // selectShaftNameToolStripMenuItem
            // 
            this.selectShaftNameToolStripMenuItem.Name = "selectShaftNameToolStripMenuItem";
            this.selectShaftNameToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.selectShaftNameToolStripMenuItem.Text = "Select shaft name";
            // 
            // labelMessage
            // 
            this.labelMessage.AutoSize = true;
            this.labelMessage.ForeColor = System.Drawing.Color.Green;
            this.labelMessage.Location = new System.Drawing.Point(192, 89);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(37, 13);
            this.labelMessage.TabIndex = 7;
            this.labelMessage.Text = "[code]";
            // 
            // ShaftControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonRemoveKnoten);
            this.Controls.Add(this.buttonAddKnoten);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgvShafts);
            this.Name = "ShaftControl";
            this.Size = new System.Drawing.Size(723, 335);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShafts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderDgvCell)).EndInit();
            this.contextMenuStripShaft.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonCreatePipe;
        private System.Windows.Forms.Button buttonSetEndShaft;
        private System.Windows.Forms.Button buttonSetStartShaft;
        private System.Windows.Forms.TextBox textBoxEndShaft;
        private System.Windows.Forms.TextBox textBoxStartShaft;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvShafts;
        private System.Windows.Forms.ErrorProvider errorProviderDgvCell;
        private System.Windows.Forms.Button buttonRemoveKnoten;
        private System.Windows.Forms.Button buttonAddKnoten;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewButtonColumn ColumnShaftEntity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnShaftName;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnShaftObjektArt;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnShaftGeoobjektArt;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnShaftDiameter;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnShaftEmpty;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripShaft;
        private System.Windows.Forms.ToolStripMenuItem selectShaftNameToolStripMenuItem;
        private System.Windows.Forms.Label labelMessage;
    }
}
