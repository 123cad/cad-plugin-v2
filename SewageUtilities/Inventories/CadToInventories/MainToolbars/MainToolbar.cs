﻿using CadPlugin.Inventories.CadToInventories.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPlugin.Inventories.CadToInventories.MainToolbars
{
    public partial class MainToolbar : Form
    {
        class MyMessageFilter : IMessageFilter
        {
            public const int WM_KEYUP = 0x0101;
            public const int WM_LBUTTONDOWN = 0x0201;

            public event Action EscapeClicked;
            private void OnEscapeClicked()
            {
                EscapeClicked?.Invoke();
            }
            public bool PreFilterMessage(ref Message m)
            {
                if (m.Msg == WM_KEYUP && m.WParam.ToInt32() == (int)Keys.Escape)
                {
                    OnEscapeClicked();
                }
                    return false;
            }
        }

        private static MainToolbar Instance = null;
        public bool FormAlreadyOpened { get; private set; }

        private CadManager cadMgr = new CadManager();
        
        /// <summary>
        /// Fired when data is ready to be processed (not fired if user cancels).
        /// In this way because we want to run Inventory after form is closed.
        /// </summary>
        public event Action<Inventory.CAD.CadToInventories.Datas.DataManager> DataReady;

        /// <summary>
        /// Used this because of some problems.
        /// </summary>
        public event Action<Keys> GlobalKeyUp;
        public static MainToolbar GetForm()
        {
            if (Instance == null)
                new MainToolbar();
            else
            {
                if (!Instance.Visible)
                    Instance.Visible = true;
            }
            return Instance;
        }
        /// <summary>
        /// When in selection mode, form is invisible.
        /// LostFocus event is fired, but has to be ignored (currently selected
        /// object is highlighted on the drawing, and has to remain like that - no 
        /// selected object changed event should be fired).
        /// </summary>
        public bool IsSelectionMode { get { return !Visible; } }
        private readonly PipeControl_obsolete pipeControl;
        private readonly ShaftControl_obsolete shaftControl;
        private MainToolbar()
        {

            Instance = this;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            InitializeComponent();

            #region Designer code
            // Has to be done manually, because designer doesn't work.
            //
            // PipeControl
            //
            pipeControl = new PipeControl_obsolete(this, cadMgr);
            pipeControl.Dock = DockStyle.Fill;
            tabPagePipe.Controls.Add(pipeControl);

            //
            // ShaftControl
            //
            shaftControl = new ShaftControl_obsolete(this, cadMgr);
            shaftControl.Dock = DockStyle.Fill;
            tabPageShaft.Controls.Add(shaftControl);
            #endregion

            shaftControl.CreatePipe += (p1, p2) =>
            {
                pipeControl.CreateNewPipe(p1, p2);
            };
            shaftControl.ShaftUpdated += (p) =>
            {
                pipeControl.UpdateShaft(p);
            };
            shaftControl.ShaftRemoved += (p) =>
            {
                pipeControl.RemoveShaft(p);
            };

            pipeControl.CancelPipeSelection += () =>
            {
                cadMgr.UnselectCurrentObject();
            };
            shaftControl.CancelShaftSelection += () =>
            {
                cadMgr.UnselectCurrentObject();
            };

            pipeControl.PipeSelectionChanged += (p) =>
            {
                cadMgr.SetObjectSelected(p);
            };
            shaftControl.ShaftSelectionChanged += (p) =>
            {
                cadMgr.SetObjectSelected(p);
            };
            tabControl.SelectedTab = tabPageShaft;


            FormClosing += (_, __) =>
            {
                cadMgr.UnselectCurrentObject();
            };
            this.FormClosed += (_, __) =>
            {
                //Application.RemoveMessageFilter(myFilter);
                Instance = null; 
                 
            };
            Shown += (_, __) =>
            {
                FormAlreadyOpened = true;
            };
        }
        /// <summary> 
        /// NOTE Only this combination worked for editing control ESC key press.
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        protected override bool ProcessKeyPreview(ref Message m)
        {
            if (m.Msg == MyMessageFilter.WM_KEYUP)
            {
                switch(m.WParam.ToInt32())
                {
                    case (int)Keys.Escape:
                        GlobalKeyUp?.Invoke(Keys.Escape);
                        break;
                    case (int)Keys.Enter:
                        GlobalKeyUp?.Invoke(Keys.Enter);
                        break;
                }
            }
            return base.ProcessKeyPreview(ref m);
        }

        /// <summary>
        /// Lets user select Text from the drawing.
        /// </summary>
        /// <param name="adapter">Object for which text is being selected
        /// (it is possible to select text for other object than selected one)</param>
        /// <returns></returns>
        internal string SelectText(PipeAdapter adapter)
        {
            string s = null;

            Instance.Visible = false;
            cadMgr.SetObjectSelected(adapter, true);

            Instance.Visible = true;

            return s;
        }
        
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonLoadToInventory_Click(object sender, EventArgs e)
        {
            // TODO get data to load to inventory
            Inventory.CAD.CadToInventories.Datas.DataManager mgr = new Inventory.CAD.CadToInventories.Datas.DataManager();
            var pipe = pipeControl.GetAllPipes().Select(p => p.InventoryPipe).ToList();
            var shaft = shaftControl.GetAllShafts().Select(p=>p.InventoryShaft).ToList();
            mgr.Pipes = pipe;
            mgr.Shafts = shaft;
            if (!pipeControl.IsDataValid)
            {
                MessageBox.Show("Pipes exists with duplicated names.\nUnable to proceed.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!shaftControl.IsDataValid)
            {
                MessageBox.Show("Shafts exists with duplicated names.\nUnable to proceed.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataReady?.Invoke(mgr);
            Close();
        }

    }
}
