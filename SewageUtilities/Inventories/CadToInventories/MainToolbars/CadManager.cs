﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Inventories.CadToInventories.Data;
using CadPlugin.Common;
using System.Diagnostics;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Inventories.CadToInventories.MainToolbars
{
    /// <summary>
    /// Used for CAD operations.
    /// </summary>
    class CadManager
    {
        private List<ObjectId> lastSelectedObjects = null;
        //private ObjectId? lastSelected = null;
        /// <summary>
        /// User can change current document, and we want to associate this instance only with 
        /// single one. Since command invokes this constructor, it is enough to get current document there.
        /// </summary>
        private readonly Document document;

        public CadManager()
        {
            document = Application.DocumentManager.MdiActiveDocument;
        }
        /// <summary>
        /// Sets object as selected (for example it can be Highlighted).
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="zoom">Should drawing be zoomed to the object.</param>
        public void SetObjectSelected(IAdapter obj, bool zoom = false)
        {
            UnselectCurrentObject();
            IEnumerable<ObjectId> oids = obj.GetAssociatedObjectIds();
            SetObjectSelectedState(oids, true);

            lastSelectedObjects = oids.ToList();
            if (zoom)
            {
                using (Transaction tr = document.Database.TransactionManager.StartTransaction())
                {
                    List<Entity> entities = new List<Entity>();
                    foreach (ObjectId id in oids)
                    {
                        Entity e = tr.GetObject(id, OpenMode.ForRead) as Entity;
                        entities.Add(e);
                    }
                    CADZoomHelper.ZoomToEntities(document.Editor, entities, 15);
                    tr.Commit();
                }
            }
        }

        /// <summary>
        /// Unselects object which is currently selected.
        /// </summary>
        public void UnselectCurrentObject()
        {
            if (lastSelectedObjects != null)
            {
                SetObjectSelectedState(lastSelectedObjects, false);
                lastSelectedObjects = null;
            }
        }
        /// <summary>
        /// Sets selected state for objects.
        /// </summary>
        /// <param name="oids"></param>
        /// <param name="selected">true - select, false - unselect</param>
        private void SetObjectSelectedState(IEnumerable<ObjectId> oids, bool selected)
        { 
            Database db = document.Database;
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                foreach (ObjectId oid in oids)
                {
                    Debug.Assert(oid != ObjectId.Null, "Set selected object null");
                    Entity e = tr.GetObject(oid, OpenMode.ForWrite) as Entity;
                    if (selected)
                        e.Highlight();
                    else
                        e.Unhighlight();
                }
                tr.Commit();
            }
        }
        /// <summary>
        /// Returns the text selected by the user.
        /// </summary>
        /// <returns></returns>
        public string GetTextString()
        {
            Editor ed = document.Editor;
            PromptEntityOptions ent = new PromptEntityOptions("Select Text");
            ent.SetRejectMessage("Object is not text!");
            ent.AddAllowedClass(typeof(MText), true);
            ent.AddAllowedClass(typeof(DBText), true);
            
            PromptEntityResult res = ed.GetEntity(ent);
            string s = null;
            if (res.Status == PromptStatus.OK)
            {
                using (Transaction tr = document.Database.TransactionManager.StartTransaction())
                {
                    Entity e = tr.GetObject(res.ObjectId, OpenMode.ForRead) as Entity;
                    MText mt = e as MText;
                    DBText t = e as DBText;
                    if (mt != null)
                        s = mt.Contents;
                    if (t != null)
                        s = t.TextString;
                }
            }
            ed.WriteMessage("\n");
            return s;
        }
        /// <summary>
        /// Assigns entity to a shaft. Returns false in case of error or if user canceled.
        /// </summary>
        /// <returns></returns>
        public bool AssignShaftEntity(ShaftAdapter shaft)
        {
            Editor ed = document.Editor;
            PromptEntityOptions ent = new PromptEntityOptions("Select Shaft:");
            ent.SetRejectMessage("Object can be: BlockReference, Circle!");
            ent.AddAllowedClass(typeof(BlockReference), true);
            ent.AddAllowedClass(typeof(Circle), true);

            PromptEntityResult res = ed.GetEntity(ent);

            if (res.ObjectId != ObjectId.Null)
            {
                List<MyUtilities.Geometry.Point3d> points = new List<MyUtilities.Geometry.Point3d>();
                MyUtilities.Geometry.Point3d center = new MyUtilities.Geometry.Point3d();
                using (Transaction tr = document.Database.TransactionManager.StartTransaction())
                {
                    TransactionData t = new TransactionData(document, tr);
                    Entity e = tr.GetObject(res.ObjectId, OpenMode.ForRead) as Entity;
                    Circle c = e as Circle;
                    BlockReference br = e as BlockReference;
                    if (c != null)
                    {
                        center = c.Center.FromCADPoint();
                    }
                    if (br != null)
                    {
                        center = br.Position.FromCADPoint();
                    }
                }
                shaft.SetCadEntity(res.ObjectId, center);
            }

            ed.WriteMessage("\n");
            return res.Status == PromptStatus.OK && res.ObjectId != ObjectId.Null;
        }
        /// <summary>
        /// Sets pipe object (Line, Polyline, Polyline2d, Polyline3d).
        /// </summary>
        /// <returns></returns>
        public bool AssignPipeEntity(PipeAdapter pipe)
        {
            Editor ed = document.Editor;
            PromptEntityOptions ent = new PromptEntityOptions("Select Pipe:");
            ent.SetRejectMessage("Object can be: Line, Polyline, Polyline3d!");
            ent.AddAllowedClass(typeof(Line), true);
            ent.AddAllowedClass(typeof(Polyline), true);
            ent.AddAllowedClass(typeof(Polyline2d), true);
            ent.AddAllowedClass(typeof(Polyline3d), true);

            PromptEntityResult res = ed.GetEntity(ent);

            if (res.ObjectId != ObjectId.Null)
            {
                List<MyUtilities.Geometry.Point3d> points = new List<MyUtilities.Geometry.Point3d>();
                using (Transaction tr = document.Database.TransactionManager.StartTransaction())
                {
                    Entity e = tr.GetObject(res.ObjectId, OpenMode.ForRead) as Entity;
                    Line l = e as Line;
                    if (l != null)
                    {
                        points.Add(l.StartPoint.FromCADPoint());
                        points.Add(l.EndPoint.FromCADPoint());
                    }
                    else
                    {
                        var v = CADPolylineHelper.GetPoints(e, tr)?.Select(x => x.FromCADPoint());
                        if (v != null)
                            points.AddRange(v.ToArray());
                    }
                }
                pipe.SetCadEntity(res.ObjectId, points);
            }

                ed.WriteMessage("\n");
            return res.Status == PromptStatus.OK && res.ObjectId != ObjectId.Null;
        }

    }
}
