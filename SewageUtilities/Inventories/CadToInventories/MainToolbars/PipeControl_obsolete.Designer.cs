﻿namespace CadPlugin.Inventories.CadToInventories.MainToolbars
{
    partial class PipeControl_obsolete
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvPipes = new System.Windows.Forms.DataGridView();
            this.ColumnPipeEntity = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ColumnPipeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPipeObjektArt = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColumnPipeGeoobjektArt = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColumnPipeSlope = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPipeMaterial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPipeDiameter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPipeEmpty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStripPipeData = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemPipeData = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonRemoveKante = new System.Windows.Forms.Button();
            this.errorProviderDgvCell = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPipes)).BeginInit();
            this.contextMenuStripPipeData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderDgvCell)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPipes
            // 
            this.dgvPipes.AllowUserToAddRows = false;
            this.dgvPipes.AllowUserToDeleteRows = false;
            this.dgvPipes.AllowUserToResizeRows = false;
            this.dgvPipes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPipes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPipes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnPipeEntity,
            this.ColumnPipeName,
            this.ColumnPipeObjektArt,
            this.ColumnPipeGeoobjektArt,
            this.ColumnPipeSlope,
            this.ColumnPipeMaterial,
            this.ColumnPipeDiameter,
            this.ColumnPipeEmpty});
            this.dgvPipes.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPipes.Location = new System.Drawing.Point(0, 0);
            this.dgvPipes.MultiSelect = false;
            this.dgvPipes.Name = "dgvPipes";
            this.dgvPipes.RowHeadersVisible = false;
            this.dgvPipes.Size = new System.Drawing.Size(761, 231);
            this.dgvPipes.TabIndex = 3;
            // 
            // ColumnPipeEntity
            // 
            this.ColumnPipeEntity.HeaderText = "Entity";
            this.ColumnPipeEntity.Name = "ColumnPipeEntity";
            this.ColumnPipeEntity.Text = "Update...";
            this.ColumnPipeEntity.ToolTipText = "Update only if pipe is not straight from start shaft to end shaft.";
            this.ColumnPipeEntity.Width = 55;
            // 
            // ColumnPipeName
            // 
            this.ColumnPipeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnPipeName.HeaderText = "Bezeichnung";
            this.ColumnPipeName.MinimumWidth = 60;
            this.ColumnPipeName.Name = "ColumnPipeName";
            // 
            // ColumnPipeObjektArt
            // 
            this.ColumnPipeObjektArt.HeaderText = "ObjektArt";
            this.ColumnPipeObjektArt.Name = "ColumnPipeObjektArt";
            this.ColumnPipeObjektArt.ReadOnly = true;
            this.ColumnPipeObjektArt.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ColumnPipeObjektArt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ColumnPipeGeoobjektArt
            // 
            this.ColumnPipeGeoobjektArt.HeaderText = "GeoobjektArt";
            this.ColumnPipeGeoobjektArt.Name = "ColumnPipeGeoobjektArt";
            this.ColumnPipeGeoobjektArt.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnPipeGeoobjektArt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColumnPipeGeoobjektArt.Width = 120;
            // 
            // ColumnPipeSlope
            // 
            this.ColumnPipeSlope.HeaderText = "Slope";
            this.ColumnPipeSlope.Name = "ColumnPipeSlope";
            // 
            // ColumnPipeMaterial
            // 
            this.ColumnPipeMaterial.HeaderText = "Material";
            this.ColumnPipeMaterial.Name = "ColumnPipeMaterial";
            // 
            // ColumnPipeDiameter
            // 
            this.ColumnPipeDiameter.HeaderText = "Diameter";
            this.ColumnPipeDiameter.Name = "ColumnPipeDiameter";
            // 
            // ColumnPipeEmpty
            // 
            this.ColumnPipeEmpty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnPipeEmpty.HeaderText = "";
            this.ColumnPipeEmpty.Name = "ColumnPipeEmpty";
            this.ColumnPipeEmpty.ReadOnly = true;
            // 
            // contextMenuStripPipeData
            // 
            this.contextMenuStripPipeData.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemPipeData});
            this.contextMenuStripPipeData.Name = "contextMenuStripColumnName";
            this.contextMenuStripPipeData.Size = new System.Drawing.Size(153, 48);
            // 
            // toolStripMenuItemPipeData
            // 
            this.toolStripMenuItemPipeData.Name = "toolStripMenuItemPipeData";
            this.toolStripMenuItemPipeData.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItemPipeData.Text = "Select Data ";
            this.toolStripMenuItemPipeData.Click += new System.EventHandler(this.toolStripMenuItemPipeData_Click);
            // 
            // dataGridViewButtonColumn1
            // 
            this.dataGridViewButtonColumn1.HeaderText = "Entity";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.Text = "Update...";
            this.dataGridViewButtonColumn1.ToolTipText = "Update only if pipe is not straight from start shaft to end shaft.";
            this.dataGridViewButtonColumn1.Width = 55;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "Bezeichnung";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 60;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.HeaderText = "ObjektArt";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.HeaderText = "GeoobjektArt";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 120;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Slope";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Material";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Diameter";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.HeaderText = "";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // buttonRemoveKante
            // 
            this.buttonRemoveKante.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRemoveKante.Location = new System.Drawing.Point(767, 3);
            this.buttonRemoveKante.Name = "buttonRemoveKante";
            this.buttonRemoveKante.Size = new System.Drawing.Size(23, 23);
            this.buttonRemoveKante.TabIndex = 7;
            this.buttonRemoveKante.Text = "-";
            this.buttonRemoveKante.UseVisualStyleBackColor = true;
            this.buttonRemoveKante.Click += new System.EventHandler(this.buttonRemoveKante_Click);
            // 
            // errorProviderDgvCell
            // 
            this.errorProviderDgvCell.ContainerControl = this;
            // 
            // PipeControl_obsolete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonRemoveKante);
            this.Controls.Add(this.dgvPipes);
            this.Name = "PipeControl_obsolete";
            this.Size = new System.Drawing.Size(793, 234);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPipes)).EndInit();
            this.contextMenuStripPipeData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderDgvCell)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvPipes;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripPipeData;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemPipeData;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.Button buttonRemoveKante;
        private System.Windows.Forms.ErrorProvider errorProviderDgvCell;
        private System.Windows.Forms.DataGridViewButtonColumn ColumnPipeEntity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPipeName;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnPipeObjektArt;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnPipeGeoobjektArt;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPipeSlope;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPipeMaterial;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPipeDiameter;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPipeEmpty;
    }
}
