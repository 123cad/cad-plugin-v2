﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using SewageUtilities.Helpers;
using SewageUtilities.Helpers.Geometry;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace SewageUtilities.Geometry
{
    /// <summary>
    /// Class for drawing solid objects in 3d.
    /// </summary>
    public class Pipe3dVisualDrawer
    {
        /// <summary>
        /// Draws pipe segment (arc or line, depending on bulge).
        /// </summary>
        /// <param name="tw"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="diameter"></param>
        /// <param name="bulge">If 0, straight line is drawn.</param>
        /// <returns></returns>
        public static Solid3d DrawSegment(TransactionWrapper tw, Point3d start, Point3d end, ComplexSolidProfileManager profileManager, double diameter, double bulge = 0)
        {
            return DrawSegmentReduced(tw, start, end, profileManager, diameter, 0, 0, bulge);
        }
        /// <summary>
        /// Draws segment with reducing path (start, end or both) where solid is created.
        /// (used for joints, drawing a joint needs to reduce previous and next segment).
        /// </summary>
        /// <param name="tw"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="diameter"></param>
        /// <param name="reduceStart">Must be positive</param>
        /// <param name="reduceEnd">Must be positive</param>
        /// <param name="bulge"></param>
        /// <returns></returns>
        public static Solid3d DrawSegmentReduced(TransactionWrapper tw, Point3d start, Point3d end, ComplexSolidProfileManager profileManager, 
                                                double diameter, double reduceStart, double reduceEnd, double bulge = 0)
        {
            bool isArc = !System.DoubleHelper.AreEqual(0, bulge, 0.0001);
            Solid3d obj;
            Point3d newStart = start;
            Point3d newEnd = end;
            if (isArc)
            {
                double newBulge = bulge;
                using (Polyline pl = new Polyline())
                {
                    pl.AddPoints(
                        new PolylineVertex<Point2d>(start.GetAs2d(), bulge),
                        new PolylineVertex<Point2d>(end.GetAs2d(), 0)
                        );

                    Point3d? reducedPointStart = null;
                    if (reduceStart > 0)
                        reducedPointStart = pl.GetPointAtDist(reduceStart);
                    Point3d? reducedPointEnd = null;
                    if (reduceEnd > 0)
                        reducedPointEnd = pl.GetPointAtDist(pl.Length - reduceEnd);

                    List<Point3d> pts = new List<Point3d>();
                    if (reducedPointStart.HasValue) pts.Add(reducedPointStart.Value);
                    if (reducedPointEnd.HasValue) pts.Add(reducedPointEnd.Value);

                    if (pts.Any())
                    {
                        var curves = pl.GetSplitCurves(new Point3dCollection(pts.ToArray()));
                        int index = reducedPointStart.HasValue ? 1 : 0;
                        Polyline newPl = ((Polyline)curves[index]);
                        newBulge = newPl.GetBulgeAt(0);
                        newStart = newPl.GetPointAtParameter(0);
                        newEnd = newPl.GetPointAtParameter(1);
                        double z = PipeHelper.InterpolateHeight(start.Z, end.Z, pl.Length, reduceStart);
                        newStart = newStart.GetWithHeight(z);
                        z = PipeHelper.InterpolateHeight(start.Z, end.Z, pl.Length, pl.Length - reduceEnd);
                        newEnd = newEnd.GetWithHeight(z);

                        foreach (var dbo in curves)
                            ((DBObject)dbo).Dispose();
                    }
                }
                //newStart = newStart.Add(new Vector3d(0, 0, -diameter / 2));
                //newEnd = newEnd.Add(new Vector3d(0, 0, -diameter / 2));
                var helix = CADHelixHelper.DrawHelix(tw, newStart, newEnd, newBulge);
                obj = CADSolid3dHelper.CreateSolidAlongHelix(tw, helix, diameter, profileManager);
                helix.Erase();
            }
            else
            {
                Vector3d seg = start.GetVectorTo(end);
                Vector3d unit = seg.GetNormal();
                newStart = start.Add(unit.MultiplyBy(reduceStart));
                newEnd = end.Add(unit.Negate().MultiplyBy(reduceEnd));
                //newStart = newStart.Add(new Vector3d(0, 0, -diameter / 2));
                //newEnd= newEnd.Add(new Vector3d(0, 0, -diameter / 2));
                var path = new List<MyUtilities.Geometry.Point3d>() { newStart.FromCADPoint(), newEnd.FromCADPoint() };
                obj = CADSolid3dHelper.CreateSolidAlongPath(tw, path, profileManager);
            }
            return obj;
        }

        /// <summary>
        /// Pipe segments meet at an angle. Make nice transition from first to second one (same as Polyline Fillet, but in 3d).
        /// </summary>
        /// <param name="tw"></param>
        /// <param name="jointPosition">Where 2 segments meet (end of start, and start of end)</param>
        /// <param name="startSegment"></param>
        /// <param name="startBulge"></param>
        /// <param name="endSegment"></param>
        /// <param name="bulge"></param>
        /// <returns></returns>
        public static Solid3d DrawSegmentsJointWithHelix(TransactionWrapper tw,
                                                    Point3d jointPosition,
                                                    Vector3d startSegment, double startBulge,
                                                    Vector3d endSegment, double bulge,
                                                    double pipeDiameter
                                                    )
        {
            Helix h = Pipe3dDrawer.DrawSegmentsJointWithHelix(tw, jointPosition, startSegment, startBulge, endSegment, bulge, pipeDiameter*1.01);
            return CADSolid3dHelper.CreateCylinderAlongHelix(tw, h, pipeDiameter);
        }

        public class PipeVertex
        {
            public Point3d Point;
            /// <summary>
            /// Bulge to next point ( positive - arc goes right, negative - arc goes left, in downstream direction).
            /// </summary>
            public double Bulge;
            public bool IsArc => System.DoubleHelper.AreEqual(Bulge, 0, 0.0001);

            public PipeVertex(Point3d pt, double bulge=0)
            {
                Point = pt;
                Bulge = bulge;
            }
        }
        /// <summary>
        /// Draw a pipe which can consist of 1 or more segments.
        /// </summary>
        /// <param name="tw"></param>
        /// <param name="pipeDiameter"></param>
        /// <param name="vertices"></param>
        /// <returns></returns>
        public static IEnumerable<Solid3d> DrawPipeSegments(TransactionWrapper tw, double pipeDiameter, ComplexSolidEntitiesCollection profile, params PipeVertex[] vertices)
        {
            if (vertices.Length < 2)
                throw new ArgumentException("Drawing pipe requires at least 2 vertices!");
            List<Solid3d> solids = new List<Solid3d>();
            double previousReduce = 0;
            for (int i = 1; i < vertices.Length; i++)
            {
                PipeVertex previous = vertices[i - 1];
                PipeVertex current = vertices[i];
                PipeVertex next = i + 1 < vertices.Length ? vertices[i + 1] : null;
                Solid3d startSolid = null;
                Solid3d jointSolid = null;
                Solid3d endSolid = null; 
                // Draw joint and calculate if needed to reduce length of segments.
                double reducedStart = 0, reducedEnd = 0;
                if (next != null)
                {
                    var helix = Pipe3dDrawer.DrawSegmentsJointWithHelix(tw, current.Point,
                                                                            previous.Point.GetVectorTo(current.Point), previous.Bulge,
                                                                            current.Point.GetVectorTo(next.Point), current.Bulge,
                                                                            pipeDiameter,
                                                                            ref reducedStart, ref reducedEnd);

                    if (helix != null)
                    {
                        Vector3d move = new Vector3d(0, 0, pipeDiameter / 2);
                        //helix.TransformBy(Matrix3d.Displacement(move));
                        List<Solid3d> joints = new List<Solid3d>();
                        foreach(var p in profile)
                        {
                            var j = CADSolid3dHelper.CreateSolidAlongHelix(tw, helix, pipeDiameter * 1.01, p);
                            joints.Add(j);
                        }
                        jointSolid = CADSolid3dHelper.UnionSolids(tw, true, joints.ToArray());//.CreateSolidAlongHelix(tw, helix, pipeDiameter * 1.01, profile.First());
                        helix.Erase();
                    }
                }
                //endSolid = DrawSegmentReduced(tw, current.Point, next.Point, pipeDiameter, reducedStart, 0, current.Bulge);
                // Draw start segment.
                List<Solid3d> segments = new List<Solid3d>();
                foreach(var p in profile)
                {
                    var s = DrawSegmentReduced(tw, previous.Point, current.Point, p, pipeDiameter, previousReduce, reducedEnd, previous.Bulge);
                    segments.Add(s);
                }
                startSolid = CADSolid3dHelper.UnionSolids(tw, true, segments.ToArray()); // DrawSegmentReduced(tw, previous.Point, current.Point, profile.First(), pipeDiameter, previousReduce, reducedEnd, previous.Bulge);

                previousReduce = reducedEnd;
                solids.Add(startSolid);
                if (jointSolid != null)
                    solids.Add(jointSolid);
                if (endSolid != null)
                    solids.Add(endSolid);

                //TODO Joint can be needed when connecting pipe to shaft (same as pipe vertex, but segment after joint is not included).
            }
                return solids;
        }
        //TODO Draw with profile.
               
        
    }
}
