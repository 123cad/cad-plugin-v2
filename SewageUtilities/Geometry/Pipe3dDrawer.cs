﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace SewageUtilities.Helpers.Geometry
{
    /// <summary>
    /// Draws pipe elements in 3d (non modeling, just XYZ)
    /// </summary>
    public class Pipe3dDrawer
    {
        private static bool IsJointNeeded(Polyline pl, double p1z, double p2z, double p3z)
        {
            var p1 = pl.GetPointAtParameter(0.99);
            var p2 = pl.GetPointAtParameter(1);
            var p3 = pl.GetPointAtParameter(1.01);
            Vector3d v1 = p1.GetVectorTo(p2);
            Vector3d v2 = p2.GetVectorTo(p3);
            double angle = v1.FromCADVector().GetAngleDegXY(v2.FromCADVector());
            if (2 < Math.Abs(angle))
                return true;

            if (v1.Length < 0.000001 || v2.Length < 0.000001)
            {
                if (v1.Length < 0.000001 && v2.Length < 0.000001)
                    return false;
                return true;
            }

            // If pipe doesn't change XY direction, check if it changes z.
            double z1 = PipeHelper.InterpolateHeight(p1z, p2z, v1.Length, 0.99 * v1.Length);
            double z2 = PipeHelper.InterpolateHeight(p2z, p3z, v2.Length, 0.10 * v2.Length);

            v1 = v1.GetWithZ(z1);
            v2 = v2.GetWithZ(z2);

            Point3d test1 = new Point3d().Add(v1.GetNormal().MultiplyBy(v1.Length + v2.Length));
            Point3d test2 = new Point3d().Add(v1).Add(v2);
            if (test1.GetVectorTo(test2).Length > 0.05)
                return true;
            return false;
        }

        /// <summary>
        /// Pipe segments meet at an angle. Make nice transition from first to second one (same as Polyline Fillet, but in 3d).
        /// </summary>
        /// <param name="tw"></param>
        /// <param name="jointPosition">Where 2 segments meet (end of start, and start of end)</param>
        /// <returns></returns>
        public static Helix DrawSegmentsJointWithHelix(TransactionWrapper tw,
                                                    Point3d jointPosition
                                                    , Vector3d startSegment, double startBulge
                                                    , Vector3d endSegment, double endbulge
                                                    , double pipeDiameter
                                                    )
        {
            double d1 = 0, d2 = 0;
            return DrawSegmentsJointWithHelix(tw, jointPosition, startSegment, startBulge, endSegment, endbulge, pipeDiameter, ref d1, ref d2);
        }
        /// <summary>
        /// Pipe segments meet at an angle. Make nice transition from first to second one (same as Polyline Fillet, but in 3d).
        /// </summary>
        /// <param name="tw"></param>
        /// <param name="jointPosition">Where 2 segments meet (end of start, and start of end)</param>
        /// <param name="reducedStart">How much end point of first segment is moved backwards to allow joint.</param>
        /// <param name="reducedEnd">How much start point of end segment is moved forward to allow joint.</param>
        /// <returns></returns>
        public static Helix DrawSegmentsJointWithHelix(TransactionWrapper tw,
                                                    Point3d jointPosition
                                                    , Vector3d startSegment, double startBulge
                                                    , Vector3d endSegment, double bulge
                                                    , double pipeDiameter
                                                    , ref double reducedStart, ref double reducedEnd
                                                    )
        {
            double adjustedPipeDiameter = pipeDiameter * 1.4; // To add some extra space for joint.
            //double step = 0.5;
            double offset = 0.1;
          

            //NOTE All angles are in Deg (unless there is Rad suffix).
            using (Polyline pl = new Polyline())
            {
                Point3d startP = jointPosition.Add(startSegment.Negate());
                Point3d endP = jointPosition.Add(endSegment);
                pl.AddPoints(
                    new PolylineVertex<Point2d>(startP.To2d(), startBulge),
                    new PolylineVertex<Point2d>(jointPosition.To2d(), bulge),
                    new PolylineVertex<Point2d>(endP.To2d(), 0));
                if (!IsJointNeeded(pl, startP.Z, jointPosition.Z, endP.Z))
                    return null;

                double distance = pl.GetDistanceAtParameter(1);
                if (offset >= distance)
                    offset = distance * 0.5 + 0.05;
                double offset1 = offset;
                double offset2 = 0;
                Point3d p1;
                Point3d p2;
                double z;
                if (distance < 0.0001)
                {
                    startSegment = new Vector3d(0, 0, startSegment.GetNormal().Z);
                }
                else
                {
                    p1 = pl.GetPointAtDist(distance - offset1);
                    p2 = pl.GetPointAtDist(distance - offset2);
                    if (distance < 0.000001)
                        z = jointPosition.Add(startSegment.Negate()).Z;
                    else
                        z = PipeHelper.InterpolateHeight(startP.Z, jointPosition.Z, distance, distance - offset1);
                    p1 = p1.GetWithHeight(z);
                    if (distance < 0.000001)
                        z = jointPosition.Z;
                    else
                        z = PipeHelper.InterpolateHeight(startP.Z, jointPosition.Z, distance, distance - offset2);
                    p2 = p2.GetWithHeight(jointPosition.Z);
                    startSegment = p1.GetVectorTo(p2).GetNormal();
                }

                double distance1 = pl.GetDistanceAtParameter(2) - distance;
                if (2 * offset >= distance1)
                    offset = distance1 * 0.5;
                if (distance1 < 0.0001)
                {
                    endSegment = new Vector3d(0, 0, endSegment.GetNormal().Z);
                }
                else
                {
                    offset1 = 0;
                    offset2 = offset;
                    p1 = pl.GetPointAtDist(distance + offset1);

                    if (distance1 < 0.00001)
                        z = jointPosition.Z; // Because point can be 90 deg (XY length = 0)
                    else
                        z = PipeHelper.InterpolateHeight(jointPosition.Z, endP.Z, distance1, offset1);
                    p1 = p1.GetWithHeight(z);
                    p2 = pl.GetPointAtDist(distance + offset2);
                    if (distance1 < 0.00001)
                        z = jointPosition.Add(endSegment).Z;
                    else
                        z = PipeHelper.InterpolateHeight(jointPosition.Z, endP.Z, distance1, offset2);
                    p2 = p2.GetWithHeight(z);
                    endSegment = p1.GetVectorTo(p2).GetNormal();
                }

                /*
                    Helix is always drawn in plane defined with (startSegment x endSegment), and drawn
                    with height = 0. This results in that looking from the top (to XY plane), joint will not 
                    be circular arc.
                */

                // Switch to UCS (all calculations are done in XY plane where both vectors have z = 0).
                endSegment = endSegment.Negate();
                Vector3d planeNormal = endSegment.CrossProduct(startSegment);
                Matrix3d ucs = Matrix3d.WorldToPlane(new Plane(new Point3d(), planeNormal));
                Vector3d startV = startSegment.TransformBy(ucs);
                Vector3d endV = endSegment.TransformBy(ucs);
                Vector3d startNormal = startV.GetNormal();
                Vector3d endNormal = endV.GetNormal();



                Helix h = null;
                {
                    // All calculations are done in UCS.

                    double radius = adjustedPipeDiameter / 2;
                    // Calculate angle of joint.
                    double alfa = endV.FromCADVector().GetAngleDegXY(startV.FromCADVector()) / 2;

                    double beta = 180 - Math.Abs(2 * alfa);
                    // Angle between start vector and middleVector.
                    double teta = beta + Math.Abs(alfa);
                    if (alfa < 0)
                        teta = -teta;

                    // Vector on which center of arc will be 
                    Vector3d middleVector = startV.GetWithZ().RotateBy(MyUtilities.Helpers.Calculations.DegreesToRadians(teta), Vector3d.ZAxis);

                    double gama = 90 - Math.Abs(alfa);
                    double gamaRad = MyUtilities.Helpers.Calculations.DegreesToRadians(gama);
                    double l = radius / Math.Cos(gamaRad);
                    double r1 = l * Math.Cos(gamaRad);
                    double q = Math.Sqrt(l * l - r1 * r1);

                    Point3d start = new Point3d().TransformBy(ucs).Add(startNormal.MultiplyBy(-q));
                    Point3d end = new Point3d().TransformBy(ucs).Add(endNormal.MultiplyBy(q));

                    double turns = 2 * gamaRad / Math.PI / 2;
                    double arcRadius = r1;
                    Point3d arcCenter = new Point3d().TransformBy(ucs).Add(middleVector.GetNormal().MultiplyBy(l));
                    HelixDirection direction = alfa < 0 ? HelixDirection.Clockwise : HelixDirection.Counterclockwise;


                    h = CADHelixHelper.DrawHelixInTurns(tw, start, end, arcCenter, turns, arcRadius, direction);

                    reducedStart = q;
                    reducedEnd = q;
                }

                // After helix is calculated in UCS with z = 0, transform it to original position (and rotation).
                Matrix3d jointDisplacement = Matrix3d.Displacement(jointPosition.GetAsVector());
                Matrix3d combo = Matrix3d.PlaneToWorld(planeNormal).PreMultiplyBy(jointDisplacement);
                h.TransformBy(combo);

                // There was some bug with Union. Scaling this little bit seems to fixed it.
                h.TransformBy(Matrix3d.Scaling(1.2, jointPosition));

                return h;
            }
        }
    }
}
