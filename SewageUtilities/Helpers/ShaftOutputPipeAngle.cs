﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtilities.Geometry;

namespace SewageUtilities.Helpers
{

    /// <summary>
    /// Calculates angles of all
    /// </summary>
    public class ShaftPipeAngleManager
    {
        public Point3d inputPipe { get; }
        public Point2d shaftCenter { get; set; }
        public IEnumerable<ShaftOutputPipe> OutputPipes => outputPipes;

        private List<ShaftOutputPipe> outputPipes;
    }
}
