﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SewageUtilities.Pipes;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif
namespace SewageUtilities.Helpers
{
    public static class SewageColorsDefinitions
    { 
        public static Color GetEntwaesserungsartColor(PipeWaterType waterType)
        {
            string s = "";
            switch (waterType)
            {
                case PipeWaterType.REGENWASSER:
                    s = "KR";
                    break;
                case PipeWaterType.MISCHWASSER:
                    s = "KM";
                    break;
                case PipeWaterType.SCHMUTZWASSER:
                    s = "KS";
                    break;
            }

            Color color = GetEntwaesserungsartColor(s);

            return color;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entwaesserungsart">2 uppercase letter as defined in ISYBAU</param>
        /// <returns></returns>
        public static Color GetEntwaesserungsartColor(string entwaesserungsart)
        {
            Color color = Color.FromColor(System.Drawing.Color.Black);
            switch (entwaesserungsart)
            {
                case "KR":
                case "DR":
                case "GR":
                    color = Color.FromRgb(14, 81, 141);//Color.FromRgb(173, 216, 230);
                    break;
                case "KM":
                case "DM":
                case "GM":
                    color = Color.FromRgb(241, 154, 242);//Color.FromRgb(255, 0, 255);
                    break;
                case "KS":
                case "DS":
                case "GS":
                    color = Color.FromRgb(210, 146, 134);//Color.FromRgb(90, 58, 41);
                    break;
                case "KW":
                case "GW":
                    color = Color.FromRgb(14, 81, 141);
                    break;
            }

            return color;
        }
    }
}
