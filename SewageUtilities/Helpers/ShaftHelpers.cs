﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtilities.Geometry;


namespace SewageUtilities.Helpers
{
    public class ShaftOutputPipe
    {
        /// <summary>
        /// Index when passed to calculation.
        /// </summary>
        public int OriginalIndex{ get; set; }
        /// <summary>
        /// Index after sorting all output pipes, clockwise direction.
        /// </summary>
        public int CalculatedIndex { get; set; }
        public double AngleDeg { get; set; }
        public Point3d Position { get; }
        public ShaftOutputPipe(Point3d position)
        {
            Position = position;
        }
    }
    public static class ShaftHelpers
    {
        /// <summary>
        /// Returns angle in degrees, of pipeOut to pipeIn, in clockwise direction.
        /// </summary>
        public static double PipeOutToInAngleDeg(Point2d shaftCenter, Point2d pipeOut, Point2d pipeIn)
        {
            var v1 = shaftCenter.GetVectorTo(pipeOut);
            var v2 = shaftCenter.GetVectorTo(pipeIn);
            double angleDeg = v1.GetAngleDeg(v2);
            if (angleDeg < 0)
                angleDeg += 360;
            return angleDeg;
        }


        /// <summary>
        /// Calculates angles for output pipes (clockwise, relative to input pipe).
        /// </summary>
        public static IEnumerable<ShaftOutputPipe> CalculateOutputAngles(
            Point3d inputPipe,
            Point2d shaftCenter,
            params ShaftOutputPipe[] outputPipes)
        {
            var inputPipe2d = inputPipe.GetAs2d();
            var output = outputPipes.ToList();
            for (int i = 0; i < outputPipes.Length; i++)
            {
                var op = outputPipes[i];
                double angleDeg = ShaftHelpers.PipeOutToInAngleDeg(shaftCenter, op.Position.GetAs2d(), inputPipe2d);
                op.AngleDeg = angleDeg;
                op.OriginalIndex = i;
            }
            output.ForEach(p =>
            {
            });
            outputPipes
                .OrderBy(p => p.AngleDeg)
                .Select((p, index) =>
                {
                    p.CalculatedIndex = index;
                    return p;
                })
                .ToList();
            return output;
        }
    }
}
