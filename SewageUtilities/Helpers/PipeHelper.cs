﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SewageUtilities.Helpers
{
    public class PipeHelper
    {

        /// <summary>
        /// Returns absolute value of height difference, for given 2d length and slope.
        /// </summary>
        /// <param name="length"></param>
        /// <param name="slopeRatio"></param>
        /// <returns></returns>
        public static double GetHeightForSlope(double length2d, double slopeRatio)
        {
            double d = length2d * slopeRatio;
            return Math.Abs(d);
        }
        
        /// <summary>
        /// Returns new Z height for end point for a given slope.
        /// Positive slope is downstream (result is negative).
        /// </summary>
        public static double GetEndZ(Point3d start, Point3d end, double slopeRatio)
        {
            double newZ = 0;
            Vector2d pipe2d = start.GetAs2d().GetVectorTo(end.GetAs2d());
            newZ = start.Z - pipe2d.Length * slopeRatio;
            return newZ;
        }

        /// <summary>
        /// Sets start and end points of pipe depending on shaft data.
        /// </summary>
        /// <param name="startShaftCenter"></param>
        /// <param name="startShaftDiameter"></param>
        /// <param name="pipeOutHeight"></param>
        /// <param name="pipeSlopePercent"></param>
        /// <param name="startPipe"></param>
        /// <param name="endPipe"></param>
        /// <param name="endShaftCenter"></param>
        /// <param name="endShaftDiameter"></param>
        public static void AdjustPipePointsToShafts(Point2d startShaftCenter, double startShaftDiameter,
                                                double pipeOutHeight,double pipeInHeight, 
                                                ref Point3d startPipe, ref Point3d endPipe, 
                                                Point2d endShaftCenter, double endShaftDiameter)
        {
            Vector2d pipe2d = startShaftCenter.GetVectorTo(endShaftCenter);
            Vector2d pipeUnit = pipe2d.GetUnitVector();
            startPipe = startShaftCenter.Add(pipeUnit * startShaftDiameter / 2).GetWithHeight(pipeOutHeight);
            Point3d t = endShaftCenter.Add((pipeUnit * endShaftDiameter / 2).Negate()).GetWithHeight(pipeInHeight);
            //AdjustPipeEndHeight(startPipe, pipeSlopePercent / 100, ref t);
            endPipe = t;
        }
        /// <summary>
        /// Sets new Z for end point of the pipe (which depends on slope).
        /// </summary>
        /// <param name="slope">Slope is in ratio (tg)</param>
        /// <returns></returns>
        public static Point3d AdjustPipeEndHeight(Point3d start, Point3d end, double slope)
        {
            Vector2d pipe = start.GetAs2d().GetVectorTo(end.GetAs2d());
            double deltaZ = pipe.Length * slope;
            end = new Point3d(end.X, end.Y, start.Z - deltaZ);
            return end;
        }

        /// <summary>
        /// Calculate height at length2d distance from pipe start, by interpolating height.
        /// </summary>
        /// <param name="startHeight"></param>
        /// <param name="endHeight"></param>
        /// <param name="total2dLength"></param>
        /// <param name="length2d">Length for which we need height.</param>
        /// <returns>Interpolated height.</returns>
        public static double InterpolateHeight(double startHeight, double endHeight, double total2dLength, double length2d)
        {
            double deltaZ = startHeight - endHeight;
            double tempZ = length2d * deltaZ / total2dLength;
            double newZ = startHeight - tempZ;
            return newZ;
        }

        
    }

    
}
