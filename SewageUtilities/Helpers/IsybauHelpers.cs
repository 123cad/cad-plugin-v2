﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isybau2015.Identifikations.Datenkollektives.Stammdatenkollektivs.AbwassertechnischeAnlagens.Geometries.Geometriedatens;
using MyUtilities.Geometry;

namespace SewageUtilities.Helpers
{
    public static class IsybauHelpers
    {
        public static Point3d GetPoint3d(this Punkt p)
        {
            return new Point3d(p.Rechtswert, p.Hochwert, p.Punkthoehe);
        }
        public static void SetPoint3d(this Punkt p, Point3d pt)
        {
            p.Rechtswert = pt.X;
            p.Hochwert = pt.Y;
            p.Punkthoehe = pt.Z;
        }
    }
}
