﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.SewageHelpers
{
    public struct PipeSlope
    {
        /// <summary>
        /// Angle of the pipe in degrees (positive value is to the bottom).
        /// </summary>
        public double AngleDeg { get; private set; }

        /// <summary>
        /// Angle of the pipe in radians (positive value is to the bottom).
        /// </summary>
        public double AngleRad { get; private set; }

        /// <summary>
        /// Slope of the pipe (positive value is to the bottom).
        /// </summary>
        public double SlopePercent { get; private set; }

        /// <summary>
        /// Slope of pipe, in ratio (which is actually tan(angle) ).
        /// </summary>
        public double SlopeRatio => SlopePercent / 100;


        public static PipeSlope CreateFromAngle(double angleDeg)
        {
            PipeSlope ps = new PipeSlope();
            ps.AngleRad = MyUtilities.Helpers.Calculations.DegreesToRadians(angleDeg);
            ps.AngleDeg = angleDeg;
            if (System.DoubleHelper.AreEqual(90, angleDeg, 0.0001))
                ps.SlopePercent = double.MaxValue;
            else if (System.DoubleHelper.AreEqual(-90, angleDeg, 0.0001))
                ps.SlopePercent = double.MinValue;
            else
                ps.SlopePercent = Math.Tan(ps.AngleRad);
            ps.SlopePercent *= 100;
            return ps;
        }
        public static PipeSlope CreateFromSlope(double slopePercent)
        {
            PipeSlope ps = new PipeSlope();
            ps.SlopePercent = slopePercent;
            double slope = slopePercent / 100;
            double atan = Math.Atan(slope);
            ps.AngleRad = atan;
            ps.AngleDeg = MyUtilities.Helpers.Calculations.RadiansToDegrees(atan);
            return ps;
        }
        public static PipeSlope CreateFromPoints(Point2d start, Point2d end)
        {
            Vector2d v = start.GetVectorTo(end);
            double slope = -v.Y / Math.Abs(v.X);
            return CreateFromSlope(slope * 100);
        }
        public static PipeSlope CreateFromPoints(Point3d start, Point3d end)
        {
            Vector2d pipe2d = start.GetAs2d().GetVectorTo(end.GetAs2d());
            double deltaZ = start.Z - end.Z;
            double slopeRatio = deltaZ / pipe2d.Length;
            return CreateFromSlope(slopeRatio * 100);
        }

    }
}
