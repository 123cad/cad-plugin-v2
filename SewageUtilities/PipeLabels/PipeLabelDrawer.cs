﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;
using System.Drawing.Imaging;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace SewageUtilities.PipeLabels
{
	/// <summary>
	/// Used for drawing text labels for pipes.
	/// (planned to be unified solution for all sewage modules).
	/// </summary>
	public class PipeLabelDrawer
	{
		//NOTE This class is designed to be main class for drawing pipe labels.
		//NOTE Similar class exists in CadPlugin and is called PipeLabelDrawer 
		//NOTE (which should be removed in the future, and all switched to here).

		/// <summary>
		/// Class which holds data for drawing. 
		/// </summary>
		public class LabelObjects
        {
			/// <summary>
			/// MText entities.
			/// </summary>
			public IEnumerable<ObjectId> Texts => _texts;
			/// <summary>
			/// Polyline which represents arrow.
			/// </summary>
			public ObjectId Arrow { get; protected set; } = ObjectId.Null;
			private List<ObjectId> _texts = new List<ObjectId>();
			public LabelObjects()
            {

            }
			public LabelObjects(IEnumerable<ObjectId> texts, ObjectId arrowPl)
			{
				_texts.AddRange(texts);
				Arrow = arrowPl;
			}
			/// <summary>
			/// Returns DBText if it is valid, or null.
			/// </summary>
			public DBText GetText(TransactionWrapper tw, int index)
            {
				if (_texts.Count <= index)
					return null;
				try
				{
					var t = tw.GetObjectWrite<DBText>(_texts[index]);
					return t;
				}
				catch { }
				return null;
            }
			/// <summary>
			/// Deletes old text on desired index, and create new one.
			/// </summary>
			/// <param name="tw"></param>
			/// <param name="index"></param>
			/// <returns></returns>
			public DBText CreateText(TransactionWrapper tw, int index)
            {
				GetText(tw, index)?.Erase();
				var t = tw.CreateEntity<DBText>();
				if (_texts.Count <= index)
					_texts.Add(t.Id);
				else 
					_texts[index] = t.Id;
				return t;
            }

			/// <summary>
			/// Returns arrow. If ObjectId is invalid, new one is created.
			/// </summary>
			/// <param name="tw"></param>
			/// <param name="createIfInvalid"></param>
			/// <returns></returns>
			public Polyline GetArrow(TransactionWrapper tw, bool createIfInvalid = true)
            {
				Polyline pl;
				if (!Arrow.IsNull)
				{

					try
					{
						pl = tw.GetObjectWrite<Polyline>(Arrow);
						return pl;
					}
					catch
					{
						if (!createIfInvalid)
							return null;
					}
				}
				pl = tw.CreateEntity<Polyline>();
				Arrow = pl.Id;
				return pl;
            }
			public void DeleteAll(TransactionWrapper tw)
            {
				foreach(var id in Texts)
                {
					try
					{
						if (!id.IsNull)
							tw.GetObjectWrite<Entity>(id).Erase();
					}
					catch { }
                }
                try
                {
					if (!Arrow.IsNull)
						tw.GetObjectWrite<Entity>(Arrow).Erase();
                }
                catch 
                {
                }
            }
        }

        /// <summary>
        /// Updates values in entities (tries to reuse existing ones).
        /// </summary>
        /// <param name="tw"></param>
        /// <param name="pd"></param>
        /// <param name="obj">Entities which are invalid (erased, empty or not created) are replaced.</param>
        public static void UpdateLabels(TransactionWrapper tw, PipeData pd, LabelObjects obj)
        {
			drawLabelInternal(tw, pd, obj);
		}
        
		/// <summary>
		/// Creates label for pipe, and returns ids of all created objects.
		/// </summary>
		public static IEnumerable<ObjectId> DrawLabel(TransactionWrapper tw, PipeData pipe)// int diameterMM, string material)
        {
			var list = new List<ObjectId>();
			var d = new LabelObjects(new ObjectId[0], ObjectId.Null);
			drawLabelInternal(tw, pipe, d);
			if (!d.Arrow.IsNull)
				//yield return d.Arrow;
				list.Add(d.Arrow);
			foreach (var t in d.Texts)
				//yield return t;
				list.Add(t);
			return list;
        } 

		/// <summary>
		/// 
		/// </summary>
		/// <param name="tw"></param>
		/// <param name="pipe"></param>
		/// <param name="data">If null is passed, object will be created.</param>
		private static void drawLabelInternal(TransactionWrapper tw, PipeData pipe, LabelObjects data)
		{
			int currentTextIndex = 0;
			// Returns existing DBText. Otherwise, creates new one.
			Func<DBText> getText = () =>
			{
				var dbt = data.GetText(tw, currentTextIndex);
				if (dbt == null)
					dbt = data.CreateText(tw, currentTextIndex);
				dbt.Layer = pipe.Layer;
				// When reusing DBText, it can have transformation.
				// Didn't find method to reset transform, but this works.
				dbt.Rotation = 0;
				currentTextIndex++;
				return dbt;
			};
			

			List<Entity> textEntities = new List<Entity>();
			if (!tw.ModelSpace.IsWriteEnabled)
				tw.ModelSpace.UpgradeOpen();

			var v = pipe.StartPoint.GetVectorTo(pipe.EndPoint);
			MyUtilities.Geometry.Vector3d pipeVector = new MyUtilities.Geometry.Vector3d(v.X, v.Y, v.Z);
			MyUtilities.Geometry.Vector2d pipe2d = pipeVector.GetAs2d();

			double length = pipe.Length2d;
			if (System.DoubleHelper.AreEqual(length, 0.0, 0.00001))
				length = 0.001;
			double ratio = pipeVector.Z / length; // MyUtilities.Helpers.Calculations.GetSlope(pipeVector) * 100;
			double percent = ratio * 100;
			double promil = percent * 10;
			MyUtilities.Geometry.Point3d middlePoint = pipe.LabelPosition.PositionFromParameter().FromCADPoint();// pipe.StartPoint.FromCADPoint().Add(pipeVector.Multiply(0.5));
			double pipeTotalWidth = pipe.Diameter;// opts.SelectedDiameterMM / 1000.0;

			CADLayerHelper.AddLayerIfDoesntExist(tw.ModelSpace.Database, pipe.Layer);
			Func<string, DBText> createText = (s) =>
			{
				var t = getText();// tw.CreateEntity<DBText>(pipe.Layer);
				t.TextString = s;
				textEntities.Add(t);
				return t;
			};

			List<SingleText> textCollection = pipe.GetText().ToList();
			foreach (SingleText text in textCollection)
			{
				createText(text.SelectedText);
			}

			double textHeight = pipe.Scale.GetTextSize();
			textEntities.ForEach(x => ((DBText)x).Height = textHeight);

			// No spacing between text is used, but we add space in the text.
			double spacingBetweenText = textHeight / 2;// diameterText.GeometricExtents.ToRectangle().Height / 2;
													   // Use spacing between text or use spacing with space character.
			bool useSpacing = false;
			double pipeLength2d = pipe2d.Length;// pipe.StartPoint.GetAs2d().GetVectorTo(pipe.EndPoint.GetAs2d()).Length;
												//materialText.TextString = "P";
			double arrowWidth = textHeight * 2;//materialText.GeometricExtents.ToRectangle().Height * 2;
			double arrowHeight = textHeight / 3;
			{
				//var tempTexts = pipeData.GetMultilineTexts();
				//if (DoesFitToSingleLine(pipeLength2d * 0.9, 0, (DBText)textEntities.First(), tempTexts.ToString()))// tempDiameter, tempMaterial, tempSlope, tempLength))
				//{
				//	spacingBetweenText = textHeight / 2;
				//	textCollection = tempTexts.ToList();
				//}
				//else
				//{
				//	spacingBetweenText = textHeight / 2;//materialText.GeometricExtents.ToRectangle().Height / 2;
				//}

				//for (int i = 0; i < textEntities.Count; i++)
				//((DBText)textEntities[i]).TextString = textCollection[i];

			}

			// Position of pipe text (relative to pipe middle point, in direction from start point to end point.):
			// *Upper left: material
			// *Upper center: arrow
			// *Upper right: diameter
			// *Lower left: slope
			// *Lower right: lenght
			// Distance between text in the same line: arrow width.
			// Text position is calculated from the bottom left of the text.

			List<SingleLabelItem> singleTexts = new List<SingleLabelItem>();
			for (int i = 0; i < textEntities.Count; i++)
			{
				DBText txt = (DBText)textEntities[i];
				SingleLabelItem sl = new SingleLabelItem(txt, textCollection[i]);
				singleTexts.Add(sl);
			}

			//List<MyUtilities.Geometry.Rectangle<double>> textExtents = textEntities.Select(x => getRectangle((DBText)x)).ToList();
			//List<SingleLabelItem> singleTexts = textExtents.Select(x => new SingleLabelItem(x)).ToList();

			// Make extents bigger, because no space is applied.
			var arrowExtents = new MyUtilities.Geometry.Rectangle<double>(0, 0, arrowHeight * 2, arrowWidth * (useSpacing ? 1 : 1.3));
			var arrowSingleText = new SingleLabelItem(arrowExtents) { IsArrow = true };

			calculateTextPositions(new Point3d(middlePoint.X, middlePoint.Y, middlePoint.Z), pipeTotalWidth, pipeLength2d, true, arrowWidth, arrowHeight,
									spacingBetweenText,
									singleTexts.Concat(new List<SingleLabelItem> { arrowSingleText }).ToArray());
			//diameterSingleText, materialSingleText, lengthSingleText, slopeSingleText, arrowSingleText);

			double diameterOffset = pipeTotalWidth / 2.0;
			for (int i = 0; i < textEntities.Count; i++)
			{
				((DBText)textEntities[i]).Position = singleTexts[i].Position;
			}
			// By default arrow is pointing from start point to end point.
			// But text angle must always be -Pi/2:Pi/2, so if text angle is Pi/2:3/2Pi then 
			// text is inverted and arrow must point to the opposite side.
			double angleRad = MyUtilities.Helpers.Calculations.GetAngleUserTextAdjustedRad(pipeVector.GetAs2d());
			bool invertedArrow = Math.Cos(angleRad) < 0;
			// If pipe is upstream, arrow goes other way.
            if (pipeVector.Z > 0) 
                invertedArrow = !invertedArrow;

			var arrow = data.GetArrow(tw, true);// tw.CreateEntity<Polyline>();
			arrow.Layer = pipe.Layer;
			//arrow.AddVertexAt(vertexIndexes[0], new Point2d(arrowSingleText.Position.X, arrowSingleText.Position.Y), 0, 0, 0);
			//arrow.AddVertexAt(vertexIndexes[1], new Point2d(arrowSingleText.Position.X + arrowWidth / 2, arrowSingleText.Position.Y),	 0, arrowHeight, 0);
			//arrow.AddVertexAt(vertexIndexes[2], new Point2d(arrowSingleText.Position.X + arrowWidth, arrowSingleText.Position.Y), 0, 0, 0);
			var arT = arrowSingleText.Position;
            var points = new[]
            {
                new PolylineVertex<Point2d>(new Point2d(arT.X, arT.Y)),
                new PolylineVertex<Point2d>(new Point2d(arT.X + arrowWidth / 2, arT.Y), arrowHeight, 0),
                new PolylineVertex<Point2d>(new Point2d(arT.X + arrowWidth, arT.Y))
            };
            if (invertedArrow)
                points = points.Reverse().ToArray();
			arrow.ReplacePoints(points);
			arrow.Elevation = middlePoint.Z;

			double rotationAngle = angleRad;
			if (rotationAngle < 0)
				rotationAngle += 2 * Math.PI;
			if (Math.Cos(rotationAngle) < 0)
				rotationAngle -= Math.PI;
			Matrix3d rotation = Matrix3d.Rotation(rotationAngle, Vector3d.ZAxis, new Point3d(middlePoint.X, middlePoint.Y, middlePoint.Z));// middlePoint.ToCADPoint());
			textEntities.ForEach(x => x.TransformBy(rotation));
			arrow.TransformBy(rotation);

			foreach (var st in singleTexts)
				st.Dispose();
		}

		/// <summary>
		/// Settings for single text displayed for pipe label.
		/// </summary>
		private class SingleLabelItem : IDisposable
		{
			public bool IsArrow = false;
			public MyUtilities.Geometry.Rectangle<double> Rectangle { get; private set; }
			public Point3d Position;
			public SingleText Text { get; private set; }
			private DBText textEntity { get; set; }
			public bool IsTextEntity { get; private set; }
			public SingleLabelItem(MyUtilities.Geometry.Rectangle<double> rec, Point3d pt = new Point3d())
			{
				Rectangle = rec;
				Position = pt;
				IsTextEntity = false;
			}
			public SingleLabelItem(DBText dbText, SingleText t, Point3d pt = new Point3d())
			{
				Position = pt;
				Text = t;
				textEntity = dbText;
				IsTextEntity = textEntity != null;
				RefreshRectangle();
			}
			/// <summary>
			/// This item is last in row.
			/// </summary>
			public void SetBreakOnItem()
			{
				if (textEntity != null)
				{
					Text.UseShorterText();
					textEntity.TextString = Text.SelectedText;
					RefreshRectangle();
				}
			}
			/// <summary>
			/// Refreshes rectangle from current DBText.
			/// </summary>
			private void RefreshRectangle()
			{
				if (!IsTextEntity)
					return;
				MyUtilities.Geometry.Rectangle<double> rec = new MyUtilities.Geometry.Rectangle<double>();
				var ext = textEntity.GeometricExtents;
				rec.X = ext.MinPoint.X;
				rec.Y = ext.MinPoint.Y;
				rec.Width = ext.MaxPoint.X - ext.MinPoint.X;
				rec.Height = ext.MaxPoint.Y - ext.MinPoint.Y;
				Rectangle = rec;
			}

			public void Dispose()
            {
				textEntity?.Dispose();
            }
		}
		/// <summary>
		/// Checks if all text can fit to a single row.
		/// </summary>
		/// <param name="pipeWidth"></param>
		/// <param name="texts"></param>
		/// <param name="tester">Used to test for extents (according to current DB settings)</param>
		/// <returns></returns>
		private static bool DoesFitToSingleLine(double pipeWidth, double arrowWidth, DBText tester, params string[] textsWithSpacing)
		{
			bool res = false;
			double width = 0;
			width += arrowWidth;
			foreach (string s in textsWithSpacing)
			{
				tester.TextString = s;
				var extents = tester.GeometricExtents.ToRectangle();
				width += extents.Width;
			}
			if (width < 0.9 * pipeWidth)
				res = true;
			return res;
		}
		/// <summary>
		/// Calculates text positions in a single row or in multiple rows, when pipe length is too small.
		/// </summary>
		/// <param name="center"></param>
		/// <param name="pipeWidth"></param>
		/// <param name="pipeLenght2d"></param>
		/// <param name="offsetByPipeDiameter"></param>
		/// <param name="arrowWidth"></param>
		/// <param name="arrowHeight"></param>
		/// <param name="spacingBetweenText"></param>
		/// <param name="texts">Order: arrow, material, diameter,...</param>
		private static void calculateTextPositions(Point3d center,
													double pipeWidth,
													double pipeLenght2d,
													bool offsetByPipeDiameter,
													double arrowWidth, double arrowHeight,
													double spacingBetweenText,
													params SingleLabelItem[] texts)
		//SingleText materialText,
		//SingleText diameterText,
		//SingleText slopeText,
		//SingleText lengthText,
		//SingleText arrowText)
		{
			double pipeRadius = pipeWidth / 2;
			double offsetFromPipe = offsetByPipeDiameter ? pipeRadius : 0;
			// Offset between texts in the same row.
			double textHorizontalOffset = spacingBetweenText;// arrowWidth / 2;
															 // Offset between rows.
			double textRowOffset = textHorizontalOffset / 2;
			{
				//var arrowExtents = new MyUtilities.Geometry.Rectangle<double>(0, 0, arrowHeight, arrowWidth);
				//var arrowText = new SingleText(arrowExtents);
				var elements = texts.ToList();

				/*
				 * Labels are drawn at the "center" point (which is center point of the pipe).
				 * If pipe is short, then labels are divided into parts, trying to fit as many
				 * texts as possible in a single line. But, in any case, at least 1 text will be
				 * put in 1 line.
				 * 
				 * Process is done in 3 steps:
				 * 1 - Divide all texts into rows (depending on total text length and pipe length)
				 * 2 - Align texts in a single row, relative to (0,0) (they are aligned respectively to x = 0, but y has 
				 *	   more parts:
				 *	   0 row - goes above the pipe, distanced for pipe radius (+ space between rows)
				 *	   1 row - goes below the pipe, distanced also for pipe radius (+ space between rows)
				 *	   other rows - go below line above by space between rows.
				 *	   )
				 * 3 - Make final alignment - here text rows are translated to absolute position (relative to "center"), 
				 *     and 2 possibilities for horizontal alignment between rows:
				 *     1 - center alignment - every row middleX is at "center.X" position.
				 *     2 - left alignment - widest row middleX is aligned to "center.X", and all other rows are aligned
				 *		   to the start of that row (all rows start x is equal)
				 *	   
				 */

				// Texts are divided into rows (inner list represents all texts in a single row).
				List<List<SingleLabelItem>> rows = new List<List<SingleLabelItem>>();
				double allowedTextWidth = pipeLenght2d * 0.9;
				int currentRow = 0;
				while (elements.Any())
				{
					// Width of all remaining elements.
					double remainingWidth = elements.Sum(x => x.Rectangle.Width + textHorizontalOffset) - textHorizontalOffset;
					bool isSingleLine = remainingWidth < allowedTextWidth;

					if (isSingleLine)
					{
						// All remaining elements are in the single row.
						rows.Add(elements.ToList());
						// Stop iteration
						elements.Clear();
					}
					else
					{
						// Find how many texts can fit in current line.
						int rowCount = 0;
						double width = 0;
						for (int i = 0; i < elements.Count; i++)
						{
							var elem = elements[i]; 
							double w = elem.Rectangle.Width;
							if (width + textHorizontalOffset + w > allowedTextWidth)
							{
								if (i >= 1 && (elements[i - 1].Text?.AllowLineBreak ?? true))
								{
									elements[i - 1].SetBreakOnItem();
									rowCount = i;
									break;
								}
							}
							width += textHorizontalOffset + w;
						}
						// Every row must have at least 1 text.
						if (rowCount == 0)
							rowCount = 1;
						//elements[rowCount - 1].SetBreakOnItem();
						// Diameter and material are in the same row.
						//if (currentRow == 1)
						//rowCount = 2;// arrow, diameter and material are in the same row.
						// Add new row.
						rows.Add(elements.GetRange(0, rowCount));
						// Remove elements added to the row.
						elements = elements.Skip(rowCount).ToList();
					}
					currentRow++;
				}

				// Texts are divided into rows. Now need to align them (y is same for single row, x 
				// is set starting from 0)
				// Top Y coordinate for current row (text goes up to this position).
				double rowTopY = offsetFromPipe + textRowOffset;
				// Needed so rowTopY is top for the first row.
				rowTopY += rows[0].Max(x => x.Rectangle.Height);
				for (int i = 0; i < rows.Count; i++)
				{
					var row = rows[i];
					double rowHeight = row.Max(x => x.Rectangle.Height);
					double rowBottomY = rowTopY - rowHeight;
					double currentX = 0;
					for (int j = 0; j < row.Count; j++)
					{
						var elem = row[j];
						elem.Position = new Point3d(currentX, rowBottomY, 0);
						if (elem.IsArrow)
							elem.Position = elem.Position.Add(new Vector3d(elem.Rectangle.Width * 0.2, rowHeight / 2, 0));

						currentX += elem.Rectangle.Width + textHorizontalOffset;
					}
					// For next row, set topY.
					rowTopY = rowBottomY - textRowOffset;
					// If this is first row, there is pipe diameter
					if (i == 0)
						rowTopY -= pipeRadius + pipeRadius + textRowOffset;
				}


				// Set rows final horizontal alignment, and positions at the "center".
				bool centerAligned = true;// If false, left aligned. If true, center aligned.
										  // If center aligned, need to know widest row.
				double totalMaxWidth = rows.Max(x => x.Sum(y => y.Rectangle.Width + textHorizontalOffset) - textHorizontalOffset);
				// Apply additionalOffset (text is left aligned, but center is not in the middle of widest row, but 
				// on the center of the arrow - first texts).
				double additionalOffsetX = centerAligned ? 0 : totalMaxWidth / 2;// - arrowWidth / 2;
																				 // But applied only when arrow is alone in the first row
																				 //if (rows[0].Count > 1)
																				 //additionalOffsetX = 0;
				foreach (List<SingleLabelItem> row in rows)
				{
					double offsetX = 0;
					if (centerAligned)
					{
						double rowWidth = row.Sum(x => x.Rectangle.Width + textHorizontalOffset) - textHorizontalOffset;
						offsetX = -rowWidth / 2;
					}
					else
						offsetX = -totalMaxWidth / 2;
					// Move to final position, and align horizontaly.
					Vector3d moveX = new Vector3d(offsetX + additionalOffsetX + center.X, center.Y, center.Z);
					foreach (SingleLabelItem st in row)
						st.Position = st.Position.Add(moveX);
				}
			}
		}

	}
}
