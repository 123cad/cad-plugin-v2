﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SewageUtilities.PipeLabels
{
	/// <summary>
	/// Single text can be with minus at the end, but if that text is last in line
	/// then minus is excluded.
	/// If text doesn't have minus at all, then 2 cases are not different.
	/// </summary>
	public class SingleText
	{
		/// <summary>
		/// Text which is selected for display.
		/// </summary>
		public string SelectedText { get; private set; }
		/// <summary>
		/// Text display with minus (if needed) included.
		/// </summary>
		private string TextIncludeMinus { get; set; }
		/// <summary>
		/// Text without minus (if exists).
		/// </summary>
		private string TextExcludeMinus { get; set; }

		/// <summary>
		/// Is line break allowed after this text.
		/// </summary>
		public bool AllowLineBreak { get; private set; } = true;

		/// <summary>
		/// Initialize with text which is same in all cases.
		/// </summary>
		/// <param name="text"></param>
		public SingleText(string text, bool allowBreak = true)
		{
			SelectedText = TextIncludeMinus = TextExcludeMinus = text;
			AllowLineBreak = allowBreak;
		}
		/// <summary>
		/// Initialize texts, with longer (with minus) is default.
		/// </summary>
		/// <param name="textWithMinus"></param>
		/// <param name="textWithoutMinus"></param>
		public SingleText(string textWithoutMinus, string textWithMinus, bool allowBreak = true)
		{
			TextIncludeMinus = textWithMinus;
			TextExcludeMinus = textWithoutMinus;
			SelectedText = TextIncludeMinus;
			AllowLineBreak = allowBreak;
		}
		/// <summary>
		/// SelectedText is longer (with minus).
		/// </summary>
		public void UseShorterText()
		{
			SelectedText = TextExcludeMinus;
		}
		/// <summary>
		/// SelectedText is shorter (without minus).
		/// </summary>
		public void UseLongerText()
		{
			SelectedText = TextIncludeMinus;
		}
	}
}
