﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SewageUtilities.PipeLabels
{
    public enum LabelTextScale
    {
        /// <summary>
        /// Text height 0.5.
        /// </summary>
        S250 = 1,
        /// <summary>
        /// Text height 0.8.
        /// </summary>
        S500 = 2,
        /// <summary>
        /// Text height 1.6.
        /// </summary>
        S1000 = 3
    }

    public static class LabelTextScaleConverter
    {
        public const LabelTextScale DefaultScale = LabelTextScale.S500;
        public static double GetTextSize(this LabelTextScale scale)
        {
            double d = 1;
            switch (scale)
            {
                case LabelTextScale.S250:
                    d = 0.5;
                    break;
                case LabelTextScale.S500:
                    d = 0.8;
                    break;
                case LabelTextScale.S1000:
                    d = 1.6;
                    break;
            }
            return d;
        }
    }
}
