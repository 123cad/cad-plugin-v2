﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SewageUtilities.PipeLabels
{
    public class DrawLabelSettings
    {
        /// <summary>
        /// When true, pipe label will be drawn at offset (outside of pipe),
        /// which depends on diameter.
        /// When false, pipe label is drawn on center line of pipe.
        /// </summary>
        public bool ApplyPipeOffset { get; set; }
    }
}
