﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace SewageUtilities.PipeLabels.LabelPositions
{
    public abstract class PipeLabelBase : ILabelPosition
    {
        public bool Is2d { get; set; }
        public Point3d Start { get; set; }
        public Point3d End { get; set; }

        protected PipeLabelBase(bool is2d) : this(new Point3d(), new Point3d(), is2d)
        {

        }
        protected PipeLabelBase(Point3d start, Point3d end, bool is2d)
        {
            Start = start;
            End = end;
            Is2d = is2d;
        }

        public abstract Point3d PositionFromParameter(double parameter2d);

        public abstract Point3d PositionFromDistance(double distance2d);
    }
}
