﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace SewageUtilities.PipeLabels.LabelPositions
{
    public class PipeLabelLine : PipeLabelBase
    {
        public PipeLabelLine(bool is2d = false):base(is2d)
        { }
        public PipeLabelLine(Point3d start, Point3d end, bool is2d = false) : base(start, end, is2d)
        {
        }

        public override Point3d PositionFromDistance(double distance2d)
        {
            var pipe = Start.GetVectorTo(End);
            var pipe2d = pipe.GetWithZ();
            if (pipe2d.Length < 1e-3)
                return Start;
            if (distance2d < 0)
                distance2d = 0;
            if (distance2d > pipe2d.Length)
                distance2d = pipe2d.Length;
            var parameter2d = distance2d / pipe2d.Length;
            return PositionFromParameter(parameter2d);
        }

        public override Point3d PositionFromParameter(double parameter2d)
        {
            if (parameter2d < 0)
                parameter2d = 0;
            if (parameter2d > 1)
                parameter2d = 1;
            var pipe = Start.GetVectorTo(End);
            if (pipe.Length < 1e-3)
                return Start;
            var pt = Start.Add(pipe * parameter2d);
            if (Is2d)
                pt = pt.GetOnZ();
            return pt;
        }
    }
}
