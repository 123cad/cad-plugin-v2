﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace SewageUtilities.PipeLabels.LabelPositions
{

    public class PipeLabelArc : PipeLabelBase
    {
        /// <summary>
        /// Point on the arc (can be middle, or anything else).
        /// </summary>
        public Point3d ArcPoint { get; set; }

        public PipeLabelArc(bool is2d = false):base(is2d)
        {
            ArcPoint = new Point3d();
        }
        public PipeLabelArc(Point3d start, Point3d end, Point3d arcPoint, bool is2d) : base(start, end, is2d)
        {
            ArcPoint = arcPoint;
        }
        public override Point3d PositionFromParameter(double parameter2d)
        {
            if (parameter2d < 0)
                parameter2d = 0;
            if (parameter2d > 1)
                parameter2d = 1;
            using (var arc = new CircularArc2d(Start.To2d(), ArcPoint.To2d(), End.To2d()))
            {
                var d = arc.GetLength(0, 1);
                if (d < 1e-3)
                    return Start;
                // Arc can be rotated in any position, and we need specific one.
                // Easier solution is to use Polyline with transaction.
                //using(var pl = new Polyline())
                arc.ReferenceVector = arc.Center.GetVectorTo(Start.To2d());
                var doc = Application.DocumentManager.MdiActiveDocument;
                double maxParam = arc.GetParameterOf(End.To2d());
                var pt = arc.EvaluatePoint(parameter2d * maxParam);
                /*using (TransactionWrapper tw = doc.StartTransaction())
                {
                    var pl = tw.CreateEntity<Polyline>();
                    pl.AddPoints(
                        arc.Center,
                        arc.StartPoint,
                        pt,
                        arc.EndPoint
                        );

                    // Do commit even for readonly operations.
                    tw.Commit();
                }*/


                // Because parameter doesn't go [0-1].
                //pt = Start.GetAs2d().Add(pt.GetAsVector());
                double tempZ = (Start.Z - End.Z) * parameter2d;
                double z = Math.Min(Start.Z, End.Z) + Math.Abs(tempZ);
                var point = pt.GetAs3d(z);
                if (Is2d)
                    point.GetOnZ();
                return point;
            }
        }
        public override Point3d PositionFromDistance(double distance2d)
        {
            using (var arc = new CircularArc2d(Start.To2d(), End.To2d(), ArcPoint.To2d()))
            {
                var d = arc.GetLength(0, 1);
                if (d < 1e-3)
                    return Start;
                if (distance2d < 0)
                    distance2d = 0;
                if (distance2d > d)
                    distance2d = d;
                var parameter = distance2d / d;
                return PositionFromParameter(parameter);
            }
        }
    }
}
