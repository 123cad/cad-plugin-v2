﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace SewageUtilities.PipeLabels
{
    /// <summary>
    /// Calculates position of pipe (or pipe segment) where 
    /// label should be created.
    /// </summary>
    public interface ILabelPosition
    {
        Point3d Start { get; set; }
        Point3d End { get; set; }

        /// <summary>
        /// Should position be on z=0.
        /// </summary>
        bool Is2d { get; set; }
        /// <summary>
        /// Parameter range is [0-1]. 0 is at start point, 1 is at end point.
        /// <para>IMPORTANT: If parameter is != 0.5, need to recalculate angle 
        /// of the label. When it is 0.5, it is middle of the arc, 
        /// and tangent is parallel to direct segment from Start to End.</para>
        /// </summary>
        /// <param name="parameter2d"></param>
        /// <returns></returns>
        Point3d PositionFromParameter(double parameter2d = 0.5);
        /// <summary>
        /// Distance from start point to end point.
        /// If distance is too small or too big it is set to 
        /// closest valid distance.
        /// </summary>
        /// <param name="distance"></param>
        /// <returns></returns>
        Point3d PositionFromDistance(double distance2d);
    }
    
    
}
