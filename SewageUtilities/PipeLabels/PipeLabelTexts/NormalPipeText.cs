﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SewageUtilities.PipeLabels.IPipeLabelTexts
{
    public class NormalPipeText
    {
		public static NormalPipeText Instance { get; }
		static NormalPipeText()
        {
			Instance = new NormalPipeText();
        }
		public IEnumerable<SingleText> GetTexts(PipeData pipe)
		{
			string s;
			List<SingleText> l = new List<SingleText>();

			int diameter = (int)Math.Round(pipe.Diameter * 1000, 0);
			s = diameter + " ";
			l.Add(new SingleText(s, false));

			s = pipe.Material + " ";
			l.Add(new SingleText(s, s + " - "));

			s = DoubleHelper.ToStringInvariant(Math.Abs(pipe.Length2d), 2) + "";
			l.Add(new SingleText(s, s + " - "));

			s = DoubleHelper.ToStringInvariant(Math.Abs(pipe.SlopePermill), 1) + "‰";
			l.Add(new SingleText(s));

			return l;
		}
	}
}
