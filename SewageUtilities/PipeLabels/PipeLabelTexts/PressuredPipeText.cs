﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SewageUtilities.PipeLabels.IPipeLabelTexts
{
    public class PressuredPipeText
    {
		public static PressuredPipeText Instance { get; }

		static PressuredPipeText()
        {
			Instance = new PressuredPipeText();
        }

		public IEnumerable<SingleText> GetTexts(PipeDataPressure pipe)
		{

			string s;
			List<SingleText> l = new List<SingleText>();

			int diameter = (int)Math.Round(pipe.Diameter * 1000, 0);
			s = diameter + " ";
			l.Add(new SingleText(s, false));

			s = pipe.Material + " ";
			l.Add(new SingleText(s, s + " - "));

			s = "PN " + DoubleHelper.ToStringInvariant(Math.Abs(pipe.PipePressure), 1) + "";
			l.Add(new SingleText(s, s + " - "));

			s = DoubleHelper.ToStringInvariant(Math.Abs(pipe.Length2d), 1);
			l.Add(new SingleText(s));

			return l;
		}
	}
}
