﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SewageUtilities.PipeLabels.IPipeLabelTexts
{
	/// <summary>
	/// Label for connection pipe (excludes length).
	/// </summary>
    public class ConnectionPipeText
	{
		public static ConnectionPipeText Instance { get; }
		static ConnectionPipeText()
        {
			Instance = new ConnectionPipeText();
        }
		public IEnumerable<SingleText> GetTexts(PipeData pipe)
		{
			string s;
			List<SingleText> l = new List<SingleText>();

			int diameter = (int)Math.Round(pipe.Diameter * 1000, 0);
			s = diameter + " ";
			l.Add(new SingleText(s, false));

			s = pipe.Material + " ";
			l.Add(new SingleText(s, s + " - "));

			//s = DoubleHelper.ToStringInvariant(Math.Abs(pipe.Length2d), 2) + "";
			//l.Add(new SingleText(s, s + " - "));

			s = DoubleHelper.ToStringInvariant(Math.Abs(pipe.SlopePermill), 1) + "‰";
			l.Add(new SingleText(s));

			return l;
		}
	}
}
