﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace SewageUtilities.PipeLabels
{
    /// <summary>
    /// Basic data which is needed to draw label for shaft.
    /// </summary>
    public class ShaftData
    {
        public string Material { get; set; }
        /// <summary>
        /// Diameter in meters.
        /// </summary>
        public double Diameter { get; set; }
        /// <summary>
        /// Bottom of the shaft.
        /// </summary>
        public Point3d SMP { get => _smp; set { _smp = value; refresh(); } }        
        /// <summary>
        /// Top of the shaft.
        /// </summary>
        public Point3d DMP { get => _dmp; set { _dmp = value; refresh(); } }
        /// <summary>
        /// 
        /// </summary>
        public double Depth { get; private set; }


        private Point3d _smp = new Point3d();
        private Point3d _dmp = new Point3d();


        private void refresh()
        {
            Depth = _dmp.Z - _smp.Z;
        }
    }
}
