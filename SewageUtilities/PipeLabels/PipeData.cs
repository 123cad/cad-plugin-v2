﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.SewageHelpers;
using SewageUtilities.PipeLabels.IPipeLabelTexts;
using SewageUtilities.PipeLabels.LabelPositions;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace SewageUtilities.PipeLabels
{
    /// <summary>
    /// Basic data which is needed to draw a pipe label.
    /// No arcs are allowed.
    /// </summary>
    public class PipeData
    {
        public static string DefaultLayer = "123_Rohre_Beschriftung";

        public string Layer { get; set; } = DefaultLayer;
        public string Material { get; set; }
        /// <summary>
        /// Diameter in meters.
        /// </summary>
        public double Diameter { get; set; }
        /// <summary>
        /// Start point of bottom of the pipe.
        /// </summary>
        public Point3d StartPoint { get => _start; set { _start = value; refresh(); } }
        /// <summary>
        /// End point of bottom of the pipe.
        /// </summary>
        public Point3d EndPoint { get => _end; set { _end = value; refresh(); } }
        /// <summary>
        /// Set SMP length here (3d length, of SMP points from start to end shaft). 
        /// </summary>
        public double Length3d { get; set; }
        /// <summary>
        /// Set DMP length here (2d length, of DMP points from start to end shaft). 
        /// </summary>
        public double Length2d { get; set; }
        /// <summary>
        /// Relative to downstream (positive when start.Z is above end.Z).
        /// </summary>
        public double SlopePermill { get; private set; }
        /// <summary>
        /// Calculates position where label should be (line or arc).
        /// <para>By default, label for Line is used. If arc is needed, 
        /// assign LabelPosition PipeLabelArc.</para>
        /// </summary>
        public ILabelPosition LabelPosition { get; set; } = new PipeLabelLine();

        public LabelTextScale Scale { get; set; } = LabelTextScale.S250;

        private Point3d _start = new Point3d();
        private Point3d _end = new Point3d(); 
        public void SetPoint(Point3d start, Point3d end)
        {
            _start = start;
            _end = end;
            refresh();
        }
        private void refresh()
        {
            // This depends on shafts, and must be set manually.
            //var v = _start.GetVectorTo(_end);
            //Length3d = v.Length;
            //v = _start.GetOnZ().GetVectorTo(_end.GetOnZ());
            //Length2d = v.Length;
            SlopePermill = PipeSlope.CreateFromPoints(_start.FromCADPoint(), _end.FromCADPoint()).SlopePercent * 10;
            LabelPosition.Start = StartPoint;
            LabelPosition.End = EndPoint;
        }

        public virtual IEnumerable<SingleText> GetText() 
	        => NormalPipeText.Instance.GetTexts(this);
    }

    public class PipeDataPressure:PipeData
    {
        public double PipePressure { get; set; } = 0;

        public override IEnumerable<SingleText> GetText() 
	        => PressuredPipeText.Instance.GetTexts(this);
		
    }

    public class PipeDataConnection : PipeData
    {
	    public override IEnumerable<SingleText> GetText()
		    => ConnectionPipeText.Instance.GetTexts(this);
    }
}
