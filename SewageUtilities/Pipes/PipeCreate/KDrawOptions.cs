﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SewageUtilities.Pipes.PipeCreate
{
    public class KDrawOptions
    {
        public PipeWaterType PipeType { get; set; } = PipeWaterType.REGENWASSER;

        /// <summary>
        /// Diameter in meters.
        /// </summary>
        public double PipeDiameter { get; set; } = 0.3;
        public string PipeMaterial{ get; set; }
        public PipePressureSystem PipeSystem { get; set; } = PipePressureSystem.Freispiegelleitung;
        /// <summary>
        /// Effective only when PipeSystem is pressured.
        /// </summary>
        public double PipePressure{ get; set; }

        public bool DrawLabels { get; set; } = true;
        public bool DrawPipe { get; set; } = true;
    }
}
