﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace SewageUtilities.Pipes.PipeCreate
{
    public class KDrawDataSinglePipe
    {
        public Point3d StartPipe { get; set; }
        public Point3d EndPipe{ get; set; }

        public Point3d? StartShaft { get; set; }
        public Point3d? EndShaft { get; set; }
    }

    public class KDrawDataFinal
    {
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Point3d> Points => _points;

        private List<Point3d> _points = new List<Point3d>();

        /// <summary>
        /// Use this when there is no inner slope in shaft.
        /// </summary>
        public void AddShaft(Point3d smp)
        {
            _points.Add(smp);
        }

        /// <summary>
        /// First shaft doesn't have pipeIn, last one doesn't have pipeOut.
        /// </summary>
        public void AddShaft(Point3d? pipeIn, Point3d smp, Point3d? pipeOut)
        {
            if (pipeIn.HasValue)
                _points.Add(pipeIn.Value);
            _points.Add(smp);
            if (pipeOut.HasValue)
                _points.Add(pipeOut.Value);
        }
    }
}
