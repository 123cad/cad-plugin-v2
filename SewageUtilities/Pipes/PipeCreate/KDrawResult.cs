﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedUtilities.CAD;


#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif


namespace SewageUtilities.Pipes.PipeCreate
{
    public class KDrawResult : TransactionWrapperObserverBase
    {
        public List<ObjectId> CreatedObjects { get; } = new List<ObjectId>();

        
        protected override void EntityCreated(TransactionWrapper tw, Entity e)
        {
            CreatedObjects.Add(e.Id);
        }

        protected override void Detached(TransactionWrapper tw)
        {
            // Keep only unerased objects.
            foreach (var id in CreatedObjects.ToList())
                if (tw.GetObjectRead<Entity>(id, true).IsErased)
                    CreatedObjects.Remove(id);
        }
    }
}
