﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace SewageUtilities.Pipes.PipeCreate
{
    public class KDrawLayerManager
    {
        public static string PipeLayerName = "123_KDraw_LinienTyp";
        /// <summary>
        /// Layer on which 3dpoly of all pipes is created.
        /// </summary>
        public static string pipe3dPolyLayerName = "123_KDraw_SohlLinie";
        /// <summary>
        /// Layer on which colored 3 polylines are drawn (with width).
        /// </summary>
        public static string pipeColoredLayerName = "123_KDraw_LinienTyp_Farbe";
        /// <summary>
        /// Text which describes pipe.
        /// </summary>
        public static string pipeTextLayerName = "123_KDraw_Texte";

        public static void InitializeLayers(Transaction tr, LayerTable lt)
        {
            string[] layers = new string[]
            {
                PipeLayerName,
                pipe3dPolyLayerName,
                pipeColoredLayerName,
                pipeTextLayerName
            };
            foreach (var v in layers)
                loadLayer(tr, lt, v);
        }

        private static void loadLayer(Transaction tr, LayerTable lt, string name)
        {
            if (!lt.Has(name))
            {
                lt.UpgradeOpen();
                LayerTableRecord ltr = new LayerTableRecord();
                ltr.Name = name;
                lt.Add(ltr);
                tr.AddNewlyCreatedDBObject(ltr, true);
                lt.DowngradeOpen();
            }
        }
        public static void InitializeLayers(Document doc, params string[] names)
        {
            Database db = doc.Database;

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                LayerTable lt = tr.GetObject(db.LayerTableId, OpenMode.ForWrite) as LayerTable;
                foreach (var n in names)
                    loadLayer(tr, lt, n);

                // Do commit even for readonly operations.
                tr.Commit();
            }

        }
	}
}
