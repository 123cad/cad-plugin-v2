﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace SewageUtilities.Pipes.PipeCreate
{
    public class KDrawLinetypeManager
    {
		public static readonly string ContinuousArrow = "Continuous_arrow";
		/// <summary>
		/// Line of 2, space of 1.
		/// </summary>
		public static readonly string Dashed_21 = "DASHED_21";
		/// <summary>
		/// Arrow which fits 21 line.
		/// </summary>
		public static readonly string Dashed_21_Arrow = "DASHED_21_arrow";
		/// <summary>
		/// Line of 3, space of 1, a point, space of 1.
		/// </summary>
		public static readonly string Dashed_31p1 = "DASHED_31p1";
		/// <summary>
		/// Arrow which fits 31p1 line.
		/// </summary>
		public static readonly string Dashed_31p1_arrow = "DASHED_31p1_arrow";

		// If linetype with arrows works, this is not needed. (NOT EXPLORED)
		private static LinetypeSegment segment_continuous;
		private static LinetypeSegment segment_21;
		private static LinetypeSegment segment_31p1;

		/// <summary>
		/// Checks if used Linetypes are loaded and loads them if they are not.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="lt"></param>
		public static void LoadLinetypes(Transaction tr, LinetypeTable lt)
		{
			double scale = 2;
			if (!lt.Has(Dashed_21))
			{
				lt.UpgradeOpen();
				LinetypeTableRecord ltr = new LinetypeTableRecord();
				ltr.Name = Dashed_21;
				ltr.Comments = Dashed_21 + " , __ __ __ ";
				lt.Add(ltr);
				tr.AddNewlyCreatedDBObject(ltr, true);
				ltr.NumDashes = 2;
				ltr.SetDashLengthAt(0, 1 / scale);
				ltr.SetDashLengthAt(1, -0.5 / scale);
				ltr.Dispose();
				lt.DowngradeOpen();
			}
			if (!lt.Has(Dashed_21_Arrow))
			{
				lt.UpgradeOpen();
				LinetypeTableRecord ltr = new LinetypeTableRecord();
				ltr.Name = Dashed_21_Arrow;
				ltr.Comments = Dashed_21_Arrow + " , __ __ __ ";
				lt.Add(ltr);
				tr.AddNewlyCreatedDBObject(ltr, true);
				ltr.NumDashes = 3;
				// How to set anything except dash?
				//Doesn'
				//ltr.SetDashLengthAt(0, 1 / scale);
				//ltr.SetShapeRotationAt(0, 30 * Math.PI / 180);
				ltr.SetDashLengthAt(0, 1 / scale);
				ltr.SetTextAt(1, "\\");
				ltr.SetShapeScaleAt(1, 3);
				ltr.SetDashLengthAt(2, 0.5 / scale);
				// Not working
				ltr.Dispose();
				lt.DowngradeOpen();
			}
			if (!lt.Has(Dashed_31p1))
			{
				lt.UpgradeOpen();
				LinetypeTableRecord ltr = new LinetypeTableRecord();
				ltr.Name = Dashed_31p1;
				lt.Add(ltr);
				tr.AddNewlyCreatedDBObject(ltr, true);
				ltr.Comments = Dashed_31p1 + " , ___ . ___ . ___ . ";
				ltr.NumDashes = 4;
				ltr.SetDashLengthAt(0, 1 / scale);
				ltr.SetDashLengthAt(1, -1.0 / 3 / scale);
				ltr.SetDashLengthAt(2, 0.05 / scale);
				ltr.SetDashLengthAt(3, -1.0 / 3 / scale);
				ltr.Dispose();
				lt.DowngradeOpen();
			}
		}

		public class LinetypeSegment
		{
			/// <summary>
			/// Length of repetable segment.
			/// </summary>
			public double SegmentLength { get; set; }
			/// <summary>
			/// Lenght of longest line section.
			/// </summary>
			public double LineSectionLength { get; set; }
			/// <summary>
			/// Position of longest line middle, relative to start of segment.
			/// </summary>
			public double LineMiddleOffset { get; set; }
		}
		private class LinetypeWithArrows
		{
			public int TextVertexIndex = 0;
		}
		public static LinetypeSegment GetLinetypeSegment(PipeWaterType pt)
		{
			LinetypeSegment ls = null;
			switch (pt)
			{
				case PipeWaterType.MISCHWASSER:
					ls = segment_continuous;
					break;
				case PipeWaterType.REGENWASSER:
					ls = segment_21;
					break;
				case PipeWaterType.SCHMUTZWASSER:
					ls = segment_31p1;
					break;
			}
			return ls;
		}

		public static string GetLineTypeforPipe(PipeWaterType pt)
		{
			string lineTypeName = "CONTINUOUS";
			switch (pt)
			{
				case PipeWaterType.REGENWASSER:
					lineTypeName = Dashed_21;
					break;
				case PipeWaterType.MISCHWASSER:
					lineTypeName = Dashed_31p1;
					break;
				case PipeWaterType.SCHMUTZWASSER:
					lineTypeName = "CONTINUOUS";
					break;
			}
			return lineTypeName;
		}
	}
}
