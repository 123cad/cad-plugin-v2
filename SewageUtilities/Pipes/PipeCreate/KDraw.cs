﻿using SewageUtilities.Pipes.PipeCreate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SewageUtilities.PipeLabels;
using SewageUtilities.Helpers;


#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif


namespace SewageUtilities.Pipes.PipeCreaters
{
    /// <summary>
    /// Port of KDraw command.
    /// Has 2 options, let user to select data or read data.
    /// </summary>
    public class KDraw
    {
        private static bool initialized = false;
        public KDrawResult DrawPipe(TransactionWrapper tw, KDrawOptions options, KDrawDataSinglePipe data)
        {
            if (!initialized)
            {
                initialized = true;
                KDrawLayerManager.InitializeLayers(tw.Tr, tw.LayerTable);
                KDrawLinetypeManager.LoadLinetypes(tw.Tr, tw.LinetypeTable);
            }
            var result = new KDrawResult();
            tw.AddObserver(result);
            if (options.DrawPipe)
                createPipe(tw, options, data);

            Vector2d
                line2d = data.StartPipe.To2d()
                    .GetVectorTo(data.EndPipe 
                        .To2d()); // new Vector2d(data.EndPipe.X, points.PipeIn.Y) - new Vector2d(points.PipeOut.X, points.PipeOut.Y);
            Vector3d line = data.StartPipe.GetVectorTo(data.EndPipe); // points.PipeOut.GetVectorTo(points.PipeIn);
            // Gets relative angle between vectors [0 - Pi]
            double angle = line2d.GetAngleTo(Vector2d.XAxis);
            // If Y is negative, angle should be negated (but we also move it 2Pi forward, so it is positive [0-2*Pi]).
            if (line2d.Y < 0)
                angle = Math.PI * 2 - angle;
            //var smpPipe = points.PipeOut.GetVectorTo(points.PipeIn);
            var startSmp = data.StartShaft.HasValue ? data.StartShaft.Value : data.StartPipe;
            var endSmp = data.EndShaft.HasValue ? data.EndShaft.Value : data.EndPipe;
            double smpLength2d =
                startSmp.GetAs2d()
                    .GetDistanceTo(endSmp.GetAs2d()); // new Vector2d(smpPipe.X, smpPipe.Y).Length;// line2d.Length;

            //CHECK Displayed length of pipe is smp length, but slope is only pipe.

            // Create displayed text at the pipe middle point.

            if (options.DrawLabels)
            {
                switch (options.PipeSystem)
                {
                    case PipePressureSystem.Freispiegelleitung:
                        var d = new PipeData()
                        {
                            Diameter = options.PipeDiameter, //.SelectedDiameterMM / 1000.0,
                            Layer = KDrawLayerManager.pipeTextLayerName,
                            Material = options.PipeMaterial, //.SelectedMaterial,
                            StartPoint = data.StartPipe,
                            EndPoint = data.EndPipe,
                            Length2d = smpLength2d // points.StartShaftCenter.GetVectorTo(points.EndShaftCenter).Length
                        };
                        PipeLabelDrawer.DrawLabel(tw, d);
                        break;
                    case PipePressureSystem.Druckleitung:
                        var dataP = new PipeDataPressure()
                        {
                            Diameter = options.PipeDiameter,
                            Layer = KDrawLayerManager.pipeTextLayerName,
                            Material = options.PipeMaterial,
                            StartPoint = data.StartPipe,
                            EndPoint = data.EndPipe,
                            PipePressure = options.PipePressure,
                            Length2d = smpLength2d // points.StartShaftCenter.GetVectorTo(points.EndShaftCenter).Length
                        };
                        PipeLabelDrawer.DrawLabel(tw, dataP);
                        break;
                }
            }

            tw.RemoveObserver(result);
            return result;
        }
        
        public KDrawResult DrawFinal(TransactionWrapper tw, KDrawDataFinal data)
        {

            Polyline3d pl3d = tw.CreateEntity<Polyline3d>(KDrawLayerManager.pipe3dPolyLayerName); // new Polyline3d();
            pl3d.AddPoints(data.Points.ToArray());

            MyLog.MyTraceSource.Information("Created 3d poly for pipe.");
            var r = new KDrawResult();
            r.CreatedObjects.Add(pl3d.Id);
            return r;
        }

        private static void createPipe(TransactionWrapper tw, KDrawOptions options, KDrawDataSinglePipe data,
            bool useGraphic = true)
        {
            string lineTypeName = KDrawLinetypeManager.GetLineTypeforPipe(options.PipeType);
            // Color of the middle line.
            Color lineColor = SewageColorsDefinitions.GetEntwaesserungsartColor(options.PipeType);

            ObjectId lineTypeId = tw.LinetypeTable[lineTypeName];

            Vector3d startVector = data.StartPipe.GetAsVector();
            Vector3d endVector = data.EndPipe.GetAsVector();
            Vector3d pipeVector = endVector - startVector;
            pipeVector = new Vector3d(pipeVector.X, pipeVector.Y, 0);
            Vector3d normal = pipeVector.GetNormal();
            //Vector3d startVP = startVector + normal * startOffset;
            //Vector3d endVP = endVector - normal * endOffset;
            // Points which are closer to each other by offset (if )
            Point3d startPointP =
                new Point3d(startVector.X, startVector.Y, startVector.Z); // (startVP.X, startVP.Y, startVP.Z);
            Point3d endPointP = new Point3d(endVector.X, endVector.Y, endVector.Z); // (endVP.X, endVP.Y, endVP.Z);
            if (options.PipeDiameter < 1e-3 || !useGraphic)
            {
                Line l = tw.CreateEntity<Line>(KDrawLayerManager.PipeLayerName);
                l.StartPoint = startPointP;
                l.EndPoint = endPointP;
                l.LinetypeId = lineTypeId;

                // Create center line with pipe lines (parallel to center line, but with offset of radius)
                Vector3d lineVector = l.EndPoint.GetAsVector() - l.StartPoint.GetAsVector();
                Vector3d v = lineVector.GetNormal().RotateBy(Math.PI / 2, Vector3d.ZAxis);
                v *= options.PipeDiameter / 2;
                Matrix3d translate = Matrix3d.Displacement(v);
                Line pipeLeft =
                    tw.CreateEntity<Line>(KDrawLayerManager.PipeLayerName); // new Line(l.StartPoint, l.EndPoint);
                pipeLeft.StartPoint = l.StartPoint;
                pipeLeft.EndPoint = l.EndPoint;
                pipeLeft.LinetypeId = lineTypeId;
                pipeLeft.TransformBy(translate);

                v = v.RotateBy(Math.PI, Vector3d.ZAxis);
                translate = Matrix3d.Displacement(v);
                Line pipeRight =
                    tw.CreateEntity<Line>(KDrawLayerManager.PipeLayerName); // new Line(l.StartPoint, l.EndPoint);
                pipeRight.StartPoint = l.StartPoint;
                pipeRight.EndPoint = l.EndPoint;
                pipeRight.LinetypeId = lineTypeId;
                pipeRight.TransformBy(translate);
            }
            else
            {

                // Center is 4/6 diameter, side is 2 * 1/6 diameter
                double sideWidth = options.PipeDiameter / 6;
                double centerWidth = sideWidth * 4;
                Polyline center = tw.CreateEntity<Polyline>(KDrawLayerManager.pipeColoredLayerName); //) new Polyline();
                Vertex2d v2;
                Func<Point3d, Point2d> to2d = p => new Point2d(p.X, p.Y);
                v2 = new Vertex2d(startPointP, 0, centerWidth, centerWidth, 0);
                center.AddVertexAt(0, to2d(startPointP), 0, centerWidth, centerWidth); // .AppendVertex(v2);
                //tr.GetObject(vx, OpenMode.ForWrite).Erase();
                v2.Dispose(); 
                v2 = new Vertex2d(endPointP, 0, centerWidth, centerWidth, 0);
                center.AddVertexAt(1, to2d(endPointP), 0, centerWidth, centerWidth);
                //center.AppendVertex(v2);
                v2.Dispose();
                //tr.GetObject(vx, OpenMode.ForWrite).Erase();
                center.LinetypeId = lineTypeId;
                center.Color = lineColor;

                // Create center line with pipe lines (parallel to center line, but with offset of radius)
                Vector3d lineVector = center.EndPoint.GetAsVector() - center.StartPoint.GetAsVector();
                Vector3d v = lineVector.GetNormal().RotateBy(Math.PI / 2, Vector3d.ZAxis);
                v *= (centerWidth + sideWidth) / 2;
                Matrix3d translate = Matrix3d.Displacement(v);
                Polyline pipeLeft =
                    tw.CreateEntity<Polyline>(KDrawLayerManager.pipeColoredLayerName); // new Polyline();
                //v2= new Vertex2d(center.StartPoint, 0, sideWidth, sideWidth, 0);
                pipeLeft.AddVertexAt(0, to2d(center.StartPoint), 0, sideWidth, sideWidth);
                //pipeLeft.AppendVertex(v2);
                v2.Dispose();
                //v2 = new Vertex2d(center.EndPoint, 0, sideWidth, sideWidth, 0);
                pipeLeft.AddVertexAt(1, to2d(center.EndPoint), 0, sideWidth, sideWidth);
                //pipeLeft.AppendVertex(v2);
                v2.Dispose();
                pipeLeft.LinetypeId = lineTypeId;
                pipeLeft.TransformBy(translate);

                v = v.RotateBy(Math.PI, Vector3d.ZAxis);
                translate = Matrix3d.Displacement(v);
                Polyline pipeRight =
                    tw.CreateEntity<Polyline>(KDrawLayerManager.pipeColoredLayerName); // new Polyline();
                //v2= new Vertex2d(center.StartPoint, 0, sideWidth, sideWidth, 0);
                //pipeRight.AppendVertex(v2);
                pipeRight.AddVertexAt(0, to2d(center.StartPoint), 0, sideWidth, sideWidth);
                v2.Dispose();
                //v2= new Vertex2d(center.EndPoint, 0, sideWidth, sideWidth, 0);
                //pipeRight.AppendVertex(v2);
                pipeRight.AddVertexAt(1, to2d(center.EndPoint), 0, sideWidth, sideWidth);
                v2.Dispose();
                pipeRight.LinetypeId = lineTypeId;
                pipeRight.TransformBy(translate);
            }
        }
    }
}
