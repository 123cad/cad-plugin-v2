﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SewageUtilities.Pipes
{
    public enum PipePressureSystem
    {
        /// <summary>
        /// Normal pipe system (no pressure).
        /// </summary>
        Freispiegelleitung,
        /// <summary>
        /// Pressure pipe system.
        /// </summary>
        Druckleitung
    }
}
