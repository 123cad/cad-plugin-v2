﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SewageUtilities.Pipes
{
    /// <summary>
    /// Type of water in the pipes.
    /// </summary>
    public enum PipeWaterType
    {
        MISCHWASSER,
        REGENWASSER,
        SCHMUTZWASSER
    }

    public class PipeWaterTypeIsybauConverter
    {
        public static string ToIsybau(PipeWaterType p)
        {
            string s = "";
            switch (p)
            {
                case PipeWaterType.MISCHWASSER:
                    s = "KM";
                    break;
                case PipeWaterType.REGENWASSER:
                    s = "KR";
                    break;
                case PipeWaterType.SCHMUTZWASSER:
                    s = "KS";
                    break;
            }

            return s;
        }

        public static PipeWaterType ToWaterType(string isybauEA)
        {
            var r = PipeWaterType.REGENWASSER;
            switch (isybauEA.ToUpper().Trim())
            {
                case "KM":
                    r = PipeWaterType.MISCHWASSER;
                    break;
                case "KR":
                    r = PipeWaterType.REGENWASSER;
                    break;
                case "KS":
                    r = PipeWaterType.SCHMUTZWASSER;
                    break;
            }

            return r;
        }
    }

}
