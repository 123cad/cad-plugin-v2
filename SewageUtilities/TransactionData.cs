﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Common
{
	[Obsolete("Use TransactionWrapper for new code - this will be replaced eventually.")]
	public class TransactionData
	{
		public Document Doc { get; set; }
		public Database Db { get; set; }
		public Transaction Tr { get; set; }
		public BlockTable Bt { get; set; }
		public LayerTable Lt { get; set; }
		/// <summary>
		/// Model space.
		/// </summary>
		public BlockTableRecord Btr { get; set; }

        public TransactionData(Document doc, Transaction tr)
        {
            Doc = doc;
            Db = doc.Database;
            Tr = tr;
			Bt = tr.GetObjectRead<BlockTable>(Db.BlockTableId);// tr.GetObject(Db.BlockTableId, OpenMode.ForRead) as BlockTable;
			Btr = tr.GetObjectRead<BlockTableRecord>(Bt[BlockTableRecord.ModelSpace]);//, OpenMode.ForRead) as BlockTableRecord; //GetObject(Bt[BlockTableRecord.ModelSpace], OpenMode.ForRead) as BlockTableRecord;
			Lt = tr.GetObjectRead<LayerTable>(Db.LayerTableId);//, OpenMode.ForRead) as LayerTable;
        }
		public TransactionData(Document doc, Database db, Transaction tr, BlockTable bt, BlockTableRecord modelSpace)
		{
			Doc = doc;
			Db = db;
			Tr = tr;
			Bt = bt;
			Btr = modelSpace;
			Lt = tr.GetObject(Db.LayerTableId, OpenMode.ForRead) as LayerTable;
		}

		/// <summary>
		/// Just for easier access.
		/// </summary>
		/// <param name="wrapper"></param>
		public TransactionData(TransactionWrapper wrapper):this(wrapper.Doc, wrapper.Db, wrapper.Tr, wrapper.Bt, wrapper.MS)
		{

		}

		public T CreateEntity<T>() where T:Entity
		{
			return Tr.CreateEntity<T>(Btr);
		}

		/// <summary>
		/// Easier access.
		/// </summary>
		/// <returns></returns>
		public TransactionWrapper GetAsWrapper()
		{
			return new TransactionWrapper(Doc, Tr);
		}
	}
}
