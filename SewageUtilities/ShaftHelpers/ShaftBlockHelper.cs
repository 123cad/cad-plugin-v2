﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif



namespace SewageUtilities.ShaftHelpers
{
    public static class ShaftBlockHelper
    {
        private class AttributeData
        {
            public Point3d Position { get; set; }
            public AttributeReference Attribute { get; set; }
            public double TextHeight { get; set; }
            public string Text{ get; set; }

            public AttributeData(Point3d position, AttributeReference attribute, double textHeight, string text)
            {
                Position = position;
                Attribute = attribute;
                TextHeight = textHeight;
                Text = text;
            }

            public static AttributeData Create(TransactionWrapper tw, ObjectId attributeReferenceId)
            {
                var ar = tw.GetObjectWrite<AttributeReference>(attributeReferenceId);
                var res = new AttributeData(ar.Position, ar, ar.Height, ar.TextString);
                return res;
            }
        }

        private class BlockAttributeCollection
        {
            public TransactionWrapper Tw { get; private set; }
            public BlockReference BlockReference { get; private set; }
            public IEnumerable<AttributeData> Attributes => _attributes;
            private List<AttributeData> _attributes = new List<AttributeData>();

            private BlockAttributeCollection(TransactionWrapper tw, BlockReference br)
            {
                Tw = tw;
                BlockReference = br;
            }

            private void Initialize()
            {
                _attributes.Clear();
                foreach (ObjectId id in BlockReference.AttributeCollection)
                    _attributes.Add(AttributeData.Create(Tw, id));
            }
            public static BlockAttributeCollection Create(TransactionWrapper tw, BlockReference br)
            {
                var r = new BlockAttributeCollection(tw, br);
                r.Initialize();
                return r;
            }
        }


        /// <summary>
        /// Sets diameter of the shaft symbol in Block, and it's direction.
        /// </summary>
        /// <param name="diameter">If null, not changed.
        /// Otherwise, shaft symbol is scaled to diameter.
        /// </param>
        /// <param name="directionX">
        /// If null, n ot applied.
        /// Otherwise, it represents new Y-axis alignment of the symbol.
        /// </param>
        /// <remarks>
        /// If block is not round (for example rectangle), bottom line
        /// must be parallel to X-axis (it is used for alignment).
        /// Symbol diameter/length must be 1m.
        /// </remarks>
        public static void AdjustBlockSymbol(TransactionWrapper tw, BlockReference br,
            double? diameter = null,
            Vector2d? direction = null)
        {
            double currentScale = br.ScaleFactors.X;
            // Scale Block, but keep 

        }
    }
}
