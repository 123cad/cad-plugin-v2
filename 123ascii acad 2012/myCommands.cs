﻿// (C) Copyright 2013 by Microsoft 
//
using System;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using System.Collections.Generic;

// This line is not mandatory, but improves loading performances
[assembly: CommandClass(typeof(_123ascii_acad_2012.MyCommands))]

namespace _123ascii_acad_2012
{

    // This class is instantiated by AutoCAD for each document when
    // a command is called by the user the first time in the context
    // of a given document. In other words, non static data in this class
    // is implicitly per-document!
    public class MyCommands
    {
        // The CommandMethod attribute can be applied to any public  member 
        // function of any public class.
        // The function should take no arguments and return nothing.
        // If the method is an intance member then the enclosing class is 
        // intantiated for each document. If the member is a static member then
        // the enclosing class is NOT intantiated.
        //
        // NOTE: CommandMethod has overloads where you can provide helpid and
        // context menu.

        // Modal Command with localized name
        [CommandMethod("MyGroup", "MyCommand", "MyCommandLocal", CommandFlags.Modal)]
        public void MyCommand() // This method can have any name
        {
            // Put your command code here
        }

        // Modal Command with pickfirst selection
        [CommandMethod("MyGroup", "MyPickFirst", "MyPickFirstLocal", CommandFlags.Modal | CommandFlags.UsePickSet)]
        public void MyPickFirst() // This method can have any name
        {
            PromptSelectionResult result = Application.DocumentManager.MdiActiveDocument.Editor.GetSelection();
            if (result.Status == PromptStatus.OK)
            {
                // There are selected entities
                // Put your command using pickfirst set code here
            }
            else
            {
                // There are no selected entities
                // Put your command code here
            }
        }

        // Application Session Command with localized name
        [CommandMethod("MyGroup", "MySessionCmd", "MySessionCmdLocal", CommandFlags.Modal | CommandFlags.Session)]
        public void MySessionCmd() // This method can have any name
        {
            // Put your command code here
        }

        // LispFunction is similar to CommandMethod but it creates a lisp 
        // callable function. Many return types are supported not just string
        // or integer.
        [LispFunction("MyLispFunction", "MyLispFunctionLocal")]
        public int MyLispFunction(ResultBuffer args) // This method can have any name
        {
            // Put your command code here

            // Return a value to the AutoCAD Lisp Interpreter
            return 1;
        }
        public static List<string> GetLayers()
        { // Get the current document and database, and start a transaction 
            List<string> layers = new List<string>();
            Document acDoc = Application.DocumentManager.MdiActiveDocument;
            Database acCurDb = acDoc.Database;
            using (Transaction acTrans = acCurDb.TransactionManager.StartTransaction())
            { // This example returns the layer table for the current database 
                LayerTable acLyrTbl;
                acLyrTbl = acTrans.GetObject(acCurDb.LayerTableId,
                    OpenMode.ForRead) as LayerTable;
                // Step through the Layer table and print each layer name 
                foreach (ObjectId acObjId in acLyrTbl)
                {
                    LayerTableRecord acLyrTblRec;
                    acLyrTblRec = acTrans.GetObject(acObjId,
                        OpenMode.ForRead) as LayerTableRecord;
                    //acDoc.Editor.WriteMessage("\n" + acLyrTblRec.Name);
                    layers.Add(acLyrTblRec.Name);
                }  // Dispose of the transaction
            }
            return layers;
        }
        [CommandMethod("123asciinew")]
        public void Start123ASCII()
        {
            var layers = GetLayers();
            MyBlocks.BlockManager manager = new MyBlocks.BlockManager();
            if (!_123ASCII.Program.ShowDialog(manager, layers))
            {
                Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage("Closed without applying!");
                return;
            }
            //ici kroz svaki red u tabeli, citati koordinate i postavljati vrednosti atributa, uz postavljanje nove pozicije
            //prva verzija bez namestanja atributa
            List<Entity> entitiesToApply = new List<Entity>();
            _123ASCII._123ASCIIController ascii = _123ASCII.Program.ASCIIController;
            _123ASCII.MaskController mask = _123ASCII.Program.MyMaskController;
            //ako je u pitanju tacka, onda samo naci koordinate i ucitati tacke
            //ako je u pitanju blok
            ////ako je single onda ista podesavanja za sve redove
            ////ako je groups onda prvo videti kojoj grupi pripada red, pa primeniti na njemu podesavanja odgovarajuce grupe
            //ucitati tacke

            //pri ucitavanju u cad, proveravati da li postoji layer - ako ne postoji onda ga kreirati pre dodavanja entiteta
            List<List<Entity>> entities = new List<List<Entity>>();
            List<Entity> entitiesRow;

            int xCol = mask.XCoordinate;
            int yCol = mask.YCoordinate;
            int zCol = mask.ZCoordinate;

            if (mask.Type == _123ASCII.MaskType.POINT)
            {
                Document doc = Application.DocumentManager.MdiActiveDocument;
                Database db = doc.Database;
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    BlockTable bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForWrite);
                    BlockTableRecord ms = (BlockTableRecord)tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite);

                    foreach (List<string> row in ascii.GetTable())
                    {
                        double x = double.Parse(row[xCol]);
                        double y = double.Parse(row[yCol]);
                        double z = double.Parse(row[zCol]);
                        Point3d p3d = new Point3d(x, y, z);
                        DBPoint point = new DBPoint(p3d);
                        ms.AppendEntity(point);
                        tr.AddNewlyCreatedDBObject(point, true);
                    }
                    tr.Commit();
                }
            }
            else
            {
                if (mask.GroupBlock == _123ASCII.BlockGoup.SINGLE)
                {
                    //dohvati grupu i prebaci podesavanja iz nje
                    _123ASCII.Group group = mask.SingleGroup;
                    MyBlocks.SingleBlock single = manager.GetBlock(group.BlockName);
                    //postaviti novu grupu (ako ne postoji), u smislu dodati bloktablerecord, dodati layer ako ne postoji i dodati onda sve entitete
                    //posle samo dodavati blok reference sa promenjenom pozicijom, 
                    Document doc = Application.DocumentManager.MdiActiveDocument;
                    Database db = doc.Database;
                    using (Transaction tr = db.TransactionManager.StartTransaction())
                    {
                        BlockTable bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForWrite);
                        BlockTableRecord ms = (BlockTableRecord)tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite);
                        LayerTable lt = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForWrite);
                        BlockTableRecord btr = null;
                        List<ObjectId> attsIds = new List<ObjectId>();


                        if (!bt.Has(single.BlockName))
                        {

                            Database blockDB = new Database(false, true);
                            blockDB.ReadDwgFile(single.FullBlockPath, FileOpenMode.OpenForReadAndAllShare, false, "");
                            //using (Transaction trsource = blockDB.TransactionManager.StartTransaction())
                            {
                                //da li treba da kopiramo lejere?
                                /*LayerTable btsource = trsource.GetObject(blockDB.LayerTableId, OpenMode.ForRead) as LayerTable;
                                foreach (ObjectId idl in btsource)
                                {
                                    LayerTableRecord ltrs = trsource.GetObject(idl, OpenMode.ForRead) as LayerTableRecord;
                                }*/
                            }
                            ObjectId btrId = db.Insert(single.BlockName, blockDB, false);
                            //btr.Name = single.BlockName;

                            /*attsIds = new List<ObjectId>();
                            foreach (var att in single.Attributes)
                            {
                                if (!lt.Has(att.Layer))
                                {
                                    LayerTableRecord ltr = new LayerTableRecord();
                                    ltr.Name = att.Layer;
                                    lt.Add(ltr);
                                    tr.AddNewlyCreatedDBObject(ltr, true);
                                }
                                //AttributeDefinition attdef = new AttributeDefinition();
                                //attdef.Layer = att.Layer;
                                //attdef.Tag = att.Tag;
                                //attdef.Position = att.Position;
                                //attdef.Color = att.Color;
                                //attdef.Linetype = att.Linetype;
                                //attdef.LineWeight = att.LineWeight;
                                //attdef.LinetypeScale = att.LinetypeScale;
                                
                                attsIds.Add(btr.AppendEntity(att));
                                tr.AddNewlyCreatedDBObject(att, true);
                            }
                            foreach (var natt in single.NonAttributes)
                            {
                                if (!lt.Has(natt.Layer))
                                {
                                    LayerTableRecord ltr = new LayerTableRecord();
                                    ltr.Name = natt.Layer;
                                    lt.Add(ltr);
                                    tr.AddNewlyCreatedDBObject(ltr, true);
                                }
                                btr.AppendEntity(natt);
                                tr.AddNewlyCreatedDBObject(natt, true);
                            }*/
                        }
                        //else
                        {
                            btr = tr.GetObject(bt[single.BlockName], OpenMode.ForWrite) as BlockTableRecord;
                            foreach (ObjectId id in btr)
                            {
                                DBObject dbo = tr.GetObject(id, OpenMode.ForRead);
                                if ((dbo as AttributeDefinition) != null)
                                    attsIds.Add(id);
                            }
                        }

                        //[svakom entitetu u bloku postaviti nove koordinate]mozda ne treba, jer se to izgleda radi automatski, a svakom atributu vrednost
                        foreach (List<string> row in ascii.GetTable())
                        {
                            double x = double.Parse(row[xCol]);
                            double y = double.Parse(row[yCol]);
                            double z = double.Parse(row[zCol]);
                            BlockReference br = new BlockReference(new Point3d(x, y, z), btr.Id);
                            ms.AppendEntity(br);
                            tr.AddNewlyCreatedDBObject(br, true);
                            foreach (Entity ent in single.NonAttributes)
                            {


                            }
                            entitiesRow = new List<Entity>(single.Attributes.Count);

                            foreach (ObjectId id in attsIds)
                            {
                                AttributeDefinition att = tr.GetObject(id, OpenMode.ForWrite) as AttributeDefinition;
                                _123ASCII.MyAttribute matt = group.GetAttribute(att.Tag);

                                string val = matt.Prefix + (matt.FixedValue ? matt.Value : (row.Count > matt.Column ? row[matt.Column] : "")) + matt.Sufix;
                                AttributeReference atRef = new AttributeReference();
                                att.TextString = val;
                                Matrix3d matrix = br.BlockTransform;
                                atRef.SetAttributeFromBlock(att, matrix);
                                br.AttributeCollection.AppendAttribute(atRef);
                                tr.AddNewlyCreatedDBObject(atRef, true);
                            }
                            entities.Add(entitiesRow);
                        }
                        tr.Commit();
                    }
                }
                else
                {
                    foreach (List<string> row in ascii.GetTable())
                    {
                        //dohvati grupu kojoj row pripada
                        _123ASCII.Group mgroup = null;
                        string grn = "";
                        if (row.Count >= mask.SelectedGroupingColumn)
                            grn = row[mask.SelectedGroupingColumn];
                        foreach (_123ASCII.Group gr in mask.Groups)
                            if (gr.GroupName == grn)
                                mgroup = gr;
                        MyBlocks.SingleBlock single = manager.GetBlock(mgroup.BlockName);

                        double x = double.Parse(row[xCol]);
                        double y = double.Parse(row[yCol]);
                        double z = double.Parse(row[zCol]);
                        entitiesRow = new List<Entity>(single.Attributes.Count);
                        foreach (Entity ent in single.NonAttributes)
                        {
                            Entity ent1 = ent.Clone() as Entity;
                            //TODO poziciju treba odrediti svakom elementu posebno, znaci videti koji sve dolaze u obzir

                        }
                        foreach (AttributeDefinition att in single.Attributes)
                        {
                            AttributeDefinition att1 = att.Clone() as AttributeDefinition;
                            Point3d point = att1.Position;
                            point.Add(new Vector3d(x, y, z));
                            att1.Position = point;
                            _123ASCII.MyAttribute matt = mgroup.GetAttribute(att.Tag);

                            string val = matt.Prefix + (matt.FixedValue ? matt.Value : (row.Count > matt.Column ? row[matt.Column] : "")) + matt.Sufix;
                            att1.TextString = val;

                            entitiesRow.Add(att1);
                        }
                        entities.Add(entitiesRow);

                    }

                }
            }
            //u entities imamo sve spremno, treba samo da dodamo u acad
            Application.DocumentManager.MdiActiveDocument.Editor.Regen();

        }

    }

}
