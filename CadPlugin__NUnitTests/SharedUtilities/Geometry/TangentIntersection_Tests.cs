﻿using MyUtilities.Geometry;
using NUnit.Framework;
using SharedUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin__NUnitTests.SharedUtilities.Geometry
{
	class TangentIntersection_Tests
	{
		[Test]
		public void Test1()
		{
			Point2d circle = new Point2d();
			double diameter = 4;
			Point2d p = new Point2d(10, 0);

			var res = TangentIntersection.TangentPoints(circle, diameter, p);
			
			Assert.AreEqual(new Point2d(0.4, 1.9596), res.Point1);
			Assert.AreEqual(new Point2d(0.4, -1.9596), res.Point2);
		}
	}
}
