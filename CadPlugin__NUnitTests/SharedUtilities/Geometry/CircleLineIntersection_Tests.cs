﻿using MyUtilities.Geometry;
using NUnit.Framework;
using SharedUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin__NUnitTests.SharedUtilities.Geometry
{
	class CircleLineIntersection_Tests
	{
		[Test]
		public void Test1()
		{
			Point2d circle = new Point2d(10, -4);
			double diameter = 4;
			Point2d l1 = new Point2d(14.9675, -5.1508);
			Point2d l2 = new Point2d(11.9903, -3.8032);

			var res = CircleLineIntersection.GetIntersection(circle, diameter, l1, l2);

			Assert.IsTrue(res.HasIntersectionPoints);
		}
	}
}
