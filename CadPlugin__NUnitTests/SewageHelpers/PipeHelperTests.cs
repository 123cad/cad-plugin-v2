﻿using CadPlugin.SewageHelpers;
using MyUtilities.Geometry;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SewageUtilities.Helpers;
using System.Threading.Tasks;

namespace CadPlugin_NUnitTests.SewageHelpers
{
    [TestFixture]
    class PipeHelperTests
    {
        [TestCase(1, 0.5)]
        [TestCase(2, 1.0)]
        [TestCase(-1, -0.5)]
        [TestCase(-2, -1.0)]
        [TestCase(0, 0)]
        public void GetPipeSlopeInPercent_DownstreamLength2_Slope(double deltaZ, double expected)
        {
            Point3d start = new Point3d();
            Point3d end = new Point3d(2, 0, -deltaZ);
            double slope = PipeSlope.CreateFromPoints(start, end).SlopeRatio;// PipeHelper.GetPipeSlopeInRatio(start, end);

            Assert.AreEqual(expected, slope, 0.0001);
        }


        [TestCase(1, 50)]
        [TestCase(2, 100)]
        [TestCase(-1, -50)]
        [TestCase(-2, -100)]
        [TestCase(0, 0)]
        public void GetPipeSlopeInPercent_DownstreamLegnth2_Slope(double deltaZ, double expected)
        {
            Point3d start = new Point3d();
            Point3d end = new Point3d(2, 0, -deltaZ);
            double slope = PipeSlope.CreateFromPoints(start, end).SlopePercent;

            Assert.AreEqual(expected, slope, 0.0001);
        }
        [TestCase(1, 1.5)]
        [TestCase(0.1, 2.4)]
        public void AdjustPipePointsToShafts_DownstreamX_Valid(double startShaftDiameter, double endShaftDiameter)
        {
            Point2d startShaft = new Point2d(1, 0);
            Point2d endShaft = new Point2d(6, 0);
            double pipeInHeight = -0.5;
            double pipeOutHeight = 1.2;
            Point3d startPipe = new Point3d();
            Point3d endPipe = new Point3d();
            double startRadius = startShaftDiameter / 2;
            double endRadius = endShaftDiameter / 2;


            double expectedDeltaZ = startPipe.Z - ((startShaft.GetVectorTo(endShaft).Length - startRadius - endRadius) * -pipeInHeight/100);
            PipeHelper.AdjustPipePointsToShafts(startShaft, startShaftDiameter, 
                pipeOutHeight, pipeInHeight, ref startPipe, ref endPipe, 
                endShaft, endShaftDiameter);

            Assert.AreEqual(startShaft.X + startRadius, startPipe.X, 0.0001, "start pipe x");
            Assert.AreEqual(endShaft.X - endRadius, endPipe.X, 0.0001, "end pipe x");
            Assert.AreEqual(pipeOutHeight, startPipe.Z, 0.0001, "end z");
            Assert.AreEqual(pipeInHeight, endPipe.Z, 0.0001, "end z");

        }

        [TestCase(1, 0.5, -0.5)]
        [TestCase(2, 0.5, -1)]
        [TestCase(2, 1, -2)]
        public void AdjustPipeEndHeight_Downstream_Valid(double lengthX, double slope, double expectedEndZ)
        {
            Point3d start = new Point3d();
            Point3d end = new Point3d(lengthX, 0, 0);

            end = PipeHelper.AdjustPipeEndHeight(start, end, slope);

            Assert.AreEqual(expectedEndZ, end.Z, 0.000001);
        }

        [TestCase(10, 0.5, -5)]
        [TestCase(10, 0.2, -2)]
        [TestCase(10, 1, -10)]
        [TestCase(10, -0.5, 5)]

        public void GetEndZ(double lengthX, double slope, double result)
        {
            Point3d start = new Point3d(1, 1, 1);
            Point3d end = new Point3d(start.X + lengthX, 1, 1);
            double newZ = PipeHelper.GetEndZ(start, end, slope);
            Assert.AreEqual(result, newZ - start.Z, 0.000001);
        }
    }
}
