﻿This project is used to run NON cad parts of CadPlugin library.
Keep in mind that only types which do not reference CAD libraries, in any way - directly or indirectly, can be used in this way.
Also, using resources is not possible, since they require to load all dlls (brx.dll reports problem by loading - filenotfound).