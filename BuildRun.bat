@echo off
call "%vs140comntools%vsvars32.bat"
cd "%~dp0"
set f1="Build Debug Bcad.bat"
set f2="Build Debug Acad.bat"
set f3="Build Release Bcad.bat"
set f4="Build Release Acad.bat"
echo 1 - %f1%
echo 2 - %f2%
echo 3 - %f3%
echo 4 - %f4%
SET /P CIF=Select build script to run:
set f= 
if %CIF%==1  set f=%f1%
if %CIF%==2  set f=%f2%
if %CIF%==3  set f=%f3%
if %CIF%==4  set f=%f4%
@echo on
call %f%
pause