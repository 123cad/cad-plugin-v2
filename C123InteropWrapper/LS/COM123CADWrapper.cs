﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interop.C123YardXRXLib;

namespace C123InteropWrapper
{
    /// <summary>
    /// Facade for Interop communication with 123CAD.
    /// </summary>
    public class COM123CADWrapper : IDisposable
    {
        public IDrawing Dwg { get; private set; }
        public ICo123App App { get; private set; }
        public I123Sect Sect { get; private set; }
        public I123SectTerrain SectTerrain { get; private set; }
        public I123SectTableRow SectTableRow { get; private set; }
        public I123SectProp SectProp { get; private set; }
        public I123QSectArea QSectArea { get; private set; }
        public COM123CADWrapper()
        {
            Dwg = new CoDwg();
            App = new Co123App();
            Sect = new Co123Sect();
            SectTerrain = new Co123SectTerrain();
            SectTableRow = new Co123SectTableRow();
            SectProp = new Co123SectProp();
            QSectArea = new Co123QSectArea();
        }

        public void Dispose()
        {
            Dwg = null;
            App = null;
            Sect = null;
            SectTerrain = null;
            SectTableRow = null;
            SectProp = null;
            QSectArea = null;
        }
    }
}
