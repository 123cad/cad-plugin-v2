﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common
{
    public static class CADCircleHelper
	{
		/// <summary>
		/// Creates circle in nested transaction.
		/// </summary>
		public static ObjectId CreateCircle(Database db, Point3d center, double radius)
		{
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
				BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;

				Circle c = CreateCircle(tr, btr, center, radius);
				ObjectId id = c.ObjectId;
				tr.Commit();

				return id;
			}
		}
		/// <summary>
		/// Creates circle in existing transaction.
		/// </summary>
		public static Circle CreateCircle(Transaction tr, BlockTableRecord modelspace, Point3d center, double radius)
		{

			Circle c = new Circle();
			modelspace.AppendEntityCurrentLayer(c);
			tr.AddNewlyCreatedDBObject(c, true);
			c.Center = center;
			System.Diagnostics.Debug.WriteLineIf(radius == 0, "Attempted to set circle radius to 0! " +
				"This causes eDegenerateGeometry exception. Corrected to small radius.");
			c.Radius = radius > 0 ? radius : 0.0001;

			return c;
		}
		/// <summary>
		/// Creates circle in existing transaction.
		/// </summary>
		public static Circle CreateCircle(TransactionWrapper tw, Point3d center, double radius)
        {
			return CreateCircle(tw.Tr, tw.ModelSpace, center, radius);
        }
	}
}
