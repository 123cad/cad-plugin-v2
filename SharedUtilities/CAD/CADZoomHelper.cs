﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using my = MyUtilities.Geometry;
using System.Drawing;
using Autodesk.AutoCAD.Internal;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common
{
    /// <summary>
    /// Holds information enough to restore drawing's view.
    /// </summary>
    public class DrawingViewState
    {
        /// <summary>
        /// This determines position of the camera (Target + ViewDirection).
        /// </summary>
        public Vector3d ViewDirection { get; set; }
        /// <summary>
        /// This point will be in the center of the screen and WCS origin will be moved accordingly.
        /// </summary>
        public Point3d Target { get; set; }
        /// <summary>
        /// This is offset from the target.
        /// </summary>
        public Point2d Center { get; set; } = new Point2d();
        public double Width { get; set; }
        public double Height { get; set; }

        public DrawingViewState( double width, double height)
        {
            Width = width;
            Height = height;
        }
        public void SetCenterPointOfTheScreen(Point3d pt)
        {
            Target = pt;
        }
    }
    public static class CADZoomHelper
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ed"></param>
        /// <param name="ids"></param>
        /// <param name="paddingPercent"></param>
        public static void ZoomToEntities(Document doc, IEnumerable<ObjectId> ids, int paddingPercent = 5)
        {

            using (TransactionWrapper tw = doc.StartTransaction())
            {
                List<Entity> entities = new List<Entity>();
                foreach (var id in ids)
                    entities.Add(tw.GetObjectRead<Entity>(id));

                // Do commit even for readonly operations.
                ZoomToEntities(doc.Editor, entities, paddingPercent);
                tw.Commit();
            }

        }

        /// <summary>
        /// Zooms to all provided entities. 
        /// </summary>
        /// <param name="ed"></param>
        /// <param name="entities"></param>
        /// <param name="paddingPercent">Percent value [0-100] for which to increase zoomed window.</param>
        public static void ZoomToEntities(Editor ed, IEnumerable<Entity> entities, int paddingPercent = 5)
        {

            if (entities == null || !entities.Any())
            //throw new ArgumentNullException("No entities for zooming");
            {
                return;
            }
            double minX, maxX;
            double minY, maxY;
            minX = minY = maxX = maxY = 0;
            double topZ = 0;
            bool first = true;
            foreach (Entity ent in entities)
            {
                Extents3d extents = ent.GeometricExtents;

                if (first)
                {
                    minX = extents.MinPoint.X;
                    minY = extents.MinPoint.Y;
                    maxX = extents.MaxPoint.X;
                    maxY = extents.MaxPoint.Y;
                    topZ = extents.MaxPoint.Z;
                    first = false;
                    continue;
                }
                double d = extents.MinPoint.X;
                if (minX > d)
                    minX = d;
                d = extents.MinPoint.Y;
                if (minY > d)
                    minY = d;
                d = extents.MaxPoint.X;
                if (maxX < d)
                    maxX = d;
                d = extents.MaxPoint.Y;
                if (maxY < d)
                    maxY = d;
                d = extents.MaxPoint.Z;
                if (topZ < d)
                    topZ = d;
            }
            double height = maxY - minY;
            double width = maxX - minX;
            // Add some extra space around this "window"
            double deltaY = height * 0.05;
            double deltaX = width * 0.05;
            minX -= deltaX;
            minY -= deltaY;
            maxX += deltaX;
            maxY += deltaY;
            height = maxY - minY;
            width = maxX - minX;
            {
                // Adjust padding
                double percent = paddingPercent / 100.0;
                double dH = height * percent / 2;
                double dW = width * percent / 2;
                minX -= dW;
                maxX += dW;
                minY -= dH;
                maxY += dH;
                height = maxY - minY;
                width = maxX - minX;
            }
            bool useZoom = false;
            //CHECK Why should we not use zoom any time?
            if (useZoom)
            {
                string lower = DoubleHelper.ToStringInvariant(minX, 2) + "," +
                                DoubleHelper.ToStringInvariant(minY, 2) + ",0";
                string higher = DoubleHelper.ToStringInvariant(maxX, 2) + "," +
                                DoubleHelper.ToStringInvariant(maxY, 2) + ",0";
                string command = "._ZOOM W " + lower + " " + higher + " ";
                ed.Document.SendStringToExecute(command, true, false, false);
            }
            else
            {
                Point3d center = new Point3d((minX + maxX) / 2.0, (minY + maxY) / 2.0, topZ);
                double widthT = maxX - minX;
                double heightT = maxY - minY;
                ZoomTo(ed, center.FromCADPoint(), widthT, heightT);
            }
        }
        /// <summary>
        /// Zooming to view with provided width and height, with point at a center (aspect ration is preserved).
        /// <para>Returns view state before zooming.</para>
        /// </summary>
        /// <param name="ed"></param>
        /// <param name="width">In WCS scale</param>
        /// <param name="height">In WCS scale</param>
        public static DrawingViewState ZoomTo(Editor ed, my.Point3d center, double width, double height)
        {
            DrawingViewState state = null;
            using (Transaction tr = ed.Document.Database.TransactionManager.StartTransaction())
            {
                // Get the current view
                using (ViewTableRecord acView = ed.GetCurrentView())
                {
                    // Get current view ratio.
                    double dViewRatio = (acView.Width / acView.Height);

                    // Check to see if the new width fits in current window
                    height = height;
                    double width1 = height * dViewRatio;
                    //double dx = (pMax.X - pMin.X);
                    if (width1 > width)
                    {
                        width = width1;
                        height = width1 / dViewRatio;
                    }

                    // Save current state.
                    state = new DrawingViewState(acView.Width, acView.Height);
                    state.Center = acView.CenterPoint;
                    state.Target = acView.Target;
                    state.ViewDirection = acView.ViewDirection;

                    acView.Height = height;
                    acView.Width = width;
                    acView.Target = center.ToCADPoint();
                    acView.ViewDirection = new Vector3d(0, 0, 1);
                    // Center point is offset from Target point.
                    acView.CenterPoint = new Point2d(0,0);

                    // Set the current view
                    ed.SetCurrentView(acView);
                }
                // Commit the changes
                tr.Commit();
            }
            return state;
        }
        /// <summary>
        /// Restores view from provided state, and returns previous state.
        /// </summary>
        /// <param name="state"></param>
        public static void SetView(Editor ed, DrawingViewState state)
        {
            using (ViewTableRecord acView = ed.GetCurrentView())
            {
                double scale1 = acView.Width / acView.Height;
                double scale2 = state.Width / state.Height;
                double width = state.Width;
                double height = state.Height;
                if (Math.Abs(scale1 - scale2) > 0.000001)
                {
                    width = height * scale1;
                }
                acView.Width = width;
                acView.Height = height;
                acView.ViewDirection = state.ViewDirection;
                acView.Target = state.Target;
                acView.CenterPoint = state.Center;
                ed.SetCurrentView(acView);
            }
        }
    }
}