﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Common
{
	public class CADSelectionHelper
	{
		/// <summary>
		/// Selects BlockReferences only.
		/// </summary>
		/// <param name="includeAnyBlockElement"></param>
		/// <returns></returns>
		public static List<ObjectId> SelectBlockReferences(bool includeAnyBlockElement = false)
		{
			TypedValue[] filterCriteria = new TypedValue[1];
			filterCriteria.SetValue(new TypedValue((int)DxfCode.Start, "INSERT"), 0);
			SelectionFilter filter = new SelectionFilter(filterCriteria);

			PromptSelectionResult res = Application.DocumentManager.MdiActiveDocument.Editor.GetSelection(filter);
			if (res.Status != PromptStatus.OK)
				return null;

			ObjectId[] selectedIds = res.Value.GetObjectIds();

			return selectedIds.ToList();
		}
	}
}
