﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtilities.Helpers;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using System.IO;
using SharedUtilities.CAD;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif
#if AUTOCAD
namespace Autodesk.AutoCAD.DatabaseServices
#endif

#if BRICSCAD
namespace Teigha.DatabaseServices
#endif
{
    public class TransactionWrapper :IDisposable
    {
        /// <summary>
        /// If true, for every created Entity information
        /// is written to Commandline in CAD.
        /// </summary>
        public static bool IsConsoleOutputEnabled
        {
            get => _isConsoleOutputEnabled;
            set
            {
                _isConsoleOutputEnabled = value;
                string s = _isConsoleOutputEnabled ? "Enabled" : "Disabled";
                string msg = $"\t TransactionWrapper: Created entity Console output {s}.";
                if (_isConsoleOutputEnabled)
	                msg = "".PadLeft(10, '*') + msg;
                else
	                msg = msg + "".PadLeft(10, '*');
                msg += "\n";
                Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage(msg);
            }
        }

        public static bool IsDebugOutputEnabled
        {
	        get => _isDebugOutputEnabled;
	        set
	        {
				_isDebugOutputEnabled = value;
                string s = _isDebugOutputEnabled ? "Enabled" : "Disabled";
                string msg = $"\t TransactionWrapper: Created entity Debug output {s}.";
                if (_isDebugOutputEnabled)
	                msg = "".PadLeft(10, '*') + msg;
                else
	                msg = msg + "".PadLeft(10, '*');
                msg += "\n";
                Debug.WriteLine(msg);
			}
        }

        /// <summary>
        /// BlockTable in initial Read state.
        /// </summary>
        public BlockTable BlockTable
        {
            get
            {
                if (_BlockTable == null)
                    _BlockTable = Tr.GetObjectRead<BlockTable>(Db.BlockTableId);
                return _BlockTable;
            }
        }

        /// <summary>
        /// BlockTableRecord for ModelSpace in initial Read state.
        /// </summary>
        public BlockTableRecord ModelSpace
        {
            get
            {
                if (_ModelSpace == null)
                    _ModelSpace = Tr.GetObjectRead<BlockTableRecord>(BlockTable[BlockTableRecord.ModelSpace]);
                return _ModelSpace;
            }
        }
        /// <summary>
        /// LayerTable in initial Read state.
        /// </summary>
        public LayerTable LayerTable
        {
            get
            {
                if (_LayerTable == null)
                    _LayerTable = Tr.GetObjectRead<LayerTable>(Db.LayerTableId);
                return _LayerTable;
            }
        }
        /// <summary>
        /// LinetypeTable in initial Read state.
        /// </summary>
        public LinetypeTable LinetypeTable
        {
            get
            {
                if (_LinetypeTable == null)
                    _LinetypeTable = Tr.GetObjectRead<LinetypeTable>(Db.LinetypeTableId); 
                return _LinetypeTable;
            }
        }
        /// <summary>
        /// DBDictionary in initial Read state.
        /// </summary>
        public DBDictionary NamedObjectsDictionary
        {
            get
            {
                if (_NamedObjectsDictionary == null)
                    _NamedObjectsDictionary = Tr.GetObjectRead<DBDictionary>(Db.NamedObjectsDictionaryId);
                return _NamedObjectsDictionary;
            }
        }
        /// <summary>
        /// Same as NamedObjectsDictionary.
        /// </summary>
        public DBDictionary NOD => NamedObjectsDictionary;

        /// <summary>
        /// Same as ModelSpace.
        /// </summary>
        public BlockTableRecord MS => ModelSpace;
        /// <summary>
        /// Same as BlockTable.
        /// </summary>
        public BlockTable Bt => BlockTable;
        /// <summary>
        /// Same as LayerTable.
        /// </summary>
        public LayerTable Lt => LayerTable;

        /// <summary>
        /// Same as Transaction.
        /// </summary>
        public Transaction Tr => Transaction;
        /// <summary>
        /// Same as Document.
        /// </summary>
        public Document Doc => Document;
        /// <summary>
        /// Same as Database.
        /// </summary>
        public Database Db => Database;
        /// <summary>
        /// If true, Commit will be called when disposing this object.
        /// <para>No need to call it manually in transaction block.</para>
        /// </summary>
        /// <remarks>It is suggested by documentation that Commit should be 
        /// called at the end of each transaction (even for readonly scenario).
        /// So this could help to skip explicit call to commit by the user.</remarks>
        public bool CommitOnDispose { get; set; } = false;


        public Transaction Transaction { get; }
        public Document Document { get; }
        public Database Database { get; }

        private BlockTable _BlockTable = null;
        private BlockTableRecord _ModelSpace = null;
        private LayerTable _LayerTable = null;
        private DBDictionary _NamedObjectsDictionary = null;
        private LinetypeTable _LinetypeTable = null;
        private static bool _isConsoleOutputEnabled = false;
        private static bool _isDebugOutputEnabled = false;
        /// <summary>
        /// Invokes method on each observer when desired event happened.
        /// </summary>
        private List<ITransactionWrapperObserver> Observers { get; } = new List<ITransactionWrapperObserver>();

        public TransactionWrapper(Document doc, Transaction tr)
        {
            Document = doc;
            Database = doc.Database;
            Transaction = tr;
        }

        /// <summary>
        /// Adds Observer which gets notified about events in TW.
        /// </summary>
        public void AddObserver(ITransactionWrapperObserver o)
        {
            Observers.Add(o);
            o.Attached(this);
        }

        public void RemoveObserver(ITransactionWrapperObserver o)
        {
            Observers.Remove(o);
            o.Detached(this);
        }

        /// <summary>
        /// Creates entity and adds it to database and transaction.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T CreateEntity<T>(string layerName = null
#if DEBUG
            , [CallerFilePath] string file = null
            , [CallerMemberName] string member = null
            , [CallerLineNumber] int? line = null
#endif
            ) where T:Entity
        {
            T entity = Tr.CreateEntity<T>(ModelSpace, layerName);
#if DEBUG
            if (IsConsoleOutputEnabled)
            {
                string handle = entity.Id.Handle.ConvertToString().ToUpper().PadLeft(3);
                string objectName = typeof(T).Name.PadRight(10);
                string fileName = Path.GetFileName(file);
                string directory = Path.GetDirectoryName(file);
                string s = $"Created {handle} {objectName} by {member} in {fileName}:{line} >>> {directory}\n";
				if (IsDebugOutputEnabled)
					Debug.Write(s);
				if (IsConsoleOutputEnabled)
					Document.Editor.WriteMessage(s);
            }
#endif
            Observers?.ForEach(o => o?.EntityCreated(this, entity));
            return entity;
        }
        /// <summary>
        /// Opens object for read.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetObjectRead<T>(ObjectId id, bool openErased = false) where T:DBObject
        {
            return Tr.GetObjectRead<T>(id, openErased);
        }
        /// <summary>
        /// Opens object for write.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetObjectWrite<T>(ObjectId id, bool openErased = false) where T:DBObject
        {
            return Tr.GetObjectWrite<T>(id, openErased);
        }

        public void Commit() => Tr.Commit();
        public void Abort() => Tr.Abort();

        /// <summary>
        /// Counts all entities in the drawing (counters are divided by types).
        /// </summary>
        /// <returns></returns>
        public ObjectsCounter CountEntities()
        {
            ObjectsCounter counter = new ObjectsCounter();
            foreach (var oid in MS)
            {
                DBObject obj = Tr.GetObjectRead<DBObject>(oid);
                counter.Increase(Tr.GetObject(oid, OpenMode.ForRead));
            }
            return counter;
        }

        public static implicit operator Transaction(TransactionWrapper tw) 
            => tw.Transaction;

        public void Dispose()
        {
            Observers?.ForEach(o =>
            {
                o?.Detached(this);
            });
            Observers?.Clear();
            if (CommitOnDispose)
                Tr.Commit();
            Tr.Dispose();
        }
    }
}
