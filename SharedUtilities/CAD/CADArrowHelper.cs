﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyGeometry = MyUtilities.Geometry;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif


namespace CadPlugin.Common
{
	/// <summary>
	/// For creating arrow.
	/// </summary>
	public static class CADArrowHelper
	{
		/// <summary>
		/// Position of arrow relative to aligning text object.
		/// </summary>
		public enum ArrowPositionText
		{
			/// <summary>
			/// Arrow starts on the left and ends at left top point.
			/// </summary>
			LeftTop,
			LeftMiddle,
			LeftBottom,
			RightTop,
			RightMiddle,
			RightBottom
		}
		/// <summary>
		/// Position of arrow relative to a point 
		/// </summary>
		public enum ArrowPositionPoint
		{
			/// <summary>
			/// Arrow ends at point.
			/// </summary>
			Left, 
			/// <summary>
			/// Arrow starts at point.
			/// </summary>
			Right,
			/// <summary>
			/// Arrow center is in the middle of the point.
			/// </summary>
			Middle
		}
		/// <summary>
		/// Direction of arrow.
		/// </summary>
		public enum ArrowDirection
		{
			// Same direction as aligning vector.
			Along,
			// Opposite direction of aligning vector.
			Against
		}

		public class ArrowParameters
		{
			/// <summary>
			/// Length of tail part.
			/// </summary>
			public double TailLength { get; set; }
			/// <summary>
			/// Length of head part.
			/// </summary>
			public double HeadLength { get; set; }
			/// <summary>
			/// Width of tail (start point of arrow to start point of head).
			/// </summary>
			public double TailWidth { get; set; }
			/// <summary>
			/// Point closer to tail has this width. End point has width = 0.
			/// </summary>
			public double HeadWidth { get; set; }

			public ArrowDirection Direction { get; set; }
			public ArrowParameters()
			{
				Direction = ArrowDirection.Along;
			}
			/// <summary>
			/// Creates width and height relative to reference values.
			/// </summary>
			/// <param name="referenceLength"></param>
			/// <param name="referenceWidth"></param>
			/// <param name="scaleTailLength"></param>
			/// <param name="scaleTailWidth"></param>
			/// <param name="scaleHeadLength"></param>
			/// <param name="scaleHeadWidth"></param>
			public ArrowParameters(double referenceLength, double referenceWidth, double scaleTailLength, double scaleHeadLength, double scaleTailWidth, double scaleHeadWidth)
			{
				TailLength = referenceLength * scaleTailLength;
				TailWidth = referenceWidth * scaleTailWidth;
				HeadLength = referenceLength * scaleHeadLength;
				HeadWidth = referenceWidth * scaleHeadWidth;
			}
		}

		/// <summary>
		/// Creates arrow 
		/// </summary>
		/// <param name="arrow"></param>
		/// <param name="location"></param>
		/// <param name="alignmentPosition">Valid are only left, right or middle.</param>
		/// <param name="pars"></param>
		public static void CreateArrow(Polyline arrow, MyGeometry.Point2d location, MyGeometry.Vector2d alignment, ArrowParameters pars, ArrowPositionPoint alignmentPosition)
		{
			// eDegenerateGeometry if attempted to remove last vertex.
			while (arrow.NumberOfVertices > 0)
				arrow.RemoveVertexAt(0);
			
			// Start point of arrow is always start point of tail. End point of arrow, is "sharp" point.
			MyGeometry.Vector2d alignmentUnit = alignment.GetUnitVector();


			double arrowTotalLength = pars.TailLength + pars.HeadLength;

			MyGeometry.Vector2d displacement = new MyGeometry.Vector2d();// location.GetAsVector();
			switch (alignmentPosition)
			{
				case ArrowPositionPoint.Left:
					displacement = displacement.Add(alignmentUnit.Multiply(arrowTotalLength).Negate());
					break;
				case ArrowPositionPoint.Right:
					//displacement.Add(alignmentUnit.Multiply(arrowTotalLength));
					break;
				case ArrowPositionPoint.Middle:
					displacement = displacement.Add(alignmentUnit.Multiply(arrowTotalLength / 2.0).Negate());
					break;
			}

			// By default pipe is orientated from start point (0,0) to end point which is on alignment line (in direction of vector).
			// This means that start point of arrow and TopLeft point of MText are same. 
			List<MyGeometry.Point2d> points = new List<MyGeometry.Point2d>();
			points.Add(new MyGeometry.Point2d());
			points.Add(new MyGeometry.Point2d().Add(alignmentUnit.Multiply(pars.TailLength)));
			points.Add(points[1].Add(alignmentUnit.Multiply(pars.HeadLength)));
			if (pars.Direction == ArrowDirection.Against)
				points.Reverse();
			displacement = displacement.Add(location.GetAsVector());
			points[0] = points[0].Add(displacement);
			points[1] = points[1].Add(displacement);
			points[2] = points[2].Add(displacement);

			arrow.AddVertexAt(0, points[0].ToCADPoint(), 0, pars.TailWidth, pars.TailWidth);
			arrow.AddVertexAt(1, points[1].ToCADPoint(), 0, pars.HeadWidth, 0);
			arrow.AddVertexAt(2, points[2].ToCADPoint(), 0, 0, 0);
		}
		/// <summary>
		/// Creates arrow aligned to MText.
		/// </summary>
		/// <param name="arrow">Polyline must be added to database. Any existing vertex will be deleted.</param>
		/// <param name="text"></param>
		/// <param name="alignment">Vector in direction of text.</param>
		/// <returns></returns>
		public static void CreateArrowAndAlign(Polyline arrow, MText text, MyGeometry.Vector2d alignment, ArrowParameters pars, ArrowPositionText alignmentPosition)
		{
			while (arrow.NumberOfVertices > 0)
				arrow.RemoveVertexAt(0);

			// MText location is TopLeft. 
			// Vector which goes normaly on text.
			MyGeometry.Vector2d alignmentNormal = alignment.GetAs3d().CrossProduct(new MyGeometry.Vector3d(0, 0, 1)).GetAs2d();
			MyGeometry.Vector2d alignmentNormalUnit = alignmentNormal.GetUnitVector();
			MyGeometry.Vector2d alignmentUnit = alignment.GetUnitVector();

			// Start point of arrow is always start point of tail. End point of arrow, is "sharp" point.


			double arrowTotalLength = pars.TailLength + pars.HeadLength;
			double textHeight = text.TextHeight;
			double textLength = 0;
			{
				double rotation = text.Rotation;
				text.Rotation = 0;
				textLength = text.GeometricExtents.MaxPoint.X - text.GeometricExtents.MinPoint.X;
				text.Rotation = rotation;
			}

			CreateArrow(arrow, text.Location.FromCADPoint().GetAs2d(), alignment, pars, ArrowPositionPoint.Right);

			MyGeometry.Vector2d displacement = new MyGeometry.Vector2d();
			// Move a little bit from the text.
			displacement = displacement.Add(alignmentUnit.Multiply(textHeight * 0.3).Negate());
			switch (alignmentPosition)
			{
				case ArrowPositionText.LeftTop:
					displacement = displacement.Add(alignmentUnit.Multiply(arrowTotalLength).Negate());
					break;
				case ArrowPositionText.LeftMiddle:
					displacement = displacement.Add(alignmentUnit.Multiply(arrowTotalLength).Negate());
					displacement = displacement.Add(alignmentNormalUnit.Multiply(textHeight / 2));
					break;
				case ArrowPositionText.LeftBottom:
					displacement = displacement.Add(alignmentUnit.Multiply(arrowTotalLength).Negate());
					displacement = displacement.Add(alignmentNormalUnit.Multiply(textHeight));
					break;
				case ArrowPositionText.RightTop:
					displacement = displacement.Add(alignmentUnit.Multiply(textLength));
					break;
				case ArrowPositionText.RightMiddle:
					displacement = displacement.Add(alignmentUnit.Multiply(textLength));
					displacement = displacement.Add(alignmentNormalUnit.Multiply(textHeight / 2));
					break;
				case ArrowPositionText.RightBottom:
					displacement = displacement.Add(alignmentUnit.Multiply(textLength));
					displacement = displacement.Add(alignmentNormalUnit.Multiply(textHeight));
					break;
			}

			List<MyGeometry.Point2d> points = new List<MyGeometry.Point2d>(3);
			points.Add(arrow.GetPoint2dAt(0).FromCADPoint());
			points.Add(arrow.GetPoint2dAt(1).FromCADPoint());
			points.Add(arrow.GetPoint2dAt(2).FromCADPoint());


			points[0] = points[0].Add(displacement);
			points[1] = points[1].Add(displacement);
			points[2] = points[2].Add(displacement);

			// Can't remove last point! eDegenerateGeometry.
			while (arrow.NumberOfVertices > 1)
				arrow.RemoveVertexAt(0);
			arrow.AddVertexAt(1, points[0].ToCADPoint(), 0, pars.TailWidth, pars.TailWidth);
			arrow.AddVertexAt(2, points[1].ToCADPoint(), 0, pars.HeadWidth, 0);
			arrow.AddVertexAt(3, points[2].ToCADPoint(), 0, 0, 0);

			arrow.RemoveVertexAt(0);

			/*// By default pipe is orientated from start point (0,0) to end point which is on alignment line (in direction of vector).
			// This means that start point of arrow and TopLeft point of MText are same. 
			List<MyGeometry.Point2d> points = new List<MyGeometry.Point2d>();
			points.Add(new MyGeometry.Point2d());
			points.Add(new MyGeometry.Point2d().Add(alignmentUnit.Multiply(pars.TailLength)));
			points.Add(points[1].Add(alignmentUnit.Multiply(pars.HeadLength)));
			if (pars.Direction == ArrowDirection.Against)
				points.Reverse();
			points[0] = points[0].Add(displacement);
			points[1] = points[1].Add(displacement);
			points[2] = points[2].Add(displacement);

			arrow.AddVertexAt(0, points[0].ToCADPoint(), 0, pars.TailWidth, pars.TailWidth);
			arrow.AddVertexAt(1, points[1].ToCADPoint(), 0, pars.HeadWidth, 0);
			arrow.AddVertexAt(2, points[2].ToCADPoint(), 0, 0, 0);*/



		}
	}
}
