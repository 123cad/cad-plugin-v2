﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common
{
    public class PolylineVertex<T> where T : struct
    {
        public T Point { get; set; }
        /// <summary>
        /// Positive - turn left to next vertex (center of arx is on the left).
        /// Negative is opposite.
        /// </summary>
        public double Bulge { get; set; }
        public double StartWidth { get; set; }
        public double EndWidth { get; set; }

        public PolylineVertex(T pt)
        {
            Point = pt;
        }
        public PolylineVertex(T pt, double bulge) : this(pt, bulge, 0, 0)
        {
        }
        public PolylineVertex(T pt, double startWidth, double endWidth) : this(pt, 0, startWidth, endWidth)
        {
        }
        public PolylineVertex(T pt, double bulge, double startWidth, double endWidth) : this(pt)
        {
            Bulge = bulge;
            StartWidth = startWidth;
            EndWidth = endWidth;
        }

        public override string ToString()
        {
            return $"({Point.ToString()}) Bulge: {Bulge.ToString("0.00")}  Width: {StartWidth.ToString("0.00")}:{EndWidth.ToString("0.00")}";

        }
        public bool Equals(PolylineVertex<T> t) 
        {
            // SHIT solution, Generic is completely broken, but i need to use specific IsEqualTo
            // because of tolerance.
            IEqualityComparer<T> comparer = Common.Comparers.CADComparer.Get<T>();
            if (!comparer.Equals(Point, t.Point))
                return false;

            if (!System.DoubleHelper.AreEqual(Bulge, t.Bulge, 0.001))
                return false;
            if (!System.DoubleHelper.AreEqual(StartWidth, t.StartWidth, 0.0001))
                return false;
            if (!System.DoubleHelper.AreEqual(EndWidth, t.EndWidth, 0.0001))
                return false;
            return true;
        }
        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(PolylineVertex<T>))
                return Equals((PolylineVertex<T>)obj);
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
    /// <summary>
    /// Contains helper methods for polyline object (Polyline, Polyline2d, Polyline3d).
    /// </summary>
    public static class CADPolylineHelper
    {

        //NOTE Object which works with any polyline.

        /// <summary>
        /// Returns 3d points for entity which can be Polyline, Polyline2d, Polyline3d.
        /// </summary>
        /// <param name="ent"></param>
        /// <returns></returns>
        public static List<Point3d> GetPoints(Entity ent, Transaction data)
        {
            List<Point3d> pts = new List<Point3d>();

            Polyline pl = ent as Polyline;
            Polyline2d pl2 = ent as Polyline2d;
            Polyline3d pl3 = ent as Polyline3d;
            if (pl != null)
            {
                pts.AddRange(pl.GetPoints().Select(x => new Point3d(x.X, x.Y, 0)).ToArray());
            }
            else if (pl2 != null)
            {
                pts.AddRange(pl2.GetPoints(data).Select(x => new Point3d(x.X, x.Y, 0)).ToArray());
            }
            else if (pl3 != null)
            {
                pts.AddRange(pl3.GetPoints(data));
            }
            else
                throw new InvalidCastException("Polyline get points: object is not polyline!");

            return pts;
        }
        /// <summary>
        /// Reverses points in Polyline, Polyline2d or Polyline3d object.
        /// </summary>
        /// <param name="ent"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static void ReversePoints(Entity ent, Transaction data)
        {
            Curve c = ent as Curve;
            c.ReverseCurve();
        }

        /// <summary>
        /// Replaces points in Polyline, Polyline2d or Polyline3d object, with provided ones.
        /// </summary>
        /// <param name="ent"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static void ReplacePoints(Entity ent, Transaction data, params Point3d[] pts)
        {
            Polyline pl = ent as Polyline;
            Polyline2d pl2 = ent as Polyline2d;
            Polyline3d pl3 = ent as Polyline3d;
            if (pl != null)
            {
                pl.ReplacePoints(pts.Select(x => new Point2d(x.X, x.Y)).ToArray());
            }
            else if (pl2 != null)
            {
                throw new NotImplementedException("replace points for polyline2d not implemented");
                //pts.AddRange(pl2.GetPoints(data).Select(x => new Point3d(x.X, x.Y, 0)).ToArray());
            }
            else if (pl3 != null)
            {
                pl3.ReplacePoints(data, pts);
                //pts.AddRange(pl3.GetPoints(data));
            }
            else
                throw new InvalidCastException("Polyline replace points: object is not polyline!");
        }

        /// <summary>
        /// Splits polyline at desired distances, with preserving initial bulge.
        /// (bulge is applied to segment from current to next point)
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="distances">Not needed to be ordered. Out of scope distances are ignored.</param>
        /// <returns></returns>
        public static IEnumerable<PolylineVertex<Point2d>> SplitPolylineAtDistances
            (IEnumerable<PolylineVertex<Point2d>> vertices, params double[] distances)
        {
            int index = 0;
            return SplitPolylineAtDistances2d(vertices, ref index, distances);
        }
        /// <summary>
        /// Splits polyline at desired distances (in xy plane), with adjusting bulge.
        /// (bulge is applied to segment from current to next point)
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="distances">Not needed to be ordered. Out of scope distances are ignored.</param>
        public static IEnumerable<PolylineVertex<Point2d>> SplitPolylineAtDistances2d
            (IEnumerable<PolylineVertex<Point2d>> vertices, ref int newIndex, params double[] distances)
        {
            var doc = Application.DocumentManager.MdiActiveDocument;
            //using (TransactionWrapper tw = doc.StartTransaction())
            using (Polyline pl = new Polyline())// ;// tw.CreateEntity<Polyline>();
            {
                pl.AddPoints(vertices.ToArray());
                distances = distances.OrderBy(x => x).ToArray();

                double totalLength = pl.Length;
                List<Point3d> breakPoints = new List<Point3d>();
                for (int i = 0; i < distances.Count(); i++)
                {
                    double d = distances[i];
                    if (totalLength < d)
                        break;
                    breakPoints.Add(pl.GetPointAtDist(d));
                }

                var result = splitPolyline(pl, breakPoints);

                int index = 0;
                bool found = false;
                var e1 = vertices.GetEnumerator();
                var e2 = result.GetEnumerator();
                // Second collection must be of equal of greater length.
                while (e1.MoveNext() && e2.MoveNext())
                {
                    if (!e1.Current.Point.IsEqualTo(e2.Current.Point, new Tolerance(0, 0.0001)))
                    {
                        found = true;
                        break;
                    }
                    index++;
                }
                if (!found)
                    index = -1;
                newIndex = index;

                return result;
            }
        }
        public static IEnumerable<PolylineVertex<Point2d>> SplitPolylineAtParameters
            (IEnumerable<PolylineVertex<Point2d>> vertices, params double[] parameters)
        {
            var doc = Application.DocumentManager.MdiActiveDocument;
            //using (TransactionWrapper tw = doc.StartTransaction())
            using (Polyline pl = new Polyline())
            {
                pl.AddPoints(vertices.ToArray());
                parameters = parameters.OrderBy(x => x).ToArray();

                int verticesCount = vertices.Count();
                List<Point3d> breakPoints = new List<Point3d>();
                for (int i = 0; i < parameters.Length; i++)
                {
                    double p = parameters[i];
                    if ((int)p >= verticesCount - 1)
                        break;
                    breakPoints.Add(pl.GetPointAtParameter(p));
                }
                
                var result = splitPolyline(pl, breakPoints);


                return result;
            }
        }
        private static IEnumerable<PolylineVertex<Point2d>> splitPolyline(Polyline pl, IEnumerable<Point3d> breakPoints)
        {
            Point3dCollection cl = new Point3dCollection(breakPoints.ToArray());
            var objects = pl.GetSplitCurves(cl);

            List<PolylineVertex<Point2d>> newPoints = new List<PolylineVertex<Point2d>>();
            PolylineVertex<Point2d> last = null;
            foreach (DBObject o in objects)
            {
                Polyline poly = (Polyline)o;

                PolylineVertex<Point2d>[] vertices = poly.GetVertices(false).ToArray();
                newPoints.AddRange(vertices.Take(vertices.Length - 1));
                poly.Dispose();

                last = vertices[vertices.Length - 1];
            }
            newPoints.Add(last);
            return newPoints;
        }

        /// <summary>
        /// Makes interpolated 3d points from 2d arc. 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="bulge"></param>
        /// <param name="startZ">Z of start point.</param>
        /// <param name="endZ">Offset of End point relative to Start point (negative means EndZ is below Start)</param>
        /// <param name="interpolationStepParameter">Parameter (0.0-1.0) which defines step length relative to arc length. </param>
        /// <returns></returns>
        public static IEnumerable<Point3d> Interpolate(Point2d start, Point2d end, double bulge, double startZ, double endZ, double interpolationStepParameter)
        {
            double deltaZ = startZ - endZ;
            var doc = Application.DocumentManager.MdiActiveDocument;
            List<Point3d> interpolatedPoints = new List<Point3d>();
            List<Point2d> points = new List<Point2d>();
            using (Polyline pl = new Polyline())
            {
                pl.AddPoints(
                    new PolylineVertex<Point2d>(start, bulge),
                    new PolylineVertex<Point2d>(end)
                    );
                if (interpolationStepParameter < 0.001)
                    interpolationStepParameter = 0.1;
                double zStep = deltaZ / (1 / interpolationStepParameter);
                double currentZ = startZ;

                int stepCount = (int)Math.Round(1 / interpolationStepParameter) + 1;
                double currentParameter = 0;
                for (int i = 0; i < stepCount; i++)
                {
                    Point2d pt = pl.GetPointAtParameter(currentParameter).GetAs2d();
                    interpolatedPoints.Add(pt.GetAs3d(currentZ));
                    currentZ -= zStep;
                    currentParameter += interpolationStepParameter;
                    if (i == stepCount - 1)
                        currentParameter = 1;
                }

                return interpolatedPoints;
            }

        }

        /// <summary>
        /// Returns bulge (for polyline) for arc from start to end point.
        /// Middle point is helping point, which is on arc.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="middle"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static double GetBulgeArc(Point2d start, Point2d middle, Point2d end)
        {
            var v1 = start.GetVectorTo(middle);
            var v2 = start.GetVectorTo(end);
            if (v1.IsCodirectionalTo(v2))
                return 0;

            using (CircularArc2d ca = new CircularArc2d(start, middle, end))
            { 

                double bulge = Math.Tan((ca.EndAngle - ca.StartAngle) / 4);
                if (ca.IsClockWise)
                    bulge = -bulge;
                return bulge;
            }
        }
        /// <summary>
        /// Returns bulge (for polyline) for arc from start to end point.
        /// Center is center of radius.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="center"></param>
        /// <returns></returns>
        public static double GetBulgeCircle(Point2d start, Point2d end, Point2d center)
        {
            var v = start.GetVectorTo(center);
            Point2d middle;
            using (CircularArc2d ca = new CircularArc2d(center, v.Length))
            {
                // Get middle point and use GetBulgeArc to get bulge.
                Point2d testPoint = start.Add(v.GetNormal().MultiplyBy(v.Length / 2));
                middle = ca.GetClosestPointTo(testPoint).Point;
            }
            return GetBulgeArc(start, end, middle);
        }
    }
}

