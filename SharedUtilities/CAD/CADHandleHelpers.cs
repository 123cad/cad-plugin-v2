﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common
{
    public class CADHandleHelpers
    {/// <summary>
	 /// Prints handle for the object selected by the user.
	 /// </summary>
	 /// <param name="doc"></param>
		public static void WriteObjectHandle(Document doc)
		{
			while (true)
			{
				PromptEntityOptions opt = new PromptEntityOptions("Select object (esc for cancel):\n");
				PromptEntityResult res = doc.Editor.GetEntity(opt);
				if (res.Status == PromptStatus.Cancel)
					return;
				doc.Editor.WriteMessage("ObjectId = " + res.ObjectId.ToString() + ", ObjectHandle = <" + res.ObjectId.Handle.Value.ToString() + "> hex: 0x" + res.ObjectId.Handle.Value.ToString("X"));
			}
		}

		public static void SelectObjectByHandle(Document doc)
		{
			while (true)
			{


				PromptStringOptions opt = new PromptStringOptions("Enter object handle in hex (without 0x)(esc for cancel):");
				PromptResult res = doc.Editor.GetString(opt);
				if (res.Status == PromptStatus.Cancel)
					return;
				long l;
				try
				{
					l = long.Parse(res.StringResult, System.Globalization.NumberStyles.HexNumber);
				}
				catch (System.Exception)
				{
					doc.Editor.WriteMessage("Not valid handle!\n");
					continue;
				}
				Handle h = new Handle(l);
				ObjectId oid = doc.Database.GetObjectId(false, h, 0);
				ObjectId[] oids = new ObjectId[1];
				oids[0] = oid;
				try
				{
					doc.Editor.SetImpliedSelection(oids);
				}
				catch (System.Exception)
				{
					doc.Editor.WriteMessage("Not valid oid!\n");
				}
			}
		}
	}
}

