﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
using Teigha.GraphicsInterface;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.GraphicsInterface;
#endif

namespace CadPlugin.Common.CAD
{
    /// <summary>
    /// Abstract class which handles calculation of current mouse position.
    /// 
    /// </summary>
    public abstract class DrawJigBase : DrawJig
    {

        public Matrix3d UCS
        {
            get
            {
                return Application.DocumentManager.MdiActiveDocument.Editor.CurrentUserCoordinateSystem;
            }
        }
        /// <summary>
        /// Current mouse position.
        /// </summary>
        public Point3d CurrentPosition { get; private set; }


        /// <summary>
        /// Initial position;
        /// </summary>
        protected Point3d startPosition { get; }

        /// <summary>
        /// Last point that user has chosen by moving the mouse.
        /// </summary>
        private Point3d newPoint;

        public DrawJigBase(Point3d startPosition)
        {
            this.startPosition = startPosition;
            CurrentPosition = startPosition;
        }

        protected override bool WorldDraw(WorldDraw draw)
        {
            WorldGeometry geo = draw.Geometry;
            if (geo != null)
            {
                geo.PushModelTransform(UCS);

                // Represents vector from current reference point of entities, to user selected point.
                Vector3d moveVector = CurrentPosition.GetVectorTo(newPoint);
                CurrentPosition = newPoint;
                Matrix3d moveMatrix = Matrix3d.Displacement(moveVector);

                //Vector3d v = center.GetVectorTo(CurrentPosition);
                DrawEntities(moveVector, moveMatrix, geo);

                geo.PopModelTransform();
            }

            return true;
        }

        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            JigPromptPointOptions prOptions1 = new JigPromptPointOptions("\n");
            prOptions1.UseBasePoint = false;
            prOptions1.UserInputControls =
                UserInputControls.NullResponseAccepted | UserInputControls.Accept3dCoordinates |
                UserInputControls.GovernedByUCSDetect | UserInputControls.GovernedByOrthoMode/* |
                UserInputControls.AcceptMouseUpAsPoint*/;

            PromptPointResult prResult1 = prompts.AcquirePoint(prOptions1);
            if (prResult1.Status == PromptStatus.Cancel || prResult1.Status == PromptStatus.Error)
                return SamplerStatus.Cancel;

            Point3d tempPt = prResult1.Value.TransformBy(UCS.Inverse());
            newPoint = tempPt;

            return SamplerStatus.OK;
        }


        /// <summary>
        /// Intented for extending, to draw additional entities.
        /// </summary>
        /// <param name="moveFromLastPoint">Vector of movement between last user point and current user point.</param>
        /// <param name="transformFromLastPoint">Displacement matrix from last user point to current one</param>
        protected abstract void DrawEntities(Vector3d moveFromLastPoint, Matrix3d transformFromLastPoint, WorldGeometry draw);
    }
    /// <summary>
    /// Jigs provided entitites to follow mouse position.
    /// </summary>
    public class DefaultDrawJig : DrawJigBase
    {
        private IEnumerable<Entity> entities;
         
        public DefaultDrawJig(Point3d startPosition, IEnumerable<Entity> entities) : base (startPosition)
        {
            this.entities = entities;
        }

        protected override void DrawEntities(Vector3d moveFromLastPoint, Matrix3d transformFromLastPoint, WorldGeometry draw)
        {
            foreach (var e in entities)
            {
                e.TransformBy(transformFromLastPoint);
                draw.Draw(e);
            }
        }
    }
}
