﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif


namespace CadPlugin.Common
{
	/// <summary> 
	/// Contains some common methods regarding working with CAD. When there is 
	/// enough data, it can be extracted to separate class.
	/// </summary>
	public static class CADCommonHelper
	{	


		/// <summary>
		/// Creates set of offset points. Same like OFFSET command.
		/// </summary>
		/// <param name="points"></param>
		/// <param name="tr"></param>
		/// <param name="offset"></param>
		/// <returns></returns>
		public static IList<Point2d> CreateOffsetPoints(IEnumerable<Point2d> points, double offset)
		{
			Polyline pl = new Polyline();// tr.Tr.CreateEntity(tr.Btr, EntityToCreate.Polyline) as Polyline;
			pl.AddPoints(points.ToArray());

			DBObjectCollection collection = pl.GetOffsetCurves(offset);
			int entityCount = 0;
			Entity ent = null;
			foreach (Entity e in collection)
			{
				// Should be only 1 entity.
				entityCount++;
				if (entityCount > 1)
					MyLog.MyTraceSource.Warning($"More than 1 entity exists in offseting {entityCount}! ");
				ent = e;
			}
			Polyline plOffset = ent as Polyline;
			Debug.Assert(plOffset != null, "Offset entity is not a Polyline!");
			List<Point2d> newPoints = new List<Point2d>();
			if (plOffset != null)
			{
				var v = plOffset.GetPoints();
				foreach(Point2d pt in v)
				{
					newPoints.Add(pt);
				}
			}
			pl.Dispose();
			//pl.Erase();
			return newPoints;
		}

		/// <summary>
		/// Creates collection of points in 2d (have same height as original points). Same like OFFSET command.
		/// (since only 2d can be offset, this is just helper method for 2d offset).
		/// </summary>
		/// <param name="points"></param>
		/// <param name="tr"></param>
		/// <param name="offset"></param>
		/// <returns></returns>
		public static IList<Point3d> CreateOffsetPoints(List<Point3d> points, double offset)
		{
			List<Point2d> points2 = points.Select(t => new Point2d(t.X, t.Y)).ToList();
			IList<Point2d> offsetPts2 = CreateOffsetPoints(points2,  offset);
			//Debug.Assert(points2.Count == offsetPts2.Count, "Created offset points count is different!");
			bool invalid = false;
			if (points2.Count != offsetPts2.Count && points2.Count == 2)
			{
				// Maybe some points are vertical.
				//if (points2[0].GetDistanceTo(points2[1]) < 1e-4)
				
			}
			List<Point3d> pts = new List<Point3d>();
			for (int i = 0; i < points2.Count; i++)
			{
				Point3d p3 = points[i];
				Point2d p2 = offsetPts2[i];
				Point3d p = new Point3d(p2.X, p2.Y, p3.Z);
				pts.Add(p);
			}
			return pts;
		}
        public static Extents2d GetObjectsExtents(Database db, IEnumerable<ObjectId> oids)
        {
            double minX = 0, minY = 0;
            double maxX = 0, maxY = 0;
            bool initialized = false;
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                foreach (ObjectId oid in oids)
                {
                    Entity e = tr.GetObject(oid, OpenMode.ForRead) as Entity;
                    if (e != null)
                    {
                        var extents = e.GeometricExtents;
                        if (!initialized)
                        {
                            initialized = true;
                            minX = extents.MinPoint.X;
                            minY = extents.MinPoint.Y;
                            maxX = extents.MaxPoint.X;
                            maxY = extents.MaxPoint.Y;
                        }
                        if (minX > extents.MinPoint.X)
                            minX = extents.MinPoint.X;
                        if (maxX < extents.MaxPoint.X)
                            maxX = extents.MaxPoint.X;

                        if (minY > extents.MinPoint.Y)
                            minY = extents.MinPoint.Y;
                        if (maxY < extents.MaxPoint.Y)
                            maxY = extents.MaxPoint.Y;
                    }
                }

                // Commit event readonly transaction is better for performance.
                tr.Commit();
            }
            return new Extents2d(minX, minY, maxX, maxY);
        }
        /// <summary>
        /// Returns rectangle which represents current view coordinate data.
        /// </summary>
        /// <param name="ed"></param>
        /// <returns></returns>
        public static ViewRectangle GetCurrentView(Editor ed)
        {
            double currentWidth = 1;
            double currentHeight = 1;
            MyUtilities.Geometry.Point3d center;
            using (ViewTableRecord acView = ed.GetCurrentView())
            {
                currentWidth = acView.Width;
                currentHeight = acView.Height;
                center = acView.CenterPoint.FromCADPoint().GetAsPoint3d();
            }
            ViewRectangle rectangle = new ViewRectangle(currentWidth, currentHeight, center);
            return rectangle;
        }
        
        /// <summary>
        /// Gets Extents which includes all objects in the drawing.
        /// CAUTION: this can impact on performance (100.000 objects 
        /// </summary>
        /// <param name="doc"></param>
        public static ViewRectangle GetExtents(Document doc)
        {
            // This doesn't work since acad 2000.
            /*Point3d max = doc.Database.Extmax;
            Point3d min = doc.Database.Extmin;
            double width = max.X - min.X;
            double height = max.Y - min.Y;
            Point3d center = new Point3d((max.X + min.X) / 2, (max.Y + min.Y) / 2, 0);
            ViewRectangle rec = new ViewRectangle(width, height, center.FromCADPoint());
            //doc.SendStringToExecute("._zoom _all ", true, false, false);*/
            double minX = 0;
            double minY = 0;
            double maxX = 0;
            double maxY = 0;
            bool initialized = false;
            int counter = 0;
            using (TransactionWrapper tr = doc.StartTransaction())
            {
                foreach (ObjectId oid in tr.MS)
                {

                    Entity e = tr.GetObjectRead<Entity>(oid);
                    if (e == null)
                        continue;
                    counter++;
                    var ext = e.GeometricExtents;
                    if (!initialized)
                    {
                        initialized = true;
                        minX = ext.MinPoint.X;
                        maxX = ext.MaxPoint.X;
                        minY = ext.MinPoint.Y;
                        maxY = ext.MaxPoint.Y;
                    }
                    if (minX > ext.MinPoint.X)
                        minX = ext.MinPoint.X;
                    if (maxX < ext.MaxPoint.X)
                        maxX = ext.MaxPoint.X;
                    if (minY > ext.MinPoint.Y)
                        minY = ext.MinPoint.Y;
                    if (maxY < ext.MaxPoint.Y)
                        maxY = ext.MaxPoint.Y;
                }
                //foreach(var id in doc.Database.)

                tr.Commit();
            }

            double width = maxX - minX;
            double height = maxY - minY;
            Point3d center = new Point3d(minX + width / 2, minY + height / 2, 0);
            var data = new ViewRectangle(width, height, center.FromCADPoint());
            return data;
        }

	}
}
