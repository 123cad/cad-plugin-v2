﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common.CAD
{
    public class SystemVariables
    {
        public static Osnap LastOsnap { get; private set; } = Osnap.EndPoint;
        public static void ApplySavedOsnap() => SetOsnap(LastOsnap);
        public static void SaveCurrentOsnap() => LastOsnap = GetOsnap();
        public static void SetOsnap(Osnap os)
        {
            Application.SetSystemVariable("OSMODE", (Int16)os);
        }
        /// <summary>
        /// Returns current osnap.
        /// </summary>
        /// <returns></returns>
        public static Osnap GetOsnap()
        {
            int v = (Int16)Application.GetSystemVariable("OSMODE");
            Osnap snap = Osnap.None;
            foreach (Osnap o in Enum.GetValues(typeof(Osnap)))
            {
                if (((int)o & v) != 0)
                    snap |= o;
            }
            return snap;
        }

        public static object GetSystemVariable(string name)
        {
            return Application.GetSystemVariable(name);
        }
        public static void SetSystemVariable(string name, object value)
        {
            Application.SetSystemVariable(name, value);
        }
    }
    public enum Osnap
    {
        None = 0x0,
        EndPoint = 0x0001,
        MidPoint = 0x0002,
        Center = 0x0004,
        Node = 0x0008,
        Quadrant = 0x0010,
        Intersection = 0x0020,
        Insertion = 0x0040,
        Perpendicular = 0x0080,
        Tangent = 0x0100,
        Nearest = 0x0200,
        GeometricCenter = 0x0400,
        ApparentIntersection = 0x0800,
        Extension = 0x1000,
        Parallel = 0x2000,
        /// <summary>
        /// Developers creating custom routines can 
        /// use the 16384 bitcode to temporarily 
        /// suppress the current running object 
        /// snap settings without losing the 
        /// original settings.
        /// </summary>
        SuppressCurrentSettings = 0x4000
    }
}
