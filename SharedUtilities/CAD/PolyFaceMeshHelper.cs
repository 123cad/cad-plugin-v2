﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace SharedUtilities.CAD
{
    public static class PolyFaceMeshHelper
    {
        /// <summary>
        /// Extracts objects of type T from PolyfaceMesh.
        /// <para>
        /// IMPORTANT: Invoker is responsible for disposing all created objects
        /// (because they are not added to transaction).
        /// </para>
        /// </summary>
        public static IEnumerable<T> Extract<T>(this PolyFaceMesh f, Transaction tr) 
            where T:class
        {
            var list = new List<T>();
            try
            {
                var collection = new DBObjectCollection();
                f.Explode(collection);

                foreach(DBObject obj in collection)
                {
                    var t = obj as T;
                    if (t != null)
                        list.Add(t);
                }
            }
            catch { }
            return list;
        }

        /// <summary>
        /// Extracts faces from PolyFaceMesh.
        /// <para>
        /// IMPORTANT: Invoker is responsible for disposing all created Faces
        /// (because they are not added to transaction).
        /// </para>
        /// </summary>
        public static IEnumerable<Face> ExtractFaces(this PolyFaceMesh f, Transaction tr)
            => f.Extract<Face>(tr);
    }
}
