﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common
{
    public class CADFacePoints
    {
        public Point3d P1 { get; set; }
        public Point3d P2 { get; set; }
        public Point3d P3 { get; set; }

        /// <summary>
        /// Maybe exists.
        /// </summary>
        public Point3d? P4 { get; set; }

        public IEnumerable<Point3d> GetPoints()
        {
            var l = new List<Point3d>()
            {
                P1, P2, P3
            };
            if (P4.HasValue)
                l.Add(P4.Value);
            return l;
        }
    }
    public static class CADFaceHelper
	{
		public static List<Point3d> GetPointsCollection(this Face f)
        {
            var pts = new List<Point3d>();
            Point3d last = new Point3d();
            // There should be 4 points, where 3 and 4 are same.
            for (int i = 0; i < 4; i++)
            {
                var p = f.GetVertexAt((short)i);
                if (i == 3)
                {
                    if (p.GetVectorTo(last).Length < 1e-5)
                        break;
                }
                pts.Add(p);
                last = p;
            }
            return pts;
        }

        public static CADFacePoints GetPoints(this Face f)
        {
            var pts = GetPointsCollection(f);
            var pt = new CADFacePoints();
            pt.P1 = pts[0];
            pt.P2 = pts[1];
            pt.P3 = pts[2];
            if (pts.Count >= 4)
                pt.P4 = pts[3];
            return pt;
        }
    }
}

