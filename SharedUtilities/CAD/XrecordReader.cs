﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

#if AUTOCAD
namespace Autodesk.AutoCAD.DatabaseServices
#endif

#if BRICSCAD
namespace Teigha.DatabaseServices
#endif
{
	/// <summary>
	/// Wrapper around Xrecord object data. Performance improvement over data access.
	/// </summary>
	public class XrecordReader
	{
		public Xrecord Xrecord { get; private set; }
		/// <summary>
		/// Total count of TypedValue objects.
		/// </summary>
		public int DataCount => originalValues.Length;

		private TypedValue[] originalValues { get; }
		public XrecordReader(Xrecord xrecord)
		{
			Xrecord = xrecord;
			originalValues = xrecord.Data?.AsArray()??new TypedValue[0];
		}

		public TypedValue this[int i]
		{
			get => originalValues[i];
		}

	}
}
