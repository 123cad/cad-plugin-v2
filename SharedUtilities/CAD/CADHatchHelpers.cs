﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common
{
    public class CADHatchHelpers
    {

		/// <summary>
		/// Returns all hatch patterns from CAD directory.
		/// </summary>
		public static void GetAllHatchPatterns(ref List<string> defaultPatterns, ref List<string> customPatterns, bool loadDefault = true)
		{
			try
			{
#if BRICSCAD
				string fileName = "default.pat";
#endif
#if AUTOCAD
			string fileName = "acad.pat";
#endif
				//foreach(string s in HatchPatterns)
				string fullName = HostApplicationServices.Current.FindFile(fileName, null, FindFileHint.PatternFile);
				if (string.IsNullOrEmpty(fullName))
					return;
				string dir = Path.GetDirectoryName(fullName);
				List<string> patFiles = new List<string>();
				string[] files = Directory.GetFiles(dir, "*.pat");
				foreach (string s in files)
				{
					string file = Path.GetFileName(s);
					if (file != fileName)
					{
						// Custom pattern file can contain only 1 pattern (with same name as file name)
						customPatterns.Add(Path.GetFileNameWithoutExtension(file));
					}
					else
					{
						if (loadDefault)
							// Read all commands from line path.
							using (FileStream fs = new FileStream(s, FileMode.Open))
							{
								StreamReader reader = new StreamReader(fs);
								while (true)
								{
									string line = reader.ReadLine();
									if (reader.EndOfStream)
										break;
									if (string.IsNullOrEmpty(line))
										continue;
									if (line[0] == '*')
									{
										int endIndex = line.IndexOf(',') - 1;
										if (endIndex == -1)
											continue;
										string pattern = line.Substring(1, endIndex);
										defaultPatterns.Add(pattern);
									}
								}
							}
					}
				}
			}
			catch { }

		}
	}
}
