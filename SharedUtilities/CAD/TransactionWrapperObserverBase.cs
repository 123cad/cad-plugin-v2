﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace SharedUtilities.CAD
{
    public interface ITransactionWrapperObserver
    {
        void EntityCreated(TransactionWrapper tw, Entity e);
        void Attached(TransactionWrapper tw);
        void Detached(TransactionWrapper tw);
    }
    /// <summary>
    /// Use this to get notifications of events in
    /// transaction wrapper.
    /// </summary>
    /// <remarks>
    /// Implement this interface, and add it to TransactionWrapper.
    ///
    /// Warning: Do not save Entity instance! Use it for business logic,
    /// but only save ObjectId.
    /// </remarks>
    public abstract class TransactionWrapperObserverBase : ITransactionWrapperObserver
    {
        /// <summary>
        /// Invoked when entity is created.
        /// </summary>
        protected virtual void EntityCreated(TransactionWrapper tw, Entity e){}

        /// <summary>
        /// When added to TransactionWrapper.
        /// </summary>
        protected virtual void Attached(TransactionWrapper tw){}

        /// <summary>
        /// When removed from TransactionWrapper (includes Dispose).
        /// </summary>
        protected virtual void Detached(TransactionWrapper tw){}


        // Explicitly implemented so not everyone can see interface methods.

        void ITransactionWrapperObserver.EntityCreated(TransactionWrapper tw, Entity e)
        {
            EntityCreated(tw, e);
        }

        void ITransactionWrapperObserver.Attached(TransactionWrapper tw)
        {
            Attached(tw);
        }

        void ITransactionWrapperObserver.Detached(TransactionWrapper tw)
        {
            Detached(tw);
        }
    }
}
