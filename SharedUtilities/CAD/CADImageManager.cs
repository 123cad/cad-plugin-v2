﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common
{
    public class CADImageManager
    {
        public class ImageInfo
        {
            /// <summary>
            /// Unique image id in the drawing.
            /// </summary>
            public string Id { get; private set; }
            /// <summary>
            /// Full file path to the image.
            /// </summary>
            public string FilePath { get; private set; }
            private ImageInfoProperties _Properties;
            public ImageInfoProperties Properties
            {
                get { return _Properties; }
                set
                {
                    if (value == null)
                        throw new ArgumentNullException("ImageInfo can't have empty properties.");
                    _Properties = value;
                }
            }
            protected ImageInfo(string id, string path)
            {
                Id = id;
                FilePath = path;
                Properties = new ImageInfoProperties();
            }
        }
        public class ImageInfoProperties
        {
            public double Width { get; set; }
            public double Height { get; set; }
            /// <summary>
            /// Top left corner of the image.
            /// </summary>
            public MyUtilities.Geometry.Point3d BottomLeftPosition { get; set; }
            /// <summary>
            /// Direction is considered [TopLeft-TopRight] or [BottomLeft-BottomRight].
            /// (x axis of the image)
            /// </summary>
            public MyUtilities.Geometry.Vector3d DirectionVector { get; set; }
            /// <summary>
            /// Rotation of the image in radians.
            /// </summary>
            public double ImageRotationRad { get; set; } = 0;
            public ImageInfoProperties()
            {
                Width = 1;
                Height = 1;
                BottomLeftPosition = new MyUtilities.Geometry.Point3d(0, 0, 0);
                DirectionVector = new MyUtilities.Geometry.Vector3d(1,0,0);
            }
        }
        private class ImageInfo2:ImageInfo
        {
            /// <summary>
            /// With this CADImageManager class gains access to invoke ImageInfo constructor, but it
            /// is not possible to do from outside of the class.
            /// This assures us that only this enclosing class can create info object.
            /// </summary>
            /// <param name="id"></param>
            public ImageInfo2(string id, string path):base(id, path)
            {

            }
        }
        private readonly Transaction tr;
        private readonly Database db;
        private DBDictionary imageDictionary;
        /// <summary>
        /// To improve performance - we don't want to open single object many times.
        /// </summary>
        Dictionary<string, Tuple<ObjectId, RasterImageDef>> rasterImageDefs = new Dictionary<string, Tuple<ObjectId, RasterImageDef>>();

        public CADImageManager(Transaction tr, Database db)
        {
            this.tr = tr;
            this.db = db;
            ObjectId imageDictionaryId = RasterImageDef.GetImageDictionary(db);
            if (imageDictionaryId.IsNull)
            {
                RasterImageDef.CreateImageDictionary(db);
                imageDictionaryId = RasterImageDef.GetImageDictionary(db);
            }
            imageDictionary = tr.GetObject(imageDictionaryId, OpenMode.ForRead) as DBDictionary;
        }
        public ImageInfo LoadImage(string fileName)
        {
            ImageInfo info = GetImageInfo(fileName);

            // RasterImageDef object in ImageTable.
            RasterImageDef rasterImageDefinition;
            // Id of the image object in RasterImageDef.
            ObjectId rasterImageDefinitionId;
            
            if (imageDictionary.Contains(info.Id))
            {
                rasterImageDefinitionId = imageDictionary.GetAt(info.Id);
                rasterImageDefinition = tr.GetObject(rasterImageDefinitionId, OpenMode.ForWrite) as RasterImageDef;
            }
            else
            {
                rasterImageDefinition = new RasterImageDef();
                imageDictionary.UpgradeOpen();
                rasterImageDefinitionId = imageDictionary.SetAt(info.Id, rasterImageDefinition);
                tr.AddNewlyCreatedDBObject(rasterImageDefinition, true);

                rasterImageDefinition.SourceFileName = info.FilePath;
                rasterImageDefinition.Load();

                imageDictionary.DowngradeOpen();
            rasterImageDefs.Add(info.Id, new Tuple<ObjectId, RasterImageDef>(rasterImageDefinitionId, rasterImageDefinition));
            }
            
            return info;
        }
        public void DrawImage(List<Entity> createdEntities, params ImageInfo[] infos)
        {
            ObjectId rasterImageDefinitionId;
            RasterImageDef rasterImageDefinition = null;
            var bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
            var btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
            foreach (ImageInfo info in infos)
            {
                if (!rasterImageDefs.ContainsKey(info.Id))
                    throw new FileNotFoundException($"Image {info.Id} has not been loaded! You must Load it first.");

                rasterImageDefinitionId = rasterImageDefs[info.Id].Item1;
                rasterImageDefinition = rasterImageDefs[info.Id].Item2;


                RasterImage rasterImage = new RasterImage();
                rasterImage.ImageDefId = rasterImageDefinitionId;
                btr.AppendEntity(rasterImage);
                tr.AddNewlyCreatedDBObject(rasterImage, true);
                createdEntities.Add(rasterImage);

                double imageWidth = info.Properties.Width;// info.Properties.Width * rasterImageDefinition.ResolutionMMPerPixel.X;
                double imageHeight = info.Properties.Height;// info.Properties.Height * rasterImageDefinition.ResolutionMMPerPixel.Y;

                Vector3d widthVector = new Vector3d(imageWidth, 0, 0);
                Vector3d heightVector = new Vector3d(0, imageHeight, 0);

                var pos = info.Properties.BottomLeftPosition;

                // Image is rotated around coordinate center. If some other point is used (than bottomleft), then, at the end, we need to translate image to desired position.
                // 

                //Point3d bottomLeft = new Point3d(pos.X, pos.Y - imageHeight, pos.Z);

                Vector3d directionX = info.Properties.DirectionVector.GetAs2d().GetAs3d(0).GetUnitVector().Multiply(imageWidth).ToCADVector();
                double angleRad = Math.PI / 180 * 90;
                Vector3d directionY = info.Properties.DirectionVector.GetAs2d().GetAs3d(0).GetUnitVector().Multiply(imageHeight).ToCADVector().RotateBy(angleRad, Vector3d.ZAxis);
                // Coordinate system point is considered to be Image bottom left point.
                CoordinateSystem3d coordinateSystem = new CoordinateSystem3d(pos.ToCADPoint(), directionX, directionY);
                coordinateSystem = new CoordinateSystem3d(pos.ToCADPoint(), new Vector3d(imageWidth, 0, 0), new Vector3d(0, imageHeight, 0));
                rasterImage.Orientation = coordinateSystem;
                //rasterImage.TransformBy(Matrix3d.Displacement(new Vector3d(0, -imageHeight, 0)));
                //double angleRad = new MyUtilities.Geometry.Vector3d(1, 0, 0).GetAngleRadXY(info.Properties.DirectionVector);
                //rasterImage.Rotation = angleRad;

                if (!DoubleHelper.AreEqual(info.Properties.ImageRotationRad, 0, 0.001))
                {
                    Point3d center = new Point3d(pos.X + imageWidth / 2, pos.Y + imageHeight / 2, 0);
                    rasterImage.TransformBy(Matrix3d.Rotation(info.Properties.ImageRotationRad, Vector3d.ZAxis, center));
                }
                rasterImage.AssociateRasterDef(rasterImageDefinition);
            }
            //tr.Doc.Editor.Regen();
        }
        private ImageInfo GetImageInfo(string filePath)
        {
            RasterImageDef rasterImageDefinition;
            ObjectId rasterImageDefinitionId;
            string fileName = Path.GetFileName(filePath);
            string id = fileName;
            ImageInfo info = null;
            int index = 0;
            do
            {
                if (imageDictionary.Contains(id))
                {
                    rasterImageDefinitionId = imageDictionary.GetAt(id);
                    rasterImageDefinition = tr.GetObject(rasterImageDefinitionId, OpenMode.ForWrite) as RasterImageDef;
                    if (rasterImageDefinition.SourceFileName == filePath)
                    {
                        info = new ImageInfo2(id, filePath);
                    }
                    id = fileName + index++;
                }
                else
                {
                    info = new ImageInfo2(id, filePath);
                }
            } while (info == null);
            return info;
        }
    }
}
