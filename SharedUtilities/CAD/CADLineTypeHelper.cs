﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Common 
{
	public static class CADLineTypeHelper
	{

		/// <summary>
		/// Returns all line types existing in the drawing.
		/// </summary>
		/// <param name="doc"></param>
		/// <returns></returns>
		public static List<string> GetLineTypes(Document doc = null)
		{
			if (doc == null)
				doc = Application.DocumentManager.MdiActiveDocument;
			List<string> lineTypes = new List<string>();
			Document acDoc = doc;
			Database acCurDb = acDoc.Database;
			using (Transaction acTrans = acCurDb.TransactionManager.StartTransaction())
			{
				LinetypeTable table = acTrans.GetObject(acCurDb.LinetypeTableId, OpenMode.ForRead) as LinetypeTable;
				foreach (ObjectId id in table)
				{
					LinetypeTableRecord rec = acTrans.GetObject(id, OpenMode.ForRead) as LinetypeTableRecord;
					lineTypes.Add(rec.Name);
				}
			}
			return lineTypes;
		}

		/// <summary>
		/// If linetype is loaded, does nothing. If linetype is not loaded, but exists as file, it is loaded.
		/// If not loaded and not file found - false is returned.
		/// </summary>
		/// <param name="db"></param>
		/// <param name="name"></param>
		/// <returns></returns>
		public static bool CheckLinetype(Database db, string name)
		{
				//bool loaded = false;
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				LinetypeTable lt = (LinetypeTable)tr.GetObject(db.LinetypeTableId, OpenMode.ForRead);
				return lt.Has(name);
			}
			/*
			
				if (lt.Has(name))
					loaded = true;
#if BRICSCAD
				string fileName = "default.lin";
#endif
#if AUTOCAD
				string fileName = "acad.lin";
#endif
				if (!loaded)
				{
					string fullName = HostApplicationServices.Current.FindFile(fileName, null, FindFileHint.Default);
//					if (string.IsNullOrEmpty(fullName))
//						loaded = false; 
					try
					{
						db.LoadLineTypeFile(name, fullName);
						loaded = true; 
					}
					catch (System.Exception e)
					{
						loaded = false;
					}
					if (!loaded)
					{
						string dir = Path.GetDirectoryName(fullName);
						List<string> patFiles = new List<string>();
						string[] files = Directory.GetFiles(dir, "*.lin");
						foreach (string s in files)
						{
							//if (Path.GetFileNameWithoutExtension(s).ToLower() == name.ToLower())
							{
								try
								{
									db.LoadLineTypeFile(name, s);
									loaded = true;
								}
								catch (System.Exception e)
								{
									loaded = false;
									break;
								}
							}
						}
					}
				}
				if (loaded)
					tr.Commit();
			}
			return loaded;*/
		}


		/// <summary>
		/// If linetype not loaded, it is created.
		/// </summary>
		public static ObjectId GetLineTypeId(Database db, string name)
		{
			ObjectId lineTypeId = ObjectId.Null;
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				LinetypeTable lt = (LinetypeTable)tr.GetObject(db.LinetypeTableId, OpenMode.ForWrite);
#if AUTOCAD
				string[] files = new string[]{ "acad.lin"};
#elif BRICSCAD
				string[] files = new string[] { "iso.lin", "default.lin" };
#endif
				bool found = false;

				if (!lt.Has(name))
				{
					foreach (string fileName in files)
					{
						try
						{
							db.LoadLineTypeFile(name, fileName);
							found = true;
							break;
						}
						catch (System.Exception)
						{

						}
					}
					if (!found)
					{
						System.Windows.Forms.MessageBox.Show("Selected linetype can't be found. Assigned \"Continuos\" linetype");
						name = "Continuous";
					}
					tr.Commit();
				}
			}
			//Must be in separate transactions, because of linetype load operation.
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				LinetypeTable lt = (LinetypeTable)tr.GetObject(db.LinetypeTableId, OpenMode.ForWrite);
				//else
				bool found = false;
				foreach (ObjectId id in lt)
				{
					LinetypeTableRecord ltr = tr.GetObject(id, OpenMode.ForRead) as LinetypeTableRecord;
					if (ltr.Name == name)
					{
						lineTypeId = id;
						found = true;
						break;
					}
				}
				if (!found)
					throw new NullReferenceException();

			}
			return lineTypeId;
		}
		/// <summary>
		/// Returns line type if it exists. If not, new one is created.
		/// Set arbitary number of dashes, where >0 means dash is visible, otherwise dash is not visible.
		/// </summary>
		public static ObjectId CreateLineType(Database db, string name, params double[] dashes)
		{
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				LinetypeTable lt = (LinetypeTable)tr.GetObject(db.LinetypeTableId, OpenMode.ForWrite);
				bool found = false;
				ObjectId ltId = ObjectId.Null;
				foreach (ObjectId id in lt)
				{
					LinetypeTableRecord ltr = tr.GetObject(id, OpenMode.ForRead) as LinetypeTableRecord;
					if (ltr.Name == name)
					{
						ltId = ltr.ObjectId;
						found = true;
						break;
					}
				}
				if (!found)
				{
					LinetypeTableRecord ltr = new LinetypeTableRecord();
					ltr.AsciiDescription = name;
					ltr.Name = name;
					ltr.Comments = name + " , __ __ __ ";
					ltr.PatternLength = dashes.Sum();
					ltr.NumDashes = dashes.Length;
					for (int i = 0; i < dashes.Length; i++)
						ltr.SetDashLengthAt(i, dashes[i]);
					//ltr.NumDashes = 2;
					//ltr.SetDashLengthAt(0, 0.2);
					//ltr.SetDashLengthAt(1, -0.1);
					lt.Add(ltr);
					tr.AddNewlyCreatedDBObject(ltr, true);
					ltId = ltr.ObjectId;
					tr.Commit();
				}
				return ltId;
			}
		}


	}
}