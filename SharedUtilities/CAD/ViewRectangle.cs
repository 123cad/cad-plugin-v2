﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Common
{
    public class ViewRectangle
    {
        public double Width { get; set; }
        public double Height { get; set; }
        public Point3d Center { get; set; }

        public ViewRectangle(double width, double height, Point3d center = new Point3d())
        {
            Width = width;
            Height = height;
            Center = center;
        }
    }
}
