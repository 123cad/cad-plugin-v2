﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Common
{
	public enum HelixDirection
	{
		Clockwise,
		Counterclockwise
	}

	public class CADHelixHelper
	{

		public static ObjectId DrawHelix(Document doc, Point3d start, Point3d end, double bulge)
		{
			using (TransactionWrapper tw = doc.StartTransaction())
			{
				var helix = DrawHelix(tw, start, end, bulge);
				ObjectId oid = helix.ObjectId;
				tw.Commit();
				return oid;
			}
		}
		/// <summary>
		/// Draws helix up to 1 revolution (mainly for pipe). It starts at start and ends at end points, and 
		/// bulge represents arc (same like in Polyline - which means it is 2d plus height).
		/// (start and end must not be in the same XY!)
		/// </summary>
		/// <param name="tw"></param>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="bulge"> Positive value is right helix (from start to end point).</param>
		public static Helix DrawHelix(TransactionWrapper tw, Point3d start, Point3d end, double bulge)
		{
			if (start.GetAs2d().IsEqualTo(end.GetAs2d(), new Tolerance(0, 0.0001)))
				throw new NotSupportedException("Helix can't have same start and end point.");
			double arcLength = 0;
			double arcRadius = 0;
			Point3d arcCenter;
			using (Polyline pl = new Polyline())
			{
				pl.AddPoints(
					new PolylineVertex<Point2d>(start.GetAs2d(), bulge),
					new PolylineVertex<Point2d>(end.GetAs2d(), bulge)
				);
				var arc = pl.GetArcSegmentAt(0);
				//arcLength = arc.GetLength(0, 1); // Couldn't get arc length to work (o.O).
				arcLength = pl.GetDistanceAtParameter(1);
				arcCenter = arc.Center;
				arcRadius = arc.Radius;
			}

			HelixDirection direction = bulge > 0 ? HelixDirection.Counterclockwise : HelixDirection.Clockwise;
			return DrawHelix(tw, start, end, arcCenter, arcLength, arcRadius, direction);

		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="tw"></param>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="arcCenter"></param>
		/// <param name="arcLength"></param>
		/// <param name="arcRadius"></param>
		/// <param name="clockwiseDirection">true - clockwise, false - counterclockwise</param>
		/// <returns></returns>
		public static Helix DrawHelix(TransactionWrapper tw, Point3d start, Point3d end, Point3d arcCenter, double arcLength, double arcRadius, HelixDirection direction)
		{
			double turns = arcLength / (arcRadius * 2 * Math.PI);
			return DrawHelixInTurns(tw, start, end, arcCenter, turns, arcRadius, direction);

			//// Helix is drawn relative to (0,0,0). Whole arc is moved to center, and returned back after calculation.
			//Vector3d offset = arcCenter.GetAsVector();
			//Helix h = tw.CreateEntity<Helix>();
			//h.StartPoint = start.Add(offset.Negate());//.Add(start.GetVectorTo(end).MultiplyBy(0.5));
			//										  //h.EndPoint = end;
			//h.Constrain = ConstrainType.Turns;
			//h.BaseRadius = arcRadius;
			//h.TopRadius = arcRadius;
			//h.Height = end.Z - start.Z;
			//h.Turns = arcLength / (arcRadius * 2 * Math.PI);
			//if (h.Turns == 0)
			//	h.Turns = 1;
			//h.Twist = direction == HelixDirection.Clockwise;
			////h.Twist = bulge < 0;
			//h.CreateHelix();
			//
			//DBPoint p1 = tw.CreateEntity<DBPoint>();
			//p1.Position = start;
			//p1 = tw.CreateEntity<DBPoint>();
			//p1.Position = end;
			////h.StartPoint = new Point3d(1, 0, 0);
			//
			//// Start of helix is in center, move start point to center.
			//h.TransformBy(Matrix3d.Displacement(offset.Add(new Vector3d(0, 0, start.Z))));
			//
			//return h;
		}
		/// <summary>
		/// Draw Helix by specifying turn count.
		/// </summary>
		/// <param name="tw"></param>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="arcCenter"></param>
		/// <param name="turns">Percentage of turn (0-100% = 0-360 deg)</param>
		/// <param name="arcRadius"></param>
		/// <param name="clockwiseDirection">true - clockwise, false - counterclockwise</param>
		/// <returns></returns>
		public static Helix DrawHelixInTurns(TransactionWrapper tw, Point3d start, Point3d end, Point3d arcCenter, double turns, double arcRadius, HelixDirection direction)
		{
			// Helix is drawn relative to (0,0,0). Whole arc is moved to center, and returned back after calculation.
			Vector3d offset = arcCenter.GetAsVector();
			Helix h = tw.CreateEntity<Helix>();
			h.StartPoint = start.Add(offset.Negate());//.Add(start.GetVectorTo(end).MultiplyBy(0.5));
													  //h.EndPoint = end;
			h.Constrain = ConstrainType.Turns;
			h.BaseRadius = arcRadius;
			h.TopRadius = arcRadius;
			h.Height = end.Z - start.Z;
			h.Turns = turns;// arcLength / (arcRadius * 2 * Math.PI);
			if (h.Turns == 0)
				h.Turns = 1;
			//NOTE If helix is going down, this has to be inverted.
			bool twist = direction == HelixDirection.Counterclockwise;
			if (start.Z > end.Z)
				twist = !twist;
			h.Twist = twist;
			//h.Twist = bulge < 0;
			h.CreateHelix();

			//DBPoint p1 = tw.CreateEntity<DBPoint>();
			//p1.Position = start;
			//p1 = tw.CreateEntity<DBPoint>();
			//p1.Position = end;

			// Start of helix is in center, move start point to center.
			h.TransformBy(Matrix3d.Displacement(offset.Add(new Vector3d(0, 0, start.Z))));

			return h;
		}
	}
}
