﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common
{
    public class CADDBDictionaryHelper
    {
        /// <summary>
        /// Returns dictionary which exists on a given path. null is returned if not found.
        /// </summary>
        public static DBDictionary GetSubDictionary(Transaction tr, ObjectId currentDictionaryId, params string[] subNames)
        {
            DBDictionary currentDic = tr.GetObject(currentDictionaryId, OpenMode.ForRead) as DBDictionary;
			return currentDic.OpenSubDictionary(tr, subNames);
        }

	}
}
