﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common
{
	//NOTE idea is to make things easier to work with Polylines.
	// There should be 3 derrived classes, for each polyline type.
	// Check what happens with polyline2d vertex, when not in database.
	// Polyline doesn't have to be added to database, so TransactionData is needed only when doing so.
	// Maybe we don't need classes, maybe one class with statis methods could be enough.


	/// <summary>
	/// Used to abstract access to different polyline objects.
	/// </summary>
	public abstract class PolylineWrapper
	{
		public abstract void AddPoints(IEnumerable<Point2d> pts);
		public abstract void AddPoints(IEnumerable<Point3d> pts);
		public abstract Point2d Get2dPoint();
		public abstract Point3d Get3dPoint();
	}
}
