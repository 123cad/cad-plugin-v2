﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if AUTOCAD
using Autodesk.AutoCAD.Geometry;
namespace Autodesk.AutoCAD.DatabaseServices
#endif

#if BRICSCAD
using Teigha.Geometry;
namespace Teigha.DatabaseServices
#endif
{
	public static class CADMTextHelper
	{
        /// <summary>
        /// MText newline character is 2 characters (uppercase): \P .
        /// </summary>
        public static readonly string NewlineCharacter = MText.LineBreak.ToUpper();

		/// <summary>
		/// If text is represented as a rectangle, this enum defines
		/// point which will be used for alignment.
		/// </summary>
		public enum TextAlignment
		{
			/// <summary>
			/// Default in acad/bcad
			/// </summary>
			TopLeft, 
			TopRight,
			BottomRight,
			BottomLeft, 
			TopMiddle,
			RightMiddle,
			BottomMiddle,
			LeftMiddle
		}
		/// <summary>
		/// How text is rotated when it has to be aligned with X axis.
		/// </summary>
		public enum HorizontRotation
		{
			/// <summary>
			/// Rotate around center point of text rectangle
			/// (text remains in exact position, but can be read without rotating head).
			/// </summary>
			TextCenterPoint,
			/// <summary>
			/// Rotate around point on the line, which reflects center point of the text
			/// (text switches side of line).
			/// </summary>
			TextMiddlePointOnLine
		}

		/// <summary>
		/// Parameters used for rotating MText to align with vector.
		/// </summary>
		public class MTextParameters
		{
			/// <summary>
			/// If text is represented as Rectangle, determines which point of rectangle is 
			/// centered.
			/// </summary>
			public TextAlignment TextAlignment { get; set; }
			/// <summary>
			/// Distance from TextAlignment point to the vector.
			/// Note: for Left/Right Middle alignment this can't be applied (0 is used).
			/// </summary>
			public double TextDelta { get; set; }
			/// <summary>
			/// Keep text [-90,90] deg relative to X axis.
			/// </summary>
			public bool RotateToKeepHorizont { get; set; }
			/// <summary>
			/// If RotateToKeepHorizont is true, determines how rotation is done.
			/// </summary>
			public HorizontRotation RotationType { get; set; }

			/// <summary>
			/// Default parameters (aligned with top left, no rotation).
			/// </summary>
			public MTextParameters()
			{
				// Default in acad/bcad.
				TextAlignment = TextAlignment.TopLeft;
				TextDelta = 0;
				RotateToKeepHorizont = false;
				RotationType = HorizontRotation.TextCenterPoint;
			}
		}

		/// <summary>
		/// Aligning multiple texts into single line  (behavior is same as there is big single text).
		/// </summary>
		/// <param name="texts"></param>
		/// <param name="position"></param>
		/// <param name="direction"></param>
		/// <param name="parameters"></param>
		public static void AlignWithVector(this IEnumerable<MText> texts, MyUtilities.Geometry.Point2d position, MyUtilities.Geometry.Vector2d direction, MTextParameters parameters)
		{
			// DeltaX is used for aligning (adjusting single text one by one, but with center as there is one big text).
			// DeltaX must be from desired center to alignment point.
			// This function adjusts delta according to TextAlignment. desiredDelta is from left edge to center.
			Func<TextAlignment, double, double, double> adjustDeltatoAlignment = (alignment, width, desiredDelta) =>
			{
				double val = 0;
				switch (alignment)
				{
					case TextAlignment.TopLeft:
					case TextAlignment.BottomLeft:
					case TextAlignment.LeftMiddle:
						val = desiredDelta;
						break;
					case TextAlignment.TopRight:
					case TextAlignment.BottomRight:
					case TextAlignment.RightMiddle:
						val = desiredDelta + width;
						break;
					case TextAlignment.TopMiddle:
					case TextAlignment.BottomMiddle:
						val = desiredDelta + width / 2;
						break;
				}
				return val;
			};
			Func<MText, double> widthWithPadding = t =>
			{
				double width = t.GeometricExtents.MaxPoint.X - t.GeometricExtents.MinPoint.X;
				double height = t.GeometricExtents.MaxPoint.Y - t.GeometricExtents.MinPoint.Y;
				return width + height * 0.5;
			};
			double totalWidth = texts.Sum(x =>
			{
				return widthWithPadding(x);
			});
			double deltaX = 0;
			switch (parameters.TextAlignment)
			{
				case TextAlignment.TopLeft:
				case TextAlignment.BottomLeft:
				case TextAlignment.LeftMiddle:
					deltaX = 0;
					break;
				case TextAlignment.TopRight:
				case TextAlignment.BottomRight:
				case TextAlignment.RightMiddle:
					deltaX = totalWidth;
					break;
				case TextAlignment.TopMiddle:
				case TextAlignment.BottomMiddle:
					deltaX = totalWidth / 2;
					break;
			}
			deltaX = -deltaX;
			foreach (MText t in texts)
			{
				double w = widthWithPadding(t);
				double d = adjustDeltatoAlignment(parameters.TextAlignment, w, deltaX);
				AlignWithVectorInternal(t, position, direction, parameters, d);
				deltaX += w;
			}
		}

		/// <summary>
		/// Aligns text with provided vector (angle from the vector is used for rotation,
		/// point is new location of the text).
		/// </summary>
		public static void AlignWithVector(this MText text, MyUtilities.Geometry.Point2d position, MyUtilities.Geometry.Vector2d direction, MTextParameters parameters)
		{

			AlignWithVectorInternal(text, position, direction, parameters, 0);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="text"></param>
		/// <param name="position"></param>
		/// <param name="direction"></param>
		/// <param name="parameters"></param>
		/// <param name="deltaX">Offset distance along x axis from position (less than o is left, bigger than 0 is  right)</param>
		private static void AlignWithVectorInternal(MText text, MyUtilities.Geometry.Point2d position, MyUtilities.Geometry.Vector2d direction, MTextParameters parameters, double deltaX)
		{
			double angleDeg = new MyUtilities.Geometry.Vector2d(1, 0).GetAngleDeg(direction);
			double angleRad = MyUtilities.Helpers.Calculations.DegreesToRadians(angleDeg);
			bool rotate = false;
			if (parameters.RotateToKeepHorizont)
			{
				if (angleDeg > 90 || angleDeg < -90)
					rotate = true;
			}
			Extents3d extents = text.GeometricExtents;
			double textWidth = extents.MaxPoint.X - extents.MinPoint.X;
			double textHeight = extents.MaxPoint.Y - extents.MinPoint.Y;
			// Just in case.
			text.Rotation = 0;
			// Set text to (0,0) with Text alignment.
			MyUtilities.Geometry.Vector2d textAlignmentVector = GetTextAlignmentVector(textWidth, textHeight, parameters.TextDelta, parameters.TextAlignment);
			text.Location = textAlignmentVector.Negate().Add(new MyUtilities.Geometry.Vector2d(deltaX, 0)).GetAsPoint().GetAsPoint3d().ToCADPoint();

			Matrix3d transformation = Matrix3d.Displacement(new Vector3d());
			if (rotate)
			{
				if (parameters.RotationType == HorizontRotation.TextMiddlePointOnLine)
				{
					Point3d t = text.Location;
					text.Location = new Point3d(t.X, -t.Y + textHeight, t.Z);
				}
				// Now text is on position where it has to be, just has to be rotated around center point.
				Point3d center = new Point3d(
									text.Location.X + textWidth / 2 - deltaX,
									text.Location.Y - textHeight / 2,
									text.Location.Z);
				transformation = transformation.PreMultiplyBy(Matrix3d.Rotation(Math.PI, Vector3d.ZAxis, center));
			}

			transformation = transformation.PreMultiplyBy(Matrix3d.Rotation(angleRad, Vector3d.ZAxis, new Point3d()));
			//transformation = Matrix3d.Rotation(angleRad, Vector3d.ZAxis, new Point3d());
			transformation = transformation.PreMultiplyBy(Matrix3d.Displacement(position.GetAsPoint3d().GetAsVector().ToCADVector()));

			text.TransformBy(transformation);
		}
		/// <summary>
		/// Vector which points to the new text position (defined with TextAlignment), from text TopLeft as (0,0).
		/// delta is distance from alignment vector (for top alignment it moves up, for bottom alignment it moves down).
		/// </summary>
		private static MyUtilities.Geometry.Vector2d GetTextAlignmentVector(double width, double height,double delta, TextAlignment align)
		{
			// Text by default is positioned to TopLeft.
			// It is possible that alignment different from TopLeft is required.
			// This vector goes from TopLeft to desired alignment point.
			double width2 = width / 2;
			double heigh2 = height / 2;
			MyUtilities.Geometry.Vector2d vector = new MyUtilities.Geometry.Vector2d();
			// Y coordinate which represents delta from center to reference point.
			// Top alignment means text is below vector.
			// Bottom alignment means text is above vector.
			double deltaY = 0;
			
			switch (align)
			{
				case TextAlignment.TopLeft:
					// Already there.
					//vector = new Geometry.Vector2d();
					deltaY = delta;
					break;
				case TextAlignment.TopRight:
					vector = new MyUtilities.Geometry.Vector2d(width, 0);
					deltaY = delta;
					break;
				case TextAlignment.BottomRight:
					vector = new MyUtilities.Geometry.Vector2d(width, -height);
					deltaY = -delta;
					break;
				case TextAlignment.BottomLeft:
					vector = new MyUtilities.Geometry.Vector2d(0, -height);
					deltaY = -delta;
					break;
				case TextAlignment.TopMiddle:
					vector = new MyUtilities.Geometry.Vector2d(width2, 0);
					deltaY = delta;
					break;
				case TextAlignment.RightMiddle:
					vector = new MyUtilities.Geometry.Vector2d(width, -heigh2);
					break;
				case TextAlignment.BottomMiddle:
					vector = new MyUtilities.Geometry.Vector2d(width2, -height);
					deltaY = -delta;
					break;
				case TextAlignment.LeftMiddle:
					vector = new MyUtilities.Geometry.Vector2d(0, -heigh2);
					break;
			}
			vector = vector.Add(new MyUtilities.Geometry.Vector2d(0, deltaY));
			return vector;
		}
		
        /// <summary>
        /// Returns string or null if wrong index.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="lineIndex"></param>
        /// <returns></returns>
        public static string GetTextInLine(this MText t, int lineIndex)
        {
            string res = null;
            string s = t.Contents.Replace(NewlineCharacter, "\n");
            string[] rows = t.Contents.Split('\n');
            if (rows.Length < lineIndex)
                res = rows[lineIndex];
            return res;
        }
	}
}
