﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Common
{

	public class LayerVisibilityData
    {
		public string Name { get; set; }
		/// <summary>
		/// LightBulb icon - All layers can be off.
		/// </summary>
		public bool IsOff { get; set; }
		/// <summary>
		/// Snowflake icon - All layers except current.
		/// </summary>
		public bool IsFrozen { get; set; }
		public bool IsVisible => !(IsOff || IsFrozen);


		/// <summary>
		/// Represents single layer with visibile-affecting values (hiden and frozen).
		/// <para>Hidden layer is not visible in layer table! IsOff should be used, not IsHidden!</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="isOff">Off(Hidden) layer stays in memory, is part of ZOOM EXTENTS.</param>
		/// <param name="isFrozen">Frozen layer is removed from memory. Not part of ZOOM EXTENTS.</param>
		public LayerVisibilityData(string name, bool isOff, bool isFrozen)
        {
            Name = name;
            IsOff = isOff;
            IsFrozen = isFrozen;
        }
    }
	public static class CADLayerHelper
	{
		private static bool isDefaultLayer(string layerName)
        {
			return layerName == "0";
        }
		/// <summary>
		/// Default layer can't be off, so it needs to be handled separately.
		/// </summary>
		/// <param name="lt"></param>
		/// <param name="isOff">null means don't change it</param>
		/// <param name="isFrozen">null means don't change</param>
		/// <returns></returns>
		private static void setLayerVisibility(LayerTableRecord lt, bool? isOff = null, bool? isFrozen = null)
        {
			if (lt.Name == "0")
				// Setting layer 0 to be invisible is done only on IsFrozen.
				lt.IsFrozen = (isOff??false) || (isFrozen??lt.IsFrozen);
			else
            {
				if (isOff.HasValue)
					lt.IsOff = isOff.Value;
				if (isFrozen.HasValue)
					lt.IsFrozen = isFrozen.Value;
            }				
        }
		/// <summary>
		/// Tries to set layer current. Returns true if succeeded.
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="layerName">If null, it sets default layer current.</param>
		/// <returns></returns>
		public static bool SetCurrentLayer(Document doc, string layerName = null)
        {
			bool result = false;
            using (TransactionWrapper tw = doc.StartTransaction())
            {
				if (layerName == null)
				{
					doc.Database.Clayer = doc.Database.LayerZero;
					result = true;
				}
				else if (tw.LayerTable.Has(layerName))
                {
					doc.Database.Clayer = tw.LayerTable[layerName];
					result = true;
                }

                // Do commit even for readonly operations.
                tw.Commit();
            }
			return result;
        }
		/// <summary>
		/// Returns name of the current layer.
		/// </summary>
		/// <param name="doc"></param>
		/// <returns></returns>
		public static string GetCurrentLayer(Document doc)
        {
			string layer = null;

            using (TransactionWrapper tw = doc.StartTransaction())
            {
				var ltr = tw.GetObjectRead<LayerTableRecord>(doc.Database.Clayer);
				layer = ltr.Name;

                // Do commit even for readonly operations.
                tw.Commit();
            }
			return layer;
        }

		/// <summary>
		/// Returns all layers which exist in the drawing.
		/// </summary>
		/// <param name="doc"></param>
		/// <returns></returns>
		public static List<string> GetLayers(Document doc)
		{ // Get the current document and database, and start a transaction 
			List<string> layers = new List<string>();
			Document acDoc = doc;
			Database acCurDb = acDoc.Database;
			using (Transaction acTrans = acCurDb.TransactionManager.StartTransaction())
			{ // This example returns the layer table for the current database 
				LayerTable acLyrTbl;
				acLyrTbl = acTrans.GetObject(acCurDb.LayerTableId,
					OpenMode.ForRead) as LayerTable;
				// Step through the Layer table and print each layer name 
				foreach (ObjectId acObjId in acLyrTbl)
				{
					LayerTableRecord acLyrTblRec;
					acLyrTblRec = acTrans.GetObject(acObjId,
						OpenMode.ForRead) as LayerTableRecord;
					//acDoc.Editor.WriteMessage("\n" + acLyrTblRec.Name);
					layers.Add(acLyrTblRec.Name);
				}  // Dispose of the transaction
			}
			return layers;
		}
		public static bool DoesLayerExist(Database db, string layer)
		{
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				LayerTable lt = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForWrite);

				return lt.Has(layer);
			}
		}
		/// <summary>
		/// Checks if layer exists. If not, it is added
		/// </summary>
		public static ObjectId AddLayerIfDoesntExist(Database db, string layer)
		{
			ObjectId layerId = ObjectId.Null;
			using (var ld = Application.DocumentManager.MdiActiveDocument.LockDocument())
			{
				using (Transaction tr = db.TransactionManager.StartTransaction())
				{
					LayerTable lt = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForWrite);

					if (!lt.Has(layer))
					{
						LayerTableRecord rec = new LayerTableRecord();
						rec.Name = layer;
						lt.Add(rec);
						tr.AddNewlyCreatedDBObject(rec, true);
						layerId = rec.ObjectId;
						rec.Dispose();
					}
					else
					{
						bool found = false;
						foreach (ObjectId id in lt)
						{
							LayerTableRecord ltr = tr.GetObject(id, OpenMode.ForRead) as LayerTableRecord;
							if (ltr.Name == layer)
							{
								layerId = id;
								found = true;
								break;
							}
						}
						if (!found)
							throw new NullReferenceException();
					}
					tr.Commit();
				}
			}
			return layerId;
		}
		/// <summary>
		/// Sets linetype for specified layer.
		/// </summary>
		/// <param name="db"></param>
		/// <param name="layer"></param>
		/// <param name="linetypeId"></param>
		public static void SetLayerLineType(Database db, string layer, ObjectId linetypeId)
		{
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				LayerTable lt = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForWrite);

				if (lt.Has(layer))
				{
					foreach (ObjectId id in lt)
					{
						LayerTableRecord ltr = tr.GetObject(id, OpenMode.ForWrite) as LayerTableRecord;
						if (ltr.Name == layer)
						{
							ltr.LinetypeObjectId = linetypeId;
							tr.Commit();
							break;
						}
					}
				}
			}
		}
		/// <summary>
		/// Sets frozen state for all provided layers (if layer not found, it is ignored.
		/// <para>Frozen layer does not generate entities on the drawing, nor takes part in ZOOM EXTENTS.</para>
		/// <para>It is released from the memory (helps when performance is issue).</para>
		/// </summary>
		public static void SetLayersFrozenState(Document doc, bool isFrozen, params string[] layers)
		{
            using (TransactionWrapper tw = doc.StartTransaction())
            {
				var lt = tw.LayerTable;
				foreach (var layer in layers)
					if (lt.Has(layer))
					{
						LayerTableRecord ltr = tw.GetObjectWrite<LayerTableRecord>(lt[layer]);
						setLayerVisibility(ltr, null, isFrozen);
					}

				// Do commit even for readonly operations.
				tw.Commit();
            }
		}
		/// <summary>
		/// Returns all layers in the drawing which are in provided frozen state.
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="frozenState"></param>
		/// <returns></returns>
		public static IEnumerable<string> GetLayersWithFrozenState(Document doc, bool frozenState = true)
        {
			List<string> layers = new List<string>();
			using (TransactionWrapper tw = doc.StartTransaction())
			{
				LayerTable lt = tw.LayerTable;
				foreach(var oid in lt)
                {
					LayerTableRecord ltr = tw.GetObjectRead<LayerTableRecord>(oid);
					if (ltr.IsFrozen == frozenState)
						layers.Add(ltr.Name);
                }
				tw.Commit();
			}
			return layers;
		}
		/// <summary>
		/// Off layer still participates in ZOOM extents. Objects are there, but are not drawn.
		/// <para>When performance is issue, consider freezing layer(s).</para>
		/// </summary>
		public static void SetLayersOffState(Document doc, bool isOff, params string[] layers)
		{
            using (TransactionWrapper tw = doc.StartTransaction())
			{
				var lt = tw.LayerTable;
				foreach (var layer in layers)
					if (lt.Has(layer))
					{
						LayerTableRecord ltr = tw.GetObjectWrite<LayerTableRecord>(lt[layer]);
						setLayerVisibility(ltr, isOff, null);
					}
				tw.Commit();
			}
		}
		/// <summary>
		/// Returns all layers in the drawing which are in provided off state.
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="offState"></param>
		/// <returns></returns>
		public static IEnumerable<string> GetLayersWithOffState(Document doc, bool offState = true)
		{
			List<string> layers = new List<string>();
			using (TransactionWrapper tw = doc.StartTransaction())
			{
				LayerTable lt = tw.LayerTable;
				foreach (var oid in lt)
				{
					LayerTableRecord ltr = tw.GetObjectRead<LayerTableRecord>(oid);
					if (ltr.IsOff == offState)
						layers.Add(ltr.Name);
				}
				tw.Commit();
			}
			return layers;
		}
		/// <summary>
		/// Sets visibility data for provided layers.
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="data"></param>
		public static void SetLayersData(Document doc, IEnumerable<LayerVisibilityData> data)
        {

            using (TransactionWrapper tw = doc.StartTransaction())
            {
				var lt = tw.LayerTable;

				foreach(var d in data)
                {
					if (lt.Has(d.Name))
                    {
						var ltr = tw.GetObjectWrite<LayerTableRecord>(lt[d.Name]);
						setLayerVisibility(ltr, d.IsOff, d.IsFrozen);
                    }
                }

                // Do commit even for readonly operations.
                tw.Commit();
            }
        }

		/// <summary>
		/// Returns all layers with Visibility values.
		/// </summary>
		/// <param name="doc"></param>
		/// <returns></returns>
		public static IEnumerable<LayerVisibilityData> GetAllLayersData(Document doc)
        {
			var list = new List<LayerVisibilityData>();
            using (TransactionWrapper tw = doc.StartTransaction())
            {
				LayerTable lt = tw.LayerTable;
				foreach (var oid in lt)
				{
					LayerTableRecord ltr = tw.GetObjectRead<LayerTableRecord>(oid);
					var d = new LayerVisibilityData(ltr.Name, ltr.IsOff, ltr.IsFrozen);
					list.Add(d);
				}

				// Do commit even for readonly operations.
				tw.Commit();
            }
			return list;
        }

		public static void DeleteLayers(Database db, params string[] layers)
		{
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				LayerTable lt = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForWrite);
				foreach (string layer in layers)
				{
					// Default layer can't be deleted.
					if (isDefaultLayer(layer))
						continue;
					if (lt.Has(layer))
					{
						ObjectId oid = lt[layer];
						LayerTableRecord ltr = tr.GetObject(oid, OpenMode.ForWrite) as LayerTableRecord;
						ltr.Erase();
					}
				}
				tr.Commit();
			}
		}
	}
}
