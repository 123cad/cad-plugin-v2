﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin
{
    interface IProgressBarStep
    {
        void PerformIteration();
    }
    /// <summary>
    /// Provide total number of iterations, ProgressBar will determine optimum number of steps.
    /// </summary>
    public class ProgressBar:IProgressBarStep, IDisposable
    {
        /// <summary>
        /// Total number of steps.
        /// </summary>
        public int TotalSteps { get; private set; }
        /// <summary>
        /// Steps performed since start.
        /// </summary>
        public int CurrentStepCount { get; private set; }
        private ProgressMeter meter;
        /// <summary>
        /// Size of step (number of iterations until step is increased).
        /// </summary>
        private int stepResolution;
        /// <summary>
        /// Increased until stepResolution, then set to 0.
        /// </summary>
        private int currentIterationInnerIndex;

        public ProgressBar()
        {
            meter = new ProgressMeter();
        }
        public void Start(uint totalIterations, string message = "")
        {
            stepResolution = (int)(totalIterations > 250 ? totalIterations / 200.0 : totalIterations + 1);
            TotalSteps = (int)(totalIterations / stepResolution) + 1;
            CurrentStepCount = 0;
            currentIterationInnerIndex = 0;

            meter.SetLimit(TotalSteps);
            meter.Start(message);
            CurrentStepCount = 0;
        }
        public void PerformIteration()
        {
            currentIterationInnerIndex++;
            if (currentIterationInnerIndex < stepResolution)
                return;

            meter.MeterProgress();
            currentIterationInnerIndex = 0;
            CurrentStepCount++;
        }
        /// <summary>
        /// Set full bar (100%).
        /// </summary>
        public void SetToFull()
        {
           while(CurrentStepCount <= TotalSteps)
            {
                CurrentStepCount++;
                meter.MeterProgress();
            }
        }
        public void Stop()
        {
            meter.Stop();
        }

        public void Dispose()
        {
            if (!meter.IsDisposed)
            {
                meter.Stop();
                meter.Dispose();
            }
        }
    }
}
