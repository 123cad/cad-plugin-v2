﻿using sys = System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common.CAD
{
    public class EraseOverrule : ObjectOverrule
    {
        public override void Erase(DBObject dbObject, bool erasing)
        {
            throw new Exception(ErrorStatus.NotApplicable);
        }
    }

    /// <summary>
    /// Prevents user to do apply any change to any entity.
    /// </summary>
    public class DocumentUserLock
    {
        static EraseOverrule eraseRule = null;
        private static Overrule objectOverrule = new EraseObjectOverrule();
        private static Overrule gripOverrule = new EraseGripOverrule();
        private static Overrule transformOverrule = new EraseTransformOverrule();
        private static bool previousOverruling = false;
        private static CommandEventHandler preventUndo = null;
        /// <summary>
        /// Who locked the document - name will be displayed in warning.
        /// </summary>
        private static string Owner = null;
        private static HashSet<string> skipCommands;
        /// <summary>
        /// Prevents user to change drawing (by disabling any entity selection).
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="dontShowForCommands">For specific commands doesn't show warning window.
        /// <para>Global versions, with "_" prefix will be added automatically.</para></param>
        public static void Lock(Document doc, string owner = null, IEnumerable<string> dontShowForCommands = null)
        {
            // TEST OVERRULLING!!!
            Owner = owner;
            if (preventUndo != null)
                throw new sys.InvalidOperationException("Document is already locked!");
            var l = (dontShowForCommands ?? new string[] { })
                .Distinct()
                .Select(x=>x.ToUpper())
                .SelectMany(x => new string[] { x, "_" + x });
            skipCommands = new HashSet<string>(l);
                
            doc.Editor.SelectionAdded += Editor_SelectionAdded;
            eraseRule = new EraseOverrule();
            doc.CommandWillStart += Doc_CommandWillStart;
            //Overrule.AddOverrule(RXObject.GetClass(typeof(Entity)),eraseRule, true);
            //Overrule.Overruling = true;
            //preventUndo = (_, e)=>
            //{
            //    if (e.GlobalCommandName == "U" || e.GlobalCommandName == "UNDO" || e.GlobalCommandName == "_UNDO")
            //    {
            //        doc.SendStringToExecute("CANCELCMD\n", true, false, true);
            //        //System.Windows.MessageBox.Show("Please do not undo while KSOPT is opened!");
            //        //doc.SendStringToExecute("_REDO\n", true, true, false);
            //    }
            //};
            //doc.CommandWillStart += preventUndo;
            return;
            previousOverruling = Overrule.Overruling;
            Overrule.AddOverrule(RXObject.GetClass(typeof(Entity)), objectOverrule, true);
            Overrule.AddOverrule(RXObject.GetClass(typeof(Entity)), gripOverrule, true);
            Overrule.AddOverrule(RXObject.GetClass(typeof(Entity)), transformOverrule, true);
            Overrule.Overruling = true;
        }

        private static void Doc_CommandWillStart(object sender, CommandEventArgs e)
        {
            if (skipCommands.Contains(e.GlobalCommandName.ToUpper()))
                return;
            sys.Windows.Forms.MessageBox.Show($"Starting a command while {(Owner != null?$"{Owner} is opened":"document is locked ")} has unpredictable behavior!", "Warning", sys.Windows.Forms.MessageBoxButtons.OK, sys.Windows.Forms.MessageBoxIcon.Warning);
            //Application.DocumentManager.MdiActiveDocument.SendStringToExecute("CANCELCMD ", true, true, true);
        }

        private static void DocumentManager_DocumentLockModeChanged(object sender, DocumentLockModeChangedEventArgs e)
        {
            e.Veto();
        }

        public static void TemporaryAllowStarted()
        {
            if (eraseRule == null)
                return;
            //Overrule.RemoveOverrule(RXObject.GetClass(typeof(Entity)), eraseRule);
            eraseRule = null;
        }
        public static void TemporaryAllowFinished()
        {
            eraseRule = new EraseOverrule();
            //Overrule.AddOverrule(RXObject.GetClass(typeof(Entity)), eraseRule, true);
        }


        private static void Editor_SelectionAdded(object sender, SelectionAddedEventArgs e)
        {
            for (int i = 0; i < e.AddedObjects.Count; i++)
            {
                e.Remove(0);
            }
        }

        public static void Unlock(Document doc)
        {
            doc.CommandWillStart -= Doc_CommandWillStart;
            doc.Editor.SelectionAdded -= Editor_SelectionAdded;
            //Overrule.RemoveOverrule(RXObject.GetClass(typeof(Entity)),eraseRule);
            eraseRule = null;
            preventUndo = null;
            return;
            Overrule.RemoveOverrule(RXObject.GetClass(typeof(Entity)), objectOverrule);
            Overrule.RemoveOverrule(RXObject.GetClass(typeof(Entity)), gripOverrule);
            Overrule.RemoveOverrule(RXObject.GetClass(typeof(Entity)), transformOverrule);
            Overrule.Overruling = previousOverruling;
        }
    }
    class EraseObjectOverrule:ObjectOverrule
    {
        public override void Erase(DBObject dbObject, bool erasing)
        {
            //Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage("Unable to delete while KSOPT is running");
            //base.Erase(dbObject, erasing);
            //throw new Exception(ErrorStatus.NotApplicable);
            
        }
        
    }
    class EraseGripOverrule:GripOverrule
    {
        public override void GetGripPoints(Entity entity, GripDataCollection grips, double curViewUnitSize, int gripSize, Vector3d curViewDir, GetGripPointsFlags bitFlags)
        {
            throw new Exception(ErrorStatus.NotApplicable);

            //            base.GetGripPoints(entity, grips, curViewUnitSize, gripSize, curViewDir, bitFlags);
        }
        public override void MoveGripPointsAt(Entity entity, GripDataCollection grips, Vector3d offset, MoveGripPointsFlags bitFlags)
        {
            throw new Exception(ErrorStatus.NotApplicable);
        }
        public override void GetStretchPoints(Entity entity, Point3dCollection stretchPoints)
        {
            throw new Exception(ErrorStatus.NotApplicable);
        }
        public override void GetGripPoints(Entity entity, Point3dCollection gripPoints, IntegerCollection snapModes, IntegerCollection geometryIds)
        {
            throw new Exception(ErrorStatus.NotApplicable);
        }
        public override void MoveGripPointsAt(Entity entity, IntegerCollection indices, Vector3d offset)
        {
            throw new Exception(ErrorStatus.NotApplicable);
        }
        public override void MoveStretchPointsAt(Entity entity, IntegerCollection indices, Vector3d offset)
        {
            throw new Exception(ErrorStatus.NotApplicable);
        }
    }
    class EraseTransformOverrule:TransformOverrule
    {
        public override void Explode(Entity entity, DBObjectCollection entitySet)
        {
            throw new Exception(ErrorStatus.NotApplicable);
        }
        public override void TransformBy(Entity entity, Matrix3d transform)
        {
            throw new Exception(ErrorStatus.NotApplicable);
        }
    }
    
}
