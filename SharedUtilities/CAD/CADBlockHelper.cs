﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using MyExceptions;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common
{
	public static class CADBlockHelper
	{

		public static bool DoesBlockNameExist(TransactionWrapper tw, string name)
		{
			return tw.BlockTable.Has(name);
		}
		/// <summary>
		/// Returns names of all blocks loaded to the drawing.
		/// </summary>
		/// <param name="tw"></param>
		/// <returns></returns>
		public static IEnumerable<string> GetBlockNames(TransactionWrapper tw)
		{
			foreach(var b in tw.BlockTable)
			{
				BlockTableRecord rec = tw.Tr.GetObject(b, OpenMode.ForRead) as BlockTableRecord;
				if (rec == null)
					continue;
				yield return rec.Name;
			}
		}

		/// <summary>
		/// Creates new block with provided entities, and MText as attributes.
		/// </summary>
		/// <param name="tw"></param>
		/// <param name="name"></param>
		/// <param name="entities"></param>
		/// <returns></returns>
		public static ObjectId CreateNewBlockWithAttributes(TransactionWrapper tw, string name, Entity[] entities, MText[] attributes)
		{
			if (tw.BlockTable.Has(name))
				throw new DuplicateObjectException($"Block with name {name} already exists!");
			if (!tw.BlockTable.IsWriteEnabled)
				tw.BlockTable.UpgradeOpen();
			if (!tw.ModelSpace.IsWriteEnabled)
				tw.ModelSpace.UpgradeOpen();
			BlockTableRecord rec = new BlockTableRecord();
			rec.Name = name;
			ObjectId blockRecordId = tw.BlockTable.Add(rec);
			tw.Tr.AddNewlyCreatedDBObject(rec, true);
			foreach (var e in entities)
			{
				rec.AppendEntity(e);
				tw.Tr.AddNewlyCreatedDBObject(e, true);
			}
			foreach(var a in attributes)
			{
				AttributeDefinition ad = new AttributeDefinition();
				ad.Tag = a.Contents;
				ad.Position = a.Location;
				ad.TextString = a.Text;
				ad.Height = a.TextHeight;
#if AUTOCAD
				// There was some error.
				if (ad.Color != a.Color)
#endif
				ad.Color = a.Color;
				if (!string.IsNullOrEmpty(a.Layer))
					ad.Layer = a.Layer;
				rec.AppendEntity(ad);
				tw.Tr.AddNewlyCreatedDBObject(ad, true);
			}

			return blockRecordId;
		}
		/// <summary>
		/// Creates new block (and adds to the drawing) with provided entities (no attributes).
		/// </summary>
		/// <param name="tw"></param>
		/// <param name="name"></param>
		/// <param name="entities"></param>
		/// <returns></returns>
		public static ObjectId CreateNewBlock(TransactionWrapper tw, string name, params Entity[] entities)
		{
			if (tw.BlockTable.Has(name))
				throw new DuplicateObjectException($"Block with name {name} already exists!");
			if (!tw.BlockTable.IsWriteEnabled)
				tw.BlockTable.UpgradeOpen();
			if (!tw.ModelSpace.IsWriteEnabled)
				tw.ModelSpace.UpgradeOpen();
			BlockTableRecord rec = new BlockTableRecord();
			rec.Name = name;
			ObjectId blockRecordId = tw.BlockTable.Add(rec);
			tw.Tr.AddNewlyCreatedDBObject(rec, true);
			foreach(var e in entities)
			{
				rec.AppendEntity(e);
				tw.Tr.AddNewlyCreatedDBObject(e, true);

			}

			return blockRecordId;
		}
		public static void DeleteBlock(Document doc, string blockName)
		{
			using (TransactionWrapper tw = doc.StartTransaction())
			{
				DeleteBlock(tw, blockName);
				tw.Commit();
			}

		}
		/// <summary>
		/// Deletes block with all its references.
		/// </summary>
		/// <param name="tw"></param>
		/// <param name="blockName"></param>
		public static void DeleteBlock(TransactionWrapper tw, string blockName)
		{
			//TODO Still needs a little bit testing.
			if (!tw.BlockTable.Has(blockName))
				throw new KeyNotFoundException($"Unable to delete Block. Name ({blockName}) not found!");
			if (!tw.BlockTable.IsWriteEnabled)
				tw.BlockTable.UpgradeOpen();
			if (!tw.ModelSpace.IsWriteEnabled)
				tw.ModelSpace.UpgradeOpen();

			BlockTableRecord btr = tw.GetObjectWrite<BlockTableRecord>(tw.BlockTable[blockName]);
			foreach(ObjectId br in btr.GetBlockReferenceIds(true, true))
			{
				DBObject o = tw.GetObjectWrite<DBObject>(br);
				o.Erase();
			}
			foreach(ObjectId id in btr)
			{
				DBObject o = tw.GetObjectWrite<DBObject>(id);
				o.Erase();
			}
			try
			{
				tw.BlockTable.RemoveField(blockName);
			}
			catch
			{
				// Autocad 2015 crashed here (no key error).
			}

			btr.Erase();
		}
		/// <summary>
		/// Loads attributes from the block (if it is not found, it is loaded from file).
		/// </summary>
		public static void LoadBlockAttributes(BlockTable blockTable, ref BlockTableRecord blockRecord, string blockName, string fullBlockPath, Database db, List<ObjectId> attributesId, Transaction tr)
		{
			if (!blockTable.Has(blockName))
			{
				Database blockDB = new Database(false, true);
				blockDB.ReadDwgFile(fullBlockPath, FileOpenMode.OpenForReadAndAllShare, false, "");
				ObjectId btrId = db.Insert(blockName, blockDB, false);
			}
			blockRecord = tr.GetObject(blockTable[blockName], OpenMode.ForWrite) as BlockTableRecord;
			foreach (ObjectId id in blockRecord)
			{
				DBObject dbo = tr.GetObject(id, OpenMode.ForRead);
				if ((dbo as AttributeDefinition) != null)
					attributesId.Add(id);
			}
		}
		

		/// <summary>
		/// Tries to load Block (by provided file name) from default Blocks folder.
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="blockName"></param>
		public static void LoadBlockFromBlocksFolder(Document doc, string blockName)
        {
            string blocksPath = @"C:\Program Files\Common Files\123CAD Blöcke\";
			LoadBlockFromOutside(doc.Database, blocksPath, blockName);
        }

		/// <summary>
		/// Loads block from destination path if it is not already loaded.
		/// </summary>
		public static void LoadBlockFromOutside(Database db, string dirPath, string blockName)
		{
			using (Transaction tr = db.TransactionManager.StartTransaction())
			{
				BlockTable bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForWrite);
				if (!bt.Has(blockName))
				{
					using (Database blockdb = new Database(false, true))
					{
						string fileName = dirPath + blockName + ".dwg";
						if (!File.Exists(fileName))
						{
							MyLog.MyTraceSource.Error("Block: " + blockName + " is not found on: <" + dirPath + ">");
							throw new FileNotFoundException(fileName);
						}
						blockdb.ReadDwgFile(fileName, FileOpenMode.OpenForReadAndAllShare, false, "");
						ObjectId btrId = db.Insert(blockName, blockdb, false);
					}
				}
				tr.Commit();
			}
		}
		/// <summary>
		/// Creates BlockReference for provided blockName.
		/// </summary>
		/// <param name="tw"></param>
		/// <param name="blockName"></param>
		/// <param name="insertionPoint"></param>
		/// <param name="attNameValue">Values for attributes.</param>
		/// <param name="attNameColor">Dictionary of colors for attributes.</param>
		/// <returns></returns>
		public static BlockReference CreateBlockReference(TransactionWrapper tw, string blockName,
			Point2d insertionPoint, Dictionary<string, string> attNameValue = null, Dictionary<string, Color> attNameColor = null)
        {
			return CreateBlockReference(tw, tw.BlockTable, tw.ModelSpace, blockName, insertionPoint.GetAs3d(), attNameValue, attNameColor);
        }
		/// <summary>
		/// Creates BlockReference for existing blockName.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="bt"></param>
		/// <param name="modelSpace"></param>
		/// <param name="blockName"></param>
		/// <param name="insertionPoint"></param>
		/// <param name="attNameValue"></param>
		/// <param name="attNameColor">Defines colors for attributes (only for which it needs to be changed).</param>
		/// <param name=""></param>
		/// <returns></returns>
		public static BlockReference CreateBlockReference(Transaction tr, BlockTable bt, BlockTableRecord modelSpace, string blockName,
			Point3d insertionPoint, Dictionary<string, string> attNameValue, Dictionary<string, Color> attNameColor = null)
		{
			if (!bt.Has(blockName))
				return null;
			BlockTableRecord block = tr.GetObject(bt[blockName], OpenMode.ForRead) as BlockTableRecord;

			BlockReference br = new BlockReference(insertionPoint, block.Id);
			if (!modelSpace.IsWriteEnabled)
				modelSpace.UpgradeOpen();
			modelSpace.AppendEntity(br);
			tr.AddNewlyCreatedDBObject(br, true);

			if (block.HasAttributeDefinitions)
			{
				foreach (ObjectId id in block)
				{
					DBObject obj = tr.GetObject(id, OpenMode.ForRead);
					if (obj is AttributeDefinition)
					{
						AttributeDefinition attDef = (AttributeDefinition)obj;
						string attName = attDef.Tag;
						AttributeReference att = new AttributeReference();
						br.AttributeCollection.AppendAttribute(att);
						tr.AddNewlyCreatedDBObject(att, true);

						att.SetAttributeFromBlock(attDef, br.BlockTransform);

						if (attNameColor?.ContainsKey(attName) ?? false)
							att.Color = attNameColor[attName];

						att.Position = attDef.Position.TransformBy(br.BlockTransform);
						string s = "";
						if (attNameValue?.ContainsKey(attName)??false)
							s = attNameValue[attName];
						att.TextString = s;

					}
				}
			}

			return br;
		}

        /// <summary>
        /// Sets BlockReferenceScale to provided one.
        /// </summary>
        /// <param name="tr"></param>
        /// <param name="br"></param>
        /// <param name="scale">BlockReference scale will be set to this value.</param>
        public static void ScaleBlockReferenceTo(Transaction tr, BlockReference br, double scale, bool scaleAttributes = false)
        {
            double currentScale = br.ScaleFactors.X;
            double newScale = scale / currentScale;
            ScaleBlockReference(tr, br, newScale, scaleAttributes);
        }

		/// <summary>
		/// Scales Entities inside block 
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="br"></param>
		/// <param name="scaleBy">Scales block by this scale (current scale = 3, scaleBy = 2, result = 6).</param>
		/// <param name="scaleAttributes">If false, attributes are not moved.</param>
		public static void ScaleBlockReference(Transaction tr, BlockReference br, double scaleBy, bool scaleAttributes = false)
		{
			//CHECK should attributes remain with same distance from block symbol? (may be very hard to achieve)
			if (scaleAttributes)
			{
				Matrix3d m3 = Matrix3d.Scaling(scaleBy, br.Position);
				br.TransformBy(m3);
			}
			else
            {
                double oldScale = br.ScaleFactors.X;
				// Set text to "" so GeometricExtents measure only symbol size. (setting Visible = false doesn't work???)
				// Save current attributes.
				List<Tuple<AttributeReference, double, Vector3d, string>> temp = new List<Tuple<AttributeReference, double, Vector3d, string>>();
				// Save start attribute position and relative position (to start attribute) of all other attributes.
				// So when repositioned, just 
				Vector2d start = new Vector2d();
				Vector2d startRelative = new Vector2d();
				bool first = true;
				foreach (ObjectId id in br.AttributeCollection)
				{
					AttributeReference ar = tr.GetObject(id, OpenMode.ForWrite) as AttributeReference;
					Vector2d relativePosition = new Vector2d();
					if (first)
					{
						first = false;
						// Absolute position of first attribute.
						start = new Vector2d(ar.Position.X, ar.Position.Y);
						// Relative position of first attribute to block center.
						startRelative = start.Subtract(new Vector2d(br.Position.X, br.Position.Y));
						// Relative position of first attribute to itself.
						relativePosition = new Vector2d(0, 0);
					}
					else
						relativePosition = new Vector2d(ar.Position.X, ar.Position.Y).Subtract(start);
					temp.Add(new Tuple<AttributeReference, double, Vector3d, string>(ar, ar.Height, new Vector3d(relativePosition.X, relativePosition.Y, ar.Position.Z), ar.TextString));
					ar.TextString = "";
				}
				// Current diameter
				//double diameter = Math.Abs(br.GeometricExtents.MaxPoint.X - br.GeometricExtents.MinPoint.X);

				//double newDiameter = ;
				Matrix3d scaleM = Matrix3d.Scaling(scaleBy, br.Position);
				br.TransformBy(scaleM);

				startRelative = startRelative.MultiplyBy(1);
				start = startRelative.Add(new Vector2d(br.Position.X, br.Position.Y));

				// First attribute is moved, all others are positioned relative to it.
				foreach (Tuple<AttributeReference, double, Vector3d, string> tar in temp)
				{
					tar.Item1.UpgradeOpen();
					tar.Item1.Height = tar.Item2;
					Vector3d v = tar.Item3;
					Vector2d vp = start.Add(new Vector2d(v.X, v.Y));
					tar.Item1.Position = new Point3d(vp.X, vp.Y, v.Z);
					tar.Item1.TextString = tar.Item4;
				}
			}
		}
		/// <summary>
		/// Scales BlockReference (without attributes) in x, y and z axis.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="br"></param>
		/// <param name="scaleX"></param>
		/// <param name="scaleY"></param>
		/// <param name="scaleZ"></param>
		/// NOTE This doesn't work correctly. Needs more testing. Method above is working.
		public static void ScaleBlockReference(Transaction tr, BlockReference br, double scaleX, double scaleY, double scaleZ = 1)
		{
			// Set text to "" so GeometricExtents measure only symbol size. (setting Visible = false doesn't work???)
			// Save current attributes.
			List<Tuple<AttributeReference, double, Vector3d, string>> temp = new List<Tuple<AttributeReference, double, Vector3d, string>>();
			// Save start attribute position and relative position (to start attribute) of all other attributes.
			// So when repositioned, just 
			Vector2d start = new Vector2d();
			Vector2d startRelative = new Vector2d();
			bool first = true;
			foreach (ObjectId id in br.AttributeCollection)
			{
				AttributeReference ar = tr.GetObject(id, OpenMode.ForWrite) as AttributeReference;
				Vector2d relativePosition = new Vector2d();
				if (first)
				{
					first = false;
					// Absolute position of first attribute.
					start = new Vector2d(ar.Position.X, ar.Position.Y);
					// Relative position of first attribute to block center.
					startRelative = start.Subtract(new Vector2d(br.Position.X, br.Position.Y));
					// Relative position of first attribute to itself.
					relativePosition = new Vector2d(0, 0);
				}
				else
					relativePosition = new Vector2d(ar.Position.X, ar.Position.Y).Subtract(start);
				temp.Add(new Tuple<AttributeReference, double, Vector3d, string>(ar, ar.Height, new Vector3d(relativePosition.X, relativePosition.Y, ar.Position.Z), ar.TextString));
				ar.TextString = "";
			}
			// Current diameter
			//double diameter = Math.Abs(br.GeometricExtents.MaxPoint.X - br.GeometricExtents.MinPoint.X);

			//double newDiameter = ;
			//Matrix3d scaleM = Matrix3d.Scaling(scale, br.Position);
			//br.TransformBy(scaleM);
			if (System.DoubleHelper.AreEqual(scaleX, 0, 0.00001))
			//if (DoubleHelper.AreEqual(scaleX, 0, 0.000001))
				scaleX = 1;
			if (System.DoubleHelper.AreEqual(scaleY, 0, 0.00001))
			//if (DoubleHelper.AreEqual(scaleY, 0, 0.000001))
				scaleY = 1;
			if (System.DoubleHelper.AreEqual(scaleZ, 0, 0.000001))
			//if (DoubleHelper.AreEqual(scaleZ, 0, 0.000001))
				scaleZ = 1;
			br.ScaleFactors = new Scale3d(scaleX, scaleY, scaleZ);

			startRelative = startRelative.MultiplyBy(startRelative.Length * scaleX);
			start = startRelative.Add(new Vector2d(br.Position.X, br.Position.Y));

			// First attribute is moved, all others are positioned relative to it.
			foreach (Tuple<AttributeReference, double, Vector3d, string> tar in temp)
			{
				tar.Item1.UpgradeOpen();
				tar.Item1.Height = tar.Item2;
				Vector3d v = tar.Item3;
				Vector2d vp = start.Add(new Vector2d(v.X, v.Y));
				tar.Item1.Position = new Point3d(vp.X, vp.Y, v.Z);
				tar.Item1.TextString = tar.Item4;

			}
		}
		/// <summary>
		/// OLD approach by manually calculating points in a Polyline object, and updating them.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="br"></param>
		/// <param name="scaleAttributes">If false, attributes are not moved.</param>
		[Obsolete("Use new method for scaling (which scales block directly, not manually calculating vertices)")]
		private static void ScaleBlockReferenceOLD(Transaction tr, BlockReference br, double scaleX, double scaleY)
		{
			// Set text to "" so GeometricExtents measure only symbol size. (setting Visible = false doesn't work???)
			// Save current attributes.
			List<Tuple<AttributeReference, double, Vector3d, string>> temp = new List<Tuple<AttributeReference, double, Vector3d, string>>();
			// Save start attribute position and relative position (to start attribute) of all other attributes.
			// So when repositioned, just 
			Vector2d start = new Vector2d();
			Vector2d startRelative = new Vector2d();
			bool first = true;
			foreach (ObjectId id in br.AttributeCollection)
			{
				AttributeReference ar = tr.GetObject(id, OpenMode.ForWrite) as AttributeReference;
				Vector2d relativePosition = new Vector2d();
				if (first)
				{
					first = false;
					// Absolute position of first attribute.
					start = new Vector2d(ar.Position.X, ar.Position.Y);
					// Relative position of first attribute to block center.
					startRelative = start.Subtract(new Vector2d(br.Position.X, br.Position.Y));
					// Relative position of first attribute to itself.
					relativePosition = new Vector2d(0, 0);
				}
				else
					relativePosition = new Vector2d(ar.Position.X, ar.Position.Y).Subtract(start);
				temp.Add(new Tuple<AttributeReference, double, Vector3d, string>(ar, ar.Height, new Vector3d(relativePosition.X, relativePosition.Y, ar.Position.Z), ar.TextString));
				ar.TextString = "";
			}
			// Current diameter
			IEnumerable<Entity> entities = GetBlockDefinitionEntities(tr, br);

			foreach (Entity e in entities)
			{
				Polyline pl = e as Polyline;
				{
					pl.UpgradeOpen();
					// Considered is rectangle, where center is preserved in the same location.
					// First initialize to lenght 1, and then scale (because of some problems).
					MyUtilities.Geometry.Point2d leftT = new MyUtilities.Geometry.Point2d(-0.5, 0.5);// pl.GetPoint2dAt(0).FromCADPoint();
					MyUtilities.Geometry.Point2d rightT = new MyUtilities.Geometry.Point2d(0.5, 0.5);//pl.GetPoint2dAt(1).FromCADPoint();
					MyUtilities.Geometry.Point2d rightB = new MyUtilities.Geometry.Point2d(0.5, -0.5);//pl.GetPoint2dAt(2).FromCADPoint();
					MyUtilities.Geometry.Point2d leftB = new MyUtilities.Geometry.Point2d(-0.5, -0.5);//pl.GetPoint2dAt(3).FromCADPoint();
					double totalX = rightT.X - leftT.X;
					double totalY = leftT.Y - leftB.Y;
					double newX = totalX * scaleX;
					double newY = totalY * scaleY;
					double tX = newX / 2;
					double tY = newY / 2;
					double blockCenterX = 0;// br.Position.X;
					double blockCenterY = 0;// br.Position.Y;
					leftT = new MyUtilities.Geometry.Point2d(blockCenterX - tX, blockCenterY + tY);
					rightT = new MyUtilities.Geometry.Point2d(blockCenterX + tX, blockCenterY + tY);
					rightB = new MyUtilities.Geometry.Point2d(blockCenterX + tX, blockCenterY - tY);
					leftB = new MyUtilities.Geometry.Point2d(blockCenterX - tX, blockCenterY - tY);
					pl.AddVertexAt(0, leftB.ToCADPoint(), 0, 0, 0);
					pl.AddVertexAt(0, rightB.ToCADPoint(), 0, 0, 0);
					pl.AddVertexAt(0, rightT.ToCADPoint(), 0, 0, 0);
					pl.AddVertexAt(0, leftT.ToCADPoint(), 0, 0, 0);
					//while (pl.NumberOfVertices > 4)
					//pl.RemoveVertexAt(4);
					// Without reseting, original polyline in block is displayed, despite 
					// polyline points have changed. This can be noticeable while jigging,
					// because then real polyline is displayed, or if block is exploded.
					// Maybe REGEN works also (i think it does), but i prefer not to
					// affect all entities in the drawing.
					pl.Reset(true, 4);
					pl.Closed = true;
					break;
				}
			}
			startRelative = new Vector2d(startRelative.X * scaleX, startRelative.Y * scaleY);
			start = startRelative.Add(new Vector2d(br.Position.X, br.Position.Y));

			// First attribute is moved, all others are positioned relative to it.
			first = true;
			foreach (Tuple<AttributeReference, double, Vector3d, string> tar in temp)
			{
				tar.Item1.UpgradeOpen();
				tar.Item1.Height = tar.Item2;
				Vector3d v = tar.Item3;
				Vector2d vp = start.Add(new Vector2d(v.X, v.Y));
				tar.Item1.Position = new Point3d(vp.X, vp.Y, v.Z);
				tar.Item1.TextString = tar.Item4;
			}
		}
		/// <summary>
		/// Returns Definition entities in BlockTableRecord.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="br"></param>
		/// <returns></returns>
		public static List<Entity> GetBlockDefinitionEntities(Transaction tr, BlockReference br)
		{
			List<Entity> list = new List<Entity>();

			BlockTableRecord rec = tr.GetObject(br.BlockTableRecord, OpenMode.ForRead) as BlockTableRecord;
			if (rec != null)
			{
				foreach (ObjectId id in rec)
				{
					Entity ent = tr.GetObject(id, OpenMode.ForRead) as Entity;
					list.Add(ent);
				}
			}

			return list;
		}
		/// <summary>
		/// Returns all BlockReferences for specified block name.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="blockTableId"></param>
		/// <returns></returns>
		public static IEnumerable<ObjectId> GetAllBlocksForName(TransactionWrapper data, string blockName)
		{
			if (!data.Bt.Has(blockName))
				yield break;

			BlockTableRecord btr = data.Tr.GetObject(data.Bt[blockName], OpenMode.ForRead) as BlockTableRecord;
			ObjectIdCollection blocks = btr.GetBlockReferenceIds(true, false);
			foreach (ObjectId id in blocks)
				yield return id;
		}
		/*/// <summary>
		/// Scales rectangle block, with changing aspect ratio.
		/// </summary>
		public static void ScaleRectangleBlockReference(Transaction tr, BlockReference br, double scaleX, double scaleY, bool scaleAttributes = false)
		{
			//CHECK should attributes remain with same distance from block symbol? (may be very hard to achieve)
			if (scaleAttributes)
			{
				Matrix3d m3 = Matrix3d.Scaling(scale, br.Position);
				br.TransformBy(m3);
			}
			else
			{
				// Set text to "" so GeometricExtents measure only symbol size. (setting Visible = false doesn't work???)
				// Save current attributes.
				List<Tuple<AttributeReference, double, Point3d, string>> temp = new List<Tuple<AttributeReference, double, Point3d, string>>();
				foreach (ObjectId id in br.AttributeCollection)
				{
					AttributeReference ar = tr.GetObject(id, OpenMode.ForWrite) as AttributeReference;
					temp.Add(new Tuple<AttributeReference, double, Point3d, string>(ar, ar.Height, ar.Position, ar.TextString));
					ar.TextString = "";
				}
				// Current diameter
				//double diameter = Math.Abs(br.GeometricExtents.MaxPoint.X - br.GeometricExtents.MinPoint.X);
				List<Entity> entities = GetBlockReferenceEntities(tr, br);
				foreach(Entity e in entities)
				{
					Polyline pl = e as Polyline;
					Polyline2d pl2 = e as Polyline2d;
					Polyline3d pl3 = e as Polyline3d;
					if (pl != null)
					{

					}
					if (pl2 != null)
					{

					}
					if (pl3 != null)
					{

					}
				}
				//double newDiameter = ;
				Matrix3d scaleM = Matrix3d.Scaling(scale, br.Position);
				br.TransformBy(scaleM);


				Point2d center = new Point2d(br.Position.X, br.Position.Y);
				// Restore attributes
				foreach (Tuple<AttributeReference, double, Point3d, string> tar in temp)
				{
					tar.Item1.UpgradeOpen();
					tar.Item1.Height = tar.Item2;
					// Scale distance of attribute to the center also (needs testing!).
					Point3d pt = tar.Item3;
					Point2d p2 = new Point2d(pt.X, pt.Y);
					Vector2d posVector = center.GetVectorTo(p2);
					posVector = posVector.MultiplyBy(scale);
					Point2d newPos = center.Add(posVector);
					tar.Item1.Position = new Point3d(newPos.X, newPos.Y, pt.Z);
					tar.Item1.TextString = tar.Item4;
				}
			}
		}*/
		/// <summary>
		/// Makes attributes of the BlockReference invisible. (uses Invisible property, which is not related to Visible property).
		/// </summary>
		/// <param name="br"></param>
		/// <param name="tr"></param>
		/// <param name="invisibleAttributes">If not null, only specified attributes are set invisible. If null all attributes are invisible.</param>
		public static void MakeAttributesInvisible(this BlockReference br, Transaction tr, HashSet<string> invisibleAttributes = null)
		{
			AttributeCollection atts = br.AttributeCollection;
			foreach (ObjectId oid in atts)
			{
				AttributeReference ar = tr.GetObject(oid, OpenMode.ForWrite) as AttributeReference;
				ar.Invisible = invisibleAttributes == null || invisibleAttributes.Contains(ar.Tag);
				ar.DowngradeOpen();
			}
		}

		/// <summary>
		/// Sets block attributes to specified size. If attribute is not in the dictionary, defaultSize is used.
		/// </summary>
		/// <param name="defaultSize"></param>
		/// <param name="attributes"></param>
		public static void SetAttributesTextSize(this BlockReference br, Transaction tr, double defaultSize, Dictionary<string, double> attributes = null)
		{
			double rotation = br.Rotation;
			br.Rotation = 0;
			AttributeCollection atts = br.AttributeCollection;
			List<AttributeReference> attRefs= new List<AttributeReference>();
			List<double> attPaddingTop = new List<double>();
			foreach (ObjectId oid in atts)
			{
				AttributeReference ar = tr.GetObject(oid, OpenMode.ForWrite) as AttributeReference;
				if (attRefs.Count >= 1)
				{
					double length = attRefs.Last().Position.GetVectorTo(ar.Position).Length;
					double padding = length - ar.Height;
					attPaddingTop.Add(padding);
				}
				attRefs.Add(ar);
			}
			string st = string.Concat(attRefs.Select(x => $"({x.Position.X}) \\r\\n").ToArray());
			for (int i = 0; i < attRefs.Count; i++)
			{
				var attRef = attRefs[i];
				double h = defaultSize;
				if (attributes != null && attributes.ContainsKey(attRef.Tag))
					h = attributes[attRef.Tag];
				if (i >= 1)
				{
					double previousY = attRefs[i - 1].Position.Y;
					double paddingTop = attPaddingTop[i - 1];
					Point3d p = attRef.Position;

					attRef.Position = new Point3d(p.X, previousY - paddingTop - h, p.Z);
				}
				attRef.Height = h;
				attRef.DowngradeOpen();
			}



			/*foreach (ObjectId oid in atts)
			{
				AttributeReference ar = tr.GetObject(oid, OpenMode.ForWrite) as AttributeReference;
				double h = defaultSize;
				if (attributes != null && attributes.ContainsKey(ar.Tag))
					h = attributes[ar.Tag];
				ar.Height = h;
				ar.DowngradeOpen();
			}*/


			br.Rotation = rotation;
		}

        /// <summary>
        /// Returns collection of all attributes inside block.
        /// </summary>
        /// <param name="br"></param>
        /// <param name="tr"></param>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<string, string>> ReadAllBlockAttributes(this BlockReference br, Transaction tr)
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
            foreach (ObjectId aId in br.AttributeCollection)
            {
                AttributeReference ar = tr.GetObject(aId, OpenMode.ForRead) as AttributeReference;
                if (ar == null)
                    continue;
                list.Add(new KeyValuePair<string, string>(ar.Tag, ar.TextString));
            }
            return list;
        }

        /// <summary>
        ///  Iterates through attributes until the one with name "key" is found. Returns null if not found.
        /// </summary>
        /// <param name="br"></param>
        /// <param name="tr"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetAttributeValue(this BlockReference br, Transaction tr, string key)
        {
            foreach (ObjectId aId in br.AttributeCollection)
            {
                AttributeReference ar = tr.GetObject(aId, OpenMode.ForRead) as AttributeReference;
                if (ar == null)
                    continue;
                if (((string)ar.Tag) == key && !string.IsNullOrEmpty(ar.TextString))
                {
                    return ar.TextString;
                }
            }
            return null;
        }

        /// <summary>
        /// Sets new value to the attribute. If attribute name is not found, false is returned.
        /// </summary>
        /// <param name="br"></param>
        /// <param name="tr"></param>
        /// <param name="attributeName"></param>
        /// <param name="attributeValue"></param>
        /// <returns></returns>
        public static bool SetAttributeValue(this BlockReference br, Transaction tr, string attributeName, string attributeValue)
        {
            foreach (ObjectId aId in br.AttributeCollection)
            {
                AttributeReference ar = tr.GetObject(aId, OpenMode.ForRead) as AttributeReference;
                if (ar == null)
                    continue;
                if (((string)ar.Tag) == attributeName)
                {
                    ar.UpgradeOpen();
                    ar.TextString = attributeValue;
                    return true;
                }
            }
            return false;
        }

        public static void UpdateBlockReference(TransactionWrapper tw, BlockReference br,
            Dictionary<string, string> attNameValue = null, Dictionary<string, Color> attNameColor = null)
        {
            attNameValue = attNameValue ?? new Dictionary<string, string>();
            attNameColor = attNameColor ?? new Dictionary<string, Color>();
            foreach (ObjectId attId in br.AttributeCollection)
            {
                var ar = tw.GetObjectWrite<AttributeReference>(attId);
                if (attNameValue.ContainsKey(ar.Tag))
                    ar.TextString = attNameValue[ar.Tag];
                if (attNameColor.ContainsKey(ar.Tag))
                    ar.Color = attNameColor[ar.Tag];
                ar.DowngradeOpen();
            }
        }
        /// <summary>
        /// Assigns new value to attributes. If attribute is not found, it is ignored.
        /// </summary>
        /// <param name="br"></param>
        /// <param name="tr"></param>
        /// <param name="nameValues">Attribute collection (att name, att value).</param>
        public static void SetAttributesValues(this BlockReference br, Transaction tr, 
            Dictionary<string, string> nameValues)
        {
			foreach(ObjectId aId in br.AttributeCollection)
            {
				var ar = tr.GetObjectWrite<AttributeReference>(aId);
				if (nameValues.ContainsKey(ar.Tag))
                {
					ar.TextString = nameValues[ar.Tag];
                }
				ar.DowngradeOpen();
            }
        }

        /// <summary>
        /// Returns all BlockTableRecord objects in the drawing (BlockReference is referencing single BlockTableRecord).
        /// </summary>
        /// <param name="tr"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static IEnumerable<BlockTableRecord> GetAllBlockDefinitions(Transaction tr, Database db)
        {
            List<BlockTableRecord> list = new List<BlockTableRecord>();
            BlockTable bt = tr.GetObjectRead<BlockTable>(db.BlockTableId);
            foreach(ObjectId oid in bt)
            {
                BlockTableRecord btr = tr.GetObjectRead<BlockTableRecord>(oid);
                if (btr.IsLayout)
                    continue;
                list.Add(btr);
            }
            return list;
        }

        /// <summary>
        /// Returns all BlockReference objects existing in the drawing.
        /// </summary>
        /// <param name="tr"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static IEnumerable<BlockReference> GetAllBlockReferences(Transaction tr, Database db)
        {
            List<BlockReference> blockReferences = new List<BlockReference>();
            IEnumerable<BlockTableRecord> blocks = GetAllBlockDefinitions(tr, db);
            foreach(BlockTableRecord btr in blocks)
            {
                IEnumerable<ObjectId> brIds = btr.GetBlockReferenceIds(true, false).Cast<ObjectId>();
                foreach(ObjectId brId in brIds)
                {
                    BlockReference br = tr.GetObjectRead<BlockReference>(brId);
                    blockReferences.Add(br);
                }

            }
            return blockReferences;
        }

        /// <summary>
        /// Checks whether BlockTableRecord contains attribute (has attribute defined).
        /// All BlockReferences share same attributes - they can be hidden, but existing.
        /// </summary>
        /// <param name="tr"></param>
        /// <param name="btr"></param>
        /// <returns></returns>
        public static bool DoesAttributeExist(Transaction tr, BlockTableRecord btr, string attribute)
        {
            foreach(ObjectId oid in btr)
            {
                AttributeDefinition obj = tr.GetObjectRead<AttributeDefinition>(oid);
                if (obj == null)
                    continue;
                if (obj.Tag == attribute)
                    return true;
            }
            return false;
        }
    }
}
