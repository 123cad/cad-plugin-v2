﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
#endif

namespace CadPlugin.Common
{
	/// <summary>
	/// Monitors mouse position on the drawing.
	/// </summary>
	public class PointMonitor:IDisposable, ICursorScreenMovement
	{
		// Holds current point of the cursor on the drawing.
		public Point3d CurrentPoint { get; private set; }
		public string TooltipMessage { get; set; } = null;
		public event Action<PointMonitorEventArgsWrapper> PointMonitorEvent;
		public event Action<Point3d> CursorMoved;

		private Editor ed { get; set; }

		private PointMonitor() { }

		/// <summary>
		/// Note: Putting Breakpoint here, while Tooltip is running, for some reason can result with StackOverflowException.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public virtual void PointMonitorEventHandler(object sender, PointMonitorEventArgs e)
		{
			CurrentPoint = new Point3d(e.Context.ComputedPoint.X, e.Context.ComputedPoint.Y, e.Context.ComputedPoint.Z);
			if (!string.IsNullOrEmpty(TooltipMessage))
				e.AppendToolTipText(TooltipMessage);
			Point3d screenPosition = Cursor.GetCurrentPosition().ToCADPoint().To3d();
			PointMonitorEvent?.Invoke(new PointMonitorEventArgsWrapper(e, screenPosition));
			CursorMoved?.Invoke(screenPosition);		
		}
		public void Finish()
		{
			ed.PointMonitor -= PointMonitorEventHandler;
		}
		/// <summary>
		/// Creates and start point monitor.
		/// </summary>
		/// <param name="ed"></param>
		/// <returns></returns>
		public static PointMonitor Start(Editor ed, string msg = null)
		{
			PointMonitor pm = new PointMonitor()
			{
				ed = ed
			};
			ed.PointMonitor += pm.PointMonitorEventHandler;
			pm.TooltipMessage = msg;
			return pm;
		}

		public void Dispose()
		{
			Finish();
		}
	}
}
