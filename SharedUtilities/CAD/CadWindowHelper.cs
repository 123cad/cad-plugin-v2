﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Common
{
    public class CADWindowHelper
    {
        // For now just explanations because of specific cases that exist.

        /*
 #if BRICSCAD
            //f.Show(Application.MainWindow);
            //f.ShowDialog();
            //Application.ShowModelessDialog(f);
            f.ShowDialog();
            //toolbar.ShowDialog( Application.MainWindow);
            //toolbar.ShowDialog(Application.MainWindow);
            //Application.ShowModelessDialog(Application.MainWindow, toolbar, false);
            //Application.ShowModalDialog(toolbar); 


            //Application.ShowModelessDialog(Application.MainWindow, toolbar);
#endif
#if AUTOCAD

            //Application.ShowModalDialog(Application.MainWindow.Handle, f);
            //Application.ShowModelessDialog(Application.MainWindow.Handle, f);
            //f.Show();
            //Inventory.RunFacade.ActiveForm.Visible = true;
            //return ;

            //Application.ShowModelessDialog(Application.MainWindow.Handle, f);
            //f.Show();
            //f.ShowDialog();
            //Application.ShowModalDialog(Application.MainWindow.Handle, f);
            f.ShowDialog();
#endif


        // This method exists in CadCommonHelper and is used by line/hatch toolbars.
        public static void SetFocus(this Document doc)
		{
			//SetForegroundWindow(Application.MainWindow.Handle);
			//SetFocus(doc.Window.Handle);
			//SetFocus(Application.MainWindow.Handle);
#if BRICSCAD
			Application.MainWindow.Focus();
			doc.Window.Focus();
#endif
			//SetForegroundWindow(Application.DocumentManager.MdiActiveDocument.Window.Handle);

#if AUTOCAD
					// "It is in acmgdinternal.dll, which you need to set reference to."
					Autodesk.AutoCAD.Internal.Utils.SetFocusToDwgView();
                    // maybe is possible like this, because it is displayed in class hierarchy (however, not sure why compiler complains).
            form.Show((System.Windows.Forms.IWin32Window)Application.MainWindow);

        //
#endif
		}
         */
    }
}
