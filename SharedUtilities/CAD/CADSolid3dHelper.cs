﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using runtime = Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using runtime = Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common
{
	/// <summary>
	/// Collection of ComplexSolidEntities objects which share single center point - for example when pipe consists of 2 solids which
	/// don't have connection points - 2 different objects are needed).
	/// </summary>
	public class ComplexSolidEntitiesCollection : IList<ComplexSolidProfileManager>, IDisposable
	{
		// Created as IList because maybe is needed to add some functionality or some additional options.
		// So don't expose inner List.
		private List<ComplexSolidProfileManager> entities = new List<ComplexSolidProfileManager>();

		public ComplexSolidProfileManager this[int index]
		{
			get { return entities[index]; }
			set { entities[index] = value; }
		}

		public int Count => entities.Count;

		public bool IsReadOnly => false;

		public void Add(ComplexSolidProfileManager item)
		{
			entities.Add(item);
		}

		public void Clear() => entities.Clear();

		public bool Contains(ComplexSolidProfileManager item) => entities.Contains(item);

		public void CopyTo(ComplexSolidProfileManager[] array, int arrayIndex) => entities.CopyTo(array, arrayIndex);

		public void Dispose()
		{
			foreach (var ent in entities)
				ent.Dispose();
		}

		public IEnumerator<ComplexSolidProfileManager> GetEnumerator() => entities.GetEnumerator();

		public int IndexOf(ComplexSolidProfileManager item) => entities.IndexOf(item);

		public void Insert(int index, ComplexSolidProfileManager item) => entities.Insert(index, item);

		public bool Remove(ComplexSolidProfileManager item) => entities.Remove(item);

		public void RemoveAt(int index) => entities.RemoveAt(index);

		IEnumerator IEnumerable.GetEnumerator()
		{
			return entities.GetEnumerator();
		}
	}
	
	
	/// <summary>
	/// Provides access to ComplexSolidProfile.
	/// </summary>
	public class ComplexSolidProfileManager:IDisposable
    {
		//NOTE Needed because any drawer which uses this profile
		//NOTE will change position with Translate, and then there is no 
		//NOTE possibility to return it back to original position.
		//NOTE So any user of this profile will get its copy, and is responsible of 
		//NOTE disposing it after usage.

		private ComplexSolidProfile sourceProfile { get; set; }
		public ComplexSolidProfileManager(ComplexSolidProfile profile)
        {
			if (profile == null || profile.BaseProfileEntity == null)
				throw new ArgumentException("Profile is null, or Base profile is not set");
			sourceProfile = profile;
        }

		/// <summary>
		/// Create simple circle profile. Just helper.
		/// </summary>
		/// <param name="diameter"></param>
		/// <returns></returns>
		public static ComplexSolidProfileManager CreateCircleProfile(double diameter)
		{
			Circle c = new Circle();
			c.Diameter = diameter;

			var pr = new ComplexSolidProfile(c);
			var f = new ComplexSolidProfileManager(pr);
			return f;
		}

		public ComplexSolidProfile GetProfile()
        {
			return sourceProfile.Clone();
        }

		public void Dispose()
        {
			sourceProfile.Dispose();
			sourceProfile = null;
        }
    }

	/// <summary>
	/// Keeps entity which represents profile of a 3d (it will be swept along the path).
	/// </summary>
	public class ComplexSolidProfile:IDisposable
	{
		/// <summary>
		/// Entity which represents profile of solid object.
		/// </summary>
		public Entity BaseProfileEntity { get; set; }
		/// <summary>
		/// Collection of objects which are applied to solid created by BaseProfile.
		/// (it could be inside hole of the pipe.)
		/// </summary>
		public List<Entity> InnerEntities { get; } = new List<Entity>();

		public ComplexSolidProfile(Entity baseProfil)
		{
			BaseProfileEntity = baseProfil;
		}

		public ComplexSolidProfile Clone()
        {
			var c = new ComplexSolidProfile((Entity)BaseProfileEntity.Clone());
			c.InnerEntities.AddRange(InnerEntities.Select(x => (Entity)(x.Clone())));
			return c;
        }

		public Entity CloneBaseEntity()
        {
			return (Entity)BaseProfileEntity.Clone();
        }
		public IEnumerable<Entity> CloneInnerEntities()
        {
			var l = InnerEntities.Select(x => (Entity)(x.Clone())).ToList();
			return l;
        }


		/// <summary>
		/// Applies transformation to all entities (base and inner).
		/// </summary>
		/// <param name="transformation"></param>
		public void TransformAll(Matrix3d transformation)
        {
			BaseProfileEntity.TransformBy(transformation);
			foreach (var e in InnerEntities)
				e.TransformBy(transformation);
        }

		public void Dispose()
		{
			if (BaseProfileEntity != null)
			{
				if (BaseProfileEntity.ObjectId != ObjectId.Null)
					// If object has been added to database.
					BaseProfileEntity.Erase();
				else
					BaseProfileEntity.Dispose();
			}
			foreach (var inner in InnerEntities)
			{
				if (inner.ObjectId != ObjectId.Null)
					// If object has been added to database.
					inner.Erase();
				else
					inner.Dispose();
			}
		}
	}
	public class CADSolid3dHelper
	{
		/// <summary>
		/// Creates cylinder with start-end vector as center.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="diameter"></param>
		/// <returns></returns>
		public static Solid3d CreateCylinderAlignedTo(TransactionWrapper data, MyUtilities.Geometry.Point3d start, MyUtilities.Geometry.Point3d end, double diameter)
		{
			MyUtilities.Geometry.Vector3d center = start.GetVectorTo(end);
			return CreateCylinderAlignedTo(data, start, center, diameter);
		}
		/// <summary>
		/// Creates cylinder with centerVector vector as center.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="start"></param>
		/// <param name="aligned"></param>
		/// <returns></returns>
		public static Solid3d CreateCylinderAlignedTo(TransactionWrapper tr, MyUtilities.Geometry.Point3d start, MyUtilities.Geometry.Vector3d centerVector, double diameter)
		{

			Solid3d solid = tr.CreateEntity<Solid3d>();

			// New approach - sweep a line.
			Line l = new Line();
			l.StartPoint = start.ToCADPoint();
			l.EndPoint = start.Add(centerVector).ToCADPoint();

			Circle c = new Circle();
			c.Center = l.StartPoint;
			c.Diameter = diameter;

			SweepOptionsBuilder sob = new SweepOptionsBuilder();
			sob.Align = SweepOptionsAlignOption.AlignSweepEntityToPath;
			sob.BasePoint = l.StartPoint;

			solid.CreateSweptSolid(c, l, sob.ToSweepOptions());


			l.Dispose();
			c.Dispose();



			// This is old approach.
			/*double height = centerVector.Length;
			solid.CreateFrustum(height, diameter, diameter, diameter);
			// Since origin is set to half of height, move it so bottom part of cylinder is on Z=0.
			Matrix3d m = Matrix3d.Displacement(Vector3d.ZAxis.MultiplyBy(height / 2));

			MyUtilities.Geometry.Vector3d perpendicular = centerVector.CrossProduct(new MyUtilities.Geometry.Vector3d(0, 0, 1));
			MyUtilities.Geometry.Vector2d v1, v2;
			// Switch to 2d - XY becomes X, Z remains Z.
			v1 = new MyUtilities.Geometry.Vector2d(0, 1);
			v2 = new MyUtilities.Geometry.Vector2d(centerVector.GetAs2d().Length, centerVector.Z);
			// Angle used to rotate vector with perperdicular vector as axis.
			double angle = v1.GetAngleRad(v2);

			m = m.PreMultiplyBy(Matrix3d.Rotation(angle, perpendicular.ToCADVector(), new Point3d(0, 0, 0)));
			m = m.PreMultiplyBy(Matrix3d.Displacement(start.GetAsVector().ToCADVector()));

			solid.TransformBy(m);*/

			return solid;
		}

		public static Solid3d CreateSolidAlongHelix(TransactionWrapper tw, Helix helix, double diameter, ComplexSolidProfileManager profileFactory)
        {
			SweepOptionsBuilder sob = new SweepOptionsBuilder();
			sob.Align = SweepOptionsAlignOption.AlignSweepEntityToPath;
			sob.BasePoint =  helix.StartPoint;

			var start = helix.GetPointAtParameter(0);
			var next = helix.GetPointAtParameter(0.01);

			var profile = profileFactory.GetProfile();

			FixProfileRotation(profile, start, next);
			var move = Matrix3d.Displacement(helix.StartPoint.GetAsVector());
			profile.TransformAll(move);

			//Circle c = new Circle();
			//c.Center = helix.StartPoint;
			//c.Diameter = diameter;

			// eGeneralModelingFailure - if circle has bigger diameter than helix.
			Solid3d solid = tw.CreateEntity<Solid3d>();
			try
			{
				//tw.CreateEntity<Solid3d>();
				solid.CreateSweptSolid(profile.BaseProfileEntity, helix, sob.ToSweepOptions());
				foreach (var inner in profile.InnerEntities)
				{
					Solid3d hollowSolid = tw.CreateEntity<Solid3d>();// CreateSolidAlongPath(data, points, inner);

					hollowSolid.CreateSweptSolid(inner, helix, sob.ToSweepOptions());
					solid.BooleanOperation(BooleanOperationType.BoolSubtract, hollowSolid);

					hollowSolid.Erase();
				}
					//throw new runtime.Exception();
			}
			catch (runtime.Exception e)
			{
				string msg = "Error occured while trying to draw Solid3d with Helix." + e.Message;
#if DEBUG
				msg += "\nKeep Helix?";
				if (System.Windows.Forms.MessageBox.Show(msg, "", System.Windows.Forms.MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
				{
                    using (TransactionWrapper tw1 = tw.Doc.StartTransaction())
                    {

						Entity c1 = (Entity)(profile.BaseProfileEntity.Clone());
						Entity en = (Entity)helix.Clone();
						tw1.MS.AppendEntity(c1);
						tw1.MS.AppendEntity(en);
						tw1.Tr.AddNewlyCreatedDBObject(c1, true);
						tw1.Tr.AddNewlyCreatedDBObject(en, true);
                        // Do commit even for readonly operations.
                        tw1.Commit();
                    }
				}
#else
				System.Windows.Forms.MessageBox.Show(msg);
#endif

			}
			finally
			{
				//c.Dispose();
				profile.Dispose();
			}

			return solid;
		}

		public static Solid3d CreateCylinderAlongHelix(TransactionWrapper tw, Helix helix, double diameter)
		{
			SweepOptionsBuilder sob = new SweepOptionsBuilder();
			sob.Align = SweepOptionsAlignOption.AlignSweepEntityToPath;
			sob.BasePoint = helix.StartPoint;

			Circle c = new Circle();
			c.Center = helix.StartPoint;
			c.Diameter = diameter;

			

			// eGeneralModelingFailure - if circle has bigger diameter than helix.
			Solid3d solid = tw.CreateEntity<Solid3d>(); ;
			try
			{
				tw.CreateEntity<Solid3d>();
				solid.CreateSweptSolid(c, helix, sob.ToSweepOptions());
			}
			catch(runtime.Exception e)
			{
				string msg = "Error occured while trying to draw Solid3d with Helix." + e.Message;
#if DEBUG
				msg += "\nKeep Helix?";
				if (System.Windows.Forms.MessageBox.Show(msg, "", System.Windows.Forms.MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
				{
					Circle c1 = (Circle)c.Clone();
					Entity en = (Entity)helix.Clone();
					tw.MS.AppendEntity(c1);
					tw.MS.AppendEntity(en);
					tw.Tr.AddNewlyCreatedDBObject(c1, true);
					tw.Tr.AddNewlyCreatedDBObject(en, true);
				}
#else
				System.Windows.Forms.MessageBox.Show(msg);
#endif

			}
			finally
			{
				c.Dispose();
			}

			return solid;
		}

		/// <summary>
		/// Creates cylinder of given diameter, along path represented by polyline.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="points"></param>
		/// <param name="diameter"></param>
		/// <param name="solidToUpdate">If not null, doesn't create new Solid3d, but uses it instead.</param>
		/// <returns></returns>
		public static Solid3d CreateCylinderAlongPath(TransactionWrapper tr, Polyline path, double diameter, Solid3d solidToUpdate = null)
		{
			if (diameter < 0.005)
				diameter = 0.005;

			Solid3d solid = solidToUpdate;
			if (solid == null)
				solid = tr.CreateEntity<Solid3d>();
				

			Circle c = new Circle();
			c.Center = path.StartPoint;
			c.Diameter = diameter;

			SweepOptionsBuilder sob = new SweepOptionsBuilder();
			sob.Align = SweepOptionsAlignOption.AlignSweepEntityToPath;
			sob.BasePoint = path.StartPoint;

			//if (points.First().GetAs2d().GetVectorTo(points.ElementAt(1).GetAs2d()).Length > 0.001)
			solid.CreateSweptSolid(c, path, sob.ToSweepOptions());


			//Debug.Assert(false, "Check if Polyline needs to be appended and then removed from database");
			c.Dispose();

			return solid;
		}
		/// <summary>
		/// Creates cylinder of given diameter, along path represented by points.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="points"></param>
		/// <param name="diameter"></param>
		/// <param name="solidToUpdate">If not null, doesn't create new Solid3d, but uses it instead.</param>
		/// <returns></returns>
		public static Solid3d CreateCylinderAlongPath(TransactionWrapper tr, IEnumerable<MyUtilities.Geometry.Point3d> points, double diameter, Solid3d solidToUpdate = null)
		{
			if (!points.Any())
				return null;
			if (diameter < 0.005)
				diameter = 0.005;

			Solid3d solid = solidToUpdate;
			if (solid == null)
				solid = tr.CreateEntity<Solid3d>();

			Point3d startPoint = points.First().ToCADPoint();

			Polyline3d pl = tr.CreateEntity<Polyline3d>();
			foreach (MyUtilities.Geometry.Point3d p in points)
				pl.AppendPoint(p.ToCADPoint());

			Circle c = new Circle();
			c.Center = startPoint;
			c.Diameter = diameter;

			SweepOptionsBuilder sob = new SweepOptionsBuilder();
			sob.Align = SweepOptionsAlignOption.AlignSweepEntityToPath;
			sob.BasePoint = startPoint;

			//if (points.First().GetAs2d().GetVectorTo(points.ElementAt(1).GetAs2d()).Length > 0.001)
			try
			{
				solid.CreateSweptSolid(c, pl, sob.ToSweepOptions());
			}
			catch(System.Exception e)
			{
				System.Windows.Forms.MessageBox.Show("Error with sweeping solid3d" + e.Message);
			}


			//Debug.Assert(false, "Check if Polyline needs to be appended and then removed from database");
			c.Dispose();
			pl.Erase();

			return solid;
		}
		/// <summary>
		/// Creates cylinder of given diameter, along path represented by points.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="points"></param>
		/// <param name="diameter"></param>
		/// <param name="solidToUpdate">If not null, doesn't create new Solid3d, but uses it instead.</param>
		/// <returns></returns>
		public static Solid3d CreateCylinderAlongSpline(TransactionWrapper tr, IEnumerable<MyUtilities.Geometry.Point3d> points, double diameter, Solid3d solidToUpdate = null)
		{
			if (!points.Any())
				return null;
			if (diameter < 0.005)
				diameter = 0.005;

			Solid3d solid = solidToUpdate;
			if (solid == null)
				solid = tr.CreateEntity<Solid3d>();

			Point3d startPoint = points.First().ToCADPoint();

			Spline spl = tr.CreateEntity<Spline>();
			int index = 0;
			foreach (var pt in points)
			{
				if (index < spl.NumFitPoints)
					spl.SetFitPointAt(index, pt.ToCADPoint());
				else
					spl.InsertFitPointAt(index, pt.ToCADPoint());
				index++;
			}

			Circle c = new Circle();
			c.Center = startPoint;
			c.Diameter = diameter;

			SweepOptionsBuilder sob = new SweepOptionsBuilder();
			sob.Align = SweepOptionsAlignOption.AlignSweepEntityToPath;
			sob.BasePoint = startPoint;

			//if (points.First().GetAs2d().GetVectorTo(points.ElementAt(1).GetAs2d()).Length > 0.001)
			try
			{ 
				solid.CreateSweptSolid(c, spl, sob.ToSweepOptions());
			}
			catch (System.Exception e)
			{
				System.Windows.Forms.MessageBox.Show("Error with sweeping solid3d" + e.Message);
			}


			//Debug.Assert(false, "Check if Polyline needs to be appended and then removed from database");
			c.Dispose();
			//spl.Erase();

			return solid;
		}
		/// <summary>
		/// Creates Cuboid (Rectangular prism) of given x and y length, along path represented by points.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="points"></param>
		/// <param name="diameter"></param>
		/// <returns></returns>
		public static Solid3d CreateCuboidAlongPath(TransactionWrapper tr, IEnumerable<MyUtilities.Geometry.Point3d> points, double lengthX, double lengthY)
		{
			if (!points.Any())
				return null;

			Solid3d solid = tr.CreateEntity<Solid3d>();

			Point3d startPoint = points.First().ToCADPoint();

			Polyline3d pl = new Polyline3d();
			foreach (MyUtilities.Geometry.Point3d p in points)
				pl.AppendPoint(p.ToCADPoint());

			double centerX = startPoint.X;
			double centerY = startPoint.Y;
			double centerZ = startPoint.Z;
			double centerOffsetX = lengthX / 2;
			double centerOffsetY = lengthY / 2;

			Polyline3d polyBase = new Polyline3d();
			polyBase.Closed = true;
			Point3d[] pts = new Point3d[4];
			pts[0] = new Point3d(centerX - centerOffsetX, centerY + centerOffsetY, startPoint.Z);
			pts[1] = new Point3d(centerX + centerOffsetX, centerY + centerOffsetY, startPoint.Z);
			pts[2] = new Point3d(centerX + centerOffsetX, centerY - centerOffsetY, startPoint.Z);
			pts[3] = new Point3d(centerX - centerOffsetX, centerY - centerOffsetY, startPoint.Z);

			for (int i = 0; i < 4; i++)
				polyBase.AppendPoint(pts[i]);


			SweepOptionsBuilder sob = new SweepOptionsBuilder();
			sob.Align = SweepOptionsAlignOption.AlignSweepEntityToPath;
			sob.BasePoint = startPoint;

			solid.CreateSweptSolid(polyBase, pl, sob.ToSweepOptions());


			Debug.Assert(false, "Check if Polyline needs to be appended and then removed from database");
			polyBase.Dispose();
			pl.Dispose();

			return solid;
		}
		private static void FixProfileRotation(ComplexSolidProfile profile, Point3d start, Point3d next)
        {
			var path2d = start.GetAs2d().GetVectorTo(next.GetAs2d()).FromCADVector();
			var axis = Vector2d.XAxis.FromCADVector();
			// Rotate object so it matches direction of axis, and then rotate everything by path angle.
			double angleOfPath = axis.GetAngleDeg(path2d);
			double angleDeg = +90 + angleOfPath;
			// No need to align - 0 or 180 works.
			angleDeg = angleOfPath > 0 ? 180 : 0;
			double angleRad = MyUtilities.Helpers.Calculations.DegreesToRadians(angleDeg);
			Matrix3d rotation = Matrix3d.Rotation(angleRad, Vector3d.ZAxis, new Point3d());
			//complexEntity.TransformAll(rotation);
			profile.TransformAll(rotation);
		}
		/// <summary>
		/// Creates solid by sweeping BaseProfile along the path defined by the points,
		/// and subtracts all inner entities.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="points">This method supports only straight a</param>
		/// <param name="complexEntity">Entity (in 2d) which is used to sweep (profile of the 3d). Must be positioned relative to (0,0) - do not move it.</param>
		/// <returns></returns>
		public static Solid3d CreateSolidAlongPath(TransactionWrapper tr, IEnumerable<MyUtilities.Geometry.Point3d> points, ComplexSolidProfileManager profileManager)
		{
			if (!points.Any())
				return null;

			using (ComplexSolidProfile profile = profileManager.GetProfile())
			{

				Point3d startPoint = points.First().ToCADPoint();
				// Strange problem. When second point of the path has y>0, profile is inverted.
				// Otherwise it is ok.
				//{
				//	var path2d = points.First().GetAs2d().GetVectorTo(points.ElementAt(1).GetAs2d());
				//	var axis = Vector2d.XAxis.FromCADVector();
				//	// Rotate object so it matches direction of axis, and then rotate everything by path angle.
				//	double angleOfPath = axis.GetAngleDeg(path2d);
				//	double angleDeg = +90 + angleOfPath;
				//	// No need to align - 0 or 180 works.
				//	angleDeg = angleOfPath > 0 ? 180 : 0;
				//	double angleRad = MyUtilities.Helpers.Calculations.DegreesToRadians(angleDeg);
				//	Matrix3d rotation = Matrix3d.Rotation(angleRad, Vector3d.ZAxis, new Point3d());
				//	//complexEntity.TransformAll(rotation);
				//	profile.TransformAll(rotation);
				//}
				FixProfileRotation(profile, points.First().ToCADPoint(), points.ElementAt(1).ToCADPoint());

				Matrix3d move = Matrix3d.Displacement(startPoint.GetAsVector());
				profile.BaseProfileEntity.TransformBy(move);
				//complexEntity.InnerEntities.ForEach(x => x.TransformBy(move));

				// If start and end points are closer than 1 cm, make distance 1 cm.
				// Otherwise exception is raised at CreateSweptSolid().
				MyUtilities.Geometry.Point3d p1 = points.First();
				MyUtilities.Geometry.Point3d p2 = points.Last();
				if (p1.GetVectorTo(p2).Length < 0.01)
				{
					List<MyUtilities.Geometry.Point3d> pts = points.ToList();
					pts[pts.Count - 1] = new MyUtilities.Geometry.Point3d(p1.X, p1.Y, p1.Z + 0.01);
					points = pts;
				}

				Solid3d solid = tr.CreateEntity<Solid3d>();


				Polyline3d pl = tr.CreateEntity<Polyline3d>();
				foreach (MyUtilities.Geometry.Point3d p in points)
					pl.AppendPoint(p.ToCADPoint());


				SweepOptionsBuilder sob = new SweepOptionsBuilder();
				sob.Align = SweepOptionsAlignOption.AlignSweepEntityToPath;
				sob.BasePoint = startPoint;

				try
                {
					solid.CreateSweptSolid(profile.BaseProfileEntity, pl, sob.ToSweepOptions());
                }
				catch (System.Exception)
                {

                }

				foreach (var inner in profile.InnerEntities)
				{
					Solid3d hollowSolid = tr.CreateEntity<Solid3d>();// CreateSolidAlongPath(data, points, inner);
					inner.TransformBy(move);

					hollowSolid.CreateSweptSolid(inner, pl, sob.ToSweepOptions());
					solid.BooleanOperation(BooleanOperationType.BoolSubtract, hollowSolid);

					hollowSolid.Erase();
				}

				pl.Erase();

				return solid;
			}
		}
		/// <summary>
		 /// Creates solid by sweeping BaseProfile along the path defined by the points,
		 /// and subtracts all inner entities.
		 /// </summary>
		 /// <param name="tr"></param>
		 /// <param name="path">Create solid along this 2d path (include arcs).</param>
		 /// <param name="complexEntity">Entity (in 2d) which is used to sweep (profile of the 3d). Must be positioned relative to (0,0) - do not move it.</param>
		 /// <returns></returns>
		public static Solid3d CreateSolidAlongPath(TransactionWrapper tr, Polyline path, ComplexSolidProfileManager profileManager)
		{
			using (ComplexSolidProfile profile = profileManager.GetProfile())
			{
				//var points = path.
				var points = path.GetPoints().Select(x => x.FromCADPoint().GetWithHeight(path.Elevation)).ToList();
				if (!points.Any())
					return null;

				Point3d startPoint = points.First().ToCADPoint();
				// Strange problem. When second point of the path has y>0, profile is inverted.
				// Otherwise it is ok.
				{
					var path2d = points.First().GetAs2d().GetVectorTo(points.ElementAt(1).GetAs2d());
					var axis = Vector2d.XAxis.FromCADVector();
					// Rotate object so it matches direction of axis, and then rotate everything by path angle.
					double angleOfPath = axis.GetAngleDeg(path2d);
					double angleDeg = +90 + angleOfPath;
					// No need to align - 0 or 180 works.
					angleDeg = angleOfPath > 0 ? 180 : 0;
					double angleRad = MyUtilities.Helpers.Calculations.DegreesToRadians(angleDeg);
					Matrix3d rotation = Matrix3d.Rotation(angleRad, Vector3d.ZAxis, new Point3d());
					profile.TransformAll(rotation);
				}



				Matrix3d move = Matrix3d.Displacement(startPoint.GetAsVector());
				profile.TransformAll(move);

				// If start and end points are closer than 1 cm, make distance 1 cm.
				// Otherwise exception is raised at CreateSweptSolid().
				MyUtilities.Geometry.Point3d p1 = points.First();
				MyUtilities.Geometry.Point3d p2 = points.Last();
				if (p1.GetVectorTo(p2).Length < 0.01)
				{
					List<MyUtilities.Geometry.Point3d> pts = points.ToList();
					pts[pts.Count - 1] = new MyUtilities.Geometry.Point3d(p1.X, p1.Y, p1.Z + 0.01);
					points = pts;
				}

				Solid3d solid = tr.CreateEntity<Solid3d>();


				/*Polyline3d pl = data.Tr.CreateEntity<Polyline3d>(data.Btr);
				foreach (MyUtilities.Geometry.Point3d p in points)
					pl.AppendPoint(p.ToCADPoint());*/


				SweepOptionsBuilder sob = new SweepOptionsBuilder();
				sob.Align = SweepOptionsAlignOption.AlignSweepEntityToPath;
				sob.BasePoint = startPoint;

				solid.CreateSweptSolid(profile.BaseProfileEntity, path, sob.ToSweepOptions());

				foreach (var inner in profile.InnerEntities)
				{
					Solid3d hollowSolid = tr.CreateEntity<Solid3d>();// CreateSolidAlongPath(data, points, inner);
					inner.TransformBy(move);

					hollowSolid.CreateSweptSolid(inner, path, sob.ToSweepOptions());
					solid.BooleanOperation(BooleanOperationType.BoolSubtract, hollowSolid);

					hollowSolid.Erase();
				}

				//pl.Erase();

				return solid;
			}
		}

		/// <summary>
		/// Joins all solids into single one.
		/// </summary>
		/// <param name="tw"></param>
		/// <param name="solids"></param>
		/// <returns></returns>
		public static Solid3d UnionSolids(TransactionWrapper tw, bool eraseSolids = false, params Solid3d[] solids)
		{
			Solid3d solid = tw.CreateEntity<Solid3d>();
			
			foreach (var s in solids)
			{
				bool erase = true;
				bool changeColor = false;
				try
				{
					//s.TransformBy(Matrix3d.Displacement(new Vector3d(0.00001, 0, 0)));
					//s.TransformBy(Matrix3d.Scaling(1.00001, s.GeometricExtents.MaxPoint));
					solid.BooleanOperation(BooleanOperationType.BoolUnite, s);
				}
				catch(System.Exception e)
				{
					changeColor = true;
					//solid.BooleanOperation(BooleanOperationType.BoolUnite, s);

				}
				if (erase)
					s.Erase();
				if(changeColor)
					s.Color = Color.FromColor(System.Drawing.Color.Violet);
			}
			return solid;
		}


		/// <summary>
		/// Creates cone which is cut if topDiameter is >0. 
		/// </summary>
		/// <param name="td"></param>
		/// <param name="bottom"></param>
		/// <param name="top"></param>
		/// <param name="bottomDiameter"></param>
		/// <param name="topDiameter"></param>
		/// <returns>Cone which is centered so bottom center is at bottom point, and it goes along Z axis.</returns>
		public static Solid3d CreateCircularCutCone(TransactionWrapper td, Point3d bottom, double height, double bottomDiameter, double topDiameter)
		{
			Database db = td.Db;
			Solid3d solidCone;
			solidCone = td.Tr.CreateEntity<Solid3d>(td.MS);
			double bottomOffset = height / 2; // CreateFrustrum creates cone where its center is in 0,0,0
			solidCone.CreateFrustum(height, bottomDiameter / 2, bottomDiameter / 2, topDiameter / 2);
			solidCone.TransformBy(Matrix3d.Displacement(new Point3d(0, 0, -bottomOffset).GetVectorTo(bottom)));

			return solidCone;
		}
		/// <summary>
		/// Creates lofted solid, with start and end circles which do't have to be eccentric (same XY).
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="bottom"></param>
		/// <param name="top"></param>
		/// <param name="bottomDiameter"></param>
		/// <param name="topDiameter"></param>
		/// <returns></returns>
		public static Solid3d CreateEccentricCutCone(TransactionWrapper tr, Point3d bottom, Point3d top, double bottomDiameter, double topDiameter)
		{
			Solid3d solid = tr.CreateEntity<Solid3d>();
			Circle bottomC = tr.CreateEntity<Circle>();
			Circle topC = tr.CreateEntity<Circle>();
			bottomC.Center = bottom;
			topC.Center = top;
			bottomC.Diameter = bottomDiameter;
			topC.Diameter = topDiameter;
#if BRICSCAD
			// On Autocad this is crashing (second parameter is null). 
			solid.CreateLoftedSolid(new Entity[] { bottomC, topC }, null, null, new LoftOptions());
#endif
#if AUTOCAD
			// Autocad doesn't allow second null parameter.
			solid.CreateLoftedSolid(new Entity[] { bottomC, topC }, new Entity[] { }, null, new LoftOptions());
#endif
			bottomC.Erase();
			topC.Erase();
			bottomC.Dispose();
			topC.Dispose();
			return solid;
		}

	}
}
