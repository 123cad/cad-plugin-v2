﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadUtilities.Interaction
{
    /// <summary>
    /// Jig that is using PointMonitor (can be used with other Editor interactions).
    /// </summary>
    /// <remarks>
    /// For example, we can be using Editor.SelectEntity, while having jig function.
    /// </remarks>
    public class FakeJig
    {
        //TODO Need to extract more classes from main plugin, in order for this to work.
        //TODO But, this will introduce potential problems with all code that is referencing those, 
        //TODO so will do it when there is little bit more time.
    }
}
