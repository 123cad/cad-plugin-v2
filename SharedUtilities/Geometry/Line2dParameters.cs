﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtilities.Geometry;

namespace SharedUtilities.Geometry
{
	public class Line2dParameters
	{
		public double K { get; private set; }
		public double N { get; private set; }
		public Line2dParameters(double k, double n)
		{
			K = k;
			N = n;
		}
		public Line2dParameters(Point2d p1, Point2d p2)
		{
			double deltaX = p2.X - p1.X;
			if (System.DoubleHelper.AreEqual(deltaX, 0, 0.001))
			{
				//CHECK
				K = double.MaxValue;
				N = p1.X;
			}
			else
			{
				// y = k * x + n
				K = (p2.Y - p1.Y) / (p2.X - p1.X);
				N = p1.Y - p1.X * K;
			}
		}
		public double GetY(double x)
		{
			double y = K * x + N;
			return y;
		}

		/// <summary>
		/// Moves line along the y axis (positive val moves up, negative moves down).
		/// </summary>
		/// <param name="val"></param>
		public void MoveY(double val)
		{
			N += val;
		}
	}
}
