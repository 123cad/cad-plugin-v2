﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedUtilities.Geometry
{

	public class TangentPoints
	{
		public Point2d Point1 { get; set; }
		public Point2d Point2 { get; set; }
		//public Line2dParameters Line1 { get; set; }
		//public Line2dParameters Line2 { get; set; }
		
	}
	/// <summary>
	/// Calculates intersection between a line and a circle.
	/// </summary>
	public class TangentIntersection
	{
		/// <summary>
		/// Calculates 2 lines starting at a given point
		/// (outside of given circle), which are tangents to a
		/// given circle.
		/// Returns 2 points of intersection between circle and those tangents.
		///
		/// There is a circle and a point.
		/// </summary>
		public static TangentPoints TangentPoints(Point2d circleCenter, double circleDiameter, Point2d pointP)
		{
			// Point must be outside of circle.
			var l = circleCenter.GetVectorTo(pointP).Length;
			if (l <= circleDiameter)
				throw new NotSupportedException("TangentIntersection requires pointP to be outside of circle. ");

			// Translate circle to center (for easier calculation). 
			Vector2d offset = circleCenter.GetAsVector();
			Point2d p0 = pointP.Add(offset.Negate());

			double x0, y0;
			x0 = p0.X;
			y0 = p0.Y;

			// x2 + y2 = r2 (circle is in center)
			// y - y0 = k (x - x0)
			double r = circleDiameter/2;
			double Dk = x0 * x0 + y0 * y0 - r * r;
			Dk = r * Math.Sqrt(Dk);
			double k1 = (-x0 * y0 + Dk) / (r * r - x0 * x0);
			double k2 = (-x0 * y0 - Dk) / (r * r - x0 * x0);

			double x1 = k1 * (k1 * x0 - y0) / (1 + k1 * k1);
			double x2 = k2 * (k2 * x0 - y0) / (1 + k2 * k2);

			double y1 = k1 * (x1 - x0) + y0;
			double y2 = k2 * (x2 - x0) + y0;
			var res = new TangentPoints();
			res.Point1 = new Point2d(x1, y1).Add(offset);
			res.Point2 = new Point2d(x2, y2).Add(offset);
			return res;
		}
	}
}
