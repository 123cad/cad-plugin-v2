﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common
{
    /// <summary>
    /// Custom defined Coordinate system 3d.
    /// </summary>
    public class ModelSpace3dCAD
    {
        //NOTE Works same as ModelSpace3d from Utilities.
        // m11 m12 m13 m14
        // m21 m22 m23 m24
        // m31 m32 m33 m34
        // m41 m42 m43 m44

        // xAxis = (m11, 

        private Matrix3d modelToWorld { get; set; }
        private Matrix3d worldToModel { get; set; }

        public ModelSpace3dCAD(Vector3d xAxis, Vector3d yAxis, Vector3d zAxis, Point3d center)
        {
            xAxis = xAxis.GetNormal();
            yAxis = yAxis.GetNormal();
            zAxis = zAxis.GetNormal();
            modelToWorld = Matrix3d.AlignCoordinateSystem(new Point3d(), Vector3d.XAxis, Vector3d.YAxis, Vector3d.ZAxis,
                                            center, xAxis, yAxis, zAxis);
            worldToModel = modelToWorld.Inverse();
        }
        public Point3d WorldToModel(Point3d p)
        {
            var pt = p.TransformBy(worldToModel);
            return pt;
        }
        public Point3d ModelToWorld(Point3d p)
        {
            var pt = p.TransformBy(modelToWorld);
            return pt;
        }
        public Vector3d WorldToModel(Vector3d p)
        {
            var pt = p.TransformBy(worldToModel);
            return pt;
        }
        public Vector3d ModelToWorld(Vector3d p)
        {
            var pt = p.TransformBy(modelToWorld);
            return pt;
        }
    }
}
