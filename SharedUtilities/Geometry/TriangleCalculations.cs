﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace SharedUtilities.Geometry
{
    public class TriangleCalculations
    {
        /// <summary>
        /// Checks if point is inside triangle (in 2d).
        /// </summary>
        /// <param name="triangleP1">Triangle vertex.</param>
        /// <param name="triangleP2">Triangle vertex</param>
        /// <param name="triangleP3">Triangle vertex</param>
        /// <param name="p">Point which is tested.</param>
        /// <returns></returns>
        public static bool IsPointInside(
            Point3d triangleP1, Point3d triangleP2, Point3d triangleP3,
            Point3d p)
        {
            
            bool result = IsPointInside(
                triangleP1.GetAs2d(),
                triangleP2.GetAs2d(),
                triangleP3.GetAs2d(),
                p.GetAs2d()
                );

            return result;
        }


        /// <summary>
        /// Checks if point is inside triangle.
        /// </summary>
        /// <param name="triangleP1">Triangle vertex.</param>
        /// <param name="triangleP2">Triangle vertex</param>
        /// <param name="triangleP3">Triangle vertex</param>
        /// <param name="p">Point which is tested.</param>
        /// <remarks>
        /// It works by creating a ray in tested point, 
        /// and checking number of intersections with triangle sides.
        /// If number is even (including 0), point is outside. Otherwise, it is 
        /// inside.
        /// </remarks>
        /// <see cref="https://forums.autodesk.com/t5/objectarx/point-inside-polyline/td-p/316152"/>
        private static bool IsPointInside(
            Point2d triangleP1, Point2d triangleP2, Point2d triangleP3,
            Point2d p)
        {
            var tp = p;
            var p1 = triangleP1;
            var p2 = triangleP2;
            var p3 = triangleP3;
            Ray2d ray = new Ray2d(tp, new Vector2d(1, 0));
            var l1 = new LineSegment2d(p1, p2);
            var l2 = new LineSegment2d(p2, p3);
            var l3 = new LineSegment2d(p3, p1);
            int counter = 0;
            var pts = l1.IntersectWith(ray);
            if (pts.Any())
                counter += l1.IsOn(pts[0]) ? 1 : 0;
            pts = l2.IntersectWith(ray);
            if (pts.Any())
                counter += l2.IsOn(pts[0]) ? 1 : 0;
            pts = l3.IntersectWith(ray);
            if (pts.Any())
                counter += l3.IsOn(pts[0]) ? 1 : 0;

            bool result = counter > 0 && counter % 2 == 1;

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <returns></returns>
        /// <see cref="https://www.wikihow.com/Calculate-the-Center-of-Gravity-of-a-Triangle"/>
        public static Point2d GetCenterOfGravity(
            Point2d p1, Point2d p2, Point2d p3)
        {
            var x = p1.X + p2.X + p3.X;
            var y = p1.Y + p2.Y + p3.Y;
            x /= 3;
            y /= 3;
            return new Point2d(x, y);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <returns></returns>
        /// <see cref="https://www.wikihow.com/Calculate-the-Center-of-Gravity-of-a-Triangle"/>
        public static Point3d GetCenterOfGravity(
            Point3d p1, Point3d p2, Point3d p3)
        {
            var x = p1.X + p2.X + p3.X;
            var y = p1.Y + p2.Y + p3.Y;
            var z = p1.Z + p2.Z + p3.Z;
            x /= 3;
            y /= 3;
            z /= 3;
            return new Point3d(x, y, z);
        }

    }
}
