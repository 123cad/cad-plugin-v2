﻿using MyUtilities.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace SharedUtilities.Geometry
{
	public class CircleLineIntersectionResult
	{
		/// <summary>
		/// There are 3 possibilities:
		///		No intersection points
		///		1 intersection point (tangent)
		///		2 intersection points
		/// </summary>
		public bool HasIntersectionPoints
			=> Point1.HasValue || Point2.HasValue;

		/// <summary>
		/// Only 1 intersection point.
		/// </summary>
		public bool IsTangent
			=> HasIntersectionPoints && !Point2.HasValue;
		public Point2d? Point1 { get; set; }
		public Point2d? Point2 { get; set; }

		/// <summary>
		/// No intersection points.
		/// </summary>
		public CircleLineIntersectionResult()
		{
		}

		/// <summary>
		/// Has 1 intersection point (tangent point).
		/// </summary>
		public CircleLineIntersectionResult(Point2d p)
		{
			Point1 = p;
		}

		/// <summary>
		/// Has 2 intersection points.
		/// </summary>
		public CircleLineIntersectionResult(Point2d p1, Point2d p2)
		{
			Point1 = p1;
			Point2 = p2;
		}

	}

	/// <summary>
	/// Calculates intersection between line and circle.
	/// </summary>
	public class CircleLineIntersection
	{
		public static CircleLineIntersectionResult GetIntersection(Point2d centerCircle, double circleDiameter, Point2d point1, Point2d point2)
		{
			try
			{
				bool rotated = false;
				 
				Vector2d offset = centerCircle.GetAsVector();
				// Circle is at (0,0)
				Point2d p1 = point1.Add(offset.Negate());
				Point2d p2 = point2.Add(offset.Negate());
				if (Math.Abs(p1.X - p2.X) < 1e-6)
				{
					// If vertical, rotate 90 (cc) and unrotated (90 (c))
					rotated = true;
					p1 = new Point2d(-p1.Y, p1.X);
					p2 = new Point2d(-p2.Y, p2.X);
				}
				Line2dParameters lineParam = new Line2dParameters(p1, p2);
				// 3 possibilities:
				//		D < 0 - no intersection
				//		D = 0 - 1 intersection (tangent point)
				//		D > 0 - 2 intersection points
				// D = b^2 - 4ac
				// Combine from 2 equations:
				// x^2+y^2=r^2
				// y = kx+n ( y=k(x-x0)+y0 ; includes k and 1 point)
				double k = lineParam.K;
				double x0, y0;
				x0 = p1.X;
				y0 = p1.Y;
				double r = circleDiameter / 2;
				// In equation number 4 is lost (b^2 has multiplier 4)
				double a = 1 + k * k;
				double b = 2 * k * (y0 - k * x0);
				double c = k * k * x0 * x0 - 2 * k * x0 * y0 + y0 * y0 - r * r;

				double D = b * b - 4 * a * c;

				//double b2 = k * k * (y0 * y0 - 2 * k * x0 * y0 + k * k * x0 * x0);
				//double ac = (1 + k * k) * (k * k * x0 * x0 - 2 * k * x0 * y0 + y0 * y0 - r * r);
				//double D = b2 - ac;
				CircleLineIntersectionResult res = null;
				if (D < 0)
					res = new CircleLineIntersectionResult();
				else if (D < 1e-6)
				{
					double x = -b / (2 * a);
					double y = lineParam.GetY(x);
					var pt = new Point2d(x, y);
					//pt = pt.Add(offset);
					res = new CircleLineIntersectionResult(pt);
				}
				else
				{
					D = Math.Sqrt(D);
					double x1 = (-b + D) / (2 * a);
					double x2 = (-b - D) / (2 * a);
					double y1 = lineParam.GetY(x1);
					double y2 = lineParam.GetY(x2);
					var pt1 = new Point2d(x1, y1);
					var pt2 = new Point2d(x2, y2);
					
					res = new CircleLineIntersectionResult(pt1, pt2);
				}

				var resP1 = res.Point1;
				var resP2 = res.Point2;
				if (rotated)
				{
					if (resP1.HasValue)
						resP1 = new Point2d(resP1.Value.Y, -resP1.Value.X);
					if (resP2.HasValue)
						resP2 = new Point2d(resP2.Value.Y, -resP2.Value.X);
				}

				res.Point1 = resP1?.Add(offset);
				res.Point2 = resP2?.Add(offset);
				return res;
			}
			catch (Exception e)
			{
				return new CircleLineIntersectionResult();
			}
		}
	}
}
