﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using my = MyUtilities.Geometry;

#if BRICSCAD
using cad = Teigha.Geometry;
#endif

#if AUTOCAD
using cad = Autodesk.AutoCAD.Geometry;
#endif

//namespace CadPlugin.Geometry
//{
	/// <summary>
	/// Separate class for conversion between my Graphics and acad/bcad graphics.
	/// This is done in order to enable unit testing (acad/bcad dlls must not be loaded).
	/// So separate class is used for conversion (if conversion added to my graphics classes, 
	/// when class is loaded acad/bcad dll will be attempted to load).
	/// </summary>
	public static class CADGraphicsConverter
	{

		/// <summary>
		/// Converts CAD Point2d to My Point2d.
		/// </summary>
		/// <param name="v"></param>
		public static my.Point2d FromCADPoint(this cad.Point2d v)
		{
			return new my.Point2d(v.X, v.Y);
		}
		/// <summary>
		/// Converts CAD Point3d to My Point3d.
		/// </summary>
		/// <param name="v"></param>
		public static my.Point3d FromCADPoint(this cad.Point3d v)
		{
			return new my.Point3d(v.X, v.Y, v.Z);
		}
		/// <summary>
		/// Converts CAD Vector2d to My Vector2d.
		/// </summary>
		/// <param name="v"></param>
		public static my.Vector2d FromCADVector(this cad.Vector2d v)
		{
			return new my.Vector2d(v.X, v.Y);
		}
		/// <summary>
		/// Converts CAD Vector3d to My Vector3d.
		/// </summary>
		/// <param name="v"></param>
		public static my.Vector3d FromCADVector(this cad.Vector3d v)
		{
			return new my.Vector3d(v.X, v.Y, v.Z);
		}


		/// <summary>
		/// Converts My Point2d to CAD Point2d.
		/// </summary>
		/// <param name="v"></param>
		public static cad.Point2d ToCADPoint(this my.Point2d v)
		{
			return new cad.Point2d(v.X, v.Y);
		}
		/// <summary>
		/// Converts My Point3d to CAD Point3d.
		/// </summary>
		/// <param name="v"></param>
		public static cad.Point3d ToCADPoint(this my.Point3d v)
		{
			return new cad.Point3d(v.X, v.Y, v.Z);
		}
		/// <summary>
		/// Converts My Vector2d to CAD Vector2d.
		/// </summary>
		/// <param name="v"></param>
		public static cad.Vector2d ToCADVector(this my.Vector2d v)
		{
			return new cad.Vector2d(v.X, v.Y);
		}
		/// <summary>
		/// Converts My Vector3d to CAD Vector3d.
		/// </summary>
		/// <param name="v"></param>
		public static cad.Vector3d ToCADVector(this my.Vector3d v)
		{
			return new cad.Vector3d(v.X, v.Y, v.Z);
		}
	}
//}
