﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common.Comparers
{
    /// <summary>
    /// Comparers only for CAD types.
    /// </summary>
    public class CADComparer
    {
        private static Dictionary<Type, object> comparers = new Dictionary<Type, object>();
        static CADComparer()
        {
            // Automatic load of all defined comparers.
            var types = typeof(CADComparer).GetNestedTypes( System.Reflection.BindingFlags.NonPublic);
            foreach(Type comparer in types)
            {
                Type genericType = comparer.GetInterfaces()[0].GenericTypeArguments[0];
                comparers.Add(genericType, Activator.CreateInstance(comparer));
            }
        }
        public static IEqualityComparer<T> Get<T>()
        {
            var t = typeof(T);
            IEqualityComparer<T> comparer;
            if (comparers.ContainsKey(t))
                comparer = (IEqualityComparer<T>)comparers[t];
            else
                comparer = EqualityComparer<T>.Default;
            return comparer;
        }

        class Point2dEquality : IEqualityComparer<Point2d>
        {
            public static Point2dEquality Instance = new Point2dEquality();
            public bool Equals(Point2d x, Point2d y) => x.IsEqualTo(y, new Tolerance(0, 0.0001));
            public int GetHashCode(Point2d obj) => obj.GetHashCode();
        }
        class Point3dEquality : IEqualityComparer<Point3d>
        {
            public static Point3dEquality Instance = new Point3dEquality();
            public bool Equals(Point3d x, Point3d y) => x.IsEqualTo(y, new Tolerance(0, 0.0001));
            public int GetHashCode(Point3d obj) => obj.GetHashCode();
        }
    }
}
