﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Common.Comparers
{
    /// <summary>
    /// Comparers for non CAD types.
    /// </summary>
    class MyComparer
    {
        private static Dictionary<Type, object> comparers = new Dictionary<Type, object>();
        static MyComparer()
        {
            // Automatic load of all defined comparers.
            var types = typeof(CADComparer).GetNestedTypes(System.Reflection.BindingFlags.NonPublic);
            foreach (Type comparer in types)
            {
                Type genericType = comparer.GetInterfaces()[0].GenericTypeArguments[0];
                comparers.Add(genericType, Activator.CreateInstance(comparer));
            }
        }
        public static IEqualityComparer<T> Get<T>()
        {
            var t = typeof(T);
            IEqualityComparer<T> comparer;
            if (comparers.ContainsKey(t))
                comparer = (IEqualityComparer<T>)comparers[t];
            else
                comparer = EqualityComparer<T>.Default;
            return comparer;
        }
    }
}
