﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class DoubleHelper
{
	private static string[] decimals = new string[30];
	/// <summary>
	/// Converts double to string with Invariant culture (point is decimal point).
	/// </summary>
	/// <returns></returns>
	public static string ToStringInvariant(double d, int decimalPlaces, bool thousandSeparator = false)
	{
		if (decimalPlaces < 0)
			decimalPlaces = 0;
		if (decimalPlaces >= decimals.Length)
			decimalPlaces = decimals.Length - 1;
		if (decimals[decimalPlaces] == null)
			decimals[decimalPlaces] = "0." + string.Concat(Enumerable.Repeat("0", decimalPlaces));
		string s = decimals[decimalPlaces];
		if (thousandSeparator)
			s = "0," + s;
		return d.ToString(s, CultureInfo.InvariantCulture);
	}
	/// <summary>
	/// Parses string to double with invariant culture (point is decimal point).
	/// </summary>
	public static double ParseInvariantDouble(string s)
	{
		NumberStyles style = NumberStyles.AllowDecimalPoint |
							 NumberStyles.AllowLeadingSign |
							 NumberStyles.AllowLeadingWhite |
							 NumberStyles.AllowThousands |
							 NumberStyles.AllowTrailingWhite;
		return double.Parse(s, style, CultureInfo.InvariantCulture);
	}
	/// <summary>
	/// Checks whether s contains valid double value.
	/// </summary>
	/// <param name="s"></param>
	/// <returns></returns>
	public static bool IsValidString(string s)
	{
		NumberStyles style = NumberStyles.AllowDecimalPoint |
							 NumberStyles.AllowLeadingSign |
							 NumberStyles.AllowLeadingWhite |
							 NumberStyles.AllowThousands |
							 NumberStyles.AllowTrailingWhite;
		double d;
		return double.TryParse(s, style, CultureInfo.InvariantCulture, out d);
	}

	/// <summary>
	/// Checks if string can be parsed by double, using InvariantCulture. 
	/// </summary>
	/// <param name="s"></param>
	/// <param name="d"></param>
	/// <returns></returns>
	public static bool TryParseInvariant(string s, out double d)
	{
		NumberStyles style = NumberStyles.AllowDecimalPoint |
							 NumberStyles.AllowLeadingSign |
							 NumberStyles.AllowLeadingWhite |
							 NumberStyles.AllowThousands |
							 NumberStyles.AllowTrailingWhite;
		if (!double.TryParse(s, style, CultureInfo.InvariantCulture, out d))
			return false;
		return true;
	}
	/// <summary>
	/// Tells if two d objects are equal with provided precision.
	/// </summary>
	/// <param name="d1"></param>
	/// <param name="d2"></param>
	/// <param name="precision"></param>
	/// <returns></returns>
    [Obsolete("Switch to MyUtilities DoubleHelper")]
	public static bool AreEqual(double d1, double d2, double precision)
	{
		double d = Math.Abs(d1 - d2);
		return d < precision;
	}
}
