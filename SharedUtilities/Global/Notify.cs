﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

//namespace CadPlugin.Common

    public class Notify : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Invokes provided delegate, then raises PropertyChanged event.
        /// </summary>
        protected void OnPropertyChanged(Action a, [CallerMemberName] string name = null)
        {
            a();
            OnPropertyChanged(name);
        }

        /// <summary>
        /// Assigns value to field, then raises PropertyChanged event.
        /// </summary>
        protected void OnPropertyChanged<T>(ref T field, T value, [CallerMemberName] string name = null)
        {
            field = value;
            OnPropertyChanged(name);
        }
    }

