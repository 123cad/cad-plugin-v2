﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CadPlugin
{
	/// <summary>
	/// Serializable Color object.
	/// </summary>
	[Serializable]
	public class ColorPersistence
	{
		public int Red;
		public int Green;
		public int Blue;
		public int Alpha;
		public ColorPersistence()
		{

		}
		public ColorPersistence(Color c)
		{
			Red = c.R;
			Green = c.G;
			Blue = c.B;
			Alpha = c.A;
		}
		public Color ToColor()
		{
			return Color.FromArgb(Alpha, Red, Green, Blue);
		}
	}
}
