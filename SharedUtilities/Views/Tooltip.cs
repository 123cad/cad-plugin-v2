﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif


namespace CadPlugin.Common
{
    /// <summary>
    /// Tooltip which can display text or custom WindowsForm.
    /// Tooltip is transparent and can be clicked through.
    /// </summary>
    public class Tooltip : IDisposable
    {
        #region PInvoke
        // WINAPI Constants
        private const int GWL_EXSTYLE = -20;
        private const int WS_EX_LAYERED = 0x80000;
        private const int LWA_ALPHA = 2;
        private const int WS_EX_TRANSPARENT = 0x20;

        [DllImport("user32.dll")]
        static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll", EntryPoint = "SetWindowLongA", SetLastError = true)]
        private static extern int SetWindowLong(IntPtr hwnd, int nIndex, int dwNewLong);
        [DllImport("user32.dll")]
        private static extern bool SetLayeredWindowAttributes(IntPtr hwnd, uint crKey, byte bAlpha, uint dwFlags);

        #endregion
        /// <summary>
        /// Effective only for default form.
        /// </summary>
        public string DisplayedText
        {
            get => _DisplayedText;
            set
            {
                _DisplayedText = value;
                DefaultForm f = Form as DefaultForm;
                if (f != null)
                    f.DisplayedMessage = value;
            }
        }
        /// <summary>
        /// Offset from provided position when moving.
        /// </summary>
        public int OffsetX { get; set; } = 10;
        /// <summary>
        /// Offset from provided position when moving.
        /// </summary>
        public int OffsetY { get; set; } = 10;
        public bool IsTransparent
        {
            get => Form.AllowTransparency;
            set => Form.AllowTransparency = value;
        }

        /// <summary>
        /// If set, tooltip is moved with this
        /// </summary>
        public ICursorScreenMovement MovementSource
        {
            set
            {
                if (_MovementSource != null)
                    _MovementSource.CursorMoved -= _MovementSource_CursorMovedEventHandler;
                _MovementSource = value;
                _MovementSource.CursorMoved += _MovementSource_CursorMovedEventHandler;
            }
        }
        /// <summary>
        /// On every move event generates new text to be displayed.
        /// </summary>
        //public Func<string> OnMovementTextUpdate;

        /// <summary>
        /// Displayed form.
        /// </summary>
        public Form Form { get; private set; }
        
        
        private ICursorScreenMovement _MovementSource;
        private string _DisplayedText = "";
        private System.Drawing.Color transparentColor = SystemColors.Control;
        /// <summary>
        /// Returns Tooltip instance with default or provided Form.
        /// When custom form is provided, it is responsible for all display updates.
        /// </summary>
        /// <param name="custom">If not provided, default one is used. Disposed with Tooltip.</param>
        /// <returns></returns>
        public static Tooltip GetTooltip(Form custom = null)
        {
            var f = custom ?? new DefaultForm();
            return new Tooltip(f);
        }
        private Tooltip(Form f)
        {
            Form = f;
            f.FormBorderStyle = FormBorderStyle.None;
            f.Width = 120;
            f.Height = 80;
            f.BackColor = transparentColor;
            f.ControlBox = true;
            f.TopMost = true;
            f.ShowInTaskbar = false;
            //f.AllowTransparency = true;
            f.ShowIcon = false;
            f.TransparencyKey = transparentColor;
            //if (IsTransparent)
            {
                // Can be done in different way. But for now, we stick with it.
                if (!(f is DefaultForm))
                    SetControlsTransparent(f);
            }
            SetWindowLong(f.Handle, GWL_EXSTYLE, GetWindowLong(f.Handle, GWL_EXSTYLE) | WS_EX_LAYERED | WS_EX_TRANSPARENT);
            f.Show();
        }
        public void SetVisible(bool visible)
        {
            Form.Visible = visible;
        }
        private void SetControlsTransparent(Control start)
        {
            start.BackColor = transparentColor;
            foreach (Control c in start.Controls)
                SetControlsTransparent(c);
        }
        public void Move(int x, int y)
        {
            Form.Location = new Point(x + OffsetX, y + OffsetY);
        }
        public void Move(Point p) => Move(p.X, p.Y);

        private void _MovementSource_CursorMovedEventHandler(Point3d obj)
        {
            Move(new Point((int)obj.X, (int)obj.Y));
            //if (OnMovementTextUpdate != null)
                //DisplayedText = OnMovementTextUpdate();
        }
        public void Dispose()
        {
            if (!(Form?.IsDisposed ?? true))
            {
                Form.Dispose();
                Form = null;
            }
        }
        class DefaultForm : Form
        {
            public string DisplayedMessage
            {
                set
                {
                    setMessage(value);
                }
            }
            private Action<string> setMessage;
            private int margin = 4;
            public DefaultForm() : base()
            {
                Width = 220;
                Height = 180;
                Label l = new Label();
                l.BorderStyle = BorderStyle.FixedSingle;
                l.Location = new Point();// margin, margin);
                //l.MaximumSize = new Size(Width - 2 * margin, Height - 2 * margin);
                l.Padding = new Padding(4);
                l.AutoSize = true;
                setMessage = m => l.Text = m;
                Controls.Add(l);
                l.SizeChanged += L_SizeChanged;
            }

            private void L_SizeChanged(object sender, EventArgs e)
            {
                Label l = (Label)sender;
                Width = l.Width;// + margin * 2;
                Height = l.Height;// + margin * 2;
            }

            //NOTE If there are problems with background size, form can be transparent, and label
            //NOTE background set to some other color, so it is not transparent.
            //NOTE In this way we will see only label.
        }
    }
}
