﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

namespace CadPlugin.Common
{
    public interface ICursorScreenMovement
    {
        event Action<Point3d> CursorMoved;
    }
    public class PointMonitorEventArgsWrapper
    {
        public PointMonitorEventArgs E { get; private set; }
        public Point3d ScreenPosition { get; private set; }

        public static implicit operator PointMonitorEventArgs(PointMonitorEventArgsWrapper t)=>t.E;

        public PointMonitorEventArgsWrapper(PointMonitorEventArgs e, Point3d screenPosition)
        {
            E = e;
            ScreenPosition = screenPosition;
        }
    }
}
