﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin.Common
{
    /// <summary>
    /// Global cursor operations (related to screen, not to application).
    /// </summary>
    public class Cursor
    {
        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetCursorPos(out POINT lpPoint);
        // DON'T use System.Drawing.Point, the order of the fields in System.Drawing.Point isn't guaranteed to stay the same.

        [DllImport("user32.dll")]
        static extern IntPtr GetCursor();

        public struct POINT
        {
            public int X;
            public int Y;
            public override string ToString()
            {
                return $"x={X.ToString("0.00")} \n y={Y.ToString("0.00")}";
            }
        }

        /// <summary>
        /// Returns current position of cursor, in screen coordinates.
        /// </summary>
        /// <returns></returns>
        public static MyUtilities.Geometry.Point2d GetCurrentPosition()
        {
            POINT pt;
            GetCursorPos(out pt);
            return new MyUtilities.Geometry.Point2d(pt.X, pt.Y);
        }
    }
}
