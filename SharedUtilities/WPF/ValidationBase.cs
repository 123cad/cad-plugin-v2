﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WPF
{
    /// <summary>
    /// Validation for WPF, using INotifyDataError interface.
    /// <para>ViewModel should inherit from this class, to get 
    /// access to validation (with preserving property changed
    /// mixed with validation, or by itself).</para>
    /// </summary>
    public class ValidationBase : Notify, INotifyDataErrorInfo
    {
        public bool HasErrors => errors.Any();

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        private Dictionary<string, List<string>> errors { get; } = new Dictionary<string, List<string>>();

        public IEnumerable GetErrors(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
                return errors.Values;
            else
            {
                if (errors.ContainsKey(propertyName))
                    return errors[propertyName];
                return null;
            }
        }

        /// <summary>
        /// Sets errors for specified property.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="propertyErrors"></param>
        private void SetErrors(string propertyName, List<string> propertyErrors)
        {
            // Clear any errors that already exist for this property.
            errors.Remove(propertyName);
            // Add the list collection for the specified property.
            errors.Add(propertyName, propertyErrors);
            if ((errors[propertyName]?.Count ?? 0) == 0)
                errors.Remove(propertyName);

            // Raise the error-notification event.
            if (ErrorsChanged != null)
                ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
        }


        private void ClearErrors(string propertyName)
        {
            // Remove the error list for this property.
            errors.Remove(propertyName);
            // Raise the error-notification event.
            if (ErrorsChanged != null)
                ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Keeps error messages in validation action.
        /// </summary>
        /// <remarks>Used only to </remarks>
        protected class ErrorManager
        {
            public List<string> Errors = new List<string>();
            /// <summary>
            /// Adds error message.
            /// </summary>
            public ErrorManager Add(string s)
            {
                if (!string.IsNullOrEmpty(s))
                    Errors.Add(s);
                return this;
            }
            /// <summary>
            /// Adds error message if expression is true.
            /// </summary>
            public ErrorManager AddIf(bool expression, string msg)
            {
                if (expression)
                    Errors.Add(msg);
                return this;
            }
            public ErrorManager AddNotIf(bool expression, string msg)
            {
                if (!expression)
                    Errors.Add(msg);
                return this;
            }
        }
        /// <summary>
        /// Performs validation, and then update followed by PropertyChanged event.
        /// </summary>
        /// <param name="validate">Validation logic (add existing errors to parameter).</param>
        /// <param name="update">Assign new value to Property - PropertyChanged event is raised
        /// automatically.</param>
        protected void ValidateChange(Action<ErrorManager> validate, Action update, [CallerMemberName] string name = null)
        {
            var mgr = new ErrorManager();
            validate?.Invoke(mgr);
            if (mgr.Errors.Any())
                SetErrors(name, mgr.Errors);
            else
                ClearErrors(name);
            // Invoke update in any case, so OnPropertyChanged is invoked.
            if (update == null)
                update = () => { };
            OnPropertyChanged(update, name);
        }
    }
}
