﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyExceptions;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif


#if AUTOCAD
namespace Autodesk.AutoCAD.DatabaseServices
#endif

#if BRICSCAD
namespace Teigha.DatabaseServices
#endif
{
    public static class CADDBDictionaryExtensions
    {
        /// <summary>
        /// Returns dictionary from fiven path, in Read state.
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="tr"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static DBDictionary OpenSubDictionary(this DBDictionary dic, Transaction tr, params string[] path)
        {
            DBDictionary d = dic;
            foreach(var n in path)
            {
                if (d.Contains(n))
                {
                    d = tr.GetObjectRead<DBDictionary>(d.GetAt(n));
                    continue;
                }
                else
                {
                    d = null;
                    break;
                }
            }
            return d;
        }
        /// <summary>
		///  Opens DBDictionary(if not found creates new ones) on a given path, and returns last Dictionary.
		/// </summary>
		/// <param name="tr"></param>
		/// <param name="current"></param>
		/// <param name="names"></param>
		/// <returns></returns>
		public static DBDictionary OpenOrCreateSubDictionary(this DBDictionary dic, Transaction tr, params string[] names)
        {
            // Input dic will be writeenabled (because i can't test all code which uses it, it was like this, so just leave safer way).
            // All dictionaries which are between input dic and returned dic will remain in ReadOnly state.
            DBDictionary currentDic = dic;
            if (!currentDic.IsWriteEnabled)
                currentDic.UpgradeOpen();
            foreach (string name in names)
            {
                if (!currentDic.Contains(name))
                {
                    if (!currentDic.IsWriteEnabled)
                        currentDic.UpgradeOpen();
                    DBDictionary d = new DBDictionary();
                    currentDic.SetAt(name, d);
                    tr.AddNewlyCreatedDBObject(d, true);
                    if (currentDic != dic)
                        currentDic.DowngradeOpen();
                    currentDic = d;
                }
                else
                    currentDic = tr.GetObject(currentDic.GetAt(name), OpenMode.ForRead) as DBDictionary;
            }
            if (!currentDic.IsWriteEnabled)
                currentDic.UpgradeOpen();
            return currentDic;
        }

        /// <summary>
        /// If XRecord exists in dictionary, it is opened for Read.
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="tr"></param>
        /// <param name="entry"></param>
        /// <returns></returns>
        public static Xrecord OpenXrecord(this DBDictionary dic, Transaction tr, string entry)
        {
            if (dic.Contains(entry))
                return tr.GetObjectRead<Xrecord>(dic.GetAt(entry));
            return null;
        }
        /// <summary>
        /// If XRecord exists in dictionary, it is opened for Read and wrapped in XrecordReader.
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="tr"></param>
        /// <param name="entry"></param>
        /// <returns></returns>
        public static XrecordReader OpenXrecordReader(this DBDictionary dic, Transaction tr, string entry)
        {
            var xr = dic.OpenXrecord(tr, entry);
            if (xr != null)
                return new XrecordReader(xr);
            return null;
        }

        /// <summary>
        /// Opens existing Xrecord (creates new one if not found).
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="tr"></param>
        /// <param name="entry"></param>
        public static Xrecord OpenOrCreateXrecord(this DBDictionary dic, Transaction tr, string entry)
        {
            Xrecord rec = null;
            if (dic.Contains(entry))
            {
                rec = tr.GetObjectRead<Xrecord>(dic.GetAt(entry));
            }
            else
            {
                rec = new Xrecord();
                if (!dic.IsWriteEnabled)
                    dic.UpgradeOpen();
                dic.SetAt(entry, rec);
                tr.AddNewlyCreatedDBObject(rec, true);
            }
            return rec;
        }

        public static void SetXrecordValue<T>(this DBDictionary dic, Transaction tr, string entry, params T[] vals)
        {
            dic.SetXrecordData(tr, entry, vals.Select(x => CADTypedValueHelpers.Create(x)).ToArray());
        }

        /// <summary>
        /// Sets data to Xrecord (it is created if not found).
        /// </summary>
        public static void SetXrecordData(this DBDictionary dic, Transaction tr, string entry, params TypedValue[] vals)
        {
            dic.SetXrecordData(tr, entry, new ResultBuffer(vals));
        }
        /// <summary>
        /// Sets data to Xrecord (it is created if not found).
        /// </summary>
        public static void SetXrecordData(this DBDictionary dic, Transaction tr, string entry, ResultBuffer buffer)
        {
            var rec = dic.OpenOrCreateXrecord(tr, entry);
            rec.SetData(buffer);
        }

        /// <summary>
        /// If value is found, sets to provided parameter.
        /// Returns true if value is found. Otherwise false.
        /// </summary>
        /// <remarks>
        /// For getting multiple values from single entry, use XrecordReader.
        /// </remarks>
        /// <param name="index">Index in result buffer of requested value.</param>
        public static bool TryGetXrecordValue<T>(this DBDictionary dic, TransactionWrapper tw, string entry, ref T value, int index = 0)
        {
            var reader = dic.OpenXrecordReader(tw, entry);
            if (reader == null || reader.DataCount <= index)
                return false;
            var typedValue = reader[index];
            value = typedValue.GetValue<T>(tw.Db);
            return true;
        }

        /// <summary>
        /// Same as <see cref="TryGetXrecordValue"/> except it returns desired value
        /// or throws exception.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dic"></param>
        /// <param name="tw"></param>
        /// <param name="entry"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static T GetXrecordValue<T>(this DBDictionary dic, TransactionWrapper tw, string entry, int index = 0)
        {
            T temp = default(T);
            if (dic.TryGetXrecordValue(tw, entry, ref temp, index))
                return temp;
            throw new TypedValueException($"Unable to get TypedValue for  ({entry}) with index {index}");
        }


        /// <summary>
        /// Removes all entries not in the collection of usedEntries.
        /// (new entries are NOT created, even if found in usedEntries).
        /// </summary>
        public static void RemoveUnusedEntries(this DBDictionary dic, 
            TransactionWrapper tw,
            IEnumerable<string> usedEntries)
        {
            var hash = new HashSet<string>(usedEntries.Distinct());
            var persisted = dic.Cast<DBDictionaryEntry>().ToList();
            if (!dic.IsWriteEnabled)
                dic.UpgradeOpen();
            foreach(var t in persisted)
                if (!hash.Contains(t.Key))
                    dic.Remove(t.Key);
        }
    }
}
