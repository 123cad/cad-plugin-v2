﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if AUTOCAD
namespace Autodesk.AutoCAD.DatabaseServices
#endif

#if BRICSCAD
namespace Teigha.DatabaseServices
#endif
{
	public static class CADEntityExtensions
	{

		/// <summary>
		/// Saves provided data to extension dictionary of the entity.
		/// </summary>
		/// <param name="ent"></param>
		/// <param name="record"></param>
		/// <param name="name"></param>
		public static void SaveExtensionData(this Entity ent, Transaction tr, string name, params TypedValue[] val)
		{
			if (ent.ExtensionDictionary.IsNull)
				ent.CreateExtensionDictionary();
			ObjectId oid = ent.ExtensionDictionary;
			DBDictionary extDic = tr.GetObject(oid, OpenMode.ForWrite) as DBDictionary;
			Xrecord rec = new Xrecord();
			ResultBuffer buffer = new ResultBuffer(val);
			rec.Data = buffer;
			extDic.SetAt(name, rec);
			tr.AddNewlyCreatedDBObject(rec, true);
		}
		/// <summary>
		/// Appends value to ExtensionDictionaryEntry (if already exists, does nothing).
		/// </summary>
		/// <param name="ent"></param>
		/// <param name="tr"></param>
		/// <param name="name"></param>
		/// <param name="val"></param>
		public static void AppendExtensionDataValue(this Entity ent, Transaction tr, string name, TypedValue val)
		{
			if (ent.ExtensionDictionary.IsNull)
				ent.CreateExtensionDictionary();
			ObjectId oid = ent.ExtensionDictionary;
			DBDictionary extDic = tr.GetObject(oid, OpenMode.ForWrite) as DBDictionary;
			if (!extDic.Contains(name) || extDic.GetAt(name).IsNull)
				return;
			Xrecord rec = tr.GetObject(extDic.GetAt(name), OpenMode.ForWrite) as Xrecord;
			if (rec == null)
			{
				rec = new Xrecord();
				ResultBuffer buffer = new ResultBuffer();
				rec.Data = buffer;
				extDic.SetAt(name, rec);
				tr.AddNewlyCreatedDBObject(rec, true);
			}
			TypedValue[] values = rec.Data != null ? rec.Data.AsArray() : null;
			bool exists = false;
			if (values != null)
				foreach (TypedValue v in rec.Data.AsArray())
					if (val.Value == v.Value)
					{
						exists = true;
						break;
					}
			if (!exists)
			{
				if (values == null)
					values = new TypedValue[0];
				ResultBuffer buffer = new ResultBuffer(values);
				buffer.Add(val);
				rec.Data = buffer;
			}

		}
		/// <summary>
		/// Removes entries from ExtensionDictionary.
		/// </summary>
		/// <param name="ent"></param>
		/// <param name="tr"></param>
		/// <param name="names"></param>
		public static void RemoveFromExtensionData(this Entity ent, Transaction tr, params string[] names)
		{
			if (!ent.ExtensionDictionary.IsNull)
			{
				ObjectId extId = ent.ExtensionDictionary;
				DBDictionary extDic = tr.GetObject(extId, OpenMode.ForWrite) as DBDictionary;
				foreach (string name in names)
				{
					if (extDic.Contains(name))
						extDic.Remove(name);
				}
			}
		}
		/// <summary>
		/// Saves ObjectId handle to Extension dictionary, under provided name.
		/// </summary>
		/// <param name="ent"></param>
		/// <param name="id"></param>
		/// <param name="name"></param>
		public static void SaveExtensionDataObjectId(this Entity ent, Transaction tr, ObjectId id, string name)
		{
			TypedValue val = id.ToTypedValue();
			ent.SaveExtensionData(tr, name, val);
		}
		/// <summary>
		/// Saves string to extension dictionary under name.
		/// </summary>
		/// <param name="ent"></param>
		/// <param name="tr"></param>
		/// <param name="name"></param>
		/// <param name="value"></param>
		public static void SaveExtensionDataString(this Entity ent, Transaction tr, string name, string value)
		{
			TypedValue val = new TypedValue((int)DxfCode.Text, value);
			SaveExtensionData(ent, tr, name, val);

		}
		/// <summary>
		/// From ExtensionDictionary removes values under provided entry (by name). Entry is not removed.
		/// </summary>
		/// <param name="ent"></param>
		/// <param name="tr"></param>
		/// <param name="name"></param>
		/// <param name="value"></param>
		public static void RemoveExtensionDataValue(this Entity ent, Transaction tr, string name, TypedValue value)
		{
			if (ent.ExtensionDictionary.IsNull)
				ent.CreateExtensionDictionary();
			ObjectId oid = ent.ExtensionDictionary;
			DBDictionary extDic = tr.GetObject(oid, OpenMode.ForWrite) as DBDictionary;
			if (!extDic.Contains(name) || extDic.GetAt(name).IsNull)
				return;
			Xrecord rec = tr.GetObject(extDic.GetAt(name), OpenMode.ForWrite) as Xrecord;
			TypedValue[] existingValues = rec.Data.AsArray();
			List<TypedValue> keptValues = new List<TypedValue>();
			foreach (TypedValue val in existingValues)
			{
				//TEST (object equality)
				if (val.TypeCode != value.TypeCode || val.Value.GetHashCode() != value.Value.GetHashCode())
					keptValues.Add(val);
			}
			ResultBuffer buffer = new ResultBuffer(keptValues.ToArray());
			rec.Data = buffer;

		}
		/// <summary>
		/// Loads all entries from extension dcitionary for the entity.
		/// </summary>
		public static Dictionary<string, TypedValue[]> LoadExtensionData(this Entity ent, Transaction tr)
		{
			Dictionary<string, TypedValue[]> data = new Dictionary<string, TypedValue[]>();
			if (!ent.ExtensionDictionary.IsNull)
			{
				ObjectId extId = ent.ExtensionDictionary;
				DBDictionary extDic = tr.GetObject(extId, OpenMode.ForRead) as DBDictionary;
				foreach (DBDictionaryEntry entry in extDic)
				{
					if (entry.Value.IsNull)
						continue;
					Xrecord rec = tr.GetObject(entry.Value, OpenMode.ForRead) as Xrecord;
					if (rec == null)
						continue;
					ResultBuffer buffer = rec.Data;
					if (buffer == null)
						buffer = new ResultBuffer();
					data.Add(entry.Key, buffer.AsArray());
				}
			}

			return data;
		}
		/// <summary>
		/// Loads entries from extensions dictionary for provided names.
		/// </summary>
		public static Dictionary<string, TypedValue[]> LoadExtensionData(this Entity ent, Transaction tr, params string[] names)
		{
			Dictionary<string, TypedValue[]> data = new Dictionary<string, TypedValue[]>();
			if (!ent.ExtensionDictionary.IsNull)
			{
				ObjectId extId = ent.ExtensionDictionary;
				DBDictionary extDic = tr.GetObject(extId, OpenMode.ForRead) as DBDictionary;
				foreach (string name in names)
				{
					if (!extDic.Contains(name))
						continue;
					ObjectId oid = extDic.GetAt(name);
					if (oid.IsNull)
						continue;
					Xrecord rec = tr.GetObject(oid, OpenMode.ForRead) as Xrecord;
					if (rec == null)
						continue;
					ResultBuffer buffer = rec.Data;
					if (buffer != null)
						data.Add(name, buffer.AsArray());
				}
			}

			return data;
		}
		public static TypedValue? LoadExtensionDataSingle(this Entity ent, Transaction tr, string name, int index = 0)
		{
			var dic = LoadExtensionData(ent, tr, name);
			if (dic.ContainsKey(name) && dic[name].Length > index)
				return dic[name][index];
			return null;
		}
		/// <summary>
		/// Loads string from extension dictionary under specified name.
		/// </summary>
		/// <param name="ent"></param>
		/// <param name="tr"></param>
		/// <param name="name"></param>
		/// <returns></returns>
		public static string LoadExtensionString(this Entity ent, Transaction tr, string name)
		{
			var dic = LoadExtensionData(ent, tr, name);
			if (dic.ContainsKey(name))
			{
				TypedValue val = dic[name][0];
				string s = val.Value as string;
				return s;
			}
			return null;
		}
		/// <summary>
		/// Loads ObjectId from saved handle in extension dictionary. Returns null if not found.
		/// </summary>
		/// <param name="ent"></param>
		/// <param name="name"></param>
		/// <param name="typedValueIndex">Index of handle in typedvalue array.</param>
		/// <returns></returns>
		public static ObjectId? LoadFromExtensionDataObjectId(this Entity ent, Transaction tr, Database db, string name, int typedValueIndex = 0)
		{
			Dictionary<string, TypedValue[]> dic = ent.LoadExtensionData(tr, name);
			if (dic.ContainsKey(name))
			{
				if (dic[name].Length > typedValueIndex)
				{
					TypedValue val = dic[name][typedValueIndex];
					ObjectId id = val.GetObjectId(db);
					//if (id != ObjectId.Null)
					return id;
				}
			}
			return null;
		}
	}
}
