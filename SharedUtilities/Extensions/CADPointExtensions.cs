﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

#if AUTOCAD
namespace Autodesk.AutoCAD.DatabaseServices
#endif

#if BRICSCAD
namespace Teigha.DatabaseServices
#endif
{
	public static class CADPointExtensions
	{
		/// <summary>
		/// Distance between 2 points.
		/// </summary>
		public static double GetDistanceTo(this Point2d pt, MyUtilities.Geometry.Point2d pt2)
        {
			double x = pt.X - pt2.X;
			double y = pt.Y - pt2.Y;
			double d = Math.Sqrt(x * x + y * y);
			return d;
		}
		/// <summary>
		/// Distance between 2 points.
		/// </summary>
		public static double GetDistanceTo(this MyUtilities.Geometry.Point2d pt2, Point2d pt)
        {
			double x = pt.X - pt2.X;
			double y = pt.Y - pt2.Y;
			double d = Math.Sqrt(x * x + y * y);
			return d;
        }

		/// <summary>
		/// Distance between 2 points.
		/// </summary>
		public static double GetDistanceTo(this Point3d pt, MyUtilities.Geometry.Point3d pt2)
		{
			double x = pt.X - pt2.X;
			double y = pt.Y - pt2.Y;
			double z = pt.Z - pt2.Z;
			double d = Math.Sqrt(x * x + y * y + z * z);
			return d;
		}
		/// <summary>
		/// Distance between 2 points.
		/// </summary>
		public static double GetDistanceTo(this MyUtilities.Geometry.Point3d pt2, Point3d pt)
		{
			double x = pt.X - pt2.X;
			double y = pt.Y - pt2.Y;
			double z = pt.Z - pt2.Z;
			double d = Math.Sqrt(x * x + y * y + z * z);
			return d;
		}

		/// <summary>
		/// </summary>
		/// <param name="pt"></param>
		/// <returns></returns>
		public static Point2d GetAs2d(this Point3d pt)
		{
			return new Point2d(pt.X, pt.Y);
		}
		/// <summary>
		/// Returns Point3d on new height.
		/// </summary>
		/// <param name="pt"></param>
		/// <param name="z"></param>
		/// <returns></returns>
		public static Point3d GetOnZ(this Point3d pt, double z = 0)
		{
			return new Point3d(pt.X, pt.Y, z);
		}
		/// <summary>
		/// Returns as Point3d, with optional set Z height.
		/// </summary>
		/// <param name="pt"></param>
		/// <param name="z"></param>
		/// <returns></returns>
		public static Point3d GetAs3d(this Point2d pt, double z = 0)
		{
			return new Point3d(pt.X, pt.Y, z);
		}
	}
}
