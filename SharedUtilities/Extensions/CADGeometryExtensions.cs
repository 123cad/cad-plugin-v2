﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
namespace Teigha.Geometry
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
namespace Autodesk.AutoCAD.Geometry
#endif

//namespace CadPlugin.Common.Extensions
{
	public static class CADGeometryExtensions
	{
		/// <summary>
		/// Returns new point with provided z.
		/// </summary>
		/// <param name="pt"></param>
		/// <param name="z"></param>
		/// <returns></returns>
		public static Point3d GetWithHeight(this Point3d pt, double z = 0)
		{
			return new Point3d(pt.X, pt.Y, z);
		}

		/// <summary>
		/// Returns new Vector3d with new z coordinate.
		/// </summary>
		/// <param name="v"></param>
		/// <param name="z"></param>
		/// <returns></returns>
		public static Vector3d GetWithZ(this Vector3d v, double z = 0)
		{
			return new Vector3d(v.X, v.Y, z);
		}

		public static Point3d To3d(this Point2d p, double z = 0)
		{
			return new Point3d(p.X, p.Y, z);
		}
		public static Vector3d To3d(this Vector2d v, double z = 0)
		{
			return new Vector3d(v.X, v.Y, z);
		}
		public static Point2d To2d(this Point3d p)
		{
			return new Point2d(p.X, p.Y);
		}
		public static Vector2d To2d(this Vector3d v)
		{
			return new Vector2d(v.X, v.Y);
		}
	}
}
