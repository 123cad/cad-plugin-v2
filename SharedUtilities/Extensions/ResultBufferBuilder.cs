﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

#if AUTOCAD
namespace Autodesk.AutoCAD.DatabaseServices
#endif

#if BRICSCAD
namespace Teigha.DatabaseServices
#endif
{
	public class ResultBufferBuilder
	{
		private List<TypedValue> values = new List<TypedValue>();
		/// <summary>
		/// Number of items currently in buffer.
		/// </summary>
		public int CurrentCount => values.Count;

		public ResultBufferBuilder Add(params TypedValue[] vals)
		{
			values.AddRange(vals);
			return this;
		}
		public ResultBufferBuilder Add(params bool[] vals)
		{
			foreach (var b in vals)
			{
				TypedValue tv = CADTypedValueHelpers.Create(b);
				values.Add(tv);
			}
			return this;
		}
		public ResultBufferBuilder Add(params int[] vals)
		{
			foreach (int i in vals)
			{
				TypedValue tv = CADTypedValueHelpers.Create(i);
				values.Add(tv);
			}
			return this;
		}
		public ResultBufferBuilder Add(params double[] vals)
		{
			foreach (var d in vals)
			{
				TypedValue tv = CADTypedValueHelpers.Create(d);
				values.Add(tv);
			}
			return this;
		}
		public ResultBufferBuilder Add(params string[] vals)
		{
			foreach (var d in vals)
			{
				TypedValue tv = CADTypedValueHelpers.Create(d);
				values.Add(tv);
			}
			return this;
		}
		public ResultBufferBuilder Add(params Point2d[] vals)
		{
			foreach (var p in vals)
			{
				TypedValue tv = CADTypedValueHelpers.Create(p);
				values.Add(tv);
			}
			return this;
		}
		public ResultBufferBuilder Add(params Point3d[] vals)
		{
			foreach (var p in vals)
			{
				TypedValue tv = CADTypedValueHelpers.Create(p);
				values.Add(tv);
			}
			return this;
		}
		public ResultBufferBuilder Add(params ObjectId[] vals)
		{
			foreach (var oid in vals)
			{
				TypedValue tv = CADTypedValueHelpers.Create(oid);
				values.Add(tv);
			}
			return this;
		}
		public ResultBufferBuilder AddObjectIdNull(int repeatCount = 1)
		{
			Add(Enumerable.Repeat(ObjectId.Null, repeatCount).ToArray());
			return this;
		}
		/// <summary>
		/// Removes all values currently in buffer.
		/// </summary>
		/// <returns></returns>
		public ResultBuffer Clear()
		{
			values.Clear();
			return this;
		}
		public ResultBuffer GetResultBuffer()
		{
			return new ResultBuffer(values.ToArray());
		}
		/// <summary>
		/// Allow assignment of ResultBufferBuilder to ResultBuffer with runtime conversion.
		/// </summary>
		/// <param name="b"></param>
		public static implicit operator ResultBuffer(ResultBufferBuilder b)
		{
			return b.GetResultBuffer();
		}
	}
}
