﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

#if AUTOCAD
namespace Autodesk.AutoCAD.ApplicationServices
#endif

#if BRICSCAD
namespace Bricscad.ApplicationServices
#endif
{
    public static class DBObjectExstensions
    {
        /// <summary>
        /// If object is erased, it is unerased (with upgrading to write).
        /// </summary>
        /// <param name="obj"></param>
        public static void Unerase(this DBObject obj)
        {
            if (obj.IsErased)
            {
                if (!obj.IsWriteEnabled)
                    obj.UpgradeOpen();
                obj.Erase(false);
            }
        }
    }
}
