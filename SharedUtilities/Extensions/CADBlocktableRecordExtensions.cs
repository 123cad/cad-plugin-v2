﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if AUTOCAD
namespace Autodesk.AutoCAD.DatabaseServices
#endif

#if BRICSCAD
namespace Teigha.DatabaseServices
#endif
{
	public static class CADBlocktableRecordExtensions
	{

		/// <summary>
		/// Original AppendEntityCurrentLayer always sets layer to "0" (?!), and not the current layer.
		/// This one sets current layer.
		/// </summary>
		/// <param name="btr"></param>
		/// <param name="ent"></param>
		public static void AppendEntityCurrentLayer(this BlockTableRecord btr, Entity ent)
		{
			if (!btr.IsWriteEnabled)
				btr.UpgradeOpen();
			btr.AppendEntity(ent);
			ent.LayerId = btr.Database.Clayer;
		}
	}
}
