﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPlugin.Common;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
using System.Diagnostics;
#endif

#if AUTOCAD
namespace Autodesk.AutoCAD.DatabaseServices
#endif

#if BRICSCAD
namespace Teigha.DatabaseServices
#endif 
{
	public static class CADPolylineExtensions
	{
        #region Polyline
        /// <summary>
        /// Appends points to polyline, in the order given.
        /// </summary>
        public static void AddPoints(this Polyline pl, params Point2d[] pts)
        {
            foreach (Point2d p in pts)
                pl.AddVertexAt(pl.NumberOfVertices, p, 0, 0, 0);
        }
        /// <summary>
        /// Appends points to polyline, in the order given.
        /// </summary>
        public static void AddPoints(this Polyline pl, IEnumerable<Point2d> pts)
        {
            foreach (Point2d p in pts)
                pl.AddVertexAt(pl.NumberOfVertices, p, 0, 0, 0);
        }
        /// <summary>
        /// Appends points to polyline, in the order given.
        /// </summary>
        public static void AddPoints(this Polyline pl, params PolylineVertex<Point2d>[] pts)
        {
            foreach (var p in pts)
                pl.AddVertexAt(pl.NumberOfVertices, p.Point, p.Bulge, p.StartWidth, p.EndWidth);
        }
        /// <summary>
        /// Appends points to polyline, in the order given.
        /// </summary>
        public static void AddPoints(this Polyline pl, IEnumerable<PolylineVertex<Point2d>> pts)
        {
            foreach (var p in pts)
                pl.AddVertexAt(pl.NumberOfVertices, p.Point, p.Bulge, p.StartWidth, p.EndWidth);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="includeWidth">For optimization, we don't need width all the time.</param>
        /// <returns></returns>
        public static IEnumerable<PolylineVertex<Point2d>> GetVertices(this Polyline pl, bool includeWidth = true)
        {
            List<PolylineVertex<Point2d>> vertices = new List<PolylineVertex<Point2d>>();
            for (int i = 0; i < pl.NumberOfVertices; i++)
            {
                Point2d p = pl.GetPoint2dAt(i);
                double bulge = pl.GetBulgeAt(i);
                double startW = 0, endW = 0;
                if (i < pl.NumberOfVertices - 1)
                {
                    startW = pl.GetStartWidthAt(i);
                    endW = pl.GetEndWidthAt(i);
                }
                vertices.Add(new PolylineVertex<Point2d>(p, bulge, startW, endW));
            }
            return vertices;
        }
        /// <summary>
        /// Returns copy of points in Polyline.
        /// </summary>
        /// <param name="pl"></param>
        /// <returns></returns>
        public static IEnumerable<Point2d> GetPoints(this Polyline pl)
        {
            List<Point2d> points = new List<Point2d>();
            for (int i = 0; i < pl.NumberOfVertices; i++)
            {
                Point2d p = pl.GetPoint2dAt(i);
                points.Add(p);
            }
            return points;
        }

        /// <summary>
        /// Replaces existing points with provided ones.
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="pts"></param>
        public static void ReplacePoints(this Polyline pl, params Point2d[] pts)
        {
            if (pts.Count() < 2)
                throw new NotSupportedException("Polyline must have 2 points!");
            int oldCount = pl.NumberOfVertices;
            int currentLastIndex = pl.NumberOfVertices;
            pl.AddPoints(pts);
            for (int i = 0; i < oldCount; i++)
                pl.RemoveVertexAt(0);
        }
        /// <summary>
        /// Replaces existing points with provided ones.
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="pts"></param>
        public static void ReplacePoints(this Polyline pl, IEnumerable<Point2d> pts)
        {
            if (pts.Count() < 2)
                throw new NotSupportedException("Polyline must have 2 points!");
            int oldCount = pl.NumberOfVertices;
            int currentLastIndex = pl.NumberOfVertices;
            pl.AddPoints(pts);
            for (int i = 0; i < oldCount; i++)
                pl.RemoveVertexAt(0);
        }
        /// <summary>
        /// Replaces existing points with provided ones.
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="pts"></param>
        public static void ReplacePoints(this Polyline pl, params PolylineVertex<Point2d>[] pts)
        {
            if (pts.Count() < 2)
                throw new NotSupportedException("Polyline must have 2 points!");
            int oldCount = pl.NumberOfVertices;
            int currentLastIndex = pl.NumberOfVertices;
            pl.AddPoints(pts);
            for (int i = 0; i < oldCount; i++)
                pl.RemoveVertexAt(0);
        }
        /// <summary>
        /// Replaces existing points with provided ones.
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="pts"></param>
        public static void ReplacePoints(this Polyline pl, IEnumerable<PolylineVertex<Point2d>> pts)
        {
            if (pts.Count() < 2)
                throw new NotSupportedException("Polyline must have 2 points!");
            int oldCount = pl.NumberOfVertices;
            int currentLastIndex = pl.NumberOfVertices;
            pl.AddPoints(pts);
            for (int i = 0; i < oldCount; i++)
                pl.RemoveVertexAt(0);
        }

        public static bool IsPointOnPolyline(this Polyline pl, Point3d pt)
        {
            try
            {
                pl.GetParameterAtPoint(pt);
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
        #endregion

        #region Polyline2d
        /// <summary>
        /// Appends points to polyline, in the order given.
        /// </summary>
        public static void AppendPoints(this Polyline2d pl, params Point2d[] pts)
        {
            foreach (Point2d p in pts)
            {
                Vertex2d v = new Vertex2d(new Point3d(p.X, p.Y, 0), 0, 0, 0, 0);
                pl.AppendVertex(v);
                v.Dispose();
            }
        }
        /// <summary>
        /// Returns copy of points in Polyline2d.
        /// </summary>
        /// <param name="pl"></param>
        /// <returns></returns>
        public static IEnumerable<Point2d> GetPoints(this Polyline2d pl, Transaction tr)
        {
            List<Point2d> points = new List<Point2d>();
            foreach (ObjectId id in pl)
            {
                Vertex2d v = tr.GetObject(id, OpenMode.ForRead) as Vertex2d;
                Point2d p = new Point2d(v.Position.X, v.Position.Y);
                points.Add(p);
            }
            return points;
        }

        public static bool IsPointOnPolyline(this Polyline2d pl, Point3d pt)
        {
            try
            {
                pl.GetParameterAtPoint(pt);
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        #endregion

        #region Polyline3d
        /// <summary>
        /// Shorten up creating and disposing vertex.
        /// </summary>
        /// <param name="pl"></param>
        /// <returns></returns>
        public static ObjectId AppendVertexAndDispose(this Polyline3d pl)
        {
            PolylineVertex3d p3 = new PolylineVertex3d();
            ObjectId oid = pl.AppendVertex(p3);
            p3.Dispose();
            return oid;
        }
        /// <summary>
        /// Shorten up creating and disposing vertex.
        /// </summary>
        /// <param name="pl"></param>
        /// <returns></returns>
        private static bool warningShown = false;
        public static ObjectId AppendPoint(this Polyline3d pl, Point3d point)
        {
            if (pl.Database == null)
            {
#if AUTOCAD
                if (!warningShown)
                    Debug.Fail("Autocad will throw an exception if Polyline3d hasn't been added to database.");
#endif
                warningShown = true;
            }
            PolylineVertex3d p3 = new PolylineVertex3d(point);
            ObjectId oid = pl.AppendVertex(p3);
            p3.Dispose();
            return oid;
        }
        public static void AddPoints(this Polyline3d pl, params Point3d[] pts)
        {
            foreach (var p in pts)
                pl.AppendPoint(p);
        }
        /// <summary>
        /// Returns copy of all points in Polyline3d.
        /// </summary>
        /// <param name="pl"></param>
        /// <param name="tr"></param>
        /// <returns></returns>
        public static IEnumerable<Point3d> GetPoints(this Polyline3d pl, Transaction tr)
        {
            List<Point3d> points = new List<Point3d>();
            foreach (ObjectId id in pl)
            {
                PolylineVertex3d t = tr.GetObject(id, OpenMode.ForRead) as PolylineVertex3d;
                Point3d pt = t.Position;
                points.Add(pt);
            }
            return points;
        }
        public static void ReplacePoints(this Polyline3d pl, Transaction tr, params Point3d[] points)
        {
            List<PolylineVertex3d> vertices = new List<PolylineVertex3d>();
            foreach (ObjectId oid in pl)
                vertices.Add(tr.GetObjectWrite<PolylineVertex3d>(oid));
            pl.AddPoints(points);
            foreach (var v in vertices)
                v.Erase();
        }
        /// <summary>
        /// Creates offset entity, with preserving height of points.
        /// </summary>
        /// <returns></returns>
        public static Polyline3d CreateOffsetEntity(this Polyline3d pl, double offset, TransactionWrapper tr)
        {
            List<Point3d> pts3 = pl.GetPoints(tr).ToList();
            IList<Point3d> ptsNew = CADCommonHelper.CreateOffsetPoints(pts3, offset);// pts3.Select(x => new Point2d(x.X, x.Y)).ToList();

            Polyline3d plOffset = tr.CreateEntity<Polyline3d>();
            foreach (Point3d p in ptsNew)
            {
                plOffset.AppendPoint(p);
            }
            return plOffset;
        }
        public static void SetZ(this Polyline3d pl, Transaction tr, double z = 0)
        {
            foreach (ObjectId id in pl)
            {
                PolylineVertex3d t = tr.GetObject(id, OpenMode.ForWrite) as PolylineVertex3d;
                t.Position = new Point3d(t.Position.X, t.Position.Y, z);
            }
        }
        public static bool IsPointOnPolyline(this Polyline3d pl, Point3d pt)
        {
            try
            {
                pl.GetParameterAtPoint(pt);
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
