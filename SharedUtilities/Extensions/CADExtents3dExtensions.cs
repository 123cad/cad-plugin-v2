﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

#if AUTOCAD
namespace Autodesk.AutoCAD.DatabaseServices
#endif

#if BRICSCAD
namespace Teigha.DatabaseServices
#endif
{
	public static class CadPluginExtension
	{
		/// <summary>
		/// Converts Extents3d to 2d Rectangle (Z coordinate is ignored).
		/// </summary>
		/// <param name="ext"></param>
		/// <returns></returns>
		public static MyUtilities.Geometry.Rectangle<double> ToRectangle(this Extents3d ext)
		{
			MyUtilities.Geometry.Rectangle<double> rec = new MyUtilities.Geometry.Rectangle<double>();
			rec.X = ext.MinPoint.X;
			rec.Y = ext.MaxPoint.Y;
			rec.Width = ext.MaxPoint.X - ext.MinPoint.X;
			rec.Height = ext.MaxPoint.Y - ext.MinPoint.Y;
			return rec;

		}
	}
}
