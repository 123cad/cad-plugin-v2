﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

#if AUTOCAD
namespace Autodesk.AutoCAD.DatabaseServices
#endif

#if BRICSCAD
namespace Teigha.DatabaseServices
#endif
{
	public static class XrecordReaderExtensions
	{
		/// <summary>
		/// So support GetAll<T>.
		/// </summary>
		private static Dictionary<string, MethodInfo> typeMethods = null;
		static XrecordReaderExtensions()
		{
			// Get all methods except GetAll<T>, which is able to invoke specific method
			// depending on generic type.
			typeMethods = new Dictionary<string, MethodInfo>();
			Type t = typeof(XrecordReaderExtensions);
			var methods = t.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Static | BindingFlags.Public);
			foreach (var m in methods)
			{
				if (m.Name.StartsWith("GetAll"))
					continue;
				typeMethods.Add(m.ReturnType.Name, m);
			}
		}
		public static bool GetBool(this XrecordReader rd, int dataIndex)
		{
			if (rd.DataCount <= dataIndex)
				throw new MyExceptions.ArgumentException();
			return rd[dataIndex].GetBool();
		}
		public static int GetInt(this XrecordReader rd, int dataIndex)
		{
			if (rd.DataCount <= dataIndex)
				throw new MyExceptions.ArgumentException();
			return rd[dataIndex].GetInt();
		}
		public static double GetDouble(this XrecordReader rd, int dataIndex)
		{
			if (rd.DataCount <= dataIndex)
				throw new MyExceptions.ArgumentException();
			return rd[dataIndex].GetDouble();
		}
		public static string GetString(this XrecordReader rd, int dataIndex)
		{
			if (rd.DataCount <= dataIndex)
				throw new MyExceptions.ArgumentException();
			return rd[dataIndex].GetString();
		}
		public static Point2d GetPoint2d(this XrecordReader rd, int dataIndex)
		{
			if (rd.DataCount <= dataIndex)
				throw new MyExceptions.ArgumentException();
			return rd[dataIndex].GetPoint2d();
		}

		public static Point3d GetPoint3d(this XrecordReader rd, int dataIndex)
		{
			if (rd.DataCount <= dataIndex)
				throw new MyExceptions.ArgumentException();
			return rd[dataIndex].GetPoint3d();
		}
		public static ObjectId GetObjectId(this XrecordReader wr, int dataIndex)
		{
			return wr[dataIndex].GetObjectId(wr.Xrecord.Database);
		}
		public static IEnumerable<T> GetAllAs<T>(this XrecordReader rd)
		{
			Type t = typeof(T);
			if (!typeMethods.ContainsKey(t.Name))
				throw new MyExceptions.NotSupportedException($"Type {t.FullName} not supported for XrecordReader");
			MethodInfo method = typeMethods[t.Name];
            var list = new List<T>();
			for (int i = 0; i < rd.DataCount; i++)
				list.Add((T)(method.Invoke(null, new object[] { rd, i })));
            // Test on 500 int entries proved that no big difference is between this approach and invoking method directly.
            return list;
        }
	}
}
