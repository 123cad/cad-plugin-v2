﻿using MyUtilities.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 



#if AUTOCAD
using Autodesk.AutoCAD.DatabaseServices;
namespace Autodesk.AutoCAD.ApplicationServices
#endif

#if BRICSCAD
using Teigha.DatabaseServices;
namespace Bricscad.ApplicationServices
#endif
{
	public static class CADDocumentExtensions
	{
		/// <summary>
		/// Sets focus to the drawing.
		/// </summary>
		public static void SetFocus(this Document doc)
		{
			//SetForegroundWindow(Application.MainWindow.Handle);
			//SetFocus(doc.Window.Handle);
			//SetFocus(Application.MainWindow.Handle);
#if BRICSCAD
			Application.MainWindow.Focus();
			doc.Window.Focus();
#endif
			//SetForegroundWindow(Application.DocumentManager.MdiActiveDocument.Window.Handle);

#if AUTOCAD
					// "It is in acmgdinternal.dll, which you need to set reference to."
					Autodesk.AutoCAD.Internal.Utils.SetFocusToDwgView();
#endif
		}


		/// <summary>
		/// Starts transaction and wraps it into TransactionWrapper object.
		/// <para>Parameter indicates if Commit is automatically called on Dispose
		/// (no need for explicit invoking)</para>
		/// </summary>
		/// <param name="doc"></param>
		/// <returns></returns>
		public static TransactionWrapper StartTransaction(this Document doc, bool commitOnDispose = false)
		{
			var tr = doc.Database.TransactionManager.StartTransaction();
			return new TransactionWrapper(doc, tr)
			{
				CommitOnDispose = commitOnDispose
			};
		}

		/// <summary>
		/// Returns total number of Entities in the drawing.
		/// </summary>
		/// <param name="doc"></param>
		public static int GetEntitiesTotal(this Document doc)
		{
			using (TransactionWrapper tw = doc.StartTransaction())
			{
				int i = tw.CountEntities().TotalEntities;
				tw.Commit();
				return i;
			}
		}
		/// <summary>
		/// Returns total number of Entities in the drawing.
		/// </summary>
		/// <param name="doc"></param>
		public static ObjectsCounter GetEntitiesCounter(this Document doc)
		{
			using (TransactionWrapper tw = doc.StartTransaction())
			{
				var i = tw.CountEntities();
				tw.Commit();
				return i;
			}
		}
	}
}
