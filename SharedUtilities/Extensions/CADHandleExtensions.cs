﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

#if AUTOCAD
namespace Autodesk.AutoCAD.DatabaseServices
#endif

#if BRICSCAD
namespace Teigha.DatabaseServices
#endif
	
{
	
	public static class CadHandleHelpersExtensions
	{
		public static Handle? ConvertToHandle(this string s)
		{
			long l;
			if (long.TryParse(s, out l))
				return new Handle(l);
			return null;
		}
		/// <summary>
		/// Converts handle so it can be recreated later from the string.
		/// </summary>
		public static string ConvertToString(this Handle h)
		{
			return h.Value.ToString("x");
		}
		
	}
}
