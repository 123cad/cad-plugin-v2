﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

#if AUTOCAD
namespace Autodesk.AutoCAD.DatabaseServices
#endif

#if BRICSCAD
namespace Teigha.DatabaseServices
#endif
{
	public static class CADObjectIdExtensions
	{
		/// <summary>
		/// Stores ObjectId.Handle to TypedValue (it can be used in new sessions).
		/// </summary>
		public static TypedValue ToTypedValue(this ObjectId id)
		{
			return CADTypedValueHelpers.Create(id);

			//int handleCode = (int)DxfCode.Handle;
			//TypedValue val = new TypedValue(handleCode, id.Handle);
			//return val;

		}

		/// <summary>
		/// Opens object, erases it and disposes.
		/// <para>Opening object just to erase it causes "Forgot to call Dispose" 
		/// if not disposed.
		/// </para>
		/// </summary>
		public static void Erase(this ObjectId id)
        {
			using (var obj = id.GetObject(OpenMode.ForWrite))
				obj.Erase();
        }
	}
}
