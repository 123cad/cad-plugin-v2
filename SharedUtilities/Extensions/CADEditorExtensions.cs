﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using my = MyUtilities.Geometry;

#if AUTOCAD
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
namespace Autodesk.AutoCAD.ApplicationServices
#endif

#if BRICSCAD
using Teigha.DatabaseServices;
using Teigha;
using Bricscad.EditorInput;
using Teigha.Geometry;

namespace Bricscad.ApplicationServices
#endif
{
    public static class CADEditorExtensions
    {
        /// <summary>
        /// Clears all selected entities.
        /// </summary>
        /// <param name="editor"></param>
        public static void ClearSelection(this Editor editor)
        {
            editor.SetImpliedSelection(new ObjectId[0]);
        }

        /// <summary>
        /// World to convert between
        /// </summary>
        /// <param name="editor"></param>
        /// <returns></returns>
        public static my.ModelSpace3d GetViewCoordinateConverter(this Editor editor)
        {
            using(ViewTableRecord rec = editor.GetCurrentView())
            {
                Vector3d direction = rec.ViewDirection;

                // Right-handed coordinate system.
                Vector3d yAxis = -direction;
                Vector3d xAxis = yAxis.CrossProduct(Vector3d.ZAxis);
                if (xAxis.Length < 0.00001)
                    xAxis = Vector3d.XAxis;
                xAxis = xAxis.RotateBy(-rec.ViewTwist, direction);
                Vector3d zAxis = xAxis.CrossProduct(yAxis);

                my.ModelSpace3d world = new my.ModelSpace3d(xAxis.FromCADVector(), yAxis.FromCADVector(), zAxis.FromCADVector(), 
                    rec.Target.FromCADPoint());
                return world;
            }
        }

        /// <summary>
        /// Positions view so center is in the middle of the screen.
        /// </summary>
        /// <param name="ed"></param>
        /// <param name="center"></param>
        public static void SetViewCenter(this Editor ed, Point3d center)
        {
            using (ViewTableRecord rec = ed.GetCurrentView())
            {
                rec.Target = center;
                rec.CenterPoint = new Point2d();

                ed.SetCurrentView(rec);
            }
        }

        /// <summary>
        /// Zooms current view.
        /// </summary>
        /// <param name="ed"></param>
        /// <param name="zoomPercent">[-99, 99] 0 - no change. Positive means zoom in, negative zoom out.</param>
        public static void Zoom(this Editor ed, double zoomPercent = 0)
        {
            if (System.DoubleHelper.AreEqual(0, zoomPercent, 0.001))
                return;
            if (zoomPercent > 99)
                zoomPercent = 99;
            if (zoomPercent < -99)
                zoomPercent = -99;
            using (ViewTableRecord rec = ed.GetCurrentView())
            {
                double r = 100.0 - zoomPercent;
                double scale = rec.Height / rec.Width;
                rec.Height = rec.Height * zoomPercent;
                rec.Width = rec.Width * zoomPercent;

                rec.CenterPoint = new Point2d();

                ed.SetCurrentView(rec);
            }
        }


        public static void ZoomExtents(this Editor ed)
        {
            ed.Document.SendStringToExecute("ZOOM ", true, false, false);
            ed.Document.SendStringToExecute("_e ", true, false, false);
        }


        /// <summary>
        /// Returns current view display size (unrelated to UCS position).
        /// </summary>
        /// <returns></returns>
        public static my.Rectangle<double> GetViewRectangle(this Editor ed)
        {
            using (ViewTableRecord vtr = ed.GetCurrentView())
            {
                double width = vtr.Width;
                double height = vtr.Height;
                var pt = vtr.CenterPoint;
                var rec = new my.Rectangle<double>();
                rec.Height = height;
                rec.Width = width;
                rec.X = pt.X - width / 2;
                rec.Y = pt.Y + height / 2;
                return rec;
            }
        }
    }
}
