﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyExceptions.Exceptions;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

#if AUTOCAD
namespace Autodesk.AutoCAD.DatabaseServices
#endif

#if BRICSCAD
namespace Teigha.DatabaseServices
#endif
{
	public static partial class CADTypedValueHelpers
    {

        public static Dictionary<Type, DxfCode> Codes = new Dictionary<Type, DxfCode>
        {
            [typeof(bool)] = DxfCode.Bool,

            [typeof(int)] = DxfCode.Int32,
            [typeof(short)] = DxfCode.Int16,
            [typeof(byte)] = DxfCode.Int8,
            [typeof(long)] = DxfCode.Int64,

            [typeof(double)] = DxfCode.XReal,
            [typeof(float)] = DxfCode.XReal,

            [typeof(string)] = DxfCode.Text,

            [typeof(Point2d)] = DxfCode.XCoordinate,
            [typeof(Point3d)] = DxfCode.XCoordinate,

            [typeof(ObjectId)] = DxfCode.Handle,
        };

		public static TypedValue Create(bool b)
		{
			return new TypedValue((int)DxfCode.Bool, b);
		}
		public static TypedValue Create(int i)
		{
			return new TypedValue((int)DxfCode.Int32, i);
		}
		public static TypedValue Create(double d)
		{
			return new TypedValue((int)DxfCode.XReal, d);
		}
		public static TypedValue Create(string s)
		{
			return new TypedValue((int)DxfCode.Text, s);
		}
		public static TypedValue Create(Point2d pt)
		{
			return new TypedValue((int)DxfCode.XCoordinate, pt.To3d());
		}
		public static TypedValue Create(Point3d pt)
		{
			return new TypedValue((int)DxfCode.XCoordinate, pt);
		}
		public static TypedValue Create(ObjectId oid)
		{
			int handleCode = (int)DxfCode.Handle;
			TypedValue val = new TypedValue(handleCode, oid.Handle);
			return val;
		}

        public static TypedValue Create<T>(T val)
        {
            var t = typeof(T);
            object data = val;

            if (t.IsEnum)
            {
                // Convert Enum to int.
                int enumValue = (int)(object)val;
                data = enumValue;
                t = typeof(int);
            }

            if (!Codes.ContainsKey(t))
                throw new NotImplementedException($"Type {t.Name} is not added to CADTypedValueExtensions");

            var code = Codes[t];
            if (data is Point2d p)
                data = p.To3d();
            if (data is ObjectId id)
                data = id.Handle;

            var result = new TypedValue((int) code, data);
            return result;
        }
	}

	public static class CADTypedValueExtensions
	{
		public static bool GetBool(this TypedValue val)
		{
			if (val.TypeCode != (int)DxfCode.Bool)
				throw new TypeConversionException($"Not possible to convert value {val.Value.GetType().Name} to int");
			return (bool)val.Value;
		}
		public static int GetInt(this TypedValue val)
		{
			if (val.TypeCode != (int)DxfCode.Int32)
				throw new TypeConversionException($"Not possible to convert value {val.Value.GetType().Name} to int");
			return (int)val.Value;
		}
		public static double GetDouble(this TypedValue val)
		{
			if (val.TypeCode != (int)DxfCode.XReal)
				throw new TypeConversionException($"Not possible to convert value {val.Value.GetType().Name} to double");
			return (double)val.Value;
		}
		public static string GetString(this TypedValue val)
		{
			if (val.TypeCode != (int)DxfCode.Text)
				throw new TypeConversionException($"Not possible to convert value {val.Value.GetType().Name} to double");
			return (string)val.Value;
		}
		public static Point2d GetPoint2d(this TypedValue val)
		{
			if (val.TypeCode != (int)DxfCode.XCoordinate)
				throw new TypeConversionException($"Not possible to convert value {val.Value.GetType().Name} to Point2d");
			return GetPoint3d(val).GetAs2d();
		}
		public static Point3d GetPoint3d(this TypedValue val)
		{
			if (val.TypeCode != (int)DxfCode.XCoordinate)
				throw new TypeConversionException($"Not possible to convert value {val.Value.GetType().Name} to Point3d");
			return (Point3d)val.Value;
		}


		/// <summary>
		/// Returns ObjectId 
		/// </summary>
		public static ObjectId GetObjectId(this TypedValue val, Database db)
		{
			if (val.TypeCode != (int)DxfCode.Handle)
				throw new TypeConversionException($"Not possible to convert value {val.Value.GetType().Name} to Handle");
			Handle h = val.GetHandle();// GetHandle(val);
			ObjectId oid = db.GetObjectId(false, h, 0);
			return oid;
		}

		private static Handle GetHandle(this TypedValue val)
		{
#if AUTOCAD
			long l = Convert.ToInt64(val.Value.ToString(), 16);
			Handle h = new Handle(l);
#endif
#if BRICSCAD
			Handle h = (Handle)val.Value;
#endif
			return h;
		}


        /// <summary>
        /// Database is needed only for ObjectId type.
        /// </summary>
        public static T GetValue<T>(this TypedValue tv, Database db = null)
        {
            object result = null;
            if (tv.TypeCode == (int) CADTypedValueHelpers.Codes[typeof(ObjectId)])
            {
                if (db == null)
                    throw new NotSupportedException("Database is needed to fetch value from TypedValue for ObjectId");
                result = tv.GetObjectId(db);
            }
            else
            {
                result = tv.Value;
            }

            try
            {
                var found = CADTypedValueHelpers.Codes.Any(x => (short) x.Value == tv.TypeCode);
                if (!found)
                    throw new NotSupportedException($"TypeCode {tv.TypeCode} is not found in supported TypeCodes!");
                //if (!(result is T))
                    //throw new NotSupportedException($"Type of value and desired type are different!");
                return (T) result;
            }
            catch
            {
                string s = $"Unable to cast Value of type {tv.Value.GetType().Name} to type {typeof(T).Name}";
                MyLog.MyTraceSource.Error(s);
                throw new InvalidCastException(s);
            }

        }

    }
}
