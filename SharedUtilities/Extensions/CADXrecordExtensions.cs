﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

#if AUTOCAD
namespace Autodesk.AutoCAD.DatabaseServices
#endif

#if BRICSCAD
namespace Teigha.DatabaseServices
#endif
{
	public static class CADXrecordExtensions
	{
		/// <summary>
		/// Replaces existing data.
		/// </summary>
		/// <param name="rec"></param>
		/// <param name="vals"></param>
		public static Xrecord SetData(this Xrecord rec, params TypedValue[] vals)
		{
			if (!rec.IsWriteEnabled)
				rec.UpgradeOpen();
			rec.Data = new ResultBuffer(vals);
			return rec;
		}
		/// <summary>
		/// Replaces existing data.
		/// </summary>
		/// <param name="rec"></param>
		/// <param name="vals"></param>
		public static Xrecord SetData(this Xrecord rec, ResultBuffer buffer)
		{
			if (!rec.IsWriteEnabled)
				rec.UpgradeOpen();
			rec.Data = buffer;
			return rec;
		}

		/// <summary>
		/// Returns Reader which wraps access to data.
		/// </summary>
		/// <param name="rec"></param>
		/// <returns></returns>
		public static XrecordReader GetReader(this Xrecord rec)
		{
			return new XrecordReader(rec);
		}



	}
}
