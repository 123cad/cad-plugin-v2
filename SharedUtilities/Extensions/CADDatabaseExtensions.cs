﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

#if AUTOCAD
namespace Autodesk.AutoCAD.ApplicationServices
#endif

#if BRICSCAD
namespace Bricscad.ApplicationServices
#endif
{
    public static class CADDatabaseExtensions
    {
        /// <summary>
        /// Returns all entities from drawing in Read state.
        /// </summary>
        /// <param name="db"></param>
        /// <param name="tr"></param>
        /// <returns></returns>
        public static IEnumerable<Entity> GetAllEntities(this Database db, Transaction tr)
        {
            List<Entity> entites = new List<Entity>();
            BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
            BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
            foreach (var oid in btr)
                entites.Add(tr.GetObjectRead<Entity>(oid));
            return entites;
        }
        /// <summary>
        /// Returns all ObjectIds from the drawing.
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static IEnumerable<ObjectId> GetAllEntities(this Database db)
        {
            List<ObjectId> oids = new List<ObjectId>();
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                foreach(ObjectId oid in btr)
                {
                    oids.Add(oid);
                }
                tr.Commit();
            }
            return oids;
        }

        /// <summary>
        /// Returns all entities of type T (subtype of Entity), in Read state.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="db"></param>
        /// <param name="tr"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetAllEntities<T>(this Database db, Transaction tr) where T:Entity
        {
            List<T> entites = new List<T>();
            BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
            BlockTableRecord btr = tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
            foreach (var oid in btr)
            {
                var t = tr.GetObjectRead<Entity>(oid);
                var p = t as T;
                if (p != null)
                    entites.Add(p);
            }
            return entites;
        }

        public static void DeleteAllEntities(this Database db)
        {
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                foreach (Entity e in GetAllEntities(db, tr))
                {
                    e.UpgradeOpen();
                    e.Erase();
                }

                tr.Commit();
            }

        }


    }
}
