﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

#if AUTOCAD
namespace Autodesk.AutoCAD.DatabaseServices
#endif

#if BRICSCAD
namespace Teigha.DatabaseServices
#endif
{
	public static class CADTransactionExtensions
	{
		/// <summary>
		/// Creates entity and adds it to database.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="tr"></param>
		/// <param name="modelSpace"></param>
		/// <returns></returns>
		public static T CreateEntity<T>(this Transaction tr, BlockTableRecord modelSpace, string layerName = null) where T : Entity
		{
			//NOTE This should be done better, without enum - in more OO way.
			T val;
			try
			{
				val = Activator.CreateInstance<T>();
				if (!modelSpace.IsWriteEnabled)
					modelSpace.UpgradeOpen();
				modelSpace.AppendEntityCurrentLayer(val);
				tr.AddNewlyCreatedDBObject(val, true);
			}
			catch (System.Exception)
            {
				throw new NotSupportedException($"Creating entity of type: {typeof(T).FullName} is not supported!");
			}
			if (!string.IsNullOrEmpty(layerName) && val != null)
			{
				val.Layer = layerName;
			}
			return val;
		}

		/// <summary>
		/// Gets object in read mode.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="tr"></param>
		/// <param name="id"></param>
		/// <returns></returns>
		public static T GetObjectRead<T>(this Transaction tr, ObjectId id, bool openErased = false) where T : DBObject
		{
			T e = tr.GetObject(id, OpenMode.ForRead, openErased) as T;
			return e;
		}
		/// <summary>
		/// Gets object in write mode.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="tr"></param>
		/// <param name="id"></param>
		/// <returns></returns>
		public static T GetObjectWrite<T>(this Transaction tr, ObjectId id, bool openErased = false) where T : class
		{
			DBObject e = tr.GetObject(id, OpenMode.ForWrite, openErased) as DBObject;
			return e as T;
		}



		/// <summary>
		/// Helper method. Creates xrecord and adds it to the entry.
		/// </summary>
		public static Xrecord CreateXRecord(this Transaction tr, DBDictionary entry, string key)
		{
			Xrecord rec = new Xrecord();
			entry.SetAt(key, rec);
			tr.AddNewlyCreatedDBObject(rec, true);
			return rec;
		}
		/// <summary>
		/// Helper method. Creates dbdictionary and adds it to the entry.
		/// </summary>
		public static DBDictionary CreateDBDictionary(this Transaction tr, DBDictionary entry, string key)
		{
			DBDictionary dic = new DBDictionary();
			entry.SetAt(key, dic);
			tr.AddNewlyCreatedDBObject(dic, true);
			return dic;
		}

	}
}
