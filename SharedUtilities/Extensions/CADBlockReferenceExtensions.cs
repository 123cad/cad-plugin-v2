﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtilities.Helpers;

#if BRICSCAD
using Bricscad.ApplicationServices;
using Teigha.Runtime;
using Teigha.DatabaseServices;
using Teigha.Geometry;
using Bricscad.EditorInput;
using Teigha.Colors;
#endif

#if AUTOCAD
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;
#endif

#if AUTOCAD
namespace Autodesk.AutoCAD.ApplicationServices
#endif

#if BRICSCAD
namespace Bricscad.ApplicationServices
#endif
{

    /// <summary>
    /// Parameters passed for rotation of a Block.
    /// </summary>
    public class BlockRotationParameters
    {
        private enum RotationType
        {
            ByAngle,
            ToAngle,
            ToDirectionX
        }

        public bool RotateAttributes{ get; set; }
        public bool RotateSymbol{ get; set; }

        private double angleDeg{ get; set; }
        private Vector2d directionX { get; set; }
        private RotationType rotationType{ get; }

        private BlockRotationParameters(RotationType t)
        {
            rotationType = t;
        }

        /// <summary>
        /// Rotates Block by specified angle.
        /// </summary>
        public static BlockRotationParameters CreateRotationBy(double angleDeltaDeg, 
            bool rotateSymbol = true, bool rotateAttributes = true)
        {
            var p = new BlockRotationParameters(RotationType.ByAngle)
            {
                RotateSymbol = rotateSymbol,
                RotateAttributes = rotateAttributes
            };
            p.angleDeg = angleDeltaDeg;
            return p;
        }

        /// <summary>
        /// Sets new angle for the block.
        /// </summary>
        public static BlockRotationParameters CreateRotateTo(double newAngleDeg,
            bool rotateSymbol = true, bool rotateAttributes = true)
        {
            var p = new BlockRotationParameters(RotationType.ToAngle)
            {
                RotateSymbol = rotateSymbol,
                RotateAttributes = rotateAttributes
            };
            p.angleDeg = newAngleDeg;
            return p;
        }

        public static BlockRotationParameters CreateAlignDirection(Vector2d directionX,
            bool rotateSymbol = true, bool rotateAttributes = true)
        {
            var p = new BlockRotationParameters(RotationType.ToDirectionX)
            {
                RotateSymbol = rotateSymbol,
                RotateAttributes = rotateAttributes
            };
            p.directionX = directionX;
            return p;
        }

        /// <summary>
        /// Returns angle by which Block needs to be rotated.
        /// </summary>
        public double GetRotateByAngleRad(BlockReference br)
        {
            double resultRad = 0;
            double angleRad = MyUtilities.Helpers.Calculations.DegreesToRadians(angleDeg);
            switch (rotationType)
            {
                case RotationType.ByAngle:
                    resultRad = angleRad;
                    break;
                case RotationType.ToAngle:
                    resultRad = angleRad - br.Rotation;
                    break;
                case RotationType.ToDirectionX:
                    double newAngleRad = new MyUtilities.Geometry.Vector2d(1, 0).GetAngleRad(directionX.FromCADVector());
                    newAngleRad += newAngleRad < 0 ? Math.PI * 2 : 0;
                    resultRad = newAngleRad - br.Rotation;
                    break;
            }

            return resultRad;
        }
    }

    public static class CADBlockReferenceExtensions
    {
        /// <summary>
        /// Setting rotation angle to provided angleDeg.
        /// </summary>
        public static void Rotate(this BlockReference br, TransactionWrapper tw, BlockRotationParameters rParams)
        {
            if (!br.IsWriteEnabled)
                br.UpgradeOpen();

            double deltaAngleRad = rParams.GetRotateByAngleRad(br);
            if (rParams.RotateAttributes && rParams.RotateSymbol)
            {
                br.Rotation += deltaAngleRad;
                RotateAttributesBy(tw, br, deltaAngleRad);
            }
            else if (rParams.RotateSymbol)
            {
                // Keep attributes in place.
                br.Rotation += deltaAngleRad;
            }
            else if (rParams.RotateAttributes)
            {
                RotateAttributesBy(tw, br, deltaAngleRad);
            }
        }

        private static void RotateAttributesBy(TransactionWrapper tw, BlockReference br, double deltaAngleRad)
        {
            Point3d topLeft = new Point3d(); 
            var attributes = new List<Tuple<AttributeReference, Vector3d>>();
            bool first = true;
            Vector3d reference = new Vector3d();
            foreach (ObjectId id in br.AttributeCollection)
            {
                var ar = tw.GetObjectRead<AttributeReference>(id);
                if (first)
                {
                    first = false;
                    topLeft = ar.Position;
                    reference = topLeft.GetAsVector().Negate();
                }

                var offset = reference.Add(ar.Position.GetAsVector());
                attributes.Add(new Tuple<AttributeReference, Vector3d>(ar, offset));

            }

            if (attributes.Any())
            {
                var rotationMatrix = Matrix3d.Rotation(deltaAngleRad, Vector3d.ZAxis, br.Position);
                foreach (ObjectId id in br.AttributeCollection)
                {
                    var ar = tw.GetObjectWrite<AttributeReference>(id);
                    ar.TransformBy(rotationMatrix);
                }
            }
        }

    }
}
