﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class ExceptionExtension
{
    /// <summary>
    /// Converts Exception to string, with data appended.
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    public static string ToStringData(this Exception e)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine(e.Message);
        foreach (KeyValuePair<object, object> d in e.Data)
            sb.AppendLine($"\t{d.Key} : {d.Value}");
        return sb.ToString();
    }
}
