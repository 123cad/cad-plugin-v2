﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtilities.Helpers;

namespace MyExceptions.Exceptions
{
    public class TypeConversionException : Exception
    {
        public TypeConversionException(string msg):base(msg)
        {

        }
    }
}
