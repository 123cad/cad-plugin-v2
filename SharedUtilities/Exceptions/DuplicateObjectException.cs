﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtilities.Helpers;

namespace MyExceptions
{
    public class DuplicateObjectException : Exception
    {
        public DuplicateObjectException(string msg):base(msg)
        {

        }
    }
}
