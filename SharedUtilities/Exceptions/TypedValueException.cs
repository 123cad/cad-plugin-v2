﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtilities.Helpers;

namespace MyExceptions
{
    public class TypedValueException : Exception
    {
        public TypedValueException(string msg):base(msg)
        {

        }
    }
}
