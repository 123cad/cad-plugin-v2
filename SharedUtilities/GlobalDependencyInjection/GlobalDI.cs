﻿using GlobalDependencyInjection;
using Ninject;
using Ninject.Modules;
using Ninject.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedUtilities.GlobalDependencyInjection
{
    /// <summary>
    /// Keeps Kernel for DI in global scope.
    /// All modules can use it.
    /// </summary>
    /// <remarks>
    /// Autoload won't work in 2 reasons:
    ///     1 - not all assemblies are loaded at this point
    ///     2 - iterating through all types will trigger loading of all types and assemblies
    ///         in assembly.
    /// </remarks>
    public class GlobalDI
    {
        public static IKernel Kernel { get; private set; } = new StandardKernel();

        /// <summary>
        /// Loads new module.
        /// This should be done as soon as possible (best is when loading plugin - 
        /// no classes will be loaded until then (except those referenced in Plugin class)).
        /// </summary>
        /// <param name="module"></param>
        public static void LoadModule<T>() where T : BaseDIModule
        {
            Kernel.Load((BaseDIModule)Activator.CreateInstance(typeof(T)));
        }

        /// <summary>
        /// Loads new module.
        /// Use this for modules which contain types from CAD assemblies.
        /// </summary>
        /// <remarks>Needed because for testing we can use fake types - so it can run without CAD infrastructure.</remarks>
        /// <param name="module"></param>
        public static void LoadModuleCad<T>() where T : BaseDiModuleCad
        {
            var t = typeof(T);
            if (Kernel.HasModule(t.Name))
                return;
            Kernel.Load((BaseDiModuleCad)Activator.CreateInstance(t));
        }

        public static T Get<T>()
        {
            T v = default(T);

            // Bricscad throws NotSupported exception on first invoke of
            // DI. But everything seem to work.
            // Bricscad.ApplicationServices.AssemblyLoader.OnLoad
            v = Kernel.Get<T>();
            return v;
        }
        public static T Get<T>(params IParameter[] parameters)
        {
            return Kernel.Get<T>(parameters);
        }
    }
}
