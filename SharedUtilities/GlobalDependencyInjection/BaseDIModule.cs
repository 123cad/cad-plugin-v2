﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalDependencyInjection
{
    /// <summary>
    /// Base modules for all modules in current plugin.
    /// </summary>
    public abstract class BaseDIModule : NinjectModule
    {
        public sealed override void Load()
        {
            LoadModule();
        }
        /// <summary>
        /// Types which do not reference CAD assemblies.
        /// </summary>
        protected abstract void LoadModule();
    }
}
namespace GlobalDependencyInjection
{
    #if BRICSCAD
    using Bricscad.ApplicationServices;
    using Teigha.Runtime;
    using Teigha.DatabaseServices;
    using Teigha.Geometry;
    using Bricscad.EditorInput;
    using Teigha.Colors;
    #endif

    #if AUTOCAD
    using Autodesk.AutoCAD.Runtime;
    using Autodesk.AutoCAD.ApplicationServices;
    using Autodesk.AutoCAD.DatabaseServices;
    using Autodesk.AutoCAD.Geometry;
    using Autodesk.AutoCAD.EditorInput;
    using Autodesk.AutoCAD.Colors;
    #endif
    /// <summary>
    /// For types which reference types in CAD assemblies.
    /// </summary>
    public abstract class BaseDiModuleCad:NinjectModule
    {
        public sealed override void Load()
        {
            LoadModule();
        }
        protected abstract void LoadModule();

    }

}
