﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CadPlugin.Sewage.Create.Settings;
using CadPlugin.Sewage.Plannings;
using CadPlugin.Sewage.Planning;
using MyUtilities.Geometry;
using CadPlugin.Sewage.Planning.PipeShaftDataManagers;
using CadPlugin.Sewage.Planning.Calculations.OffsetCalculations;
using CadPlugin.Sewage.Planning.Calculations.SegmentDecoraters;

namespace CadPlugin_Test.Planning
{
	[TestClass]
	public class PipeShaftDataManager_Test
	{
		private double precision = 0.001;
		private PipeShaftDataManager manager;
		[TestInitialize]
		public void Prepare()
		{

			manager = new DownstreamManager();

			PipeShaftData data = (PipeShaftData)manager.Data;
			data.ToggleStartShaftCreate(true);
			data.ToggleEndShaftCreate(true);
			data.StartShaftData.CoverHeight = 12.5;
			data.StartShaftData.TerrainHeight = 10.3;
			data.StartShaftData.PipeOutHeight = 8;
			data.StartShaftData.BottomHeight = 5.6;
		}
		[TestMethod]
		public void UpdateStartShaft_Test()
		{
			IPipeShaftData data = manager.Data;
			IShaftData shaft = data.StartShaft;
			// Original value.
			double old = 0;
			// New value.
			double val = 0;
			// Delta - to return to original value.
			double delta = 0;

			// Cover
			old = shaft.CoverHeight;
			val = 13.1;
			delta = shaft.CoverHeight - shaft.TerrainHeight;
			manager.UpdateStartShaft(val, ShaftFields.Cover);
			Assert.AreEqual(val, shaft.CoverHeight, precision);
			manager.UpdateStartShaft(delta, ShaftFields.CoverTerrainDelta);
			Assert.AreEqual(old, shaft.CoverHeight, precision);

			// Terrain
			old = shaft.TerrainHeight;
			val = 11.256;
			double t = shaft.TerrainHeight - shaft.BottomHeight;
			manager.UpdateStartShaft(val, ShaftFields.Terrain);
			Assert.AreEqual(val, shaft.TerrainHeight, precision);
			Assert.AreEqual(shaft.TerrainHeight - shaft.BottomHeight, t, precision);
			manager.UpdateStartShaft(old, ShaftFields.Terrain);


			// Pipe out
			old = shaft.PipeOutHeight;
			val = 8.578;
			delta = shaft.TerrainHeight - shaft.PipeOutHeight;
			t = shaft.PipeOutHeight - shaft.BottomHeight;
			manager.UpdateStartShaft(val, ShaftFields.PipeOut);
			Assert.AreEqual(val, shaft.PipeOutHeight, precision);
			Assert.AreEqual(shaft.PipeOutHeight - shaft.BottomHeight, t, precision);
			manager.UpdateStartShaft(delta, ShaftFields.TerrainPipeOutDelta);
			Assert.AreEqual(old, shaft.PipeOutHeight, precision);
			Assert.AreEqual(shaft.PipeOutHeight - shaft.BottomHeight, t, precision);

		}
		[TestMethod]
		public void UpdateEndShaft_Test()
		{
			IPipeShaftData data = manager.Data;
			IShaftData shaft = data.EndShaft;
			// Original value.
			double old = 0;
			// New value.
			double val = 0;
			// Delta - to return to original value.
			double delta = 0;

			// Cover
			old = shaft.CoverHeight;
			val = 13.1;
			delta = shaft.CoverHeight - shaft.TerrainHeight;
			manager.UpdateEndShaft(val, ShaftFields.Cover);
			Assert.AreEqual(val, shaft.CoverHeight, precision);
			manager.UpdateEndShaft(delta, ShaftFields.CoverTerrainDelta);
			Assert.AreEqual(old, shaft.CoverHeight, precision);

			// Terrain
			old = shaft.TerrainHeight;
			val = 11.256;
			double t = shaft.TerrainHeight - shaft.BottomHeight;
			manager.UpdateEndShaft(val, ShaftFields.Terrain);
			Assert.AreEqual(val, shaft.TerrainHeight, precision);
			Assert.AreEqual(shaft.TerrainHeight - shaft.BottomHeight, t, precision);
			manager.UpdateEndShaft(old, ShaftFields.Terrain);


			// Pipe out
			old = shaft.PipeOutHeight;
			val = 8.578;
			delta = shaft.TerrainHeight - shaft.PipeOutHeight;
			t = shaft.PipeOutHeight - shaft.BottomHeight;
			manager.UpdateEndShaft(val, ShaftFields.PipeOut);
			Assert.AreEqual(val, shaft.PipeOutHeight, precision);
			Assert.AreEqual(shaft.PipeOutHeight - shaft.BottomHeight, t, precision);
			manager.UpdateEndShaft(delta, ShaftFields.TerrainPipeOutDelta);
			Assert.AreEqual(old, shaft.PipeOutHeight, precision);
			Assert.AreEqual(shaft.PipeOutHeight - shaft.BottomHeight, t, precision);

		}

		#region No segments (NS) tests 

		private double fixedTerrainHeight = 2.5;
		private double fixedTerrainPipeOutDelta = 1;
		private PipeShaftDataManager GetManager_NS_fixedTerrain_minSlope()
		{
			PipeShaftDataManager m = new DownstreamManager();
			PipeShaftData data = (PipeShaftData)m.Data;
			data.ToggleStartShaftCreate(true);
			data.ToggleEndShaftCreate(true);
			data.StartShaftData.CoverHeight = 1.6;
			data.StartShaftData.TerrainHeight = 1.5;
			data.StartShaftData.PipeOutHeight = 0.5;
			data.StartShaftData.BottomHeight = 0;

			data.EndShaftData.CoverHeight = 1.4;
			data.EndShaftData.TerrainHeight = 1.5;
			data.EndShaftData.PipeOutHeight = data.EndShaftData.TerrainHeight - fixedTerrainPipeOutDelta;
			data.EndShaftData.CoverHeight = 0.25;

			data.PipeData.SegmentsUsed = false;
			data.MinPipeSlope = 0.05;

			data.ApplyMinPipeSlope = true;
			//data.ApplyMinDeltaShaftTerrainPipeOut = false;

			m.TerrainHeightCalculator = new PointTerrainHeight(new Point3d(0, 0, fixedTerrainHeight));
			m.Calculation = new OffsetDownstream(data.StartShaft.Radius, data.EndShaft.Radius);

			return m;
		}
		private PipeShaftDataManager GetManager_NS_fixedTerrain_minDelta()
		{
			PipeShaftDataManager m = new DownstreamManager();
			PipeShaftData data = (PipeShaftData)m.Data;
			data.ToggleStartShaftCreate(true);
			data.ToggleEndShaftCreate(true);
			data.StartShaftData.CoverHeight = 1.6;
			data.StartShaftData.TerrainHeight = 1.5;
			data.StartShaftData.PipeOutHeight = 0.5;
			data.StartShaftData.BottomHeight = 0;

			data.EndShaftData.CoverHeight = 1.4;
			data.EndShaftData.TerrainHeight = 1.5;
			data.EndShaftData.PipeOutHeight = data.EndShaftData.TerrainHeight - fixedTerrainPipeOutDelta;
			data.EndShaftData.CoverHeight = 0.25;

			data.PipeData.SegmentsUsed = false;
			data.PipeData.Slope = 0.05;
			((ShaftData)data.EndShaft).MinPipeOutDelta = 1.6;

			data.ApplyMinPipeSlope = false;
			((ShaftData)data.EndShaft).MinPipeOutDeltaUsed = true;

			m.TerrainHeightCalculator = new PointTerrainHeight(new Point3d(0, 0, fixedTerrainHeight));
			m.Calculation = new OffsetDownstream(data.StartShaft.Radius, data.EndShaft.Radius);

			return m;
		}
		private PipeShaftDataManager GetManager_NS_fixedTerrain_minSlopeAndDelta()
		{
			PipeShaftDataManager m = new DownstreamManager();
			PipeShaftData data = (PipeShaftData)m.Data;
			data.ToggleStartShaftCreate(true);
			data.ToggleEndShaftCreate(true);
			data.StartShaftData.CoverHeight = 1.6;
			data.StartShaftData.TerrainHeight = 1.5;
			data.StartShaftData.PipeOutHeight = 0.5;
			data.StartShaftData.BottomHeight = 0;

			data.EndShaftData.CoverHeight = 1.4;
			data.EndShaftData.TerrainHeight = 1.5;
			data.EndShaftData.PipeOutHeight = data.EndShaftData.TerrainHeight - fixedTerrainPipeOutDelta;
			data.EndShaftData.CoverHeight = 0.25;

			data.PipeData.SegmentsUsed = false;
			data.PipeData.Slope = 0.05;
			((ShaftData)data.EndShaft).MinPipeOutDelta= 1.6;
			data.MinPipeSlope = 0.05;

			data.ApplyMinPipeSlope = true;
			((ShaftData)data.EndShaft).MinPipeOutDeltaUsed = true;

			m.TerrainHeightCalculator = new PointTerrainHeight(new Point3d(0, 0, fixedTerrainHeight));
			m.Calculation = new OffsetDownstream(data.StartShaft.Radius, data.EndShaft.Radius);

			return m;
		}

		#region SetStartPoint
		[TestMethod]
		public void SetStartPoint_NS_Test1()
		{
			PipeShaftDataManager mgr = GetManager_NS_fixedTerrain_minSlope();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			Point3d mouse = new Point3d(1, 2, 5);
			mgr.SetStartPoint(mouse);


			Assert.AreEqual(new Point3d(1, 2, 4), data.StartPoint, "start point");
			Assert.AreEqual(5.1, data.StartShaft.CoverHeight, "cover");
			Assert.AreEqual(5, data.StartShaft.TerrainHeight, "terrain");
			Assert.AreEqual(4, data.StartShaft.PipeOutHeight, "pipe out");
			Assert.AreEqual(3.5, data.StartShaft.BottomHeight, "bottom");
		}
		[TestMethod, ExpectedException(typeof(NotSupportedException))]
		public void SetStartPoint_NS_Test2()
		{
			PipeShaftDataManager mgr = GetManager_NS_fixedTerrain_minSlope();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			Point3d mouse = new Point3d(1, 2, 5);
			mgr.SetStartPoint(mouse);

			// Move only if end point has been set.
			mgr.MoveNextShaft();
		}
		#endregion
		#region SetEndPoint
		/*
		 *  Min slope
		 */

		[TestMethod]
		public void SetEndPoint_NS_minSlope_fixedTerrain_Test1()
		{
			PipeShaftDataManager mgr = GetManager_NS_fixedTerrain_minSlope();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			Point3d startPoint = new Point3d(1, 2, 5);
			mgr.SetStartPoint(startPoint);

			Point3d mouse = new Point3d(10, 2, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			// Start point represents shaft pipe out height - that is why it is 4 instead of 5.
			Assert.AreEqual(new Point3d(1, 2, 4), data.StartShaftData.CenterPipeOutPoint, "start shaft pipe out point.");
			Assert.AreEqual(new Point3d(1.5, 2, 4), data.Pipe.StartPoint, "pipe start point.");
			Assert.AreEqual(new Point2d(10, 2), data.EndPoint.GetAs2d(), "end point.");
			Assert.AreEqual(new Point2d(9.5, 2), data.PipeData.EndPoint.GetAs2d(), "pipe end point.");

			Assert.AreEqual(0.4, data.StartShaft.PipeOutHeight - data.EndShaft.PipeOutHeight - data.EndShaft.InnerSlopeDeltaH, 0.0001, "pipe out delta");
		}
		[TestMethod]
		public void SetEndPoint_NS_minSlope_fixedTerrain_angleStep_Test1()
		{
			PipeShaftDataManager mgr = GetManager_NS_fixedTerrain_minSlope();
			PipeShaftData data = (PipeShaftData)mgr.Data;
			data.PipeData.AngleSteppingUsed = true;
			data.PipeData.AngleStepDeg = 30;

			Point3d startPoint = new Point3d(0, 2, 5);
			mgr.SetStartPoint(startPoint);
			double slope = data.MinPipeSlope;
			// to make sure pipe out is not changed from first shaft.
			data.MinPipeSlope = 0;
			mgr.SetEndPoint(new Point3d(11, 2, 5));
			mgr.Recalculate();
			mgr.MoveNextShaft();
			data.MinPipeSlope = slope;

			// 30 deg step. y is moved from 2 to 1, so small angle exists - but it should be rounded to 0.
			Point3d mouse = new Point3d(19.944272, 1, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			// z is not 4 but 3.975. pipe slope is 0, but there is inner shaft slope.
			Assert.AreEqual(new Point3d(11, 2, 3.975), data.StartShaftData.CenterPipeOutPoint, "start shaft pipe out point.");
			Assert.AreEqual(new Point3d(11.5, 2, 3.975), data.Pipe.StartPoint, "pipe start point.");
			Assert.AreEqual(new Point2d(20, 2), data.EndPoint.GetAs2d(), "end point.");
			Assert.AreEqual(new Point2d(19.5, 2), data.PipeData.EndPoint.GetAs2d(), "pipe end point.");

			Assert.AreEqual(0.4, data.StartShaft.PipeOutHeight - data.EndShaft.PipeOutHeight - data.EndShaft.InnerSlopeDeltaH, 0.0001, "pipe out delta");
		}
		[TestMethod]
		public void SetEndPoint_NS_minSlope_fixedTerrain_angleStep_Test2()
		{
			PipeShaftDataManager mgr = GetManager_NS_fixedTerrain_minSlope();
			PipeShaftData data = (PipeShaftData)mgr.Data;
			data.PipeData.AngleSteppingUsed = true;
			data.PipeData.AngleStepDeg = 5;

			Point3d startPoint = new Point3d(0, 2, 5);
			mgr.SetStartPoint(startPoint);
			double slope = data.MinPipeSlope;
			// to make sure pipe out is not changed from first shaft.
			data.MinPipeSlope = 0;
			mgr.SetEndPoint(new Point3d(11, 2, 5));
			mgr.Recalculate();
			mgr.MoveNextShaft();
			data.MinPipeSlope = slope;

			// 5 deg step. xy angle is 3 deg - it should be rounded to 5
			Point3d mouse = new Point3d(19.987666, 1.52898, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			double angleRad = MyUtilities.Helpers.Calculations.DegreesToRadians(data.PipeData.AngleStepDeg);
			// z is not 4 but 3.975. pipe slope is 0, but there is inner shaft slope.
			Assert.AreEqual(new Point3d(11, 2, 3.975), data.StartShaftData.CenterPipeOutPoint, "start shaft pipe out point.");
			Point3d pipeStartPoint = new Point3d(11 + data.StartShaft.Radius * Math.Cos(angleRad), 2 - data.StartShaft.Radius * Math.Sin(angleRad), 3.975);
			Assert.AreEqual(pipeStartPoint, data.Pipe.StartPoint, "pipe start point.");
			Point2d pipeEndPoint = pipeStartPoint.GetAs2d().Add(new Vector2d(8 * Math.Cos(angleRad), -8 * Math.Sin(angleRad)));
			Assert.AreEqual(pipeEndPoint, data.PipeData.EndPoint.GetAs2d(), "pipe end point.");
			Point2d endPoint = pipeEndPoint.Add(new Vector2d(data.EndShaft.Radius * Math.Cos(angleRad), -data.EndShaft.Radius * Math.Sin(angleRad)));
			Assert.AreEqual(endPoint, data.EndPoint.GetAs2d(), "end point.");

			Assert.AreEqual(0.4, data.StartShaft.PipeOutHeight - data.EndShaft.PipeOutHeight - data.EndShaft.InnerSlopeDeltaH, 0.0001, "pipe out delta");
		}

		/*
		 *	Min delta 
		 */

		[TestMethod]
		public void SetEndPoint_NS_minDelta_fixedTerrain_Test1()
		{
			PipeShaftDataManager mgr = GetManager_NS_fixedTerrain_minDelta();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			Point3d startPoint = new Point3d(1, 2, 5);
			mgr.SetStartPoint(startPoint);

			Point3d mouse = new Point3d(10, 2, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			// Start point represents shaft pipe out height - that is why it is 4 instead of 5.
			Assert.AreEqual(new Point3d(1, 2, 4), data.StartShaftData.CenterPipeOutPoint, "start shaft pipe out point.");
			Assert.AreEqual(new Point3d(1.5, 2, 4), data.Pipe.StartPoint, "pipe start point.");
			Assert.AreEqual(new Point2d(10, 2), data.EndPoint.GetAs2d(), "end point.");
			Assert.AreEqual(new Point2d(9.5, 2), data.PipeData.EndPoint.GetAs2d(), "pipe end point.");

			double deltaZ = 3.075;// Start pipe height = 4. End pipe terrain = 2.5. Delta = 1.6. inner delta = 0.025. 4 - (2.5 - 1.6 + 0.025) = 3.075
			Assert.AreEqual(deltaZ, data.StartShaft.PipeOutHeight - data.EndShaft.PipeOutHeight - data.EndShaft.InnerSlopeDeltaH, 0.0001, "pipe out delta");
		}
		[TestMethod]
		public void SetEndPoint_NS_minDelta_fixedTerrain_angleStep_Test1()
		{
			PipeShaftDataManager mgr = GetManager_NS_fixedTerrain_minDelta();
			PipeShaftData data = (PipeShaftData)mgr.Data;
			data.PipeData.AngleSteppingUsed = true;
			data.PipeData.AngleStepDeg = 30;

			Point3d startPoint = new Point3d(0, 2, 5);
			mgr.SetStartPoint(startPoint);
			double delta = data.EndShaft.MinPipeOutDelta;
			// to make sure pipe out is not changed from first shaft.
			((ShaftData)data.EndShaft).MinPipeOutDelta = 0;
			mgr.SetEndPoint(new Point3d(11, 2, 5));
			mgr.Recalculate();
			mgr.MoveNextShaft();
			((ShaftData)data.EndShaft).MinPipeOutDelta = delta;

			// 30 deg step. y is moved from 2 to 1, so small angle exists - but it should be rounded to 0.
			Point3d mouse = new Point3d(19.944272, 1, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			// z is not 4 but 3.975. pipe slope is 0, but there is inner shaft slope.
			Assert.AreEqual(new Point3d(11, 2, 2.5), data.StartShaftData.CenterPipeOutPoint, "start shaft pipe out point.");
			Assert.AreEqual(new Point3d(11.5, 2, 2.5), data.Pipe.StartPoint, "pipe start point.");
			Assert.AreEqual(new Point2d(19.5, 2), data.PipeData.EndPoint.GetAs2d(), "pipe end point.");
			Assert.AreEqual(new Point2d(20, 2), data.EndPoint.GetAs2d(), "end point.");

			double deltaZ = 1.6 - 0.025;// Start pipe height = 2.5. End pipe terrain = 2.5. Delta = 1.6. inner delta = 0.025. 
			Assert.AreEqual(deltaZ, data.StartShaft.PipeOutHeight - data.EndShaft.PipeOutHeight - data.EndShaft.InnerSlopeDeltaH, 0.0001, "pipe out delta");
		}
		[TestMethod]
		public void SetEndPoint_NS_minDelta_fixedTerrain_angleStep_Test2()
		{
			PipeShaftDataManager mgr = GetManager_NS_fixedTerrain_minDelta();
			PipeShaftData data = (PipeShaftData)mgr.Data;
			data.PipeData.AngleSteppingUsed = true;
			data.PipeData.AngleStepDeg = 5;

			Point3d startPoint = new Point3d(0, 2, 5);
			mgr.SetStartPoint(startPoint);
			double delta = data.EndShaft.MinPipeOutDelta;
			// to make sure pipe out is not changed from first shaft.
			((ShaftData)data.EndShaft).MinPipeOutDelta = 0;
			mgr.SetEndPoint(new Point3d(11, 2, 5));
			mgr.Recalculate();
			mgr.MoveNextShaft();
			((ShaftData)data.EndShaft).MinPipeOutDelta = delta;

			// 5 deg step. xy angle is 3 deg - it should be rounded to 5
			Point3d mouse = new Point3d(19.987666, 1.52898, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			double angleRad = MyUtilities.Helpers.Calculations.DegreesToRadians(data.PipeData.AngleStepDeg);
			// z is not 4 but 3.975. pipe slope is 0, but there is inner shaft slope.
			Assert.AreEqual(new Point3d(11, 2, 2.5), data.StartShaftData.CenterPipeOutPoint, "start shaft pipe out point.");
			Point3d pipeStartPoint = new Point3d(11 + data.StartShaft.Radius * Math.Cos(angleRad), 2 - data.StartShaft.Radius * Math.Sin(angleRad), 2.5);
			Assert.AreEqual(pipeStartPoint, data.Pipe.StartPoint, "pipe start point.");
			Point2d pipeEndPoint = pipeStartPoint.GetAs2d().Add(new Vector2d(8 * Math.Cos(angleRad), -8 * Math.Sin(angleRad)));
			Assert.AreEqual(pipeEndPoint, data.PipeData.EndPoint.GetAs2d(), "pipe end point.");
			Point2d endPoint = pipeEndPoint.Add(new Vector2d(data.EndShaft.Radius * Math.Cos(angleRad), -data.EndShaft.Radius * Math.Sin(angleRad)));
			Assert.AreEqual(endPoint, data.EndPoint.GetAs2d(), "end point.");

			Assert.AreEqual(1.6 - 0.025, data.StartShaft.PipeOutHeight - data.EndShaft.PipeOutHeight - data.EndShaft.InnerSlopeDeltaH, 0.0001, "pipe out delta");
		}

		/*
		 *  Min slope and delta
		 */
		[TestMethod]
		public void SetEndPoint_NS_slopeAndDelta_fixedTerrain_Test1()
		{
			// Min delta to be chosen.
			// y = 1.6. y/x = 0.05 => 
			//	x > 32, y/x < 0.05 
			//	x < 32, y/x > 0.05
			PipeShaftDataManager mgr = GetManager_NS_fixedTerrain_minSlopeAndDelta();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			Point3d startPoint = new Point3d(1, 2, fixedTerrainHeight + fixedTerrainPipeOutDelta);// z is terrain height (terrain-pipeout is 1)
			mgr.SetStartPoint(startPoint);

			Point3d mouse = new Point3d(10, 2, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			Assert.AreEqual(new Point3d(1, 2, 2.5), data.StartShaftData.CenterPipeOutPoint, "start shaft pipe out point.");
			Assert.AreEqual(1.6, data.StartPoint.Z - data.EndPoint.Z + data.EndShaft.InnerSlopeDeltaH, 0.0001, "min delta");
			double length = data.EndPoint.GetAs2d().GetVectorTo(data.StartPoint.GetAs2d()).Length;
			Assert.AreEqual(9, length, 0.0001, "length");
		}
		[TestMethod]
		public void SetEndPoint_NS_slopeAndDelta_fixedTerrain_Test2()
		{
			// Min slope to be chosen
			// y = 1.6. y/x = 0.05 => 
			//	x > 32, y/x < 0.05 
			//	x < 32, y/x > 0.05
			PipeShaftDataManager mgr = GetManager_NS_fixedTerrain_minSlopeAndDelta();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			Point3d startPoint = new Point3d(1, 2, fixedTerrainHeight + fixedTerrainPipeOutDelta);
			mgr.SetStartPoint(startPoint);

			Point3d mouse = new Point3d(35, 2, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			double deltaH = data.StartPoint.Z - data.EndPoint.Z;
			Assert.AreEqual(1.7 - data.EndShaft.InnerSlopeDeltaH, deltaH + data.EndShaft.InnerSlopeDeltaH, 0.0001, "min delta");
			double pipeLength = data.Pipe.EndPoint.GetAs2d().GetVectorTo(data.Pipe.StartPoint.GetAs2d()).Length;
			Assert.AreEqual(33, pipeLength, 0.0001, "pipe length");
			double shaftLength = data.EndPoint.GetAs2d().GetVectorTo(data.StartPoint.GetAs2d()).Length;
			Assert.AreEqual(34, shaftLength, 0.0001, "center to center shaft");
			double slope = deltaH / pipeLength;
			Assert.AreEqual(0.05, slope, 0.0001, "slope");
		}

		#endregion


		#endregion

		#region With segments tests (S)

		private PipeShaftDataManager GetManager_S_fixedTerrain_minSlope()
		{
			PipeShaftDataManager m = new DownstreamManager();
			PipeShaftData data = (PipeShaftData)m.Data;
			data.ToggleStartShaftCreate(true);
			data.ToggleEndShaftCreate(true);
			data.StartShaftData.CoverHeight = 1.6;
			data.StartShaftData.TerrainHeight = 1.5;
			data.StartShaftData.PipeOutHeight = 0.5;
			data.StartShaftData.BottomHeight = 0;

			data.EndShaftData.CoverHeight = 1.4;
			data.EndShaftData.TerrainHeight = 1.5;
			data.EndShaftData.PipeOutHeight = data.EndShaftData.TerrainHeight - fixedTerrainPipeOutDelta;
			data.EndShaftData.CoverHeight = 0.25;

			data.PipeData.SegmentsUsed = true;
			data.PipeData.SegmentLength = 1.2;
			data.PipeData.StartEndSegmentLength = 0.25;
			data.MinPipeSlope = 0.05;

			data.ApplyMinPipeSlope = true;
			//data.ApplyMinDeltaShaftTerrainPipeOut = false;

			m.TerrainHeightCalculator = new PointTerrainHeight(new Point3d(0, 0, fixedTerrainHeight));
			m.Calculation = new OffsetDownstream(data.StartShaft.Radius, data.EndShaft.Radius);
			m.Calculation = new SegmentDownstream(m.Calculation, data.Pipe.SegmentLength, data.Pipe.StartEndSegmentLength);

			return m;
		}
		private PipeShaftDataManager GetManager_S_fixedTerrain_minDelta()
		{
			PipeShaftDataManager m = new DownstreamManager();
			PipeShaftData data = (PipeShaftData)m.Data;
			data.ToggleStartShaftCreate(true);
			data.ToggleEndShaftCreate(true);
			data.StartShaftData.CoverHeight = 1.6;
			data.StartShaftData.TerrainHeight = 1.5;
			data.StartShaftData.PipeOutHeight = 0.5;
			data.StartShaftData.BottomHeight = 0;

			data.EndShaftData.CoverHeight = 1.4;
			data.EndShaftData.TerrainHeight = 1.5;
			data.EndShaftData.PipeOutHeight = data.EndShaftData.TerrainHeight - fixedTerrainPipeOutDelta;
			data.EndShaftData.CoverHeight = 0.25;

			data.PipeData.SegmentsUsed = true;
			data.PipeData.SegmentLength = 1.2;
			data.PipeData.StartEndSegmentLength = 0.25;
			data.MinPipeSlope = 0.05;
			((ShaftData)data.EndShaft).MinPipeOutDelta = 1.6;

			data.ApplyMinPipeSlope = false;
			((ShaftData)data.EndShaft).MinPipeOutDeltaUsed = true;

			m.TerrainHeightCalculator = new PointTerrainHeight(new Point3d(0, 0, fixedTerrainHeight));
			m.Calculation = new OffsetDownstream(data.StartShaft.Radius, data.EndShaft.Radius);
			m.Calculation = new SegmentDownstream(m.Calculation, data.Pipe.SegmentLength, data.Pipe.StartEndSegmentLength);

			return m;
		}
		private PipeShaftDataManager GetManager_S_fixedTerrain_minSlopeAndDelta()
		{
			PipeShaftDataManager m = new DownstreamManager();
			PipeShaftData data = (PipeShaftData)m.Data;
			data.ToggleStartShaftCreate(true);
			data.ToggleEndShaftCreate(true);
			data.StartShaftData.CoverHeight = 1.6;
			data.StartShaftData.TerrainHeight = 1.5;
			data.StartShaftData.PipeOutHeight = 0.5;
			data.StartShaftData.BottomHeight = 0;

			data.EndShaftData.CoverHeight = 1.5;
			data.EndShaftData.TerrainHeight = 1.5;
			data.EndShaftData.PipeOutHeight = data.EndShaftData.TerrainHeight - fixedTerrainPipeOutDelta;
			data.EndShaftData.CoverHeight = 0.25;

			data.PipeData.SegmentsUsed = true;
			data.PipeData.SegmentLength = 1.2;
			data.PipeData.StartEndSegmentLength = 0.25;
			data.PipeData.Slope = 0.05;
			((ShaftData)data.EndShaft).MinPipeOutDelta = 1.6;
			data.MinPipeSlope = 0.05;

			data.ApplyMinPipeSlope = true;
			((ShaftData)data.EndShaft).MinPipeOutDeltaUsed = true;

			m.TerrainHeightCalculator = new PointTerrainHeight(new Point3d(0, 0, fixedTerrainHeight));
			m.Calculation = new OffsetDownstream(data.StartShaft.Radius, data.EndShaft.Radius);
			m.Calculation = new SegmentDownstream(m.Calculation, data.Pipe.SegmentLength, data.Pipe.StartEndSegmentLength);

			return m;
		}
		private PipeShaftDataManager GetManager_NS_3dfaceTerrainUp_minSlopeAndDelta()
		{
			PipeShaftDataManager m = new DownstreamManager();
			PipeShaftData data = (PipeShaftData)m.Data;
			data.ToggleStartShaftCreate(true);
			data.ToggleEndShaftCreate(true);
			data.StartShaftData.CoverHeight = 1.6;
			data.StartShaftData.TerrainHeight = 1.5;
			data.StartShaftData.PipeOutHeight = 0.5;
			data.StartShaftData.BottomHeight = 0;

			data.EndShaftData.CoverHeight = 1.5;
			data.EndShaftData.TerrainHeight = 1.5;
			data.EndShaftData.PipeOutHeight = data.EndShaftData.TerrainHeight - fixedTerrainPipeOutDelta;
			data.EndShaftData.CoverHeight = 0.25;

			data.PipeData.SegmentsUsed = false;
			data.PipeData.Slope = 0.05;
			((ShaftData)data.EndShaft).MinPipeOutDelta = 1.6;
			data.MinPipeSlope = 0.05;

			data.ApplyMinPipeSlope = true;
			((ShaftData)data.EndShaft).MinPipeOutDeltaUsed = true;

			m.TerrainHeightCalculator = new Face3dHeight(new Point3d(10, 2, 1), new Point3d(11, 1, 2), new Point3d(11, 3, 2));
			m.Calculation = new OffsetDownstream(data.StartShaft.Radius, data.EndShaft.Radius);

			return m;
		}
		private PipeShaftDataManager GetManager_NS_3dfaceTerrainDown_minSlopeAndDelta()
		{
			PipeShaftDataManager m = new DownstreamManager();
			PipeShaftData data = (PipeShaftData)m.Data;
			data.ToggleStartShaftCreate(true);
			data.ToggleEndShaftCreate(true);
			data.StartShaftData.CoverHeight = 1.6;
			data.StartShaftData.TerrainHeight = 1.5;
			data.StartShaftData.PipeOutHeight = 0.5;
			data.StartShaftData.BottomHeight = 0;

			data.EndShaftData.CoverHeight = 1.5;
			data.EndShaftData.TerrainHeight = 1.5;
			data.EndShaftData.PipeOutHeight = data.EndShaftData.TerrainHeight - fixedTerrainPipeOutDelta;
			data.EndShaftData.CoverHeight = 0.25;

			data.PipeData.SegmentsUsed = false;
			data.PipeData.Slope = 0.05;
			((ShaftData)data.EndShaft).MinPipeOutDelta = 1.6;
			data.MinPipeSlope = 0.05;

			data.ApplyMinPipeSlope = true;
			((ShaftData)data.EndShaft).MinPipeOutDeltaUsed = true;

			m.TerrainHeightCalculator = new Face3dHeight(new Point3d(10, 1, 2), new Point3d(10, 3, 2), new Point3d(11, 2, 1));
			m.Calculation = new OffsetDownstream(data.StartShaft.Radius, data.EndShaft.Radius);

			return m;
		}
		private PipeShaftDataManager GetManager_S_3dfaceTerrainUp_minSlopeAndDelta()
		{
			PipeShaftDataManager m = new DownstreamManager();
			PipeShaftData data = (PipeShaftData)m.Data;
			data.ToggleStartShaftCreate(true);
			data.ToggleEndShaftCreate(true);
			data.StartShaftData.CoverHeight = 1.6;
			data.StartShaftData.TerrainHeight = 1.5;
			data.StartShaftData.PipeOutHeight = 0.5;
			data.StartShaftData.BottomHeight = 0;

			data.EndShaftData.CoverHeight = 1.5;
			data.EndShaftData.TerrainHeight = 1.5;
			data.EndShaftData.PipeOutHeight = data.EndShaftData.TerrainHeight - fixedTerrainPipeOutDelta;
			data.EndShaftData.CoverHeight = 0.25;

			data.PipeData.SegmentsUsed = true;
			data.PipeData.SegmentLength = 1.2;
			data.PipeData.StartEndSegmentLength = 0.25;
			data.PipeData.Slope = 0.05;
			((ShaftData)data.EndShaft).MinPipeOutDelta = 1.6;
			data.MinPipeSlope = 0.05;

			data.ApplyMinPipeSlope = true;
			((ShaftData)data.EndShaft).MinPipeOutDeltaUsed = true;

			m.TerrainHeightCalculator = new Face3dHeight(new Point3d(10, 2, 1), new Point3d(11, 1, 2), new Point3d(11, 3, 2));
			m.Calculation = new OffsetDownstream(data.StartShaft.Radius, data.EndShaft.Radius);
			m.Calculation = new SegmentDownstream(m.Calculation, data.Pipe.SegmentLength, data.Pipe.StartEndSegmentLength);

			return m;
		}

		private PipeShaftDataManager GetManager_S_3dfaceTerrainDown_minSlopeAndDelta()
		{
			PipeShaftDataManager m = new DownstreamManager();
			PipeShaftData data = (PipeShaftData)m.Data;
			data.ToggleStartShaftCreate(true);
			data.ToggleEndShaftCreate(true);
			data.StartShaftData.CoverHeight = 1.6;
			data.StartShaftData.TerrainHeight = 1.5;
			data.StartShaftData.PipeOutHeight = 0.5;
			data.StartShaftData.BottomHeight = 0;

			data.EndShaftData.CoverHeight = 1.5;
			data.EndShaftData.TerrainHeight = 1.5;
			data.EndShaftData.PipeOutHeight = data.EndShaftData.TerrainHeight - fixedTerrainPipeOutDelta;
			data.EndShaftData.CoverHeight = 0.25;

			data.PipeData.SegmentsUsed = true;
			data.PipeData.SegmentLength = 1.2;
			data.PipeData.StartEndSegmentLength = 0.25;
			data.PipeData.Slope = 0.05;
			((ShaftData)data.EndShaft).MinPipeOutDelta = 1.6;
			data.MinPipeSlope = 0.05;

			data.ApplyMinPipeSlope = true;
			((ShaftData)data.EndShaft).MinPipeOutDeltaUsed = true;

			m.TerrainHeightCalculator = new Face3dHeight(new Point3d(10, 1, 2), new Point3d(10, 3, 2), new Point3d(11, 2, 1));
			m.Calculation = new OffsetDownstream(data.StartShaft.Radius, data.EndShaft.Radius);
			m.Calculation = new SegmentDownstream(m.Calculation, data.Pipe.SegmentLength, data.Pipe.StartEndSegmentLength);

			return m;
		}

		#region SetStartPoint
		[TestMethod]
		public void SetStartPoint_S_Test1()
		{
			PipeShaftDataManager mgr = GetManager_S_fixedTerrain_minSlope();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			Point3d mouse = new Point3d(1, 2, 5);
			mgr.SetStartPoint(mouse);


			Assert.AreEqual(new Point3d(1, 2, 4), data.StartPoint, "start point");
			Assert.AreEqual(5.1, data.StartShaft.CoverHeight, "cover");
			Assert.AreEqual(5, data.StartShaft.TerrainHeight, "terrain");
			Assert.AreEqual(4, data.StartShaft.PipeOutHeight, "pipe out");
			Assert.AreEqual(3.5, data.StartShaft.BottomHeight, "bottom");
		}
		[TestMethod, ExpectedException(typeof(NotSupportedException))]
		public void SetStartPoint_S_Test2()
		{
			PipeShaftDataManager mgr = GetManager_S_fixedTerrain_minSlope();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			Point3d mouse = new Point3d(1, 2, 5);
			mgr.SetStartPoint(mouse);

			// Move only if end point has been set.
			mgr.MoveToNextPipe();
		}
		#endregion
		#region SetEndPoint
		/*
		 *  Min slope
		 */

		[TestMethod]
		public void SetEndPoint_S_minSlope_fixedTerrain_Test1()
		{
			PipeShaftDataManager mgr = GetManager_S_fixedTerrain_minSlope();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			Point3d startPoint = new Point3d(1, 2, 5);
			mgr.SetStartPoint(startPoint);

			Point3d mouse = new Point3d(10, 2, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			// Start point represents shaft pipe out height - that is why it is 4 instead of 5.
			Assert.AreEqual(new Point3d(1, 2, 4), data.StartShaftData.CenterPipeOutPoint, "start shaft pipe out point.");
			Assert.AreEqual(new Point3d(1.5, 2, 4), data.Pipe.StartPoint, "pipe start point.");
			// start center x: 1
			// start point pipe x: 1.5
			// x length start/end pipe = 9 - 0.5 (start shaft radius) - 0.5 (end shaft radius) = 8.
			// Assume 8 is 3d length which is available.
			// 8 - 2 * 0.25 (startEndSegment) = 7.5
			// 7.5 / segment lengths (1.2) = 6.25
			// 6 * 1.2 = 7.2
			// 7.2 + startEndsegmentlength = 7.7 ; this is total pipe length which we test (find closest value to 8).
			// x value of 7.7 with slope 5% = 7.6904 (y value is = 0.3845)
			// if we say number of segments is 7: 7 * 1.2 = 8.4
			// 8.4 + 0.5 = 8.9
			// x value of 8.9 with slope 5% = 8.8889 ( y value is = 0.4438)
			// segment cound 6 is closer to 8 than 7 segments.
			Assert.AreEqual(new Point2d(7.6904 + 1.5, 2), data.Pipe.EndPoint.GetAs2d(), "end point.");
			Assert.AreEqual(new Point2d(7.6904 + 1.5 + 0.5, 2), data.EndPoint.GetAs2d(), "pipe end point.");

			Assert.AreEqual(0.3845, data.StartShaft.PipeOutHeight - data.EndShaft.PipeOutHeight - data.EndShaft.InnerSlopeDeltaH, 0.0001, "pipe out delta");
		}

		[TestMethod]
		public void SetEndPoint_S_minSlope_fixedTerrain_angleStep_Test1()
		{
			PipeShaftDataManager mgr = GetManager_S_fixedTerrain_minSlope();
			PipeShaftData data = (PipeShaftData)mgr.Data;
			data.PipeData.AngleSteppingUsed = true;
			data.PipeData.AngleStepDeg = 30;

			// -0.1 because segment is 1.2 and startEndSegment is 0.25. 10.1 + 1 is 11.1 not 11.0.
			Point3d startPoint = new Point3d(-0.1, 2, 5);
			mgr.SetStartPoint(startPoint);
			double slope = data.MinPipeSlope;
			// to make sure pipe out is not changed from first shaft.
			data.MinPipeSlope = 0;
			mgr.SetEndPoint(new Point3d(11, 2, 5));
			mgr.Recalculate();
			mgr.MoveNextShaft();
			data.MinPipeSlope = slope;

			// 30 deg step. y is moved from 2 to 1, so small angle exists - but it should be rounded to 0.
			Point3d mouse = new Point3d(19.944272, 1, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			// z is not 4 but 3.975. pipe slope is 0, but there is inner shaft slope.
			Assert.AreEqual(new Point3d(11, 2, 3.975), data.StartShaftData.CenterPipeOutPoint, "start shaft pipe out point.");
			Assert.AreEqual(new Point3d(11.5, 2, 3.975), data.Pipe.StartPoint, "pipe start point.");
			// pipe x length = 8.9443
			// pipe 3d length = 9
			// 

			Assert.AreEqual(new Point3d(11.5 + 7.6904, 2, 3.5905), data.PipeData.EndPoint, "pipe end point.");
			Assert.AreEqual(new Point3d(11.5 + 7.6904 + 0.5, 2, 3.5905), data.EndPoint, "end point.");

			Assert.AreEqual(0.3845, data.StartShaft.PipeOutHeight - data.EndShaft.PipeOutHeight - data.EndShaft.InnerSlopeDeltaH, 0.0001, "pipe out delta");
		}

		[TestMethod]
		public void SetEndPoint_S_minSlope_fixedTerrain_angleStep_Test2()
		{
			PipeShaftDataManager mgr = GetManager_S_fixedTerrain_minSlope();
			PipeShaftData data = (PipeShaftData)mgr.Data;
			data.PipeData.AngleSteppingUsed = true;
			data.PipeData.AngleStepDeg = 5;

			Point3d startPoint = new Point3d(-0.1, 2, 5);
			mgr.SetStartPoint(startPoint);
			double slope = data.MinPipeSlope;
			// to make sure pipe out is not changed from first shaft.
			data.MinPipeSlope = 0;
			mgr.SetEndPoint(new Point3d(11, 2, 5));
			mgr.Recalculate();
			mgr.MoveNextShaft();
			data.MinPipeSlope = slope;

			// 5 deg step. xy angle is 3 deg - it should be rounded to 5. xy length is 9.
			Point3d mouse = new Point3d(19.987666, 1.52898, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			double angleRad = MyUtilities.Helpers.Calculations.DegreesToRadians(data.PipeData.AngleStepDeg);
			// z is not 4 but 3.975. pipe slope is 0, but there is inner shaft slope.
			Assert.AreEqual(new Point3d(11, 2, 3.975), data.StartShaftData.CenterPipeOutPoint, "start shaft pipe out point.");

			Point3d pipeStartPoint = new Point3d(11 + data.StartShaft.Radius * Math.Cos(angleRad), 2 - data.StartShaft.Radius * Math.Sin(angleRad), 3.975);
			Assert.AreEqual(pipeStartPoint, data.Pipe.StartPoint, "pipe start point.");

			Point3d pipeEndPoint = pipeStartPoint.Add(new Vector3d(7.6904 * Math.Cos(angleRad), -7.6904 * Math.Sin(angleRad), 0));
			pipeEndPoint = pipeEndPoint.GetAs2d().GetWithHeight(3.5905);
			Assert.AreEqual(pipeEndPoint, data.PipeData.EndPoint, "pipe end point.");

			Point2d endPoint = pipeEndPoint.GetAs2d().Add(new Vector2d(data.EndShaft.Radius * Math.Cos(angleRad), -data.EndShaft.Radius * Math.Sin(angleRad)));
			Assert.AreEqual(endPoint, data.EndPoint.GetAs2d(), "end point.");

			Assert.AreEqual(0.3845, data.StartShaft.PipeOutHeight - data.EndShaft.PipeOutHeight - data.EndShaft.InnerSlopeDeltaH, 0.0001, "pipe out delta");
		}

		/*
		 *	Min delta 
		 */

		[TestMethod]
		public void SetEndPoint_S_minDelta_fixedTerrain_Test1()
		{
			PipeShaftDataManager mgr = GetManager_S_fixedTerrain_minDelta();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			Point3d startPoint = new Point3d(1, 2, 5);
			mgr.SetStartPoint(startPoint);

			Point3d mouse = new Point3d(10, 2, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			// Start point represents shaft pipe out height - that is why it is 4 instead of 5.
			Assert.AreEqual(new Point3d(1, 2, 4), data.StartShaftData.CenterPipeOutPoint, "start shaft pipe out point.");
			Assert.AreEqual(new Point3d(1.5, 2, 4), data.Pipe.StartPoint, "pipe start point.");
			// Verified with bricscad
			Assert.AreEqual(new Point2d(8.5593, 2), data.PipeData.EndPoint.GetAs2d(), "pipe end point.");
			Assert.AreEqual(new Point3d(9.0593, 2, 0.925), data.EndPoint, "end point.");

			double deltaZ = 3.075;// Start pipe height = 4. End pipe terrain = 2.5. Delta = 1.6. inner delta = 0.025. 4 - (2.5 - 1.6 + 0.025) = 3.075
			Assert.AreEqual(deltaZ, data.StartShaft.PipeOutHeight - data.EndShaft.PipeOutHeight - data.EndShaft.InnerSlopeDeltaH, 0.0001, "pipe out delta");
		}
		[TestMethod]
		public void SetEndPoint_S_minDelta_fixedTerrain_angleStep_Test1()
		{
			PipeShaftDataManager mgr = GetManager_S_fixedTerrain_minDelta();
			PipeShaftData data = (PipeShaftData)mgr.Data;
			data.PipeData.AngleSteppingUsed = true;
			data.PipeData.AngleStepDeg = 30;

			Point3d startPoint = new Point3d(-0.1, 2, 3.55);
			mgr.SetStartPoint(startPoint);
			double delta = data.EndShaft.MinPipeOutDelta;
			// to make sure pipe out is not changed from first shaft.
			((ShaftData)data.EndShaft).MinPipeOutDelta = 0;
			mgr.SetEndPoint(new Point3d(11, 2, 5));
			mgr.Recalculate();
			mgr.MoveNextShaft();
			((ShaftData)data.EndShaft).MinPipeOutDelta = delta;

			// 30 deg step. y is moved from 2 to 1, so small angle exists - but it should be rounded to 0.
			Point3d mouse = new Point3d(19.944272, 1, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			// z is not 4 but 3.975. pipe slope is 0, but there is inner shaft slope.
			Assert.AreEqual(new Point3d(11, 2, 2.5), data.StartShaftData.CenterPipeOutPoint, "start shaft pipe out point.");
			Assert.AreEqual(new Point3d(11.5, 2, 2.5), data.Pipe.StartPoint, "pipe start point.");
			Assert.AreEqual(new Point2d(19.0372, 2), data.PipeData.EndPoint.GetAs2d(), "pipe end point.");
			Assert.AreEqual(new Point2d(19.5372, 2), data.EndPoint.GetAs2d(), "end point.");

			double deltaZ = 1.6 - 0.025;// Start pipe height = 2.5. End pipe terrain = 2.5. Delta = 1.6. inner delta = 0.025. 
			Assert.AreEqual(deltaZ, data.StartShaft.PipeOutHeight - data.EndShaft.PipeOutHeight - data.EndShaft.InnerSlopeDeltaH, 0.0001, "pipe out delta");
		}
		[TestMethod]
		public void SetEndPoint_S_minDelta_fixedTerrain_angleStep_Test2()
		{
			PipeShaftDataManager mgr = GetManager_S_fixedTerrain_minDelta();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			Point3d startPoint = new Point3d(-0.1, 2, 3.5);
			mgr.SetStartPoint(startPoint);
			double delta = data.EndShaft.MinPipeOutDelta;
			// to make sure pipe out is not changed from first shaft.
			((ShaftData)data.EndShaft).MinPipeOutDelta = 0;
			mgr.SetEndPoint(new Point3d(11, 2, 5));
			mgr.Recalculate();
			mgr.MoveNextShaft();
			((ShaftData)data.EndShaft).MinPipeOutDelta = delta;

			data.PipeData.AngleSteppingUsed = true;
			data.PipeData.AngleStepDeg = 5;

			// 5 deg step. xy angle is 3 deg - it should be rounded to 5
			Point3d mouse = new Point3d(19.987666, 1.52898, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			double angleRad = MyUtilities.Helpers.Calculations.DegreesToRadians(data.PipeData.AngleStepDeg);
			// z is not 4 but 3.975. pipe slope is 0, but there is inner shaft slope.
			Assert.AreEqual(new Point3d(11, 2, 2.5), data.StartShaftData.CenterPipeOutPoint, "start shaft pipe out point.");
			Point3d pipeStartPoint = new Point3d(11 + data.StartShaft.Radius * Math.Cos(angleRad), 2 - data.StartShaft.Radius * Math.Sin(angleRad), 2.5);
			Assert.AreEqual(pipeStartPoint, data.Pipe.StartPoint, "pipe start point.");
			double pipeLength2d = data.Pipe.StartPoint.GetAs2d().GetVectorTo(data.Pipe.EndPoint.GetAs2d()).Length;
			double pipeLength3d = data.Pipe.StartPoint.GetVectorTo(data.Pipe.EndPoint).Length;
			Assert.AreEqual(pipeLength3d, 7.7, 0.0001, "pipe length 3d");
			Point2d pipeEndPoint = pipeStartPoint.GetAs2d().Add(new Vector2d(pipeLength2d * Math.Cos(angleRad), -pipeLength2d * Math.Sin(angleRad)));
			Assert.AreEqual(pipeEndPoint, data.PipeData.EndPoint.GetAs2d(), "pipe end point.");
			Point2d endPoint = pipeEndPoint.Add(new Vector2d(data.EndShaft.Radius * Math.Cos(angleRad), -data.EndShaft.Radius * Math.Sin(angleRad)));
			Assert.AreEqual(endPoint, data.EndPoint.GetAs2d(), "end point.");

			Assert.AreEqual(1.6 - 0.025, data.StartShaft.PipeOutHeight - data.EndShaft.PipeOutHeight - data.EndShaft.InnerSlopeDeltaH, 0.0001, "pipe out delta");
		}

		/*
		 *  Min slope and delta
		 */

		[TestMethod]
		public void SetEndPoint_S_slopeAndDelta_fixedTerrain_Test1()
		{
			// Min delta to be chosen.
			// y = 1.6. y/x = 0.05 => 
			//	x > 32, y/x < 0.05 
			//	x < 32, y/x > 0.05
			PipeShaftDataManager mgr = GetManager_S_fixedTerrain_minSlopeAndDelta();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			Point3d startPoint = new Point3d(1, 2, fixedTerrainHeight + fixedTerrainPipeOutDelta);// z is terrain height (terrain-pipeout is 1)
			mgr.SetStartPoint(startPoint);

			Point3d mouse = new Point3d(10, 2, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			Assert.AreEqual(new Point3d(1, 2, 2.5), data.StartShaftData.CenterPipeOutPoint, "start shaft pipe out point.");
			Assert.AreEqual(1.6, data.StartPoint.Z - data.EndPoint.Z + data.EndShaft.InnerSlopeDeltaH, 0.0001, "min delta");
			double shaftCenterLength = data.EndPoint.GetAs2d().GetVectorTo(data.StartPoint.GetAs2d()).Length;
			double pipeLength = data.PipeData.StartPoint.GetVectorTo(data.PipeData.EndPoint).Length;
			double pipeLength2d = data.PipeData.StartPoint.GetAs2d().GetVectorTo(data.PipeData.EndPoint.GetAs2d()).Length;
			Assert.AreEqual(pipeLength, 7.7, 0.0001, "pipe 3d length");
			Assert.AreEqual(1 + pipeLength2d, shaftCenterLength, 0.0001, "length");
		}
		[TestMethod]
		public void SetEndPoint_S_slopeAndDelta_fixedTerrain_Test2()
		{
			// Min slope to be chosen
			// y = 1.6. y/x = 0.05 => 
			//	x > 32, y/x < 0.05 
			//	x < 32, y/x > 0.05
			PipeShaftDataManager mgr = GetManager_S_fixedTerrain_minSlopeAndDelta();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			Point3d startPoint = new Point3d(1, 2, fixedTerrainHeight + fixedTerrainPipeOutDelta);
			mgr.SetStartPoint(startPoint);

			Point3d mouse = new Point3d(35, 2, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			Assert.AreEqual(new Point3d(1, 2, 2.5), data.StartShaftData.CenterPipeOutPoint, "start shaft pipe out point.");

			double deltaH = data.StartPoint.Z - data.EndPoint.Z;
			// Length 3d calculated is 32.9 => y = 1.6429, x = 32.8590.
			Assert.AreEqual(1.6429, deltaH, 0.0001, "min delta");
			double pipeLength = data.Pipe.EndPoint.GetAs2d().GetVectorTo(data.Pipe.StartPoint.GetAs2d()).Length;
			Assert.AreEqual(32.8590, pipeLength, 0.0001, "pipe length");
			double shaftLength = data.EndPoint.GetAs2d().GetVectorTo(data.StartPoint.GetAs2d()).Length;
			Assert.AreEqual(33.8590, shaftLength, 0.0001, "center to center shaft");
			double slope = deltaH / pipeLength;
			Assert.AreEqual(0.05, slope, 0.0001, "slope");
		}


		[TestMethod]
		public void SetEndPoint_NS_slopeAndDelta_3dfaceUp_Test1()
		{
			// Min slope is used.
			// at x=11, delta = 1 (delta to start pipe out)
			// at x=12, delta = 2 (delta to start pipe out)			
			PipeShaftDataManager mgr = GetManager_NS_3dfaceTerrainUp_minSlopeAndDelta();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			// Terrain height 2 -> becomes pipe out height 1.
			Point3d startPoint = new Point3d(0.5, 2, 2);
			mgr.SetStartPoint(startPoint);

			Point3d mouse = new Point3d(11.5, 2, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			Assert.AreEqual(new Point3d(1, 2, 1), mgr.Data.Pipe.StartPoint, "start pipe");
			double slope = (data.Pipe.StartPoint.Z - data.Pipe.EndPoint.Z) / data.Pipe.StartPoint.GetAs2d().GetVectorTo(data.Pipe.EndPoint.GetAs2d()).Length;
			Assert.AreEqual(0.05, slope, 0.0001, "pipe slope");
			double terrainDelta = data.EndShaft.TerrainHeight - data.EndShaft.PipeOutHeight;
			Assert.AreEqual(1.5 + 0.5 + data.EndShaft.InnerSlopeDeltaH, terrainDelta, 0.0001, "terrain delta");

			//Assert.AreEqual(new Point3d(11.5, ))

			mouse = new Point3d(12, 2, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			terrainDelta = data.EndShaft.TerrainHeight - data.EndShaft.PipeOutHeight;
			// Height of the shaft center is used for pipe in/out. Pipe end at 11.5 has height of 1.5, but 2 is used as end shaft center.
			Assert.AreEqual(2 + 0.05 * 10.5 + data.EndShaft.InnerSlopeDeltaH, terrainDelta, 0.0001, "terrain delta");

			// Min delta is used
			mouse = new Point3d(10, 2, 100);
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			slope = (data.Pipe.StartPoint.Z - data.Pipe.EndPoint.Z) / data.Pipe.StartPoint.GetAs2d().GetVectorTo(data.Pipe.EndPoint.GetAs2d()).Length;
			Assert.AreEqual((1.6 - data.EndShaft.InnerSlopeDeltaH)/ 8.5, slope, 0.0001, "min delta slope");
			terrainDelta = data.EndShaft.TerrainHeight - data.EndShaft.PipeOutHeight;
			Assert.AreEqual(1.6, terrainDelta, 0.0001, "min delta terrain pipe out");
		}

		[TestMethod]
		public void SetEndPoint_NS_slopeAndDelta_3dfaceDown_Test1()
		{
			// Min slope is used.
			// at x=11, delta = 1 (delta to start pipe out)
			// at x=12, delta = 2 (delta to start pipe out)			
			PipeShaftDataManager mgr = GetManager_NS_3dfaceTerrainDown_minSlopeAndDelta();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			// Terrain height 2 -> becomes pipe out height 1.
			Point3d startPoint = new Point3d(0.5, 2, 2);
			mgr.SetStartPoint(startPoint);

			Point3d mouse = new Point3d(9, 2, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			Assert.AreEqual(new Point3d(1, 2, 1), mgr.Data.Pipe.StartPoint, "start pipe");
			double slope = (data.Pipe.StartPoint.Z - data.Pipe.EndPoint.Z) / data.Pipe.StartPoint.GetAs2d().GetVectorTo(data.Pipe.EndPoint.GetAs2d()).Length;
			Assert.AreEqual(0.05, slope, 0.0001, "pipe slope");
			double terrainDelta = data.EndShaft.TerrainHeight - data.EndShaft.PipeOutHeight;
			Assert.AreEqual(2 + 0.05 * 7.5 + data.EndShaft.InnerSlopeDeltaH, terrainDelta, 0.0001, "terrain delta");

			//Assert.AreEqual(new Point3d(11.5, ))

			mouse = new Point3d(9.5, 2, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			terrainDelta = data.EndShaft.TerrainHeight - data.EndShaft.PipeOutHeight;
			// Height of the shaft center is used for pipe in/out. Pipe end at 9 has height of 2, but 1.5 is used as end shaft center.
			Assert.AreEqual(1.5 + 0.05 * 8 + data.EndShaft.InnerSlopeDeltaH, terrainDelta, 0.0001, "terrain delta");


			// Min delta is used
			mouse = new Point3d(11, 2, 100);
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			slope = (data.Pipe.StartPoint.Z - data.Pipe.EndPoint.Z) / data.Pipe.StartPoint.GetAs2d().GetVectorTo(data.Pipe.EndPoint.GetAs2d()).Length;
			Assert.AreEqual((1.6 - data.EndShaft.InnerSlopeDeltaH) / 9.5, slope, 0.0001, "min delta slope");
			terrainDelta = data.EndShaft.TerrainHeight - data.EndShaft.PipeOutHeight;
			Assert.AreEqual(1.6, terrainDelta, 0.0001, "min delta terrain pipe out");
		}

		[TestMethod]
		public void SetEndPoint_S_slopeAndDelta_3dfaceUp_Test1()
		{
			// Min slope is used.
			// at x=11, delta = 1 (delta to start pipe out)
			// at x=12, delta = 2 (delta to start pipe out)			
			PipeShaftDataManager mgr = GetManager_S_3dfaceTerrainUp_minSlopeAndDelta();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			// Terrain height 2 -> becomes pipe out height 1.
			Point3d startPoint = new Point3d(0.5, 2, 2);
			mgr.SetStartPoint(startPoint);

			Point3d mouse = new Point3d(11.5, 2, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			Assert.AreEqual(new Point3d(1, 2, 1), mgr.Data.Pipe.StartPoint, "start pipe");
			double slope = (data.Pipe.StartPoint.Z - data.Pipe.EndPoint.Z) / data.Pipe.StartPoint.GetAs2d().GetVectorTo(data.Pipe.EndPoint.GetAs2d()).Length;
			Assert.AreEqual(0.05, slope, 0.0001, "pipe slope");
			double length = data.Pipe.StartPoint.GetVectorTo(data.Pipe.EndPoint).Length;
			Assert.AreEqual(8 * data.Pipe.SegmentLength + data.Pipe.StartEndSegmentLength * 2, length, 0.0001, "pipe lenght");
			
		}

		[TestMethod]
		public void SetEndPoint_S_slopeAndDelta_3dfaceDown_Test1()
		{
			// Min slope is used.
			// at x=11, delta = 1 (delta to start pipe out)
			// at x=12, delta = 2 (delta to start pipe out)			
			PipeShaftDataManager mgr = GetManager_S_3dfaceTerrainDown_minSlopeAndDelta();
			PipeShaftData data = (PipeShaftData)mgr.Data;

			// Terrain height 2 -> becomes pipe out height 1.
			Point3d startPoint = new Point3d(0.5, 2, 2);
			mgr.SetStartPoint(startPoint);

			Point3d mouse = new Point3d(9, 2, 100);// z not important.
			mgr.SetEndPoint(mouse);
			mgr.Recalculate();

			Assert.AreEqual(new Point3d(1, 2, 1), mgr.Data.Pipe.StartPoint, "start pipe");
			double slope = (data.Pipe.StartPoint.Z - data.Pipe.EndPoint.Z) / data.Pipe.StartPoint.GetAs2d().GetVectorTo(data.Pipe.EndPoint.GetAs2d()).Length;
			Assert.AreEqual(0.05, slope, 0.0001, "pipe slope");
			
		}
		#endregion
		#endregion
	}
}
