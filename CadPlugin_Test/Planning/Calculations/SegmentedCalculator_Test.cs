﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CadPlugin.Sewage.Planning.Calculations;
using MyUtilities.Geometry;
using System.Collections.Generic;
using CadPlugin.Sewage.Plannings;
using CadPlugin.Sewage.Planning;
using SharedUtilities.Geometry;

namespace CadPlugin_Test.Planning.Calculations
{
	[TestClass]
	public class SegmentedCalculator_Test
	{
		[TestMethod]
		public void LinesIntersection_Test()
		{
			// y1 = 2x - 2
			// y2 = x + 2
			Point2d p1_1 = new Point2d(0, -2);
			Point2d p1_2 = new Point2d(1, 0);
			Point2d p2_1 = new Point2d(0, 2);
			Point2d p2_2 = new Point2d(1, 3);
			Point2d p = SegmentedCalculator.LinesIntersection(p1_1, p1_2, p2_1, p2_2);

			Assert.AreEqual(new Point2d(4, 6), p);
		}
		[TestMethod]
		public void LineCircleIntersection_BasicTest()
		{
			Point2d center = new Point2d(0, 1);
			double radius = 1;
			Point2d p1 = new Point2d(1, 1);
			Point2d p2 = new Point2d(2, 1);
			double referenceLength = 0.5;
			Line2dParameters lineParams = new Line2dParameters(p1, p2);

			Point2d? p = SegmentedCalculator.LineCircleIntersection(center, radius, lineParams, referenceLength);

			Assert.AreEqual(new Point2d(1, 1), p.Value);
		}
		[TestMethod]
		public void LineCircleIntersection_Test1()
		{
			Point2d center = new Point2d(223.8307529, 70.9885586);
			double radius = 7.7;
			Point2d p1 = new Point2d(229.141478, 76.560114);
			Point2d p2 = new Point2d(216.133822, 70.9312405);
			double referenceLength = 0.5;
			Line2dParameters lineParams = new Line2dParameters(p1, p2);

			Point2d? p = SegmentedCalculator.LineCircleIntersection(center, radius, lineParams, referenceLength);

			Assert.AreEqual(p.Value.X, p1.X, 0.01);
			Assert.AreEqual(p.Value.Y, p1.Y, 0.01);
			// this fails because of 3rd decimal.
			//Assert.AreEqual(p1, p);
		}
		[TestMethod]
		public void LineCircleIntersection_Test2()
		{
			Point2d center = new Point2d(103, 71);
			double radius = 6;
			Point2d p1 = new Point2d(107.0451, 66.5686);
			Point2d p2 = new Point2d(97.1436, 72.3049);
			double referenceLength = 0.5;
			Line2dParameters lineParams = new Line2dParameters(p1, p2);

			Point2d? p = SegmentedCalculator.LineCircleIntersection(center, radius, lineParams, referenceLength);

			Assert.AreEqual(p.Value.X, p1.X, 0.0001);
			Assert.AreEqual(p.Value.Y, p1.Y, 0.0001);
			//Assert.AreEqual(p1, p);
		}

		[TestMethod]
		public void Calculate_Test1()
		{
			Point3d pipeCenter = new Point3d(0, 0, 0);
			Vector3d nonSegmentedPipe = new Vector3d(9.7, 0, -0.5);
			List<double> segmentedLengths = new List<double>() { 8, 9, 10, 11 };
			TerrainCalculator calc = new Face3dHeight(new Point3d(9, -0.5, 0), new Point3d(11, -0.5, 4), new Point3d(10, 1, 2));
			double delta = 2;

			Point3d? p = SegmentedCalculator.Calculate(pipeCenter, nonSegmentedPipe, segmentedLengths, calc, delta);

			Assert.AreEqual(10, p.Value.X, "X");
			Assert.AreEqual(0, p.Value.Y, "Y");
			Assert.AreEqual(2, p.Value.Z, "Z");
		}
		[TestMethod]
		public void Calculate_Test2()
		{
			Point3d pipeCenter = new Point3d(1, 0, 0);
			Vector3d nonSegmentedPipe = new Vector3d(8.7, 0, -0.5);
			List<double> segmentedLengths = new List<double>() { 8, 9, 10, 11 };
			TerrainCalculator calc = new Face3dHeight(new Point3d(9, -0.5, 0), new Point3d(11, -0.5, 4), new Point3d(10, 1, 2));
			double delta = 2;

			Point3d? p = SegmentedCalculator.Calculate(pipeCenter, nonSegmentedPipe, segmentedLengths, calc, delta);

			Assert.AreEqual(10, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(2, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void Calculate_Test3()
		{
			Point3d pipeCenter = new Point3d(1, 1, 0);
			Vector3d nonSegmentedPipe = new Vector3d(8.7, 0, -0.5);
			List<double> segmentedLengths = new List<double>() { 7, 8, 9, 10, 11 };
			TerrainCalculator calc = new Face3dHeight(new Point3d(9, -0.5, 0), new Point3d(11, -0.5, 4), new Point3d(10, 1, 2));
			double delta = 2;

			Point3d? p = SegmentedCalculator.Calculate(pipeCenter, nonSegmentedPipe, segmentedLengths, calc, delta);

			Assert.AreEqual(10, p.Value.X, 0.0001, "X");
			Assert.AreEqual(1, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(2, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void Calculate_Test4()
		{
			Point3d pipeCenter = new Point3d(1, 1, 0);
			Vector3d nonSegmentedPipe = new Vector3d(0, 8.7, -0.5);
			List<double> segmentedLengths = new List<double>() { 8, 9, 10, 11 };
			TerrainCalculator calc = new Face3dHeight(new Point3d(-0.5, 9, 0), new Point3d(-0.5, 11, 4), new Point3d(1, 10, 2));
			double delta = 2;

			Point3d? p = SegmentedCalculator.Calculate(pipeCenter, nonSegmentedPipe, segmentedLengths, calc, delta);

			Assert.AreEqual(1, p.Value.X, 0.0001, "X");
			Assert.AreEqual(10, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(2, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void Calculate_Test5()
		{
			Point3d pipeCenter = new Point3d(1, 1, 0);
			Vector3d nonSegmentedPipe = new Vector3d(0, 8.7, -0.5);
			List<double> segmentedLengths = new List<double>() { 8, 9, 10, 11 };
			TerrainCalculator calc = new Face3dHeight(new Point3d(-0.5, 9, 0), new Point3d(-0.5, 11, 4), new Point3d(1, 10, 2));
			double delta = 2;

			Point3d? p = SegmentedCalculator.Calculate(pipeCenter, nonSegmentedPipe, segmentedLengths, calc, delta);

			Assert.AreEqual(1, p.Value.X, 0.0001, "X");
			Assert.AreEqual(10, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(2, p.Value.Z, 0.0001, "Z");
		}

		#region CalculatePointWithMinSlope
		[TestMethod]
		public void CalculatePointWithMinSlope_Basic1()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double slope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, -9.6 * slope);
			Point3d start = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			Vector3d v = SegmentedCalculator.CalculatePointWithMinSlope(iterator, nonSegmentedPipe);
			Point3d p = start.Add(v);// SegmentedCalculator.CalculatePointWithMinSlope(iterator, nonSegmentedPipe, start); 
			Assert.AreEqual(10, v.Length, "segments");
			Assert.AreEqual(9.9875, p.X, 0.0001, "X");
			Assert.AreEqual(0, p.Y, 0.0001, "Y");
			Assert.AreEqual(-0.49937, p.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlope_Basic2()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double slope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, -9.4 * slope);
			Point3d start = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			Vector3d v = SegmentedCalculator.CalculatePointWithMinSlope(iterator, nonSegmentedPipe);
			Point3d p = start.Add(v);// SegmentedCalculator.CalculatePointWithMinSlope(iterator, nonSegmentedPipe);
			Assert.AreEqual(9, start.GetVectorTo(p).Length, "segments");
			Assert.AreEqual(8.98875, p.X, 0.0001, "X");
			Assert.AreEqual(0, p.Y, 0.0001, "Y");
			Assert.AreEqual(-0.44944, p.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlope_XY45deg()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double slope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(5, 5, -(5 * Math.Sqrt(2)) * slope);
			Point3d start = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			Vector3d v = SegmentedCalculator.CalculatePointWithMinSlope(iterator, nonSegmentedPipe);
			Point3d p = start.Add(v);// SegmentedCalculator.CalculatePointWithMinSlope(iterator, nonSegmentedPipe);
			Assert.AreEqual(7, start.GetVectorTo(p).Length, 0.1, "segments");
			Assert.AreEqual(4.943596, p.X, 0.0001, "X");
			Assert.AreEqual(4.943596, p.Y, 0.0001, "Y");
			Assert.AreEqual(-0.3495, p.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlope_XY45deg_centerOffset()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double slope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(5, 5, -(5 * Math.Sqrt(2)) * slope);
			Point3d start = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			Vector3d v = SegmentedCalculator.CalculatePointWithMinSlope(iterator, nonSegmentedPipe);
			Point3d p = start.Add(v);// SegmentedCalculator.CalculatePointWithMinSlope(iterator, nonSegmentedPipe);
			Assert.IsNotNull(p);
			Assert.AreEqual(7, start.GetVectorTo(p).Length, 0.1, "segments");
			Assert.AreEqual(5.943596, p.X, 0.0001, "X");
			Assert.AreEqual(6.943596, p.Y, 0.0001, "Y");
			Assert.AreEqual(-0.3495, p.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlope_XY45deg_centerOffset_offsetZ()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double slope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(5, 5, -(5 * Math.Sqrt(2)) * slope);
			Point3d start = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 2));

			Vector3d v = SegmentedCalculator.CalculatePointWithMinSlope(iterator, nonSegmentedPipe);
			Point3d p = start.Add(v);// SegmentedCalculator.CalculatePointWithMinSlope(iterator, nonSegmentedPipe);
			Assert.IsNotNull(p);
			Assert.AreEqual(7, start.GetVectorTo(p).Length, 0.1, "segments");
			Assert.AreEqual(5.943596, p.X, 0.0001, "X");
			Assert.AreEqual(6.943596, p.Y, 0.0001, "Y");
			Assert.AreEqual( 1.1505, p.Z, 0.0001, "Z");
		}
		#endregion

		
		#region CalculatePointWithMinDelta
		// Basic tests, with flat terrain.
		[TestMethod]
		public void CalculatePointWithMinDelta_FlatTerrain_1()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, -0.5);
			Point3d start = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinDelta(iterator, nonSegmentedPipe, start, calc, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinDelta_FlatTerrain_2()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, -0.5);
			Point3d start = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinDelta(iterator, nonSegmentedPipe, start, calc, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinDelta_FlatTerrain_3()
		{
			double segmentLength = 1;
			double startEndSegment = 0.5;
			double minDelta = 1;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, -0.5);
			Point3d start = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinDelta(iterator, nonSegmentedPipe, start, calc, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinDelta_FlatTerrain_4()
		{
			double segmentLength = 1;
			double startEndSegment = 0.5;
			double minDelta = 1;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, -0.5);
			Point3d start = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinDelta(iterator, nonSegmentedPipe, start, calc, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
		}

		// Basic tests, with terrain going upwards, and downwards.
		[TestMethod]
		public void CalculatePointWithMinDelta_SlopeTerrain_Up1()
		{
			double segmentLength = 1;
			double startEndSegment = 0.5;
			double minDelta = 0;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, -0.5);
			Point3d start = new Point3d(0, 0, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new Face3dHeight(new Point3d(9, 0, 0), new Point3d(10, 1, 1), new Point3d(11, 0, 2));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinDelta(iterator, nonSegmentedPipe, start, calc, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinDelta_SlopeTerrain_Up2()
		{
			double segmentLength = 1;
			double startEndSegment = 0.5;
			double minDelta = 1;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, -0.5);
			Point3d start = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new Face3dHeight(new Point3d(9, 0, 0), new Point3d(10, 1, 1), new Point3d(11, 0, 2));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinDelta(iterator, nonSegmentedPipe, start, calc, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinDelta_SlopeTerrain_Down1()
		{
			double segmentLength = 1;
			double startEndSegment = 0.5;
			double minDelta = 2;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, -0.5);
			Point3d start = new Point3d(0, 0, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new Face3dHeight(new Point3d(11, 0, 0), new Point3d(10, 1, 1), new Point3d(9, 0, 2));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinDelta(iterator, nonSegmentedPipe, start, calc, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinDelta_SlopeTerrain_Down2()
		{
			double segmentLength = 1;
			double startEndSegment = 0.5;
			double minDelta = 1;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, -0.5);
			Point3d start = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new Face3dHeight(new Point3d(11, 0, 0), new Point3d(10, 1, 1), new Point3d(9, 0, 2));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinDelta(iterator, nonSegmentedPipe, start, calc, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
		}

		[TestMethod]
		public void CalculatePointWithMinDelta_SlopeTerrain_Up_XYByAngle_Center1()
		{
			// Test created with bricscad.
			// Pipe angle XY is 45deg.
			double segmentLength = 1;
			double startEndSegment = 0.5;
			double minDelta = 1;
			Vector3d nonSegmentedPipe = new Vector3d(5, 5, -0.1);
			Point3d start = new Point3d(0, 0, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			// Slope to the pipe should be 45deg, 
			TerrainCalculator calc = new Face3dHeight(new Point3d(5, 5, 1), new Point3d(3.64, 5.64, 0.5), new Point3d(4.29, 4.29, 0));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinDelta(iterator, nonSegmentedPipe, start, calc, minDelta);
			Assert.IsNotNull(p);
			// When point is calculated, get x and y (for 45deg):
			// xy = sqrt(radius^2 - Z^2)
			// x = y = xy / sqrt(2)
			// Calculated is that radius is 7 (z is -0.07).
			// xy = 6.99965 (if bricscad shows 7, it is because of precision displayed)
			// x = 4.9495
			// In bricscad it is not visible that xy = 6.99
			Assert.AreEqual(7, p.Value.GetAsVector().Length, 0.1);
			Assert.AreEqual(4.9495, p.Value.X, 0.0001, "X");
			Assert.AreEqual(4.9495, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(-0.0711, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinDelta_SlopeTerrain_Up_XYByAngle_Center2()
		{
			// Test created with bricscad.
			// Pipe angle XY is 45deg.
			double segmentLength = 1;
			double startEndSegment = 0.5;
			double minDelta = 1;
			Vector3d nonSegmentedPipe = new Vector3d(5.445, 5.445, -0.1);
			Point3d start = new Point3d(0, 0, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			// Slope to the pipe should be 45deg, 
			TerrainCalculator calc = new Face3dHeight(new Point3d(5, 5, 1), new Point3d(3.64, 5.64, 0.5), new Point3d(4.29, 4.29, 0));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinDelta(iterator, nonSegmentedPipe, start, calc, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(8, p.Value.GetAsVector().Length, 0.1);
			Assert.AreEqual(5.62276, p.Value.X, 0.0001, "X");
			Assert.AreEqual(5.62276, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0.8771, p.Value.Z, 0.0001, "Z");
		}
		// Tests where y != 0.
		[TestMethod]
		public void CalculatePointWithMinDelta_SlopeTerrain_Up_XYByAngle1()
		{
			// Test created with bricscad.
			// Pipe angle XY is 45deg.
			double segmentLength = 1;
			double startEndSegment = 0.5;
			double minDelta = 1;
			Vector3d nonSegmentedPipe = new Vector3d(5, 5, -0.1);
			Point3d start = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			// Slope to the pipe should be 45deg, 
			TerrainCalculator calc = new Face3dHeight(new Point3d(6, 7, 1), new Point3d(4.64, 7.64, 0.5), new Point3d(5.29, 6.29, 0));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinDelta(iterator, nonSegmentedPipe, start, calc, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(7, start.GetVectorTo(p.Value).Length, 0.1);
			Assert.AreEqual(5.9495, p.Value.X, 0.0001, "X");
			Assert.AreEqual(6.9495, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(-0.0711, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinDelta_SlopeTerrain_Up_XYByAngle2()
		{
			// Test created with bricscad.
			// Pipe angle XY is 45deg.
			double segmentLength = 1;
			double startEndSegment = 0.5;
			double minDelta = 1;
			Vector3d nonSegmentedPipe = new Vector3d(5.445, 5.445, -0.1);
			Point3d start = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			// Slope to the pipe should be 45deg, 
			TerrainCalculator calc = new Face3dHeight(new Point3d(6, 7, 1), new Point3d(4.64, 7.64, 0.5), new Point3d(5.29, 6.29, 0));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinDelta(iterator, nonSegmentedPipe, start, calc, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(8, start.GetVectorTo(p.Value).Length, 0.1);
			Assert.AreEqual(6.62276, p.Value.X, 0.0001, "X");
			Assert.AreEqual(7.62276, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0.8771, p.Value.Z, 0.0001, "Z");
		}

		[TestMethod]
		public void CalculatePointWithMinDelta_SlopeTerrain_Up_XYByAngle3()
		{
			// Test created with bricscad.
			// Pipe angle XY is 45deg.
			double segmentLength = 1;
			double startEndSegment = 0.5;
			double minDelta = 1;
			Vector3d nonSegmentedPipe = new Vector3d(5, 5, -0.1);
			Point3d start = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			// Slope to the pipe should be 45deg, 
			TerrainCalculator calc = new Face3dHeight(new Point3d(6, 7, 2.5), new Point3d(4.64, 7.64, 2), new Point3d(5.29, 6.29, 1.5));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinDelta(iterator, nonSegmentedPipe, start, calc, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(7, start.GetVectorTo(p.Value).Length, 0.1);
			Assert.AreEqual(5.9495, p.Value.X, 0.0001, "X");
			Assert.AreEqual(6.9495, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.4289, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinDelta_SlopeTerrain_Up_XYByAngle4()
		{
			// Test created with bricscad.
			// Pipe angle XY is 45deg.
			double segmentLength = 1;
			double startEndSegment = 0.5;
			double minDelta = 1;
			Vector3d nonSegmentedPipe = new Vector3d(5.445, 5.445, -0.1);
			Point3d start = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			// Slope to the pipe should be 45deg, 
			TerrainCalculator calc = new Face3dHeight(new Point3d(6, 7, 2.5), new Point3d(4.64, 7.64, 2), new Point3d(5.29, 6.29, 1.5));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinDelta(iterator, nonSegmentedPipe, start, calc, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(8, start.GetVectorTo(p.Value).Length, 0.1);
			Assert.AreEqual(6.62276, p.Value.X, 0.0001, "X");
			Assert.AreEqual(7.62276, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(2.3771, p.Value.Z, 0.0001, "Z");
		}
		#endregion


		#region Calculate point with min slope and delta

		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_Y0_MinSlopeUsed_test1()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.00;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, 0);
			Point3d start = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_Offset_MinSlopeUsed_test1()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.00;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, 0);
			Point3d start = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(11, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_OffsetZ_MinSlopeUsed_test1()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.00;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, 0);
			Point3d start = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 2.5));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(11, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_Y0_MinSlopeUsed_test2()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, -minSlope * 9.6);
			Point3d start = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(9.9875, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(-0.4994, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_offset_MinSlopeUsed_test2()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, -minSlope * 9.6);
			Point3d start = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10.9875, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(-0.4994, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_offsetZ_MinSlopeUsed_test2()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, -minSlope * 9.6);
			Point3d start = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 2.5));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10.9875, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5 - 0.4994, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_Y0_MinSlopeUsed_test3()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.00;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, 0);
			Point3d start = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(9, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_offset_MinSlopeUsed_test3()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.00;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, 0);
			Point3d start = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_offsetZ_MinSlopeUsed_test3()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.00;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, 0);
			Point3d start = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 2.5));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_Y0_MinSlopeUsed_test4()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, -minSlope * 9.4);
			Point3d start = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(8.98878, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(-0.44944, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_offset_MinSlopeUsed_test4()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, -minSlope * 9.4);
			Point3d start = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(9.98878, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(-0.44944, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_offsetZ_MinSlopeUsed_test4()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, -minSlope * 9.4);
			Point3d start = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 2.5));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(9.98878, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5 - 0.44944, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_Y0_MinDeltaUsed_test1()
		{
			// Terrain is 0, start height is 0. 
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, -minSlope * 9.6);
			Point3d start = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 0));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(9.9499, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(-1, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_offset_MinDeltaUsed_test1()
		{
			// Terrain is 0, start height is 0. 
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, -minSlope * 9.6);
			Point3d start = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 0));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10.9499, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(-1, p.Value.Z, 0.0001, "Z");
		}

		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_Y10_MinDeltaUsed_test1()
		{
			// Terrain is 0, start height is 0. 
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, -minSlope * 9.4);
			Point3d start = new Point3d(0,0,10);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 0));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(14, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(8.6602, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(-1, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_offsetZ_MinDeltaUsed_test1()
		{
			// Terrain is 0, start height is 0. 
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, -minSlope * 9.6);
			Point3d start = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1.5));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10.9499, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5 - 1, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_Y0_MinDeltaUsed_test2()
		{
			// Terrain is 0, start height is 0. 
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, -minSlope * 9.4);
			Point3d start = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 0));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(8.94427, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(-1, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_offset_MinDeltaUsed_test2()
		{
			// Terrain is 0, start height is 0. 
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, -minSlope * 9.4);
			Point3d start = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 0));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(9.94427, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(-1, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_offsetZ_MinDeltaUsed_test2()
		{
			// Terrain is 0, start height is 0. 
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, -minSlope * 9.4);
			Point3d start = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1.5));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(9.94427, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5 - 1, p.Value.Z, 0.0001, "Z");
		}

		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_y0_terrainSlopeUp_minDeltaUsed_test1()
		{
			// Terrain is 0, start height is 0. 
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, -minSlope * 9.6);
			Point3d start = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new Face3dHeight(new Point3d(7.05668, 0, 0), new Point3d(9.9875, 0, 0.5006), new Point3d(6.36396,1.0407, 0));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(9.9875, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(-0.4994, p.Value.Z, 0.0001, "Z");
		}

		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_offset_terrainSlopeUp_minDeltaUsed_test1()
		{
			// Terrain is 0, start height is 0. 
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, -minSlope * 9.6);
			Point3d start = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new Face3dHeight(new Point3d(8.05668, 2, 0), new Point3d(10.9875, 2, 0.5006), new Point3d(7.36396, 3.0407, 0));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10.9875, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(-0.4994, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_offsetZ_terrainSlopeUp_minDeltaUsed_test1()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, -minSlope * 9.6);
			Point3d start = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new Face3dHeight(new Point3d(8.05668, 2, 1.5), new Point3d(10.9875, 2, 2.5006), new Point3d(7.36396, 3.0407, 1.5));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10.9875, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5 - 0.4994, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_offsetZ_terrainSlopeUp_minSlopeUsed_test1()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 0.8;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, -minSlope * 9.6);
			Point3d start = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new Face3dHeight(new Point3d(8.05668, 2, 1.5), new Point3d(10.9875, 2, 2.5006), new Point3d(7.36396, 3.0407, 1.5));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10.9875, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5 - 0.4994, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_offsetZ_terrainSlopeUp_minDeltaUsed_test2()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, -minSlope * 9.6);
			Point3d start = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new Face3dHeight(new Point3d(8.05668, 2, 1.5), new Point3d(10.9875, 2, 2.0006), new Point3d(7.36396, 3.0407, 1.5));

			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaDownstream(iterator, nonSegmentedPipe, start, calc, minSlope, minDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, start.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10.9875, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5 - 0.4994, p.Value.Z, 0.0001, "Z");
		}
		// Da preuzme min slope umesto delta
		#endregion

		#region Upstream

		#region Upstream min slope and delta

		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_Y0_MinSlopeUsed_test1()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.00;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, 0);
			Point3d end = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 1, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
		}

		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_Offset_MinSlopeUsed_test1()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.00;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, 0);
			Point3d end = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 1, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(11, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
		}

		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_OffsetZ_MinSlopeUsed_test1()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.00;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, 0);
			Point3d end = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 2.5));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 2.5, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(11, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5, p.Value.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_Y0_MinSlopeUsed_test2()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, minSlope * 9.6);
			Point3d end = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 1, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(9.9875, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "z");
			Assert.AreEqual(-0.4994, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_offset_MinSlopeUsed_test2()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, minSlope * 9.6);
			Point3d end = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			startPointDelta = Math.Abs(startPointDelta);
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 1, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10.9875, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(-0.4994, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_offsetZ_MinSlopeUsed_test2()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, minSlope * 9.6);
			Point3d end = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 2.5));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 2.5, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10.9875, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(1.5 - 0.4994, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_Y0_MinSlopeUsed_test3()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.00;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, 0);
			Point3d end = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 1, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(9, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(0, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_offset_MinSlopeUsed_test3()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.00;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, 0);
			Point3d end = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 1, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(0, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_offsetZ_MinSlopeUsed_test3()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.00;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, 0);
			Point3d end = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 2.5));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 2.5, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(1.5, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_Y0_MinSlopeUsed_test4()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, minSlope * 9.4);
			Point3d end = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 1, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(8.98878, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(-0.44944, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_offset_MinSlopeUsed_test4()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, minSlope * 9.4);
			Point3d end = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 1, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(9.98878, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(-0.44944, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_offsetZ_MinSlopeUsed_test4()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, minSlope * 9.4);
			Point3d end = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 2.5));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 2.5, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(9.98878, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(1.5 - 0.44944, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_Y0_MinDeltaUsed_test1()
		{
			// Terrain is 0, start height is 0. 
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, minSlope * 9.6);
			Point3d end = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 0));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 0, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(9.9499, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(-1, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_offset_MinDeltaUsed_test1()
		{
			// Terrain is 0, start height is 0. 
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, minSlope * 9.6);
			Point3d end = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 0));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 0, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10.9499, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(-1, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_offsetZ_MinDeltaUsed_test1()
		{
			// Terrain is 0, start height is 0. 
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, minSlope * 9.6);
			Point3d end = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1.5));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 1.5, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10.9499, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(1.5 - 1, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_Y0_MinDeltaUsed_test2()
		{
			// Terrain is 0, start height is 0. 
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, minSlope * 9.4);
			Point3d end = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 0));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 0, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(8.94427, p.Value.X, 0.0001, "X");
			Assert.AreEqual(0, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(-1, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_offset_MinDeltaUsed_test2()
		{
			// Terrain is 0, start height is 0. 
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, minSlope * 9.4);
			Point3d end = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 0));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 0, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(9.94427, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(0, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(-1, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_offsetZ_MinDeltaUsed_test2()
		{
			// Terrain is 0, start height is 0. 
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.4, 0, minSlope * 9.4);
			Point3d end = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new PointTerrainHeight(new Point3d(0, 0, 1.5));

			double startPointDelta = calc.GetHeight(end.GetAs2d()) - end.Z;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, 1.5, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(9, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(9.94427, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(1.5 - 1, end.Z, 0.0001, "Z");
		}

		//NOTE Face terrain calculator. End point is fixed, start point is variable.
		// Start point z becomes remains on the same height as initial end point (so test results
		// from downstream can be used), end point z remains the same as in downstream.
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_y0_terrainSlopeUp_minDeltaUsed_test1()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, minSlope * 9.6);
			Point3d end = new Point3d();
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new Face3dHeight(new Point3d(7.05668, 0, 0), new Point3d(9.98750, 0, 0.5006), new Point3d(6.36396, 1.0407, 0));

			Point3d expectedStartPoint = new Point3d(9.9875, 0, 0);
			double startPointDelta = calc.GetHeight(expectedStartPoint.GetAs2d()) - end.Z;
			double endTerrainHeight = end.Z + minDelta;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, endTerrainHeight, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(expectedStartPoint, p.Value, "start point wrong");
			Assert.AreEqual(0 - 0.4994, end.Z, 0.0001, "end Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_offset_terrainSlopeUp_minDeltaUsed_test1()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, minSlope * 9.6);
			Point3d end = new Point3d(1, 2, 0);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new Face3dHeight(new Point3d(8.05668, 2, 0), new Point3d(10.9875, 2, 0.5006), new Point3d(7.36396, 3.0407, 0));

			Point3d expectedStartPoint = new Point3d(10.9875, 2, 0);
			double startPointDelta = calc.GetHeight(expectedStartPoint.GetAs2d()) - end.Z;
			double endTerrainHeight = end.Z + minDelta;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, endTerrainHeight, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(expectedStartPoint, p.Value, "start point wrong");
			Assert.AreEqual(-0.4994, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_offsetZ_terrainSlopeUp_minDeltaUsed_test1()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, minSlope * 9.6);
			Point3d end = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new Face3dHeight(new Point3d(8.05668, 2, 1.5), new Point3d(10.9875, 2, 2.5006), new Point3d(7.36396, 3.0407, 1.5));

			Point3d expectedStartPoint = new Point3d(10.9875, 2, 1.5);
			double startPointDelta = calc.GetHeight(expectedStartPoint.GetAs2d()) - end.Z;
			double endTerrainHeight = end.Z + minDelta;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, endTerrainHeight, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(expectedStartPoint, p.Value, "start point wrong");
			Assert.AreEqual(10.9875, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(1.5 - 0.4994, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_offsetZ_terrainSlopeUp_minSlopeUsed_test1()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 0.8;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, minSlope * 9.6);
			Point3d end = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new Face3dHeight(new Point3d(8.05668, 2, 1.5), new Point3d(10.9875, 2, 2.5006), new Point3d(7.36396, 3.0407, 1.5));

			Point3d expectedStartPoint = new Point3d(10.9875, 2, 1.5);
			double startPointDelta = calc.GetHeight(expectedStartPoint.GetAs2d()) - end.Z;
			double endTerrainHeight = end.Z + minDelta;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, endTerrainHeight, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10.9875, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(1.5 - 0.4994, end.Z, 0.0001, "Z");
		}
		[TestMethod]
		public void CalculatePointWithMinSlopeAndDelta_UP_offsetZ_terrainSlopeUp_minDeltaUsed_test2()
		{
			double segmentLength = 1;
			double startEndSegment = 0;
			double minDelta = 1;
			double minSlope = 0.05;
			Vector3d nonSegmentedPipe = new Vector3d(9.6, 0, minSlope * 9.6);
			Point3d end = new Point3d(1, 2, 1.5);
			double nonSegmentedLength = nonSegmentedPipe.Length;
			int segments = SegmentedDecorater.GetLowerSegmentCount(nonSegmentedLength, segmentLength, startEndSegment);
			SegmentedLengthIterator iterator = new SegmentedLengthIterator(segmentLength, startEndSegment, segments, segments + 1);
			TerrainCalculator calc = new Face3dHeight(new Point3d(8.05668, 2, 1.5), new Point3d(10.9875, 2, 2.0006), new Point3d(7.36396, 3.0407, 1.5));

			Point3d expectedStartPoint = new Point3d(10.9875, 2, 1.5);
			double startPointDelta = calc.GetHeight(expectedStartPoint.GetAs2d()) - end.Z;
			double endTerrainHeight = end.Z + minDelta;
			Point3d? p = SegmentedCalculator.CalculatePointWithMinSlopeAndDeltaUpstream(iterator, nonSegmentedPipe, ref end, endTerrainHeight, calc, minSlope, minDelta, startPointDelta);
			Assert.IsNotNull(p);
			Assert.AreEqual(10, end.GetVectorTo(p.Value).Length, 0.0001, "segment count");
			Assert.AreEqual(10.9875, p.Value.X, 0.0001, "X");
			Assert.AreEqual(2, p.Value.Y, 0.0001, "Y");
			Assert.AreEqual(1.5, p.Value.Z, 0.0001, "Z");
			Assert.AreEqual(1.5 - 0.4994, end.Z, 0.0001, "Z");
		}
		
		#endregion

		#endregion
	}
}
