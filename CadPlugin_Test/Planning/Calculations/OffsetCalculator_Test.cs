﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CadPlugin.Sewage.Plannings;
using CadPlugin.Sewage.Create.Settings;
using MyUtilities.Geometry;
using CadPlugin.Sewage.Planning;
using CadPlugin.Sewage.Planning.PipeShaftDataManagers;
using CadPlugin.Sewage.Planning.Calculations.OffsetCalculations;

namespace CadPlugin_Test.Planning.Calculations
{
	[TestClass]
	public class OffsetCalculator_Test
	{
		static double Precision = 0.00001;
		IPipeShaftData fixedSlopeData;
		PipeShaftDataManager fixedSlopeDataManager;
		IPipeShaftData fixedDeltaData;
		PipeShaftDataManager fixedDeltaDataManager;
		[TestInitialize]
		public void Prepare()
		{
			fixedSlopeDataManager = new DownstreamManager();
			fixedDeltaDataManager = new DownstreamManager();

			fixedSlopeData = fixedSlopeDataManager.Data;
			fixedSlopeData.ToggleStartShaftCreate(true);
			fixedSlopeData.ToggleEndShaftCreate(true);
			fixedSlopeData.StartPoint = new MyUtilities.Geometry.Point3d(0, 0, 0);
			fixedSlopeData.ApplyMinPipeSlope = true;
			//fixedSlopeData.ApplyMinDeltaShaftTerrainPipeOut = false;
			((PipeShaftData)fixedSlopeData).PipeData.Slope = 0.05;

			fixedDeltaData = fixedDeltaDataManager.Data;
			fixedDeltaData.ToggleStartShaftCreate(true);
			fixedDeltaData.ToggleEndShaftCreate(true);
			fixedDeltaData.StartPoint = new MyUtilities.Geometry.Point3d(0, 0, 0);
			fixedDeltaData.ApplyMinPipeSlope = false;
			((ShaftData)fixedDeltaData.EndShaft).MinPipeOutDeltaUsed = true;
			((PipeShaftData)fixedDeltaData).EndShaftData.TerrainHeight = 0;
			((PipeShaftData)fixedDeltaData).EndShaftData.PipeOutHeight = 0;
			((PipeShaftData)fixedDeltaData).PipeData.Slope = 0.05;
			((ShaftData)fixedDeltaData.EndShaft).MinPipeOutDelta = 2.025;


		}

		[TestMethod]
		public void UpdatePosition_FixedSlope_Down()
		{
			IPipeShaftData data = fixedSlopeData;
			OffsetCalculator calc = new OffsetDownstream(0.5, 0.5);
			data.EndPoint = new Point3d(11, 0, 0);
			calc.UpdatePosition(fixedSlopeDataManager, new PointTerrainHeight(new Point3d()));

			Assert.IsTrue(calc.EndPointOffset.X.Equal(10.5));
			Assert.IsTrue(calc.EndPointOffset.Z.Equal(-0.5));
		}

		[TestMethod]
		public void UpdatePosition_FixedPipeOutDelta()
		{
			IPipeShaftData data = fixedDeltaData;
			OffsetCalculator calc = new OffsetDownstream(0.5, 0.5);
			data.EndPoint = new Point3d(11, 0, 0);
			calc.UpdatePosition(fixedDeltaDataManager, new PointTerrainHeight(new Point3d()));

			Vector3d v = calc.StartPointOffset.GetVectorTo(calc.EndPointOffset);

			double slope = -v.Z / new Vector2d(v.X, v.Y).Length;
			Assert.AreEqual(slope, 0.2, Precision);
		}
		[TestMethod]
		public void UpdatePosition_CoordinatesNotChanged()
		{
			IPipeShaftData data = fixedSlopeData;
			OffsetCalculator calc = new OffsetDownstream(0.5, 0.5);
			data.EndPoint = new Point3d(11, 0, 0);
			calc.UpdatePosition(fixedSlopeDataManager, new PointTerrainHeight(new Point3d()));

			Assert.AreEqual(calc.EndPoint.GetAs2d(), data.EndPoint.GetAs2d(), "Position should be same as point is provided.");
		}
	}
}
