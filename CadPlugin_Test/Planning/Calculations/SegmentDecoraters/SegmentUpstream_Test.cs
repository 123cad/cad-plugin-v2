﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CadPlugin.Sewage.Plannings;
using CadPlugin.Sewage.Planning.Calculations.OffsetCalculations;
using CadPlugin.Sewage.Planning.PipeShaftDataManagers;
using CadPlugin.Sewage.Planning;
using CadPlugin.Sewage.Create.Settings;
using CadPlugin.Sewage.Planning.Calculations.SegmentDecoraters;
using MyUtilities.Geometry;
using CadPlugin.Sewage.Planning.Calculations;

namespace CadPlugin_Test.Planning.Calculations.SegmentDecoraters
{
	[TestClass]
	public class SegmentUpstream_Test
	{
		static double Precision = 0.00001;
		private PipeShaftDataManager mgr;

		PipeShaftData dataG;

		#region PipeOutslope only
		private void Initialize_fixedTerrain_OutSlope()
		{
			mgr = new UpstreamManager();
			mgr.TerrainHeightCalculator = new PointTerrainHeight(new Point3d(0, 0, 0));
			mgr.Data.ApplyMinPipeSlope = false;
			mgr.Data.ToggleStartShaftCreate(true);
			mgr.Data.ToggleEndShaftCreate(true);
			((ShaftData)mgr.Data.EndShaft).MinPipeOutDeltaUsed = true;

			((ShaftData)mgr.Data.EndShaft).MinPipeOutDelta = 1.4;
			((PipeData)mgr.Data.Pipe).SegmentLength = 1;
			double startEndSegment = 0.5;
			((PipeData)mgr.Data.Pipe).StartEndSegmentLength = startEndSegment;
			PipeCalculation pipe = new OffsetUpstream(startEndSegment, startEndSegment);
			PipeCalculation segments = new SegmentUpstream(pipe, mgr.Data.Pipe.SegmentLength, startEndSegment);

			mgr.Calculation = segments;
		}
		[TestMethod]
		public void UpdatePosition_1()
		{
			// Simple test, terrain is 0, start and end point are on 0.
			// Length is 10, from center shaft to center shaft.
			// This is 9 for pipe, - startEndPipeSegment = 8. - slope (which is caused by pipe out fixed)
			Initialize_fixedTerrain_OutSlope();
			mgr.Data.EndPoint = new MyUtilities.Geometry.Point3d(0, 0, 0);
			mgr.Data.StartPoint = new MyUtilities.Geometry.Point3d(10, 0, 0);

			mgr.Recalculate();

			Assert.AreEqual(new Point3d(0, 0, -1.375), mgr.Data.EndPoint, "end point height should be adjusted.");
			Assert.AreEqual(new Point3d(0.5, 0, -1.375), mgr.Data.Pipe.EndPoint, "end point height should be adjusted.");
			Assert.AreEqual(new Point3d(9.3943, 0, 0), mgr.Data.Pipe.StartPoint, "start point X should be adjusted.");
			Assert.AreEqual(new Point3d(9.8943, 0, 0), mgr.Data.StartPoint, "start point X should be adjusted.");
		}

		public void Prepare_fixedSlope_Up()
		{
			mgr = new UpstreamManager();

			dataG = (PipeShaftData)mgr.Data;
			dataG.ToggleStartShaftCreate(true);
			dataG.ToggleEndShaftCreate(true);
			dataG.StartPoint = new MyUtilities.Geometry.Point3d(0, 0, 0);
			dataG.ApplyMinPipeSlope = true;
			//psData.ApplyMinDeltaShaftTerrainPipeOut = false;
			dataG.PipeData.StartEndSegmentLength = 0.25;
			dataG.PipeData.Slope = 0.05;

		}
		public void Prepare_fixedDelta_Up()
		{
			mgr = new UpstreamManager();

			dataG = (PipeShaftData)mgr.Data;
			dataG.ToggleStartShaftCreate(true);
			dataG.ToggleEndShaftCreate(true);
			dataG.StartPoint = new MyUtilities.Geometry.Point3d(0, 0, 0);
			dataG.ApplyMinPipeSlope = false;
			((ShaftData)mgr.Data.EndShaft).MinPipeOutDeltaUsed = true;
			dataG.EndShaftData.TerrainHeight = 0;
			dataG.PipeData.StartEndSegmentLength = 0.25;
			dataG.EndShaftData.PipeOutHeight = -2.0 - dataG.EndShaft.InnerSlopeDeltaH;
			dataG.PipeData.Slope = 0.05;
		}


		[TestMethod]
		public void UpdatePosition_Up_FixedPipeOutDelta_StartEndSegment()
		{
			Prepare_fixedDelta_Up();
			IPipeShaftData data = dataG;
			OffsetCalculator calc = new OffsetUpstream(0.5, 0.5);
			SegmentedDecorater segmentCalc = new SegmentUpstream(calc, data.Pipe.SegmentLength, data.Pipe.StartEndSegmentLength);
			data.StartPoint = new Point3d(11, 0, 0);
			data.EndPoint = new Point3d(0, 0, 0);

			segmentCalc.UpdatePosition(mgr, new PointTerrainHeight(new Point3d()));

			Vector3d v = segmentCalc.StartPointOffset.GetVectorTo(segmentCalc.EndPointOffset);

			double segments = v.Length - data.Pipe.StartEndSegmentLength * 2;
			int segmentsR = (int)Math.Round(segments);
			Assert.AreEqual(10, segmentsR, Precision);
		}
		[TestMethod]
		public void UpdatePosition_Up_FixedPipeOutDelta_StartEndSegment_slopeTerrain()
		{
			Prepare_fixedDelta_Up();
			IPipeShaftData data = dataG;
			
			OffsetCalculator calc = new OffsetUpstream(0.5, 0.5);
			SegmentedDecorater segmentCalc = new SegmentUpstream(calc, data.Pipe.SegmentLength, data.Pipe.StartEndSegmentLength);
			data.StartPoint = new Point3d(11, 0, 0);
			data.EndPoint = new Point3d(0, 0, 0);

			segmentCalc.UpdatePosition(mgr, new Face3dHeight(new Point3d(5, 0, 0), new Point3d(10.8126, 0, data.EndShaft.MinPipeOutDelta), new Point3d(5, 1, 0)));
			//segmentCalc.UpdatePosition(mgr, new PointTerrainHeight(new Point3d()));

			Vector3d v = segmentCalc.StartPointOffset.GetVectorTo(segmentCalc.EndPointOffset);

			double segments = v.Length - data.Pipe.StartEndSegmentLength * 2;
			int segmentsR = (int)Math.Round(segments);
			Assert.AreEqual(10, segmentsR, Precision);
		}
		[TestMethod]
		public void UpdatePosition_Up_FixedSlope_StartEndSegment()
		{
			Prepare_fixedSlope_Up();
			IPipeShaftData data = dataG;
			OffsetCalculator calc = new OffsetUpstream(0.5, 0.5);
			SegmentedDecorater segmentCalc = new SegmentUpstream(calc, data.Pipe.SegmentLength, data.Pipe.StartEndSegmentLength);
			data.EndPoint = new Point3d(0, 0, 0);
			data.StartPoint = new Point3d(11, 0, 0);

			segmentCalc.UpdatePosition(mgr, new PointTerrainHeight(new Point3d(0,0,1)));

			Vector3d v = segmentCalc.StartPointOffset.GetVectorTo(segmentCalc.EndPointOffset);

			Assert.AreEqual(-10.486899, v.X, Precision);
			Assert.AreEqual(new Point2d(0, 0), data.EndPoint.GetAs2d(), "end shaft must not be moved");
		}
		#endregion
		#region PipeOutSlope and Delta
		// Included in SegmentedCalculator

		//NOTE Min slope is calculated in offset calculator - it is considered that min slope has been set. This means
		// that vector is only shrinked/extended, slope is not affected. Therefore, no tests needed.

		//NOTE Min delta is same as for downstream, no need for tests.
		// (end point is calculated relating to terrain and minDelta - just provide good values, algorithm is same)

		[TestMethod]
		public void test1()
		{
			mgr = new UpstreamManager();

			dataG = (PipeShaftData)mgr.Data;
			dataG.ToggleStartShaftCreate(true);
			dataG.ToggleEndShaftCreate(true);
			dataG.EndPoint = new MyUtilities.Geometry.Point3d(0, 0, 0);
			dataG.ApplyMinPipeSlope = true;
			((ShaftData)dataG.EndShaft).MinPipeOutDeltaUsed = true;
			dataG.PipeData.StartEndSegmentLength = 0.25;
			dataG.PipeData.SegmentLength = 1;
			dataG.PipeData.Slope = 0.05;

			OffsetCalculator calc = new OffsetUpstream(0.5, 0.5);
			SegmentedDecorater segCalc = new SegmentUpstream(calc, dataG.Pipe.SegmentLength, dataG.Pipe.StartEndSegmentLength);
			dataG.EndPoint = new Point3d(0, 0, 0);
			dataG.StartPoint = new Point3d(5, 0, 5);

			//segCalc.UpdatePosition(mgr, new PointTerrainHeight(new Point3d(0, 0, 0)));
			segCalc.UpdatePosition(mgr, new PointTerrainHeight(new Point3d(0, 0, 10)));

			Assert.AreEqual(0, segCalc.EndPoint.X);

		}
		#endregion
	}
}
