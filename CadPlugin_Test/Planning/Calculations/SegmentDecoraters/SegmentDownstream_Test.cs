﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CadPlugin_Test.Planning.Calculations.SegmentDecoraters
{
	[TestClass]
	public class SegmentDownstream_Test
	{
		//NOTE Downstream tests are included in SegmentedDecorater_Test 
		// (because initially there was only downstream class.
	}
}
