﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CadPlugin.Sewage.Plannings;
using CadPlugin.Sewage.Planning.PipeShaftDataManagers;
using CadPlugin.Sewage.Create.Settings;
using CadPlugin.Sewage.Planning.Calculations.OffsetCalculations;
using MyUtilities.Geometry;
using System.Collections.Generic;

namespace CadPlugin_Test.Planning.Calculations.OffsetCalculations
{
	/// <summary>
	/// Tests only if angle step is correct.
	/// </summary>
	[TestClass]
	public class OffsetUpstream_Test
	{
		#region Y = 0
		private PipeShaftDataManager dataManager;

		private void Prepare()
		{
			dataManager = new UpstreamManager();
			PipeCalculation calculation = new OffsetUpstream(0, 0);
			dataManager.Calculation = calculation;

			PipeShaftData data;
			data = (PipeShaftData)dataManager.Data;
			data.StartPoint = new MyUtilities.Geometry.Point3d(10, 0, 0);
			data.EndPoint = new MyUtilities.Geometry.Point3d(0, 0, 0);
			data.PipeData.AngleStepDeg = 90;
			data.PipeData.AngleSteppingUsed = true;

			//dataManager.MoveNextShaft();
			data.PipeData.Direction = data.StartPoint.GetVectorTo(data.EndPoint);
			((List<PipeShaftData>)dataManager.DataHistory).Add(data);
			data.EndPoint = data.StartPoint;
		}
		[TestMethod]
		public void AdjustAngleUpstream_notMoved_1()
		{
			Prepare();
			Point3d end = dataManager.Data.StartPoint;
			Point3d start =  new Point3d(11, 0, 0);
			((OffsetUpstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(end.GetAs2d(), dataManager.Data.EndPoint.GetAs2d(), "end point should not be moved");
			Assert.AreEqual(new Point2d(11, 0), start.GetAs2d(), "start point should not be moved");
		}
		[TestMethod]
		public void AdjustAngleUpstream_notMoved_2()
		{
			Prepare();
			Point3d end = dataManager.Data.StartPoint;
			Point3d start = new Point3d(11, 0.9, 0);
			((OffsetUpstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(end.GetAs2d(), dataManager.Data.EndPoint.GetAs2d(), "end point should not be moved");
			double x = 11 - end.X;
			double y = Math.Sqrt(x * x + 0.9 * 0.9);
			Assert.AreEqual(new Point2d(end.X + y, 0), start.GetAs2d(), "start point should not be moved");
		}
		[TestMethod]
		public void AdjustAngleUpstream_notMoved_3()
		{
			Prepare();
			Point3d end = dataManager.Data.StartPoint;
			// point is exactly 45 deg, which is rounded to 0.
			Point3d start = new Point3d(11, 1, 0);
			((OffsetUpstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(end.GetAs2d(), dataManager.Data.EndPoint.GetAs2d(), "end point should not be moved");
			Assert.AreEqual(new Point2d(end.X + 1.4142, 0), start.GetAs2d(), "start point should not be moved");
		}
		[TestMethod]
		public void AdjustAngleUpstream_Moved_1()
		{
			Prepare();
			Point3d end = dataManager.Data.StartPoint;
			// y should go little bit over 45 deg, so it is rounded up to 90 deg.
			Point3d start = new Point3d(11, 1.000000001, 0);
			((OffsetUpstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(end.GetAs2d(), dataManager.Data.EndPoint.GetAs2d(), "end point should not be moved");
			Assert.AreEqual(new Point2d(10, 1.4142), start.GetAs2d(), "start point ");
		}
		[TestMethod]
		public void AdjustAngleUpstream_Moved_2()
		{
			Prepare();
			Point3d end = dataManager.Data.StartPoint;
			Point3d start = new Point3d(11, 2, 0);
			((OffsetUpstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(end.GetAs2d(), dataManager.Data.EndPoint.GetAs2d(), "end point should not be moved");
			double x = 11 - start.X;
			double len = Math.Sqrt(x * x + 2 * 2);
			Assert.AreEqual(new Point2d(10, len), start.GetAs2d(), "start point");
		}
		#endregion
		#region Y = 1

		private void Prepare_Y()
		{
			dataManager = new UpstreamManager();
			PipeCalculation calculation = new OffsetUpstream(0, 0);
			dataManager.Calculation = calculation;

			PipeShaftData data;
			data = (PipeShaftData)dataManager.Data;
			data.StartPoint = new MyUtilities.Geometry.Point3d(10, 1, 0);
			data.EndPoint = new MyUtilities.Geometry.Point3d(0, 1, 0);
			data.PipeData.AngleStepDeg = 90;
			data.PipeData.AngleSteppingUsed = true;

			//dataManager.MoveNextShaft();
			data.PipeData.Direction = data.StartPoint.GetVectorTo(data.EndPoint);
			((List<PipeShaftData>)dataManager.DataHistory).Add(data);
			data.EndPoint = data.StartPoint;
		}
		[TestMethod]
		public void AdjustAngleUpstream_notMoved_1Y()
		{
			Prepare_Y();
			Point3d end = dataManager.Data.StartPoint;
			Point3d start = new Point3d(11, 1, 0);
			((OffsetUpstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(end.GetAs2d(), dataManager.Data.EndPoint.GetAs2d(), "end point should not be moved");
			Assert.AreEqual(new Point2d(11, 1), start.GetAs2d(), "start point should not be moved");
		}
		[TestMethod]
		public void AdjustAngleUpstream_notMoved_2Y()
		{
			Prepare_Y();
			Point3d end = dataManager.Data.StartPoint;
			Point3d start = new Point3d(11, 1.9, 0);
			((OffsetUpstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(end.GetAs2d(), dataManager.Data.EndPoint.GetAs2d(), "end point should not be moved");
			double x = 11 - end.X;
			double y = Math.Sqrt(x * x + 0.9 * 0.9);
			Assert.AreEqual(new Point2d(end.X + y, 1), start.GetAs2d(), "start point should not be moved");
		}
		[TestMethod]
		public void AdjustAngleUpstream_notMoved_3Y()
		{
			Prepare_Y();
			Point3d end = dataManager.Data.StartPoint;
			// point is exactly 45 deg, which is rounded to 0.
			Point3d start = new Point3d(11, 2, 0);
			((OffsetUpstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(end.GetAs2d(), dataManager.Data.EndPoint.GetAs2d(), "end point should not be moved");
			Assert.AreEqual(new Point2d(end.X + 1.4142, 1), start.GetAs2d(), "start point should not be moved");
		}
		[TestMethod]
		public void AdjustAngleUpstream_Moved_1Y()
		{
			Prepare_Y();
			Point3d end = dataManager.Data.StartPoint;
			// y should go little bit over 45 deg, so it is rounded up to 90 deg.
			Point3d start = new Point3d(11, 2.000000001, 0);
			((OffsetUpstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(end.GetAs2d(), dataManager.Data.EndPoint.GetAs2d(), "end point should not be moved");
			Assert.AreEqual(new Point2d(10, 2.4142), start.GetAs2d(), "start point ");
		}
		[TestMethod]
		public void AdjustAngleUpstream_Moved_2Y()
		{
			Prepare_Y();
			Point3d end = dataManager.Data.StartPoint;
			Point3d start = new Point3d(11, 3, 0);
			((OffsetUpstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(end.GetAs2d(), dataManager.Data.EndPoint.GetAs2d(), "end point should not be moved");
			double x = 11 - start.X;
			double len = Math.Sqrt(x * x + 2 * 2);
			Assert.AreEqual(new Point2d(10, len + 1), start.GetAs2d(), "start point");
		}
		#endregion
	}
}
