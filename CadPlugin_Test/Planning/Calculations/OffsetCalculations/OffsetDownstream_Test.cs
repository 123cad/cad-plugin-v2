﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CadPlugin.Sewage.Plannings;
using CadPlugin.Sewage.Planning.PipeShaftDataManagers;
using CadPlugin.Sewage.Create.Settings;
using CadPlugin.Sewage.Planning.Calculations.OffsetCalculations;
using MyUtilities.Geometry;
using System.Collections.Generic;

namespace CadPlugin_Test.Planning.Calculations.OffsetCalculations
{
	[TestClass]
	public class OffsetDownstream_Test
	{
		#region Y = 0
		private PipeShaftDataManager dataManager;

		private void Prepare()
		{
			dataManager = new DownstreamManager();
			PipeCalculation calculation = new OffsetDownstream(0, 0);
			dataManager.Calculation = calculation;

			PipeShaftData data;
			data = (PipeShaftData)dataManager.Data;
			data.StartPoint = new MyUtilities.Geometry.Point3d(0, 0, 0);
			data.EndPoint = new MyUtilities.Geometry.Point3d(10, 0, 0);
			data.PipeData.AngleStepDeg = 90;
			data.PipeData.AngleSteppingUsed = true;

			//dataManager.MoveNextShaft();
			data.PipeData.Direction = data.StartPoint.GetVectorTo(data.EndPoint);// new Vector3d(1, 0, 0);
			((List<PipeShaftData>)dataManager.DataHistory).Add(data);
			data.StartPoint = data.EndPoint;
		}
		[TestMethod]
		public void AdjustAngleDownstream_notMoved_1()
		{
			Prepare();
			Point3d start = dataManager.Data.StartPoint;
			Point3d end = new Point3d(11, 0, 0);
			((OffsetDownstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(start.GetAs2d(), dataManager.Data.StartPoint.GetAs2d(), "start point should not be moved");
			Assert.AreEqual(new Point2d(11, 0), end.GetAs2d(), "end point should not be moved");
		}
		[TestMethod]
		public void AdjustAngleDownstream_notMoved_2()
		{
			Prepare();
			Point3d start = dataManager.Data.StartPoint;
			Point3d end = new Point3d(11, 0.9, 0);
			((OffsetDownstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(start.GetAs2d(), dataManager.Data.StartPoint.GetAs2d(), "start point should not be moved");
			double x = 11 - start.X;
			double y = Math.Sqrt(x * x + 0.9 * 0.9);
			Assert.AreEqual(new Point2d(start.X + y, 0), end.GetAs2d(), "end point should not be moved");
		}
		[TestMethod]
		public void AdjustAngleDownstream_notMoved_3()
		{
			Prepare();
			Point3d start = dataManager.Data.StartPoint;
			// point is exactly 45 deg, which is rounded to 0.
			Point3d end = new Point3d(11, 1, 0);
			((OffsetDownstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(start.GetAs2d(), dataManager.Data.StartPoint.GetAs2d(), "start point should not be moved");
			Assert.AreEqual(new Point2d(start.X + 1.4142, 0), end.GetAs2d(), "end point should not be moved");
		}
		[TestMethod]
		public void AdjustAngleDownstream_Moved_1()
		{
			Prepare();
			Point3d start = dataManager.Data.StartPoint;
			// y should go little bit over 45 deg, so it is rounded up to 90 deg.
			Point3d end = new Point3d(11, 1.000000001, 0);
			((OffsetDownstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(start.GetAs2d(), dataManager.Data.StartPoint.GetAs2d(), "start point should not be moved");
			Assert.AreEqual(new Point2d(10, 1.4142), end.GetAs2d(), "end point");
		}
		[TestMethod]
		public void AdjustAngleDownstream_Moved_2()
		{
			Prepare();
			Point3d start = dataManager.Data.StartPoint;
			Point3d end = new Point3d(11, 2, 0);
			((OffsetDownstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(start.GetAs2d(), dataManager.Data.StartPoint.GetAs2d(), "start point should not be moved");
			double x = 11 - start.X;
			double len = Math.Sqrt(x * x + 2 * 2);
			Assert.AreEqual(new Point2d(10, len), end.GetAs2d(), "end point");
		}

		#endregion

		#region Y=1

		private void Prepare_Y()
		{
			dataManager = new DownstreamManager();
			PipeCalculation calculation = new OffsetDownstream(0, 0);
			dataManager.Calculation = calculation;

			PipeShaftData data;
			data = (PipeShaftData)dataManager.Data;
			data.StartPoint = new MyUtilities.Geometry.Point3d(0, 1, 0);
			data.EndPoint = new MyUtilities.Geometry.Point3d(10, 1, 0);
			data.PipeData.AngleStepDeg = 90;
			data.PipeData.AngleSteppingUsed = true;

			//dataManager.MoveNextShaft();
			data.PipeData.Direction = data.StartPoint.GetVectorTo(data.EndPoint);// new Vector3d(1, 0, 0);
			((List<PipeShaftData>)dataManager.DataHistory).Add(data);
			data.StartPoint = data.EndPoint;
		}
		[TestMethod]
		public void AdjustAngleDownstream_notMoved_1Y()
		{
			Prepare_Y();
			Point3d start = dataManager.Data.StartPoint;
			Point3d end = new Point3d(11, 1, 0);
			((OffsetDownstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(start.GetAs2d(), dataManager.Data.StartPoint.GetAs2d(), "start point should not be moved");
			Assert.AreEqual(new Point2d(11, 1), end.GetAs2d(), "end point should not be moved");
		}
		[TestMethod]
		public void AdjustAngleDownstream_notMoved_2Y()
		{
			Prepare_Y();
			Point3d start = dataManager.Data.StartPoint;
			Point3d end = new Point3d(11, 1.9, 0);
			((OffsetDownstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(start.GetAs2d(), dataManager.Data.StartPoint.GetAs2d(), "start point should not be moved");
			double x = 11 - start.X;
			double y = Math.Sqrt(x * x + 0.9 * 0.9);
			Assert.AreEqual(new Point2d(start.X + y, 1), end.GetAs2d(), "end point should not be moved");
		}
		[TestMethod]
		public void AdjustAngleDownstream_notMoved_3Y()
		{
			Prepare_Y();
			Point3d start = dataManager.Data.StartPoint;
			// point is exactly 45 deg, which is rounded to 0.
			Point3d end = new Point3d(11, 2, 0);
			((OffsetDownstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(start.GetAs2d(), dataManager.Data.StartPoint.GetAs2d(), "start point should not be moved");
			Assert.AreEqual(new Point2d(start.X + 1.4142, 1), end.GetAs2d(), "end point should not be moved");
		}
		[TestMethod]
		public void AdjustAngleDownstream_Moved_1Y()
		{
			Prepare_Y();
			Point3d start = dataManager.Data.StartPoint;
			// y should go little bit over 45 deg, so it is rounded up to 90 deg.
			Point3d end = new Point3d(11, 2.000000001, 0);
			((OffsetDownstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(start.GetAs2d(), dataManager.Data.StartPoint.GetAs2d(), "start point should not be moved");
			Assert.AreEqual(new Point2d(10, 1 + 1.4142), end.GetAs2d(), "end point");
		}
		[TestMethod]
		public void AdjustAngleDownstream_Moved_2Y()
		{
			Prepare_Y();
			Point3d start = dataManager.Data.StartPoint;
			Point3d end = new Point3d(11, 3, 0);
			((OffsetDownstream)dataManager.Calculation).AdjustAngle_Test(dataManager, ref start, ref end);

			Assert.AreEqual(start.GetAs2d(), dataManager.Data.StartPoint.GetAs2d(), "start point should not be moved");
			double x = 11 - start.X;
			double len = Math.Sqrt(x * x + 2 * 2);
			Assert.AreEqual(new Point2d(10, len + 1), end.GetAs2d(), "end point");
		}

		#endregion
	}
}
