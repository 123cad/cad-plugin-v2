﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CadPlugin.Sewage.Create.Settings;
using MyUtilities.Geometry;
using CadPlugin.Sewage.Plannings;
using CadPlugin.Sewage.Planning;
using CadPlugin.Sewage.Planning.PipeShaftDataManagers;
using CadPlugin.Sewage.Planning.Calculations.OffsetCalculations;
using CadPlugin.Sewage.Planning.Calculations.SegmentDecoraters;

namespace CadPlugin_Test.Planning.Calculations
{
	[TestClass]
	public class SegmentedDecorater_Test
	{
		static double Precision = 0.00001;
		PipeShaftData psData;
		PipeShaftDataManager mgr;
		public void Prepare_fixedSlope_Down()
		{
			mgr = new DownstreamManager();

			psData = (PipeShaftData)mgr.Data;
			psData.ToggleStartShaftCreate(true);
			psData.ToggleEndShaftCreate(true);
			psData.StartPoint = new MyUtilities.Geometry.Point3d(0, 0, 0);
			psData.ApplyMinPipeSlope = true;
			//psData.ApplyMinDeltaShaftTerrainPipeOut = false;
			psData.PipeData.StartEndSegmentLength = 0.25;
			psData.PipeData.Slope = 0.05;

		}
		public void Prepare_fixedDelta_Down()
		{
			mgr = new DownstreamManager();

			psData = (PipeShaftData)mgr.Data;
			psData.ToggleStartShaftCreate(true);
			psData.ToggleEndShaftCreate(true);
			psData.StartPoint = new MyUtilities.Geometry.Point3d(0, 0, 0);
			psData.ApplyMinPipeSlope = false;
			((ShaftData)psData.EndShaft).MinPipeOutDeltaUsed = true;
			psData.EndShaftData.TerrainHeight = 0;
			psData.PipeData.StartEndSegmentLength = 0.25;
			psData.EndShaftData.PipeOutHeight = -2.0 - psData.EndShaft.InnerSlopeDeltaH;
			psData.PipeData.Slope = 0.05;
		}
		[TestMethod]
		public void UpdatePosition_FixedPipeOutDelta_StartEndSegment()
		{
			Prepare_fixedDelta_Down();
			IPipeShaftData data = psData;
			OffsetCalculator calc = new OffsetDownstream(0.5, 0.5);
			SegmentedDecorater segmentCalc = new SegmentDownstream(calc, data.Pipe.SegmentLength, data.Pipe.StartEndSegmentLength);
			data.EndPoint = new Point3d(11, 0, 0);

			segmentCalc.UpdatePosition(mgr, new PointTerrainHeight(new Point3d()));

			Vector3d v = segmentCalc.StartPointOffset.GetVectorTo(segmentCalc.EndPointOffset);

			double segments = v.Length - data.Pipe.StartEndSegmentLength * 2;
			int segmentsR = (int)Math.Round(segments);
			Assert.AreEqual(10, segmentsR, Precision);
		}
		[TestMethod]
		public void UpdatePosition_FixedSlope_StartEndSegment()
		{
			Prepare_fixedSlope_Down();
			IPipeShaftData data = psData;
			OffsetCalculator calc = new OffsetDownstream(0.5, 0.5);
			SegmentedDecorater segmentCalc = new SegmentDownstream(calc, data.Pipe.SegmentLength, data.Pipe.StartEndSegmentLength);
			data.EndPoint = new Point3d(11, 0, 0);
			segmentCalc.UpdatePosition(mgr, new PointTerrainHeight(new Point3d()));

			Vector3d v = segmentCalc.StartPointOffset.GetVectorTo(segmentCalc.EndPointOffset);

			Assert.AreEqual(10.486899, v.X, Precision);
		}

		[TestMethod]
		public void GetNumberOfSegments()
		{
			double lenght = 9.5;
			double segmentLength = 1;
			double startEndSegment = 0;
			int segments = SegmentedDecorater.GetNumberOfSegments(lenght, segmentLength, startEndSegment);
			Assert.AreEqual(9, segments);

			lenght = 9.5;
			segmentLength = 1;
			startEndSegment = 0.25;
			segments = SegmentedDecorater.GetNumberOfSegments(lenght, segmentLength, startEndSegment);
			Assert.AreEqual(9, segments);

			lenght = 8.9;
			segmentLength = 1;
			startEndSegment = 0;
			segments = SegmentedDecorater.GetNumberOfSegments(lenght, segmentLength, startEndSegment);
			Assert.AreEqual(9, segments);

			lenght = 9.2;
			segmentLength = 1;
			startEndSegment = 0.25;
			segments = SegmentedDecorater.GetNumberOfSegments(lenght, segmentLength, startEndSegment);
			Assert.AreEqual(9, segments);

		}

		[TestMethod]
		public void GetLowerSegmentCount_Test()
		{
			double lenght = 9.5;
			double segmentLength = 1;
			double startEndSegment = 0;

			int segments = SegmentedDecorater.GetLowerSegmentCount(lenght, segmentLength, startEndSegment);
			Assert.AreEqual(9, segments);

			lenght = 9.8;
			segments = SegmentedDecorater.GetLowerSegmentCount(lenght, segmentLength, startEndSegment);
			Assert.AreEqual(9, segments);

			lenght = 9.5;
			segmentLength = 2;
			startEndSegment = 0.25;
			segments = SegmentedDecorater.GetLowerSegmentCount(lenght, segmentLength, startEndSegment);
			Assert.AreEqual(4, segments);

			startEndSegment = 1;
			segments = SegmentedDecorater.GetLowerSegmentCount(lenght, segmentLength, startEndSegment);
			Assert.AreEqual(3, segments);
		}
	}
}
