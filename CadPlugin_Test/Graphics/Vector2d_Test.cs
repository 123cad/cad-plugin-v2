﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyUtilities.Geometry;
namespace CadPlugin_Test.Graphics
{
	[TestClass]
	public class Vector2d_Test
	{
		[TestMethod]
		public void MyTestMethod()
		{
			Vector2d v1 = new Vector2d(10,0);
			Vector2d v2 = new Vector2d(10, 10);
			double angle = v1.GetAngleDeg(v2);

			Assert.AreEqual(angle, 45, 0.0001);

			angle = v2.GetAngleDeg(v1);

			Assert.IsTrue(angle.Equal(-45));

			v2 = new Vector2d(1, 9);
			angle = v1.GetAngleDeg(v2);
			angle = v2.GetAngleDeg(v1);

			// Ako pukne test, proveriti sve uglove. 
			// ocekivano je da metoda vraca 0-90.

		}
	}
}
