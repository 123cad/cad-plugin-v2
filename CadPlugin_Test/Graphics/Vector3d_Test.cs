﻿using MyUtilities.Geometry;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin_Test.Graphics
{
	[TestClass]
	public class Vector3d_Test
	{
		[TestMethod]
		public void MyTestMethod()
		{
			Vector3d v1 = new Vector3d();
			Vector3d v2 = new Vector3d(1, 2, 3);

			Vector3d t = v1.Add(v2);

			Assert.IsTrue(t.X.Equal(1) && t.Y.Equal(2) && t.Z.Equal(3));
		}
	}
}
