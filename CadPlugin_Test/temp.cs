﻿using MyUtilities.Geometry;
using CadPlugin.HatchToolbars.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Windows.Forms;

namespace CadPlugin_Test
{
	[TestClass]
	public class MyTestClass
	{
		//[TestMethod]
		public void MyTestMethod()
		{
			XmlSerializer ser = new XmlSerializer(typeof(HatchType));
			HatchType t = new Predefined();
			string xml = "";

			using (StringWriter sw = new StringWriter())
			{
				using (XmlWriter writer = XmlWriter.Create(sw)) 
				{
					ser.Serialize(writer, t);
					xml = sw.ToString();
				}
			}
			
		}
	}
}
