﻿using CadPlugin.PositionMaker.Models;
using Inventory.Positions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin_Test.Positions
{
	[TestClass]
	public class PositionsManager_Tests
	{

		[TestMethod]
		public void AddSegment_Test1()
		{
			PositionsManager mgr = new PositionsManager();
			SegmentNode t = new SegmentNode("10", null);
			mgr.AddSegment(t);
			t = new SegmentNode("02", null);
			mgr.AddSegment(t);
			t = new SegmentNode("01", null);
			mgr.AddSegment(t);

			var res = new string[]
			{
				"01",
				"02",
				"10"
			};
			for (int i = 0; i < mgr.FirstSegmentsCollection.Count(); i++)
				Assert.AreEqual(res[i], mgr.FirstSegmentsCollection.ElementAt(i).Id, "Values not ordered correctly!");

		}

		[TestMethod]
		public void GetAllSegments_Test()
		{
			PositionsManager mgr = new PositionsManager();
			SegmentNode start = new SegmentNode("02", null);
			mgr.AddSegment(start);
			SegmentNode n = new SegmentNode("12", start);
			start.AddChild(n);
			SegmentData data = new SegmentData("011", n)
			{
				Description = "Long description",
				Quantity = 1.43,
				UnitPrice = 12.2,
				VatPercent = 7
			};
			n.AddChild(data);
			data = new SegmentData("001", n)
			{
				Description = "Long description",
				Quantity = 16.43,
				UnitPrice = 124.2,
				VatPercent = 0
			};
			n.AddChild(data);

			IEnumerable<Segment> en = mgr.GetAllSegments();
			List<Segment> list = en.ToList();
			Assert.AreEqual(4, list.Count);
			Assert.AreEqual("02", list[0].Id);
			Assert.IsInstanceOfType(list[1], typeof(SegmentNode));
			Assert.IsInstanceOfType(list[2], typeof(SegmentData));
			Assert.AreEqual("001", list[2].Id);
			Assert.AreEqual("011", list[3].Id);
			Assert.AreEqual(2, list[3].LevelIndex);

		}
	}
}
