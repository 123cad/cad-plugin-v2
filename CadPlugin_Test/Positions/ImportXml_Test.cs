﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CadPlugin.PositionMaker.Models;
using System.Collections.Generic;
using System.Linq;
using Inventory.Positions;

namespace CadPlugin_Test.Positions
{
	[TestClass]
	public class ImportXml_Test
	{
		[TestMethod]
		public void LoadXmlTest()
		{
			string xml = @"
<c123cad>
	<Positions>
		<Elements>
			<Segment>
				<Id>01</Id>
				<Title>Level 1 segment</Title>
				<Children>
					<Segment>
						<Id>12</Id>
						<Title>Level 2 segment</Title>
						<PositionData>
							<Description></Description>
							<Quantity>1.54</Quantity>
							<UnitPrice>23.63</UnitPrice>
							<TotalTax percent=""7""></TotalTax>
						</PositionData>
					</Segment>
					<Segment>
						<Id>03</Id>
						<Title>Level 2 segment</Title>
						<PositionData>
							<Description></Description>
							<Quantity>8.546</Quantity>
							<UnitPrice>12.51</UnitPrice>
							<TotalTax percent=""0.0""></TotalTax>
						</PositionData>
					</Segment>
				</Children>
			</Segment>
		</Elements>
	</Positions>
</c123cad>
				";
			PositionsManager mgr = ImportXml.LoadFromString(xml);
			var seg0 = mgr.FirstSegmentsCollection.First();
			Assert.IsInstanceOfType(seg0, typeof(SegmentNode));
			Assert.AreEqual("01", seg0.Id);
			var seg1 = ((SegmentNode)seg0).Children.First();
			Assert.AreEqual("03", seg1.Id);
			Assert.IsInstanceOfType(seg1, typeof(SegmentData));
			var seg1D = (SegmentData)seg1;
			Assert.AreEqual(106.91, seg1D.TotalPrice, 0.01);
		}
	}
}
