﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CadPlugin.PositionMaker.Models;
using Inventory.Positions;

namespace CadPlugin_Test.Positions
{
	[TestClass]
	public class ExportXml_Test
	{
		[TestMethod]
		public void SaveXml_Test()
		{
			PositionsManager mgr = new PositionsManager();
			SegmentNode start = new SegmentNode("02", null);
			mgr.AddSegment(start);
			SegmentNode n = new SegmentNode("12", start);
			start.AddChild(n);
			SegmentData data = new SegmentData("001", n)
			{
				Description = "Long description",
				Quantity = 1.43,
				UnitPrice = 12.2,
				VatPercent = 7
			};
			n.AddChild(data);
			data = new SegmentData("011", n)
			{
				Description = "Long description",
				Quantity = 16.43,
				UnitPrice = 124.2,
				VatPercent = 0
			};
			n.AddChild(data);

			ExportXml.SaveFile(mgr, "test1.xml");

			string s = ExportXml.SaveToString(mgr);
			System.IO.File.WriteAllText("test.xml", s);
			System.Diagnostics.Process.Start("test.xml");
		}
	}
}
