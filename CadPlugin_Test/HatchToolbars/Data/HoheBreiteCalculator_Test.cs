﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyUtilities.Geometry;
using System.Linq;
using CadPlugin.HatchToolbars.Data;
using System.Collections.Generic;

namespace CadPlugin_Test.HatchToolbars.Data
{
	[TestClass]
	public class HoheBreiteCalculator_Test
	{
		[TestMethod]
		public void RectangleTester()
		{
			double br = 0;
			double ho = 0;
			Point3d[] points = Enumerable.Repeat(new Point3d(), 5).ToArray();
			bool res = HoheBreiteCalculator.Calculate(ref ho, ref br, points);
			Assert.IsFalse(res, "points not recognized as rectangle");

			points = Enumerable.Repeat(new Point3d(), 4).ToArray();
			points[0] = new Point3d(1, 1, 0);
			points[1] = new Point3d(2, 1, 0);
			points[2] = new Point3d(2, 0, 0);
			points[3] = new Point3d(1, 0, 0);

			// Rotate all points to check if index of top left point is not important.
			List<Point3d> list = points.ToList();
			for (int i = 0; i < 3; i++)
			{
				Point3d pt = list.First();
				list.RemoveAt(0);
				list.Add(pt);

				res = HoheBreiteCalculator.Calculate(ref ho, ref br, points);
				Assert.IsTrue(res, "not recognized as rectangle, but all vertices have angles >0");
			}
			

			points[0] = new Point3d(0, 1, 0);
			points[1] = new Point3d(0, 0, 0);
			points[2] = new Point3d(1, 0, 0);
			points[3] = new Point3d(1, 1, 0);
			// Rotate all points to check if index of top left point is not important.
			list = points.ToList();
			for (int i = 0; i < 3; i++)
			{
				Point3d pt = list.First();
				list.RemoveAt(0);
				list.Add(pt);

				res = HoheBreiteCalculator.Calculate(ref ho, ref br, points);
				Assert.IsTrue(res, "not recognized as rectangle, but all vertices have angles <0");
			}


			points[0] = new Point3d(0, 1, 0);
			points[2] = new Point3d(0, 0, 0);
			points[1] = new Point3d(1, 0, 0);
			points[3] = new Point3d(1, 1, 0);
			// Rotate all points to check if index of top left point is not important.
			list = points.ToList();
			for (int i = 0; i < 3; i++)
			{
				Point3d pt = list.First();
				list.RemoveAt(0);
				list.Add(pt);

				res = HoheBreiteCalculator.Calculate(ref ho, ref br, points);
				Assert.IsFalse(res, "recognized as rectangle , but it isn't");
			}


			points = Enumerable.Repeat(new Point3d(), 4).ToArray();
			points[0] = new Point3d(0, 1, 0);
			points[2] = new Point3d(1, 1, 0);
			points[1] = new Point3d(1, 0, 0);
			points[3] = new Point3d(0, 0, 0);

			// Rotate all points to check if index of top left point is not important.
			list = points.ToList();
			for (int i = 0; i < 3; i++)
			{
				Point3d pt = list.First();
				list.RemoveAt(0);
				list.Add(pt);

				res = HoheBreiteCalculator.Calculate(ref ho, ref br, points);
				Assert.IsFalse(res, "recognized as rectangle, butit shouldn't");
			}
		}

		[TestMethod]
		public void HoheBreiteTester()
		{
			double breite = 0;
			double hohe = 0;
			Point3d[] points = Enumerable.Repeat(new Point3d(), 4).ToArray();

			points[0] = new Point3d(1, 1, 0);
			points[1] = new Point3d(2, 1, 0);
			points[2] = new Point3d(2, 0, 0);
			points[3] = new Point3d(1, 0, 0);
			bool res = HoheBreiteCalculator.Calculate(ref hohe, ref breite, points);
			Assert.IsTrue(res);
			Assert.AreEqual(breite, 1, 0.00000001);
			Assert.AreEqual(hohe, 1, 0.00000001);

			points[0] = new Point3d(1, 1, 0);
			points[1] = new Point3d(3, 1, 0);
			points[2] = new Point3d(2, 0, 0);
			points[3] = new Point3d(1, 0, 0);
			res = HoheBreiteCalculator.Calculate(ref hohe, ref breite, points);
			Assert.IsTrue(res);
			Assert.AreEqual(breite, 1.5, 0.00000001);
			Assert.AreEqual(hohe, 1.2071, 0.0001);

			points[0] = new Point3d(1, 1, 0);
			points[1] = new Point3d(2, 1, 0);
			points[2] = new Point3d(3, 0, 0);
			points[3] = new Point3d(1, 0, 0);
			res = HoheBreiteCalculator.Calculate(ref hohe, ref breite, points);
			Assert.IsTrue(res);
			Assert.AreEqual(breite, 1.5, 0.00000001);
			Assert.AreEqual(hohe, 1.2071, 0.0001);

			points[0] = new Point3d(1, 1, 0);
			points[1] = new Point3d(3, 1, 0);
			points[2] = new Point3d(3, 0, 0);
			points[3] = new Point3d(1, 0, 0);
			res = HoheBreiteCalculator.Calculate(ref hohe, ref breite, points);
			Assert.IsTrue(res);
			Assert.AreEqual(breite, 2, 0.00000001);
			Assert.AreEqual(hohe, 1, 0.00000001);
		}
	}
}
