﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPlugin_Test
{
	static class DoubleEqualPrecision
	{
		public static double Precision = 0.00001;
		public static bool Equal(this double d, double d1)
		{
			double t = Math.Abs(d - d1);
			return t < Precision;
		}
	}
}
