﻿Used for loading Autocad/Bricscad plugin.

We want 1 file to be present in installation directory. In the past we had Autocad/Bricscad dlls, but it required manually loading all related assemblies (
because of probing; current dir and bin are default, and we needed Libraries subdirectory).
Using this, old Autocad/Bricscad will be moved to Libraries, therefore probing will automatically find all required assemblies (except few special ones, 
which are still loaded manually). This assembly will just manually load assembly in Libraries.